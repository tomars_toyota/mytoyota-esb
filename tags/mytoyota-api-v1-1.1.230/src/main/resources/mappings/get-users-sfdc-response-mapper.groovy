import groovy.util.XmlSlurper
import static groovy.json.JsonOutput.*

def userResponse = new XmlSlurper().parseText(payload)?.result?.ownerDetails

user = [:]
user.firstName = userResponse.firstName.text()
user.lastName = userResponse.lastName.text()
user.email = userResponse.email.text()

return prettyPrint(toJson(user))