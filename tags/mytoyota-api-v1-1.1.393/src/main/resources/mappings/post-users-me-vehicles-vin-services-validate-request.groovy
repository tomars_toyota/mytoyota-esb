import groovy.json.JsonSlurper

if(!payload)
	throw new org.mule.module.apikit.exception.BadRequestException('Invalid request')

// Helper to remove leading zeroes
// Used when checking dealer code against known codes that support service booking
def stripLeadingZeros = { s ->
	return s.replaceFirst("^0*", "")
}

apiRequest = new JsonSlurper().parseText(payload)

// Dealer Service Booking safety check
// In order for a Dealer to be "Service Booking Enabled" it must:
//   1) Have a dmsID (serviceSiteId) value
//   2) Dealer code must be configured in properties
// The purpose of this is to ensure that Service Booking isn't possible even if the dmsID is populated

// Read list of supported Dealer Codes from a property
def serviceBookingDealersList = serviceBookingDealerCodes.split(",").collect {it.trim() as String}
// Check if current Dealer Code is included
if (! (serviceBookingDealersList.contains( stripLeadingZeros(apiRequest['dealerId']))) ) {
	throw new org.mule.module.apikit.exception.BadRequestException('Service booking not supported for '+apiRequest['dealerId'])
}

//validate request
if(!(apiRequest.containsKey('dealerId') &&
		apiRequest.containsKey('branchCode') &&
		apiRequest.containsKey('operationTypeID') &&
		apiRequest.containsKey('operationID')
	))
	throw new org.mule.module.apikit.exception.BadRequestException('Invalid request')

return payload