import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper

def payloadMap = new JsonSlurper().parseText(payload)
def request = [:]
def user = [:]

request['cosiId'] = flowVars['orderId']

return prettyPrint(toJson(request))
