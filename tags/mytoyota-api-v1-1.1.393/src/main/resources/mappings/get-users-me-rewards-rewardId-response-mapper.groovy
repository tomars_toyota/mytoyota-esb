import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper
import au.com.toyota.esb.mytoyota.api.util.GroovyHelper

reward = new JsonSlurper().parseText(payload)
reward.remove('isSuccessful')
reward.remove('message')
if(reward?.cardFront?.cardImage)
	reward.cardFront.cardImage='https://'+ sitecoreHost + reward.cardFront.cardImage

return prettyPrint(toJson(GroovyHelper.removeNulls(reward, true)))