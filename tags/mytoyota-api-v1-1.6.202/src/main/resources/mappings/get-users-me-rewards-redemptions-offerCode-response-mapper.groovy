import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper
import au.com.toyota.esb.mytoyota.api.util.GroovyHelper
import java.text.*

def rewardResponse = null 
if (payload) {
	rewardResponse = new JsonSlurper().parseText(payload)
}


def response = [:]
try{
	def coupon = rewardResponse?.coupons?.first()
	response.status = coupon?.dailyClaimedStatus
	response.lastClaimedDate = coupon?.lastRedemptionDate
} catch(Exception ex) {
	response.status = "VALID"
	response.lastClaimedDate = ""
}

return prettyPrint(toJson(GroovyHelper.removeNulls(response, true)))