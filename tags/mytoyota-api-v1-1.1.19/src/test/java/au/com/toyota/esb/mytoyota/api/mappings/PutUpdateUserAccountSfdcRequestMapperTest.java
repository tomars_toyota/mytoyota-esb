package au.com.toyota.esb.mytoyota.api.mappings;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

import javax.script.*;

import org.apache.commons.io.IOUtils;
import org.custommonkey.xmlunit.Diff;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

public class PutUpdateUserAccountSfdcRequestMapperTest extends AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;
	
	@BeforeClass
	public static void setupBeforeClass() throws Exception {
		// XMLUnit setup
		XMLUnit.setIgnoreWhitespace(true);
		XMLUnit.setIgnoreDiffBetweenTextAndCDATA(true);
	}
	public PutUpdateUserAccountSfdcRequestMapperTest() throws ScriptException, SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/put-update-user-account-info-sfdc-provider-request-mapper.groovy");
	}
	
	@Test
	public void testRequestMapper() throws Exception {
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(getClass().getResourceAsStream(
						"/in/put-update-user-account-info-request.json")))
						.flowVar("myToyotaId", "myToyotaId")
				.run().toString();
		System.out.println(result);
		String xml = groovy.xml.XmlUtil.serialize(result);
		// expected
		String expectedResult = IOUtils.toString(getClass().getResourceAsStream(
				"/out/put-update-user-account-info-sfdc-request.xml"));
		Diff diff = new Diff(expectedResult, xml);
		assertTrue(diff.similar());
	}
	
}
