package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

import javax.script.*;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

public class PutUpdateUserAccountMyKitRequestMapperTest extends AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;
	
	public PutUpdateUserAccountMyKitRequestMapperTest() throws ScriptException, SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/put-update-user-account-info-mykit-provider-request-mapper.groovy");
	}
	
	@Test
	public void testRequestMapper() throws Exception {
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(getClass().getResourceAsStream(
						"/in/put-update-user-account-info-request.json")))
				.run().toString();
		System.out.println(result);
		
		// expected
		String expectedResult = IOUtils.toString(getClass().getResourceAsStream(
				"/out/put-update-user-account-info-mykit-request.json"));
		JSONAssert.assertEquals(expectedResult, result, true);
	}
	
}
