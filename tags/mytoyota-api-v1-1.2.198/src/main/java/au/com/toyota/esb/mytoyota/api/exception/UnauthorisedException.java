package au.com.toyota.esb.mytoyota.api.exception;

import org.mule.module.apikit.exception.MuleRestException;

@SuppressWarnings("serial")
public class UnauthorisedException extends MuleRestException{
	public UnauthorisedException(String message) {
		super(message);
	}
}
