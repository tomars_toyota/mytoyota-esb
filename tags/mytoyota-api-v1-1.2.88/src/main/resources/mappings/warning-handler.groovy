import org.mule.api.transport.PropertyScope
import static groovy.json.JsonOutput.*
import org.mule.module.apikit.exception.*
import au.com.toyota.esb.mytoyota.api.exception.UnauthorisedException

status = message.getProperty("http.status", PropertyScope.INBOUND)

/*
 * this scripts adds a "warning" field to the mainPayload sessionVars, which is the payload that is going
 * to be returned to the consumer, in case of any error status code from called backend API.
 * It also sets the "abortProcessing" flag to stop the flow from making any further calls to the backend 
 */

if(status in [200, 201])
	return payload
else {
	flowVars['abortProcessing'] = true
	reason = message.getProperty("http.reason", PropertyScope.INBOUND)
	if(!reason)
		reason = ""
	// payloadMap = new groovy.json.JsonSlurper().parseText(sessionVars['mainPayload'])
	//payloadMap.warning = warningMsg + ", failed with error: " + status + (reason ? ": "+reason : "")
	sessionVars['warnings'] << warningMsg + ", failed with error: " + status + (reason ? ": "+reason : "")
	sessionVars['mainPayload'] = prettyPrint(toJson(payloadMap)) 
}