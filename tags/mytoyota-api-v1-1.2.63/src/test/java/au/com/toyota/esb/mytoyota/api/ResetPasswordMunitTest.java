package au.com.toyota.esb.mytoyota.api;

import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.Properties;

import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.util.IOUtils;
import org.mule.util.StringUtils;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;


/*
 * Munit test to unit test the functionality for user reset password
 * @author: jgarner
 */
public class ResetPasswordMunitTest extends FunctionalMunitSuite {

	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"post-user-reset-password.xml",
				"post-user-reset-password-technical-adapter.xml",
				"get-user-account-info-technical-adapter.xml",
				"post-passwords-encrypt-myKit-technical-adapter.xml",
				"sfdc-update-user.xml",
				"create-salesforce-sessionid-soap-header.xml",
				"sfdc-technical-adapter.xml",
				"config.xml"
		}, " ");
	}
	
	@Test
	public void testSuccessfulPasswordResetTokenNotPassed() throws Exception {
		
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
			attribute("name").ofNamespace("doc").withValue("IM API Reset Password"))
			.thenApply(new MockResponseTransformer() {
				@Override
				public MuleMessage transform(MuleMessage originalMessage) {
					MuleMessage newMessage = null;
					
					try {
						newMessage = originalMessage.createInboundMessage();
						newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
						String responsePayload = IOUtils.toString(getClass().getResourceAsStream("/in/post-user-reset-password-passwordresettoken-im-response.json"));
						newMessage.setPayload(responsePayload);
					} catch (Exception e) {
						// catch exception statements
					}
					return newMessage;
				}
			});
		
		String requestPayload = IOUtils.toString(getClass().getResourceAsStream("/in/post-user-reset-password-passwordresettoken-not-passed-request.json"));
		
		MuleEvent event = testEvent(requestPayload);
		
		// Invoke the flow
		@SuppressWarnings("unused")
		MuleEvent output = runFlow("post:/users/me/resetPassword:myToyota-config", event);
		
		verifyLoggerCall("esb.mytoyota-api-v1.post-user-reset-password.request", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.post-user-reset-password.response", 1);
		
		verifyLoggerCall("esb.mytoyota-api-v1.post-user-reset-password-technical-adapter.request", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.post-user-reset-password-technical-adapter.response", 1);
			
	}
	
	@Test
	public void testSuccessfulPasswordResetTokenPassed() throws Exception {
		
		// Mock Identity Management API Password Reset successful
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
			attribute("name").ofNamespace("doc").withValue("IM API Reset Password"))
			.thenApply(new MockResponseTransformer() {
				@Override
				public MuleMessage transform(MuleMessage originalMessage) {
					MuleMessage newMessage = null;
					
					try {
						newMessage = originalMessage.createInboundMessage();
						newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
						String responsePayload = IOUtils.toString(getClass().getResourceAsStream("/in/post-user-reset-password-passwordresettoken-im-response.json"));
						newMessage.setPayload(responsePayload);
					} catch (Exception e) {
						// catch exception statements
					}
					return newMessage;
				}
			});
		// Mock Identity Management API Get User Info
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
				attribute("name").ofNamespace("doc").withValue("IM API Get User"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						
						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							String responsePayload = IOUtils.toString(getClass().getResourceAsStream("/in/post-user-reset-password-passwordresettoken-im-get-user-profile-response.json"));
							newMessage.setPayload(responsePayload);
						} catch (Exception e) {
							// catch exception statements
						}
						return newMessage;
					}
				});
		// Mock encrypt password myKit API call response
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
				attribute("name").ofNamespace("doc").withValue("MyKit API: POST /passwords/encrypt"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						
						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							String responsePayload = "{\"encryptedPassword\": \"c7576b05dd8e64190e799d692be3d843\"}";
							
							newMessage.setPayload(responsePayload);
						} catch (Exception e) {
							// catch exception statements
						}
						return newMessage;
					}
				});		
		//Mock sfdc login response
		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Partner Operation login"))
				.thenReturn(muleMessageWithPayload(
						IOUtils.toString(getClass().getResourceAsStream("/in/sfdc-login-response.xml"))));

		//Mock sfdc updateOwner response
		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(
						attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Operation updateOwner"))
				.thenReturn(muleMessageWithPayload(""));
		
		String requestPayload = IOUtils.toString(getClass().getResourceAsStream("/in/post-user-reset-password-passwordresettoken-passed-request.json"));
		
		MuleEvent event = testEvent(requestPayload);
		
		// Invoke the flow
		@SuppressWarnings("unused")
		MuleEvent output = runFlow("post:/users/me/resetPassword:myToyota-config", event);
		
		// Verify correct path in Mule flow
		verifyCallOfMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("IM API Get User")).times(1);
		
		System.out.println("**** Result: "+output.getMessage().getPayload());
		
		// Verify payload output
		// expected
		String expectedResult = IOUtils.toString(getClass().getResourceAsStream(
				"/out/post-user-reset-password-passwordresettoken-passed-response.json"));
				
		verifyLoggerCall("esb.mytoyota-api-v1.post-user-reset-password.request", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.post-user-reset-password.response", 1);
		
		verifyLoggerCall("esb.mytoyota-api-v1.post-user-reset-password-technical-adapter.request", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.post-user-reset-password-technical-adapter.response", 1);
		
		verifyLoggerCall("esb.mytoyota-api-v1.get-user-account-info-im-technical-adapter.request", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.get-user-account-info-im-technical-adapter.response", 1);
		
		verifyLoggerCall("esb.mytoyota-api-v1.post-passwords-encrypt.mykit.request", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.post-passwords-encrypt.mykit.response", 1);
		
		verifyLoggerCall("esb.mytoyota-api-v1.get-salesforce-sessionid-subflow.request", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.get-salesforce-sessionid-subflow.response", 1);
		
		verifyLoggerCall("esb.mytoyota-api-v1.sfdc-update-user.request", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.sfdc-update-user.response", 1);
		
		// JSONAssert.assertEquals(output.getMessage().getPayloadAsString(), expectedResult,true);	
	}
	
	@Test
	public void testFailuresStatusCodes() throws Exception {

		int[] statusCodes = new int[] { 404, 405, 415, 406, 401, 500 };
		
		String requestPayload = IOUtils.toString(getClass().getResourceAsStream("/in/post-user-reset-password-passwordresettoken-passed-request.json"));
		
		MuleEvent event = testEvent(requestPayload);
		
		for (int i = 0; i < statusCodes.length; i++) {
			new StatusCodesTestingHelper().runFailureTest(statusCodes[i], "IM API Reset Password",
					"post:/users/me/resetPassword:user-reset-password-technical-adapter", event);
		}
	}
	
	private void verifyLoggerCall (String category, int expectedNumberOfCalls) {
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue(category)).times(expectedNumberOfCalls);
	}
}
