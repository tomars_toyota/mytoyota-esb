import groovy.xml.MarkupBuilder
import groovy.json.JsonSlurper
import java.text.SimpleDateFormat

def writer = new StringWriter()
def xmlMarkup = new MarkupBuilder(writer)

if(payload instanceof org.mule.transport.NullPayload)
	payload = null

//def rootEle =
xmlMarkup.mkp.xmlDeclaration(version: "1.0", encoding: "UTF-8")
xmlMarkup.'urn:create' (
	'xmlns:urn' : "urn:enterprise.soap.sforce.com",
	'xmlns:urn1' : "urn:sobject.enterprise.soap.sforce.com",
	'xmlns:xsi' : "http://www.w3.org/2001/XMLSchema-instance"
	) {
		'urn:sObjects' ('xsi:type' : "urn1:Attachment") {
			'ParentId' (flowVars['sfdcCaseId'])
			'Body' (sessionVars['requestPayload']['registrationDetails']['encodedImage'])
			'Name' (sessionVars['requestPayload']['registrationDetails']['documentFilename'])
		}	
	}
//println '__________ payload to salesforce: ' + writer.toString()
return writer.toString()
