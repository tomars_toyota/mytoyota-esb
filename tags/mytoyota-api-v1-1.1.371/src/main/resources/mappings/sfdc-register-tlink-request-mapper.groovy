import groovy.xml.MarkupBuilder
import groovy.json.JsonSlurper
import java.text.SimpleDateFormat
import au.com.toyota.esb.mytoyota.api.util.GroovyHelper

tlinkRequestPayload = sessionVars['requestPayload']

if(!tlinkRequestPayload)
	throw new org.mule.module.apikit.exception.BadRequestException("Invalid request")

def writer = new StringWriter()
def xmlMarkup = new MarkupBuilder(writer)


def tlinkUserDetails = new JsonSlurper().parseText(tlinkRequestPayload)
def userDetailsMap = new JsonSlurper().parseText(sessionVars['userDetails'])

//remove nulls
tlinkUserDetails = GroovyHelper.removeNulls(tlinkUserDetails)
userDetailsMap = GroovyHelper.removeNulls(userDetailsMap)

//validate userDetailsMap
if(!(userDetailsMap.containsKey('firstName') &&
		userDetailsMap.containsKey('lastName') &&
		userDetailsMap.containsKey('email')))
	throw new Exception('Invalid user details found')

def postalAddress = tlinkUserDetails?.address?.postal
def residentialAddress = tlinkUserDetails?.address?.residential

xmlMarkup.mkp.xmlDeclaration(version: "1.0", encoding: "UTF-8")
xmlMarkup.'sp:activateDeactivateCMUser' (
	'xmlns:sp' : "http://soap.sforce.com/schemas/class/SP_UserRegWSTransactionStructure"
	) {
		'sp:request'{
			if(postalAddress) {
				'sp:Address' {
					if(postalAddress.suburb != null)
						'sp:cityorsuburb' (postalAddress.suburb)
					if(postalAddress.postcode != null)
						'sp:postcode' (postalAddress.postcode)
					if(postalAddress.state != null)
						'sp:state' (postalAddress.state)
					if(postalAddress.streetAddress != null)
						'sp:street' (postalAddress.streetAddress)
				}
			}
			if(residentialAddress) {
				'sp:Address2' {
					if(residentialAddress.suburb != null)
						'sp:cityorsuburb' (residentialAddress.suburb)
					if(residentialAddress.postcode != null)
						'sp:postcode' (residentialAddress.postcode)
					if(residentialAddress.state != null)
						'sp:state' (residentialAddress.state)
					if(residentialAddress.streetAddress != null)
						'sp:street' (residentialAddress.streetAddress)
				}
			}
			if(sessionVars['myToyotaId'])
				'sp:myToyotaId' (sessionVars['myToyotaId'])
			'sp:ownerDetails'{
				'sp:email' (userDetailsMap.email)
				'sp:firstName' (userDetailsMap.firstName)
				'sp:lastName' (userDetailsMap.lastName)
				'sp:personmobilephone' (tlinkUserDetails.mobile)
				'sp:salutation' (userDetailsMap.title)
			}
			if(tlinkUserDetails?.vin != null)
				'sp:VIN' (tlinkUserDetails.vin)
		}	
	}
//println '__________ payload to salesforce: ' + writer.toString()
return writer.toString()



// <sp:request>
//    <sp:address>
//       <sp:cityorsuburb>?</sp:cityorsuburb>
//       <sp:postcode>?</sp:postcode>
//       <sp:state>?</sp:state>
//       <sp:street>?</sp:street>
//    </sp:address>
//    <sp:address2>
//       <sp:cityorsuburb>?</sp:cityorsuburb>
//       <sp:postcode>?</sp:postcode>
//       <sp:state>?</sp:state>
//       <sp:street>?</sp:street>
//    </sp:address2>
//    <sp:myToyotaId>?</sp:myToyotaId>
//    <sp:ownerDetails>
//       <sp:email>?</sp:email>
//       <sp:firstName>?</sp:firstName>
//       <sp:lastName>?</sp:lastName>
//       <sp:personmobilephone>?</sp:personmobilephone>
//       <sp:salutation>?</sp:salutation>
//    </sp:ownerDetails>
//    <sp:VIN>?</sp:VIN>
// </sp:request>