package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;

import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.custommonkey.xmlunit.XMLTestCase;
import org.junit.Test;
import org.xml.sax.SAXException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

public class GetSfdcUserConnectedMobilityRequestMapper extends XMLTestCase {

	private ScriptRunner scriptRunner;

	public GetSfdcUserConnectedMobilityRequestMapper() throws ScriptException, SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/sfdc-get-user-connected-mobility-status-request-mapper.groovy");
	}

	@Test
	public void testRequestMapping() throws Exception {

		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload("")
				.flowVar("myToyotaId", "MYT-000003a3")
				.flowVar("vin", "JTNKU3JE60J021675")
				.run().toString();

		System.out.println(result);

		// expected
		String expectedResult = IOUtils.toString(getClass().getResourceAsStream("/out/checkCMUserAndVehicle-sfdc-request.xml"));
		assertXMLEqual(expectedResult, result);
	}
}
