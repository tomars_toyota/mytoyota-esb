package au.com.toyota.esb.mytoyota.api;

import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.HashMap;

import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.util.IOUtils;
import org.mule.util.StringUtils;
import org.skyscreamer.jsonassert.JSONAssert;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;

public class GetUsersMeAccountSummaryMunitTest extends FunctionalMunitSuite {

	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"get-users-me-account-summary.xml",
				"get-users-me-account-summary-technical-adapter.xml",
				"get-user-account-info-technical-adapter.xml",
				"get-users-me-vehicles-technical-adapter.xml",
				"get-vehicle-details-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulGetUsersAccountSummary() throws Exception {
		
		String requestPath = "/api/myToyota/v1/users/me/accountSummary";

		//stub get:/users/me myKIT response		
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("My User Details from myKIT"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							newMessage = originalMessage.createInboundMessage();

							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);

							newMessage.setPayload(
									getClass().getResourceAsStream("/in/get-user-account-info-mykit-response-account-summary.json"));
						} catch (Exception e) {
							// catch exception statements
						}

						return newMessage;
					}
				});
		
		//stub get:/users/me/vehicles myKIT response
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(
						attribute("name").ofNamespace("doc").withValue("My Toyota App API GET Retrieve Users Vehicles"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							newMessage = originalMessage.createInboundMessage();

							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							newMessage.setInvocationProperty("userAccount", "{"
																  +"\"firstName\": \"John\","
																  +"\"lastName\": \"Smith\""
																  +"}");
							newMessage.setPayload(
									getClass().getResourceAsStream("/in/get-users-me-vehicles-mykit-response-account-summary.json"));
						} catch (Exception e) {
							// catch exception statements
						}

						return newMessage;
					}
				});
		
		//stub get:/vehicles/{vin} vwhicle API response		
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Vehicle API GET vehicle by vin"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							newMessage = originalMessage.createInboundMessage();

							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							newMessage.setInvocationProperty("userAccount", "{"
																  +"\"firstName\": \"John\","
																  +"\"lastName\": \"Smith\""
																  +"}");
							newMessage.setInvocationProperty("vin","JTNKU3JE60J021676");
							newMessage.setPayload(
									getClass().getResourceAsStream("/in/get-vehicle-details-response-account-summary.json"));
						} catch (Exception e) {
							// catch exception statements
						}

						return newMessage;
					}
				});
		
		
		
		MuleEvent event = testEvent(requestPath);
		MuleMessage message = muleMessageWithPayload(null);
		message.setProperty("http.query.params", new HashMap<>(), PropertyScope.INBOUND);
		
		event.setMessage(message);	
		
		// Invoke the flow
		MuleEvent output = runFlow("get:/users/me/accountSummary:myToyota-config", event);
		
		System.out.println(output.getMessage().getPayload());
		
		// expected
		String expectedResult = "{" 
									+ "\"firstName\": \"John\", " 
									+ "\"lastName\": \"Smith\", "
									+ "\"vin\": \"JTNKU3JE60J021676\", "
									+ "\"vehicleDescription\": \"Camry L4 Altise 2.4L Petrol Automatic Sedan\" " 
									+ "}";
		
		JSONAssert.assertEquals(output.getMessage().getPayloadAsString(), expectedResult, true);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.get-users-me-account-summary.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.get-users-me-account-summary.response"))
				.times(1);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category")
						.withValue("esb.mytoyota-api-v1.get-users-me-account-summary.technical-adapter.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category")
						.withValue("esb.mytoyota-api-v1.get-users-me-account-summary.technical-adapter.response"))
				.times(1);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-vehicle-details-technical-adapter.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-vehicle-details-technical-adapter.response"))
				.times(1);		
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category")
						.withValue("esb.mytoyota-api-v1.get-users-me-account-summary.technical-adapter.no-active-vehicle"))
				.times(0);
	}
	
	@Test
	public void testSuccessfulGetUsersNoActiveVehiclesAccountSummary() throws Exception {
		
		String requestPath = "/api/myToyota/v1/users/me/accountSummary";

		//stub get:/users/me myKIT response		
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("My User Details from myKIT"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							newMessage = originalMessage.createInboundMessage();

							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);

							newMessage.setPayload(
									getClass().getResourceAsStream("/in/get-user-account-info-mykit-response-account-summary.json"));
						} catch (Exception e) {
							// catch exception statements
						}

						return newMessage;
					}
				});
		
		//stub get:/users/me/vehicles myKIT response
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(
						attribute("name").ofNamespace("doc").withValue("My Toyota App API GET Retrieve Users Vehicles"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							newMessage = originalMessage.createInboundMessage();

							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							newMessage.setInvocationProperty("userAccount", "{"
																  +"\"firstName\": \"John\","
																  +"\"lastName\": \"Smith\""
																  +"}");
							newMessage.setPayload(
									getClass().getResourceAsStream("/in/get-users-me-vehicles-mykit-response-account-summary-no-active-vehicles.json"));
						} catch (Exception e) {
							// catch exception statements
						}

						return newMessage;
					}
				});
		
		MuleEvent event = testEvent(requestPath);
		MuleMessage message = muleMessageWithPayload(null);
		message.setProperty("http.query.params", new HashMap<>(), PropertyScope.INBOUND);
		
		event.setMessage(message);	
		
		// Invoke the flow
		MuleEvent output = runFlow("get:/users/me/accountSummary:myToyota-config", event);
		
		System.out.println(output.getMessage().getPayload());
		
		// expected
		String expectedResult = "{" 
									+ "\"firstName\": \"John\", " 
									+ "\"lastName\": \"Smith\", "
									+ "}";
		
		JSONAssert.assertEquals(output.getMessage().getPayloadAsString(), expectedResult, true);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.get-users-me-account-summary.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.get-users-me-account-summary.response"))
				.times(1);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category")
						.withValue("esb.mytoyota-api-v1.get-users-me-account-summary.technical-adapter.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category")
						.withValue("esb.mytoyota-api-v1.get-users-me-account-summary.technical-adapter.response"))
				.times(1);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-vehicle-details-technical-adapter.request"))
				.times(0);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-vehicle-details-technical-adapter.response"))
				.times(0);		
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category")
						.withValue("esb.mytoyota-api-v1.get-users-me-account-summary.technical-adapter.no-active-vehicle"))
				.times(1);
	}
}
