import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper


//populate flowVars['myToyotaId'] to staisfy update myKit flow
flowVars['myToyotaId'] = sessionVars['myToyotaId']

order = new JsonSlurper().parseText(sessionVars['orderResponse'])

myKitRequest = [:]
myKitRequest.title = order.title
myKitRequest.mobile = order.mobile

//myKitRequest.landline = 								//yet to be added?

address = order.address?.postal

postalAddress = [:]
postalAddress.streetAddress = address?.streetAddress
postalAddress.suburb = address?.suburb
postalAddress.state = address?.state
postalAddress.postcode = address?.postcode
// If an address (or part of address) is received without a totalCheckId, then set to 'other'
// Ensures myKIT will store the formatted address correctly
if (address?.totalCheckId == null) {
    postalAddress.totalCheckId = "other"
}

myKitRequest.address = ['postal': postalAddress]


return prettyPrint(toJson(myKitRequest))