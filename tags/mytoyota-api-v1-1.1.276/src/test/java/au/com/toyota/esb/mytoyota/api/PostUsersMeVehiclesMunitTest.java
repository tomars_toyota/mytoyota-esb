package au.com.toyota.esb.mytoyota.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mule.munit.common.mocking.Attribute.attribute;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.module.apikit.exception.BadRequestException;
import org.mule.munit.runner.functional.FunctionalMunitSuite;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;


//@Ignore
public class PostUsersMeVehiclesMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"post-users-me-vehicles.xml",
				"post-users-me-vehicles-technical-adapter.xml",
				"get-vehicle-details.xml",
				"get-vehicle-details-technical-adapter.xml",
				"sfdc-add-vehicle.xml",
				"sfdc-create-case.xml",
				"sfdc-add-attachment.xml",
				"create-salesforce-sessionid-soap-header.xml",
				"sfdc-technical-adapter.xml",
				"config.xml"
		}, " ");
	}
	
	//@Test
	public void testIncorrectParametersMissingBatchTest() throws Exception {
		
		String payload = IOUtils.toString(getClass().getResourceAsStream("/in/post-users-me-vehicles-missing-batch-number-api-request.json"));
		MuleEvent event = testEvent(payload);

		try {
			runFlow("post:/users/me/vehicles:myToyota-config", event);
		} catch (Exception e) {
			assertEquals("Not org.mule.module.apikit.exception.BadRequestException was thrown!",
					e.getCause().getCause() instanceof BadRequestException, true);
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.post-add-user-vehicle.request"))
				.times(1);
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.post-add-user-vehicle.response"))
				.times(0);
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.post-add-user-vehicle-technical-adapter.request"))
				.times(0);
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.post-add-user-vehicle-technical-adapter.response"))
				.times(0);
			return;
		}
		assertTrue(false);
	}
	
	//@Test
	public void testIncorrectParametersMissingRegistrationDetailsTest() throws Exception {
		
		String payload = IOUtils.toString(getClass().getResourceAsStream("/in/post-users-me-vehicles-missing-registration-details-api-request.json"));
		MuleEvent event = testEvent(payload);

		try {
			runFlow("post:/users/me/vehicles:myToyota-config", event);
		} catch (Exception e) {
			assertEquals("Not org.mule.module.apikit.exception.BadRequestException was thrown!",
					e.getCause().getCause() instanceof BadRequestException, true);
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.post-add-user-vehicle.request"))
				.times(1);
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.post-add-user-vehicle.response"))
				.times(0);
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.post-add-user-vehicle-technical-adapter.request"))
				.times(0);
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.post-add-user-vehicle-technical-adapter.response"))
				.times(0);
			return;
		}
		assertTrue(false);
	}

	//@Test
	public void testVinBatchNumberSuccessfulRequest() throws Exception {

		// Mock Get Vehicle Details
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
				attribute("name").ofNamespace("doc").withValue("Vehicle API GET vehicle by vin"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						
						try {
							newMessage = originalMessage.createInboundMessage();
							
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							
							newMessage.setPayload(IOUtils.toString(getClass().getResourceAsStream("/in/get-vehicle-details-response.json")));
						} catch (Exception e) {
							// catch exception statements
						}
						
						return newMessage;
					}
				});

		// Mock myKIT
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
				attribute("name").ofNamespace("doc").withValue("My Toyota App API POST Add Vehicle"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						
						try {
							newMessage = originalMessage.createInboundMessage();
							
							newMessage.setProperty("http.status", 201, PropertyScope.INBOUND);
							
							newMessage.setPayload(IOUtils.toInputStream("", "UTF-8"));
						} catch (Exception e) {
							// catch exception statements
						}
						
						return newMessage;
					}
				});
		
		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Partner Operation login"))
				.thenReturn(muleMessageWithPayload(
						IOUtils.toString(getClass().getResourceAsStream("/in/sfdc-login-response.xml"))));
		
		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Operation addAVehicle"))
				.thenReturn(muleMessageWithPayload(""));

		
		String payload = IOUtils.toString(getClass().getResourceAsStream("/in/post-users-me-vehicles-api-request.json"));
		MuleEvent event = testEvent(payload);

		runFlow("post:/users/me/vehicles:myToyota-config", event);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.post-add-user-vehicle.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.post-add-user-vehicle.response"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.post-add-user-vehicle-technical-adapter.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.post-add-user-vehicle-technical-adapter.response"))
				.times(1);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-vehicle-details-technical-adapter.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-vehicle-details-technical-adapter.response"))
				.times(1);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.sfdc-add-vehicle.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.sfdc-add-vehicle.response"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.get-salesforce-sessionid-subflow.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-salesforce-sessionid-subflow.response"))
				.times(1);
		
		System.out.println("**** Result: "+event.getMessage().getPayload());
	}
	
	@Test
	public void testVinRegistrationDetailsSuccessfulRequest() throws Exception {

		whenMessageProcessor("request").ofNamespace("http").withAttributes(
				attribute("name").ofNamespace("doc").withValue("My Toyota App API POST Add Vehicle"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						
						try {
							newMessage = originalMessage.createInboundMessage();
							
							newMessage.setProperty("http.status", 201, PropertyScope.INBOUND);
							
							newMessage.setPayload(IOUtils.toInputStream("", "UTF-8"));
						} catch (Exception e) {
							// catch exception statements
						}
						
						return newMessage;
					}
				});
		
		// Mock Login
		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Enterprise Operation login"))
				.thenReturn(muleMessageWithPayload(
						IOUtils.toString(getClass().getResourceAsStream("/in/sfdc-enterprise-wsdl-login-response.xml"))));
		// Mock create case
		whenMessageProcessor("consumer").ofNamespace("ws")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Operation create"))
			.thenReturn(muleMessageWithPayload(
					IOUtils.toString(getClass().getResourceAsStream("/in/sfdc-create-case-response.xml"))));

		// Mock create attachment for case
		whenMessageProcessor("consumer").ofNamespace("ws")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Operation create (for attachment)"))
			.thenReturn(muleMessageWithPayload(
					IOUtils.toString(getClass().getResourceAsStream("/in/sfdc-create-attachment-response.xml"))));

		
		String payload = IOUtils.toString(getClass().getResourceAsStream("/in/post-users-me-vehicles-api-registration-details-request.json"));
		MuleEvent event = testEvent(payload);

		runFlow("post:/users/me/vehicles:myToyota-config", event);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.post-add-user-vehicle.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.post-add-user-vehicle.response"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.post-add-user-vehicle-technical-adapter.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.post-add-user-vehicle-technical-adapter.response"))
				.times(1);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.sfdc-create-case.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.sfdc-create-case.response"))
				.times(1);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.sfdc-add-attachment.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.sfdc-add-attachment.response"))
				.times(1);

		// Two calls to Salesforce means session ID flows invoked twice
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-salesforce-sessionid-subflow.request"))
				.times(2);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-salesforce-sessionid-subflow.response"))
				.times(2);
		
		System.out.println("**** Result: "+event.getMessage().getPayload());
	}
	
//	@Test
//	public void testFailuresStatusCodes() throws Exception {
//
//		int[] statusCodes = new int[] { 404, 405, 415, 406, 401, 500 };
//
//		String payload = IOUtils.toString(getClass().getResourceAsStream("/in/post-users-me-vehicles-api-request.json"));
//		MuleEvent event = testEvent(payload);		
//
//		for (int i = 0; i < statusCodes.length; i++) {
//			new StatusCodesTestingHelper().runFailureTest(statusCodes[i], "My Toyota App API POST Add Vehicle",
//					"post:/users/me/vehicles:myToyota-config", event);
//		}
//	}
}
