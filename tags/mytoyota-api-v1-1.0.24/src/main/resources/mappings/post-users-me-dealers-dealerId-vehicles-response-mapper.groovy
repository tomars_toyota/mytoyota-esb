import groovy.json.JsonSlurper
import groovy.json.JsonOutput.*

def tuneResponse = new JsonSlurper().parseText(payload)

def response = [:]
if(tuneResponse?.response) {
	response.dmsCustomerId = tuneResponse?.response[0]?.responseMessage
	response.dmsVehicleId = tuneResponse?.response[1]?.responseMessage
	response = response.findAll {it.value != null}
}
if(!response)
	throw new Exception("Failed to create vehicle user association")
else
	return response