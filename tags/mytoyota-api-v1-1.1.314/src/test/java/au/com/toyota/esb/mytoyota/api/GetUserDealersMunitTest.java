package au.com.toyota.esb.mytoyota.api;

import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.junit.Assert.assertEquals;
import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.module.http.internal.request.ResponseValidatorException;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.util.IOUtils;
import org.mule.util.StringUtils;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;


/*
 * Munit test to unit test the functionality to get dealers of a user
 * @author: swapnil
 */
public class GetUserDealersMunitTest extends FunctionalMunitSuite {
	
	private static Properties adapterProperties = new Properties();

	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"get-user-dealers.xml",
				"get-user-dealers-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulGetUserDealers() throws Exception {
		
		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));

		final String responsePayload = IOUtils.toString(getClass().getResourceAsStream("/in/get-users-dealers-mytoyotaapi-response.json"));

		whenMessageProcessor("request").ofNamespace("http").withAttributes(
			attribute("name").ofNamespace("doc").withValue("My Toyota App API Get User Dealers"))
			.thenApply(new MockResponseTransformer() {
				@Override
				public MuleMessage transform(MuleMessage originalMessage) {
					MuleMessage newMessage = null;
					
					try {
						newMessage = originalMessage.createInboundMessage();
						
						newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
						newMessage.setProperty("Content-Type", "application/json;charset=UTF-8", PropertyScope.INBOUND);
						
						newMessage.setPayload(IOUtils.toInputStream(responsePayload, "UTF-8"));
					} catch (Exception e) {
						// catch exception statements
					}
					
					return newMessage;
				}
			});
		
		MuleEvent event = testEvent("");
		Map<String, Object> props = new HashMap<String,Object>();
		//props.put("showDeletedOnly", true);
        
		
		MuleMessage msg = muleMessageWithPayload(null);
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
		event.setMessage(msg);	
        
		// Invoke the flow
		MuleEvent output = runFlow("get:/users/me/dealers:myToyota-config", event);
		

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Get User Dealers App Request")).times(1);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Get User Dealers App Response")).times(1);	

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Get User Dealers App Request")).times(1);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Get User Dealers App Response")).times(1);	

		assertJsonEquals(IOUtils.toString(getClass().getResourceAsStream(
				"/out/get-users-dealers-api-response.json")), output
				.getMessage().getPayloadAsString());

	}
	
	@Test
	public void testSuccessfulGetUserDeletedDealers() throws Exception {
		
		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));

		final String responsePayload = IOUtils.toString(getClass().getResourceAsStream("/in/get-users-deleted-dealers-mytoyotaapi-response.json"));

		whenMessageProcessor("request").ofNamespace("http").withAttributes(
			attribute("name").ofNamespace("doc").withValue("My Toyota App API Get User Deleted Dealers"))
			.thenApply(new MockResponseTransformer() {
				@Override
				public MuleMessage transform(MuleMessage originalMessage) {
					MuleMessage newMessage = null;
					
					try {
						newMessage = originalMessage.createInboundMessage();
						
						newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
						newMessage.setProperty("Content-Type", "application/json;charset=UTF-8", PropertyScope.INBOUND);
						
						newMessage.setPayload(IOUtils.toInputStream(responsePayload, "UTF-8"));
					} catch (Exception e) {
						// catch exception statements
					}
					
					return newMessage;
				}
			});
		
		MuleEvent event = testEvent("");
		Map<String, Object> props = new HashMap<String,Object>();
		props.put("showDeletedOnly", true);
        
		
		MuleMessage msg = muleMessageWithPayload(null);
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
		event.setMessage(msg);	
        
		// Invoke the flow
		MuleEvent output = runFlow("get:/users/me/dealers:myToyota-config", event);
		

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Get User Dealers App Request")).times(1);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Get User Dealers App Response")).times(1);	

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Get User Dealers App Request")).times(1);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Get User Dealers App Response")).times(1);	

		assertJsonEquals(IOUtils.toString(getClass().getResourceAsStream(
				"/out/get-users-deleted-dealers-api-response.json")), output
				.getMessage().getPayloadAsString());

	}
	
	@Test
	public void testFailureGetUserDealers() throws Exception {
		
		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));

		MuleEvent event = testEvent("");
		Map<String, Object> props = new HashMap<String,Object>();
		MuleMessage msg = muleMessageWithPayload(null);
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
		event.setMessage(msg);	
        
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
			attribute("name").ofNamespace("doc").withValue("My Toyota App API Get User Dealers"))
			.thenThrow(new java.lang.Exception("Response code 400 mapped as failure."));
			
		try {

			// Invoke the flow
			runFlow("get:/users/me/dealers:myToyota-config", event);
			
			// validating exception thrown
			assertEquals(true, false);

		} catch (java.lang.Exception e) {

			
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Get User Dealers App Request")).times(1);	
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Get User Dealers App Response")).times(0);	

			verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Get User Dealers App Request")).times(1);	
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Get User Dealers App Response")).times(0);
			
		}

	}
	
	@Test
	public void testFailuresStatusCodes() throws Exception {
		
		int[] statusCodes = new int[] { 404, 405, 415, 406, 401, 500 };	
		
		MuleEvent event = testEvent("");
		Map<String, Object> props = new HashMap<String,Object>();
		MuleMessage msg = muleMessageWithPayload(null);
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
		event.setMessage(msg);
		
		for (int i = 0; i < statusCodes.length; i++) {
			new StatusCodesTestingHelper().runFailureTest(statusCodes[i], "My Toyota App API Get User Dealers",
					"get:/users/me/dealers:myToyota-config", event);
		}
	}
}
