import groovy.json.JsonSlurper
import org.mule.api.transport.PropertyScope

if (!payload) {
	throw new org.mule.module.apikit.exception.NotFoundException("Email not found in myToyota")
}	

def userAccount = new JsonSlurper().parseText(payload)

def responseMap = [:]

responseMap.firstName = userAccount?.firstName
responseMap.lastName = userAccount?.lastName
sessionVars['myToyotaId'] = userAccount?.myToyotaID

return responseMap