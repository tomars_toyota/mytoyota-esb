import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper

if(!payload)
	throw new org.mule.module.apikit.exception.BadRequestException('Invalid request')
	
def vehicleRequest = new JsonSlurper().parseText(payload)

// Build vehicle list
def vehicle = [:]
vehicle.batchNumber = vehicleRequest?.batchNumber
vehicle.compliancePlate = vehicleRequest?.compliancePlate
vehicle.engineNumber = vehicleRequest?.engineNumber
vehicle.vin = vehicleRequest?.vin
vehicle.materialNumber = vehicleRequest?.materialNumber
vehicle.deliveryDate = vehicleRequest?.deliveryDate
vehicle.vehicleDescription = vehicleRequest?.vehicleDescription
vehicle.katashikiCode = vehicleRequest?.katashikiCode
vehicle.transmission = vehicleRequest?.transmission
vehicle.sellingDealerCode = vehicleRequest?.sellingDealerCode
vehicle.sellingDealerName = vehicleRequest?.sellingDealerName
vehicle.registrationNumber = vehicleRequest?.registrationNumber
vehicle.production = vehicleRequest?.production

if(vehicleRequest?.suffix) {
    vehicle.suffixCode = vehicleRequest.suffix.code
    vehicle.suffixDescription = vehicleRequest.suffix.description
}
if(vehicleRequest?.trim) {
    vehicle.trimCode = vehicleRequest.trim.code
    vehicle.trimDescription = vehicleRequest.trim.description
}
if(vehicleRequest?.paint) {
    vehicle.paintCode = vehicleRequest.paint.code
    vehicle.paintDescription = vehicleRequest.paint.description
}

vehicle.salesType = vehicleRequest?.salesType	

//stock images
imagesList = []
vehicleRequest?.images.each { Map image ->
	imageMap = [:]
	imageMap.code = image.size
	imageMap.url = image.link
	//remove nulls
	imageMap = imageMap.findAll {it.value != null }

	if(imageMap)
		imagesList << imageMap
}

if(imagesList)
	vehicle.images = imagesList


//vehicle user
vehicleUserMap = [:]
if(vehicleRequest?.vehicleUser) {
	vehicleUserMap.registrationNumber = vehicleRequest.vehicleUser.registrationNumber
	vehicleUserMap.state = vehicleRequest.vehicleUser.state

	//MYT-1101 default isVerified to true
	vehicleUserMap.isVerified = true
	
	//remove nulls
	vehicleUserMap = vehicleUserMap.findAll {it.value != null }
}

if(vehicleRequest.customImages)
	vehicleUserMap.vehicleImage = vehicleRequest.customImages[0].encodedImage


if(vehicleUserMap) {
	vehicle.vehicleUsers = []
	vehicle.vehicleUsers << vehicleUserMap
}

//remove nulls
vehicle = vehicle.findAll {it.value != null }

if(!vehicle)
	throw new org.mule.module.apikit.exception.BadRequestException('Invalid request')

return prettyPrint(toJson(vehicle))