package au.com.toyota.esb.mytoyota.api;

import static org.mule.munit.common.mocking.Attribute.attribute;

import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.util.IOUtils;
import org.mule.util.StringUtils;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;


public class PostUsersMeRewardsRewardIdEntriesMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"post-users-me-rewards-rewardId-entries.xml",
				"post-users-me-rewards-rewardId-entries-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulGetPreferredDealer() throws Exception {
		
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
			attribute("name").ofNamespace("doc").withValue("Post rewards entries to myKIT"))
			.thenApply(new MockResponseTransformer() {
				@Override
				public MuleMessage transform(MuleMessage originalMessage) {
					MuleMessage newMessage = null;
					
					try {
						newMessage = originalMessage.createInboundMessage();
						
						newMessage.setProperty("http.status", 201, PropertyScope.INBOUND);
						newMessage.setProperty("Content-Type", "application/json;charset=UTF-8", PropertyScope.INBOUND);
						
						newMessage.setPayload(IOUtils.toInputStream("", "UTF-8"));
					} catch (Exception e) {
						// catch exception statements
					}
					
					return newMessage;
				}
			});
		
		
		MuleEvent event = testEvent("{\"rewardType\":\"competition\",\"rewardId\":\"guid\",\"description\":\"\",\"entries\":[{\"questionId\":\"12345\",\"questionTitle\":\"Tell us what you think?\",\"answer\":\"Nothing\"},{\"questionId\":\"123456\",\"questionTitle\":\"What is your favourite myToyota features? (multiple choice question)\",\"answer\":\"Service Booking\"}]}");
		event.setFlowVariable("myToyotaId", "MYT-000001f6");
		
		// Invoke the flow
		runFlow("post:/users/me/rewards/{rewardId}/entries:myToyota-config", event);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("category")
				.withValue("esb.mytoyota-api-v1.post-users-me-rewards-rewardId-entries.request")).times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("category")
				.withValue("esb.mytoyota-api-v1.post-users-me-rewards-rewardId-entries.response")).times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.post-users-me-rewards-rewardId-entries-technical-adapter.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.post-users-me-rewards-rewardId-entries-technical-adapter.response"))
				.times(1);
	}
	
	@Test
	public void testFailuresStatusCodes() throws Exception {
		
		int[] statusCodes = new int[] { 404, 405, 415, 406, 401, 500 };	
		
		MuleEvent event = testEvent("");
		event.setFlowVariable("myToyotaId", "MYT-000001f6");
		
		for (int i = 0; i < statusCodes.length; i++) {
			new StatusCodesTestingHelper().runFailureTest(statusCodes[i],
					"Post rewards entries to myKIT",
					"post:/users/me/rewards/{rewardId}/entries:myToyota-config", event);
		}
	}
}
