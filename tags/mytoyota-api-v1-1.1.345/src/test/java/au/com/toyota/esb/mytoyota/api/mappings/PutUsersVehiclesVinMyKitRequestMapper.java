package au.com.toyota.esb.mytoyota.api.mappings;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import javax.script.ScriptException;

import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

public class PutUsersVehiclesVinMyKitRequestMapper extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;

	public PutUsersVehiclesVinMyKitRequestMapper() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/put-users-vehicles-vin-mykit-request-mapper.groovy");
	}

	@Test
	public void testSuccessfulRequestMapper() throws Exception {
		String payload = "{"
							+ "\"vehicleUser\": {"
								+ "\"registrationNumber\": \"AAA111\","
								+ "\"state\": \"VIC\""
							+ "}"
						+ "}";
		
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(payload)
				.flowVar("vin", "JTNKU3JE60J021675")
				.run().toString();

		System.out.println(result);
		//System.out.println("------------------------");
		
		// expected
		String expectedResult =  "{\"vin\": \"JTNKU3JE60J021675\","
										+ "\"vehicleUsers\": ["
										+ "	{"
											+ "\"registrationNumber\": \"AAA111\","
											+ "\"state\": \"VIC\""
										+ "}"
									+ "]"
								+ "}";
		//System.out.println(expectedResult);
		JSONAssert.assertEquals(expectedResult, result, true);
	}
	
	@Test
	public void testEmptyRequestMapper() throws Exception {
		try {
			ScriptRunBuilder.runner(scriptRunner).payload("").run().toString();

		} catch (Exception e) {
			if (e.getMessage().contains("Invalid request"))
				assertTrue(true);
			else
				assertTrue(false);
			return;
		}
		assertTrue(false);
	}
}
