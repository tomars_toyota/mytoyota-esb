package au.com.toyota.esb.mytoyota.api.mappings;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import javax.script.ScriptException;

import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

public class PostUsersVehiclesMyKitRequestMapperTest extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;

	public PostUsersVehiclesMyKitRequestMapperTest() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/post-users-vehicles-mykit-request-mapper.groovy");
	}

	//@Test
	public void testSuccessfulRequestMapper() throws Exception {
		String payload = "{\"vin\": \"JTNKU3JE60J021675\","
							+ "\"vehicleUser\": {"
								+ "\"registrationNumber\": \"AAA111\","
								+ "\"state\": \"VIC\""
							+ "}"
						+ "}";
		
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(payload)
				.run().toString();

		System.out.println(result);
		//System.out.println("------------------------");
		
		// expected
		String expectedResult =  "{\"vin\": \"JTNKU3JE60J021675\","
										+ "\"vehicleUsers\": ["
										+ "	{"
											+ "\"registrationNumber\": \"AAA111\","
											+ "\"state\": \"VIC\""
										+ "}"
									+ "]"
								+ "}";
		//System.out.println(expectedResult);
		JSONAssert.assertEquals(expectedResult, result, true);
	}
	
	//@Test
	public void testEmptyRequestMapper() throws Exception {
		try {
			ScriptRunBuilder.runner(scriptRunner).payload("{}").run().toString();

		} catch (Exception e) {
			if (e.getMessage().contains("Invalid request"))
				assertTrue(true);
			else
				assertTrue(false);
			return;
		}
		assertTrue(false);
	}
}
