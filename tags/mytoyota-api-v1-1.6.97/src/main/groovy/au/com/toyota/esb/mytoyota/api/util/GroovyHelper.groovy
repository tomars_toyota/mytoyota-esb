package au.com.toyota.esb.mytoyota.api.util

public class GroovyHelper {
	def static removeNulls(col) {
	    if (col instanceof List)
	        return col.collect { removeNulls(it) }.findAll { it != null && it != '' && it != [:] && it != []}
	    else if (col instanceof Map) 
	        return col.collectEntries { [(it.key): removeNulls(it.value) ] }.findAll { it.value != null && it.value != '' && it.value != [] && it.value != [:]}
	    else return col
	}

	def static removeNulls(col, ignoreBlanks) {
	    if(!ignoreBlanks)
	        if (col instanceof List)
	            return col.collect { removeNulls(it) }.findAll { it != null && it != '' && it != [:] && it != []}
	        else if (col instanceof Map) 
	            return col.collectEntries { [(it.key): removeNulls(it.value) ] }.findAll { it.value != null && it.value != '' && it.value != [] && it.value != [:]}
	        else return col
	    else
	        if (col instanceof List)
	            return col.collect { removeNulls(it, ignoreBlanks) }.findAll { it != null && it != [:] && it != []}
	        else if (col instanceof Map) 
	            return col.collectEntries { [(it.key): removeNulls(it.value, ignoreBlanks) ] }.findAll { it.value != null && it.value != [] && it.value != [:]}
	        else return col
	}
}