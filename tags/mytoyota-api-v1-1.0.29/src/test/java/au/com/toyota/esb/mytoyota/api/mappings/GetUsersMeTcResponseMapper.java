package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

import javax.script.*;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;
import static org.junit.Assert.assertTrue;

public class GetUsersMeTcResponseMapper extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;

	public GetUsersMeTcResponseMapper() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/get-users-me-tc-response-mapper.groovy");
	}

	 @Test
	public void testSuccessfulResponseMapper() throws Exception {
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(getClass().getResourceAsStream("/in/get-users-me-tc-im-response.json")))
				.run().toString();

		System.out.println(result);

		// expected
		String expectedResult = IOUtils.toString(getClass()
				.getResourceAsStream(
						"/out/get-users-me-tc-response.json"));
		JSONAssert.assertEquals(expectedResult, result, true);
	}
	
	 @Test
	public void testFailedResponseMapper() throws Exception {
		boolean isRightExcepion = false;
		
		try {
			ScriptRunBuilder
					.runner(scriptRunner)
					.payload(IOUtils.toString(getClass().getResourceAsStream("/in/get-users-me-tc-im-response-no-tc.json")))
					.run().toString();
		} catch (Exception e) {
			if(e.getMessage().indexOf("No available accepted terms and conditions version") > 0);
				isRightExcepion = true;
		}

		assertTrue(isRightExcepion);
	}
}
