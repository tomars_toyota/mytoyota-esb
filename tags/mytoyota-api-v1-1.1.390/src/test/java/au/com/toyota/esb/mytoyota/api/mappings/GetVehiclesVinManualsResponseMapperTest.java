package au.com.toyota.esb.mytoyota.api.mappings;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

public class GetVehiclesVinManualsResponseMapperTest extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;

	public GetVehiclesVinManualsResponseMapperTest() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/get-vehicles-vin-manuals-response-mapper.groovy");
	}

	@Test
	public void testSuccessfulFirstDateDateResponseMapper() throws Exception {
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(getClass().getResourceAsStream("/in/get-vehicles-vin-toyota-manuals-response.json")))
				.sessionVar("katashikiCode","GSV50R-JETDKQ")
				.sessionVar("production","2014-08")
				.sessionVar("vehicleDescription","Aurion AT-X 3.5L Petrol Automatic Sedan")
				.binding("basePath", "http://toyotamanuals.com.au/docs/")
				.run().toString();
						
		System.out.println(result);

		// expected
		String expectedResult = IOUtils.toString(getClass()
				.getResourceAsStream(
						"/out/get-vehicles-vin-manuals-response.json"));
		JSONAssert.assertEquals(expectedResult, result, true);
	}	
	
	@Test
	public void testEmptyToyotaManualsResponseMapper() throws Exception {
		try {
			String result = ScriptRunBuilder
					.runner(scriptRunner)
					.payload("[]")
					.sessionVar("katashikiCode","GSV50R-JETDKQ")
					.sessionVar("production","2014-08")
					.sessionVar("vehicleDescription","Aurion AT-X 3.5L Petrol Automatic Sedan")
					.binding("basePath", "http://toyotamanuals.com.au/docs/")
					.run().toString();
		} catch (Exception e) {
			System.out.println("**** Exception: "+e.getCause());
			assertTrue(e.getCause() instanceof javax.script.ScriptException);
			assertEquals("org.mule.module.apikit.exception.NotFoundException: No vehicle manuals found", e.getCause().getMessage());
		}
	}
}
