import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper
import org.apache.commons.lang.StringUtils 
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.mule.api.transport.PropertyScope
  
Logger logger = LoggerFactory.getLogger("esb.mytoyota-api-v1.get-vehicle-outstanding-recalls-campaignCode-content-mapping.script")


JsonSlurper js = new JsonSlurper()

//logger.info("payload: " + payload)


def response = [:]

status = message.getProperty("http.status", PropertyScope.INBOUND)

if (status instanceof String)
	status = new Integer(status)


if (status == 200) {
	def parsedPayload = js.parseText(payload)
	parsedPayload?.remove("isSuccessful")
	parsedPayload?.remove("message")
	response = parsedPayload
	
}
else {
	response.recallCategory = "INFO"
}

response = au.com.toyota.esb.mytoyota.api.util.GroovyHelper.removeNulls(response);



return prettyPrint(toJson(response))
