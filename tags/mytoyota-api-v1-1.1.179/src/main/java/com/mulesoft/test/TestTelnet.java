package com.mulesoft.test;

import java.io.DataInputStream;
import java.io.InputStream;
import java.net.Socket;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.lang.exception.ExceptionUtils;
import org.mule.api.MuleEventContext;
import org.mule.api.lifecycle.Callable;

public class TestTelnet
implements Callable {
    public Object onCall(MuleEventContext eventContext) throws Exception {
    	// String ipAddress =  "10.14.208.120";
    	String ipAddress1 = null; 
    	String ipAddress2 = null;
    	int port1 = 0;
    	int port2 = 0;
    	
    	if (eventContext.getMessage().getInvocationProperty("ipAddress1") != null ) {
    		ipAddress1 = eventContext.getMessage().getInvocationProperty("ipAddress1");
    		port1 = 443; // ssl port
    	}
    	if (eventContext.getMessage().getInvocationProperty("ipAddress2") != null ) {
    		ipAddress2 = eventContext.getMessage().getInvocationProperty("ipAddress2");
    		port2 = 53; // telnet port
    	}
    	
        String javaOutput = this.runJavaTelnet(ipAddress1, ipAddress2, port1, port2);
        
        eventContext.getMessage().setPayload((Object)(javaOutput));
        return eventContext.getMessage();
    }
    private String runJavaTelnet(String ipAddress1, String ipAddress2, int port1, int port2) {
        StringBuilder result = new StringBuilder("");  
        
        if (ipAddress1 != null) {
        	result.append("About to attempt Telnet connectivity to: "+ipAddress1 +":"+ port1 +"\n");
        	result.append( connect(ipAddress1, port1) );
        }
        if (ipAddress2 != null) {
        	result.append("About to attempt Telnet connectivity to: "+ipAddress2 +":"+ port2 +"\n");
        	result.append( connect(ipAddress2, port2) );
        }
        
        return result.toString();
    }
    
    private String connect(String ipAddress, int port) {
    	StringBuilder result = new StringBuilder("");
    	
    	DateFormat dateFormat = new SimpleDateFormat("yyyymmdd HH:mm:ss");
        Date startDate = new Date();
        Date endDate = null;
        
        try {
        	Socket s1 = new Socket(ipAddress,port);
            InputStream is = s1.getInputStream();
            DataInputStream dis = new DataInputStream(is);
            
            if(dis != null) {
            	endDate = new Date();
                result.append(endDate+": Connected to ip "+ipAddress+":"+port+"\n");
            } else {
            	endDate = new Date();
            	result.append(endDate+": Could not connect to ip "+ipAddress+":"+port+"\n");
            }
            dis.close();
            s1.close();
        }
        catch (Exception e) {
            result.append(ExceptionUtils.getStackTrace((Throwable)e));
        }
        return result.toString();
    }
}