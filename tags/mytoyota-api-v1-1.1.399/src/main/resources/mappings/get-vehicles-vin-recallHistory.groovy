import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper


JsonSlurper js = new JsonSlurper()

def recallsResponse = js.parseText(payload)
response = [:]

response.batchNumber = recallsResponse.batchNumber
response.compliancePlate = recallsResponse.compliancePlate
response.engineNumber = recallsResponse.engineNumber
response.ignitionKeyNumber = recallsResponse.ignitionKeyNumber
response.materialNumber = recallsResponse.materialNumber
response.production = recallsResponse.production
response.vin = recallsResponse.vin
recallsList = []

if(recallsResponse.recalls) {

	recallsResponse.recalls.groupBy({it.repairOrderNumber}).each{
	    recallItem = [:]
	    
	    recallItem['repairDate'] = it.value[0]['repairDate']
	    recallItem['odometerReadingKm'] = it.value[0]['odometerReadingKm']
	    recallItem['repairOrderNumber'] = it.key
	    recallItem['dealerCode'] = it.value[0]['dealerCode']
	    recallItem['dealerName'] = it.value[0]['dealerName']
	    
	    recallItem['repairOrderItems'] = it.value
	    recallItem['repairOrderItems'] = recallItem['repairOrderItems'].each{
	        it.remove('repairDate')
	        it.remove('odometerReadingKm')
	        it.remove('repairOrderNumber')
	        it.remove('dealerCode')
	        it.remove('dealerName')
	    }
	    recallsList.add(recallItem)
	}
}

response.recalls = recallsList



return prettyPrint(toJson(response))
