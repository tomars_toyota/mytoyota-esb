import static groovy.json.JsonOutput.*
					
msg = [:]

if(flowVars['isMissingId'] == true) {
	msg['message'] = 'Invalid request, missing user ID'
	flowVars['statusReason'] = 'Invalid request, missing user ID'
	flowVars['statusCode'] = "400"
} else {

	msg['message'] = 'Failed to get required user'
	
	errorMap = [:]
	
	if(flowVars['targetSystem'] == 'sfdc') {
		errorMap.domain = 'Salesforce'
		
		exceptionPayload = message.getExceptionPayload()
		
		errorMap.message = exceptionPayload.getMessage()
		msg['error'] = errorMap

		flowVars['statusReason'] = exceptionPayload.getMessage()
		
		if (exceptionPayload.getException().getFaultCode().toString().contains('Client'))
			flowVars['statusCode'] = "400"
		else
			flowVars['statusCode'] = "500"
	} else {
		flowVars['statusReason'] = 'Backend system error'
		flowVars['statusCode'] = "500"
	}
}
return toJson(msg)