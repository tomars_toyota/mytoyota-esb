package au.com.toyota.esb.mytoyota.api;

import static org.junit.Assert.assertEquals;
import static org.mule.munit.common.mocking.Attribute.attribute;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.mule.DefaultMuleMessage;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.cache.InvalidatableCachingStrategy;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;

import com.jayway.jsonpath.JsonPath;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;

//@Ignore
public class GetVinCheckMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"get-vehicle-vin-check.xml",
				"get-vehicle-vin-check-technical-adapter.xml",
				"get-vehicle-details-technical-adapter.xml",
				"config.xml"
		}, " ");
	}
	@Test
	public void testSuccessfulValidRequest() throws Exception {

		testSuccessfulRequestImpl(true, "/in/get-vehicle-details-response.json");
	}
	@Test
	public void testSuccessfulInvalidRequest() throws Exception {
		testSuccessfulRequestImpl(false, "/in/get-vehicles-vin-vehiclesApi-lexus-response.json");
	}
	
	
	
	
	private void testSuccessfulRequestImpl(boolean isValid, final String mockedResponeFile) throws Exception {

		// Invalidate Cache as this unit test relies on different stubbed response from cached endpoint
		InvalidatableCachingStrategy cache = muleContext.getRegistry().<InvalidatableCachingStrategy>get("Generic_Caching_Strategy_Low_Currency");
		cache.invalidate();
		
		final String vin = "JTNKU3JE60J021675";
		String requestPath = "/api/myToyota/v1/vehicles/" + vin + "/check";
		
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Vehicle API GET vehicle by vin"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							newMessage.setProperty("vin", vin, PropertyScope.INVOCATION);

							newMessage.setPayload(getClass()
									.getResourceAsStream(mockedResponeFile));
						} catch (Exception e) {
							// catch exception statements
						}

						return newMessage;
					}
				});
		
		MuleEvent event = testEvent(requestPath);
		MuleEvent output = runFlow("get:/vehicles/{vin}/check:myToyota-config", event);

		System.out.println(output.getMessage().getPayload());

		assertEquals(vin, JsonPath.read(output.getMessage().getPayloadAsString(), "$.vin"));
		assertEquals(isValid, JsonPath.read(output.getMessage().getPayloadAsString(), "$.isValid"));
	}
}
