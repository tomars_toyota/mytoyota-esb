package au.com.toyota.esb.mytoyota.api;

import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.junit.Assert.assertTrue;
import static org.mule.munit.common.mocking.Attribute.attribute;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;

public class GetUsersMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"get-users.xml",
				"get-users-sfdc-technical-adapter.xml",
				"create-salesforce-sessionid-soap-header.xml",
				"config.xml"
		}, " ");
	}

	//@Test
	public void testSuccessfulRequest() throws Exception {

		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(attribute("name").ofNamespace("doc")
						.withValue("Invoke Salesforce Partner Operation login"))
				.thenReturn(muleMessageWithPayload(
						IOUtils.toString(getClass().getResourceAsStream("/in/sfdc-login-response.xml"))));
	
		
		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(attribute("name").ofNamespace("doc")
						.withValue("Invoke Salesforce Operation retrieveGuestInformation"))
				.thenReturn(muleMessageWithPayload(
						IOUtils.toString(getClass().getResourceAsStream("/in/get-users-sfdc-response.xml"))));
		
		
		MuleMessage message = muleMessageWithPayload("");
		message.setProperty("salesforceId", "0011900000NDTZc", PropertyScope.INBOUND);
		
		MuleEvent event = testEvent("");
		event.setMessage(message);

		MuleEvent output = runFlow("get:/users:myToyota-config", event);

		System.out.println(output.getMessage().getPayload());
		
		String apiResponse = IOUtils.toString(getClass().getResourceAsStream("/out/get-users-sfdc-api-response.json"));
		
		assertJsonEquals(apiResponse, output.getMessage().getPayloadAsString());
	}
	
	//@Test
	public void unsuccessfulRequest() throws Exception {

		MuleEvent event = testEvent("");

		MuleEvent output = runFlow("get:/users:myToyota-config", event);
		
		System.out.println(output.getMessage().getPayload());
		
		assertTrue(output.getMessage().getInvocationProperty("statusReason").equals("Invalid request, missing user ID"));
		assertTrue(output.getMessage().getInvocationProperty("statusCode").equals("400"));
		assertJsonEquals("{\"message\":\"Invalid request, missing user ID\"}", 
				output.getMessage().getPayloadAsString());
	}
}
