package au.com.toyota.esb.mytoyota.api;

import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.HashMap;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.skyscreamer.jsonassert.JSONAssert;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;

@Ignore
public class PostSessionTLinkCreateTokenMunitTest extends FunctionalMunitSuite {

	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] { "post-create-user-session-tlink.xml",
				"post-create-user-session-tlink-technical-adapter.xml", 
				"get-user-account-info-technical-adapter.xml",
				"session-management-im-provider-interactions.xml", 
				"config.xml" }, " ");
	}

	@Test
	public void testSuccessfulCall() throws Exception {
		// Mock get user profile "IM API Get User" response
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("IM API Get User"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							// newMessage.setOutboundProperty("X-User-Id", UID);
							newMessage.setPayload(IOUtils.toString(getClass().getResourceAsStream(
									"/in/post-user-session-tlink-create-get-user-profile-im-provider-response.json")));
						} catch (Exception e) {
						}
						return newMessage;
					}
				});

		// Mock update user profile "IM API Update User" response
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("IM API Update User"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("login", "ahmed.huwait+108@gmail.com", PropertyScope.INVOCATION);

							HashMap<String, String> apiSessionToken = new HashMap<>();
							apiSessionToken.put("value", "bf82744d-f8e2-4e9b-a6e2-aedc7a4ae7ba");
							apiSessionToken.put("expiry", "2017-11-11 17:34:14");
							newMessage.setProperty("APISessionToken", apiSessionToken, PropertyScope.INVOCATION);

							newMessage.setProperty("UID", "11ee6c60a37746c08c371b39e07bb0df", PropertyScope.INVOCATION);
							newMessage.setProperty("myToyotaId", "TEST-000000cd", PropertyScope.INVOCATION);

							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);

						} catch (Exception e) {
						}
						return newMessage;
					}
				});

		MuleEvent event = testEvent("");

		// Invoke the flow
		MuleEvent output = runFlow("post:/sessions/tlink:myToyota-config", event);
		String responseMessagePayload = output.getMessage().getPayloadAsString();
		System.out.println("**** response: " + responseMessagePayload);

		String expected = IOUtils
				.toString(getClass().getResourceAsStream("/out/post-create-user-session-tlink-response.json"));

		JSONAssert.assertEquals(expected, responseMessagePayload, false);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.post-create-user-session-tlink.request"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.post-create-user-session-tlink.response"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category")
						.withValue("esb.mytoyota-api-v1.post-create-user-session-tlink-technical-adapter.request"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category")
						.withValue("esb.mytoyota-api-v1.post-create-user-session-tlink-technical-adapter.response"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-user-account-info-im-technical-adapter.request"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-user-account-info-im-technical-adapter.response"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.session-management-store-session-token.request"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.session-management-store-session-token.response"))
				.times(1);
	}

}
