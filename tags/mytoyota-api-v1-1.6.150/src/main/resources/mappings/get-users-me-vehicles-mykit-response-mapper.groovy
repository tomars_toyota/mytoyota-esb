import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper
import java.text.SimpleDateFormat
import au.com.toyota.esb.mytoyota.api.util.GroovyHelper


v = payload

def vehicle = [:]
vehicle.batchNumber = v?.batchNumber
vehicle.compliancePlate = v?.compliancePlate
vehicle.engineNumber = v?.engineNumber
vehicle.vin = v?.vin
vehicle.materialNumber = v?.materialNumber

//enrich vehcile with its description, for unverified vehicles only
if(flowVars['vehicle'])
	vehicle.vehicleDescription = new JsonSlurper().parseText(flowVars['vehicle'])?.vehicleDescription


vehicle.deliveryDate = v?.deliveryDate
vehicle.katashikiCode = v?.katashikiCode
vehicle.transmission = v?.transmission
vehicle.sellingDealerCode = v?.sellingDealerCode
vehicle.sellingDealerName = v?.sellingDealerName
vehicle.registrationNumber = v?.registrationNumber
vehicle.production = v?.production

//MYT-1101
vehicle.isVerified = v?.isVerified

if(v?.discontinuedDate)
	vehicle.discontinuedDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date(v.discontinuedDate))
if(v?.ownershipCreatedDate)
	vehicle.ownershipCreatedDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date(v.ownershipCreatedDate))

vehicle.releaseYear = v?.year

if(v?.suffixCode) {
	vehicle.suffix = [:]
	vehicle.suffix.code = v.suffixCode
	vehicle.suffix.description = v.suffixDescription
}
if(v?.trimCode) {
	vehicle.trim = [:]
	vehicle.trim.code = v.trimCode
	vehicle.trim.description = v.trimDescription
}
if(v?.paintCode) {
	vehicle.paint = [:]
	vehicle.paint.code = v.paintCode
	vehicle.paint.description = v.paintDescription
}


vehicle.salesType = v?.salesType	

//stock images
imagesList = []
v?.images.each { Map image ->
	imageMap = [:]
	imageMap.size = image.code
	imageMap.link = image.url

	//remove nulls
	imageMap = imageMap.findAll {it.value != null && it.value != ""}

	if(imageMap)
		imagesList << imageMap
}

if(imagesList)
	vehicle.images = imagesList

//vehicle user
vehicleUserMap = [:]

vehicleUserMap.registrationNumber = v?.userRegistrationNumber
vehicleUserMap.state = v?.state

//remove nulls
vehicleUserMap = vehicleUserMap.findAll {it.value != null && it.value != ""}
vehicleUserMap.relationshipTypes = v?.relationshipTypes

if(vehicleUserMap)
	vehicle.vehicleUser = vehicleUserMap

//custom image
if(v?.vehicleImage) {
	customImages = []
	customImage = [:]
	customImage.code = "primary"
	customImage.link = v?.vehicleImage
	customImages << customImage
	vehicle.customImages = customImages
}

vehicle.vehicleOdo = v?.odometer

//remove nulls, ignore blanks
vehicle = GroovyHelper.removeNulls(vehicle, true)

// Add to vehicle list
// response['vehicles'].add(vehicle)

if(!flowVars['vehiclesList'])
	flowVars['vehiclesList'] = []
flowVars['vehiclesList'] << vehicle
