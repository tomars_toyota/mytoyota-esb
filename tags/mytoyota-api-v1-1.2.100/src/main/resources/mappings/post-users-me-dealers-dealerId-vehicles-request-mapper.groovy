import java.net.URLEncoder;
import groovy.json.JsonSlurper

def request = new JsonSlurper().parseText(payload)
def postBody = null

//println "request: "+request

//default vehicleOdo to "1" if it is not set 
vehicleOdoVar = 1
if(request['vehicleOdo'] != null && request['vehicleOdo'] != 0)
	vehicleOdoVar = request['vehicleOdo']

// 'newYear' vehicle year field mandatory in TUNE, sometimes not set from front-ends
// Hardcoded as a workaround because TUNE performs a 'vin ping' anyway to retrieve vehicle details
def vehicleYearValue = ( !(request['vehicleYear']) || request['vehicleYear'] == "0000") ? "2010" : request['vehicleYear']
	
// generate the 'x-www-form-urlencoded' encoded body
try{ 
	postBody = new StringBuilder()
		.append("SiteID="        + URLEncoder.encode(flowVars['dmsID'], "UTF-8"))
		.append("&Mode=CV")
		.append("&newFirstName=" + URLEncoder.encode(request['firstName'], "UTF-8"))
		.append("&newLastName="  + URLEncoder.encode(request['lastName'], "UTF-8"))
		.append("&newEmail="     + URLEncoder.encode(request['email'], "UTF-8"))
		.append("&newMobile="    + URLEncoder.encode(request['mobile'], "UTF-8"))
		.append("&newFrnCid="    + URLEncoder.encode(flowVars['myToyotaId'], "UTF-8"))
		.append("&newMake="      + URLEncoder.encode(request['vehicleMake'], "UTF-8"))
		.append("&newModel="     + URLEncoder.encode(request['vehicleModel'], "UTF-8"))
		//.append("&newOtherModel=")
		.append("&newRego="      + URLEncoder.encode(request['vehicleRegistration'], "UTF-8"))
		.append("&newYear="      + vehicleYearValue)
		.append("&newOdo="       + vehicleOdoVar)
} catch (NullPointException) {
	throw new org.mule.module.apikit.exception.BadRequestException("Invalid request")
}
//println '______ '+postBody.toString()

return postBody.toString()
