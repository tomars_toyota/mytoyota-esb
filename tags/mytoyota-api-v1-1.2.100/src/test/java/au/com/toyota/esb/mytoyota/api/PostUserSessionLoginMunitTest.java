package au.com.toyota.esb.mytoyota.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;

//@Ignore
public class PostUserSessionLoginMunitTest extends FunctionalMunitSuite {
	
	private static Properties adapterProperties = new Properties();
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"post-create-user-session.xml",
				"post-create-user-session-technical-adapter.xml",
				"post-create-user-session-native-login-technical-adapter.xml",
				"post-create-user-session-social-login-registration-technical-adapter.xml",
				"get-user-id-availability.xml", 
				"get-user-id-availability-technical-adapter.xml",
				"post-passwordpolicy-validations-technical-adapter.xml", 
				"post-register-user-technical-adapter.xml",
				"get-user-account-info-technical-adapter.xml", 
				"complete-login-for-cosi-async.xml",
				"post-link-cosi-technical-adapter-flow.xml", 
				"get-orders-order-id.xml",
				"get-orders-order-id-technical-adapter.xml", 
				"put-update-user-account-info-technical-adapter.xml",
				"session-management-im-provider-interactions.xml", 
				"create-salesforce-sessionid-soap-header.xml",
				"post-passwords-encrypt-myKit-technical-adapter.xml", 
				"sfdc-technical-adapter.xml",
				"sfdc-update-user.xml", 
				"sfdc-create-prospect.xml", 
				"sfdc-update-guest-preferences.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulNativeLogin() throws Exception {
		
		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));

		String requestPath = "post:/sessions:application/json:myToyota-config";
		String nativeLoginRequest = IOUtils.toString(getClass().getResourceAsStream("/in/post-user-session-login-native-request.json"));
		final String imLoginResponse = IOUtils.toString(getClass().getResourceAsStream("/in/post-user-session-login-native-im-provider-response.json"));
		String imUpdateAccountResponse = IOUtils.toString(getClass().getResourceAsStream("/in/post-user-session-login-add-session-token-im-provider-response.json"));

		// Create Mule Event
		MuleEvent event = testEvent("");
		
		// Add query parameter value, for native this is blank. If you don't set this the MUnit throws a MEL exception
		Map<String, Object> props = new HashMap<String,Object>();
		props.put("loginType", "");

		// Prime the Mule Event message with the request payload
		MuleMessage requestMessage = muleMessageWithPayload(nativeLoginRequest);
			
		// Add query param map to Mule Event
		requestMessage.setProperty("http.query.params", props, PropertyScope.INBOUND);
        
        event.setMessage(requestMessage);	
        
		// Configure IM API Login to return successful login response
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("IM API Login"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							newMessage = originalMessage.createInboundMessage();

							newMessage.setProperty("http.status", 201, PropertyScope.INBOUND);

							newMessage.setPayload(imLoginResponse);
						} catch (Exception e) {
							// catch exception statements
						}

						return newMessage;
					}
				});

		// Configure IM API Update User to return successfully
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("IM API Update User"))
				.thenReturn(muleMessageWithPayload(imUpdateAccountResponse));
		
		// Invoke the flow
		//@SuppressWarnings("unused")
		MuleEvent output = runFlow(requestPath, event);
		MuleMessage responseMessage = output.getMessage(); 
		
		// High level flow request
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log API Request")).times(1);
			// Technical adapter native login request		
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Native Login Request")).times(1);
				// Successful response from IM		
				verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Successful Response")).times(1);
			// Technical adapter native login response
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Native Login Response")).times(1);
		// High level flow response
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log API Response")).times(1);


		assertEquals("myToyota ID", "MYT-00000000", responseMessage.getInvocationProperty("myToyotaId"));
		assertEquals("UID", "93f295c9ad664c36be464d221acd6fb0", responseMessage.getInvocationProperty("UID"));
		assertTrue(responseMessage.getInvocationProperty("APISessionToken") != null);
	}
	
	@Test
	public void testSuccessfulNativeLoginExistingTokens() throws Exception {
		
		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));

		String requestPath = "post:/sessions:application/json:myToyota-config";
		String nativeLoginRequest = IOUtils.toString(getClass().getResourceAsStream("/in/post-user-session-login-native-request.json"));
		final String imLoginResponse = IOUtils.toString(getClass().getResourceAsStream("/in/post-user-session-login-native-existing-tokens-im-provider-response.json"));
		String imUpdateAccountResponse = IOUtils.toString(getClass().getResourceAsStream("/in/post-user-session-login-add-session-token-im-provider-response.json"));

		// Create Mule Event
		MuleEvent event = testEvent("");
		
		// Add query parameter value, for native this is blank. If you don't set this the MUnit throws a MEL exception
		Map<String, Object> props = new HashMap<String,Object>();
		props.put("loginType", "");

		// Prime the Mule Event message with the request payload
		MuleMessage requestMessage = muleMessageWithPayload(nativeLoginRequest);
			
		// Add query param map to Mule Event
		requestMessage.setProperty("http.query.params", props, PropertyScope.INBOUND);
        
        event.setMessage(requestMessage);	
        
		// Configure IM API Login to return successful login response
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("IM API Login"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							newMessage = originalMessage.createInboundMessage();

							newMessage.setProperty("http.status", 201, PropertyScope.INBOUND);

							newMessage.setPayload(imLoginResponse);
						} catch (Exception e) {
							// catch exception statements
						}

						return newMessage;
					}
				});

		// Configure IM API Update User to return successfully
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("IM API Update User"))
				.thenReturn(muleMessageWithPayload(imUpdateAccountResponse));
		
		// Invoke the flow
		//@SuppressWarnings("unused")
		MuleEvent output = runFlow(requestPath, event);
		MuleMessage responseMessage = output.getMessage(); 
		
		// High level flow request
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log API Request")).times(1);
			// Technical adapter native login request		
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Native Login Request")).times(1);
				// Successful response from IM		
				verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Successful Response")).times(1);
			// Technical adapter native login response
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Native Login Response")).times(1);
		// High level flow response
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log API Response")).times(1);


		assertEquals("myToyota ID", "MYT-00000000", responseMessage.getInvocationProperty("myToyotaId"));
		assertEquals("UID", "93f295c9ad664c36be464d221acd6fb0", responseMessage.getInvocationProperty("UID"));
		assertTrue(responseMessage.getInvocationProperty("APISessionToken") != null);
	}
		
	@Test
	public void testFailuresStatusCodes() throws Exception {

		int[] statusCodes = new int[] { 404, 405, 415, 406, 401, 500 };

		MuleEvent event = testEvent("");
		
		Map<String, Object> props = new HashMap<String,Object>();
		props.put("loginType", "");
		String nativeLoginRequest = IOUtils.toString(getClass().getResourceAsStream("/in/post-user-session-login-native-request.json"));
		
		MuleMessage requestMessage = muleMessageWithPayload(nativeLoginRequest);
			
		requestMessage.setProperty("http.query.params", props, PropertyScope.INBOUND);
        
        event.setMessage(requestMessage);

		for (int i = 0; i < statusCodes.length; i++) {
			new StatusCodesTestingHelper().runFailureTest(statusCodes[i], "IM API Login",
					"post:/sessions:application/json:myToyota-config", event);
		}
	}
}
