package au.com.toyota.esb.mytoyota.api.metrics;

import java.io.StringWriter;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import org.apache.log4j.Logger;
import org.mule.api.context.notification.MessageProcessorNotificationListener;
import org.mule.api.processor.MessageProcessor;
import org.mule.context.notification.MessageProcessorNotification;
import org.mule.module.apikit.Router;
import org.mule.module.http.internal.request.DefaultHttpRequester;

public class MuleComponentProcessListener
		implements MessageProcessorNotificationListener<MessageProcessorNotification> {
	private static Logger log = Logger.getLogger("esb.mytoyota-api-v1.instrument");
	private boolean logMetrics;
	private ThreadLocal<Stack<Map<String, String>>> callStack;

	public MuleComponentProcessListener() {
		log.info("MuleComponentProcessListener is up...");
		callStack = ThreadLocal.withInitial(Stack::new);
	}

	@Override
	public void onNotification(MessageProcessorNotification notification) {
		log.debug("In MuleComponentProcessListener.onNotification: " + logMetrics);

		if (logMetrics) {
			String transactionId = notification.getSource().getSession().getProperty("transactionId");
			String status = (notification.getExceptionThrown() != null) ? "ERROR" : "OK";

			MessageProcessor process = notification.getProcessor();
			String className = process.getClass().getSimpleName();
			boolean isHttpRequest = process instanceof DefaultHttpRequester;
			boolean isApiKitRouter = process instanceof Router;

			if (className.equalsIgnoreCase("Router") && isApiKitRouter) {
				handleRouter(notification, transactionId, status);

			}
			if (className.equalsIgnoreCase("DefaultHttpRequester") && isHttpRequest) {
				handleDefaultHttpRequester(notification, transactionId, status);
			}
		}
	}

	private void handleDefaultHttpRequester(MessageProcessorNotification notification, String transactionId,
			String status) {
		try {
			DefaultHttpRequester def = (DefaultHttpRequester) notification.getProcessor();
			if (notification.getAction() == MessageProcessorNotification.MESSAGE_PROCESSOR_PRE_INVOKE) {
				Map<String, String> props = new HashMap<>();
				props.put("defaultHttpRequester.startTime", System.currentTimeMillis() + "");
				callStack.get().push(props);
			}
			if (notification.getAction() == MessageProcessorNotification.MESSAGE_PROCESSOR_POST_INVOKE) {
				Map<String, String> call = callStack.get().pop();
				if (call != null) {
					LogMessageBuilder builder = new LogMessageBuilder();
					builder.threadId(Thread.currentThread().getId()).metric("RT").direction("outbound")
							.transactionId(transactionId).host(def.getHost()).method(def.getMethod())
							.path(def.getPath()).status(status).executionTime(System.currentTimeMillis()
									- Long.parseLong(call.get("defaultHttpRequester.startTime")));
					logMessage(builder.buildAsTags());
				}
			}
		} catch (Exception e) {
			log.error("Error in MuleComponentProcessListener.handleDefaultHttpRequester", e);
		}
	}

	private void handleRouter(MessageProcessorNotification notification, String transactionId, String status) {
		try {

			LogMessageBuilder builder = new LogMessageBuilder();

			if (notification.getAction() == MessageProcessorNotification.MESSAGE_PROCESSOR_PRE_INVOKE) {
				Map<String, String> props = new HashMap<>();
				notification.getSource().getMessage().getInboundPropertyNames().forEach(name -> {
					props.put(name, notification.getSource().getMessage().getInboundProperty(name));
				});
				props.put("router.startTime", System.currentTimeMillis() + "");
				callStack.get().push(props);

				builder = new LogMessageBuilder();
				builder.threadId(Thread.currentThread().getId()).transactionId(transactionId).metric("HIT")
						.httpParams(props);

				logMessage(builder.buildAsTags());
			}
			if (notification.getAction() == MessageProcessorNotification.MESSAGE_PROCESSOR_POST_INVOKE) {

				if (callStack.get() != null) {
					Map<String, String> callParams = callStack.get().pop();
					if (callParams != null) {
						builder = new LogMessageBuilder();
						builder.threadId(Thread.currentThread().getId()).metric("RT").direction("inbound")
								.transactionId(transactionId).httpParams(callParams).status(status)
								.executionTime(System.currentTimeMillis()
										- Long.parseLong(callParams.get("router.startTime")));
						logMessage(builder.buildAsTags());
					}
				}
				
				callStack.remove();
			}
		} catch (Exception e) {
			log.error("Error in MuleComponentProcessListener.handleRouter", e);
			if (notification.getAction() == MessageProcessorNotification.MESSAGE_PROCESSOR_POST_INVOKE) {
				callStack.remove();
			}
		}
	}

	private void logMessage(String msg) {
		log.info(msg);
	}

	class LogMessageBuilder {

		private Map<Params, String> data = new HashMap<>();

		LogMessageBuilder transactionId(String transactionId) {
			putIfNotEmpty(Params.transactionId, transactionId);
			return this;
		}

		LogMessageBuilder threadId(long threadId) {
			putIfNotEmpty(Params.threadId, String.valueOf(threadId));
			return this;
		}

		LogMessageBuilder host(String host) {
			putIfNotEmpty(Params.host, host);
			return this;
		}

		LogMessageBuilder path(String path) {
			putIfNotEmpty(Params.path, path);
			return this;
		}

		LogMessageBuilder method(String method) {
			putIfNotEmpty(Params.method, method);
			return this;
		}

		LogMessageBuilder queryString(String queryString) {
			putIfNotEmpty(Params.queryString, queryString);
			return this;
		}

		LogMessageBuilder metric(String metric) {
			putIfNotEmpty(Params.metric, metric);
			return this;
		}

		LogMessageBuilder direction(String direction) {
			putIfNotEmpty(Params.direction, direction);
			return this;
		}

		LogMessageBuilder status(String status) {
			putIfNotEmpty(Params.status, status);
			return this;
		}

		LogMessageBuilder executionTime(long executionTime) {
			putIfNotEmpty(Params.executionTime, String.valueOf(executionTime));
			return this;
		}

		LogMessageBuilder httpParams(Map<String, String> httpInboundProperties) {

			return host(httpInboundProperties.get("host")).method(httpInboundProperties.get("http.method"))
					.path(httpInboundProperties.get("http.request.path"))
					.queryString(httpInboundProperties.get("http.query.string"));

		}

		public String buildAsTags() {

			StringWriter sw = new StringWriter();
			sw.append("[METRICS]");
			data.keySet().stream().sorted().forEach(k -> {
				sw.append("[").append(k.name()).append("=").append(data.getOrDefault(k, "")).append("]");
			});
			return sw.toString();
		}

		private void putIfNotEmpty(Params k, String v) {
			if (notEmpty(v)) {
				data.put(k, v);
			}
		}
	}

	private static boolean notEmpty(String str) {
		return str != null && !str.trim().isEmpty();
	}

	private enum Params {
		metric, transactionId, threadId, direction, method, host, path, queryString, status, executionTime
	}

	public boolean isLogMetrics() {
		return logMetrics;
	}

	public void setLogMetrics(boolean logMetrics) {
		this.logMetrics = logMetrics;
	}

}
