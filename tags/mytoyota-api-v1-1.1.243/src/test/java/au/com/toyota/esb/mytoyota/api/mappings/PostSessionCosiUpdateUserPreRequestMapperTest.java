package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;

import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

public class PostSessionCosiUpdateUserPreRequestMapperTest extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;

	public PostSessionCosiUpdateUserPreRequestMapperTest() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/post-create-user-session-technical-adapter-cosi-update-user-pre-request-mapper.groovy");
	}

	@Test
	public void testSuccessfulRequestMapper() throws Exception {
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(getClass()
						.getResourceAsStream("/in/get-orders-by-order-id-vehicle-api-response.json")))
				.run().toString();

		System.out.println(result);
		
		// expected
		String expectedResult = IOUtils.toString(getClass()
				.getResourceAsStream(
						"/out/post-cosi-login-async-update-user-mykit-request.json"));
		JSONAssert.assertEquals(expectedResult, result, true);
	}
	
	@Test
	public void testSuccessfulRequestMapperNoPostalAddress() throws Exception {
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(getClass()
						.getResourceAsStream("/in/get-orders-by-order-id-vehicle-api-no-address-response.json")))
				.run().toString();

		System.out.println(result);
		
		// expected
		String expectedResult = IOUtils.toString(getClass()
				.getResourceAsStream(
						"/out/post-cosi-login-async-update-user-mykit-no-address-request.json"));
		JSONAssert.assertEquals(expectedResult, result, true);
	}
	
	@Test
	public void testSuccessfulRequestMapperStreetAddressOnly() throws Exception {
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(getClass()
						.getResourceAsStream("/in/get-orders-by-order-id-vehicle-api-street-address-only-response.json")))
				.run().toString();

		System.out.println(result);
		
		// expected
		String expectedResult = IOUtils.toString(getClass()
				.getResourceAsStream(
						"/out/post-cosi-login-async-update-user-mykit-street-address-only-request.json"));
		JSONAssert.assertEquals(expectedResult, result, true);
	}
	
}
