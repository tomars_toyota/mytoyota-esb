package au.com.toyota.esb.mytoyota.api.mappings;

import java.util.HashMap;

import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.DefaultMuleMessage;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.context.DefaultMuleContextFactory;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

//@Ignore
public class PostUserChangePasswordRequestMapperTest extends AbstractMuleContextTestCase {

	private ScriptRunner scriptRunner;
	
	public PostUserChangePasswordRequestMapperTest() throws ScriptException{
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/post-user-change-password-request-mapper.groovy");
	}
	
	@Test
	public void testRequestMapper() throws Exception {
		
		String token = "eyJjdHkiOiJKV1QiLCJlbmMiOiJBMjU2R0NNIiwiYWxnIjoiZGlyIn0..4MpwkEeiRMY8Lu-x."
				+ "Q04GObcTeL2wtrFcVrm5mvUP9WRUCvNJKKiASzutcfVdLhgGYpjVLz-LNe2Qd-7cauTuuSTkGaXXBWdX1IbCeLyfI"
				+ "uQZG7ouRdqX4slNf1A1uFZvhxl0Oj_NrQjKdthetePQxJzfnTODs8QYYMdeLM9nSImtkAwT-KC9Arzz-qhoeXaX3Dp"
				+ "0BD93-KT8Yb6y8GJaxCQUYcYyGcRNgbw6QkPg81dk4UyTzouC9S9tnKeI93UCcW4AHjWVKqqC9G0b0zAaHV9cAYMJb8"
				+ "0l4emep1u9sl5E-7SnJtflDitduCe47pSFuAlxqNZvC19pma_2cr8ODOcFUciy9RKza15rWbHnEL5YKvVmxXQz13Rf"
				+ "zY9bIquL9NS1xQ.aMuOugYFuUMgzkIq2fJ48w";
		
		HashMap<String, Object> requestPayload = new HashMap<String, Object>();
		requestPayload.put("newPassword", "password1");
		requestPayload.put("password", "password11");

		
		muleContext = new DefaultMuleContextFactory().createMuleContext();
		MuleMessage message = new DefaultMuleMessage(null,muleContext);
		message.setProperty("APISessionToken", token, PropertyScope.INBOUND);
		
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(requestPayload)
				.message(message)
				.sessionVar("key", "value")
				.binding("secretKey", "PxpRI_jZNkP_eIgmr_gApbM")
				.binding("encryptionEnabled", "false")
				.run().toString();
		
		System.out.println("________result: "+result);

		// expected
		String expectedResult = IOUtils.toString(getClass()
				.getResourceAsStream(
						"/out/post-user-change-password-request.json"));
		JSONAssert.assertEquals(expectedResult, result, true);
	}
}
