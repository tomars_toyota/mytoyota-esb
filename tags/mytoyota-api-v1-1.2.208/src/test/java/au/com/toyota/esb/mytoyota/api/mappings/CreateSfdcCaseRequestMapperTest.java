package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;

import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.custommonkey.xmlunit.XMLTestCase;
import org.junit.Test;
import org.xml.sax.SAXException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

public class CreateSfdcCaseRequestMapperTest extends XMLTestCase {

	private ScriptRunner scriptRunner;

	public CreateSfdcCaseRequestMapperTest() throws ScriptException, SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/sfdc-create-case-request-mapper.groovy");
	}

	@Test
	public void testRequestMapper() throws Exception {
		
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload("")
				.binding("regoNumber", "AAA111")
				.binding("regoState", "VIC")
				.binding("myToyotaId", "TEST-000002e9")
				.binding("vin", "JTNKU3JE60J021675")
				.binding("recordTypeId", "01230000000OJdz")
				.flowVar("vinSFDCId", "a0622000000g1frAAA")
				.flowVar("myToyotaUserAccountSFDCId", "0012200000FiI5sAAF")
				.run().toString();

		System.out.println(result);

		// expected
		String expectedResult = IOUtils.toString(getClass().getResourceAsStream("/out/create-case-sfdc-request.xml"));
		assertXMLEqual(expectedResult, result);
	}
}
