package au.com.toyota.esb.mytoyota.api.mappings;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

public class MapNativeRegoUserDetailsTest extends AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;
	
	public MapNativeRegoUserDetailsTest() throws ScriptException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/map-native-rego-user-details.groovy");
	}
	
	@Test
	public void testResponseMapper() throws Exception {
		
		String result = ScriptRunBuilder.runner(scriptRunner)
				.payload(IOUtils
						.toString(getClass().getResourceAsStream("/in/retrieve-guest-info-sfdc-response.json")))
				.run().toString();
						
		System.out.println(result);
		
		// expected
		String expectedResult = IOUtils.toString(getClass()
				.getResourceAsStream(
						"/out/retrieve-guest-info-sfdc-mapped-response.json"));
		JSONAssert.assertEquals(expectedResult, result, true);
	}
	
	@Test
	public void testEmptyResponseMapper() throws Exception {
		
		Object result = ScriptRunBuilder.runner(scriptRunner)
				.payload(IOUtils
						.toString(getClass().getResourceAsStream("/in/retrieve-guest-info-sfdc-empty-response.json")))
				.run();
		
		assertTrue(result.equals(org.mule.transport.NullPayload.class));
	}
}
