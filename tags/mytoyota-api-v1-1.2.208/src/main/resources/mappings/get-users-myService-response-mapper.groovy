import static groovy.json.JsonOutput.*
import au.com.toyota.esb.mytoyota.api.util.GroovyHelper

user = [:]

// For Testing only Generate an email for new Registrations
Date dateNow = new Date()
def emailPrefix = dateNow.format("yyyMMddHHmmssSSS")


// This is to test the existing user
if (flowVars.refId == "123456") {
	user.firstName = "OSB myToyota"
	user.lastName = "Existing"
	user.email = "mytoyotatest2+56@gmail.com"

}
else {
	user.firstName = "OSB Firstname"
	user.lastName = "Lastname"
	user.email = "mytoyotatest2+" + emailPrefix + "@gmail.com"

}

user.dateOfBirth = "1984-10-13"
user.mobile = "0431234567"
user.landline = "0438911234"

addressMap = [:]

residentialAddress = "20 OSB Test, Melbourne, VIC, 3030"
residentialAddressMap = [:]

residentialAddressMap.suburb = "Melbourne"
residentialAddressMap.postcode = "3000"
residentialAddressMap.state = "VIC"
residentialAddressMap.streetAddress = "20 OSB Test"
if(GroovyHelper.removeNulls(residentialAddressMap)){
	residentialAddressMap.totalCheckId = 'other'
	addressMap.residential = residentialAddressMap
}

postalAddress= "20 OSB Test, Melbourne, VIC, 3030"
postalAddressMap = [:]

postalAddressMap.suburb = "Melbourne"
postalAddressMap.postcode = "3000"
postalAddressMap.state = "VIC"
postalAddressMap.streetAddress = "20 OSB Test"

if(GroovyHelper.removeNulls(postalAddressMap)){
	postalAddressMap.totalCheckId = 'other'
	addressMap.postal = postalAddressMap
}

addressMap =  GroovyHelper.removeNulls(addressMap)
if(addressMap)
	user.address = addressMap


return prettyPrint(toJson(user))

