import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper
import org.apache.commons.lang.StringUtils 
import org.slf4j.Logger
import org.slf4j.LoggerFactory
  
Logger logger = LoggerFactory.getLogger("esb.mytoyota-api-v1.get-vehicle-outstanding-recalls-campaignCode-content-mapping.script");



JsonSlurper js = new JsonSlurper()

//logger.info("payload: " + payload)

def contentResponse = null
if (payload && !(payload instanceof org.mule.transport.NullPayload)) {
	def parsedPayload = js.parseText(payload)
	contentResponse = parsedPayload?.content
}


response = [:]

response.recallCategory = flowVars.recallCategory
response.content = contentResponse

response = au.com.toyota.esb.mytoyota.api.util.GroovyHelper.removeNulls(response);



return prettyPrint(toJson(response))
