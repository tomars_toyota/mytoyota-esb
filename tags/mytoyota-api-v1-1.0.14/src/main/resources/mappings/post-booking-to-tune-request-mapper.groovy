
import java.net.URLEncoder;


// generate the 'x-www-form-urlencoded' encoded body
def postBody = new StringBuilder()
	.append("SiteID="       + URLEncoder.encode(flowVars['dmsID'], "UTF-8"))
	.append("&Rego="        + URLEncoder.encode(payload['registrationNumber'], "UTF-8"))
	.append("&Service="     + URLEncoder.encode(payload['selectedService'], "UTF-8"))
	.append("&VehID="       + URLEncoder.encode(payload['vehicleID'], "UTF-8"))
	.append("&BookDate="    + payload['bookDate'])
	.append("&BookTime="    + payload['bookTime'])
	.append("&OutDate="     + payload['outDate'])
	.append("&OutTime="     + payload['outTime'])
	.append("&ConfType="    + URLEncoder.encode(payload['confirmationType'], "UTF-8"))
	.append("&ConfTo="      + URLEncoder.encode(payload['confirmationTo'], "UTF-8"))
	.append("&SrvPrc="      + payload['servicePrice'])
	.append("&UpSells="     + (payload.containsKey('upSells') ? URLEncoder.encode(payload['upSells'], "UTF-8") : ""))
	.append("&Notes="       + (payload.containsKey('notes') ? URLEncoder.encode(payload['notes'], "UTF-8") : ""))
	.append("&Wait="        + (payload.containsKey('waitAtDealerFlag')   ? payload['waitAtDealerFlag']   : ""))
	.append("&Express="     + (payload.containsKey('expressServiceFlag') ? payload['expressServiceFlag'] : ""))
	.append("&CourtesyBus=" + (payload.containsKey('courtesyBusFlag')    ? payload['courtesyBusFlag']    : ""))
	.append("&Loan="        + (payload.containsKey('loanVehicleFlag')    ? payload['loanVehicleFlag']    : ""))
	

return postBody.toString()
