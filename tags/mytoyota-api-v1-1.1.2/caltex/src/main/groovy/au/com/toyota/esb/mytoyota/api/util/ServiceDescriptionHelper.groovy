package au.com.toyota.esb.mytoyota.api.util

public class ServiceDescriptionHelper {
	def static String formatServiceDescription(serviceDescription, disableFormat) {
		if (serviceDescription == null || disableFormat == true) 
			return serviceDescription
	
		def formattedServiceDescription = serviceDescription
		formattedServiceDescription = formatKmServiceDescription(formattedServiceDescription)
		formattedServiceDescription = formatMonthServiceDescription(formattedServiceDescription)
		formattedServiceDescription = formatNumberServiceDescription(formattedServiceDescription)
		
		return formattedServiceDescription
	}
	
	def static formatKmServiceDescription(serviceDescription) {
		def formattedDescription = serviceDescription
		
		def group = (serviceDescription =~ /.*(\d+)(\s*K[mM])/)
		
		if (group != null && group.size() > 0) {
			if (group[0] != null && group[0].size() > 2) {
				def kmString = group[0][2]
				def formattedKM = " km"
				formattedDescription = formattedDescription.replaceAll(kmString, formattedKM)
			}
		} 
		
		return formattedDescription
	}
	
	def static formatMonthServiceDescription(serviceDescription) {

		def formattedServiceDescription = serviceDescription.replaceAll("MTHS", "Months")
		return formattedServiceDescription
		
	}
	
	def static formatKm(kmNum) {

		return java.text.NumberFormat.getNumberInstance(Locale.US).format(new Integer(kmNum))
	}
	
	def static formatNumberServiceDescription(serviceDescription) {
		
		def formattedDescription = serviceDescription
		
		def group = (serviceDescription =~ /^\D*(\d+?)\s*(km)/)
		
		if (group != null && group.size() > 0) {
			
			if (group[0] != null && group[0].size() > 1) {
				def kmDigits = group[0][1]
				def formattedKM = formatKm(kmDigits)
				formattedDescription = formattedDescription.replaceAll(kmDigits, formattedKM)
			}
		} 
			
	
		return formattedDescription
	}

	
}