import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper
import au.com.toyota.esb.mytoyota.api.util.GroovyHelper
import au.com.toyota.esb.mytoyota.api.util.CounterHelper
import java.text.*


def storedCounter = muleContext.getRegistry().lookupObject("storedCounter")
println "storedCounter: " + storedCounter
if (!storedCounter) {
	println "initialising storedCounter"
	storedCounter = 1;
	muleContext.getRegistry().registerObject("storedCounter", storedCounter)
}


def response = [:]
def currentCounter = storedCounter
println "currentCounter: " + currentCounter
if ((currentCounter % 5) == 0) {
	response.status = "CLAIMED"
	response.lastClaimedDate = new Date()
}
else {
	response.status = "VALID"	
}


currentCounter = currentCounter + 1
muleContext.getRegistry().registerObject("storedCounter", currentCounter)

return prettyPrint(toJson(GroovyHelper.removeNulls(response, true)))