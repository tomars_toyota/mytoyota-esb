package au.com.toyota.esb.mytoyota.api;

import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mule.munit.common.mocking.Attribute.attribute;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.cache.InvalidatableCachingStrategy;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;

//@Ignore
public class GetWarrantyHistoryMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"get-vehicle-warranty-history.xml",
				"get-vehicle-warranty-history-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	//@Test
	public void testSuccessfulRequest() throws Exception {

		final String apiResponse = IOUtils.toString(getClass().getResourceAsStream("/in/get-vehicle-warranty-history-response.json"));
		String vin = "6T153BK360X070769";
		String requestPath = "/api/myToyota/v1/vehicles/" + vin + "/warrantyHistory";

		whenMessageProcessor("request").ofNamespace("http")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("Vehicle API GET Warranty History"))
			.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							newMessage = originalMessage.createInboundMessage();

							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);

							newMessage.setPayload(apiResponse);
						} catch (Exception e) {
							// catch exception statements
						}

						return newMessage;
					}
				});

		MuleEvent event = testEvent(requestPath);
		event.setFlowVariable("vin", vin);

		MuleEvent output = runFlow("get:/vehicles/{vin}/warrantyHistory:myToyota-config", event);

		System.out.println(output.getMessage().getPayload());
		assertJsonEquals(apiResponse, output.getMessage().getPayloadAsString());
	}
	
	//@Test
	public void testUnsuccessfulRequest() throws Exception {

		final String vin = "ABC123";
		final String requestPath = "/api/myToyota/v1/vehicles/" + vin + "/warrantyHistory";

		whenMessageProcessor("request").ofNamespace("http")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("Vehicle API GET Warranty History"))
			.thenThrow(new org.mule.module.apikit.exception.NotFoundException(requestPath));
		
		MuleEvent event = testEvent(requestPath);
		event.setFlowVariable("vin", vin);

		try {
			runFlow("get:/vehicles/{vin}/warrantyHistory:myToyota-config", event);
		} catch (Exception e) {
			assertTrue(e.getCause() instanceof org.mule.module.apikit.exception.NotFoundException);
			assertEquals(requestPath, e.getCause().getMessage());
		}

	}

	//@Test
	public void testFailuresStatusCodes() throws Exception {

		int[] statusCodes = new int[] { 404, 405, 415, 406, 401, 500 };

		String vin = "6T153BK360X070769";
		String requestPath = "/api/myToyota/v1/vehicles/" + vin + "/warrantyHistory";
		MuleEvent event = testEvent(requestPath);
		event.setFlowVariable("vin", vin);

		for (int i = 0; i < statusCodes.length; i++) {
			
			// Invalidate Cache as this unit test relies on different stubbed response from cached endpoint
			InvalidatableCachingStrategy cache = muleContext.getRegistry().<InvalidatableCachingStrategy>get("Generic_Caching_Strategy_Low_Currency");
			cache.invalidate();
			
			new StatusCodesTestingHelper().runFailureTest(statusCodes[i], "Vehicle API GET Warranty History",
					"get:/vehicles/{vin}/warrantyHistory:myToyota-config", event);
		}
	}
}
