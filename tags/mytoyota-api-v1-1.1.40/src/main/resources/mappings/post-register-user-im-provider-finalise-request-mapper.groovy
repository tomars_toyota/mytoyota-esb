import groovy.json.JsonBuilder
import groovy.json.JsonSlurper

//println '______ payload: '+payload
registerResponse = new JsonSlurper().parseText(payload) 

flowVars['UID'] = registerResponse?.UID
flowVars['regToken'] = registerResponse?.regToken

def requestPayload = flowVars['requestPayload']

def payload = [:]

//include regToken for RDR scenario, for Gigy'as setAccountInfo() in this case is 
//part of the registration, i.e. registration is not finalised yet when it is called
if(flowVars['isRdr'] == 'true') {
	payload.UID = registerResponse?.UID
	payload.isVerified = true
}

// myToyotaId returned back from Fabrics myToyota backend
def applicationIdentifiers = ['myToyota':['id':flowVars['myToyotaID']]]
payload.applicationIdentifiers = applicationIdentifiers

// map accepted TC version
if(requestPayload?.termsAndConditions?.acceptedVersion?.length() > 0) {
	def termsAndConditions = ['acceptedVersion':requestPayload?.termsAndConditions?.acceptedVersion]
	payload.termsAndConditions = termsAndConditions
}

// map accepted Privacy Statement version
if(requestPayload?.privacyStatement?.acceptedVersion?.length() > 0) {
	def privacyStatement = ['acceptedVersion':requestPayload?.privacyStatement?.acceptedVersion]
	payload.privacyStatement = privacyStatement
}

return payload

// won't map these, don't want this much customer data in gigya
// profile.lastName = requestPayload?.lastName ?: ''
// profile."phones.type" = "mobile"
// profile."phones.number" = requestPayload?.mobile ?: ''