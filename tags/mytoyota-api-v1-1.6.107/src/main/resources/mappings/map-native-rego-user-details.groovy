import groovy.json.JsonSlurper
import static groovy.json.JsonOutput.*
import au.com.toyota.esb.mytoyota.api.util.GroovyHelper

sfdcUser = new JsonSlurper().parseText(payload)
myKitUser = [:]
myKitUser.title = sfdcUser.title ?: null
myKitUser.mobile = sfdcUser.mobile ?: null
myKitUser.landline = sfdcUser.landline ?: null
myKitUser.dateOfBirth = sfdcUser.dateOfBirth ?: null
myKitUser.address = sfdcUser.address ?: null


// If an address (or part of address) is received without a totalCheckId, then set it to 'other' because
//  the old address' referenceId stays in myKIT causing the front-ends to lookup the old address based on this ID.
if ((
		(myKitUser?.address?.postal?.streetAddress || myKitUser?.address?.postal?.streetAddress?.length() == 0) ||
		(myKitUser?.address?.postal?.suburb        || myKitUser?.address?.postal?.suburb?.length() == 0) ||
		(myKitUser?.address?.postal?.state         || myKitUser?.address?.postal?.state?.length() == 0) ||
		(myKitUser?.address?.postal?.postcode      || myKitUser?.address?.postal?.postcode?.length() == 0) ||
		(myKitUser?.address?.postal?.country       || myKitUser?.address?.postal?.country?.length() == 0)) && 
		(myKitUser?.address?.postal?.totalCheckId == null)) {
	myKitUser.address.postal['totalCheckId'] = 'other'
 }

if ((
		(myKitUser?.address?.residential?.streetAddress || myKitUser?.address?.residential?.streetAddress?.length() == 0) ||
		(myKitUser?.address?.residential?.suburb        || myKitUser?.address?.residential?.suburb?.length() == 0) ||
		(myKitUser?.address?.residential?.state         || myKitUser?.address?.residential?.state?.length() == 0) ||
		(myKitUser?.address?.residential?.postcode      || myKitUser?.address?.residential?.postcode?.length() == 0) ||
		(myKitUser?.address?.residential?.country       || myKitUser?.address?.residential?.country?.length() == 0)) && 
		(myKitUser?.address?.residential?.totalCheckId == null)) {
	myKitUser.address.residential['totalCheckId'] = 'other'
 }

myKitUser = GroovyHelper.removeNulls(myKitUser)

if(!myKitUser)
	return org.mule.transport.NullPayload
else
	return prettyPrint(toJson(myKitUser))