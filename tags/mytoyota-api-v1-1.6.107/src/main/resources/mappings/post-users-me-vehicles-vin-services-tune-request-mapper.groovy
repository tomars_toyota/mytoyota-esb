import java.net.URLEncoder;
import groovy.json.JsonSlurper

JsonSlurper js = new JsonSlurper()
def request = js.parseText(payload)

// println '____________ payload: '+payload

// generate the 'x-www-form-urlencoded' encoded body
def postBody = new StringBuilder()
	.append("SiteID="       + URLEncoder.encode(flowVars['dmsID'], "UTF-8"))
	.append("&Rego="         + URLEncoder.encode(request['registrationNumber'], "UTF-8"))
	.append("&Vehid="         + request['dmsVehicleId'])
	.append("&Service="     + URLEncoder.encode(request['operationTypeID']+'|'+
			request['operationID']+'|'+
			request['serviceTsaFlag']+'|'+
			(request['serviceOperationPrice'] == null ? 0 :request['serviceOperationPrice']) +'|'+
			request['externalNumberOfClaim']+'|'+
			request['serviceCode'], "UTF-8"))
	.append("&BookDate="    + getDaysSinceDayOne(request['dropOffDate']))
	.append("&BookTime="    + getSecSinceMidnight(request['dropOffTime']))
	.append(request.containsKey('pickUpDate') ? ("&OutDate=" + getDaysSinceDayOne(request['pickUpDate'])) : "")
	.append(request.containsKey('pickUpTime') ? ("&OutTime=" + getSecSinceMidnight(request['pickUpTime'])) : "")
	// Confirmation Type can be Email or SMS, however myToyota always sends Email, and business doesn't want to send SMS, refer: https://toyotaaustralia.atlassian.net/browse/MYT-589
	//.append(getConfType(request['confirmationType']))
	.append("&ConfType=")
	.append(request.containsKey('confirmationTo') ? ("&ConfTo=" + URLEncoder.encode(request['confirmationTo'], "UTF-8")) : "")
	.append("&SrvPrc="      + request['totalServicePrice'])
	.append(extractUpsells(request))
	.append((request.containsKey('notes') ? ("&Notes=" + URLEncoder.encode(request['notes'], "UTF-8")) : ""))
	.append((request.containsKey('waitAtDealerFlag') ? ("&Wait=" + request['waitAtDealerFlag']) : ""))
	.append((request.containsKey('notexpressServiceFlages') ? ("&Express=" + request['expressServiceFlag']) : ""))
	.append((request.containsKey('courtesyBusFlag') ? ("&CourtesyBus=" + request['courtesyBusFlag']) : ""))
	.append((request.containsKey('loanVehicleFlag') ? ("&Loan=" + request['loanVehicleFlag']) : ""))
	.append("&FrnCID=" + URLEncoder.encode(flowVars['myToyotaId'], "UTF-8"))
	.append(getCustomerTypeAndBusinessName(request['customerType'], request['businessName']))
	.append((request.containsKey('contactDetails') && request.contactDetails.containsKey('firstName')) ? ("&FirstName=" + URLEncoder.encode(request.contactDetails['firstName'], "UTF-8")) : "")
	.append((request.containsKey('contactDetails') && request.contactDetails.containsKey('lastName')) ? ("&LastName=" + URLEncoder.encode(request.contactDetails['lastName'], "UTF-8")) : "")
	.append((request.containsKey('contactDetails') && request.contactDetails.containsKey('phone')) ? ("&Phone=" + URLEncoder.encode(request.contactDetails['phone'], "UTF-8")) : "")
	.append((request.containsKey('contactDetails') && request.contactDetails.containsKey('email')) ? ("&Email=" + URLEncoder.encode(request.contactDetails['email'], "UTF-8")) : "")

int getDaysSinceDayOne(String stringDate) {
	def firstDay = new GregorianCalendar(1900, Calendar.JANUARY, 1, 0, 0, 0).time
	def tuneDateOffset = 2415022
	return (new Date().parse("yyyy-MM-dd", stringDate) - firstDay) + tuneDateOffset
}

int getSecSinceMidnight(String stringTime) {
	def parsedTime = new Date().parse("HH:mm", stringTime)
	((parsedTime.hours + (parsedTime.minutes / 60)) * 3600).intValue()
}

String getConfType(String confTypeIn) {
	if (confTypeIn == 'email')
		return '&ConfType=E'
	else if (confTypeIn == 'SMS')
		return '&ConfType=S'
	else
		return ''
}

String extractUpsells(Map request) {
	if (request.upsells) {
		upsellsString = ''
		request.upsells.each { Map upsell ->
			upsellsString += upsell.id+","+upsell.price+","
		}
		return '&UpSells='+URLEncoder.encode(upsellsString[0..-2].replaceAll("POA", "0"), "UTF-8")
	} else
		return ''
}

String getCustomerTypeAndBusinessName(String customerTypeIn, String businessName) {
    returnString = ''
    if (customerTypeIn == 'private')
        returnString = '&CustType=P'
    else if (customerTypeIn == 'business') {
        returnString = '&CustType=B'
        if (businessName)
            returnString = new StringBuilder()
                .append(returnString)
                .append('&BusinessName=' + URLEncoder.encode(businessName,"UTF-8"))
                .toString()
    }

    return returnString
}


// println postBody.toString()

return postBody.toString()
