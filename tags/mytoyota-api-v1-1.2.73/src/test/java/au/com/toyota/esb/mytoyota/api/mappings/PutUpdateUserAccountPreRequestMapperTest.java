package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

import javax.script.*;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

public class PutUpdateUserAccountPreRequestMapperTest extends AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;
	
	public PutUpdateUserAccountPreRequestMapperTest() throws ScriptException, SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/put-update-user-account-info-pre-request-mapper.groovy");
	}
	
	@Test
	public void testRequestMapperIncludeFirstLastName() throws Exception {
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(getClass().getResourceAsStream(
						"/in/put-update-user-account-info-from-salesforce-include-first-lastname-request.json")))
				.run().toString();
		System.out.println(result);
		
		// expected
		String expectedResult = IOUtils.toString(getClass().getResourceAsStream(
				"/out/put-update-user-account-info-from-salesforce-include-first-lastname-response.json"));
		JSONAssert.assertEquals(expectedResult, result, true);
	}
	
	@Test
	public void testRequestMapperBlankFirstLastName() throws Exception {
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(getClass().getResourceAsStream(
						"/in/put-update-user-account-info-from-salesforce-request.json")))
				.run().toString();
		System.out.println(result);
		
		// expected
		String expectedResult = IOUtils.toString(getClass().getResourceAsStream(
				"/out/put-update-user-account-info-from-salesforce-response.json"));
		JSONAssert.assertEquals(expectedResult, result, true);
	}
	
	@Test
	public void testRequestMapperBlankEmailAddress() throws Exception {
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(getClass().getResourceAsStream(
						"/in/put-update-user-account-info-from-salesforce-blank-email-request.json")))
				.run().toString();
		System.out.println(result);
		
		// expected
		String expectedResult = IOUtils.toString(getClass().getResourceAsStream(
				"/out/put-update-user-account-info-from-salesforce-include-first-lastname-response.json"));
		JSONAssert.assertEquals(expectedResult, result, true);
	}
}
