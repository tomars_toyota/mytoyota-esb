import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper


JsonSlurper js = new JsonSlurper()

def recallsResponse = js.parseText(payload)
response = [:]

response.batchNumber = recallsResponse.batchNumber
response.compliancePlate = recallsResponse.compliancePlate
response.engineNumber = recallsResponse.engineNumber
response.ignitionKeyNumber = recallsResponse.ignitionKeyNumber
response.materialNumber = recallsResponse.materialNumber
response.production = recallsResponse.production
response.vin = recallsResponse.vin
recallsList = []

if(recallsResponse.outstandingRecalls) {

	recallsResponse.outstandingRecalls.groupBy({it.campaignCode}).each{
	    campaignItem = [:]
	    
	    campaignItem['lastUpdateTimstamp'] = it.value[0]['lastUpdateTimstamp']
	    campaignItem['campaignCode'] = it.key
	    campaignItem['campaignDescription'] = it.value[0]['campaignDescription']
	    
	    campaignItem['campaignItems'] = it.value
	    campaignItem['campaignItems'] = campaignItem['campaignItems'].each{
	        it.remove('lastUpdateTimstamp')
	        it.remove('campaignCode')
	        it.remove('campaignDescription')
	    }
	    recallsList.add(campaignItem)
	}
}

response.outstandingRecalls = recallsList



return prettyPrint(toJson(response))
