package au.com.toyota.esb.mytoyota.api.munit;

import static org.junit.Assert.assertEquals;
import static org.mule.munit.common.mocking.Attribute.attribute;

import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.module.apikit.exception.BadRequestException;
import org.mule.module.apikit.exception.MethodNotAllowedException;
import org.mule.module.apikit.exception.NotAcceptableException;
import org.mule.module.apikit.exception.NotFoundException;
import org.mule.module.apikit.exception.UnsupportedMediaTypeException;
import org.mule.munit.runner.functional.FunctionalMunitSuite;

import au.com.toyota.esb.mytoyota.api.exception.UnauthorisedException;

public class StatusCodesTestingHelper extends FunctionalMunitSuite {

	public void runFailureTest(final int statusCode, String httpConnectorName, String flowName) throws Exception {
		runFailureTest(statusCode, httpConnectorName, flowName, null);
	}

	public void runFailureTest(final int statusCode, String httpConnectorName, String flowName, MuleEvent event)
			throws Exception {
		if(httpConnectorName != null)
			whenMessageProcessor("request").ofNamespace("http")
					.withAttributes(attribute("name").ofNamespace("doc").withValue(httpConnectorName))
					.thenApply(new MockResponseTransformer() {
						@Override
						public MuleMessage transform(MuleMessage originalMessage) {
							MuleMessage newMessage = null;
	
							try {
								newMessage = originalMessage.createInboundMessage();
								newMessage.setProperty("http.status", statusCode, PropertyScope.INBOUND);
							} catch (Exception e) {
								// // catch exception statements
							}
	
							return newMessage;
						}
					});

		try {
			if (event == null)
				event = testEvent("");
			// Invoke the flow
			runFlow(flowName, event);

			// validating exception thrown
			assertEquals(true, false);

		} catch (Exception e) {

			switch (statusCode) {
			case 404:
				assertEquals("status code 404 did not throw org.mule.module.apikit.exception.NotFoundException",
						e.getCause().getCause().getCause().getCause() instanceof NotFoundException, true);
				break;
			case 405:
				assertEquals("status code 405 did not throw org.mule.module.apikit.exception.MethodNotAllowedException",
						e.getCause().getCause().getCause().getCause() instanceof MethodNotAllowedException, true);
				break;
			case 415:
				assertEquals("status code 415 did not throw org.mule.module.apikit.exception.UnsupportedMediaTypeException",
						e.getCause().getCause().getCause().getCause() instanceof UnsupportedMediaTypeException, true);
				break;
			case 406:
				assertEquals("status code 406 did not throw org.mule.module.apikit.exception.NotAcceptableException",
						e.getCause().getCause().getCause().getCause() instanceof NotAcceptableException, true);
				break;
			case 400:
				assertEquals("status code 400 did not throw org.mule.module.apikit.exception.BadRequestException",
						e.getCause().getCause().getCause().getCause() instanceof BadRequestException, true);
				break;
			case 401:
				assertEquals("status code 401 did not throw au.com.toyota.esb.mytoyota.api.exception.UnauthorisedException",
						e.getCause().getCause().getCause().getCause() instanceof UnauthorisedException, true);
				break;
			default:
				assertEquals("status code 401 did not throw java.lang.Exception",
						e.getCause().getCause().getCause().getCause() instanceof Exception, true);
				break;
			}
		}
	}
}
