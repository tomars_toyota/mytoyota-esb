package au.com.toyota.esb.mytoyota.api;

import static org.junit.Assert.assertEquals;
import static org.mule.munit.common.mocking.Attribute.attribute;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.util.IOUtils;

import com.jayway.jsonpath.JsonPath;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;

//@Ignore
public class GetOrderByOrderIdMunitTest extends FunctionalMunitSuite {

	@Override
	protected String getConfigResources() {
		return StringUtils.join(
				new String[] { "get-order-by-order-id.xml", 
							   "get-orders-technical-adapter.xml", 
							   "config.xml" }, " ");
	}

	@Test
	public void testSuccessfulRequest() throws Exception {

		final String orderId = "1234";
		String requestPath = "/api/myToyota/v1/orders/" + orderId;

		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Vehicle API GET orders by order id"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							newMessage.setProperty("orderId", orderId, PropertyScope.INVOCATION);
							newMessage.setPayload(getClass().getResourceAsStream("/in/get-orders-by-order-id-vehicle-api-response.json"));
						} catch (Exception e) {
							// catch exception statements
						}

						return newMessage;
					}
				});

		MuleEvent event = testEvent(requestPath);

		MuleEvent output = runFlow("get:/orders/{orderId}:myToyota-config", event);

		System.out.println("***** RESPONSE: "+output.getMessage().getPayloadAsString());

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-orders-by-order-id.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-orders-by-order-id-technical-adapter.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("category")
				.withValue("esb.mytoyota-api-v1.get-orders-by-order-id-technical-adapter.response")).times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-orders-by-order-id.response"))
				.times(1);
	}

	@Test
	public void testFailuresStatusCodes() throws Exception {

		int[] statusCodes = new int[] { 404, 405, 415, 406, 401, 500 };

		String orderId = "1234";
		String requestPath = "/api/myToyota/v1/orders/" + orderId;
		MuleEvent event = testEvent(requestPath);
		event.setFlowVariable("orderId", orderId);

		for (int i = 0; i < statusCodes.length; i++) {
			new StatusCodesTestingHelper().runFailureTest(statusCodes[i], "Vehicle API GET orders by order id",
					"get:/orders/{orderId}:myToyota-config", event);
		}
	}
}
