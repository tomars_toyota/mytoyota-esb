import groovy.xml.MarkupBuilder
import java.text.SimpleDateFormat
import groovy.json.JsonSlurper

def writer = new StringWriter()
def xmlMarkup = new MarkupBuilder(writer)


order = new JsonSlurper().parseText(flowVars['orderResponse'])

postalAddress = order?.address?.postal

originalRequest = flowVars['requestPayload']



xmlMarkup.mkp.xmlDeclaration(version: "1.0", encoding: "UTF-8")
xmlMarkup.'sp:registerGuestTrackMyCar' (
	'xmlns:sp' : "http://soap.sforce.com/schemas/class/SP_UserRegWSTransactionStructure"
	) {
		'sp:request'{
			if(postalAddress) {
				'sp:Address' {
					if(postalAddress.suburb != null)
						'sp:cityorsuburb' (postalAddress.suburb)
					if(postalAddress.postcode != null)
						'sp:postcode' (postalAddress.postcode)
					if(postalAddress.state != null)
						'sp:state' (postalAddress.state)
					if(postalAddress.streetAddress != null)
						'sp:street' (postalAddress.streetAddress)
				}
			}
			if(originalRequest?.isMarketingOptIn != null)
				'sp:marketingPreference' (originalRequest.isMarketingOptIn)
			if(sessionVars['myToyotaId'])
				'sp:myToyotaId' (sessionVars['myToyotaId'])
			if(order?.dateOfBirth || 
					(order?.email != null) ||
					(originalRequest?.firstName != null) ||
					(order?.landline != null) ||
					(originalRequest?.lastName != null) ||
					(order?.mobile != null) ||
					(order?.title != null) ||
					(sessionVars['encryptedPassword'])) {
				'sp:ownerDetails'{
					'sp:dob' ('20/02/1990') 								//TODO: remove hardcoding
					if(order?.dateOfBirth != null) {
						if(order.dateOfBirth == '') 
							'sp:dob' (order.dateOfBirth)
						else {
							'sp:dob' (new SimpleDateFormat("dd/MM/yyyy").format(new Date().parse('yyyy-MM-dd', order.dateOfBirth)))
						}
					}
					if(order?.email != null)
						'sp:email' (order.email)
					if(originalRequest?.firstName != null)
						'sp:firstName' (originalRequest.firstName)
					if(originalRequest?.lastName != null)
						'sp:lastName' (originalRequest.lastName)
					if(sessionVars['encryptedPassword'])
						'sp:Password' (sessionVars['encryptedPassword'])
					if(order?.mobile != null)
						'sp:personmobilephone' (order.mobile)
					'sp:phone' ('0312341234') 								//TODO: remove hardcoding
					if(order?.landline != null)
						'sp:phone' (order.landline)
					if(order?.title != null)
						'sp:salutation' (order.title)
				}
			}
			if(order?.email)
				'sp:OwnersPortalUserId' (order.email)
			'sp:trackMyCar'{
				if(order?.guestOrder?.batchId != null)
					'sp:batchId' (order.guestOrder.batchId)
				if(order?.guestOrder?.cosi != null)
					'sp:cosi' (order.guestOrder.cosi)
				'sp:status' ('10') 											//TODO: remove hardcoding
				// if(order?.guestOrder?.statusUpdates[0]?.status != null)
				// 	'sp:status' (order.guestOrder.statusUpdates[0].status)
			}
		}	
	}
//println '__________ payload to salesforce: ' + writer.toString()
return writer.toString()



// <sp:request>
//    <sp:Address>
//       <sp:cityorsuburb>?</sp:cityorsuburb>
//       <sp:postcode>?</sp:postcode>
//       <sp:state>?</sp:state>
//       <sp:street>?</sp:street>
//    </sp:Address>
//    <sp:marketingPreference>?</sp:marketingPreference>
//    <sp:myToyotaId>?</sp:myToyotaId>
//    <sp:ownerDetails>
//       <sp:dob>?</sp:dob>
//       <sp:email>?</sp:email>
//       <sp:firstName>?</sp:firstName>
//       <sp:lastName>?</sp:lastName>
//       <sp:Password>?</sp:Password>
//       <sp:personmobilephone>?</sp:personmobilephone>
//       <sp:phone>?</sp:phone>
//       <sp:salutation>?</sp:salutation>
//    </sp:ownerDetails>
//    <sp:OwnersPortalUserId>?</sp:OwnersPortalUserId>
//    <sp:trackMyCar>
//       <sp:batchId>?</sp:batchId>
//       <sp:cosi>?</sp:cosi>
//       <sp:status>?</sp:status>
//    </sp:trackMyCar>
// </sp:request>