import groovy.json.JsonSlurper
import static groovy.json.JsonOutput.*

def removeNulls(col) {
    if (col instanceof List) 
        return col.findAll { it }.collect { removeNulls(it) }
    else if (col instanceof Map) 
        return col.findAll { it.value }.collectEntries { [(it.key): removeNulls(it.value) ] }
    else return col
}

def mapService(service) {
	mappedService = [:]
	mappedService.bookingId = service.bookingId
	mappedService.dropOffDate = service.dropOffDate
	mappedService.dropOffTime = service.dropOffTime
	mappedService.pickUpDate = service.pickUpDate
	mappedService.pickUpTime = service.pickUpTime 
	mappedService.status = service.status
	mappedService.notes = service.notes
	mappedService.alertType = service.confirmationType
	mappedService.totalServicePrice = service.totalServicePrice
	mappedService.waitAtDealerFlag = service.waitAtDealerFlag
	mappedService.expressServiceFlag = service.expressServiceFlag
	mappedService.courtesyBusFlag = service.courtesyBusFlag
	mappedService.loanVehicleFlag = service.loanVehicleFlag

	mappedService.dealerDetails = service.dealer

	//replace dmsId and branchCode with dmsID and branchId
	mappedService.dealerDetails.dmsId = service.dealer.dmsID
	mappedService.dealerDetails.branchCode = service.dealer.branchId
	mappedService.dealerDetails.remove('dmsID')
	mappedService.dealerDetails.remove('branchId')

	mappedService.contactDetail = service.contactDetail
	mappedService.vehicle = service.vehicle
	mappedService.serviceOperation = service.serviceOperation
	mappedService.vehicleOdo = service.odometer

	return mappedService
}

//println '___________ payload: '+payload
def myKitResponse = new JsonSlurper().parseText(payload)

def response = [:]

if(myKitResponse instanceof Map) {
	response = mapService(myKitResponse)
} else if(myKitResponse instanceof List) {
	
	servicesList = []

	myKitResponse.each { service ->
		servicesList.add(mapService(service))
	}
	response.services = servicesList
}

return prettyPrint(toJson(removeNulls(removeNulls(response))))
