package au.com.toyota.esb.mytoyota.api;

import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.mockito.Mockito.verify;
import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.ArrayList;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.mockito.Matchers;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.cache.InvalidatableCachingStrategy;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;

//@Ignore
public class GetUsersMeOrdersMunitTest extends FunctionalMunitSuite {

	@Override
	protected String getConfigResources() {
		return StringUtils.join(
				new String[] { "get-users-me-orders.xml", "get-users-me-orders-technical-adapter.xml", "config.xml" },
				" ");
	}

	@Test
	public void testSuccessfulRequest() throws Exception {

		// Invalidate Cache to ensure full flow executes
		InvalidatableCachingStrategy getUsersMeOrdersCache = muleContext.getRegistry().<InvalidatableCachingStrategy>get("Users_Me_Orders_Caching_Strategy");
		getUsersMeOrdersCache.invalidate();
		
		String expectedResponse = IOUtils
				.toString(getClass().getResourceAsStream("/out/get-users-me-orders-api-response.json"));

		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Get Linked COSIs from myKIT"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						try {
							newMessage = originalMessage.createInboundMessage();

							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);

							newMessage.setPayload(
									getClass().getResourceAsStream("/in/get-users-me-orders-mykit-response.json"));
						} catch (Exception e) {
							// catch exception statements
						}

						return newMessage;
					}
				});

		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Vehicle API GET orders by order id"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							int foreachCounter = originalMessage.getProperty("foreachCounter",
									PropertyScope.INVOCATION);

							// System.out.println("*****
							// newMessage.foreachCounter: "+foreachCounter);

							// Change the Vehicle API Stub message for each of
							// the two loop iterations
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);

							if (foreachCounter == 1) {
								newMessage.setPayload(getClass()
										.getResourceAsStream("/in/get-order-vehicle-api-0007496761-response.json"));
							} else {
								newMessage.setPayload(getClass()
										.getResourceAsStream("/in/get-order-vehicle-api-0007518430-response.json"));
							}

						} catch (Exception e) {
							// catch exception statements
						}

						return newMessage;
					}
				});

		String httpRequestPath = "/api/myToyota/v1/users/me/orders";

		MuleMessage msg = muleMessageWithPayload(null);
		msg.setProperty("http.request.path", httpRequestPath, PropertyScope.INBOUND);

		MuleEvent event = testEvent("");
		event.setMessage(msg);

		MuleEvent output = runFlow("get:/users/me/orders:myToyota-config", event);

		// String payload = output.getMessage().getPayloadAsString();
		// System.out.println("**** Response: " + payload);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-users-me-orders.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-users-me-orders.response"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-users-me-orders-technical-adapter.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-users-me-orders-technical-adapter.response"))
				.times(1);

		assertJsonEquals(expectedResponse, output.getMessage().getPayloadAsString());
	}

	@Test
	public void testVehicleAPIOrderNotFoundRequest() throws Exception {

		// Invalidate Cache to ensure full flow executes
		InvalidatableCachingStrategy getUsersMeOrdersCache = muleContext.getRegistry().<InvalidatableCachingStrategy>get("Users_Me_Orders_Caching_Strategy");
		getUsersMeOrdersCache.invalidate();
		
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Get Linked COSIs from myKIT"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						try {
							newMessage = originalMessage.createInboundMessage();

							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);

							newMessage.setPayload(
									getClass().getResourceAsStream("/in/get-users-me-orders-mykit-response.json"));
						} catch (Exception e) {
							// catch exception statements
						}

						return newMessage;
					}
				});

		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Vehicle API GET orders by order id"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 404, PropertyScope.INBOUND);
							newMessage.setPayload("{\"message\": \"Resource not found\"}");

						} catch (Exception e) {
							// catch exception statements
						}

						return newMessage;
					}
				});

		String httpRequestPath = "/api/myToyota/v1/users/me/orders";

		MuleMessage msg = muleMessageWithPayload(null);
		msg.setProperty("http.request.path", httpRequestPath, PropertyScope.INBOUND);

		MuleEvent event = testEvent("");
		event.setMessage(msg);

		MuleEvent output = runFlow("get:/users/me/orders:myToyota-config", event);

		// String payload = output.getMessage().getPayloadAsString();
		// System.out.println("**** Response: " + payload);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-users-me-orders.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-users-me-orders.response"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-users-me-orders-technical-adapter.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-users-me-orders-technical-adapter.response"))
				.times(1);

		assertJsonEquals("[]", output.getMessage().getPayloadAsString());
	}

	@Test
	public void testMyKITReturnEmptyCOSIListRequest() throws Exception {

		// Invalidate Cache to ensure full flow executes
		InvalidatableCachingStrategy getUsersMeOrdersCache = muleContext.getRegistry().<InvalidatableCachingStrategy>get("Users_Me_Orders_Caching_Strategy");
		getUsersMeOrdersCache.invalidate();
		
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Get Linked COSIs from myKIT"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							newMessage.setPayload("[]");
						} catch (Exception e) {
							// catch exception statements
						}

						return newMessage;
					}
				});

		// Mock Vehicle API although this should not be invoked for this flow
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Vehicle API GET orders by order id"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 404, PropertyScope.INBOUND);
							newMessage.setPayload("{\"message\": \"Resource not found\"}");

						} catch (Exception e) {
							// catch exception statements
						}

						return newMessage;
					}
				});

		String httpRequestPath = "/api/myToyota/v1/users/me/orders";

		MuleMessage msg = muleMessageWithPayload(null);
		msg.setProperty("http.request.path", httpRequestPath, PropertyScope.INBOUND);

		MuleEvent event = testEvent("");
		event.setMessage(msg);

		MuleEvent output = runFlow("get:/users/me/orders:myToyota-config", event);

		// String payload = output.getMessage().getPayloadAsString();
		// System.out.println("**** Response: " + payload);

		// Verify that the Vehicle API was not called (i.e. For-Loop not
		// executed)
		verifyCallOfMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Vehicle API GET orders by order id"))
				.times(0);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-users-me-orders.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-users-me-orders.response"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-users-me-orders-technical-adapter.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-users-me-orders-technical-adapter.response"))
				.times(1);

		assertJsonEquals("[]", output.getMessage().getPayloadAsString());
	}

	@Test
	public void testFailuresStatusCodes() throws Exception {

		int[] statusCodes = new int[] { 404, 405, 415, 406, 401, 500 };

		for (int i = 0; i < statusCodes.length; i++) {
			new StatusCodesTestingHelper().runFailureTest(statusCodes[i], "Get Linked COSIs from myKIT",
					"get:/users/me/orders:myToyota-config");
		}
	}
}
