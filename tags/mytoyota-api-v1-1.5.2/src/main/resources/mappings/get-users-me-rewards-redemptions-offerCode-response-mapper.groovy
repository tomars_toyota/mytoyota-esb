import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper
import au.com.toyota.esb.mytoyota.api.util.GroovyHelper
import java.text.*

def rewardResponse = null 
if (payload) {
	rewardResponse = new JsonSlurper().parseText(payload)
}

def coupon = rewardResponse?.coupons?.first()
def response = [:]
response.status = coupon?.dailyClaimedStatus
response.lastClaimedDate = coupon?.lastRedemptionDate

return prettyPrint(toJson(GroovyHelper.removeNulls(response, true)))