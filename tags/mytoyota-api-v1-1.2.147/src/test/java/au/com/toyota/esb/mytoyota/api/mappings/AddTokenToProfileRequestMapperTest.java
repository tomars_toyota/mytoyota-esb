package au.com.toyota.esb.mytoyota.api.mappings;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import javax.script.ScriptException;

import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.xml.sax.SAXException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

//@Ignore
public class AddTokenToProfileRequestMapperTest extends AbstractMuleContextTestCase {

	private ScriptRunner scriptRunner;
	
	//expiry:2018-04-07 13:44:07
	private String token1 = "eyJjdHkiOiJKV1QiLCJlbmMiOiJBMjU2R0NNIiwiYWxnIjoiZGlyIn0..4MpwkEeiRMY8Lu-x."
			+ "Q04GObcTeL2wtrFcVrm5mvUP9WRUCvNJKKiASzutcfVdLhgGYpjVLz-LNe2Qd-7cauTuuSTkGaXXBWdX1IbCeLyfI"
			+ "uQZG7ouRdqX4slNf1A1uFZvhxl0Oj_NrQjKdthetePQxJzfnTODs8QYYMdeLM9nSImtkAwT-KC9Arzz-qhoeXaX3Dp"
			+ "0BD93-KT8Yb6y8GJaxCQUYcYyGcRNgbw6QkPg81dk4UyTzouC9S9tnKeI93UCcW4AHjWVKqqC9G0b0zAaHV9cAYMJb8"
			+ "0l4emep1u9sl5E-7SnJtflDitduCe47pSFuAlxqNZvC19pma_2cr8ODOcFUciy9RKza15rWbHnEL5YKvVmxXQz13Rf"
			+ "zY9bIquL9NS1xQ.aMuOugYFuUMgzkIq2fJ48w";
	
	//expiry:2018-04-07 13:45:22
	private String token2= "eyJjdHkiOiJKV1QiLCJlbmMiOiJBMjU2R0NNIiwiYWxnIjoiZGlyIn0..nYOODhBxsbDYDn8M.7eIP"
			+ "2wyIe4EiguSmbPt06xqm-41BfFo_oCtfc7-7MrIskOfLwkZWZQ4eMzKQGATzVcbtIxFXXfwCAkD85QanKvyLUCSmyDJK"
			+ "kaCJGVpNhgR3DuXrBqKaV8Pr52kj7oSKDuYTD65B5KKEAHjarCGrxiphC4-zZym-FBs7HFaplhksDMeM98vUkRJh7JJKv"
			+ "iXJ7HBwDqfWlVPpxPIHcTBMpwbqX0YN0S9_XC-1aLrSWAVIz77ja4xd7QGotV8xMumsM2HJ-6BtVMaNWrpaGJvBDrO5D3"
			+ "mh07DCTsLa8s01IfB7MSRKv7qsZaqkhtWbRFamyICdWhQ1C6aglYi6xE70B29N5A7BfnP-uLN2eivcy-uj7vGrYpUo_A."
			+ "EEBLcqG4M8udP05iRVV2ig";
	
	//expiry:2018-04-07 13:45:46
	private String token3 = "eyJjdHkiOiJKV1QiLCJlbmMiOiJBMjU2R0NNIiwiYWxnIjoiZGlyIn0..eb1Ki1WZ3WCUU_0K.LfDl"
			+ "kla0B1q6ycanHDI1ToZL5p4jZ2j2z0BTaIv2mOPwyQPNoMdf89BR9O_MUgIrQl2dy6F3Z7Q4q8gy0dKnnGZbkweEt0v"
			+ "fh99zhlmFtc5Kt0v0FGlrfSq7j0urePhK4L9xhzIr_h7fISnfMaFsId-xbq2NyvFupqJGldKmjiazaTIMiqR5az4zk80M"
			+ "ml68V_ISCFTVWRjTNUe-e_d_7MYoeNtGY_mD_65pJob4qu5hRJccKX1za5667gnKUhRtG2yHgrgGE9Ia2b7dT8PGg-AJF"
			+ "H-ZgtRdYprsm_ZV05Z-3_6vjqteDxk3wH-OZtkWRbiyW_IyaBxoA_ZLpQ6wW0vbHcPZSHCptPurF5VoboNETNX_93PnDg."
			+ "nBtDL4YSffVoUZx3LMidxw";

	//expiry:2018-04-07 15:39:58
	private String newToken = "eyJjdHkiOiJKV1QiLCJlbmMiOiJBMjU2R0NNIiwiYWxnIjoiZGlyIn0..jKNejxWNio8zjIYQ.swzR"
			+ "KNfM_mg4Ov9wcc6e2wk-Aob4dE7mej5Qx59UEt9FVrriWw1E-YCbl4z56xbf1a13D-ikeqUJen7GvC4NpIodTd_xQPomLbB"
			+ "ZQ2IxmS2AZyjM-e4WzFd9gK_pMF6Mg-ag_7u1TTHguVnTQmfIyW48yV4oSMx4_EyMzLNp7HOoSjnt0_ewcfjv7yRPbor_ja"
			+ "Lp3z19myzfPRsO6zVlde5gO8qjnKQW3m_xKyivc1QECbvALa_Xxy2nowJtOhOKtqpcBBp_DWb_GoB2-0pRsIZ3tyEPPjnM5"
			+ "cNtnTUYMQqXAcfBlT1JHtJiDRzLR7P1gl7l8PuEH3YowaTYR7eF0XN0oI627DwlJzC1deiSRX7heI4MHzFEnA.c9S43haoX"
			+ "msG4PvfDxtyLQ";
	
	public AddTokenToProfileRequestMapperTest() throws ScriptException, SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/add-token-to-profile.groovy");
	}
	
	private HashMap<String, Object> getUserProfileMap() {
		
		ArrayList<String> myToyotaSessionToken = new ArrayList<String>();
		myToyotaSessionToken.add(token1);
		myToyotaSessionToken.add(token2);
		myToyotaSessionToken.add(token3);
		
		HashMap<String, Object> data = new HashMap<String, Object>();
		data.put("myToyotaSessionToken", myToyotaSessionToken);
		
		HashMap<String, Object> userProfileMap = new HashMap<String, Object>();
		userProfileMap.put("data", data);
		return userProfileMap; 
	}
	
	private HashMap<String, Object> getNewAPISessionToken() {
		HashMap<String, Object> newAPISessionToken = new HashMap<String, Object>();
		newAPISessionToken.put("tokenType", "myToyotaSessionToken");
		newAPISessionToken.put("value", newToken);
		return newAPISessionToken;
	}

	@Test
	public void testAddTokenRequestMapper() throws Exception {
		
		@SuppressWarnings("unchecked")
		HashMap<String, Object> result = (HashMap<String, Object>)ScriptRunBuilder
				.runner(scriptRunner)
				.payload("")
				.flowVar("userProfileMap", getUserProfileMap())
				.flowVar("APISessionToken", getNewAPISessionToken())
				.binding("maxloggedInSessions", "5")
				.binding("jwtSecretKey", "PxpRI_jZNkP_eIgmr_gApbM")
				.run();
		
		System.out.println("________result: "+result);
		
		
		ArrayList<String> expectedMyToyotaSessionToken = new ArrayList<String>();
		
		//existing tokens
		expectedMyToyotaSessionToken.add(token1);
		expectedMyToyotaSessionToken.add(token2);
		expectedMyToyotaSessionToken.add(token3);

		//new token
		expectedMyToyotaSessionToken.add(newToken);
		
		HashMap<String, Object> targetData = new HashMap<String, Object>();
		targetData.put("myToyotaSessionToken", expectedMyToyotaSessionToken);
		
		HashMap<String, Object> expected = new HashMap<>();
		expected.put("data", targetData);
		
		assertTrue(expected.equals(result));
	}
	
	@Test
	public void testReplaceTokenRequestMapper() throws Exception {
		
		@SuppressWarnings("unchecked")
		HashMap<String, Object> result = (HashMap<String, Object>)ScriptRunBuilder
				.runner(scriptRunner)
				.payload("")
				.flowVar("userProfileMap", getUserProfileMap())
				.flowVar("APISessionToken", getNewAPISessionToken())
				.binding("maxloggedInSessions", "3")
				.binding("jwtSecretKey", "PxpRI_jZNkP_eIgmr_gApbM")
				.run();
		
		System.out.println("________result: "+result);
		
		
		ArrayList<String> expectedMyToyotaSessionToken = new ArrayList<String>();
		
		//existing tokens
		expectedMyToyotaSessionToken.add(token2);
		expectedMyToyotaSessionToken.add(token3);
		//new token
		expectedMyToyotaSessionToken.add(newToken);
		
		HashMap<String, Object> targetData = new HashMap<String, Object>();
		targetData.put("myToyotaSessionToken", expectedMyToyotaSessionToken);
		
		HashMap<String, Object> expected = new HashMap<>();
		expected.put("data", targetData);
		
		assertTrue(expected.equals(result));
	}
	
	@Test
	public void testReplaceTokenRecursiveRequestMapper() throws Exception {
		
		@SuppressWarnings("unchecked")
		HashMap<String, Object> result = (HashMap<String, Object>)ScriptRunBuilder
				.runner(scriptRunner)
				.payload("")
				.flowVar("userProfileMap", getUserProfileMap())
				.flowVar("APISessionToken", getNewAPISessionToken())
				.binding("maxloggedInSessions", "1")
				.binding("jwtSecretKey", "PxpRI_jZNkP_eIgmr_gApbM")
				.run();
		
		System.out.println("________result: "+result);
		
		
		ArrayList<String> expectedMyToyotaSessionToken = new ArrayList<String>();
		
		expectedMyToyotaSessionToken.add(newToken);
		
		HashMap<String, Object> targetData = new HashMap<String, Object>();
		targetData.put("myToyotaSessionToken", expectedMyToyotaSessionToken);
		
		HashMap<String, Object> expected = new HashMap<>();
		expected.put("data", targetData);
		
		assertTrue(expected.equals(result));
	}
}
