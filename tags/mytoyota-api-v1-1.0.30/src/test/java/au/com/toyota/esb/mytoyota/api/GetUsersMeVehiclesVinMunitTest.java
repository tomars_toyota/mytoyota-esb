package au.com.toyota.esb.mytoyota.api;

import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.mule.munit.common.mocking.Attribute.attribute;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.munit.runner.functional.FunctionalMunitSuite;


//@Ignore
public class GetUsersMeVehiclesVinMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"get-users-me-vehicles-vin.xml",
				"get-users-me-vehicles-vin-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulRequest() throws Exception {

		String apiResponse = IOUtils.toString(getClass().getResourceAsStream("/in/get-users-me-vehicles-vin-mykit-response.json"));
		String expectedResponse = IOUtils.toString(getClass().getResourceAsStream("/out/get-users-me-vehicles-vin-api-response.json"));
				
		whenMessageProcessor("request").ofNamespace("http")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("My Toyota App API GET Retrieve User Vehicle by VIN"))
			.thenReturn(muleMessageWithPayload(apiResponse));

		MuleEvent event = testEvent("");

		MuleEvent output = runFlow("get:/users/me/vehicles/{vin}:myToyota-config", event);

		String payload = output.getMessage().getPayloadAsString();
		System.out.println(payload);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-user-vehicles-vin.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-user-vehicles-vin.response"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-user-vehicles-vin-technical-adapter.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-user-vehicles-vin-technical-adapter.response"))
				.times(1);

		assertJsonEquals(expectedResponse, output.getMessage().getPayloadAsString());
	}
}
