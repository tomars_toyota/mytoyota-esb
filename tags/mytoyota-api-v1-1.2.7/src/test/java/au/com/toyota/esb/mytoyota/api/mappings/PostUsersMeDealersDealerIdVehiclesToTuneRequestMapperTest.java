package au.com.toyota.esb.mytoyota.api.mappings;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

import javax.script.*;
import org.junit.Assert;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.xml.sax.SAXException;

public class PostUsersMeDealersDealerIdVehiclesToTuneRequestMapperTest extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;

	public PostUsersMeDealersDealerIdVehiclesToTuneRequestMapperTest() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner
				.createScriptRunner("/mappings/post-users-me-dealers-dealerId-vehicles-request-mapper.groovy");
	}
	
	
	//@Test
	public void testSuccessRequestMapper() throws Exception {
		String expectedResult = "SiteID=61.10169.10169011116&Mode=CV&newFirstName=John&newLastName=Smith&newEmail=jsmith%40mail.com.au"
				+ "&newMobile=0412341234&newFrnCid=MYT-000000cc"
				+ "&newMake=TOY&newModel=TOYCAM&newRego=ABC123&newYear=2016&newOdo=10000";
		
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(
						getClass().getResourceAsStream("/in/post-users-me-dealers-dealerId-vehicles-api-request.json")))
				.flowVar("dmsID","61.10169.10169011116")
				.flowVar("myToyotaId","MYT-000000cc")
				.run().toString();

		System.out.println(result);

		Assert.assertEquals(expectedResult, result);
	}
	
	//@Test
	public void testSuccessZeroOdoRequestMapper() throws Exception {
		String expectedResult = "SiteID=61.10169.10169011116&Mode=CV&newFirstName=John&newLastName=Smith&newEmail=jsmith%40mail.com.au"
				+ "&newMobile=0412341234&newFrnCid=MYT-000000cc"
				+ "&newMake=TOY&newModel=TOYCAM&newRego=ABC123&newYear=2016&newOdo=1";
		
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(
						getClass().getResourceAsStream("/in/post-users-me-dealers-dealerId-vehicles-api-request-zero-odo.json")))
				.flowVar("dmsID","61.10169.10169011116")
				.flowVar("myToyotaId","MYT-000000cc")
				.run().toString();

		System.out.println(result);

		Assert.assertEquals(expectedResult, result);
	}
	
	//@Test
	public void testSuccessNullVehicleYearRequestMapper() throws Exception {
		String expectedResult = "SiteID=61.10169.10169011116&Mode=CV&newFirstName=John&newLastName=Smith&newEmail=jsmith%40mail.com.au"
				+ "&newMobile=0412341234&newFrnCid=MYT-000000cc"
				+ "&newMake=TOY&newModel=TOYCAM&newRego=ABC123&newYear=2010&newOdo=1";
		
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(
						getClass().getResourceAsStream("/in/post-users-me-dealers-dealerId-vehicles-api-request-null-year.json")))
				.flowVar("dmsID","61.10169.10169011116")
				.flowVar("myToyotaId","MYT-000000cc")
				.run().toString();

		System.out.println(result);

		Assert.assertEquals(expectedResult, result);
	}
	
	//@Test
	public void testSuccessBlankVehicleYearRequestMapper() throws Exception {
		String expectedResult = "SiteID=61.10169.10169011116&Mode=CV&newFirstName=John&newLastName=Smith&newEmail=jsmith%40mail.com.au"
				+ "&newMobile=0412341234&newFrnCid=MYT-000000cc"
				+ "&newMake=TOY&newModel=TOYCAM&newRego=ABC123&newYear=2010&newOdo=1";
		
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(
						getClass().getResourceAsStream("/in/post-users-me-dealers-dealerId-vehicles-api-request-blank-year.json")))
				.flowVar("dmsID","61.10169.10169011116")
				.flowVar("myToyotaId","MYT-000000cc")
				.run().toString();

		System.out.println(result);

		Assert.assertEquals(expectedResult, result);
	}
	
	//@Test
	public void testSuccess0000VehicleYearRequestMapper() throws Exception {
		String expectedResult = "SiteID=61.10169.10169011116&Mode=CV&newFirstName=John&newLastName=Smith&newEmail=jsmith%40mail.com.au"
				+ "&newMobile=0412341234&newFrnCid=MYT-000000cc"
				+ "&newMake=TOY&newModel=TOYCAM&newRego=ABC123&newYear=2010&newOdo=1";
		
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(
						getClass().getResourceAsStream("/in/post-users-me-dealers-dealerId-vehicles-api-request-0000-year.json")))
				.flowVar("dmsID","61.10169.10169011116")
				.flowVar("myToyotaId","MYT-000000cc")
				.run().toString();

		System.out.println(result);

		Assert.assertEquals(expectedResult, result);
	}
}
