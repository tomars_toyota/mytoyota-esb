package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.util.Map;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

import javax.script.*;
import org.junit.Assert;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

import com.fasterxml.jackson.databind.ObjectMapper;

public class PostUsersMeDealersDealerIdVehiclesToTuneResponseMapperTest extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;

	public PostUsersMeDealersDealerIdVehiclesToTuneResponseMapperTest() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner
				.createScriptRunner("/mappings/post-users-me-dealers-dealerId-vehicles-response-mapper.groovy");
	}
	
	
	@SuppressWarnings("unchecked")
	@Test
	public void testResponseMapper() throws Exception {
		String expectedResult = IOUtils.toString(
				getClass().getResourceAsStream("/out/post-users-me-dealers-dealerId-vehicles-api-response.json"));
		String payload = (IOUtils.toString(
				getClass().getResourceAsStream("/in/post-users-me-dealers-dealerId-vehicles-tune-response.json")));
		
		Map<String, Object> result = (Map<String, Object>) ScriptRunBuilder
				.runner(scriptRunner)
				.payload(payload).run();
		Writer out = new StringWriter();
		new ObjectMapper().writeValue(out, result);
		System.out.println(out.toString());

		JSONAssert.assertEquals(expectedResult, out.toString(), true);;
	}
}
