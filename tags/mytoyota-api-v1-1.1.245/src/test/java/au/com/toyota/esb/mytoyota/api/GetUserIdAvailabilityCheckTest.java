package au.com.toyota.esb.mytoyota.api;

import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;

//@Ignore
public class GetUserIdAvailabilityCheckTest extends FunctionalMunitSuite {

	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] { "get-user-id-availability.xml",
				"get-user-id-availability-technical-adapter.xml", "config.xml" }, " ");
	}

	@Test
	public void testLoginIDAvailable() throws Exception {

		final String myKITResponse = IOUtils
				.toString(getClass().getResourceAsStream("/in/get-user-id-availability-check-mykit-response.json"));
		final String imProviderResponse = IOUtils.toString(
				getClass().getResourceAsStream("/in/get-user-id-availability-check-im-provider-response.json"));
		final String apiResponse = IOUtils
				.toString(getClass().getResourceAsStream("/out/get-user-id-availability-check-response.json"));

		String email = "available@email.com";
		String requestPath = "/api/myToyota/v1/users/check";

		// Mock IM Provider
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("IM API check loginID available"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							newMessage.setPayload(imProviderResponse);
						} catch (Exception e) {
							// catch exception statements
						}

						return newMessage;
					}
				});

		// Mock myKIT
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("myKIT API check email login available"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							newMessage.setPayload(myKITResponse);
						} catch (Exception e) {
							// catch exception statements
						}

						return newMessage;
					}
				});

		MuleMessage msg = muleMessageWithPayload(null);
		
		// Set query params
		Map<String, Object> props = new HashMap<String,Object>();
		props.put("loginID", email);
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
		
		// Set query path
		String queryString = requestPath + "?" + "loginID=" + email; 
		msg.setProperty("http.query.string", queryString, PropertyScope.INBOUND);
		
		MuleEvent event = testEvent(requestPath);
		event.setMessage(msg);
		event.setFlowVariable("loginID", email);
		
		MuleEvent output = runFlow("get:/users/check:myToyota-config", event);
		
		System.out.println(output.getMessage().getPayload());
		assertJsonEquals(apiResponse, output.getMessage().getPayloadAsString());
	}

	@Test
	public void testLoginIDNotAvailableMyKIT() throws Exception {

		final String myKITResponse = IOUtils.toString(
				getClass().getResourceAsStream("/in/get-user-id-availability-check-mykit-not-available-response.json"));
		final String imProviderResponse = IOUtils.toString(
				getClass().getResourceAsStream("/in/get-user-id-availability-check-im-provider-response.json"));
		final String apiResponse = IOUtils
				.toString(getClass().getResourceAsStream("/out/get-user-id-availability-check-not-available-response.json"));

		String email = "notavailable@email.com";
		String requestPath = "/api/myToyota/v1/users/check";

		// Mock IM Provider
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("IM API check loginID available"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							newMessage.setPayload(imProviderResponse);
						} catch (Exception e) {
							// catch exception statements
						}

						return newMessage;
					}
				});

		// Mock myKIT
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("myKIT API check email login available"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							newMessage.setPayload(myKITResponse);
						} catch (Exception e) {
							// catch exception statements
						}

						return newMessage;
					}
				});

		MuleMessage msg = muleMessageWithPayload(null);
		
		// Set query params
		Map<String, Object> props = new HashMap<String,Object>();
		props.put("loginID", email);
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
		
		// Set query path
		String queryString = requestPath + "?" + "loginID=" + email; 
		msg.setProperty("http.query.string", queryString, PropertyScope.INBOUND);
		
		MuleEvent event = testEvent(requestPath);
		event.setMessage(msg);
		event.setFlowVariable("loginID", email);

		MuleEvent output = runFlow("get:/users/check:myToyota-config", event);

		System.out.println(output.getMessage().getPayload());
		assertJsonEquals(apiResponse, output.getMessage().getPayloadAsString());
	}

	@Test
	public void testLoginIDNotAvailableIMProvider() throws Exception {

		final String myKITResponse = IOUtils
				.toString(getClass().getResourceAsStream("/in/get-user-id-availability-check-mykit-response.json"));
		final String imProviderResponse = IOUtils.toString(getClass()
				.getResourceAsStream("/in/get-user-id-availability-check-im-provider-not-available-response.json"));
		final String apiResponse = IOUtils
				.toString(getClass().getResourceAsStream("/out/get-user-id-availability-check-not-available-response.json"));

		String email = "notavailable@email.com";
		String requestPath = "/api/myToyota/v1/users/check";

		// Mock IM Provider
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("IM API check loginID available"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							newMessage.setPayload(imProviderResponse);
						} catch (Exception e) {
							// catch exception statements
						}

						return newMessage;
					}
				});

		// Mock myKIT
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("myKIT API check email login available"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							newMessage.setPayload(myKITResponse);
						} catch (Exception e) {
							// catch exception statements
						}

						return newMessage;
					}
				});

		MuleMessage msg = muleMessageWithPayload(null);
		
		// Set query params
		Map<String, Object> props = new HashMap<String,Object>();
		props.put("loginID", email);
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
		
		// Set query path
		String queryString = requestPath + "?" + "loginID=" + email; 
		msg.setProperty("http.query.string", queryString, PropertyScope.INBOUND);
		
		MuleEvent event = testEvent(requestPath);
		event.setMessage(msg);
		event.setFlowVariable("loginID", email);

		MuleEvent output = runFlow("get:/users/check:myToyota-config", event);

		System.out.println(output.getMessage().getPayload());

		assertJsonEquals(apiResponse, output.getMessage().getPayloadAsString());

		// Verify myKIT is not checked
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-user-id-availability-technical-adapter.mykit-backend.request"))
				.times(0);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-user-id-availability-technical-adapter.mykit-backend.response"))
				.times(0);
	}

//	@Test
//	public void testFailuresStatusCodes() throws Exception {
//
//		int[] statusCodes = new int[] { 404, 405, 415, 406, 401, 500 };
//
//		String email = "available@email.com";
//		String requestPath = "/api/myToyota/v1/users/check?loginID=" + email;
//		
//		MuleMessage msg = muleMessageWithPayload(null);
//		
//		// Set query params
//		Map<String, Object> props = new HashMap<String,Object>();
//		props.put("loginID", email);
//		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
//		
//		// Set query path
//		String queryString = requestPath + "?" + "loginID=" + email; 
//		msg.setProperty("http.query.string", queryString, PropertyScope.INBOUND);
//		
//		MuleEvent event = testEvent(requestPath);
//		event.setMessage(msg);
//		event.setFlowVariable("loginID", email);
//
//		for (int i = 0; i < statusCodes.length; i++) {
//			new StatusCodesTestingHelper().runFailureTest(statusCodes[i], "myKIT API check email login available",
//					"get:/users/check:myToyota-config", event);
//		}
//	}
}
