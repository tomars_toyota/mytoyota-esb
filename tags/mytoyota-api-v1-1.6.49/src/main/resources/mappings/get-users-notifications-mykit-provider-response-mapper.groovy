import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper

def notificationsListResponse = new JsonSlurper().parseText(payload)
def response = [:]

// notification list placeholder
response['notifications'] = []

// Build notification list
notificationsListResponse.each{ n -> 
	def notification = [:]
	notification.id = n?.id
	notification.code = n?.code
	notification.message = n?.message
	notification.category = n?.category
	notification.redirectable = n?.redirectable
	notification.closable = n?.closable
	notification.redirectUri = n?.redirectUri
	notification.expiryDate = n?.expiryDate
	notification.closedDate = n?.closedDate
	
	//remove nulls
	notification = notification.findAll {it.value != null && it.value != ""}

	// Add to notification list
	response['notifications'].add(notification)
}


//println ('_________________________'+toJson(response))

// Return
return prettyPrint(toJson(response))