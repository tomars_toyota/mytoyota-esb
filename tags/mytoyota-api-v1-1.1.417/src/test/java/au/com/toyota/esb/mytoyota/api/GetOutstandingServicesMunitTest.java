package au.com.toyota.esb.mytoyota.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mule.munit.common.mocking.Attribute.attribute;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;

import com.jayway.jsonpath.JsonPath;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;

@Ignore
//TODO: This seems like a duplicate test
public class GetOutstandingServicesMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"get-vehicle-details.xml",
				"get-vehicle-details-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulRequest() throws Exception {

		String vin = "6T153BK360X070769";
		String requestPath = "/api/myToyota/v1/vehicles/" + vin;

		whenMessageProcessor("request").ofNamespace("http")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("Vehicle API GET vehicle by vin"))
			.thenApply(new MockResponseTransformer() {
			@Override
			public MuleMessage transform(MuleMessage originalMessage) {
				MuleMessage newMessage = null;
				
				try {
					newMessage = originalMessage.createInboundMessage();
					
					newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
					
					newMessage.setPayload(IOUtils.toString(getClass().getResourceAsStream("/in/get-vehicle-details-response.json")));
				} catch (Exception e) {
					// catch exception statements
				}
				
				return newMessage;
			}
		});

		MuleEvent event = testEvent(requestPath);
		event.setFlowVariable("vin", vin);

		MuleEvent output = runFlow("get:/vehicles/{vin}:myToyota-config", event);

		System.out.println(output.getMessage().getPayload());

		assertEquals(vin, JsonPath.read(output.getMessage().getPayloadAsString(), "$.vin"));
		assertEquals("Camry L4 Altise 2.4L Petrol Automatic Sedan", JsonPath.read(output.getMessage().getPayloadAsString(), "$.vehicleDescription"));
	}
	
	@Test
	public void unsuccessfulRequest() throws Exception {

		String vin = "ABC123";
		String requestPath = "/api/myToyota/v1/vehicles/" + vin;

		whenMessageProcessor("request").ofNamespace("http")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("Vehicle API GET vehicle by vin"))
			.thenThrow(new org.mule.module.apikit.exception.NotFoundException(requestPath));
		
		MuleEvent event = testEvent(requestPath);
		event.setFlowVariable("vin", vin);

		try {
			runFlow("get:/vehicles/{vin}:myToyota-config", event);
		} catch (Exception e) {
			assertTrue(e.getCause() instanceof org.mule.module.apikit.exception.NotFoundException);
			assertEquals(requestPath, e.getCause().getMessage());
		}

	}
	
	@Test
	public void testFailuresStatusCodes() throws Exception {
		
		int[] statusCodes = new int[] { 404, 405, 415, 406, 401, 500 };	
		
		String vin = "ABC123";
		String requestPath = "/api/myToyota/v1/vehicles/" + vin;

		MuleEvent event = testEvent(requestPath);
		event.setFlowVariable("vin", vin);
		
		for (int i = 0; i < statusCodes.length; i++) {
			new StatusCodesTestingHelper().runFailureTest(statusCodes[i], "Vehicle API GET vehicle by vin",
					"get:/vehicles/{vin}:myToyota-config", event);
		}
	}
}
