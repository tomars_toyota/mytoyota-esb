import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper

JsonSlurper js = new JsonSlurper()
def timeSlotData = js.parseText(payload)

def response = [:]
response.availableDays = []

if(!timeSlotData) 
	return prettyPrint(toJson(response))

response.availableDays = timeSlotData

return prettyPrint(toJson(response))