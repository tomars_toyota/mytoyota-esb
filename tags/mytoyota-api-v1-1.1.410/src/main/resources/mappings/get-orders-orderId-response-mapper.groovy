import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper

def capitaliseNames(name) {
	newName = ''
	name.tokenize().each { newName += it.toLowerCase().capitalize() + ' '}
	return newName.trim()
}

order = new JsonSlurper().parseText(payload)
order.firstName = order.firstName ? capitaliseNames(order.firstName) : null 
order.lastName = order.lastName ? capitaliseNames(order.lastName) : null 

//if address is missing any of its 4 fields, remove the whole address item
postalAddress = order.address?.postal
if(!(postalAddress?.streetAddress && postalAddress?.suburb && postalAddress?.state && postalAddress?.postcode))
	order.remove('address')

return prettyPrint(toJson(order))