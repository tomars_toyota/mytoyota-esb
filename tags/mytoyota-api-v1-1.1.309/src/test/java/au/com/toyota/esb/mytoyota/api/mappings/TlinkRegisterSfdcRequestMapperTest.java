package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;

import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.custommonkey.xmlunit.XMLTestCase;
import org.junit.Test;
import org.xml.sax.SAXException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

public class TlinkRegisterSfdcRequestMapperTest extends XMLTestCase {

    private ScriptRunner scriptRunner;

    public TlinkRegisterSfdcRequestMapperTest() throws ScriptException, SAXException, IOException {
        scriptRunner = ScriptRunner.createScriptRunner("/mappings/sfdc-register-tlink-request-mapper.groovy");
    }

    @Test
    public void testRegisterTlinkRequestMapping() throws Exception {

        String result = ScriptRunBuilder
                .runner(scriptRunner)
                .payload("")
                .sessionVar("myToyotaId", "MYT-000003a3")
				.sessionVar("requestPayload",
						IOUtils.toString(getClass().getResourceAsStream("/in/post-users-me-tlink-request.json")))
				.sessionVar("userDetails",
						IOUtils.toString(
								getClass().getResourceAsStream("/out/get-user-account-info-api-response.json")))
                .run().toString();

        System.out.println(result);

        // expected
        String expectedResult = IOUtils.toString(getClass().getResourceAsStream("/out/activateDeactivateCMUser-register-tlink--sfdc-request.xml"));
        assertXMLEqual(expectedResult, result);
    }
    
    @Test
    public void testRegisterTlinkInvalidRequestRequestMapping() throws Exception {

        try {
			ScriptRunBuilder
			        .runner(scriptRunner)
			        .payload("")
			        .sessionVar("myToyotaId", "MYT-000003a3")
					.sessionVar("userDetails",
							IOUtils.toString(
									getClass().getResourceAsStream("/out/get-user-account-info-api-response.json")))
			        .run();
			//if it reached here then the test has already failed
			assertTrue(false);
		} catch (Exception e) {
			assertTrue(e.getMessage().contains("Invalid request"));
		}
        
    }
    
}