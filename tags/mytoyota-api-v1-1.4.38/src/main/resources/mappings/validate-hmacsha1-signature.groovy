import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import java.security.InvalidKeyException;

def validateUserSignature = { UID, signatureTimestamp, secret, expectedSignature ->
	def text = signatureTimestamp+"_"+UID
	def textData = text.getBytes("UTF-8");
	byte[] key = secret.decodeBase64()
	
	SecretKeySpec signingKey = new SecretKeySpec(key, "HmacSHA1");

	def mac = Mac.getInstance("HmacSHA1");

	mac.init(signingKey);
	
	def rawHmac = mac.doFinal(textData);
	
	def mySignature = rawHmac.encodeBase64().toString()
	
	def result = (mySignature == expectedSignature) ? true : false

	return result
}
def result = validateUserSignature(payload?.UID, 
								   payload?.signatureTimestamp, 
								   flowVars['imClientSecret'],
								   payload?.signature)
if (!result) {
	msg = "Signature verification failed"
	throw new au.com.toyota.esb.mytoyota.api.exception.UnauthorisedException(msg)
} 


