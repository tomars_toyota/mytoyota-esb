import groovy.json.JsonSlurper

if(!payload)
	throw new org.mule.module.apikit.exception.BadRequestException('Invalid request')

apiRequest = new JsonSlurper().parseText(payload)

//validate request
if(!(apiRequest.containsKey('dealerId') &&
		apiRequest.containsKey('branchCode') &&
		apiRequest.containsKey('operationTypeID') &&
		apiRequest.containsKey('operationID')
	))
	throw new org.mule.module.apikit.exception.BadRequestException('Invalid request')

return payload