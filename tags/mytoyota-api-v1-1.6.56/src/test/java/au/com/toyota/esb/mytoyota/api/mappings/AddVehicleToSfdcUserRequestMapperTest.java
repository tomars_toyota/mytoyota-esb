package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;
import java.util.HashMap;

import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.custommonkey.xmlunit.XMLTestCase;
import org.junit.Test;
import org.xml.sax.SAXException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

public class AddVehicleToSfdcUserRequestMapperTest extends XMLTestCase {

	private ScriptRunner scriptRunner;

	public AddVehicleToSfdcUserRequestMapperTest() throws ScriptException, SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/sfdc-add-vehicle-request-mapper.groovy");
	}

	@Test
	public void testRequestMapper() throws Exception {
		
		HashMap<String, Object> vehicleUser = new HashMap<String, Object>();
		vehicleUser.put("registrationNumber", "ABC123");
		vehicleUser.put("state", "VIC");
		
		HashMap<String, Object> payload = new HashMap<String, Object>();
		payload.put("vehicleUser", vehicleUser);
		payload.put("vin", "JTNKU3JE60J021675");
		
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(payload)
				.flowVar("myToyotaId", "MYT-000003a3")
				.flowVar("relationshipType", "O")
				.run().toString();

		System.out.println(result);

		// expected
		String expectedResult = IOUtils.toString(getClass().getResourceAsStream("/out/addAVehicle-sfdc-request.xml"));
		assertXMLEqual(expectedResult, result);
	}
	
	@Test
	public void testFailedRequestMapper() throws Exception {
		
		HashMap<String, Object> vehicleUser = new HashMap<String, Object>();
		vehicleUser.put("registrationNumber", "ABC123");
		//vehicleUser.put("state", "VIC");
		
		HashMap<String, Object> payload = new HashMap<String, Object>();
		payload.put("vehicleUser", vehicleUser);
		payload.put("vin", "JTNKU3JE60J021675");
		
		try {
			ScriptRunBuilder
				.runner(scriptRunner)
				.payload(payload)
				.flowVar("myToyotaId", "MYT-000003a3")
				.run().toString();
		} catch(Exception e) {
			assertTrue(true);
			return;
		}
		
		assertTrue(false);
	}
}
