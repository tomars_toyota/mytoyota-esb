import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper
import org.slf4j.Logger
import org.slf4j.LoggerFactory

Logger logger = LoggerFactory.getLogger("esb.mytoyota-api-v1.get-users-me-rewards-extract-registered-coupons.script");

def unregisteredCaltexVehicleOwnership = [:]
def registeredCouponButNotOwnerAnymore = [:]

def guestCaltexCouponsList = []
if (flowVars.guestCaltexCoupons) {
	guestCaltexCouponsList = new JsonSlurper().parseText(flowVars.guestCaltexCoupons)?.coupons
}

// cache all registered MemberIds
def registeredMemberIds = [:]
guestCaltexCouponsList.each { couponItem ->
	registeredMemberIds.put(couponItem.memberId, couponItem)
	
}

def myKitUserVehiclesList = []
if (flowVars.myKitUserVehicles) {
	myKitUserVehiclesList = new JsonSlurper().parseText(flowVars.myKitUserVehicles)?.findAll {!it.discontinuedDate}
}

// cache all vehicle ownership and identify which vehicle to register
def vehicleOwnershipMap = [:]
myKitUserVehiclesList.each { item ->
	def memberId = flowVars.myToyotaId + memberIdDelimiter + item.vin
	
	if (item.userRegistrationNumber && item.state) {
		if (!registeredMemberIds.containsKey(memberId)) {
			unregisteredCaltexVehicleOwnership.put(memberId, item)
		}
	}
	else {
		// skip no rego and state
		logger.info("no rego or state for this memberId: " + memberId)
	}
	vehicleOwnershipMap.put(memberId, item)
	
}

// identify which coupon to deactivate. if the vehicleownership is not present anymore
guestCaltexCouponsList.each { couponItem ->
	if (!vehicleOwnershipMap.containsKey(couponItem.memberId)) {
		registeredCouponButNotOwnerAnymore.put(couponItem.memberId, couponItem)
	}
}

flowVars.unregisteredCaltexVehicleOwnership = [] 
unregisteredCaltexVehicleOwnership.each { k, v -> 
	flowVars.unregisteredCaltexVehicleOwnership << v 
} 

flowVars.registeredCouponButNotOwnerAnymore = [] 
registeredCouponButNotOwnerAnymore.each { k, v -> 
	flowVars.registeredCouponButNotOwnerAnymore << v
} 

flowVars.vehicleOwnershipChanged = flowVars.unregisteredCaltexVehicleOwnership > 0 || flowVars.registeredCouponButNotOwnerAnymore > 0


return payload