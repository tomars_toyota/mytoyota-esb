import static groovy.json.JsonOutput.*
import org.mule.api.transport.PropertyScope;

sessionVars['plainPassword'] = payload.newPassword

request = [:]
request.password = payload.password
request.newPassword = payload.newPassword

//[MYT-977]remove all saved APISessionTokens except the current one
request.data = ['myToyotaSessionToken': [message.getProperty('APISessionToken',PropertyScope.INBOUND)]]
	
return toJson(request)