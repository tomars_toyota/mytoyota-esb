package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;

import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.xml.sax.SAXException;
import org.mule.tck.junit4.AbstractMuleContextTestCase;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;
import junit.framework.TestCase;

public class GetUsersSfdcRequestMapper extends AbstractMuleContextTestCase {

	private ScriptRunner scriptRunner;

	public GetUsersSfdcRequestMapper() throws ScriptException, SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/get-users-sfdc-request-mapper.groovy");
	}

	@Test
	public void testRequestMapper() throws Exception {

		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload("")
				.flowVar("salesforceId", "0011900000NDTZc")
				.run().toString();

		System.out.println(result);

		// expected
		String expectedResult = IOUtils.toString(getClass().getResourceAsStream("/out/get-users-sfdc-request.xml"));
		TestCase.assertEquals("XML output does not match expected XML", expectedResult, result);
	}
}
