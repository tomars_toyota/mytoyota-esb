package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;
import java.util.Properties;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

import javax.script.*;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

public class PostUsersMeVehiclesServicesServiceRequestRequestMapperTest extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;
	private Properties props;

	public PostUsersMeVehiclesServicesServiceRequestRequestMapperTest() throws ScriptException, SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner(
				"/mappings/post-users-me-vehicles-vin-services-serviceRequest-request-mapper.groovy");

		//load properties
		props = new Properties();
		props.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));	}

	@Test
	public void testSuccessfulRequestMapper() throws Exception {
		
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(getClass().getResourceAsStream("/in/get-dealers-dealerId-response.json")))
				.sessionVar("apiRequest",
						IOUtils.toString(getClass().getResourceAsStream(
								"/in/post-users-me-vehicles-vin-services-serviceRequest-api-request.json")))
				.binding("emailAccount", props.getProperty("api.ping.email.account"))
				.binding("emailTemplate", props.getProperty("api.ping.email.template"))
				.binding("fromAddress", props.getProperty("api.ping.email.from.address"))
				.binding("mergeLanguage", props.getProperty("api.ping.email.merge.language"))
				.binding("isTesting", props.getProperty("api.ping.email.isTesting"))
				.binding("testEmailsList", props.getProperty("api.ping.email.test.emails"))
				.flowVar("anyKey","anyValue")
				.run().toString();
				
		System.out.println(result);

		// expected
		String expectedResult = IOUtils.toString(getClass()
				.getResourceAsStream(
						"/out/post-users-me-vehicles-vin-services-serviceRequest-ping-request.json"));
		JSONAssert.assertEquals(expectedResult, result, true);
	}
}
