import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper
import org.mule.api.transport.PropertyScope

httpStatusCode = message.getProperty('http.status', org.mule.api.transport.PropertyScope.INBOUND)

// 404 check is required because this is an allowed HTTP 
// return status from myKIT in a social registration use case
if (httpStatusCode != 404) {
	
	def userAccount = new JsonSlurper().parseText(payload)
	
	def response = [:]
	def postalAddress = [:]
	def applicationIdentifiers = [:]
	
	response.login = userAccount?.login
	response.password = null
	response.title = userAccount?.title
	response.firstName = userAccount?.firstName
	response.lastName = userAccount?.lastName
	response.email = userAccount?.email
	response.mobile = userAccount?.mobile
	response.landline = userAccount?.landline
	response.dateOfBirth = userAccount?.dateOfBirth
	
	//Map postal address from mykit
	if(userAccount?.address){
		myKitAddress = userAccount?.address

		postalAddress.streetAddress = myKitAddress.streetAddress
		postalAddress.suburb = myKitAddress.suburb
		postalAddress.state = myKitAddress.state
		postalAddress.postcode = myKitAddress.postcode
		postalAddress.fullAddress = myKitAddress.formattedAddress
		postalAddress.totalCheckId = myKitAddress.referenceId

		postalAddress.primaryName  = myKitAddress.primaryName
	    postalAddress.secondaryName  = myKitAddress.secondaryName
	    postalAddress.unit = myKitAddress.unit
	    postalAddress.streetNo = myKitAddress.streetNo
	    postalAddress.streetName = myKitAddress.streetName
	    postalAddress.streetType = myKitAddress.streetType
	    postalAddress.country = myKitAddress.country
	    postalAddress.geoLat = myKitAddress.geoLat
	    postalAddress.geoLon = myKitAddress.geoLon
	    postalAddress.geoLatStreet = myKitAddress.geoLatStreet
	    postalAddress.geoLonStreet = myKitAddress.geoLonStreet
	    postalAddress.listing = myKitAddress.listing
	    postalAddress.postal = myKitAddress.postal
	    postalAddress.dpid = myKitAddress.dpid

	    //remove nulls
    	postalAddress = postalAddress.findAll { it.value }

    	if(postalAddress)
    		response.address = ["postal":postalAddress]
	}


	// postalAddress.streetAddress = userAccount?.streetAddress
	// postalAddress.suburb = userAccount?.suburb
	// postalAddress.state = userAccount?.state
	// postalAddress.postcode = userAccount?.postcode
	// postalAddress.fullAddress = userAccount?.fullAddress
	// postalAddress.totalCheckId = userAccount?.addressReference

	// response.address = ["postal":postalAddress]
	
	response.avatar = userAccount?.avatar
	response.licenceId = userAccount?.licenceId
	response.licenceState = userAccount?.licenceState
	response.licenceCountry = userAccount?.licenceCountry
	response.licenceExpiry = userAccount?.licenceExpiry
	applicationIdentifiers.myToyota = ["id": userAccount?.myToyotaID]
	response.applicationIdentifiers = applicationIdentifiers
	return response
	
}