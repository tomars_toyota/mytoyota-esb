package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;
import java.util.HashMap;

import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.custommonkey.xmlunit.XMLTestCase;
import org.junit.Test;
import org.xml.sax.SAXException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

public class UpdateSfdcUserPreferencesRequestMapperTest extends XMLTestCase {

    private ScriptRunner scriptRunner;

    public UpdateSfdcUserPreferencesRequestMapperTest() throws ScriptException, SAXException, IOException {
        scriptRunner = ScriptRunner.createScriptRunner("/mappings/sfdc-update-guest-preferences-request-mapper.groovy");
    }

    //@Test
    public void testUpdateAllUserPreferencesRequestMapping() throws Exception {

        HashMap<String, Object> payload = new HashMap<String, Object>();
        payload.put("globalUnsubscribe", "false");
        payload.put("goPlacesPreference", "Soft Copy Only");
        payload.put("prospectNurtureOptIn", "true");
        payload.put("trackMyCarEmailOptIn", "false");
        payload.put("trackMyCarSMSOptIn", "true");
        
        String result = ScriptRunBuilder
                .runner(scriptRunner)
                .sessionVar("myToyotaId", "MYT-000003a3")
                .payload(payload)
                .sessionVar("anything", null)    //initialise sessionVars otherwise groovy fails when triggered from Munit
                .run().toString();

        System.out.println(result);

        // expected
        String expectedResult = IOUtils.toString(getClass().getResourceAsStream("/out/updateOwner-update-guest-preferences-all-sfdc-request.xml"));
        assertXMLEqual(expectedResult, result);
    }
    
    //@Test
    public void testUpdateUserPreferencesGlobalOnlyRequestMapping() throws Exception {
        HashMap<String, Object> payload = new HashMap<String, Object>();
        payload.put("globalUnsubscribe", "false");
        
        String result = ScriptRunBuilder
                .runner(scriptRunner)
                .sessionVar("myToyotaId", "MYT-000003a3")
                .payload(payload)
                .sessionVar("anything", null)    //initialise sessionVars otherwise groovy fails when triggered from Munit
                .run().toString();

        System.out.println(result);

        // expected
        String expectedResult = IOUtils.toString(getClass().getResourceAsStream("/out/updateOwner-update-guest-preferences-global-only-sfdc-request.xml"));
        assertXMLEqual(expectedResult, result);
    }
    
    //@Test
    public void testUpdateUserPreferencesGlobalOnlyWithFLERequestMapping() throws Exception {
        HashMap<String, Object> payload = new HashMap<String, Object>();
        payload.put("globalUnsubscribe", "false");
        payload.put("email", "jsmith@mail.com.au");
        payload.put("firstName", "John");
        payload.put("lastName", "Smith");
        
        String result = ScriptRunBuilder
                .runner(scriptRunner)
                .payload(payload)
                .sessionVar("anything", null)    //initialise sessionVars otherwise groovy fails when triggered from Munit
                .run().toString();

        System.out.println(result);

        // expected
        String expectedResult = IOUtils.toString(getClass().getResourceAsStream("/out/updateOwner-update-guest-preferences-global-only-with-fle-sfdc-request.xml"));
        assertXMLEqual(expectedResult, result);
    }
    
    //@Test
    public void testUpdatePartialUserPreferencesRequestMapping() throws Exception {
        HashMap<String, Object> payload = new HashMap<String, Object>();
        payload.put("goPlacesPreference", "Soft Copy Only");
        payload.put("trackMyCarSMSOptIn", "true");
        
        String result = ScriptRunBuilder
                .runner(scriptRunner)
                .sessionVar("myToyotaId", "MYT-000003a3")
                .payload(payload)
                .sessionVar("anything", null)    //initialise sessionVars otherwise groovy fails when triggered from Munit
                .run().toString();

        System.out.println(result);

        // expected
        String expectedResult = IOUtils.toString(getClass().getResourceAsStream("/out/updateOwner-update-guest-preferences-partial-sfdc-request.xml"));
        assertXMLEqual(expectedResult, result);
    }
    
    //@Test
    public void testUpdateSingleUserPreferencesRequestMapping() throws Exception {
        HashMap<String, Object> payload = new HashMap<String, Object>();
        payload.put("trackMyCarSMSOptIn", "true");
        
        String result = ScriptRunBuilder
                .runner(scriptRunner)
                .sessionVar("myToyotaId", "MYT-000003a3")
                .payload(payload)
                .sessionVar("anything", null)    //initialise sessionVars otherwise groovy fails when triggered from Munit
                .run().toString();

        System.out.println(result);

        // expected
        String expectedResult = IOUtils.toString(getClass().getResourceAsStream("/out/updateOwner-update-guest-preferences-single-sfdc-request.xml"));
        assertXMLEqual(expectedResult, result);
    }
    
    //@Test
    public void testUpdateSingleUserPreferencesWithFLERequestMapping() throws Exception {
        HashMap<String, Object> payload = new HashMap<String, Object>();
        payload.put("trackMyCarSMSOptIn", "true");
        payload.put("email", "jsmith@mail.com.au");
        payload.put("firstName", "John");
        payload.put("lastName", "Smith");
        
        String result = ScriptRunBuilder
                .runner(scriptRunner)
                .payload(payload)
                .sessionVar("anything", null)    //initialise sessionVars otherwise groovy fails when triggered from Munit
                .run().toString();

        System.out.println(result);

        // expected
        String expectedResult = IOUtils.toString(getClass().getResourceAsStream("/out/updateOwner-update-guest-preferences-single-with-fle-sfdc-request.xml"));
        assertXMLEqual(expectedResult, result);
    }
}