package au.com.toyota.esb.mytoyota.api.mappings;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mule.munit.common.mocking.Attribute.attribute;

import java.io.IOException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

import javax.script.*;
import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;
import org.mule.api.MuleMessage;
import org.mule.DefaultMuleMessage;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.mule.context.DefaultMuleContextFactory;
import org.mule.api.MuleContext;
import org.mule.api.MuleEvent;
import org.mule.api.transport.PropertyScope;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

public class GetOrdersByEmailResponseMapperTest extends AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;
	
	public GetOrdersByEmailResponseMapperTest() throws ScriptException, SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/get-orders-by-email-response-mapper.groovy");
	}
	
	//@Test
	public void testRequestMapper() throws Exception {
		
		// This mapper returns a map, rather than a JSON string. Therefore convert it to a JSON string before the assert()
		Object result = ScriptRunBuilder
				.runner(scriptRunner)
				.sessionVar("email", "mytoyotatest1+105@gmail.com")
				.payload(IOUtils.toString(getClass().getResourceAsStream(
						"/in/get-orders-by-email-response.json")))
				.run();
		String resultJson = new ObjectMapper().writeValueAsString(result);
		System.out.println(resultJson);
		
		// expected
		String expectedResult = IOUtils.toString(getClass().getResourceAsStream(
				"/out/get-orders-by-email-mapping-response.json"));
		JSONAssert.assertEquals(expectedResult, resultJson, true);
	}
	
	//@Test
	public void testNoResultsFound() throws Exception {
		
		try {
			// This mapper returns a map, rather than a JSON string. Therefore convert it to a JSON string before the assert()
			Object result = ScriptRunBuilder
					.runner(scriptRunner)
					.sessionVar("email", "mytoyotatest1+105@gmail.com")
					.payload(IOUtils.toString(getClass().getResourceAsStream(
							"/in/get-orders-by-email-no-results-response.json")))
					.run();
		} catch (Exception e) {
			System.out.println("**** Exception: "+e.getCause());
			assertTrue(e.getCause() instanceof javax.script.ScriptException);
			assertEquals("org.mule.module.apikit.exception.NotFoundException: Email not found in orders", e.getCause().getMessage());
		}
	}
}
