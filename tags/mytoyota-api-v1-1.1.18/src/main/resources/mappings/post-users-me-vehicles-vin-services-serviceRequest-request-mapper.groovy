import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper

def removeNulls(col) {
    if (col instanceof List) 
        return col.findAll { it }.collect { removeNulls(it) }
    else if (col instanceof Map) 
        return col.findAll { it.value }.collectEntries { [(it.key): removeNulls(it.value) ] }
    else return col
}

JsonSlurper js = new JsonSlurper()

serviceRequestPayload = js.parseText(sessionVars['apiRequest'])

dealer = js.parseText(payload)

//remove unrelated sites
dealer?.sites?.removeAll { it.branchCode != serviceRequestPayload?.branchCode }

mailRequest = [:]

mailRequest.account = emailAccount

addresseeList = []

//if isTesting use testing email(s)
if(isTesting == true) {
	testEmails = testEmailsList?.split(',').findAll { it }.collect { it.trim() }
	for(testEmail in testEmails) {
		to = [:]
	
		to.email = testEmail
		to.name = dealer.dealerName
		to.type = 'to'

		addresseeList.add(to)	
	}
} else {
	to = [:]
	
	to.email = dealer.sites[0].email
	to.name = dealer.dealerName
	to.type = 'to'
	
	addresseeList.add(to)
}

//send a copy to customer
cc = [:]
	
if((serviceRequestPayload.alternativeContact && serviceRequestPayload.alterContactDetails.email)) {
	cc.email = serviceRequestPayload.alterContactDetails.email
	cc.name = serviceRequestPayload.alterContactDetails.firstName + serviceRequestPayload.alterContactDetails.lastName
	cc.type = 'cc'
	addresseeList.add(cc)
} else if (serviceRequestPayload.contactDetails.email) {
	cc.email = erviceRequestPayload.contactDetails.email
	cc.name = serviceRequestPayload.contactDetails.firstName + serviceRequestPayload.contactDetails.lastName
	cc.type = 'cc'
	addresseeList.add(cc)
}


mailRequest.to = addresseeList

mailRequest.template = emailTemplate

//build email contents
serviceRequest = [:]

//handlebars merg language used with email templates doesn't work with camelCase, so keys are mapped to '_' separated
serviceRequest.registration_number = serviceRequestPayload.registrationNumber
serviceRequest.odometer_reading_km = serviceRequestPayload.odometerReadingKm
serviceRequest.service_code = serviceRequestPayload.serviceCode
serviceRequest.operation = serviceRequestPayload.operation
serviceRequest.estimated_service_time = serviceRequestPayload.estimatedServiceTime
serviceRequest.drop_off_date = serviceRequestPayload.dropOffDate
serviceRequest.drop_off_time = serviceRequestPayload.dropOffTime
serviceRequest.pick_up_date = serviceRequestPayload.pickUpDate
serviceRequest.pick_up_time = serviceRequestPayload.pickUpTime
serviceRequest.notes = serviceRequestPayload.notes
serviceRequest.wait_at_dealer_flag = serviceRequestPayload.waitAtDealerFlag
serviceRequest.express_service_flag = serviceRequestPayload.expressServiceFlag
serviceRequest.courtesy_bus_flag = serviceRequestPayload.courtesyBusFlag
serviceRequest.loan_vehicle_flag = serviceRequestPayload.loanVehicleFlag

contact_details = [:]
contact_details.tilte = serviceRequestPayload.contactDetails.tilte
contact_details.first_name = serviceRequestPayload.contactDetails.firstName
contact_details.last_name = serviceRequestPayload.contactDetails.lastName
contact_details.email = serviceRequestPayload.contactDetails.email
contact_details.phone = serviceRequestPayload.contactDetails.phone

serviceRequest.contact_details = contact_details

serviceRequest.alternative_contact = serviceRequestPayload.alternativeContact

alter_contact_details = [:]
alter_contact_details.tilte = serviceRequestPayload.alterContactDetails.tilte
alter_contact_details.first_name = serviceRequestPayload.alterContactDetails.firstName
alter_contact_details.last_name = serviceRequestPayload.alterContactDetails.lastName
alter_contact_details.email = serviceRequestPayload.alterContactDetails.email
alter_contact_details.phone = serviceRequestPayload.alterContactDetails.phone

serviceRequest.alter_contact_details = alter_contact_details

//println 'serviceRequest: '+serviceRequest

serviceRequestMap = [:]
serviceRequestMap.name = 'serviceRequest'
serviceRequestMap.content = removeNulls(serviceRequest)

mailRequest.globalMergeVars = [serviceRequestMap]

mailRequest.fromAddress = fromAddress
mailRequest.mergeLang = mergeLanguage

return prettyPrint(toJson(mailRequest))