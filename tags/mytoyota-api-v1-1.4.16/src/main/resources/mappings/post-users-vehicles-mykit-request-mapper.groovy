import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper

if(!payload)
	throw new org.mule.module.apikit.exception.BadRequestException('Invalid request')
	
def vehicleRequest = new JsonSlurper().parseText(payload)

// Build vehicle list
def vehicle = [:]
vehicle.vin = vehicleRequest?.vin

//vehicle user
vehicleUserMap = [:]

//MYT-1101 default isVerified to true
// set regardless if there is no vehicleUser object
if (vehicle?.vin) {
	vehicleUserMap.isVerified = true
}
	
if(vehicleRequest?.vehicleUser) {
	vehicleUserMap.registrationNumber = vehicleRequest.vehicleUser.registrationNumber
	vehicleUserMap.state = vehicleRequest.vehicleUser.state
	
	
	//remove nulls
	vehicleUserMap = vehicleUserMap.findAll {it.value != null }
}

if(vehicleUserMap) {
	vehicle.vehicleUsers = []
	vehicle.vehicleUsers << vehicleUserMap
}

//remove nulls
vehicle = vehicle.findAll {it.value != null }

if(!vehicle)
	throw new org.mule.module.apikit.exception.BadRequestException('Invalid request')

return prettyPrint(toJson(vehicle))