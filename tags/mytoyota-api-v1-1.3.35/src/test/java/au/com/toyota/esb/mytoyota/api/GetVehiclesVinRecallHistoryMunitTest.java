package au.com.toyota.esb.mytoyota.api;

import static org.junit.Assert.assertEquals;
import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleException;
import org.mule.api.MuleMessage;
import org.mule.api.cache.InvalidatableCachingStrategy;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.common.mocking.SpyProcess;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.util.IOUtils;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;


public class GetVehiclesVinRecallHistoryMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"get-vehicle-details-technical-adapter.xml",
				"get-vehicle-recall-history.xml",
				"get-vehicle-recall-history-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulRequest() throws Exception {
		
		// Invalidate Cache before running unit test
		InvalidatableCachingStrategy cache = muleContext.getRegistry().<InvalidatableCachingStrategy>get("Generic_Caching_Strategy_Low_Currency");
		cache.invalidate();

		String vin = "JTDBR23E803142287";
		String requestPath = "/api/myToyota/v1/vehicles/" + vin + "/recallHistory";
		String recallType = "SSC";
		

		whenMessageProcessor("request").ofNamespace("http")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("Vehicle API GET vehicle by vin"))
			.thenApply(new MockResponseTransformer() {
			@Override
			public MuleMessage transform(MuleMessage originalMessage) {
				MuleMessage newMessage = null;
				
				try {
					newMessage = originalMessage.createInboundMessage();
					
					newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
					
					newMessage.setPayload(getClass().getResourceAsStream("/in/get-vehicle-details-response-with-items-from-wharf.json"));
					newMessage.setInvocationProperty("queryString", "recallType=SSC");
				} catch (Exception e) {
					// catch exception statements
				}
				
				return newMessage;
			}
		});
		
		whenMessageProcessor("request").ofNamespace("http")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("Vehicle API GET Recall History"))
			.thenApply(new MockResponseTransformer() {
			@Override
			public MuleMessage transform(MuleMessage originalMessage) {
				MuleMessage newMessage = null;
				
				try {
					newMessage = originalMessage.createInboundMessage();
					
					newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
					
					newMessage.setPayload(getClass().getResourceAsStream("/in/get-vehicles-vin-recallHistory-vehicleApi-response-with-items-from-wharf.json"));
				} catch (Exception e) {
					// catch exception statements
				}
				
				return newMessage;
			}
		});
		
		// Spy on the startDate if it's being set.
		spyMessageProcessor("request").ofNamespace("http")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("Vehicle API GET Recall History"))
			.before(new SpyProcess() {
	
				@Override
				public void spy(MuleEvent event) throws MuleException {
					String queryString = event.getFlowVariable("queryString");
					assertEquals("The query String should contain startDate", true, queryString.contains("startDate"));
					assertEquals("The query String should contain recallType", true, queryString.contains("recallType"));
	
				}
	
			});

		MuleEvent event = testEvent(requestPath);
		event.setFlowVariable("vin", vin);
		
		MuleMessage message = muleMessageWithPayload(null);

		Map<String, Object> props = new HashMap<String, Object>();
		props.put("recallType", recallType);
		
		String queryString = generateQueryString(props);

		message.setProperty("http.query.params", props, PropertyScope.INBOUND);
		message.setProperty("http.query.string", queryString, PropertyScope.INBOUND);
		
		event.setMessage(message);

		MuleEvent output = runFlow("get:/vehicles/{vin}/recallHistory:myToyota-config", event);
		
		// expected
		String expectedResult = IOUtils
				.toString(getClass().getResourceAsStream("/out/get-vehicles-vin-recallHistory-response-filtered-without-items-from-wharf.json"));

		System.out.println(output.getMessage().getPayload());

		assertEquals(expectedResult, output.getMessage().getPayloadAsString());
		
		
		
	}

	private String generateQueryString(Map<String, Object> props) {
		String queryString = null;
		for (String key : props.keySet()) {
			queryString = (queryString != null) ? queryString + "&" : "";
			queryString = queryString + key + "=" + props.get(key);
			
		}
		return queryString;
	}
	
	@Test
	public void testSuccessfulWhereStartDateIsProvidedFromRequest() throws Exception {
		
		// Invalidate Cache before running unit test
		InvalidatableCachingStrategy cache = muleContext.getRegistry().<InvalidatableCachingStrategy>get("Generic_Caching_Strategy_Low_Currency");
		cache.invalidate();

		String vin = "JTDBR23E803142287";
		String requestPath = "/api/myToyota/v1/vehicles/" + vin + "/recallHistory";
		String recallType = "SSC";
		final String startDate = "2005-01-01";
				
		whenMessageProcessor("request").ofNamespace("http")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("Vehicle API GET Recall History"))
			.thenApply(new MockResponseTransformer() {
			@Override
			public MuleMessage transform(MuleMessage originalMessage) {
				MuleMessage newMessage = null;
				
				try {
					newMessage = originalMessage.createInboundMessage();
					
					newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
					
					newMessage.setPayload(getClass().getResourceAsStream("/in/get-vehicles-vin-recallHistory-vehicleApi-response-with-items-from-wharf.json"));
				} catch (Exception e) {
					// catch exception statements
				}
				
				return newMessage;
			}
		});
		
		// Spy on the startDate if it's being set.
		spyMessageProcessor("request").ofNamespace("http")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("Vehicle API GET Recall History"))
			.before(new SpyProcess() {
	
				@Override
				public void spy(MuleEvent event) throws MuleException {
					String queryString = event.getFlowVariable("queryString");
					assertEquals("The query String should not be altered", true, queryString.contains(startDate));
					assertEquals("The query String should contain recallType", true, queryString.contains("recallType"));
	
				}
	
			});

		MuleEvent event = testEvent(requestPath);
		event.setFlowVariable("vin", vin);
		
		MuleMessage message = muleMessageWithPayload(null);

		event.setMessage(message);

		Map<String, Object> props = new HashMap<String, Object>();
		props.put("recallType", recallType);
		props.put("startDate", startDate);

		String queryString = generateQueryString(props);

		message.setProperty("http.query.params", props, PropertyScope.INBOUND);
		message.setProperty("http.query.string", queryString, PropertyScope.INBOUND);


		MuleEvent output = runFlow("get:/vehicles/{vin}/recallHistory:myToyota-config", event);
		
		// expected
		String expectedResult = IOUtils
				.toString(getClass().getResourceAsStream("/out/get-vehicles-vin-recallHistory-response-filtered-without-items-from-wharf.json"));

		System.out.println(output.getMessage().getPayload());

		assertEquals(expectedResult, output.getMessage().getPayloadAsString());
		
		verifyCallOfMessageProcessor("request").ofNamespace("http")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("Vehicle API GET vehicle by vin"))
				.times(0);
		
	}
	
	
	

}
