def requestPayload = flowVars['requestPayload']

def result = [:]
def profile = [:]

profile.firstName = requestPayload?.firstName ?: ''
profile.lastName = requestPayload?.lastName ?: ''
profile."phones.type" = "mobile"
profile."phones.number" = requestPayload?.mobile ?: ''
result.username = requestPayload?.login ?: ''
result.password = requestPayload?.password ?: ''
result.email = requestPayload?.email ?: ''
result.profile = profile

return result