// There should be a groovier way to do this!

def result = [:]
def profile = [:]

if (payload?.firstName?.length() > 0 ) {
	profile.firstName = payload?.firstName
}

if (payload?.lastName?.length() > 0 ) {
	profile.lastName = payload?.lastName
}

if (payload?.mobile?.length() > 0 ) {
	profile."phones.type" = "mobile"
	profile."phones.number" = payload?.mobile
}

if (payload?.email?.length() > 0 ) {
	profile.email = payload?.email
}

if (payload?.login?.length() > 0 ) {
	result.username = payload?.login
}

if (payload?.newPassword?.length() > 0 ) {
	result.newPassword = payload?.newPassword
}

if (payload?.password?.length() > 0 ) {
	result.password = payload?.password
}

if (payload?.data != null ) {
	result.data = payload?.data
}

if (profile.isEmpty() == false) {
	result.profile = profile
}

return result