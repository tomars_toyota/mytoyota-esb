package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;
import java.util.HashMap;

import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.custommonkey.xmlunit.XMLTestCase;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

public class PostUsersRegisterAtMykitRequestMapperTest extends XMLTestCase {

	private ScriptRunner scriptRunner;

	public PostUsersRegisterAtMykitRequestMapperTest() throws ScriptException, SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/post-register-user-mykit-backend-request-mapper.groovy");
	}

	@Test
	public void testRequestMapper() throws Exception {

		HashMap<String, Object> payload = new HashMap<String, Object>();
		
		payload.put("password", "123");
		payload.put("title", "Mr");
		payload.put("firstName", "John");
		payload.put("lastName", "Smith");
		payload.put("email", "jsmith@mail.com");
		payload.put("mobile", "0412341234");
		payload.put("dateOfBirth", "1985-03-20");
		payload.put("landline", "0312341234");
		payload.put("licenceId", "1233445");
		payload.put("licenceState", "Vic");
		payload.put("licenceCountry", "Au");
		payload.put("licenceExpiry", "2020-03-20");
		
		HashMap<String, Object> postal = new HashMap<String, Object>();
		postal.put("fullAddress", "446 William St");
		postal.put("totalCheckId", "79470382");
		postal.put("streetAddress", "446 William St");
		postal.put("postcode", "3000");
		postal.put("suburb", "Melbourne");
		postal.put("state", "Vic");
		
		HashMap<String, Object> address = new HashMap<String, Object>();
		address.put("postal", postal);
		
		payload.put("address", address);
		
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload("")
				.sessionVar("requestPayload", payload)
				.run().toString();

		System.out.println(result);

		// expected
		String expectedResult = IOUtils.toString(getClass().getResourceAsStream("/out/post-users-register-at-mykit-request.json"));
		JSONAssert.assertEquals(expectedResult, result, true);
	}
}
