package au.com.toyota.esb.mytoyota.api;

import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.util.StringUtils;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;


public class DeleteVehiclesVinServicesServiceIdMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"delete-vehicles-vin-services-serviceId.xml",
				"delete-vehicles-vin-services-serviceId-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulUpdateServiceBooking() throws Exception {
		
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
				attribute("name").ofNamespace("doc").withValue("MyKit API: DELETE /services/{id}"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						
						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("Content-Type", "application/json;charset=UTF-8", PropertyScope.INBOUND);
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							newMessage.setPayload("");
						} catch (Exception e) {
							// catch exception statements
						}
						
						return newMessage;
					}
				});
		
		
		MuleEvent event = testEvent("");
		
		Map<String, Object> props = new HashMap<String,Object>();
		props.put("vin", "6T153BK360X070769");
        props.put("dmsType", "TUNE");
        props.put("dmsID", "61.10169.10169011116");
		
		MuleMessage msg = muleMessageWithPayload(null);
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
		msg.setInvocationProperty("enableServiceBookingPersistenceMyKit", true);
		event.setMessage(msg);		
		
		// Invoke the flow
		runFlow("delete:/vehicles/{vin}/services/{serviceId}:myToyota-config", event);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.delete-vehicles-vin-services-serviceId.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.delete-vehicles-vin-services-serviceId.response"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.delete-vehicles-vin-services-serviceId-technical-adapter.request"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.delete-vehicles-vin-services-serviceId-technical-adapter.response"))
				.times(1);
	}
	
	@Test
	public void testDisableMyKitPersistence() throws Exception {
		
		
		MuleEvent event = testEvent("");
		
		Map<String, Object> props = new HashMap<String,Object>();
		props.put("vin", "6T153BK360X070769");
        props.put("dmsType", "TUNE");
        props.put("dmsID", "61.10169.10169011116");
		
		MuleMessage msg = muleMessageWithPayload(null);
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
		event.setMessage(msg);		
		
		// Invoke the flow
		runFlow("delete:/vehicles/{vin}/services/{serviceId}:myToyota-config", event);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.delete-vehicles-vin-services-serviceId.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.delete-vehicles-vin-services-serviceId.response"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.delete-vehicles-vin-services-serviceId-technical-adapter.request"))
				.times(0);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.delete-vehicles-vin-services-serviceId-technical-adapter.response"))
				.times(0);
	}
	
	@Test
	public void testFailuresStatusCodes() throws Exception {
		
		int[] statusCodes = new int[] { 404, 405, 415, 406, 401, 500 };	
		
		MuleEvent event = testEvent("");
		
		Map<String, Object> props = new HashMap<String,Object>();
		props.put("vin", "6T153BK360X070769");
        props.put("dmsType", "TUNE");
        props.put("dmsID", "61.10169.10169011116");
		
		MuleMessage msg = muleMessageWithPayload(null);
		msg.setInvocationProperty("enableServiceBookingPersistenceMyKit", true);
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
		
		event.setMessage(msg);
		for (int i = 0; i < statusCodes.length; i++) {
			new StatusCodesTestingHelper().runFailureTest(statusCodes[i], "MyKit API: DELETE /services/{id}",
					"delete:/vehicles/{vin}/services/{serviceId}:myToyota-config", event);
		}
	}
}
