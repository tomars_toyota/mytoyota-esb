package au.com.toyota.esb.mytoyota.api.metrics;

import java.io.StringWriter;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Stack;

import org.apache.log4j.Logger;
import org.mule.api.context.notification.MessageProcessorNotificationListener;
import org.mule.api.processor.MessageProcessor;
import org.mule.context.notification.MessageProcessorNotification;
import org.mule.module.apikit.Router;
import org.mule.module.http.internal.request.DefaultHttpRequester;

public class MuleComponentProcessListener
		implements MessageProcessorNotificationListener<MessageProcessorNotification> {
	private static Logger log = Logger.getLogger("esb.mytoyota-api-v1.instrument");
	private boolean logMetrics;
	private ThreadLocal<Stack<Map<String, String>>> callStack;

	public MuleComponentProcessListener() {
		log.info("MuleComponentProcessListener is up with logMetrics = " + logMetrics);
		callStack = ThreadLocal.withInitial(Stack::new);
	}

	@Override
	public void onNotification(MessageProcessorNotification notification) {
		if (logMetrics) {
			String transactionId = notification.getSource().getSession().getProperty("transactionId");
			String status = (notification.getExceptionThrown() != null) ? "ERROR" : "OK";

			MessageProcessor process = notification.getProcessor();
			String className = process.getClass().getSimpleName();
			boolean isHttpRequest = process instanceof DefaultHttpRequester;
			boolean isApiKitRouter = process instanceof Router;

			if (className.equalsIgnoreCase("Router") && isApiKitRouter) {
				handleRouter(notification, transactionId, status);

			}
			if (className.equalsIgnoreCase("DefaultHttpRequester") && isHttpRequest) {
				handleDefaultHttpRequester(notification, transactionId, status);
			}
		}
	}

	private void handleDefaultHttpRequester(MessageProcessorNotification notification, String transactionId,
			String status) {
		try {
			DefaultHttpRequester def = (DefaultHttpRequester) notification.getProcessor();
			if (notification.getAction() == MessageProcessorNotification.MESSAGE_PROCESSOR_PRE_INVOKE) {
				Map<String, String> props = new HashMap<>();
				props.put("defaultHttpRequester.startTime", System.currentTimeMillis() + "");
				callStack.get().push(props);
			}
			if (notification.getAction() == MessageProcessorNotification.MESSAGE_PROCESSOR_POST_INVOKE) {
				Map<String, String> call = callStack.get().pop();
				if (call != null) {
					MetricsLogBuilder builder = new MetricsLogBuilder();
					builder.threadId(Thread.currentThread().getId()).metric("RT").direction("outbound")
							.transactionId(transactionId).host(def.getHost()).method(def.getMethod())
							.path(def.getPath()).status(status).executionTime(System.currentTimeMillis()
									- Long.parseLong(call.get("defaultHttpRequester.startTime")));
					logMessage(builder.buildAsTags());
				}
			}
		} catch (Exception e) {
			log.error("Error in MuleComponentProcessListener.handleDefaultHttpRequester", e);
		}
	}

	private void handleRouter(MessageProcessorNotification notification, String transactionId, String status) {
		try {

			MetricsLogBuilder builder = new MetricsLogBuilder();

			if (notification.getAction() == MessageProcessorNotification.MESSAGE_PROCESSOR_PRE_INVOKE) {
				Map<String, String> props = new HashMap<>();
				notification.getSource().getMessage().getInboundPropertyNames()
												.stream()
												.filter(name -> Metrics.isSupported(name))
												.forEach(name -> {
													props.put(name, notification.getSource().getMessage().getInboundProperty(name));
												});
				props.put("router.startTime", System.currentTimeMillis() + "");
				callStack.get().push(props);

				builder = new MetricsLogBuilder();
				builder
				.threadId(Thread.currentThread().getId())
				.transactionId(transactionId)
				.metric("HIT")
						.httpParams(props);

				logMessage(builder.buildAsTags());
			}
			if (notification.getAction() == MessageProcessorNotification.MESSAGE_PROCESSOR_POST_INVOKE) {

				if (callStack.get() != null) {
					Map<String, String> callParams = callStack.get().pop();
					if (callParams != null) {
						builder = new MetricsLogBuilder();
						builder.threadId(Thread.currentThread().getId()).metric("RT").direction("inbound")
								.transactionId(transactionId).httpParams(callParams).status(status)
								.executionTime(System.currentTimeMillis()
										- Long.parseLong(callParams.get("router.startTime")));
						logMessage(builder.buildAsTags());
					}
				}
				
				callStack.remove();
			}
		} catch (Exception e) {
			log.error("Error in MuleComponentProcessListener.handleRouter", e);
			if (notification.getAction() == MessageProcessorNotification.MESSAGE_PROCESSOR_POST_INVOKE) {
				callStack.remove();
			}
		}
	}

	private void logMessage(String msg) {
		log.info(msg);
	}

	class MetricsLogBuilder {

		private Map<Metrics, String> data = new HashMap<>();

		MetricsLogBuilder transactionId(String transactionId) {
			putIfNotEmpty(Metrics.transactionId, transactionId);
			return this;
		}

		MetricsLogBuilder threadId(long threadId) {
			putIfNotEmpty(Metrics.threadId, String.valueOf(threadId));
			return this;
		}

		MetricsLogBuilder host(String host) {
			putIfNotEmpty(Metrics.host, host);
			return this;
		}

		MetricsLogBuilder path(String path) {
			putIfNotEmpty(Metrics.path, path);
			return this;
		}

		MetricsLogBuilder method(String method) {
			putIfNotEmpty(Metrics.method, method);
			return this;
		}

		MetricsLogBuilder queryString(String queryString) {
			putIfNotEmpty(Metrics.queryString, queryString);
			return this;
		}

		MetricsLogBuilder metric(String metric) {
			putIfNotEmpty(Metrics.metric, metric);
			return this;
		}

		MetricsLogBuilder direction(String direction) {
			putIfNotEmpty(Metrics.direction, direction);
			return this;
		}

		MetricsLogBuilder status(String status) {
			putIfNotEmpty(Metrics.status, status);
			return this;
		}

		MetricsLogBuilder executionTime(long executionTime) {
			putIfNotEmpty(Metrics.executionTime, String.valueOf(executionTime));
			return this;
		}

		MetricsLogBuilder httpParams(Map<String, String> httpInboundProperties) {

			return host(httpInboundProperties.get(Metrics.host.httpParam())).method(httpInboundProperties.get(Metrics.method.httpParam()))
					.path(httpInboundProperties.get(Metrics.path.httpParam()))
					.queryString(httpInboundProperties.get(Metrics.queryString.httpParam()));

		}

		public String buildAsTags() {

			StringWriter sw = new StringWriter();
			sw.append("[METRICS]");
			data.keySet().stream().sorted().forEach(k -> {
				sw.append("[").append(k.name()).append("=").append(data.getOrDefault(k, "")).append("]");
			});
			return sw.toString();
		}

		private void putIfNotEmpty(Metrics k, String v) {
			if (notEmpty(v)) {
				data.put(k, v);
			}
		}
	}

	private static boolean notEmpty(String str) {
		return str != null && !str.trim().isEmpty();
	}

	private enum Metrics {
		metric, transactionId, threadId, direction, method("http.method"), host("host"), path("http.request.path"), queryString("http.query.string"), status, executionTime;
		
		Metrics(){
			this("");
		}
		
		Metrics(String httpParam){
			this._httpParam = httpParam;
		}
		
		private String _httpParam;
		
		public String httpParam(){
			return this._httpParam;
		}
		
		public static boolean isSupported(String httpParam){
			
			if(notEmpty(httpParam)) {
				return Arrays.stream(Metrics.values()).anyMatch(m -> m.httpParam().equalsIgnoreCase(httpParam));
			}
			
			return false;
		}
	}
	

	public boolean isLogMetrics() {
		return logMetrics;
	}

	public void setLogMetrics(boolean logMetrics) {
		this.logMetrics = logMetrics;
	}

}
