import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper
import org.mule.api.transport.PropertyScope

def formatAddress(address) {
	return address.addressLine1 + ", " + address.city + " " + address.state + " " + address.postCode
}

def userAccountList = new JsonSlurper().parseText(payload)
if (!userAccountList) {
	throw new org.mule.module.apikit.exception.NotFoundException('User account not found')
}
def userAccount = userAccountList.first()

def response = [:]
def postalAddress = [:]
def residentialAddress = [:]
def applicationIdentifiers = [:]
def address = [:]

response.login = null
response.password = null
response.title = userAccount?.salutation
response.firstName = userAccount?.firstName
response.lastName = userAccount?.lastName
def electronicAddressEmail = userAccount?.electronicAddresses?.find { it.electronicAddressType == 'EMAIL' }
response.email = electronicAddressEmail.electronicAddress
def mobile = userAccount?.phones?.find { it.phoneType == 'M' }
response.mobile = mobile.phoneNumber
def landline = userAccount?.phones?.find { it.phoneType == 'H' }
response.landline = landline?.phoneNumber
response.dateOfBirth = userAccount?.birthDate



//Map postal address from mykit
def postalAddressItem = userAccount?.addresses?.find { it.addressType == 'SA' }
if(postalAddressItem){

	postalAddress.streetAddress = postalAddressItem.addressLine1
	postalAddress.suburb = postalAddressItem.city
	postalAddress.state = postalAddressItem.state
	postalAddress.postcode = postalAddressItem.postCode
	postalAddress.fullAddress = formatAddress(postalAddressItem)
	postalAddress.dpid = postalAddressItem.dpid
	
    //remove nulls
	postalAddress = postalAddress.findAll { it.value }

	if(postalAddress)
		address.postal = postalAddress
}

def residentialAddressItem = userAccount?.addresses?.find { it.addressType == 'BA' }
if(residentialAddressItem){

	residentialAddress.streetAddress = residentialAddressItem.addressLine1
	residentialAddress.suburb = residentialAddressItem.city
	residentialAddress.state = residentialAddressItem.state
	residentialAddress.postcode = residentialAddressItem.postCode
	residentialAddress.fullAddress = formatAddress(residentialAddressItem)
	residentialAddress.dpid = residentialAddressItem.dpid

	
    //remove nulls
	residentialAddress = residentialAddress.findAll { it.value }

	if(residentialAddress)
		address.residential = residentialAddress
}

response.address = address

response.avatar = userAccount?.avatar // generate the avatar using azure sdk

applicationIdentifiers.myToyota = ["id": flowVars.myToyotaId]
response.applicationIdentifiers = applicationIdentifiers
return response
	
