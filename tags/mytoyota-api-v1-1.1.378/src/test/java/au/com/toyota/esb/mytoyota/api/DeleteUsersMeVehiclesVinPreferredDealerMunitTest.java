package au.com.toyota.esb.mytoyota.api;

import static org.mule.munit.common.mocking.Attribute.attribute;

import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.util.IOUtils;
import org.mule.util.StringUtils;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;


public class DeleteUsersMeVehiclesVinPreferredDealerMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"delete-users-me-vehicles-vin-preferredDealer.xml",
				"delete-users-me-vehicles-vin-preferredDealer-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulGetPreferredDealer() throws Exception {
		
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
			attribute("name").ofNamespace("doc").withValue("MyKit API: DELETE /api/v1/users/{userLogin}/vehicles/{vin}/preferredDealer"))
			.thenApply(new MockResponseTransformer() {
				@Override
				public MuleMessage transform(MuleMessage originalMessage) {
					MuleMessage newMessage = null;
					
					try {
						newMessage = originalMessage.createInboundMessage();
						
						newMessage.setProperty("http.status", 201, PropertyScope.INBOUND);
						newMessage.setProperty("Content-Type", "application/json;charset=UTF-8", PropertyScope.INBOUND);
						
						newMessage.setPayload(IOUtils.toInputStream("", "UTF-8"));
					} catch (Exception e) {
						// catch exception statements
					}
					
					return newMessage;
				}
			});
		
		
		MuleEvent event = testEvent("{\"dealerId\": \"37104\",\"branchId\": \"37184\"}");
		event.setFlowVariable("vin", "6T1BF3FK00X043911");
		event.setFlowVariable("myToyotaId", "MYT-000001f6");
		
		// Invoke the flow
		runFlow("delete:/users/me/vehicles/{vin}/preferredDealer:myToyota-config", event);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("category")
				.withValue("esb.mytoyota-api-v1.delete-users-me-vehicles-vin-preferredDealer.request")).times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("category")
				.withValue("esb.mytoyota-api-v1.delete-users-me-vehicles-vin-preferredDealer.response")).times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.delete-users-me-vehicles-vin-preferredDealer-technical-adapter.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.delete-users-me-vehicles-vin-preferredDealer-technical-adapter.response"))
				.times(1);
	}
	
	@Test
	public void testFailuresStatusCodes() throws Exception {
		
		int[] statusCodes = new int[] { 404, 405, 415, 406, 401, 500 };	
		
		MuleEvent event = testEvent("");
		event.setFlowVariable("vin", "6T1BF3FK00X043911");
		event.setFlowVariable("myToyotaId", "MYT-000001f6");
		
		for (int i = 0; i < statusCodes.length; i++) {
			new StatusCodesTestingHelper().runFailureTest(statusCodes[i],
					"MyKit API: DELETE /api/v1/users/{userLogin}/vehicles/{vin}/preferredDealer",
					"delete:/users/me/vehicles/{vin}/preferredDealer:myToyota-config", event);
		}
	}
}
