package au.com.toyota.esb.mytoyota.api;

import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.util.IOUtils;
import org.mule.util.StringUtils;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;


/*
 * Munit test to unit test the functionality to get outstanding services through 
 * /users/me/vehicles/{vin}/services/outstanding
 * @author: ahuwait
 */
public class GetUsersOutstandingServicesMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"get-users-me-vehicles-vin-services-outstanding.xml",
				"get-users-me-vehicles-vin-services-outstanding-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulGetOutstandingServices() throws Exception {
		testSuccessfulGetOutstandingServicesImpl("TUNE");
	}
	
	@Test
	public void testSuccessfulGetOutstandingServicesTuneLowerCase() throws Exception {
		testSuccessfulGetOutstandingServicesImpl("tune");
	}
	
	private void testSuccessfulGetOutstandingServicesImpl(String dmsType) throws Exception {
		
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
			attribute("name").ofNamespace("doc").withValue("TUNE API: GET /getdepartment"))
			.thenApply(new MockResponseTransformer() {
				@Override
				public MuleMessage transform(MuleMessage originalMessage) {
					MuleMessage newMessage = null;
					
					try {
						newMessage = originalMessage.createInboundMessage();
						
						newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
						newMessage.setProperty("Content-Type", "application/json;charset=UTF-8", PropertyScope.INBOUND);
						
							String tuneResponse = IOUtils
									.toString(getClass()
											.getResourceAsStream(
													"/in/get-users-me-vehicles-vin-services-getdepartment-tune-response.json"));
							newMessage.setPayload(IOUtils.toInputStream(tuneResponse, "UTF-8"));
					} catch (Exception e) {
						// catch exception statements
					}
					
					return newMessage;
				}
			});
		
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
				attribute("name").ofNamespace("doc").withValue("TUNE API: GET /getvehicle"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						
						try {
							newMessage = originalMessage.createInboundMessage();
							
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							newMessage.setProperty("Content-Type", "application/json;charset=UTF-8", PropertyScope.INBOUND);
							
							String tuneResponse = IOUtils
									.toString(getClass()
											.getResourceAsStream(
													"/in/get-users-me-vehicles-vin-services-outstanding-tune-response.json"));
							newMessage.setPayload(IOUtils.toInputStream(tuneResponse, "UTF-8"));
						} catch (Exception e) {
							// catch exception statements
						}
						
						return newMessage;
					}
				});
		
		MuleEvent event = testEvent("");
		
		Map<String, Object> props = new HashMap<String,Object>();
		props.put("vin", "6T153BK360X070769");
        props.put("dmsType", dmsType);
		
		MuleMessage msg = muleMessageWithPayload(null);
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
        
        event.setMessage(msg);		
		
		// Invoke the flow
		MuleEvent output = runFlow("get:/users/me/vehicles/{vin}/services/outstanding:myToyota-config", event);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category")
						.withValue("esb.mytoyota-api-v1.get-users-me-vehicles-vin-services-outstanding.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category")
						.withValue("esb.mytoyota-api-v1.get-users-me-vehicles-vin-services-outstanding.response"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.get-users-me-vehicles-vin-services-outstanding-technical-adapter.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.get-users-me-vehicles-vin-services-outstanding-technical-adapter-tune.getdepartment.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.get-users-me-vehicles-vin-services-outstanding-technical-adapter-tune.getdepartment.response"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.get-users-me-vehicles-vin-services-outstanding-technical-adapter-tune.getvehicle.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.get-users-me-vehicles-vin-services-outstanding-technical-adapter-tune.getvehicle.response"))
				.times(1);
//		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
//				.withAttributes(attribute("category").withValue(
//						"esb.mytoyota-api-v1.get-users-me-vehicles-vin-services-outstanding-technical-adapter-tune.getprocedure.request"))
//				.times(1);
//		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
//				.withAttributes(attribute("category").withValue(
//						"esb.mytoyota-api-v1.get-users-me-vehicles-vin-services-outstanding-technical-adapter-tune.getprocedure.response"))
//				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.get-users-me-vehicles-vin-services-outstanding-technical-adapter.response"))
				.times(1);

		//System.out.println("_____________________"+output.getMessage().getPayloadAsString());
		String expectedResponse = IOUtils.toString(getClass().getResourceAsStream("/out/get-users-me-vehicles-vin-services-outstanding-api-response.json"));
		assertJsonEquals(expectedResponse, output.getMessage().getPayloadAsString());
	}
	
	@Test
	public void testNonTuneGetOutstandingServices() throws Exception {
		
		MuleEvent event = testEvent("");
		
		Map<String, Object> props = new HashMap<String,Object>();
		props.put("dmsType", "nonTune");
		
		MuleMessage msg = muleMessageWithPayload(null);
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
        
        event.setMessage(msg);		
		
		// Invoke the flow
        runFlow("get:/users/me/vehicles/{vin}/services/outstanding:myToyota-config", event);
		
				
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category")
						.withValue("esb.mytoyota-api-v1.get-users-me-vehicles-vin-services-outstanding.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category")
						.withValue("esb.mytoyota-api-v1.get-users-me-vehicles-vin-services-outstanding.response"))
				.times(1);

		verifyCallOfMessageProcessor("logger")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Extend with more DMS")).times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.get-users-me-vehicles-vin-services-outstanding-technical-adapter.request"))
				.times(0);

	}
	
	@Test
	public void testFailuresStatusCodes() throws Exception {
		
		int[] statusCodes = new int[] { 404, 405, 415, 406, 401, 500 };	
		
		MuleEvent event = testEvent("");
		
		Map<String, Object> props = new HashMap<String,Object>();
		props.put("vin", "6T153BK360X070769");
        props.put("dmsType", "TUNE");
		
		MuleMessage msg = muleMessageWithPayload(null);
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
        
        event.setMessage(msg);		
		for (int i = 0; i < statusCodes.length; i++) {
			new StatusCodesTestingHelper().runFailureTest(statusCodes[i], "TUNE API: GET /getdepartment",
					"get:/users/me/vehicles/{vin}/services/outstanding:myToyota-config", event);
		}
	}
}
