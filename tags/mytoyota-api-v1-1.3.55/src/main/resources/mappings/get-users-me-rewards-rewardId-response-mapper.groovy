import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper
import au.com.toyota.esb.mytoyota.api.util.GroovyHelper
import java.text.*

def isValidDate(String dateString) {
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
    try {
        df.parse(dateString)
        return true
    } catch (Exception e) {
        return false
    }
}


reward = new JsonSlurper().parseText(flowVars.rewardDetailsPayload)
reward.remove('isSuccessful')
reward.remove('message')
if(reward?.cardFront?.cardImage && !reward?.cardFront?.cardImage.startsWith("http"))
	reward.cardFront.cardImage='https://'+ sitecoreHost + reward.cardFront.cardImage

if(!isValidDate(reward?.expiryDate)) reward.expiryDate = null

if(flowVars.guestCouponPayload != null && !(flowVars.guestCouponPayload instanceof org.mule.transport.NullPayload)) {
	def couponList = new JsonSlurper().parseText(flowVars.guestCouponPayload)?.coupons
	def couponFirstItem = couponList.find { item -> item.dailyClaimedStatus == "VALID" }
	if (couponFirstItem != null && couponFirstItem.caltexId != null) {
		def pagesList = reward.rewardDetails?.pages
		pagesList?.find { pageItem ->
			if (pageItem.pageType == "qr-code") {
				pageItem.offerCode = couponFirstItem.caltexId
				return true
			}
			return false
		}
		
	}
	
}

	
return prettyPrint(toJson(GroovyHelper.removeNulls(reward, true)))