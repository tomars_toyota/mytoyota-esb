
import groovy.json.JsonSlurper
import groovy.json.JsonBuilder

def user = new JsonSlurper().parseText(payload)

def result = [:]
def profile = [:]

if (user?.firstName)
	profile.firstName = user.firstName

if (user?.login)
	result.username = user.login

if (user?.email)
	profile.email = user.email

result.data = [:]

if (user?.applicationIdentifiers){
		user.applicationIdentifiers.each{ k,v ->
		result.data[k] = v
		result.data[k].type = "applicationIdentifier"
	}
}

if (!profile.isEmpty())
	result.profile = profile

return new JsonBuilder(result).toString()