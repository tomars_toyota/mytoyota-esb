import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper

JsonSlurper js = new JsonSlurper()
def timeSlotData = js.parseText(payload).timeSlotData
def calendarDays = timeSlotData.calendarDays
def timeSlots = timeSlotData.timeSlots

def response = [:]
response.availableDays = []

calendarDays.each { Map day ->
	if(day.daySlots > 0) {
		def availableDay = [:]
		inDate = new Date().parse("E dd/MM", day.dayLabel)
		inDate.setYear(new Date().getYear())
		availableDay.date = inDate.format("yyyy-MM-dd")
		availableDay.times = []
		timeSlots.each { Map slot ->
			if(slot.slDay == day.dayNum)
				availableDay.times.add(slot.slTime)
		}
		response.availableDays.add(availableDay)
	}
}

return prettyPrint(toJson(response))