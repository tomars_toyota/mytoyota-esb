import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper

JsonSlurper js = new JsonSlurper()
def dealersList = js.parseText(payload)

def response = [:]
response.dealers = []

dealersList.each { Map item ->
	def dealerOut = [:]


	if(sessionVars['showDeletedOnly'] == true) {
		dealerOut.dealerId = item.dealerId
		dealerOut.branchId = item.branchId
	} else {
		dealerOut.discontinuedDate = item.discontinuedDate ? new Date(item.discontinuedDate).format('yyyy-MM-dd') : null
		dealerOut.favorite = item.favorite
		dealerOut.dealerId = item.dealer?.dealerId
		dealerOut.branchId = item.dealer?.branchId
		dealerOut.address = item.dealer?.address
		dealerOut.postcode = item.dealer?.postcode
		dealerOut.telephone = item.dealer?.telephone
		dealerOut.email = item.dealer?.email
		dealerOut.name = item.dealer?.name
		dealerOut.suburb = item.dealer?.suburb
		dealerOut.state = item.dealer?.state
		dealerOut.fiveStar = item.dealer?.fiveStar
		dealerOut.refX = item.dealer?.refX
		dealerOut.refY = item.dealer?.refY
		dealerOut.externalUri = item.dealer?.externalUri
		dealerOut.service = item.dealer?.service
		dealerOut.sales = item.dealer?.sales
		dealerOut.parts = item.dealer?.parts
		dealerOut.fleetSpecialist = item.dealer?.fleetSpecialist
		dealerOut.used = item.dealer?.used
		dealerOut.toyotaDealerPage = item.dealer?.toyotaDealerPage
	}

	response.dealers << dealerOut.findAll { it.value != null}
}

return prettyPrint(toJson(response))