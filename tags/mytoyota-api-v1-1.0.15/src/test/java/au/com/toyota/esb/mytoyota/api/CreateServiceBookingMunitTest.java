package au.com.toyota.esb.mytoyota.api;

import java.util.HashMap;
import java.util.Map;
import org.junit.Test;

import org.mule.util.StringUtils;
import org.mule.util.IOUtils;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.modules.interceptor.processors.MuleMessageTransformer;

import static org.mule.munit.common.mocking.Attribute.attribute;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;


/*
 * Munit test to unit test the functionality to get service operation time through 
 * /users/me/vehicles/{vin}/services/operationTime
 * @author: ahuwait
 */
public class CreateServiceBookingMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"post-users-me-vehicles-vin-services.xml",
				"post-users-me-vehicles-vin-services-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulCreateServiceBooking() throws Exception {
		
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
			attribute("name").ofNamespace("doc").withValue("TUNE API: POST /createBooking"))
			.thenApply(new MockResponseTransformer() {
				@Override
				public MuleMessage transform(MuleMessage originalMessage) {
					MuleMessage newMessage = null;
					
					try {
						newMessage = originalMessage.createInboundMessage();
						
						newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
						newMessage.setProperty("Content-Type", "application/json;charset=UTF-8", PropertyScope.INBOUND);
						
						String tuneResponse = "{\"bookingData\":[{\"JobCardID\":\"TB16166632\"}]}";
						newMessage.setPayload(IOUtils.toInputStream(tuneResponse, "UTF-8"));
					} catch (Exception e) {
						// catch exception statements
					}
					
					return newMessage;
				}
			});
		
		
		MuleEvent event = testEvent("");
		
		Map<String, Object> props = new HashMap<String,Object>();
		props.put("vin", "6T153BK360X070769");
        props.put("dmsType", "TUNE");
        props.put("dmsID", "61.10169.10169011116");
        
		
        MuleMessage msg = muleMessageWithPayload(IOUtils.toString(getClass()
				.getResourceAsStream("/in/post-booking-to-tune-request.json")));
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
        
        event.setMessage(msg);		
		
		// Invoke the flow
		MuleEvent output = runFlow("post:/users/me/vehicles/{vin}/services:myToyota-config", event);
		
		verifyCallOfMessageProcessor("log-message")
				.ofNamespace("esbutils")
				.withAttributes(
						attribute("name")
								.ofNamespace("doc")
								.withValue(
										"Log Post Dealer ServiceDepartment Services Request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message")
				.ofNamespace("esbutils")
				.withAttributes(
						attribute("name")
								.ofNamespace("doc")
								.withValue(
										"Log Post Dealer ServiceDepartment Services Response"))
				.times(1);

		verifyCallOfMessageProcessor("log-message")
				.ofNamespace("esbutils")
				.withAttributes(
						attribute("name").ofNamespace("doc").withValue(
								"Log Request - tech adapter")).times(1);
		verifyCallOfMessageProcessor("log-message")
				.ofNamespace("esbutils")
				.withAttributes(
						attribute("name").ofNamespace("doc").withValue(
								"Log Response - tech adapter")).times(1);
		
//		System.out.println("_____________________"+output.getMessage().getPayloadAsString());
		assertJsonEquals("{\"bookingId\":\"TB16166632\"}", output.getMessage().getPayloadAsString());

	}
	
	//not possible to externalise this class because of the visibility of the method muleMessageWithPayload
	private class MockResponseTransformer implements MuleMessageTransformer {
		int count = 0;
		String[] payloads;

		public MockResponseTransformer(String... payloads) {
			this.payloads = payloads;
		}

		@Override
		public MuleMessage transform(MuleMessage originalMessage) {
			return muleMessageWithPayload(payloads[count++]);
		}
	}
	
}
