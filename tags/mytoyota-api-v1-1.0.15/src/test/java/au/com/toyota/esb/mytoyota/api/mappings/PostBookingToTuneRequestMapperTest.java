package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

import javax.script.*;
import org.junit.Assert;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.xml.sax.SAXException;

public class PostBookingToTuneRequestMapperTest extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;

	public PostBookingToTuneRequestMapperTest() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner
				.createScriptRunner("/mappings/post-booking-to-tune-request-mapper.groovy");
	}

	@Test
	public void testResponseMapper() throws Exception {
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(getClass().getResourceAsStream("/in/post-booking-to-tune-request.json")))
				.flowVar("dmsID","61.10169.10169011116")
				.flowVar("vin","JTMBFREV80D078597")
				.run().toString();

		System.out.println(result);

		// expected
		String expectedResult = "SiteID=61.10169.10169011116&VIN=JTMBFREV80D078597&Service=TOY9983-S0000CVI-1%7CSCHEDULE%7Cfalse%7C150%7C0D1D33%7CADVASA44R++++++1003"
				+ "&BookDate=736451&BookTime=27000&OutDate=736451&OutTime=46800&ConfType=E&ConfTo=dealer%40toyota.com.au&SrvPrc=250&UpSells=0x0000000007ae1441|0x0000000007ae1488"
				+ "&Notes=Please+check+the+wipers&Wait=false&CourtesyBus=false&Loan=true";
		Assert.assertEquals(expectedResult, result);
	}
}
