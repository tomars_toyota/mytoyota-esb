package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

import javax.script.*;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

public class PutVehiclesVinServicesServiceIdRequestMapperTest extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;

	public PutVehiclesVinServicesServiceIdRequestMapperTest() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner
				.createScriptRunner("/mappings/put-vehicles-vin-services-serviceId-request-mapper.groovy");
	}
	
	//@Test
	public void testSuccessful() throws Exception {
		String expectedResult = IOUtils.toString(
				getClass().getResourceAsStream("/out/put-vehicles-vin-services-serviceId-mykit-request.json"));
		String payload = IOUtils
				.toString(getClass().getResourceAsStream("/in/put-vehicles-vin-services-serviceId-request.json"));
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(payload)
				.flowVar("dmsID","61.10169.10169011116")
				.flowVar("dmsType","TUNE")
				.flowVar("serviceId", "TB16392302")
				.run().toString();

		System.out.println(result);
		JSONAssert.assertEquals(expectedResult, result, true);
	}
	
	//@Test
	public void testUnsuccessful() throws Exception {
		String payload = IOUtils
				.toString(getClass().getResourceAsStream("/in/put-vehicles-vin-services-request-unsuccessful.json"));
		try { 
			ScriptRunBuilder
				.runner(scriptRunner)
				.payload(payload)
				.flowVar("dmsID","61.10169.10169011116")
				.flowVar("dmsType","TUNE")
				.flowVar("serviceId", "TB16392302")
				.run().toString();
		} catch (Exception e) {
			if(e.getMessage().contains("Invalid request"))
				assert(true);
			else
				assert(false);
			return;
		}
		assert(false);
	}
}
