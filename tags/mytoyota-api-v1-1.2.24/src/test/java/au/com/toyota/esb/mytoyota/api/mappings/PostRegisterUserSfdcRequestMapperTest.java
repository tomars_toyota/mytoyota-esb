package au.com.toyota.esb.mytoyota.api.mappings;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

import javax.script.*;

import org.apache.commons.io.IOUtils;
import org.custommonkey.xmlunit.Diff;
import org.custommonkey.xmlunit.XMLUnit;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

public class PostRegisterUserSfdcRequestMapperTest extends AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;
	
	@BeforeClass
	public static void setupBeforeClass() throws Exception {
		// XMLUnit setup
		XMLUnit.setIgnoreWhitespace(true);
		XMLUnit.setIgnoreDiffBetweenTextAndCDATA(true);
	}
	public PostRegisterUserSfdcRequestMapperTest() throws ScriptException, SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/post-register-user-sfdc-provider-request-mapper.groovy");
	}
	
	//@Test
	public void testRequestMapper() throws Exception {
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(getClass().getResourceAsStream(
						"/in/post-register-user-request.json")))
						.flowVar("myToyotaId", "myToyotaId")
						.flowVar("vin", "123123")
				.run().toString();
		System.out.println(result);
		String xml = groovy.xml.XmlUtil.serialize(result);
		// expected
		String expectedResult = IOUtils.toString(getClass().getResourceAsStream(
				"/out/post-register-user-sfdc-request.xml"));
		Diff diff = new Diff(expectedResult, xml);
		assertTrue(diff.similar());
	}
	
}
