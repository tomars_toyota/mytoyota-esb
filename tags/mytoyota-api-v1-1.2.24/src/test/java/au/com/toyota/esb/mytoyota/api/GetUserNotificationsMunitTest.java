package au.com.toyota.esb.mytoyota.api;

import static org.junit.Assert.assertEquals;
import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.List;

import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.util.StringUtils;

import com.jayway.jsonpath.JsonPath;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;


/*
 * Munit test to unit test the functionality to get user notifications
 * @author: ibrahim.abouelela
 */
public class GetUserNotificationsMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"get-users-notifications.xml",
				"get-users-notifications-technical-adapter.xml",
				"config.xml"
		}, " ");
	}
	
	//@Test
	public void testSuccessfulRequest() throws Exception {

		String myToyotaId = "MYT-00000005";
		
		whenMessageProcessor("request").ofNamespace("http")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("My Toyota App API GET Retrieve Users notifications"))
			.thenApply(new MockResponseTransformer() {
			@Override
			public MuleMessage transform(MuleMessage originalMessage) {
				MuleMessage newMessage = null;
				
				try {
					newMessage = originalMessage.createInboundMessage();
					
					newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
					
					newMessage.setPayload(getClass().getResourceAsStream("/in/get-user-notifications-mykit-response.json"));
				} catch (Exception e) {
					// catch exception statements
				}
				
				return newMessage;
			}
		});

		MuleEvent event = testEvent("");
		event.setFlowVariable("myToyotaId", myToyotaId);

		MuleEvent output = runFlow("get:/users/me/notifications:myToyota-config", event);
		String payload = output.getMessage().getPayloadAsString();
		System.out.println(payload);
		
		verifyCallOfMessageProcessor("log-message")
			.ofNamespace("utils")
			.withAttributes(
					attribute("name")
							.ofNamespace("doc")
							.withValue(
									"Log API Request"))
			.times(1);
		verifyCallOfMessageProcessor("log-message")
			.ofNamespace("utils")
			.withAttributes(
					attribute("name")
							.ofNamespace("doc")
							.withValue(
									"Log API Response"))
			.times(1);
		
		List<String> notifications = JsonPath.read(payload, "$.notifications");
		assertEquals(3, notifications.size());
		
		assertEquals(new Integer(5951), JsonPath.read(payload, "$.notifications[1].id"));
		assertEquals("INFO", JsonPath.read(payload, "$.notifications[1].category"));
		assertEquals(new Long("145144165330"), JsonPath.read(payload, "$.notifications[1].expiryDate"));
		
		assertEquals("TAKE_A_BREAK3", JsonPath.read(payload, "$.notifications[2].code"));
		assertEquals(false, JsonPath.read(payload, "$.notifications[2].closable"));
		assertEquals("http://redirecturi3.com", JsonPath.read(payload, "$.notifications[2].redirectUri"));
		
	}
	
	/*//@Test
	public void unsuccessfulRequest() throws Exception {

		final String vin = "ABC123";
		final String requestPath = "/api/myToyota/v1/vehicles/" + vin;

		whenMessageProcessor("request").ofNamespace("http")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("Vehicle API GET vehicle by vin"))
			.thenThrow(new org.mule.module.apikit.exception.NotFoundException(requestPath));
		
		MuleEvent event = testEvent(requestPath);
		event.setFlowVariable("vin", vin);

		try {
			runFlow("get:/vehicles/{vin}:myToyota-config", event);
		} catch (Exception e) {
			assertTrue(e.getCause() instanceof org.mule.module.apikit.exception.NotFoundException);
			assertEquals(requestPath, e.getCause().getMessage());
		}

	}*/
	
	//@Test
	public void testFailuresStatusCodes() throws Exception {
		
		int[] statusCodes = new int[] { 404, 405, 415, 406, 401, 500 };	
		
		MuleEvent event = testEvent("");
		String myToyotaId = "MYT-00000005";
		event.setFlowVariable("myToyotaId", myToyotaId);
		
		for (int i = 0; i < statusCodes.length; i++) {
			new StatusCodesTestingHelper().runFailureTest(statusCodes[i], "My Toyota App API GET Retrieve Users notifications",
					"get:/users/me/notifications:myToyota-config", event);
		}
	}
}
