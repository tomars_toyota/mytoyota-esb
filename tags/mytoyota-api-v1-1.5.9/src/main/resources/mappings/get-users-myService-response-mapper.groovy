import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper
import au.com.toyota.esb.mytoyota.api.util.GroovyHelper

user = [:]

// For Testing only Generate an email for new Registrations
/*Date dateNow = new Date()
def emailPrefix = dateNow.format("yyyMMddHHmmssSSS")


// This is to test the existing user
if (flowVars.refId == "123456") {
	user.firstName = "OSB myToyota"
	user.lastName = "Existing"
	user.email = "mytoyotatest2+56@gmail.com"

}
else {
	user.firstName = "OSB Firstname"
	user.lastName = "Lastname"
	user.email = "mytoyotatest2+" + emailPrefix + "@gmail.com"

}*/

JsonSlurper js = new groovy.json.JsonSlurper()
def parsedPayload = js.parseText(payload)

if (parsedPayload?.contact) {
	user.firstName = parsedPayload.contact.firstName
	user.lastName = parsedPayload.contact.lastName
	user.email = parsedPayload.contact.email
	//user.dateOfBirth = "1984-10-13"
	user.mobile = parsedPayload.contact.preferredContactNo
}


return prettyPrint(toJson(user))

