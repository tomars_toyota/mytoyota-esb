import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper

JsonSlurper js = new JsonSlurper()
def timeSlotData = js.parseText(payload).timeSlotData
def calendarDays = timeSlotData.calendarDays
def timeSlots = timeSlotData.timeSlots
def tuneDateOffset = 2415022

def response = [:]
response.availableDays = []

calendarDays.each { Map day ->
	if(day.daySlots > 0) {
		def availableDay = [:]
		//inDate = new Date().parse("E dd/MM", day.dayLabel)
		//inDate.setYear(new Date().getYear())

		def inDate
		outTimeSlots = []
		
		timeSlots.each { Map slot ->
			if(slot.slDay == day.dayNum) {
				outTimeSlots.add(slot.slTime)
				inDate = new GregorianCalendar(1900, Calendar.JANUARY, 1, 0, 0, 0).time + (slot.slDat - tuneDateOffset)
			}
		}
		availableDay.date = inDate.format("yyyy-MM-dd")
		availableDay.times = outTimeSlots
		response.availableDays.add(availableDay)
	}
}

return prettyPrint(toJson(response))