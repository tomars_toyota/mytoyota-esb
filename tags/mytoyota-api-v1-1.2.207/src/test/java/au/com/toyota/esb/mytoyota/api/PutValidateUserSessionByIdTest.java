package au.com.toyota.esb.mytoyota.api;

import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.ArrayList;
import java.util.HashMap;

import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.util.IOUtils;
import org.mule.util.StringUtils;

import au.com.toyota.esb.mytoyota.api.munit.BlockingMuleEventSpy;
import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;

public class PutValidateUserSessionByIdTest extends FunctionalMunitSuite {
	
	private String token = "eyJjdHkiOiJKV1QiLCJlbmMiOiJBMjU2R0NNIiwiYWxnIjoiZGlyIn0.."
			+ "BZIfgmYPXx4PM-bq.YbrfGIXdE0urI4fRf2XK4iOtfEhQUiLNZ7Vq4U6rSCHZnc"
			+ "cwcc7blVbvMCcVm6Qb6gzQfdx0MXX1vl0fJPBJ9X5NB-laSk18dBwl1NUhLsOWI"
			+ "TUN3ovUSCvbP_4WKWHeBRkvyHsSWWEnJAh3pB6DWyFNF7yCBlzHYu0NDjhCBrqZ8"
			+ "lzT7G7cLUZYaGbmRB96Go46GCxLegPj6iiTVk48sotpRoPKOgpzRtUiAvoQnDvCx"
			+ "wuWT2XAh2T0T6SmunJrV0ITAup3klEy9eI38SIj5YblQvNjE7fD_oV8203klX1QxE"
			+ "2ZyAL42d5v1oAgXTp2Vip7gv6xyNDIQujgpwsRyaAiXCS2IBCjcCzo7bU__qxcAYQ"
			+ "2L7fsWg.9ZrmZrtMYlWNeI-qAgLk6w";
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"put-validate-user-session-by-id.xml",
				"put-validate-user-session-by-id-technical-adapter.xml",
				"session-management-im-provider-interactions.xml",
				"get-user-account-info-technical-adapter.xml",
				"config.xml"
		}, " ");
	}
	
	@Test
	public void testSuccessfulUpdateSessionUserHasTokenList() throws Exception {
		assert (!testSuccessfulUpdateSessionImpl("get-user-account-info-im-response.json").contains(token));
	}
	
	@Test
	public void testSuccessfulUpdateSessionUserHasTokenMap() throws Exception {
		ArrayList<String> myToyotaSessionTokenList = testSuccessfulUpdateSessionImpl(
				"get-user-account-info-token-map-im-response.json");
		assert(myToyotaSessionTokenList.size() == 1);
		assert(!myToyotaSessionTokenList.contains(token));
	}
	
	
	@SuppressWarnings("unchecked")
	private ArrayList<String> testSuccessfulUpdateSessionImpl(String getInfoResponseFile) throws Exception {
		
		mockImApiResponses(token, getInfoResponseFile);
		

		BlockingMuleEventSpy<Object> appAPIRequestMessageSpy = new BlockingMuleEventSpy<Object>(1, 5000);
		spyMessageProcessor("component").ofNamespace("scripting")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Remove current APISessionToken from the list"))
				.before(appAPIRequestMessageSpy);

		MuleMessage message = muleMessageWithPayload(null);
		message.setProperty("APISessionToken", token, PropertyScope.INBOUND);
		
		MuleEvent event = testEvent("");
		event.setMessage(message);
		
		// Invoke the flow
		runFlow("put:/sessions/{id}:myToyota-config", event);
		
		appAPIRequestMessageSpy.block();

		HashMap<String, Object> mappedPaylod =  (HashMap<String, Object>)appAPIRequestMessageSpy.getFirstPayload();
		
		System.out.println("______ mappedPaylod: "+ mappedPaylod);
	
		return (ArrayList<String>) ((HashMap<String, Object>) mappedPaylod.get("data")).get("myToyotaSessionToken");
	}
	
	
	private void mockImApiResponses(final String token, final String getInfoResponseFile) throws Exception {
		
		// Mock get-user-account-info-im-technical-adapter flow-ref
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("IM API Get User"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							String responsePayload = IOUtils.toString(
									getClass().getResourceAsStream("/in/"+ getInfoResponseFile));
							newMessage.setPayload(responsePayload);
							newMessage.setProperty("id", token, PropertyScope.INVOCATION);
						} catch (Exception e) {
							// catch exception statements
						}
						return newMessage;
					}
				});
		
		//Mock IM API Update user
		whenMessageProcessor("request").ofNamespace("http")
		.withAttributes(attribute("name").ofNamespace("doc").withValue("IM API Update User"))
		.thenApply(new MockResponseTransformer() {
			@Override
			public MuleMessage transform(MuleMessage originalMessage) {
				MuleMessage newMessage = null;

				try {
					newMessage = originalMessage.createInboundMessage();
					newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
					newMessage.setProperty("token", token, PropertyScope.INVOCATION);
				} catch (Exception e) {
					// catch exception statements
				}
				return newMessage;
			}
		});
	}
}
