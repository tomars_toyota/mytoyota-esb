import groovy.json.JsonSlurper
import static groovy.json.JsonOutput.*
import au.com.toyota.esb.mytoyota.api.util.GroovyHelper
			
if(flowVars['userAccount'])
	userAccount = new JsonSlurper().parseText(flowVars['userAccount']) 

vehicle = null
if(!(payload instanceof org.mule.transport.NullPayload))
	vehicle = new JsonSlurper().parseText(payload)

accountSummary = [:]
accountSummary.firstName = userAccount?.firstName
accountSummary.lastName = userAccount?.lastName
accountSummary.vin = flowVars['vin']
accountSummary.vehicleDescription = vehicle?.vehicleDescription

accountSummary = GroovyHelper.removeNulls(accountSummary)

return prettyPrint(toJson(accountSummary))