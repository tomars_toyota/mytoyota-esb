import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper

vehicleRequest = new JsonSlurper().parseText(payload)

vehicle = [:]
vehicle.isVerified = vehicleRequest?.isVerified
vehicle.discontinuedDate = vehicleRequest?.discontinuedDate? new Date().parse('yyyy-MM-dd', vehicleRequest.discontinuedDate).time : null

if(vehicle.discontinuedDate)
	flowVars['updateSfdc'] = true
else
	flowVars['updateSfdc'] = false

//remove nulls
vehicle = vehicle.findAll {it.value != null }

if(!vehicle)
	throw new org.mule.module.apikit.exception.BadRequestException('Invalid request')

request = [:]
request.vehicleUsers = []
request.vehicleUsers << vehicle

return prettyPrint(toJson(request))