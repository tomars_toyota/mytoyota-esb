import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper
import au.com.toyota.esb.mytoyota.api.util.GroovyHelper

def request = [:]

def memberId = flowVars.myToyotaId + memberIdDelimiter + flowVars.unregisteredVehicle.vin
request.memberId = memberId
request.vin = flowVars.unregisteredVehicle.vin
request.guestReward = [:]
request.guestReward.rewardType = "caltex"

if (payload) {
	
	def vehicleInfo = new JsonSlurper().parseText(payload)
	if (vehicleInfo != null) {
		request.additionalInfo = [:]
		request.additionalInfo.make = vehicleInfo?.makeDescription
		request.additionalInfo.model = vehicleInfo?.model?.description
		request.additionalInfo.grade = vehicleInfo?.suffix?.description
		request.additionalInfo.registrationNo = flowVars.unregisteredVehicle.userRegistrationNumber
		request.additionalInfo.state = flowVars.unregisteredVehicle.state

	}

}


return toJson(request)