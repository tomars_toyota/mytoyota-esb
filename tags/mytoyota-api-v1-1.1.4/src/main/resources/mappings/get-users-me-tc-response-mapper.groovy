import groovy.json.JsonSlurper
import groovy.json.JsonBuilder

def acceptedTcVersion = new JsonSlurper().parseText(payload).data?.myToyotaAcceptedTerms?.acceptedVersion

if (acceptedTcVersion == null)
	throw new org.mule.module.apikit.exception.NotFoundException("No available accepted terms and conditions version")


def response = [:]

response.channel = "myToyota"
response.acceptedTcVersion = acceptedTcVersion


return new JsonBuilder(response).toString()