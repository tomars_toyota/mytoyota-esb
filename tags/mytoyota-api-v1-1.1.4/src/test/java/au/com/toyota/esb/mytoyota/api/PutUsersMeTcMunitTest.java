package au.com.toyota.esb.mytoyota.api;

import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.util.StringUtils;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;


/*
 * Munit test to unit test the functionality to update user's T&C agreed version 
 * PUT: /users/me/tc
 * @author: ahuwait
 */
public class PutUsersMeTcMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"put-users-me-tc.xml",
				"put-users-me-tc-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulPutTc() throws Exception {
		
		whenMessageProcessor("request").ofNamespace("http")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("IM API Update User's TC"))
			.thenApply(new MockResponseTransformer() {
				@Override
				public MuleMessage transform(MuleMessage originalMessage) {
					MuleMessage newMessage = null;
					
					try {
						newMessage = originalMessage.createInboundMessage();
						newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
					} catch (Exception e) {
						// catch exception statements
					}
					
					return newMessage;
				}
			});
		
		
		MuleEvent event = testEvent("");
		
		Map<String, Object> props = new HashMap<String,Object>();
		props.put("acceptedTcVersion", "3.0");
		
		MuleMessage msg = muleMessageWithPayload(null);
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
        
        event.setMessage(msg);		
		
		// Invoke the flow
		runFlow("put:/users/me/tc:myToyota-config", event);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
			.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.put-users-me-tc.request"))
			.times(1);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
			.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.put-users-me-tc.response"))
			.times(1);	

		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
			.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.put-users-me-tc-technical-adapter.request"))
			.times(1);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
			.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.put-users-me-tc-technical-adapter.response"))
			.times(1);	
	}
	
	@Test
	public void testFailuresStatusCodes() throws Exception {

		int[] statusCodes = new int[] { 404, 405, 415, 406, 401, 500 };
		
		Map<String, Object> props = new HashMap<String,Object>();
		props.put("acceptedTcVersion", "3.0");
		
		MuleMessage msg = muleMessageWithPayload(null);
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
		MuleEvent event = testEvent("");
		event.setMessage(msg);
		
		for (int i = 0; i < statusCodes.length; i++) {
			new StatusCodesTestingHelper().runFailureTest(statusCodes[i], "IM API Update User's TC",
					"put:/users/me/tc:myToyota-config", event);
		}
	}
}
