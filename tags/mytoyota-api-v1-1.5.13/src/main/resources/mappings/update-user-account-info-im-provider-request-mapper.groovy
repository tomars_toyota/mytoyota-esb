
import groovy.json.JsonSlurper
import groovy.json.JsonBuilder

def user = new JsonSlurper().parseText(payload)

def result = [:]
def profile = [:]

if (user?.firstName)
	profile.firstName = user.firstName

if (user?.login)
	result.username = user.login

if (user?.email)
	profile.email = user.email

result.data = [:]

if (user?.applicationIdentifiers){
		user.applicationIdentifiers.each{ k,v ->
		result.data[k] = v
		result.data[k].type = "applicationIdentifier"
	}
}

if (user?.termsAndConditions) {
	result.data['myToyotaAcceptedTerms'] = ['acceptedVersion':user?.termsAndConditions['acceptedVersion']]
}

if (user?.privacyStatement) {
	result.data['myToyotaAcceptedPrivacyPolicy'] = ['acceptedVersion':user?.privacyStatement['acceptedVersion']]
}

if (!profile.isEmpty())
	result.profile = profile

if(user?.UID != null)
    result.UID = user.UID
if(user?.isVerified != null)
    result.isVerified = user.isVerified

return new JsonBuilder(result).toString()