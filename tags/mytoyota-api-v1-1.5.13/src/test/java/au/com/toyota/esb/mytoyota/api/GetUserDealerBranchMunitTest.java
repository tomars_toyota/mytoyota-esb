package au.com.toyota.esb.mytoyota.api;

import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.junit.Assert.assertEquals;
import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.Properties;

import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.module.http.internal.request.ResponseValidatorException;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.util.IOUtils;
import org.mule.util.StringUtils;

import au.com.toyota.esb.mytoyota.api.munit.BlockingMuleEventSpy;
import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;


/*
 * Munit test to unit test the functionality to get branch of a user's dealer
 * @author: swapnil
 */
public class GetUserDealerBranchMunitTest extends FunctionalMunitSuite {
	
	private static Properties adapterProperties = new Properties();

	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"get-user-dealer-branch.xml",
				"get-user-dealer-branch-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulGetUserDealerBranch() throws Exception {
		
		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));

		final String myToyotaAPIResponsePayload = IOUtils.toString(getClass().getResourceAsStream("/in/get-user-dealer-branch-mytoyotaapi-response.json"));
		final String responsePayload = IOUtils.toString(getClass().getResourceAsStream("/in/get-user-dealer-branch-response.json"));
		
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
			attribute("name").ofNamespace("doc").withValue("My Toyota App API Get User Dealer Branch"))
			.thenApply(new MockResponseTransformer() {
				@Override
				public MuleMessage transform(MuleMessage originalMessage) {
					MuleMessage newMessage = null;
					
					try {
						newMessage = originalMessage.createInboundMessage();
						
						newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
						newMessage.setProperty("Content-Type", "application/json;charset=UTF-8", PropertyScope.INBOUND);
						
						newMessage.setPayload(IOUtils.toInputStream(myToyotaAPIResponsePayload, "UTF-8"));
					} catch (Exception e) {
						// catch exception statements
					}
					
					return newMessage;
				}
			});
		
		BlockingMuleEventSpy<String> appAPIRequestMessageSpy = new BlockingMuleEventSpy<String>(1, 5000);
		spyMessageProcessor("log-message").ofNamespace("utils").withAttributes(
			attribute("name").ofNamespace("doc").withValue("Log Get User Dealer Branch App Request"))
			.after(appAPIRequestMessageSpy);

		MuleEvent event = testEvent("");

		// Invoke the flow
		MuleEvent output = runFlow("get:/users/me/dealers/{dealerId}/branches/{branchId}:myToyota-config", event);
		
		appAPIRequestMessageSpy.block();

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Get User Dealer Branch App Request")).times(1);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Get User Dealer Branch App Response")).times(1);	

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Get User Dealer Branch App Request")).times(1);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Get User Dealer Branch App Response")).times(1);	

		MuleEvent appAPIRequestMessageMuleEvent = appAPIRequestMessageSpy.getMuleEvent();
			
		// Assert app api request
		assertEquals(adapterProperties.getProperty("api.mytoyota-app.authTokenHeaderValue"), 
			appAPIRequestMessageMuleEvent.getMessage().getProperty(adapterProperties.getProperty("api.mytoyota-app.authTokenHeader"), PropertyScope.OUTBOUND));
		
		assertEquals("", appAPIRequestMessageSpy.getFirstPayload());

		// Assert api response - No need to check http.status (taken care by APIKit)
		//assertEquals(200, Integer.parseInt(output.getMessage().getOutboundProperty("http.status").toString()));
		//assertEquals("application/json", output.getMessage().getOutboundProperty("Content-Type"));
		System.out.println("output.getMessage(): "+output.getMessage());
		assertJsonEquals(responsePayload, output.getMessage().getPayloadAsString());

	}
	
	@Test
	public void testFailureGetUserDealerBranch() throws Exception {
		
		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));

		MuleEvent event = testEvent(null);

		whenMessageProcessor("request").ofNamespace("http").withAttributes(
			attribute("name").ofNamespace("doc").withValue("My Toyota App API Get User Dealer Branch"))
			.thenThrow(new java.lang.Exception("Response code 400 mapped as failure."));
			
		try {

			// Invoke the flow
			runFlow("get:/users/me/dealers/{dealerId}/branches/{branchId}:myToyota-config", event);
			
			// validating exception thrown
			assertEquals(true, false);

		} catch (java.lang.Exception e) {

			
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Get User Dealer Branch App Request")).times(1);	
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Get User Dealer Branch App Response")).times(0);	

			verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Get User Dealer Branch App Request")).times(1);	
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Get User Dealer Branch App Response")).times(0);
			
		}

	}
	
	@Test
	public void testFailuresStatusCodes() throws Exception {
		
		int[] statusCodes = new int[] { 404, 405, 415, 406, 401, 500 };	
		
		for (int i = 0; i < statusCodes.length; i++) {
			new StatusCodesTestingHelper().runFailureTest(statusCodes[i], "My Toyota App API Get User Dealer Branch",
					"get:/users/me/dealers/{dealerId}/branches/{branchId}:myToyota-config");
		}
	}
}
