import groovy.json.JsonSlurper
import org.mule.api.transport.PropertyScope;

if(payload && !(payload instanceof org.mule.transport.NullPayload))
	if(payload instanceof String)
		sessionVars['requestPreferencesMap'] = new JsonSlurper().parseText(payload)
	else
		sessionVars['requestPreferencesMap'] = new JsonSlurper().parse(payload)


params = message.getProperty("http.query.params", PropertyScope.INBOUND)

//Fix an issue whereby the incoming email with '+' has been translated to a ' ' space
if(params['email'] && params['statusEmail']) {
	sessionVars['email'] = params['email'].replaceAll(" ", "+")
	flowVars['statusEmail'] = params['statusEmail']
} else if(params['mobile']) {
	//SMS unsubscribe
	sessionVars['mobile'] = params['mobile']
	
	if(!sessionVars['requestPreferencesMap'])
		sessionVars['requestPreferencesMap'] = [:]
	sessionVars['requestPreferencesMap'].trackMyCarSMSOptIn = false
	flowVars['statusEmail'] = 'true'
} else
	throw new org.mule.module.apikit.exception.BadRequestException('Missing mobile, or email and statusEmail query paramters')

return payload