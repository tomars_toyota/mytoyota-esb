// Helper to remove leading zeroes
// Used when checking dealer code against known codes that support service booking
def stripLeadingZeros = { s ->
	return s.replaceFirst("^0*", "")
}

//validate request
if(!(payload.containsKey('vin') &&
	 payload.containsKey('batch')))
	throw new org.mule.module.apikit.exception.BadRequestException('Invalid request, VIN and Batch expected')

sessionVars['receivedVin'] = payload?.vin
sessionVars['receivedBatch'] = payload?.batch

return payload