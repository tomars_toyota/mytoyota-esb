package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;

import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.custommonkey.xmlunit.XMLTestCase;
import org.junit.Test;
import org.xml.sax.SAXException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

public class UpdateSfdcUserRequestMapper extends XMLTestCase {

	private ScriptRunner scriptRunner;

	public UpdateSfdcUserRequestMapper() throws ScriptException, SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/sfdc-update-user-request-mapper.groovy");
	}

	@Test
	public void testRequestMapper() throws Exception {

		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload("")
				.flowVar("requestPayload",
						IOUtils.toString(getClass().getResourceAsStream("/in/update-sfdc-user.json")))
				.flowVar("myToyotaId", "MYT-000003a3")
				.run().toString();

		System.out.println(result);

		// expected
		String expectedResult = IOUtils.toString(getClass().getResourceAsStream("/out/updatOwner-sfdc-request.xml"));
		assertXMLEqual(expectedResult, result);
	}
}
