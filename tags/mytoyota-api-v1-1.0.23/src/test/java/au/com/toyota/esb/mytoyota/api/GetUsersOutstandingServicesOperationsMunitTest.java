package au.com.toyota.esb.mytoyota.api;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.mule.util.StringUtils;
import org.mule.util.IOUtils;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.modules.interceptor.processors.MuleMessageTransformer;

import static org.mule.munit.common.mocking.Attribute.attribute;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;


/*
 * Munit test to unit test the functionality to get outstanding services through 
 * /users/me/vehicles/{vin}/services/outstanding
 * @author: ahuwait
 */
public class GetUsersOutstandingServicesOperationsMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"get-users-me-vehicles-vin-services-outstanding-operations.xml",
				"get-users-me-vehicles-vin-services-outstanding-operations-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulGetOutstandingServices() throws Exception {
		testSuccessfulGetOutstandingServicesImpl("TUNE");
	}
	
	@Test
	public void testSuccessfulGetOutstandingServicesTuneLowerCase() throws Exception {
		testSuccessfulGetOutstandingServicesImpl("tune");
	}
	
	private void testSuccessfulGetOutstandingServicesImpl(String dmsType) throws Exception {
		
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
				attribute("name").ofNamespace("doc").withValue("TUNE API: GET /getprocedure"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						
						try {
							newMessage = originalMessage.createInboundMessage();
							
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							newMessage.setProperty("Content-Type", "application/json;charset=UTF-8", PropertyScope.INBOUND);
							
							String tuneResponse = IOUtils
									.toString(getClass()
											.getResourceAsStream(
													"/in/get-users-me-vehicles-vin-services-getprocedure-tune-response.json"));
							newMessage.setPayload(IOUtils.toInputStream(tuneResponse, "UTF-8"));
						} catch (Exception e) {
							// catch exception statements
						}
						
						return newMessage;
					}
				});
		
		MuleEvent event = testEvent("");
		
		Map<String, Object> props = new HashMap<String,Object>();
		props.put("vin", "6T153BK360X070769");
        props.put("dmsType", dmsType);
        props.put("dmsID", "61.10169.10169011116");
        props.put("operationTypeId", "SCHEDULE");
        props.put("operationId", "TOY30895-S0750M45S-1");
        
		
		MuleMessage msg = muleMessageWithPayload(null);
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
        
        event.setMessage(msg);		
		
		// Invoke the flow
		MuleEvent output = runFlow("get:/users/me/vehicles/{vin}/services/outstanding/operations:myToyota-config", event);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category")
						.withValue("esb.mytoyota-api-v1.get-users-me-vehicles-vin-services-operations.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category")
						.withValue("esb.mytoyota-api-v1.get-users-me-vehicles-vin-services-operations.response"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.get-users-me-vehicles-vin-services-operations-technical-adapter.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.get-users-me-vehicles-vin-services-operations-technical-adapter.response"))
				.times(1);
		
		//System.out.println("_____________________"+output.getMessage().getPayloadAsString());
		String expectedResponse = IOUtils.toString(getClass().getResourceAsStream("/out/get-users-me-vehicles-vin-services-outstanding-operations-api-response.json"));
		assertJsonEquals(expectedResponse, output.getMessage().getPayloadAsString());
	}
	
	@Test
	public void testNonTuneGetOutstandingServices() throws Exception {
		
		MuleEvent event = testEvent("");
		
		Map<String, Object> props = new HashMap<String,Object>();
		props.put("dmsType", "nonTune");
		props.put("dmsID", "61.10169.10169011116");
        props.put("operationTypeId", "SCHEDULE");
        props.put("operationId", "TOY30895-S0750M45S-1");
		
		MuleMessage msg = muleMessageWithPayload(null);
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
        
        event.setMessage(msg);		
		
		// Invoke the flow
        runFlow("get:/users/me/vehicles/{vin}/services/outstanding/operations:myToyota-config", event);
		
				
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category")
						.withValue("esb.mytoyota-api-v1.get-users-me-vehicles-vin-services-operations.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category")
						.withValue("esb.mytoyota-api-v1.get-users-me-vehicles-vin-services-operations.response"))
				.times(1);

		verifyCallOfMessageProcessor("logger")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Extend with more DMS")).times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.get-users-me-vehicles-vin-services-operations-technical-adapter.request"))
				.times(0);

	}
	
	//not possible to externalise this class because of the visibility of the method muleMessageWithPayload
	private class MockResponseTransformer implements MuleMessageTransformer {
		int count = 0;
		String[] payloads;

		public MockResponseTransformer(String... payloads) {
			this.payloads = payloads;
		}

		@Override
		public MuleMessage transform(MuleMessage originalMessage) {
			return muleMessageWithPayload(payloads[count++]);
		}
	}
	
}
