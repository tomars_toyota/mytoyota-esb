package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

import javax.script.*;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;
import static org.junit.Assert.assertTrue;

public class GetUserVehicleMyKitResponseMapper extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;

	public GetUserVehicleMyKitResponseMapper() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/get-user-vehicles-mykit-provider-response-mapper.groovy");
	}

	 @Test
	public void testSuccessfulResponseMapper() throws Exception {
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(getClass().getResourceAsStream("/in/get-user-vehicles-mykit-response.json")))
				.run().toString();

		System.out.println(result);

		// expected
		String expectedResult = IOUtils.toString(getClass()
				.getResourceAsStream(
						"/out/get-user-vehicles-api-response.json"));
		JSONAssert.assertEquals(expectedResult, result, true);
	}
}
