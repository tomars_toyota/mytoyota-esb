package au.com.toyota.esb.mytoyota.api;

import java.util.HashMap;
import java.util.Map;
import org.junit.Test;

import org.mule.util.StringUtils;
import org.mule.util.IOUtils;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.modules.interceptor.processors.MuleMessageTransformer;

import static org.mule.munit.common.mocking.Attribute.attribute;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;


/*
 * Munit test to unit test the functionality to get service operation time through 
 * /users/me/vehicles/{vin}/services/operationTime
 * @author: ahuwait
 */
public class PostUsersMeDealersDealerIdVehiclesMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"post-users-me-dealers-dealerId-vehicles.xml",
				"post-users-me-dealers-dealerId-vehicles-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulCreateServiceBooking() throws Exception {
		testSuccessfulCreateServiceBookingImpl("TUNE");
	}
	
	@Test
	public void testSuccessfulCreateServiceBookingTuneLowerCase() throws Exception {
		testSuccessfulCreateServiceBookingImpl("tune");
	}
	
	private void testSuccessfulCreateServiceBookingImpl(String dmsType) throws Exception {
		
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
			attribute("name").ofNamespace("doc").withValue("TUNE API: POST /newcustomer01"))
			.thenApply(new MockResponseTransformer() {
				@Override
				public MuleMessage transform(MuleMessage originalMessage) {
					MuleMessage newMessage = null;
					
					try {
						newMessage = originalMessage.createInboundMessage();
						
						newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
						newMessage.setProperty("Content-Type", "application/json;charset=UTF-8", PropertyScope.INBOUND);
						
						String tuneResponse = IOUtils.toString(getClass()
								.getResourceAsStream("/in/post-users-me-dealers-dealerId-vehicles-tune-response.json"));
						newMessage.setPayload(IOUtils.toInputStream(tuneResponse, "UTF-8"));
					} catch (Exception e) {
						// catch exception statements
					}
					
					return newMessage;
				}
			});
		
		
		MuleEvent event = testEvent("");
		event.setFlowVariable("myToyotaID", "MYT-000000cc");
		
		Map<String, Object> props = new HashMap<String,Object>();
        props.put("dmsType", dmsType);
        props.put("dmsID", "61.10169.10169011116");
        
		
        MuleMessage msg = muleMessageWithPayload(IOUtils.toString(getClass()
				.getResourceAsStream("/in/post-users-me-dealers-dealerId-vehicles-api-request.json")));
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
		
        
        event.setMessage(msg);		
		
		// Invoke the flow
		MuleEvent output = runFlow("post:/users/me/dealers/{dealerId}/vehicles:myToyota-config", event);
		
		verifyCallOfMessageProcessor("log-message")
				.ofNamespace("esbutils")
				.withAttributes(
						attribute("category")
								.withValue(
										"esb.mytoyota-api-v1.post-users-me-dealers-dealerId-vehicles.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message")
				.ofNamespace("esbutils")
				.withAttributes(
						attribute("category")
						.withValue(
								"esb.mytoyota-api-v1.post-users-me-dealers-dealerId-vehicles.response"))
				.times(1);

		verifyCallOfMessageProcessor("log-message")
				.ofNamespace("esbutils")
				.withAttributes(
						attribute("category")
						.withValue(
								"esb.mytoyota-api-v1.post-users-me-dealers-dealerId-vehicles-technical-adapter.request")).times(1);
		verifyCallOfMessageProcessor("log-message")
				.ofNamespace("esbutils")
				.withAttributes(
						attribute("category")
						.withValue(
								"esb.mytoyota-api-v1.post-users-me-dealers-dealerId-vehicles-technical-adapter.response")).times(1);
		
//		System.out.println("_____________________"+output.getMessage().getPayloadAsString());
		String expectedResponse = IOUtils.toString(getClass()
				.getResourceAsStream("/out/post-users-me-dealers-dealerId-vehicles-api-response.json"));
		assertJsonEquals(expectedResponse, output.getMessage().getPayloadAsString());

	}
	
	@Test
	public void testNonTuneCreateServiceBooking() throws Exception {
		
		MuleEvent event = testEvent("");
		
		Map<String, Object> props = new HashMap<String,Object>();
		props.put("dmsType", "nonTune");
        
		
        MuleMessage msg = muleMessageWithPayload(IOUtils.toString(getClass()
        		.getResourceAsStream("/in/post-users-me-dealers-dealerId-vehicles-api-request.json")));
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
        
        event.setMessage(msg);		
		
		// Invoke the flow
		runFlow("post:/users/me/dealers/{dealerId}/vehicles:myToyota-config", event);
		
		verifyCallOfMessageProcessor("log-message")
				.ofNamespace("esbutils")
				.withAttributes(
						attribute("category")
								.withValue(
										"esb.mytoyota-api-v1.post-users-me-dealers-dealerId-vehicles.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message")
				.ofNamespace("esbutils")
				.withAttributes(
						attribute("category")
						.withValue(
								"esb.mytoyota-api-v1.post-users-me-dealers-dealerId-vehicles.response"))
				.times(1);
		
		verifyCallOfMessageProcessor("log-message")
				.ofNamespace("esbutils")
				.withAttributes(
						attribute("category")
						.withValue(
								"esb.mytoyota-api-v1.post-users-me-dealers-dealerId-vehicles-technical-adapter.request")).times(0);
		verifyCallOfMessageProcessor("log-message")
				.ofNamespace("esbutils")
				.withAttributes(
						attribute("category")
						.withValue(
								"esb.mytoyota-api-v1.post-users-me-dealers-dealerId-vehicles-technical-adapter.response")).times(0);
		
	}

	
	//not possible to externalise this class because of the visibility of the method muleMessageWithPayload
	private class MockResponseTransformer implements MuleMessageTransformer {
		int count = 0;
		String[] payloads;

		public MockResponseTransformer(String... payloads) {
			this.payloads = payloads;
		}

		@Override
		public MuleMessage transform(MuleMessage originalMessage) {
			return muleMessageWithPayload(payloads[count++]);
		}
	}
	
}
