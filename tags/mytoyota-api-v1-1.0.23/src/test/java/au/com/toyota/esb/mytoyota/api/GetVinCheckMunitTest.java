package au.com.toyota.esb.mytoyota.api;

import static org.junit.Assert.*;
import static org.mule.munit.common.mocking.Attribute.attribute;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.munit.runner.functional.FunctionalMunitSuite;

import com.jayway.jsonpath.JsonPath;

//@Ignore
public class GetVinCheckMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"get-vehicle-vin-check.xml",
				"get-vehicle-vin-check-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulRequest() throws Exception {

		String vin = "JTNKU3JE60J021675";
		String requestPath = "/api/myToyota/v1/vehicles/" + vin + "/check";

		String apiResponse = IOUtils.toString(getClass().getResourceAsStream("/in/get-vehicles-vin-check-response.json"));
		
		whenMessageProcessor("request").ofNamespace("http")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("Vehicle API GET Vehicle VIN Check"))
			.thenReturn(muleMessageWithPayload(apiResponse));

		MuleEvent event = testEvent(requestPath);
		event.setFlowVariable("vin", vin);

		MuleEvent output = runFlow("get:/vehicles/{vin}/check:myToyota-config", event);

		System.out.println(output.getMessage().getPayload());

		assertEquals(vin, JsonPath.read(output.getMessage().getPayloadAsString(), "$.vin"));
		assertEquals(true, JsonPath.read(output.getMessage().getPayloadAsString(), "$.isValid"));
	}
	
	@Test
	public void unsuccessfulRequest() throws Exception {

		final String vin = "ABC123";
		final String requestPath = "/api/myToyota/v1/vehicles/" + vin + "/check";

		whenMessageProcessor("request").ofNamespace("http")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("Vehicle API GET Vehicle VIN Check"))
			.thenThrow(new org.mule.module.apikit.exception.NotFoundException(requestPath));
		
		MuleEvent event = testEvent(requestPath);
		event.setFlowVariable("vin", vin);

		try {
			runFlow("get:/vehicles/{vin}/check:myToyota-config", event);
		} catch (Exception e) {
			assertTrue(e.getCause() instanceof org.mule.module.apikit.exception.NotFoundException);
			assertEquals(requestPath, e.getCause().getMessage());
		}

	}
	
}
