package au.com.toyota.esb.mytoyota.api;

import static org.junit.Assert.*;
import static com.jayway.jsonpath.Criteria.*;
import static com.jayway.jsonpath.Filter.*;
import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.munit.runner.functional.FunctionalMunitSuite;

import com.jayway.jsonpath.JsonPath;

//@Ignore
public class GetUserVehiclesMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"get-user-vehicles.xml",
				"get-user-vehicles-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulRequest() throws Exception {

		String apiResponse = IOUtils.toString(getClass().getResourceAsStream("/in/get-user-vehicles-mykit-response.json"));
				
		whenMessageProcessor("request").ofNamespace("http")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("My Toyota App API GET Retrieve Users Vehicles"))
			.thenReturn(muleMessageWithPayload(apiResponse));

		MuleEvent event = testEvent("");

		MuleEvent output = runFlow("get:/users/me/vehicles:myToyota-config", event);

		String payload = output.getMessage().getPayloadAsString();
		System.out.println(payload);

		List<String> vehicles = JsonPath.read(payload, "$.vehicles");
		assertEquals(6, vehicles.size());
		
		assertEquals("JTEBR3FJ60K015246", JsonPath.read(payload, "$.vehicles[0].vin"));
		assertEquals("3121", JsonPath.read(payload, "$.vehicles[0].suffix.code"));
		assertEquals("sdfdsd", JsonPath.read(payload, "$.vehicles[0].trim.description"));
		
		assertEquals("JTNKU3JE60J021675", JsonPath.read(payload, "$.vehicles[3].vin"));
		assertEquals("ZRET", JsonPath.read(payload, "$.vehicles[3].salesType"));
		assertEquals("http://cdn.rotorint.com/Corolla/2012_08_Aug/e/hero/png/hi/260x146/Corolla_Hatch_Ascent_Sport_Sfx_B0_3R3_Wildfire_003.png", 
				JsonPath.read(payload, "$.vehicles[3].image"));
	}
	
	@Test
	public void unsuccessfulRequest() throws Exception {

		String requestPath = "/api/myToyota/v1/users/me/vehicles";

		whenMessageProcessor("contains").ofNamespace("objectstore")
		.withAttributes(attribute("name").ofNamespace("doc").withValue("Check Auth Token in ObjectStore"))
		.thenReturn(muleMessageWithPayload(true));
		
		whenMessageProcessor("retrieve").ofNamespace("objectstore")
		.withAttributes(attribute("name").ofNamespace("doc").withValue("Get Auth Token from ObjectStore"))
		.thenReturnSameEvent();
		
		whenMessageProcessor("request").ofNamespace("http")
		.withAttributes(attribute("name").ofNamespace("doc").withValue("My Toyota App API GET Retrieve Users Vehicles"))
			.thenThrow(new org.mule.module.apikit.exception.NotFoundException(requestPath));
		
		MuleEvent event = testEvent(requestPath);

		try {
			runFlow("get:/users/me/vehicles:myToyota-config", event);
		} catch (Exception e) {
			assertTrue(e.getCause() instanceof org.mule.module.apikit.exception.NotFoundException);
			assertEquals(requestPath, e.getCause().getMessage());
		}

	}
	
}
