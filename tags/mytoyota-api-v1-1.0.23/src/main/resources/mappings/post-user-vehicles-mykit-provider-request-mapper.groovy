import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper

def vehiclesListRequest = new JsonSlurper().parseText(payload).vehicles
def request = [:]

// Vehicle list placeholder
request['vehicles'] = []

// Build vehicle list
vehiclesListRequest.each{ v -> 
	def vehicle = [:]
	vehicle.batchNumber = v?.batchNumber
	vehicle.compliancePlate = v?.compliancePlate
	vehicle.engineNumber = v?.engineNumber
	vehicle.vin = v?.vin
	vehicle.materialNumber = v?.materialNumber
	vehicle.vehicleDescription = v?.vehicleDescription
	vehicle.katashikiCode = v?.katashikiCode
	vehicle.transmission = v?.transmission
	vehicle.sellingDealerCode = v?.sellingDealerCode
	vehicle.sellingDealerName = v?.sellingDealerName
	vehicle.registrationNumber = v?.registrationNumber
	vehicle.userRegistrationNumber = v?.userRegistrationNumber
	vehicle.state = v?.state
	vehicle.production = v?.production
	vehicle.discontinuedDate = v?.discontinuedDate
	
	if(v?.suffix) {
        vehicle.suffixCode = v.suffix.code
        vehicle.suffixDescription = v.suffix.description
    }
	if(v?.trim) {
        vehicle.trimCode = v.trim.code
        vehicle.trimDescription = v.trim.description
    }
    if(v?.paint) {
        vehicle.paintCode = v.paint.code
        vehicle.paintDescription = v.paint.description
    }

	vehicle.salesType = v?.salesType	
	
	/*imagesList = []
	v?.images.each { Map image ->
		imageMap = [:]
		imageMap.size = image.size
		imageMap.materialCode = image.materialCode
		imageMap.link = image.link

		if(imageMap)
			imagesList << imageMap
	}

	if(imagesList)
		vehicle.images = imagesList
	*/

	vehicle.image = v?.image
	vehicle.year = v?.year

	vehicleUsersList = []
	v?.vehicleUsers.each { Map vehicleUser ->
		//remove nulls
		vehicleUser = vehicleUser.findAll {it.value != null && it.value != ""}
		if(vehicleUser)
			vehicleUsersList << vehicleUser
	}

	if(vehicleUsersList)
		vehicle.vehicleUsers = vehicleUsersList

	//remove nulls
	vehicle = vehicle.findAll {it.value != null && it.value != ""}
	
	// Add to vehicle list
	if(vehicle)
		request['vehicles'].add(vehicle)
}

// Return
return prettyPrint(toJson(request))
//return request