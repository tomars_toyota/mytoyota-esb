import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper

def vehiclesListResponse = new JsonSlurper().parseText(payload)
def response = [:]

// Vehicle list placeholder
response['vehicles'] = []

// Build vehicle list
vehiclesListResponse.each{ v -> 
	def vehicle = [:]
	vehicle.batchNumber = v?.batchNumber
	vehicle.compliancePlate = v?.compliancePlate
	vehicle.engineNumber = v?.engineNumber
	vehicle.vin = v?.vin
	vehicle.materialNumber = v?.materialNumber
	vehicle.vehicleDescription = v?.vehicleDescription
	vehicle.katashikiCode = v?.katashikiCode
	vehicle.transmission = v?.transmission
	vehicle.sellingDealerCode = v?.sellingDealerCode
	vehicle.sellingDealerName = v?.sellingDealerName
	vehicle.registrationNumber = v?.registrationNumber
	vehicle.userRegistrationNumber = v?.userRegistrationNumber
	vehicle.state = v?.state
	vehicle.production = v?.production
	vehicle.discontinuedDate = v?.discontinuedDate
	vehicle.year = v?.year

	if(v?.suffixCode) {
		vehicle.suffix = [:]
		vehicle.suffix.code = v.suffixCode
		vehicle.suffix.description = v.suffixDescription
	}
	if(v?.trimCode) {
		vehicle.trim = [:]
		vehicle.trim.code = v.trimCode
		vehicle.trim.description = v.trimDescription
	}
	if(v?.paintCode) {
		vehicle.paint = [:]
		vehicle.paint.code = v.paintCode
		vehicle.paint.description = v.paintDescription
	}


	vehicle.salesType = v?.salesType	
	
	/*imagesList = []
	v?.images.each { Map image ->
		imageMap = [:]
		imageMap.size = image.size
		imageMap.materialCode = image.materialCode
		imageMap.link = image.link

		if(imageMap)
			imagesList << imageMap
	}

	if(imagesList)
		vehicle.images = imagesList
	*/

	vehicle.image = v?.image

	//remove nulls
	vehicle = vehicle.findAll {it.value != null && it.value != ""}


	
	// Add to vehicle list
	response['vehicles'].add(vehicle)
}


//println ('_________________________'+toJson(response))

// Return
return prettyPrint(toJson(response))