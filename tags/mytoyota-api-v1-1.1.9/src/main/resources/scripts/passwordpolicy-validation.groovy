import static groovy.json.JsonOutput.*

import org.passay.CharacterRule
import org.passay.EnglishCharacterData
import org.passay.LengthRule
import org.passay.PasswordData
import org.passay.PasswordValidator
import org.passay.Rule
import org.passay.RuleResult
import org.passay.WhitespaceRule
import org.slf4j.Logger
import org.slf4j.LoggerFactory

Logger logger = LoggerFactory.getLogger("esb.passwordpolicy-validation.validation.script");

PasswordData pass = new PasswordData(payload['password']);
List<Rule> rules = new ArrayList<Rule>();

// Len,Up,Low,Digi,Sym
ruleSet.split(',').each{r ->
	switch (r){
		case 'Len':
			if(leastLength && maxLength) {
				rules.add(new LengthRule(leastLength.toInteger(), maxLength.toInteger()))
			}else {
				throw new IllegalArgumentException("Len")
			}
			break
		case 'Up':
			if(leastUppercase) {
				rules.add(new CharacterRule(EnglishCharacterData.UpperCase, leastUppercase.toInteger()))
			}else {
				throw new IllegalArgumentException("Up")
			}
			break
		case 'Low':
			if(leastLowercase) {
				rules.add(new CharacterRule(EnglishCharacterData.LowerCase, leastLowercase.toInteger()))
			}else {
				throw new IllegalArgumentException("Low")
			}
			break
		case 'Digi':
			if(leastDigit) {
				rules.add(new CharacterRule(EnglishCharacterData.Digit, leastDigit.toInteger()))
			}else {
				throw new IllegalArgumentException("Digi")
			}
			break
		case 'Sym':
			if(leastSymbol) {
				rules.add(new CharacterRule(EnglishCharacterData.Special, leastSymbol.toInteger()))
			}else {
				throw new IllegalArgumentException("Sym")
			}
			break
		default:
			logger.warn("No password policy defined from properties")
			break
	}

}
// no whitespace
rules.add(new WhitespaceRule())

// validate password data
PasswordValidator validator = new PasswordValidator(rules)

RuleResult result = validator.validate(pass)

responseMap = [:]

responseMap['validPassword'] = result.isValid()
if (result.isValid()) {
	responseMap['message'] = "valid password"
} else {
	validationReport = []
	for (String msg : validator.getMessages(result)) {
		validationReport << msg
	}
	
	responseMap['message'] = validationReport
}


return toJson(responseMap)
