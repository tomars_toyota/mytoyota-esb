import groovy.json.JsonSlurper
import groovy.json.JsonBuilder

def user = new JsonSlurper().parseText(payload)
def result = [:]

if (user?.login != null)
	result.login = user.login

if (user?.title != null)
	result.title = user.title

if (user?.firstName != null)
	result.firstName = user.firstName

if (user?.lastName != null)
	result.lastName = user.lastName

if (user?.email != null)
	result.email = user.email

if (user?.mobile != null)
	result.mobile = user.mobile

if(user?.address?.postal) {
	postalAddress = user.address.postal
	addressMap = [:]

	addressMap.formattedAddress = postalAddress.fullAddress
	addressMap.referenceId = postalAddress.totalCheckId
	addressMap.streetAddress = postalAddress.streetAddress
	addressMap.postcode = postalAddress.postcode
	addressMap.suburb = postalAddress.suburb
	addressMap.state = postalAddress.state

	addressMap.primaryName  = postalAddress.primaryName
    addressMap.secondaryName  = postalAddress.secondaryName
    addressMap.unit = postalAddress.unit
    addressMap.streetNo = postalAddress.streetNo
    addressMap.streetName = postalAddress.streetName
    addressMap.streetType = postalAddress.streetType
    //addressMap.country = postalAddress.country 					//country is not being snet by fronte-end
    addressMap.geoLat = postalAddress.geoLat
    addressMap.geoLon = postalAddress.geoLon
    addressMap.geoLatStreet = postalAddress.geoLatStreet
    addressMap.geoLonStreet = postalAddress.geoLonStreet
    addressMap.listing = postalAddress.listing
    addressMap.postal = postalAddress.postal
    addressMap.dpid = postalAddress.dpid

    //remove nulls
    addressMap = addressMap.findAll { it.value != null}

    if(addressMap)
    	result.address = addressMap
}


if (user?.dateOfBirth != null) {
	if(user?.dateOfBirth == '')
		result.dateOfBirth = user.dateOfBirth
	else {
		//convert date to timestamp
		result.dateOfBirth = new Date().parse('yyyy-MM-dd', user.dateOfBirth).time
	}
}

if (user?.landline != null)
	result.landline = user.landline
	
if (user?.avatar)
	result.avatar = user.avatar
		
if (user?.licenceId != null)
	result.licenceId = user.licenceId

if (user?.licenceState != null)
	result.licenceState = user.licenceState

if (user?.licenceCountry != null)
	result.licenceCountry = user.licenceCountry

if (user?.licenceExpiry) {
	//convert date to timestamp
	result.licenceExpiry = new Date().parse('yyyy-MM-dd', user.licenceExpiry).time
}

return new JsonBuilder(result).toString()