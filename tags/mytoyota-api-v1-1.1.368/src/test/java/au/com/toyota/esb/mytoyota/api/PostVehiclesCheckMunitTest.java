package au.com.toyota.esb.mytoyota.api;

import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.Properties;

import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.util.IOUtils;
import org.mule.util.StringUtils;
import org.skyscreamer.jsonassert.JSONAssert;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;

public class PostVehiclesCheckMunitTest extends FunctionalMunitSuite {
	
	private static Properties adapterProperties = new Properties();

	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"post-vehicle-check.xml",
				"post-vehicle-check-technical-adapter.xml",
				"get-vehicle-details-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testVinBatchCheckCorrect() throws Exception {
		
		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));
		
		String testVinNumber = "JTNKU3JE60J021675";
		// Mock myKIT response
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
				attribute("name").ofNamespace("doc").withValue("Vehicle API GET vehicle by vin"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						
						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							String myKITResponse = IOUtils.toString(getClass().getResourceAsStream("/out/get-vehicles-vin-api-response.json"));
							newMessage.setPayload(myKITResponse);
						} catch (Exception e) {
							// catch exception statements
						}
						return newMessage;
					}
				});
				
		String requestPayload = IOUtils.toString(getClass().getResourceAsStream("/in/post-vehicles-vin-check.json"));
		String expectedResult = "{\"isValid\": true }";
		
		MuleEvent event = testEvent(requestPayload);
		event.setFlowVariable("vin",testVinNumber);
		        
		// Invoke the flow
		@SuppressWarnings("unused")
		MuleEvent output = runFlow("post:/vehicles/check:myToyota-config", event);
		
		String result = output.getMessage().getPayload().toString();
		
		// Check response payload
		JSONAssert.assertEquals(expectedResult, result, true);

		// Main flow
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log API Request")).times(1);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log API Response")).times(1);
	}
	
	@Test
	public void testVinBatchCheckIncorrect() throws Exception {
		
		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));
		
		String testVinNumber = "JTNKU3JE60J021675";
		// Mock myKIT response
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
				attribute("name").ofNamespace("doc").withValue("Vehicle API GET vehicle by vin"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						
						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							String myKITResponse = IOUtils.toString(getClass().getResourceAsStream("/out/get-vehicles-vin-api-response.json"));
							newMessage.setPayload(myKITResponse);
						} catch (Exception e) {
							// catch exception statements
						}
						return newMessage;
					}
				});
				
		String requestPayload = IOUtils.toString(getClass().getResourceAsStream("/in/post-vehicles-vin-check-incorrect-batch.json"));
		String expectedResult = "{\"isValid\": false }";
		
		MuleEvent event = testEvent(requestPayload);
		event.setFlowVariable("vin",testVinNumber);
		        
		// Invoke the flow
		@SuppressWarnings("unused")
		MuleEvent output = runFlow("post:/vehicles/check:myToyota-config", event);
		
		String result = output.getMessage().getPayload().toString();
		
		// Check response payload
		JSONAssert.assertEquals(expectedResult, result, true);

		// Main flow
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log API Request")).times(1);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log API Response")).times(1);
	}
}
	