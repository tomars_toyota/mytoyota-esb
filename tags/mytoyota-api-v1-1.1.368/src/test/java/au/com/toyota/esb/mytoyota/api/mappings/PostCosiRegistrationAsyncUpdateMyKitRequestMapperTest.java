package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;

import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

public class PostCosiRegistrationAsyncUpdateMyKitRequestMapperTest extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;

	public PostCosiRegistrationAsyncUpdateMyKitRequestMapperTest() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/post-cosi-registration-async-update-myKit-request-mapper.groovy");
	}

	@Test
	public void testSuccessfulRequestMapper() throws Exception {
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload("")
				.sessionVar("orderResponse",
						IOUtils.toString(getClass()
								.getResourceAsStream("/in/post-cosi-registration-async-get-order-response.json")))
				.flowVar("myToyotaId", "TEST-00000270")
				.sessionVar("key", "value")
				.run().toString();

		System.out.println(result);
		
		// expected
		String expectedResult = IOUtils.toString(getClass()
				.getResourceAsStream(
						"/out/post-cosi-registration-async-update-user-mykit-request.json"));
		//System.out.println(expectedResult);
		JSONAssert.assertEquals(expectedResult, result, true);
	}
	
}
