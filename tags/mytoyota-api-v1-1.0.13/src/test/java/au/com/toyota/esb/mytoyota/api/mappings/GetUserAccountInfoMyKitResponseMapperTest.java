package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

import javax.script.*;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

public class GetUserAccountInfoMyKitResponseMapperTest extends AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;
	
	public GetUserAccountInfoMyKitResponseMapperTest() throws ScriptException, SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/get-user-account-info-mykit-provider-response-mapper.groovy");
	}
	
	@Test
	public void testRequestMapper() throws Exception {
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(getClass().getResourceAsStream(
						"/in/get-user-account-info-mykit-response.json")))
				.run().toString();
		System.out.println(result);
		
		// expected
		String expectedResult = IOUtils.toString(getClass().getResourceAsStream(
				"/out/get-user-account-info-api-response.json"));
		JSONAssert.assertEquals(expectedResult, result, true);
	}
	
}
