package au.com.toyota.esb.mytoyota.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.util.IOUtils;
import org.mule.util.StringUtils;
import org.skyscreamer.jsonassert.JSONAssert;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;
import net.javacrumbs.jsonunit.JsonAssert;


/*
 * Munit test to unit test the functionality for user reset password
 * @author: jgarner
 */
public class ResetPasswordMunitTest extends FunctionalMunitSuite {

	private static Properties adapterProperties = new Properties();
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"post-user-reset-password.xml",
				"post-user-reset-password-technical-adapter.xml",
				"get-user-account-info-technical-adapter.xml",
				"config.xml"
		}, " ");
	}
	
	@Test
	public void testSuccessfulGetPasswordPolicyPasswordResetTokenNotPassed() throws Exception {
		
		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));
		
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
			attribute("name").ofNamespace("doc").withValue("IM API Reset Password"))
			.thenApply(new MockResponseTransformer() {
				@Override
				public MuleMessage transform(MuleMessage originalMessage) {
					MuleMessage newMessage = null;
					
					try {
						newMessage = originalMessage.createInboundMessage();
						newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
						String responsePayload = IOUtils.toString(getClass().getResourceAsStream("/in/post-user-reset-password-passwordresettoken-im-response.json"));
						newMessage.setPayload(responsePayload);
					} catch (Exception e) {
						// catch exception statements
					}
					return newMessage;
				}
			});
		
		String requestPayload = IOUtils.toString(getClass().getResourceAsStream("/in/post-user-reset-password-passwordresettoken-not-passed-request.json"));
		
		MuleEvent event = testEvent(requestPayload);
		
		// Invoke the flow
		@SuppressWarnings("unused")
		MuleEvent output = runFlow("post:/users/me/resetPassword:myToyota-config", event);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
		.withAttributes(attribute("name").ofNamespace("doc")
					.withValue("Log Request")).times(2);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
			.withAttributes(attribute("name").ofNamespace("doc")
						.withValue("Log Response")).times(2);	
			
	}
	
	@Test
	public void testSuccessfulGetPasswordPolicyPasswordResetTokenPassed() throws Exception {
		
		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));
		
		// Mock Identity Management API Password Reset successful
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
			attribute("name").ofNamespace("doc").withValue("IM API Reset Password"))
			.thenApply(new MockResponseTransformer() {
				@Override
				public MuleMessage transform(MuleMessage originalMessage) {
					MuleMessage newMessage = null;
					
					try {
						newMessage = originalMessage.createInboundMessage();
						newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
						String responsePayload = IOUtils.toString(getClass().getResourceAsStream("/in/post-user-reset-password-passwordresettoken-im-response.json"));
						newMessage.setPayload(responsePayload);
					} catch (Exception e) {
						// catch exception statements
					}
					return newMessage;
				}
			});
		// Mock Identity Management API Get User Info
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
				attribute("name").ofNamespace("doc").withValue("IM API Get User"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						
						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							String responsePayload = IOUtils.toString(getClass().getResourceAsStream("/in/post-user-reset-password-passwordresettoken-im-get-user-profile-response.json"));
							newMessage.setPayload(responsePayload);
						} catch (Exception e) {
							// catch exception statements
						}
						return newMessage;
					}
				});
		
		String requestPayload = IOUtils.toString(getClass().getResourceAsStream("/in/post-user-reset-password-passwordresettoken-passed-request.json"));
		
		MuleEvent event = testEvent(requestPayload);
		
		// Invoke the flow
		@SuppressWarnings("unused")
		MuleEvent output = runFlow("post:/users/me/resetPassword:myToyota-config", event);
		
		// Verify correct path in Mule flow
		verifyCallOfMessageProcessor("request").ofNamespace("http")
		.withAttributes(attribute("name").ofNamespace("doc")
					.withValue("IM API Get User")).times(1);
		
		System.out.println("**** Result: "+output.getMessage().getPayload());
		
		// Verify payload output
		// expected
		String expectedResult = IOUtils.toString(getClass().getResourceAsStream(
				"/out/post-user-reset-password-passwordresettoken-passed-response.json"));
				
		
		
		// JSONAssert.assertEquals(output.getMessage().getPayloadAsString(), expectedResult,true);	
	}
	
	@Test
	public void testFailuresStatusCodes() throws Exception {

		int[] statusCodes = new int[] { 404, 405, 415, 406, 401, 500 };
		
		String requestPayload = IOUtils.toString(getClass().getResourceAsStream("/in/post-user-reset-password-passwordresettoken-passed-request.json"));
		
		MuleEvent event = testEvent(requestPayload);
		
		for (int i = 0; i < statusCodes.length; i++) {
			new StatusCodesTestingHelper().runFailureTest(statusCodes[i], "IM API Reset Password",
					"post:/users/me/resetPassword:user-reset-password-technical-adapter", event);
		}
	}
}
