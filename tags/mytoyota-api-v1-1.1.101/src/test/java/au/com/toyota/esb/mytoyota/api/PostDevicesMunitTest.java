package au.com.toyota.esb.mytoyota.api;

import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.junit.Assert.assertEquals;
import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.Properties;

import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.module.http.internal.request.ResponseValidatorException;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.util.IOUtils;
import org.mule.util.StringUtils;

import au.com.toyota.esb.mytoyota.api.munit.BlockingMuleEventSpy;
import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;


/*
 * Munit test to unit test the functionality to post/add a device notification configurations
 * @author: ahuwait
 */
public class PostDevicesMunitTest extends FunctionalMunitSuite {

	private static Properties adapterProperties = new Properties();
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"post-devices.xml",
				"post-devices-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulPostDevices() throws Exception {
		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));
		
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
			attribute("name").ofNamespace("doc").withValue("My Toyota App API Post Devices"))
			.thenApply(new MockResponseTransformer() {
				@Override
				public MuleMessage transform(MuleMessage originalMessage) {
					MuleMessage newMessage = null;
					
					try {
						newMessage = originalMessage.createInboundMessage();
						
						newMessage.setProperty("http.status", 201, PropertyScope.INBOUND);
						
						newMessage.setPayload(IOUtils.toInputStream("", "UTF-8"));
					} catch (Exception e) {
						// catch exception statements
					}
					
					return newMessage;
				}
			});
		
		BlockingMuleEventSpy<String> appAPIRequestMessageSpy = new BlockingMuleEventSpy<String>(1, 5000);
		spyMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(
			attribute("name").ofNamespace("doc").withValue("Log Post Devices App Request"))
			.after(appAPIRequestMessageSpy);

		String requestPayload = IOUtils.toString(getClass().getResourceAsStream("/in/post-devices-request.json"));

		MuleEvent event = testEvent(requestPayload);

		// Invoke the flow
		@SuppressWarnings("unused")
		MuleEvent output = runFlow("post:/devices:myToyota-config", event);
		
		appAPIRequestMessageSpy.block();

		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Post Devices App Request")).times(1);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Post Devices App Response")).times(1);	

		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Post Devices App Request")).times(1);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Post Devices App Response")).times(1);	

		MuleEvent appAPIRequestMessageMuleEvent = appAPIRequestMessageSpy.getMuleEvent();
				
		// Assert app api request
		assertEquals("application/json", appAPIRequestMessageMuleEvent.getMessage().getProperty("Content-Type", PropertyScope.OUTBOUND));
		assertEquals(adapterProperties.getProperty("api.mytoyota-app.authTokenHeaderValue"), 
				appAPIRequestMessageMuleEvent.getMessage().getProperty(adapterProperties.getProperty("api.mytoyota-app.authTokenHeader"), PropertyScope.OUTBOUND));
		
		assertJsonEquals(requestPayload, appAPIRequestMessageSpy.getFirstPayload());
	}
	
	@Test
	public void testFailurePostDevices() throws Exception {
		
		String requestPayload = IOUtils.toString(getClass().getResourceAsStream("/in/post-devices-request.json"));

		MuleEvent event = testEvent(requestPayload);

		whenMessageProcessor("request").ofNamespace("http").withAttributes(
			attribute("name").ofNamespace("doc").withValue("My Toyota App API Post Devices"))
			.thenThrow(new java.lang.Exception("Response code 500 mapped as failure."));

		try {

			// Invoke the flow
			runFlow("post:/devices:myToyota-config", event);
			
			// validating exception thrown
			assertEquals(true, false);

		} catch (java.lang.Exception e) {
			
			verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Post Devices App Request")).times(1);	
			verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Post Devices App Response")).times(0);	

			verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Post Devices App Request")).times(1);	
			verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Post Devices App Response")).times(0);

		}

	}
	
	@Test
	public void testFailuresStatusCodes() throws Exception {

		int[] statusCodes = new int[] { 404, 405, 415, 406, 401, 500 };

		String requestPayload = IOUtils.toString(getClass().getResourceAsStream("/in/post-devices-request.json"));
		MuleEvent event = testEvent(requestPayload);

		for (int i = 0; i < statusCodes.length; i++) {
			new StatusCodesTestingHelper().runFailureTest(statusCodes[i], "My Toyota App API Post Devices",
					"post:/devices:myToyota-config", event);
		}
	}
}
