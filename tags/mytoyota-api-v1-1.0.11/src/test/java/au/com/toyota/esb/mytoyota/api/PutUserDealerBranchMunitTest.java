package au.com.toyota.esb.mytoyota.api;

import au.com.toyota.esb.mytoyota.api.munit.BlockingMuleEventSpy;

import org.junit.Test;

import java.util.Properties;

import org.mule.util.StringUtils;
import org.mule.util.IOUtils;

import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;

import org.mule.api.transport.PropertyScope;

import org.mule.munit.runner.functional.FunctionalMunitSuite;

import org.mule.modules.interceptor.processors.MuleMessageTransformer;

import org.mule.module.http.internal.request.ResponseValidatorException;

import static org.junit.Assert.*;
import static org.mule.munit.common.mocking.Attribute.attribute;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;


/*
 * Munit test to unit test the functionality to put/update a dealer to a user
 * @author: swapnil
 */
public class PutUserDealerBranchMunitTest extends FunctionalMunitSuite {
	
	private static Properties adapterProperties = new Properties();

	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"put-user-dealer-branch.xml",
				"put-user-dealer-branch-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulPutUserDealers() throws Exception {
		
		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));

		whenMessageProcessor("request").ofNamespace("http").withAttributes(
			attribute("name").ofNamespace("doc").withValue("My Toyota App API Put User Dealer Branch"))
			.thenApply(new MockResponseTransformer() {
				@Override
				public MuleMessage transform(MuleMessage originalMessage) {
					MuleMessage newMessage = null;
					
					try {
						newMessage = originalMessage.createInboundMessage();
						
						newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
						
						newMessage.setPayload(IOUtils.toInputStream("", "UTF-8"));
					} catch (Exception e) {
						// catch exception statements
					}
					
					return newMessage;
				}
			});
		
		BlockingMuleEventSpy<String> appAPIRequestMessageSpy = new BlockingMuleEventSpy<String>(1, 5000);
		spyMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(
			attribute("name").ofNamespace("doc").withValue("Log Put User Dealer Branch App Request"))
			.after(appAPIRequestMessageSpy);

		// String requestPayload = IOUtils.toString(getClass().getResourceAsStream("/in/put-user-dealers-request.json"));

		MuleEvent event = testEvent("");

		// Invoke the flow
		MuleEvent output = runFlow("put:/users/me/dealers/{dealerId}/branches/{branchId}:myToyota-config", event);
		
		appAPIRequestMessageSpy.block();

		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Put User Dealer Branch App Request")).times(1);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Put User Dealer Branch App Response")).times(1);	

		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Put User Dealer Branch App Request")).times(1);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Put User Dealer Branch App Response")).times(1);	

		MuleEvent appAPIRequestMessageMuleEvent = appAPIRequestMessageSpy.getMuleEvent();
				
		// Assert app api request
		assertEquals("application/json", appAPIRequestMessageMuleEvent.getMessage().getProperty("Content-Type", PropertyScope.OUTBOUND));
		assertEquals(adapterProperties.getProperty("api.mytoyota-app.authTokenHeaderValue"), 
				appAPIRequestMessageMuleEvent.getMessage().getProperty(adapterProperties.getProperty("api.mytoyota-app.authTokenHeader"), PropertyScope.OUTBOUND));
		
		assertJsonEquals("", appAPIRequestMessageSpy.getFirstPayload());

		// Assert api response
		assertEquals(200, Integer.parseInt(output.getMessage().getOutboundProperty("http.status").toString()));
		assertEquals("", output.getMessage().getPayload());

	}
	
	@Test
	public void testFailurePutUserDealers() throws Exception {
		
		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));

		// String requestPayload = IOUtils.toString(getClass().getResourceAsStream("/in/put-user-dealers-request.json"));

		MuleEvent event = testEvent("");

		whenMessageProcessor("request").ofNamespace("http").withAttributes(
			attribute("name").ofNamespace("doc").withValue("My Toyota App API Put User Dealer Branch"))
			.thenThrow(new ResponseValidatorException("Response code 500 mapped as failure.", event));

		try {

			// Invoke the flow
			runFlow("put:/users/me/dealers/{dealerId}/branches/{branchId}:myToyota-config", event);
			
			// validating exception thrown
			assertEquals(true, false);

		} catch (ResponseValidatorException e) {
			
			verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Put User Dealer Branch App Request")).times(1);	
			verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Put User Dealer Branch App Response")).times(0);	

			verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Put User Dealer Branch App Request")).times(1);	
			verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Put User Dealer Branch App Response")).times(0);

		}

	}
	
	//not possible to externalise this class because of the visibility of the method muleMessageWithPayload
	private class MockResponseTransformer implements MuleMessageTransformer {
		int count = 0;
		String[] payloads;

		public MockResponseTransformer(String... payloads) {
			this.payloads = payloads;
		}

		@Override
		public MuleMessage transform(MuleMessage originalMessage) {
			return muleMessageWithPayload(payloads[count++]);
		}
	}
	
}
