package au.com.toyota.esb.mytoyota.api.util

public class RecallCategoryHelper {
	def static identifyRecallCategory(campaignCode, takataUrgentCampaignCodes, takataNonUrgentCampaignCodes) {

		if (!campaignCode) {
			throw new org.mule.module.apikit.exception.BadRequestException("Invalid request, campaignCode is required")
		}
		
		// Split to list takataUrgentCampaignCodes, takataNonUrgentCampaignCodes
		
		// Identify whether the recall campaign is a Takata Recall
		def takataUrgent = false
		def takataUrgentCampaignCodesList = takataUrgentCampaignCodes.split(",").collect {it.trim() as String}
		// Check if current Campaign Code is included
		if ( takataUrgentCampaignCodesList.contains(campaignCode) ) {
			takataUrgent = true
		}
		
		def takataNonUrgent = false
		def takataNonUrgentCampaignCodesList = takataNonUrgentCampaignCodes.split(",").collect {it.trim() as String}
		// Check if current Campaign Code is included
		if ( takataNonUrgentCampaignCodesList.contains(campaignCode) ) {
			takataNonUrgent = true
		}
		
		
		// Set a variable to identify which content to pull from Sitecore
		def recallCategory = "INFO"
		if (takataUrgent) {
			recallCategory = "URGENT"
		}
		else if (takataNonUrgent) {
			recallCategory = "NONURGENT"
		}
		else {
			recallCategory = "INFO"
		}
		
		return recallCategory
	}

	
}