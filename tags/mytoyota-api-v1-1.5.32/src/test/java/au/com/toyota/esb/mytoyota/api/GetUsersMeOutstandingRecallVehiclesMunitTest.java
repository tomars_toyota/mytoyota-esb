package au.com.toyota.esb.mytoyota.api;

import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.mule.munit.common.mocking.Attribute.attribute;

import org.junit.Ignore;
import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.util.IOUtils;
import org.mule.util.StringUtils;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;


public class GetUsersMeOutstandingRecallVehiclesMunitTest extends FunctionalMunitSuite {

	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] { "get-users-me-outstanding-recall-vehicles.xml",
				"get-users-me-outstanding-recall-vehicles-technical-adapter.xml", "config.xml" }, " ");
	}

	@Test
	public void testSuccessfulGetOutstandingRecallVehiclesBooking() throws Exception {

		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(
						attribute("name").ofNamespace("doc").withValue("My Toyota App API GET Retrieve Users Vehicles"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							newMessage = originalMessage.createInboundMessage();

							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);

							newMessage.setPayload(
									getClass().getResourceAsStream("/in/get-users-me-vehicles-mykit-response.json"));
						} catch (Exception e) {
							// catch exception statements
						}

						return newMessage;
					}
				});

		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(
						attribute("name").ofNamespace("doc").withValue("Vehicle API GET Outstanding Recalls"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							newMessage = originalMessage.createInboundMessage();

							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);

							newMessage.setPayload(
									getClass().getResourceAsStream("/in/get-vehicles-vin-outstanding-recalls.json"));
						} catch (Exception e) {
							// catch exception statements
						}

						return newMessage;
					}
				});

		MuleEvent event = testEvent("");

		MuleMessage msg = muleMessageWithPayload(null);

		event.setMessage(msg);

		// Invoke the flow
		MuleEvent output = runFlow("get:/users/me/outstandingRecallVehicles:myToyota-config", event);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-users-me-outstanding-recall-vehicles.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("category")
				.withValue("esb.mytoyota-api-v1.get-users-me-outstanding-recall-vehicles.response")).times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.get-users-me-outstanding-recall-vehicles-technical-adapter.request"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.get-users-me-outstanding-recall-vehicles-technical-adapter.response"))
				.times(1);

		String expectedResponse = IOUtils
				.toString(getClass().getResourceAsStream("/out/get-users-me-outstanding-recall-response.json"));
		assertJsonEquals(expectedResponse, output.getMessage().getPayloadAsString());
	}

	/*
	 * @Test public void testFailuresStatusCodes() throws Exception {
	 * 
	 * int[] statusCodes = new int[] { 404, 405, 415, 406, 401, 500 };
	 * 
	 * MuleEvent event = testEvent("");
	 * 
	 * Map<String, Object> props = new HashMap<String,Object>();
	 * props.put("status", "quoted");
	 * 
	 * MuleMessage msg = muleMessageWithPayload(null);
	 * msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
	 * 
	 * event.setMessage(msg);
	 * 
	 * for (int i = 0; i < statusCodes.length; i++) { new
	 * StatusCodesTestingHelper().runFailureTest(statusCodes[i],
	 * "MyKit API: GET /users/{userLogin}/services",
	 * "get:/users/me/services:myToyota-config", event); } }
	 */
}
