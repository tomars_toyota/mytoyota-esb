package au.com.toyota.esb.mytoyota.api.mappings;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

import javax.script.*;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.module.apikit.exception.BadRequestException;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

public class GetVehiclesVinCheckResponseMapper extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;

	public GetVehiclesVinCheckResponseMapper() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/get-vehicles-vin-check-response-mapper.groovy");
	}

	@Test
	public void testSuccessfulInvalidResponseMapper() throws Exception {
		testSuccessfulResponseMapperImpl(false, "{\"vin\": \"6T153BK360X070769\",\"isValid\": false}");
	}
	
	
	@Test
	public void testSuccessfulValidResponseMapper() throws Exception {
		testSuccessfulResponseMapperImpl(true, "{\"vin\": \"6T153BK360X070769\",\"isValid\": true}");
	}
	
	private void testSuccessfulResponseMapperImpl(boolean isValidVin, String expectedResult) throws Exception {
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(null)
				.flowVar("vin", "6T153BK360X070769")
				.flowVar("isValidVin", isValidVin)
				.run().toString();

		System.out.println(result);

		JSONAssert.assertEquals(expectedResult, result, true);
	}
	
}
