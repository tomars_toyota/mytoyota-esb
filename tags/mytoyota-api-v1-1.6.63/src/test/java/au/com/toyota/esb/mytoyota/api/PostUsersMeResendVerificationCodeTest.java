package au.com.toyota.esb.mytoyota.api;

import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.junit.Assert.assertEquals;
import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.Properties;

import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.module.http.internal.request.ResponseValidatorException;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.util.IOUtils;
import org.mule.util.StringUtils;

import au.com.toyota.esb.mytoyota.api.munit.BlockingMuleEventSpy;
import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;


public class PostUsersMeResendVerificationCodeTest extends FunctionalMunitSuite {
	
	private static Properties adapterProperties = new Properties();

	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"post-user-resend-verification-code.xml",
				"post-user-resend-verification-code-technical-adapter.xml",
				"search-user-account-info-im-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulResendVerificationLinkRegToken() throws Exception {
		
		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));

		whenMessageProcessor("request").ofNamespace("http").withAttributes(
			attribute("name").ofNamespace("doc").withValue("IM API Resend Verification Code"))
			.thenApply(new MockResponseTransformer() {
				@Override
				public MuleMessage transform(MuleMessage originalMessage) {
					MuleMessage newMessage = null;
					
					try {
						newMessage = originalMessage.createInboundMessage();
						newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
						newMessage.setPayload(IOUtils.toInputStream("", "UTF-8"));
						String responsePayload = IOUtils.toString(getClass().getResourceAsStream("/in/im-resend-verification-email-response.json"));
						newMessage.setPayload(responsePayload);
					} catch (Exception e) {
						// catch exception statements
					}
					
					return newMessage;
				}
			});
		
		MuleMessage msg = muleMessageWithPayload(null);
		msg.setProperty("regToken", "RT1_stDrFP~02045353544b0737313232343531010b35~", PropertyScope.INBOUND);
		
		MuleEvent event = testEvent("");
		event.setMessage(msg);
		
		// Invoke the flow
		@SuppressWarnings("unused")
		MuleEvent output = runFlow("post:/users/me/resendVerificationCode:myToyota-config", event);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Request")).times(2);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Response")).times(2);
		// validate choice path
		verifyCallOfMessageProcessor("flow-ref").withAttributes(attribute("name").ofNamespace("doc").withValue("Get UID from IM by Login")).times(0);

	}
	
	@Test
	public void testSuccessfulResendVerificationLinkLogin() throws Exception {
		
		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));

		whenMessageProcessor("request").ofNamespace("http").withAttributes(
			attribute("name").ofNamespace("doc").withValue("IM API Resend Verification Code"))
			.thenApply(new MockResponseTransformer() {
				@Override
				public MuleMessage transform(MuleMessage originalMessage) {
					MuleMessage newMessage = null;
					
					try {
						newMessage = originalMessage.createInboundMessage();
						newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
						String responsePayload = IOUtils.toString(getClass().getResourceAsStream("/in/im-resend-verification-email-response.json"));
						newMessage.setPayload(responsePayload);
					} catch (Exception e) {
						// catch exception statements
					}
					
					return newMessage;
				}
			});
		
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
				attribute("name").ofNamespace("doc").withValue("IM API Search User"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						
						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							String responsePayload = IOUtils.toString(getClass().getResourceAsStream("/in/im-search-user-response.json"));
							newMessage.setPayload(responsePayload);
						} catch (Exception e) {
							// catch exception statements
						}
						
						return newMessage;
					}
				});
		MuleMessage msg = muleMessageWithPayload(null);
		msg.setProperty("login", "james.a.garner+cbb313e2-f60a-4117-1cf7-20fd6f77262b@gmail.com", PropertyScope.INBOUND);
		
		MuleEvent event = testEvent("");
		event.setMessage(msg);
		
		// Invoke the flow
		@SuppressWarnings("unused")
		MuleEvent output = runFlow("post:/users/me/resendVerificationCode:myToyota-config", event);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Request")).times(3);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Response")).times(3);
		// validate choice path
		verifyCallOfMessageProcessor("component").ofNamespace("scripting").withAttributes(attribute("name").ofNamespace("doc").withValue("Extract UID")).times(1);

	}
	
	@Test
	public void testFailureResendVerificationLinkRegToken() throws Exception {
		
		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));

		MuleMessage msg = muleMessageWithPayload(null);
		msg.setProperty("regToken", "RT1_stDrFP~02045353544b0737313232343531010b35~", PropertyScope.INBOUND);
		
		MuleEvent event = testEvent("");
		event.setMessage(msg);
		
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
			attribute("name").ofNamespace("doc").withValue("IM API Resend Verification Code"))
			.thenThrow(new java.lang.Exception("Response code 500 mapped as failure."));

		try {

			// Invoke the flow
			runFlow("post:/users/me/resendVerificationCode:myToyota-config", event);
			
			// validating exception thrown
			assertEquals(true, false);

		} catch (java.lang.Exception e) {
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Request")).times(2);	
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Response")).times(0);	
		}

	}
	
	@Test
	public void testFailuresStatusCodes() throws Exception {

		int[] statusCodes = new int[] { 404, 405, 415, 406, 401, 500 };

		MuleMessage msg = muleMessageWithPayload(null);
		msg.setProperty("regToken", "RT1_stDrFP~02045353544b0737313232343531010b35~", PropertyScope.INBOUND);
		
		MuleEvent event = testEvent("");
		event.setMessage(msg);

		for (int i = 0; i < statusCodes.length; i++) {
			new StatusCodesTestingHelper().runFailureTest(statusCodes[i], "IM API Resend Verification Code",
					"post:/users/me/resendVerificationCode:myToyota-config", event);
		}
	}
}
