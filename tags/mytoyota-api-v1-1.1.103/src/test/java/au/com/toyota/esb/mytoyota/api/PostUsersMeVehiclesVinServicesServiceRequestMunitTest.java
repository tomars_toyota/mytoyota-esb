package au.com.toyota.esb.mytoyota.api;

import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.mule.munit.common.mocking.Attribute.attribute;

import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.util.IOUtils;
import org.mule.util.StringUtils;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;


public class PostUsersMeVehiclesVinServicesServiceRequestMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"post-users-me-vehicles-vin-services-serviceRequest.xml",
				"post-users-me-vehicles-vin-services-serviceRequest-technical-adapter.xml",
				"get-dealers-by-dealer-code-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulRequest() throws Exception {

		//mock get:/dealers/{dealerCode} response 
		whenMessageProcessor("flow-ref")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("get:/dealers/{dealerCode}:call-va-api"))
				.thenReturn(muleMessageWithPayload(
						IOUtils.toString(getClass().getResourceAsStream("/in/get-dealers-dealerId-response.json"))));
		
		//mock post:/comms/email response 
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
				attribute("name").ofNamespace("doc").withValue("PING API: POST /comms/email"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						
						try {
							newMessage = originalMessage.createInboundMessage();
							
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							newMessage.setProperty("Content-Type", "application/json;charset=UTF-8", PropertyScope.INBOUND);
							
							String pingResponse = IOUtils
									.toString(getClass()
											.getResourceAsStream(
													"/in/post-email-ping-response.json"));
							newMessage.setPayload(IOUtils.toInputStream(pingResponse, "UTF-8"));
						} catch (Exception e) {
							// catch exception statements
						}
						return newMessage;
					}
				});
		
		//mock get:/users/{myToyotaId}/vehicles/{vin} response 
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
				attribute("name").ofNamespace("doc").withValue("My Toyota App API GET /users/{myToyotaId}/vehicles/{vin}"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						
						try {
							newMessage = originalMessage.createInboundMessage();
							
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							newMessage.setProperty("Content-Type", "application/json;charset=UTF-8", PropertyScope.INBOUND);
							
							newMessage.setPayload(getClass()
									.getResourceAsStream("/in/get-users-me-vehicles-vin-mykit-response.json"));
						} catch (Exception e) {
							// catch exception statements
						}
						return newMessage;
					}
				});
		
		//mock put:/users/{myToyotaId}/vehicles/{vin} response 
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
				attribute("name").ofNamespace("doc").withValue("My Toyota App API PUT /users/{myToyotaId}/vehicles/{vin}"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						
						try {
							newMessage = originalMessage.createInboundMessage();
							
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							
						} catch (Exception e) {
							// catch exception statements
						}
						return newMessage;
					}
				});

		MuleEvent event = testEvent("");
		
		MuleMessage msg = muleMessageWithPayload(IOUtils
				.toString(getClass()
						.getResourceAsStream(
								"/in/post-users-me-vehicles-vin-services-serviceRequest-primary-contact-api-request.json")));
		
        event.setMessage(msg);
		
		// Invoke the flow
		MuleEvent output = runFlow("post:/users/me/vehicles/{vin}/services/serviceRequest:myToyota-config", event);

		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category")
						.withValue("esb.mytoyota-api-v1.post-users-me-vehicles-vin-services.serviceRequest.request"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category")
						.withValue("esb.mytoyota-api-v1.post-users-me-vehicles-vin-services.serviceRequest.response"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.post-users-me-vehicles-vin-services.serviceRequest-technical-adapter.request"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.post-users-me-vehicles-vin-services.serviceRequest-technical-adapter.response"))
				.times(1);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.post-users-me-vehicles-vin-services.serviceRequest-technical-adapter.post-email.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.post-users-me-vehicles-vin-services.serviceRequest-technical-adapter.post-email.response"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.post-users-me-vehicles-vin-services.serviceRequest-technical-adapter.get-vehicle.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.post-users-me-vehicles-vin-services.serviceRequest-technical-adapter.get-vehicle.response"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.post-users-me-vehicles-vin-services.serviceRequest-technical-adapter.put-vehicle.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.post-users-me-vehicles-vin-services.serviceRequest-technical-adapter.put-vehicle.response"))
				.times(1);

		System.out.println(output.getMessage().getPayloadAsString());
		String expectedResponse = IOUtils.toString(getClass().getResourceAsStream("/out/post-users-me-vehicles-vin-services-serviceRequest-api-response.json"));
		assertJsonEquals(expectedResponse, output.getMessage().getPayloadAsString());
	}
	
	
	@Test
	public void testSuccessfulWithWarningRequest() throws Exception {

		//mock get:/dealers/{dealerCode} response 
		whenMessageProcessor("flow-ref")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("get:/dealers/{dealerCode}:call-va-api"))
				.thenReturn(muleMessageWithPayload(
						IOUtils.toString(getClass().getResourceAsStream("/in/get-dealers-dealerId-response.json"))));
		
		//mock post:/comms/email response 
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
				attribute("name").ofNamespace("doc").withValue("PING API: POST /comms/email"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						
						try {
							newMessage = originalMessage.createInboundMessage();
							
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							newMessage.setProperty("Content-Type", "application/json;charset=UTF-8", PropertyScope.INBOUND);
							
							String pingResponse = IOUtils
									.toString(getClass()
											.getResourceAsStream(
													"/in/post-email-ping-response.json"));
							newMessage.setPayload(IOUtils.toInputStream(pingResponse, "UTF-8"));
						} catch (Exception e) {
							// catch exception statements
						}
						return newMessage;
					}
				});
		
		//mock get:/users/{myToyotaId}/vehicles/{vin} response 
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
				attribute("name").ofNamespace("doc").withValue("My Toyota App API GET /users/{myToyotaId}/vehicles/{vin}"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						
						try {
							newMessage = originalMessage.createInboundMessage();
							
							newMessage.setProperty("http.status", 400, PropertyScope.INBOUND);
							newMessage.setProperty("http.reason", "Bad Request", PropertyScope.INBOUND);
							//newMessage.setProperty("Content-Type", "application/json;charset=UTF-8", PropertyScope.INBOUND);
							
//							newMessage.setPayload(getClass()
//									.getResourceAsStream("/in/get-users-me-vehicles-vin-mykit-response.json"));
						} catch (Exception e) {
							// catch exception statements
						}
						return newMessage;
					}
				});
		
		MuleEvent event = testEvent("");
		
		MuleMessage msg = muleMessageWithPayload(IOUtils
				.toString(getClass()
						.getResourceAsStream(
								"/in/post-users-me-vehicles-vin-services-serviceRequest-primary-contact-api-request.json")));
		
        event.setMessage(msg);
		
		// Invoke the flow
		MuleEvent output = runFlow("post:/users/me/vehicles/{vin}/services/serviceRequest:myToyota-config", event);

		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category")
						.withValue("esb.mytoyota-api-v1.post-users-me-vehicles-vin-services.serviceRequest.request"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category")
						.withValue("esb.mytoyota-api-v1.post-users-me-vehicles-vin-services.serviceRequest.response"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.post-users-me-vehicles-vin-services.serviceRequest-technical-adapter.request"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.post-users-me-vehicles-vin-services.serviceRequest-technical-adapter.response"))
				.times(1);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.post-users-me-vehicles-vin-services.serviceRequest-technical-adapter.post-email.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.post-users-me-vehicles-vin-services.serviceRequest-technical-adapter.post-email.response"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.post-users-me-vehicles-vin-services.serviceRequest-technical-adapter.get-vehicle.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.post-users-me-vehicles-vin-services.serviceRequest-technical-adapter.get-vehicle.response"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.post-users-me-vehicles-vin-services.serviceRequest-technical-adapter.put-vehicle.request"))
				.times(0);
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.post-users-me-vehicles-vin-services.serviceRequest-technical-adapter.put-vehicle.response"))
				.times(0);

		System.out.println(output.getMessage().getPayloadAsString());
		String expectedResponse = IOUtils.toString(getClass().getResourceAsStream("/out/post-users-me-vehicles-vin-services-serviceRequest-api-warning-response.json"));
		assertJsonEquals(expectedResponse, output.getMessage().getPayloadAsString());
	}
	
	
	@Test
	public void testFailuresStatusCodes() throws Exception {

		int[] statusCodes = new int[] { 404, 405, 415, 406, 401, 500 };

		whenMessageProcessor("flow-ref")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("get:/dealers/{dealerCode}:call-va-api"))
				.thenReturn(muleMessageWithPayload(
						IOUtils.toString(getClass().getResourceAsStream("/in/get-dealers-dealerId-response.json"))));
		
		for (int i = 0; i < statusCodes.length; i++) {
			MuleEvent event = testEvent("");
			MuleMessage msg = muleMessageWithPayload(IOUtils
					.toString(getClass()
							.getResourceAsStream(
									"/in/post-users-me-vehicles-vin-services-serviceRequest-primary-contact-api-request.json")));
			event.setMessage(msg);
			
			new StatusCodesTestingHelper().runFailureTest(statusCodes[i], "PING API: POST /comms/email",
					"post:/users/me/vehicles/{vin}/services/serviceRequest:myToyota-config", event);
		}
	}
}
