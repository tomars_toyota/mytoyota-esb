package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;

import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

public class GetUsersSfdcResponseMapperTest extends AbstractMuleContextTestCase {

	private ScriptRunner scriptRunner;

	public GetUsersSfdcResponseMapperTest() throws ScriptException, SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/get-users-sfdc-response-mapper.groovy");
	}

	@Test
	public void testRequestMapper() throws Exception {

		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(
						getClass().getResourceAsStream("/in/get-users-sfdc-response.xml")))
				.run().toString();

		System.out.println("result: "+result);

		// expected
		String expectedResult = IOUtils.toString(getClass().getResourceAsStream("/out/get-users-sfdc-api-response.json"));
		JSONAssert.assertEquals(expectedResult, result, true);
	}
}
