package au.com.toyota.esb.mytoyota.api.mappings;

import static org.junit.Assert.assertTrue;

import java.io.IOException;
import java.util.Properties;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

import javax.script.*;
import org.junit.Assert;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.xml.sax.SAXException;

public class GetDealerByDealerCodeResponseMapperTest extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;
	private Properties props;
	
	public GetDealerByDealerCodeResponseMapperTest() throws ScriptException,
			SAXException, IOException {
		
		scriptRunner = ScriptRunner
				.createScriptRunner("/mappings/get-dealers-dealerCode-response-mapper.groovy");
		
		//load properties
		props = new Properties();
		props.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));
	}
	
	
	/*
	 * Dealer has Site ID in Network Central, and myToyota 'enabled delaers' property includes the dealer code 
	 */
	@Test
	public void testServiceBookingSupportedRequestMapper() throws Exception {
		String expectedResult = IOUtils.toString(
				getClass().getResourceAsStream("/out/get-dealers-by-dealer-code-service-site-id-response.json"));
		
		String serviceBookingDealerCodes = props.getProperty("api.tune.service.booking.enabled.dealercodes.no.leading.zero");
		
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(
						getClass().getResourceAsStream("/in/get-dealers-by-dealer-code-service-site-id.json")))
				.binding("serviceBookingDealerCodes", serviceBookingDealerCodes)
				.run().toString();
		
		System.out.println(result);

		Assert.assertEquals(expectedResult, result);
	}
	
	/*
	 * Dealer has Site ID in Network Central, and myToyota 'enabled delaers' property *does not* include the dealer code 
	 */
	@Test
	public void testNotServiceBookingSupportedRequestMapper() throws Exception {
		String expectedResult = IOUtils.toString(
				getClass().getResourceAsStream("/out/get-dealers-by-dealer-code-service-site-id-not-service-booking-supported-response.json"));
		
		String serviceBookingDealerCodes = props.getProperty("api.tune.service.booking.enabled.dealercodes.no.leading.zero");
		
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(
						getClass().getResourceAsStream("/in/get-dealers-by-dealer-code-service-site-id-not-service-booking-supported.json")))
				.binding("serviceBookingDealerCodes", serviceBookingDealerCodes)
				.run().toString();
		
		System.out.println(result);

		Assert.assertEquals(expectedResult, result);
	}
}
