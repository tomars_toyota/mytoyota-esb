package au.com.toyota.esb.mytoyota.api;

import static org.mule.munit.common.mocking.Attribute.attribute;

import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.util.IOUtils;
import org.mule.util.StringUtils;
import org.skyscreamer.jsonassert.JSONAssert;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;

public class GetSystemTimeMunitTest extends FunctionalMunitSuite {

	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"get-system-time.xml",
				"get-system-time-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulSystemTime() throws Exception {
		
		String requestPath = "/api/myToyota/v1/system/time";

		
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Get System TIme"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							String responsePayload = IOUtils.toString(
									getClass().getResourceAsStream("/in/get-system-time-utility-response.json"));
							newMessage.setPayload(responsePayload);
						} catch (Exception e) {
							// catch exception statements
						}
						return newMessage;
					}
				});
		
		MuleEvent event = testEvent(requestPath);
		MuleMessage message = muleMessageWithPayload(null);
		
		event.setMessage(message);	
		
		// Invoke the flow
		MuleEvent output = runFlow("get:/system/time:myToyota-config", event);
		
		System.out.println(output.getMessage().getPayload());
		
		// expected
		String expectedResult = IOUtils.toString(getClass().getResourceAsStream(
				"/out/get-system-time-response.json"));
		
		JSONAssert.assertEquals(output.getMessage().getPayloadAsString(), expectedResult, true);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.get-system-time.technical-adapter.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.get-system-time.technical-adapter.response"))
				.times(1);
	}
	

}
