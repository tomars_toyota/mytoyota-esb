import org.mule.api.transport.PropertyScope
import org.mule.module.apikit.exception.*
import au.com.toyota.esb.mytoyota.api.exception.UnauthorisedException

def sucessStatuses = [200, 201, 202] 
status = message.getProperty("http.status", PropertyScope.INBOUND)

if (status instanceof String)
	status = new Integer(status)


println("sucessStatuses:"+ sucessStatuses)


if(binding.hasVariable("ignoredErrors")) {
   println("ignoredErrors:"+ ignoredErrors)
}

if(binding.hasVariable("ignoredErrors") && !ignoredErrors?.isEmpty()) {
   println("ignoredErrors:"+ ignoredErrors)
   sucessStatuses.addAll(ignoredErrors.split(",").collect{it.toInteger()})
}
println ("sucessStatuses:"+ sucessStatuses)
if(status in sucessStatuses) {
	println ("status is a success status:")
	return payload
}
reason = message.getProperty("http.reason", PropertyScope.INBOUND) ?: ''
excepMsg = message.getProperty("message", PropertyScope.INBOUND) ?: ''

msg  = ''

if(reason && excepMsg)
	msg = reason + ', ' + excepMsg
else if(reason)
	msg = reason
else if(excepMsg)
	msg = excepMsg


flowVars['originalPayload'] = payload

//println '_______status: ' + status

switch(status) {
	case 404:
		throw new NotFoundException(msg)
		break
	case 405:
		throw new MethodNotAllowedException(null, null)
		break
	case 415:
		throw new UnsupportedMediaTypeException()
		break
	case 406:
		throw new NotAcceptableException()
		break
	case 400:
		throw new BadRequestException(msg)
		break
	case 401:
		throw new UnauthorisedException(msg)
		break
	default:
		throw new Exception((String)msg)
}