package au.com.toyota.esb.mytoyota.api;

import static org.junit.Assert.*;
import static org.mule.munit.common.mocking.Attribute.attribute;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.munit.runner.functional.FunctionalMunitSuite;

import com.jayway.jsonpath.JsonPath;

@Ignore
public class PutUpdateUserAccountInfoMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"create-salesforce-sessionid-soap-header.xml",
				"put-update-user-account-info.xml",
				"put-update-user-account-info-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulRequest() throws Exception {

		String apiResponse = IOUtils.toString(getClass().getResourceAsStream("/in/get-vehicle-details-response.json"));
		String requestPath = "/users/me";

		whenMessageProcessor("request").ofNamespace("http")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("Vehicle API GET vehicle by vin"))
			.thenReturn(muleMessageWithPayload(apiResponse));

		MuleEvent event = testEvent(requestPath);
		//event.setFlowVariable("vin", vin);

		MuleEvent output = runFlow("put:/users/me:myToyota-config", event);

		System.out.println(output.getMessage().getPayload());

	//	assertEquals(vin, JsonPath.read(output.getMessage().getPayloadAsString(), "$.vin"));
		assertEquals("Camry L4 Altise 2.4L Petrol Automatic Sedan", JsonPath.read(output.getMessage().getPayloadAsString(), "$.vehicleDescription"));
	}
	
	@Test
	public void unsuccessfulRequest() throws Exception {

		final String vin = "ABC123";
		final String requestPath = "/api/myToyota/v1/vehicles/" + vin;

		whenMessageProcessor("request").ofNamespace("http")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("Vehicle API GET vehicle by vin"))
			.thenThrow(new org.mule.module.apikit.exception.NotFoundException(requestPath));
		
		MuleEvent event = testEvent(requestPath);
		event.setFlowVariable("vin", vin);

		try {
			runFlow("get:/vehicles/{vin}:myToyota-config", event);
		} catch (Exception e) {
			assertTrue(e.getCause() instanceof org.mule.module.apikit.exception.NotFoundException);
			assertEquals(requestPath, e.getCause().getMessage());
		}

	}
	
}
