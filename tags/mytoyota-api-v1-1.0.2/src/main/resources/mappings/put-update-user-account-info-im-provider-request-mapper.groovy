
import groovy.json.JsonSlurper
import groovy.json.JsonBuilder

def user = new JsonSlurper().parseText(payload)

def result = [:]
def profile = [:]

if (user?.firstName)
	profile.firstName = user.firstName

if (user?.lastName)
	profile.lastName = user.lastName

if (user?.mobile) {
	profile."phones.type" = "mobile"
	profile."phones.number" = user.mobile
}

if (user?.email)
	profile.email = user.email

if (user?.login)
	result.username = user.login

result.data = [:]

if (user?.applicationIdentifiers)
	result.data.applicationIdentifiers = user.applicationIdentifiers

if (!profile.isEmpty())
	result.profile = profile

return new JsonBuilder(result).toString()