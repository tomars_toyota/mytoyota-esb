import groovy.json.JsonSlurper
import groovy.json.JsonOutput
import groovy.time.TimeCategory

// Helper to remove leading zeroes
// Used when checking dealer code against known codes that support service booking
def stripLeadingZeros = { s ->
	return s.replaceFirst("^0*", "")
}

vehicle = new JsonSlurper().parseText(payload)

actualBatch = vehicle?.batchNumber

def validationResponse = [:]
	
if (stripLeadingZeros(sessionVars['receivedBatch'])?.trim() != stripLeadingZeros(actualBatch)?.trim())
	validationResponse.isValid = false
else 
	validationResponse.isValid = true

return JsonOutput.toJson(validationResponse)