package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;
import java.util.HashMap;

import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.custommonkey.xmlunit.XMLTestCase;
import org.junit.Test;
import org.xml.sax.SAXException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

public class UpdateSfdcUserRequestMapper extends XMLTestCase {

	private ScriptRunner scriptRunner;

	public UpdateSfdcUserRequestMapper() throws ScriptException, SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/sfdc-update-user-request-mapper.groovy");
	}

	@Test
	public void testRequestMapper() throws Exception {

		HashMap<String, Object> payload = new HashMap<String, Object>();
		payload.put("firstName", "John");
		payload.put("lastName", "Smith");
		payload.put("mobilePhone", "0412344321");
		payload.put("email", "jsmith@mail.com.au");
		payload.put("salesforceId", "0011900000NDTZc");
		payload.put("isMarketingOptIn", true);
		payload.put("vin", "JTNKU3JE60J021675");
			
		
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(payload)
				.flowVar("myToyotaId", "MYT-000003a3")
				.run().toString();

		System.out.println(result);

		// expected
		String expectedResult = IOUtils.toString(getClass().getResourceAsStream("/out/updatOwner-sfdc-request.xml"));
		assertXMLEqual(expectedResult, result);
	}
}
