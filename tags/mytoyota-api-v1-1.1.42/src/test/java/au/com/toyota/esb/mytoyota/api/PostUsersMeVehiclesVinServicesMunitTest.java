package au.com.toyota.esb.mytoyota.api;

import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.util.IOUtils;
import org.mule.util.StringUtils;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;


/*
 * Munit test to unit test the functionality to get service operation time through 
 * /users/me/vehicles/{vin}/services/operationTime
 * @author: ahuwait
 */
public class PostUsersMeVehiclesVinServicesMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"post-users-me-vehicles-vin-services.xml",
				"post-users-me-vehicles-vin-services-tune-technical-adapter.xml",
				"post-users-me-vehicles-vin-services-myKit-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulCreateServiceBooking() throws Exception {
		testSuccessfulCreateServiceBookingImpl("TUNE");
	}
	
	@Test
	public void testSuccessfulCreateServiceBookingTuneLowerCase() throws Exception {
		testSuccessfulCreateServiceBookingImpl("tune");
	}
	
	private void testSuccessfulCreateServiceBookingImpl(String dmsType) throws Exception {
		
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
			attribute("name").ofNamespace("doc").withValue("TUNE API: POST /createBooking"))
			.thenApply(new MockResponseTransformer() {
				@Override
				public MuleMessage transform(MuleMessage originalMessage) {
					MuleMessage newMessage = null;
					
					try {
						newMessage = originalMessage.createInboundMessage();
						
						newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
						newMessage.setProperty("Content-Type", "application/json;charset=UTF-8", PropertyScope.INBOUND);
						
						String tuneResponse = "{\"bookingData\":[{\"jobcardID\":\"TB16392302\"}]}";
						newMessage.setPayload(IOUtils.toInputStream(tuneResponse, "UTF-8"));
					} catch (Exception e) {
						// catch exception statements
					}
					
					return newMessage;
				}
			});
		
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
				attribute("name").ofNamespace("doc").withValue("MyKit API: POST /users/{myToyotaId}/services"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						
						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("Content-Type", "application/json;charset=UTF-8", PropertyScope.INBOUND);
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							newMessage.setPayload("");
						} catch (Exception e) {
							// catch exception statements
						}
						
						return newMessage;
					}
				});
		
		
		MuleEvent event = testEvent("");
		event.setFlowVariable("vin", "6T153BK360X070769");
		event.setFlowVariable("myToyotaId","MYT-000001f6");
		
		Map<String, Object> props = new HashMap<String,Object>();
        props.put("dmsType", dmsType);
        props.put("dmsID", "61.10169.10169011116");
        
		
        MuleMessage msg = muleMessageWithPayload(IOUtils.toString(getClass()
				.getResourceAsStream("/in/post-users-me-vehicles-vin-services-request.json")));
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
		
        
        event.setMessage(msg);		
		
		// Invoke the flow
		MuleEvent output = runFlow("post:/users/me/vehicles/{vin}/services:myToyota-config", event);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.post-users-me-vehicles-vin-services.response"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.post-users-me-vehicles-vin-services.response"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("category")
				.withValue("esb.mytoyota-api-v1.post-users-me-vehicles-vin-services.tune.createbooking.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.post-users-me-vehicles-vin-services.tune.createbooking.response"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.post-users-me-vehicles-vin-services.mykit.createbooking.response"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.post-users-me-vehicles-vin-services.mykit.createbooking.request"))
				.times(1);
		
		System.out.println("_____________________"+output.getMessage().getPayloadAsString());
		assertJsonEquals("{\"bookingId\":\"TB16392302\"}", output.getMessage().getPayloadAsString());

	}
	
	@Test
	public void testNonTuneCreateServiceBooking() throws Exception {
		
		MuleEvent event = testEvent("");
		
		Map<String, Object> props = new HashMap<String,Object>();
		props.put("dmsType", "nonTune");
        
		
        MuleMessage msg = muleMessageWithPayload(IOUtils.toString(getClass()
				.getResourceAsStream("/in/post-users-me-vehicles-vin-services-request.json")));
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
        
        event.setMessage(msg);		
		
		// Invoke the flow
		runFlow("post:/users/me/vehicles/{vin}/services:myToyota-config", event);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.post-users-me-vehicles-vin-services.response"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.post-users-me-vehicles-vin-services.response"))
				.times(1);

		
		verifyCallOfMessageProcessor("logger").withAttributes(
				attribute("name").ofNamespace("doc").withValue(
						"Extend with more DMS")).times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("category")
				.withValue("esb.mytoyota-api-v1.post-users-me-vehicles-vin-services.tune.createbooking.request"))
				.times(0);
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.post-users-me-vehicles-vin-services.tune.createbooking.response"))
				.times(0);

		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.post-users-me-vehicles-vin-services.mykit.createbooking.response"))
				.times(0);
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.post-users-me-vehicles-vin-services.mykit.createbooking.request"))
				.times(0);
		
	}
	
	@Test
	public void testFailuresStatusCodes() throws Exception {

		int[] statusCodes = new int[] { 404, 405, 415, 406, 401, 500 };

		for (int i = 0; i < statusCodes.length; i++) {
			MuleEvent event = testEvent("");
			event.setFlowVariable("vin", "6T153BK360X070769");
			event.setFlowVariable("myToyotaId","MYT-000001f6");
			
			Map<String, Object> props = new HashMap<String,Object>();
	        props.put("dmsType", "TUNE");
	        props.put("dmsID", "61.10169.10169011116");
	        
			
	        MuleMessage msg = muleMessageWithPayload(IOUtils.toString(getClass()
					.getResourceAsStream("/in/post-users-me-vehicles-vin-services-request.json")));
			msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
			
	        
	        event.setMessage(msg);
	        
			new StatusCodesTestingHelper().runFailureTest(statusCodes[i], "TUNE API: POST /createBooking",
					"post:/users/me/vehicles/{vin}/services:myToyota-config", event);
		}
	}
}
