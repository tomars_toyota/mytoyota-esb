package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;

import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.custommonkey.xmlunit.XMLTestCase;
import org.junit.Test;
import org.xml.sax.SAXException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

public class QuerySfdcRequestMapperTest extends XMLTestCase {

	private ScriptRunner scriptRunner;

	public QuerySfdcRequestMapperTest() throws ScriptException, SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/sfdc-query-request-mapper.groovy");
	}

	//@Test
	public void testRequestMapper() throws Exception {
		
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload("")
				.flowVar("sfdcQueryString", "SELECT ID,FirstName,LastName,PersonEmail FROM Account WHERE My_Toyota_Id__c = 'TEST-000002e9'")
				.run().toString();

		System.out.println(result);

		// expected
		String expectedResult = IOUtils.toString(getClass().getResourceAsStream("/out/sfdc-query-request.xml"));
		assertXMLEqual(expectedResult, result);
	}
}
