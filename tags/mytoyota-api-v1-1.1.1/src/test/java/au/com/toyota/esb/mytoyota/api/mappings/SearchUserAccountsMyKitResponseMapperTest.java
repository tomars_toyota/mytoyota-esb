package au.com.toyota.esb.mytoyota.api.mappings;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

public class SearchUserAccountsMyKitResponseMapperTest extends AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;
	
	public SearchUserAccountsMyKitResponseMapperTest() throws ScriptException, SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/search-accounts-in-mykit-response-mapper.groovy");
	}
	
	//@Test
	public void testRequestMapper() throws Exception {
		Object result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(getClass().getResourceAsStream(
						"/in/search-accounts-by-mobile-in-mykit-response.json")))
				.run();
		String resultJson = new ObjectMapper().writeValueAsString(result);
		System.out.println(resultJson);
		// expected
		String expectedResult = IOUtils.toString(getClass().getResourceAsStream(
				"/out/search-accounts-by-mobile-api-response.json"));
		JSONAssert.assertEquals(expectedResult, resultJson, true);
	}
	
	//@Test
	public void testNoAccountFound() throws Exception {
		try {
			ScriptRunBuilder.runner(scriptRunner).payload("[]").run();
		} catch (Exception e) {
			System.out.println("**** Exception: "+e.getCause());
			assertTrue(e.getCause().getCause() instanceof org.mule.module.apikit.exception.NotFoundException);
		}
	}
}
