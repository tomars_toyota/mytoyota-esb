package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.junit.Assert;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.xml.sax.SAXException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

public class PostUsersMeVehiclesVinServicesValidateRequestTest extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;
	private Properties props;

	public PostUsersMeVehiclesVinServicesValidateRequestTest() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner
				.createScriptRunner("/mappings/post-users-me-vehicles-vin-services-validate-request.groovy");
		//load properties
		props = new Properties();
		props.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));
	}
	
	//@Test
	public void testServiceBookingSupportedValidation() throws Exception {
		String apiRequest = IOUtils
				.toString(getClass().getResourceAsStream("/in/post-users-me-vehicles-vin-services-request-successful.json"));
		
		String serviceBookingDealerCodes = props.getProperty("api.tune.service.booking.enabled.dealercodes.no.leading.zero");
		
		// expected is same as request, as this mapper is performing only validation and no mapping
		String expectedResult = IOUtils.toString(
				getClass().getResourceAsStream("/in/post-users-me-vehicles-vin-services-request-successful.json"));

		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(apiRequest)
				.flowVar("dmsID","61.10169.10169011116")
				.flowVar("dmsType","TUNE")
				.flowVar("vin","JTMBFREV80D078597")
				.flowVar("dmsBookingId", "TB16392302")
				.binding("serviceBookingDealerCodes", serviceBookingDealerCodes)
				.run().toString();

		System.out.println(result);

		Assert.assertEquals(expectedResult, result);
	}
	
	//@Test
	public void testNotServiceBookingSupportedValidation() throws Exception {
		String apiRequest = IOUtils
				.toString(getClass().getResourceAsStream("/in/post-users-me-vehicles-vin-services-request-not-service-booking-supported.json"));
		
		String serviceBookingDealerCodes = props.getProperty("api.tune.service.booking.enabled.dealercodes.no.leading.zero");
		
		// expected is same as request, as this mapper is performing only validation and no mapping
		String expectedResult = IOUtils.toString(
				getClass().getResourceAsStream("/in/post-users-me-vehicles-vin-services-request-not-service-booking-supported.json"));

		try { 
			ScriptRunBuilder
				.runner(scriptRunner)
				.payload(apiRequest)
				.flowVar("dmsID","61.10034.10034011130")
				.flowVar("dmsType","TUNE")
				.flowVar("vin","JTMBFREV80D078597")
				.flowVar("dmsBookingId", "TB16392366")
				.binding("serviceBookingDealerCodes", serviceBookingDealerCodes)
				.run().toString();
		} catch (Exception e) {
			if(e.getMessage().contains("Service booking not supported for 2499"))
				assert(true);
			else
				assert(false);
			return;
		}
		assert(false);
	}
	
	//@Test
	public void testUnsuccessful() throws Exception {
		String apiRequest = IOUtils
				.toString(getClass().getResourceAsStream("/in/post-users-me-vehicles-vin-services-request-unsuccessful.json"));
		try { 
			ScriptRunBuilder
				.runner(scriptRunner)
				.payload("")
				.flowVar("dmsID","61.10169.10169011116")
				.flowVar("dmsType","TUNE")
				.flowVar("vin","JTMBFREV80D078597")
				.flowVar("dmsBookingId", "TB16392302")
				.sessionVar("apiRequest", apiRequest)
				.run().toString();
		} catch (Exception e) {
			if(e.getMessage().contains("Invalid request"))
				assert(true);
			else
				assert(false);
			return;
		}
		assert(false);
	}
}
