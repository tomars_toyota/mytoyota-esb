package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;
import java.util.HashMap;

import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.custommonkey.xmlunit.XMLTestCase;
import org.junit.Test;
import org.xml.sax.SAXException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

public class PostCosiRegistrationAsyncRegisterGuestTmcSfdcRequestMapperTest extends XMLTestCase {
	private ScriptRunner scriptRunner;

	public PostCosiRegistrationAsyncRegisterGuestTmcSfdcRequestMapperTest() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/sfdc-register-guest-tmc-request-mapper.groovy");
	}

	//@Test
	public void testSuccessfulRequestMapper() throws Exception {
		
//    	{
//		  "login": "james.a.garner@gmail.com",
//		  "password": "password1",
//		  "email": "james.a.garner@gmail.com",
//		  "firstName": "Pinky",
//		  "lastName": "Ray",
//		  "isMarketingOptIn": true,
//		  "termsAndConditions": {
//		    "acceptedVersion": "1.1"
//		  },
//		  "privacyStatement": {
//		    "acceptedVersion": "1.1"
//		  },
//		  "cosi": "0007518430"
//		}
    	
    	HashMap<String, Object> frontendRequestMap = new HashMap<String, Object>();
    	
    	frontendRequestMap.put("login", "james.a.garner@gmail.com");
    	frontendRequestMap.put("password", "password1");
    	frontendRequestMap.put("email", "james.a.garner@gmail.com");
    	frontendRequestMap.put("firstName", "Pinky");
    	frontendRequestMap.put("lastName", "Ray");
    	frontendRequestMap.put("isMarketingOptIn", true);
    	
    	HashMap<String, Object> termsAndConditionsMap = new HashMap<String, Object>();
    	termsAndConditionsMap.put("acceptedVersion", "1.1");
    	frontendRequestMap.put("termsAndConditions", termsAndConditionsMap);
    	
    	HashMap<String, Object> privacyStatementMap = new HashMap<String, Object>();
    	privacyStatementMap.put("acceptedVersion", "1.1");
    	frontendRequestMap.put("privacyStatement", privacyStatementMap);
    	
    	frontendRequestMap.put("cosi", "0007518430");
    	
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload("")
				.sessionVar("orderResponse",
						IOUtils.toString(getClass()
								.getResourceAsStream("/in/post-cosi-registration-async-get-order-response.json")))
				.flowVar("requestPayload", frontendRequestMap)
				.sessionVar("myToyotaId", "TEST-00000270")
				.sessionVar("encryptedPassword", "b25bb41f9df51163d12dfbe2819894b5")
				.run().toString();

		System.out.println(result);
		
		// expected
		String expectedResult = IOUtils.toString(getClass()
				.getResourceAsStream(
						"/out/post-cosi-registration-async-register-guest-tmc-sfdc-request.xml"));
		
		assertXMLEqual(expectedResult, result);
	}
	
}
