package au.com.toyota.esb.mytoyota.api;

import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.verify;
import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.mockito.Matchers;
import org.mule.api.MuleEvent;
import org.mule.api.MuleException;
import org.mule.api.MuleMessage;
import org.mule.api.cache.InvalidatableCachingStrategy;
import org.mule.api.transport.PropertyScope;
import org.mule.module.apikit.exception.BadRequestException;
import org.mule.module.apikit.exception.NotFoundException;
import org.mule.munit.common.mocking.SpyProcess;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.custommonkey.xmlunit.Diff;
import org.custommonkey.xmlunit.XMLTestCase;
import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.SimplifiedMockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;
import au.com.toyota.esb.test.BlockingPayloadSpy;

//@Ignore
public class PutUsersPreferencesMunitTest extends FunctionalMunitSuite {

	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] { "put-users-preferences.xml", "config.xml",
				"create-salesforce-sessionid-soap-header.xml", "sfdc-update-guest-preferences.xml",
				"sfdc-technical-adapter.xml", "put-users-preferences-technical-adapter.xml" }, " ");
	}

	//@Test
	public void testSuccessfulUnsubscribeByEmailForRegisteredUser() throws Exception {

		String requestPayload = IOUtils
				.toString(getClass().getResourceAsStream("/in/put-users-preferences-request.json"));
		String expectedSFDCRequest = IOUtils
				.toString(getClass().getResourceAsStream("/in/salesforce-update-guest-preferences-request-email.xml"));
		String expectedResponse = "{\"success\": true}";

		// Mock myKIT get user by email response
		whenMessageProcessor("request").ofNamespace("http")
		.withAttributes(attribute("name").ofNamespace("doc").withValue("Search Users in myKIT"))
		.thenApply(new SimplifiedMockResponseTransformer(200, IOUtils
				.toString(getClass().getResourceAsStream("/in/search-accounts-by-email-in-mykit-response.json"))));
		
		// Mock sfdc login response
		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(
						attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Partner Operation login"))
				.thenReturn(muleMessageWithPayload(
						IOUtils.toString(getClass().getResourceAsStream("/in/sfdc-login-response.xml"))));

		// Mock sfdc update user prefs response
		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(attribute("name").ofNamespace("doc")
						.withValue("Invoke Salesforce Operation updateGuestPreferences"))
				.thenReturn(muleMessageWithPayload(IOUtils.toString(
						getClass().getResourceAsStream("/in/salesforce-update-guest-preferences-response.xml"))));

		Map<String, Object> props = new HashMap<String, Object>();
		props.put("email", "james.a.garner@gmail.com");
//		props.put("mobile", "0412341234");
		props.put("statusEmail", "true");

		MuleMessage msg = muleMessageWithPayload(requestPayload);
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
		
		MuleEvent event = testEvent("");
		event.setMessage(msg);
		
		msg.setProperty("myToyotaId", "",PropertyScope.SESSION);
		
		// spy
		BlockingPayloadSpy<?> spy = new BlockingPayloadSpy<>(1);

		spyMessageProcessor("component").ofNamespace("scripting")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Map updateGuestPreferences request")).after(spy);

		MuleEvent output = runFlow("put:/users/preferences:myToyota-config", event);

		System.out.println("SFDC Update Guest Preferences Request: " + spy.getFirstPayload().toString());
		Diff diff = new Diff(expectedSFDCRequest, spy.getFirstPayload().toString());
		assertTrue(diff.toString(), diff.similar());

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.put-users-preferences.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.put-users-preferences.response"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.put-users-preferences-technical-adapter.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.put-users-preferences-technical-adapter.response"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.search-accounts-in-mykit-subflow.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.search-accounts-in-mykit-subflow.response"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.sfdc-update-guest-preferences.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.sfdc-update-guest-preferences.response"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.sfdc-technical-adapter.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.sfdc-technical-adapter.response"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.put-users-preferences-technical-adapter.user-pref-updated"))
				.times(1);

		assertJsonEquals(expectedResponse, output.getMessage().getPayloadAsString());
	}
	
	
	//@Test
	public void testSuccessfulUnsubscribeByMobileForRegisteredUser() throws Exception {

		String expectedSFDCRequest = IOUtils
				.toString(getClass().getResourceAsStream("/in/salesforce-update-guest-preferences-request-mobile.xml"));
		String expectedResponse = "{\"success\": true}";

		// Mock myKIT get user by email response
		whenMessageProcessor("request").ofNamespace("http")
		.withAttributes(attribute("name").ofNamespace("doc").withValue("Search Users in myKIT"))
		.thenApply(new SimplifiedMockResponseTransformer(200, IOUtils
				.toString(getClass().getResourceAsStream("/in/search-accounts-by-mobile-in-mykit-response.json"))));
		
		// Mock sfdc login response
		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(
						attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Partner Operation login"))
				.thenReturn(muleMessageWithPayload(
						IOUtils.toString(getClass().getResourceAsStream("/in/sfdc-login-response.xml"))));

		// Mock sfdc update user prefs response
		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(attribute("name").ofNamespace("doc")
						.withValue("Invoke Salesforce Operation updateGuestPreferences"))
				.thenReturn(muleMessageWithPayload(IOUtils.toString(
						getClass().getResourceAsStream("/in/salesforce-update-guest-preferences-response.xml"))));

		Map<String, Object> props = new HashMap<String, Object>();
//		props.put("email", "james.a.garner@gmail.com");
		props.put("mobile", "0412341234");
//		props.put("statusEmail", "true");

		MuleMessage msg = muleMessageWithPayload(null);
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
		
		MuleEvent event = testEvent("");
		event.setMessage(msg);
		
		msg.setProperty("myToyotaId", "",PropertyScope.SESSION);
		
		// spy
		BlockingPayloadSpy<?> spy = new BlockingPayloadSpy<>(1);

		spyMessageProcessor("component").ofNamespace("scripting")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Map updateGuestPreferences request")).after(spy);

		MuleEvent output = runFlow("put:/users/preferences:myToyota-config", event);

		System.out.println("SFDC Update Guest Preferences Request: " + spy.getFirstPayload().toString());
		Diff diff = new Diff(expectedSFDCRequest, spy.getFirstPayload().toString());
		assertTrue(diff.toString(), diff.similar());

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.put-users-preferences.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.put-users-preferences.response"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.put-users-preferences-technical-adapter.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.put-users-preferences-technical-adapter.response"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.search-accounts-in-mykit-subflow.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.search-accounts-in-mykit-subflow.response"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.sfdc-update-guest-preferences.request"))
				.times(2);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.sfdc-update-guest-preferences.response"))
				.times(2);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.sfdc-technical-adapter.request"))
				.times(2);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.sfdc-technical-adapter.response"))
				.times(2);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.put-users-preferences-technical-adapter.user-pref-updated"))
				.times(2);
		assertJsonEquals(expectedResponse, output.getMessage().getPayloadAsString());
	}
	
	//@Test
	public void testUnsuccessfulUnsubscribeForRegisteredUser() throws Exception {

		String requestPayload = IOUtils
				.toString(getClass().getResourceAsStream("/in/put-users-preferences-request.json"));

		// Mock myKIT get user by email response
		whenMessageProcessor("request").ofNamespace("http")
		.withAttributes(attribute("name").ofNamespace("doc").withValue("Search Users in myKIT"))
			.thenApply(new SimplifiedMockResponseTransformer(404, null));

		Map<String, Object> props = new HashMap<String, Object>();
		props.put("email", "james.a.garner@gmail.com");
		props.put("statusEmail", "true");

		MuleMessage msg = muleMessageWithPayload(requestPayload);
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);

		MuleEvent event = testEvent("");
		event.setMessage(msg);

		MuleEvent output = null; 
		try {
			output = runFlow("put:/users/preferences:myToyota-config", event);

			verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
					.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.put-users-preferences.request"))
					.times(1);
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
					.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.put-users-preferences.response"))
					.times(1);
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
					attribute("category").withValue("esb.mytoyota-api-v1.put-users-preferences-technical-adapter.request"))
					.times(1);
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
					attribute("category").withValue("esb.mytoyota-api-v1.put-users-preferences-technical-adapter.response"))
					.times(1);
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
					attribute("category").withValue("esb.mytoyota-api-v1.search-accounts-in-mykit-subflow.request"))
					.times(1);
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
					attribute("category").withValue("esb.mytoyota-api-v1.search-accounts-in-mykit-subflow.response"))
					.times(0);
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
					.withAttributes(
							attribute("category").withValue("esb.mytoyota-api-v1.sfdc-update-guest-preferences.request"))
					.times(0);
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
					.withAttributes(
							attribute("category").withValue("esb.mytoyota-api-v1.sfdc-update-guest-preferences.response"))
					.times(0);
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
					.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.sfdc-technical-adapter.request"))
					.times(0);
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
					.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.sfdc-technical-adapter.response"))
					.times(0);
		} catch (Exception e) {
			assertEquals("status code 404 did not throw org.mule.module.apikit.exception.NotFoundException",
					e.getCause().getCause().getCause().getCause() instanceof NotFoundException, true);
			return;
		}
		assertTrue(false);
	}
	
	//@Test
	public void testSuccessfulUnsubscribeForUnregisteredUser() throws Exception {

		String requestPayload = IOUtils
				.toString(getClass().getResourceAsStream("/in/put-users-preferences-request.json"));
		String expectedSFDCRequest = IOUtils
				.toString(getClass().getResourceAsStream("/in/salesforce-update-guest-preferences-with-FLE-request.xml"));
		String expectedResponse = "{\"success\": true}";

		// Mock Vehicle API get order by email response
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Vehicle API GET orders by email"))
				.thenApply(new SimplifiedMockResponseTransformer(200,
						IOUtils.toString(getClass().getResourceAsStream("/in/get-orders-by-email-response.json"))));
		
		// Mock sfdc login response
		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(
						attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Partner Operation login"))
				.thenReturn(muleMessageWithPayload(
						IOUtils.toString(getClass().getResourceAsStream("/in/sfdc-login-response.xml"))));

		// Mock sfdc update user prefs response
		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(attribute("name").ofNamespace("doc")
						.withValue("Invoke Salesforce Operation updateGuestPreferences"))
				.thenReturn(muleMessageWithPayload(IOUtils.toString(
						getClass().getResourceAsStream("/in/salesforce-update-guest-preferences-response.xml"))));

		Map<String, Object> props = new HashMap<String, Object>();
		props.put("email", "mytoyotatest1+105@gmail.com");
		props.put("statusEmail", "false");

		MuleMessage msg = muleMessageWithPayload(requestPayload);
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
		
		MuleEvent event = testEvent("");
		event.setMessage(msg);
		
		msg.setProperty("myToyotaId", "",PropertyScope.SESSION);
		
		// spy
		BlockingPayloadSpy<?> spy = new BlockingPayloadSpy<>(1);

		spyMessageProcessor("component").ofNamespace("scripting")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Map updateGuestPreferences request")).after(spy);

		MuleEvent output = runFlow("put:/users/preferences:myToyota-config", event);

		System.out.println("SFDC Update Guest Preferences Request: " + spy.getFirstPayload().toString());
		Diff diff = new Diff(expectedSFDCRequest, spy.getFirstPayload().toString());
		assertTrue(diff.toString(), diff.similar());

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.put-users-preferences.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.put-users-preferences.response"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.put-users-preferences-technical-adapter.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.put-users-preferences-technical-adapter.response"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-orders-by-email-subflow.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-orders-by-email-subflow.response"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.sfdc-update-guest-preferences.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.sfdc-update-guest-preferences.response"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.sfdc-technical-adapter.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.sfdc-technical-adapter.response"))
				.times(1);

		assertJsonEquals(expectedResponse, output.getMessage().getPayloadAsString());
	}
}
