package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;
import java.util.HashMap;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

import javax.script.*;

import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;


public class GetUsersMeVehiclesVinServicesOutstandingOperationsResponseMapperTest extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;

	public GetUsersMeVehiclesVinServicesOutstandingOperationsResponseMapperTest() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner
				.createScriptRunner("/mappings/get-users-me-vehicles-vin-services-outstanding-operations-response-mapper.groovy");
	}

	@Test
	public void testResponseMapper() throws Exception {
		
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(getClass()
						.getResourceAsStream(
								"/in/get-users-me-vehicles-vin-services-getprocedure-tune-response.json")))
				.run().toString();
		
		System.out.println(result);

		// expected
		String expectedResult = IOUtils.toString(getClass()
				.getResourceAsStream(
						"/out/get-users-me-vehicles-vin-services-outstanding-operations-api-response.json"));
		JSONAssert.assertEquals(expectedResult, result, true);
	}
}
