import java.util.Date
import java.security.MessageDigest
import java.security.SecureRandom
import com.nimbusds.jose.JWEObject;
import com.nimbusds.jose.crypto.DirectDecrypter;
import com.nimbusds.jwt.SignedJWT;
import org.mule.api.transport.PropertyScope;
try {
	// session token string
	flowVars['token'] = message.getProperty('APISessionToken',PropertyScope.INBOUND)
   
	// validate the current presenting session token
	flowVars['validToken'] = 'false'
  
	if(flowVars.retrieveToken == 'false' && flowVars.token !=null){
	   
		JWEObject jweObject = JWEObject.parse(message.getInboundProperty('APISessionToken'))
		String secretKey = jwtSecretKey
	   
		MessageDigest md = MessageDigest.getInstance("SHA-256")
		md.update(secretKey.getBytes())
		byte[] encryptionKey = md.digest()
   
		jweObject.decrypt(new DirectDecrypter(encryptionKey))
   
		SignedJWT signedJWT = jweObject.getPayload().toSignedJWT()
	   
		Date tokenExpiry = signedJWT.getJWTClaimsSet().getExpirationTime()
		def issuer = signedJWT.getJWTClaimsSet().getIssuer()
	   
		// Validation - Time expiry check only
		if(
		issuer == jwtIssuer
		&& tokenExpiry.compareTo(new Date()) > 0
		) {
			flowVars['validToken'] = 'true'
			flowVars['UID'] = signedJWT.getJWTClaimsSet().getCustomClaim('UID')
			flowVars['myToyotaId'] = signedJWT.getJWTClaimsSet().getCustomClaim('myToyotaId')
			// UIDSignature
			// signedJWT.getJWTClaimsSet().getCustomClaim('uidSignature')
		}
	}
} catch (Exception e) {
	throw new org.mule.module.apikit.exception.BadRequestException(e.getMessage())
}
return payload
