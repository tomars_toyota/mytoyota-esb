package au.com.toyota.esb.mytoyota.api;

import static org.mule.munit.common.mocking.Attribute.attribute;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.modules.interceptor.processors.MuleMessageTransformer;
import org.mule.munit.runner.functional.FunctionalMunitSuite;


//@Ignore
public class PostUserVehiclesMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"post-add-user-vehicle.xml",
				"post-add-user-vehicle-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulRequest() throws Exception {

		whenMessageProcessor("request").ofNamespace("http").withAttributes(
				attribute("name").ofNamespace("doc").withValue("My Toyota App API POST Add Vehicle"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						
						try {
							newMessage = originalMessage.createInboundMessage();
							
							newMessage.setProperty("http.status", 201, PropertyScope.INBOUND);
							
							newMessage.setPayload(IOUtils.toInputStream("", "UTF-8"));
						} catch (Exception e) {
							// catch exception statements
						}
						
						return newMessage;
					}
				});
		String payload = IOUtils.toString(getClass().getResourceAsStream("/out/post-user-vehicles-mykit-request.json"));
		MuleEvent event = testEvent(payload);

		runFlow("post:/users/me/vehicles:myToyota-config", event);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
		.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.post-add-user-vehicle.request"))
		.times(1);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
			.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.post-add-user-vehicle.response"))
			.times(1);	
	
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
			.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.post-add-user-vehicle-technical-adapter.request"))
			.times(1);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
			.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.post-add-user-vehicle-technical-adapter.response"))
			.times(1);
	}
	
	// not possible to externalise this class because of the visibility of the
	// method muleMessageWithPayload
	private class MockResponseTransformer implements MuleMessageTransformer {
		int count = 0;
		String[] payloads;

		public MockResponseTransformer(String... payloads) {
			this.payloads = payloads;
		}

		@Override
		public MuleMessage transform(MuleMessage originalMessage) {
			return muleMessageWithPayload(payloads[count++]);
		}
	}
}
