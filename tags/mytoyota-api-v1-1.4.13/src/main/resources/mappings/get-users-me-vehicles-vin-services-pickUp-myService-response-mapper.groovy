import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper

JsonSlurper js = new JsonSlurper()
def timeSlotData = js.parseText(payload)

def response = [:]
response.availableDays = []

if(!timeSlotData) 
	return prettyPrint(toJson(response))

def availableDate = [:]
availableDate.date = flowVars.serviceDate
availableDate.times = timeSlotData
response.availableDays.add(availableDate)

return prettyPrint(toJson(response))