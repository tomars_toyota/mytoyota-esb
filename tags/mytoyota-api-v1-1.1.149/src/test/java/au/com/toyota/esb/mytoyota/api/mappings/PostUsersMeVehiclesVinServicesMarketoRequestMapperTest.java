package au.com.toyota.esb.mytoyota.api.mappings;

import static org.junit.Assert.*;

import java.io.IOException;

import javax.script.ScriptException;
import javax.validation.constraints.AssertTrue;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;
import org.mule.module.json.*;

public class PostUsersMeVehiclesVinServicesMarketoRequestMapperTest extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;

	public PostUsersMeVehiclesVinServicesMarketoRequestMapperTest() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner
				.createScriptRunner("/mappings/post-users-me-vehicles-vin-services-marketo-request-mapper.groovy");
	}
	
	@Test
	public void testSuccessful() throws Exception {
		String expectedResult = IOUtils.toString(
				getClass().getResourceAsStream("/out/post-users-me-vehicles-vin-services-marketo-request.json"));
		String apiRequest = IOUtils
				.toString(getClass().getResourceAsStream("/in/post-users-me-vehicles-vin-services-request.json"));
		String dealerResponse = IOUtils
				.toString(getClass().getResourceAsStream("/in/get-dealers-by-dealer-code-api-response.json"));
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload("")
				.flowVar("vin","JTMBFREV80D078597")
				.flowVar("myToyotaId","MYT-000000c8")
				.flowVar("dmsBookingId", "TB16392302")
				.sessionVar("apiRequest", apiRequest)
				.sessionVar("dealerResponse", dealerResponse)
				.run().toString();

		System.out.println(result);
		
		JsonData expectedJsonData = new JsonData(expectedResult);
		JsonData returnedJsonData = new JsonData(result);
		
		assertTrue(expectedJsonData.get("input[0]/myToyotaId").equals(returnedJsonData.get("input[0]/myToyotaId")) &&
				expectedJsonData.get("input[0]/firstName").equals(returnedJsonData.get("input[0]/firstName")) &&
				expectedJsonData.get("input[0]/lastName").equals(returnedJsonData.get("input[0]/lastName")) &&
				expectedJsonData.get("input[0]/email").equals(returnedJsonData.get("input[0]/email")) &&
				expectedJsonData.get("input[0]/mobilePhone").equals(returnedJsonData.get("input[0]/mobilePhone")) &&
				returnedJsonData.get("input[0]/serviceBookingDate") != null &&
				expectedJsonData.get("input[0]/serviceBookingVehicleRego").equals(returnedJsonData.get("input[0]/serviceBookingVehicleRego")) &&
				expectedJsonData.get("input[0]/serviceBookingVehicleVIN").equals(returnedJsonData.get("input[0]/serviceBookingVehicleVIN")) &&
				expectedJsonData.get("input[0]/serviceBookingNumber").equals(returnedJsonData.get("input[0]/serviceBookingNumber")) &&
				expectedJsonData.get("input[0]/serviceBookingQuote").equals(returnedJsonData.get("input[0]/serviceBookingQuote")) &&
				expectedJsonData.get("input[0]/serviceBookingTypeofService").equals(returnedJsonData.get("input[0]/serviceBookingTypeofService")) &&
				expectedJsonData.get("input[0]/serviceBookingDropOffDate").equals(returnedJsonData.get("input[0]/serviceBookingDropOffDate")) &&
				expectedJsonData.get("input[0]/serviceBookingDropOffTime").equals(returnedJsonData.get("input[0]/serviceBookingDropOffTime")) &&
				expectedJsonData.get("input[0]/serviceBookingPickUpDate").equals(returnedJsonData.get("input[0]/serviceBookingPickUpDate")) &&
				expectedJsonData.get("input[0]/serviceBookingPickUpTime").equals(returnedJsonData.get("input[0]/serviceBookingPickUpTime")) &&
				expectedJsonData.get("input[0]/serviceBookingDealerPhoneNumber").equals(returnedJsonData.get("input[0]/serviceBookingDealerPhoneNumber")) &&
				expectedJsonData.get("input[0]/serviceBookingDealerAddress").equals(returnedJsonData.get("input[0]/serviceBookingDealerAddress")) &&
				expectedJsonData.get("input[0]/serviceBookingDealerEmail").equals(returnedJsonData.get("input[0]/serviceBookingDealerEmail")) &&
				expectedJsonData.get("input[0]/serviceBookingDealerWebsite").equals(returnedJsonData.get("input[0]/serviceBookingDealerWebsite"))
				);
		
//		JSONAssert.assertEquals(expectedResult, result, true);
	}
}
