package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;
import java.util.HashMap;

import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.custommonkey.xmlunit.XMLTestCase;
import org.junit.Test;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

public class PostUsersRegisterAtImRequestMapperTest extends XMLTestCase {

	private ScriptRunner scriptRunner;

	public PostUsersRegisterAtImRequestMapperTest() throws ScriptException, SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/post-register-user-im-provider-request-mapper.groovy");
	}

	@Test
	public void testRequestMapper() throws Exception {

		HashMap<String, Object> payload = new HashMap<String, Object>();
		
		payload.put("firstName", "John");
		payload.put("login", "jsmith@mail.com");
		payload.put("password", "123");
		payload.put("email", "jsmith@mail.com");
		
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload("")
				.sessionVar("requestPayload", payload)
				.flowVar("finalizeRegistration", false)
				.run().toString();

		System.out.println(result);

		// expected
		String expectedResult = IOUtils.toString(getClass().getResourceAsStream("/out/post-users-register-at-im-request.json"));
		JSONAssert.assertEquals(expectedResult, result, true);
	}
}
