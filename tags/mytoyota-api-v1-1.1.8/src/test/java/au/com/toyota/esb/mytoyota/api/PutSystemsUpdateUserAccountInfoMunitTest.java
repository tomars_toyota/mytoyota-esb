package au.com.toyota.esb.mytoyota.api;

import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.mule.munit.common.mocking.Attribute.attribute;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;


public class PutSystemsUpdateUserAccountInfoMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"put-systems-update-user-account-info.xml",
				"put-update-user-account-info-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSystemsUpdateUserDetails() throws Exception {
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
			attribute("name").ofNamespace("doc").withValue("myKIT API POST Save Account"))
			.thenApply(new MockResponseTransformer() {
				@Override
				public MuleMessage transform(MuleMessage originalMessage) {
					MuleMessage newMessage = null;
					
					try {
						newMessage = originalMessage.createInboundMessage();
						
						newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
						newMessage.setProperty("Content-Type", "application/json;charset=UTF-8", PropertyScope.INBOUND);
						String myKitExpectedRequest = IOUtils.toString(getClass().getResourceAsStream("/out/put-update-user-account-info-mykit-request.json"));
						
						newMessage.setPayload(IOUtils.toInputStream(myKitExpectedRequest, "UTF-8"));
					} catch (Exception e) {
						// catch exception statements
					}
					
					return newMessage;
				}
			});
		
			
		MuleMessage msg = muleMessageWithPayload(IOUtils.toString(getClass().getResourceAsStream("/in/put-update-user-account-info-request.json")));
        msg.setProperty("myToyotaId", "MYT-000000cc", PropertyScope.INBOUND);
        
        MuleEvent event = testEvent("");
		event.setMessage(msg);		
		
		// Invoke the flow
		MuleEvent output = runFlow("put:/users:myToyota-config", event);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log API Request")).times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log API Response")).times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Request")).times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Response")).times(1);
		
		String expectedResult = "{\"success\":true}";
		
		//System.out.println("_____________________"+output.getMessage().getPayloadAsString());
		assertJsonEquals(expectedResult, output.getMessage().getPayloadAsString());
	}
	
	@Test
	public void testFailuresStatusCodes() throws Exception {

		int[] statusCodes = new int[] { 404, 405, 415, 406, 401, 500 };

		for (int i = 0; i < statusCodes.length; i++) {
			MuleMessage msg = muleMessageWithPayload(
					IOUtils.toString(getClass().getResourceAsStream("/in/put-update-user-account-info-request.json")));
			msg.setProperty("myToyotaId", "MYT-000000cc", PropertyScope.INBOUND);
	        
	        MuleEvent event = testEvent("");
			event.setMessage(msg);
	        
			new StatusCodesTestingHelper().runFailureTest(statusCodes[i], "myKIT API POST Save Account",
					"put:/users:myToyota-config", event);
		}
	}
}
