import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper
import static au.com.toyota.esb.mytoyota.api.util.GroovyHelper.*
import java.text.*

reward = new JsonSlurper().parseText(flowVars.rewardDetailsPayload)
reward.remove('isSuccessful')
reward.remove('message')
if(reward?.cardFront?.cardImage && !reward?.cardFront?.cardImage.startsWith("http"))
	reward.cardFront.cardImage='https://'+ sitecoreHost + reward.cardFront.cardImage

// TODO: Hack the prod internal address due to sitecore couldn't replace it properly
if (reward?.cardFront?.cardImage &&  reward?.cardFront?.cardImage.contains("prod-toyota-cd.tmca-digital.com.au:443"))
	reward.cardFront.cardImage = reward.cardFront.cardImage.replace("prod-toyota-cd.tmca-digital.com.au:443", sitecoreHost)

if(!isValidDate(reward?.expiryDate)) reward.expiryDate = null

reward << ["isClaimed": false] // default

/* TESTING-ONLY NEEDS TO BE COMMENTED BEFORE COMMITTING !!
reward.defautDiscountValue=20
reward.offerExpiryDate=formatDate(new Date()).toString()
reward.offerExpiryDateFormat="dd MMM yy"*/
def response = ""
try{
	switch(flowVars.rewardType) {
		case "subscription" :
			def subscriptionPayload = new JsonSlurper().parseText(payload);
			if(subscriptionPayload && subscriptionPayload.subscriptions && !subscriptionPayload.subscriptions.isEmpty()) {
				reward << ["subscriptions": subscriptionPayload.subscriptions]
				reward << ["isClaimed": true]
			}			
			break
		case "competition" :
			def competitionPayload = new JsonSlurper().parseText(payload);
			if(competitionPayload && competitionPayload.entries && !competitionPayload.entries.isEmpty()) {
				reward << ["entries": competitionPayload.entries]
				reward << ["isClaimed": true]
			}			
			break	
		default:
			if(flowVars.guestCouponPayload != null 
						&& !(flowVars.guestCouponPayload instanceof org.mule.transport.NullPayload)) {
				def couponList = new JsonSlurper().parseText(flowVars.guestCouponPayload)?.coupons
				def couponFirstItem = couponList.find { item -> item.dailyClaimedStatus == "VALID" }
				if (couponFirstItem != null && couponFirstItem.caltexId != null) {
					def pagesList = reward.rewardDetails?.pages
					pagesList?.find { pageItem ->
						if (pageItem.pageType == "qr-code") { //
							pageItem.offerCode = couponFirstItem.caltexId
							def engine = new groovy.text.SimpleTemplateEngine()
							/* TESTING-ONLY NEEDS TO BE COMMENTED BEFORE COMMITTING !!
							pageItem.titleSuffix = ' until ${offerExpiryDate}'*/
							def pageTitle = pageItem?.title
							if (pageTitle) {
								def binding = [:]
								binding.defaultDiscountValue = (couponFirstItem.discountValue)?couponFirstItem.discountValue:reward.defaultDiscountValue
								if(pageItem.titleSuffix) {
									def offerExpiryDate = (couponFirstItem.expirationDate)?couponFirstItem.expirationDate:reward.offerExpiryDate
									if(!isValidDate(offerExpiryDate)) 
										offerExpiryDate = null
									if(offerExpiryDate){
										binding.offerExpiryDate = formatDate(offerExpiryDate, reward.offerExpiryDateFormat)
										if(binding.offerExpiryDate != null)
											pageTitle = pageTitle + pageItem.titleSuffix
									}
								}
								pageItem.originalTitle = pageItem.title
								pageItem.title = engine.createTemplate(pageTitle).make(binding).toString() + "."
							}
							
							return true
						}
						else if (pageItem.pageType == "generic-static-coupon") {
							if (reward.couponType == "PREGEN") {
								pageItem.offerCode = couponFirstItem.couponCode
								// PREGEN couponType needs to throw an exception if we couldn't display a coupon
								if (!pageItem.offerCode) {
									throw new org.mule.module.apikit.exception.NotFoundException("We couldn't get a code at the moment. Please try again later.")
								}
								// apply binding to offerlink if necessary
								def binding = [:]
								binding.offerCode = pageItem.offerCode
								
								def engine = new groovy.text.SimpleTemplateEngine()
								def text = pageItem?.offerLink?.url
								if (text) {
									def template = engine.createTemplate(text).make(binding)
									def result = template.toString()
									pageItem.offerLink.url = result
								}
								return true
							}
							else {
								return false
							}
						}
						return false
					}
					
				}
				else {
					if (reward.couponType == "PREGEN") {
						throw new org.mule.module.apikit.exception.NotFoundException("We couldn't get a code at the moment. Please try again later.")
					}
				}
				
			}		
	}	

response = prettyPrint(toJson(removeNulls(reward, true)))
	
} catch(Exception ex) {
	println "EXCEPTION:" + ex.getMessage()
	ex.printStackTrace()
	throw ex
}
return response