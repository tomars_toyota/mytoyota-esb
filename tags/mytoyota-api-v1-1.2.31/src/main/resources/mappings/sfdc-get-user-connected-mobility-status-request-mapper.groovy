import groovy.xml.MarkupBuilder

def writer = new StringWriter()
def xmlMarkup = new MarkupBuilder(writer)


xmlMarkup.mkp.xmlDeclaration(version: "1.0", encoding: "UTF-8")
xmlMarkup.'sp:checkCMUserAndVehicle' (
	'xmlns:sp' : "http://soap.sforce.com/schemas/class/SP_UserRegWSTransactionStructure"
	) {
		'sp:request'{
			'sp:myToyotaId' (flowVars['myToyotaId'])
			'sp:VIN' (flowVars['vin'])
		}	
	}
return writer.toString()


	
//       <sp:checkCMUserAndVehicle>
//          <sp:request>
//             <sp:myToyotaId>MYT-000005b9</sp:mytoyotaId>
//             <sp:VIN>MR053REH205262123</sp:VIN>
//          </sp:request>
//       </sp:checkCMUserAndVehicle>