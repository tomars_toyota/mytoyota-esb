package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;

import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

public class GetUsersMeAccountSummaryResponseMapperTest extends AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;
	
	public GetUsersMeAccountSummaryResponseMapperTest() throws ScriptException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/get-users-me-account-summary-response-mapper.groovy");
	}
	
	@Test
	public void testResponseMapper() throws Exception {
		
		String result = ScriptRunBuilder.runner(scriptRunner)
				.payload(IOUtils
						.toString(getClass().getResourceAsStream("/in/get-vehicles-vin-api-response.json")))
				.flowVar("vin", "JTNKU3JE60J021675")
				.flowVar("userAccount", IOUtils
						.toString(getClass().getResourceAsStream("/in/get-users-me-api-response.json")))
				.run().toString();
						
		System.out.println(result);
		
		// expected
		String expectedResult = IOUtils.toString(getClass()
				.getResourceAsStream(
						"/out/get-users-me-account-summary-api-response.json"));
		JSONAssert.assertEquals(expectedResult, result, true);
	}
	
	@Test
	public void testNoActiveVehicleResponseMapper() throws Exception {
		
		String result = ScriptRunBuilder.runner(scriptRunner)
				.payload(org.mule.transport.NullPayload.getInstance())
				.flowVar("userAccount", IOUtils
						.toString(getClass().getResourceAsStream("/in/get-users-me-api-response.json")))
				.run().toString();
						
		System.out.println(result);
		
		// expected
		String expectedResult = IOUtils.toString(getClass()
				.getResourceAsStream(
						"/out/get-users-me-account-summary-no-active-vehicle-api-response.json"));
		JSONAssert.assertEquals(expectedResult, result, true);
	}
	
	
}
