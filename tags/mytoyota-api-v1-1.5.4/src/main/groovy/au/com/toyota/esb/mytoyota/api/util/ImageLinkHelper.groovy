package au.com.toyota.esb.mytoyota.api.util

public class ImageLinkHelper {
	def static String formatImageLink(imageLink) {
		
		def link = imageLink
		if (link != null) {
			if (link.startsWith("http:")) {
				link = link.replace("http:", "https:")
			}
			else if (link.startsWith("//")) {
				link = "https:" + link
			}
		}
		return link
	}
	
	

	
}