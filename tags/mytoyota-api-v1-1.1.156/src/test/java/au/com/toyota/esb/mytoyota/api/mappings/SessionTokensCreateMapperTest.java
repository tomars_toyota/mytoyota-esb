package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Properties;

import java.util.HashMap;

import javax.script.ScriptException;

import org.mule.DefaultMuleMessage;
import org.mule.api.MuleContext;
import org.mule.api.MuleEventContext;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.context.DefaultMuleContextFactory;

import org.apache.commons.io.IOUtils;
import org.custommonkey.xmlunit.XMLTestCase;
import org.junit.Test;
import org.xml.sax.SAXException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

public class SessionTokensCreateMapperTest extends XMLTestCase {

	private ScriptRunner createTokenScriptRunner;
	private ScriptRunner verifyTokenScriptRunner;
	
	private MuleContext muleContext;
	private Properties props;
	
	private static HashMap<String, Object> sessionToken;

	private final String myToyotaId = "MYT-0000063c";
	private final String UID = "04707fa24bc5400cbf7cd3db2ecbf94a";
	
	public SessionTokensCreateMapperTest() throws ScriptException, SAXException, IOException {
		createTokenScriptRunner = ScriptRunner.createScriptRunner("/mappings/jwt-token-create.groovy");
		verifyTokenScriptRunner = ScriptRunner.createScriptRunner("/mappings/jwt-token-validate.groovy");
		
		//load properties
		props = new Properties();
		props.load(getClass().getResourceAsStream("/mytoyota-api-v1-constants.properties"));
				
	}
					
	@Test
	public void testCreateToken() throws Exception {
		
		muleContext = new DefaultMuleContextFactory().createMuleContext();
		MuleMessage message = new DefaultMuleMessage(null,muleContext);
		
		String secretKey = props.getProperty("jwt.secret.key");
		String issuer = props.getProperty("jwt.issuer");
		
		HashMap<String, Object> flowVars = new HashMap<String, Object>();
		flowVars.put("myToyotaId", myToyotaId);
		flowVars.put("UID", UID);
		
		String createdToken = ScriptRunBuilder
				.runner(createTokenScriptRunner)
				.payload("")
				.flowVars(flowVars)
				.binding("tokenType", "tlinkSessionToken")
				.binding("secretKey", secretKey)
				.binding("issuer", issuer)
				.message(message)
				.run().toString();
		
		sessionToken = (HashMap<String, Object>) flowVars.get("APISessionToken");
		
		assertNotNull( sessionToken );
		assert( sessionToken.get("value").toString().length() > 0);
		assert( sessionToken.get("tokenType").toString().equals("tlinkSessionToken"));
		
		System.out.println("**** Session Token 'Value': " + sessionToken.get("value"));
		System.out.println("**** Session Token 'tokenType': " + sessionToken.get("tokenType"));
	}
	
	@Test
	public void testVerifyToken() throws Exception {
		
		assertNotNull( sessionToken );
		assertNotNull( sessionToken.get("value") );
		assertNotNull( sessionToken.get("tokenType") );
		
		muleContext = new DefaultMuleContextFactory().createMuleContext();
		MuleMessage message = new DefaultMuleMessage(null,muleContext);
		message.setProperty("tlinkSessionToken", sessionToken.get("value"), PropertyScope.INBOUND);
		
		String secretKey = props.getProperty("jwt.secret.key");		
		String issuer = props.getProperty("jwt.issuer");
		
		HashMap<String, Object> flowVars = new HashMap<String, Object>();
		flowVars.put("retrieveToken", "false");
		
		String verifiedToken = ScriptRunBuilder
				.runner(verifyTokenScriptRunner)
				.payload("")
				.flowVars(flowVars)
				.binding("jwtSecretKey", secretKey)
				.binding("jwtIssuer", issuer)
				.binding("tokenType", sessionToken.get("tokenType") )
				.message(message)
				.run().toString();
		
		assert( flowVars.get("validToken").equals("true") );
		assert( flowVars.get("myToyotaId").equals(myToyotaId) );
		assert( flowVars.get("UID").equals(UID) );
	}
}
