import groovy.json.JsonSlurper
import static groovy.json.JsonOutput.*
import java.text.DateFormat
import java.text.SimpleDateFormat

def manualsResponse
if (payload != org.mule.transport.NullPayload) {
	manualsResponse = new JsonSlurper().parseText(payload)
} else {
	return ""
}


def response = [:]
def manualsList = []

manualsResponse?.each { manual ->
	def map = [:]
	map['type'] = manual?.documentType
	map['title'] = manual?.title
	map['link'] = manualsHost + basePath + manual?.prettyName
	manualsList.add(map)
}


// If there was a manual found, add the vehicle description and production date
if(manualsList.size() > 0) {
    response['vehicleDescription'] = sessionVars['vehicleDescription']
    response['production'] = sessionVars['production']
    response['manuals'] = manualsList
} else {
	throw new org.mule.module.apikit.exception.NotFoundException("No vehicle manuals found")
}

return prettyPrint(toJson(response))

