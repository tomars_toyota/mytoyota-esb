def removeNulls(col) {
    if (col instanceof List)
        return col.collect { removeNulls(it) }.findAll { it != null && it != '' && it != [:] && it != []}
    else if (col instanceof Map) 
        return col.collectEntries { [(it.key): removeNulls(it.value) ] }.findAll { it.value != null && it.value != '' && it.value != [] && it.value != [:]}
    else return col
}
