import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper
import au.com.toyota.esb.mytoyota.api.util.GroovyHelper
import java.text.*
import java.util.List

def isValidDate(String dateString) {
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
    try {
        df.parse(dateString)
        return true
    } catch (ParseException e) {
        return false
    }
}

def isExpired(String dateString) {
	Date now = new Date()
	if(isValidDate(dateString)) {
		Date parsedDate= Date.parse("yyyy-MM-dd'T'HH:mm:ss'Z'", dateString)
		if(parsedDate >= now) return false 
		else return true 
	} 
	else {
		return false
	}
	

}
def isRewardApplicable(Boolean isTMCA, List userTypeList) {
	if (!userTypeList) {
		userTypeList = []
	}

	isApplicable = false;

	if (isTMCA) {
		isApplicable = userTypeList.contains(userTypeTMCA)
	}
	else {
		isApplicable = userTypeList.contains(userTypeGuest)
	}
	return isApplicable

}


scRewards = []
if(flowVars['scRewards'])
	scRewards = new JsonSlurper().parseText(flowVars['scRewards'])?.rewards

claimedRewards = []
if(payload != null && !(payload instanceof org.mule.transport.NullPayload)) 
	claimedRewards = new JsonSlurper().parseText(payload)

isTMCA = Boolean.valueOf(flowVars['isTMCA'])
addVehicleRequired = Boolean.valueOf(flowVars['addVehicleRequired'])

rewardsList = []
scRewards.each { item ->
	item.isClaimed = item.id in claimedRewards.rewardId
	if(item?.cardFront?.cardImage)
		item.cardFront.cardImage='https://'+ sitecoreHost + item.cardFront.cardImage
	item.remove('isSuccessful')
	item.remove('message')

	// check if reward is applicable
	if (isRewardApplicable(isTMCA, item.userTypes)) {
		if (!isExpired(item.expiryDate)) {
			if(!isValidDate(item.expiryDate)) item.expiryDate = null
			rewardsList.add(item)
		}	
	}

	if (!item?.singleClaimOnly) {
		item.noOfRemainingClaims = 1
	}

	item.remove('userTypes')
	
}

response = ["rewards":GroovyHelper.removeNulls(rewardsList, true), "addVehicleRequired": addVehicleRequired]
return prettyPrint(toJson(response))