import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper
import java.text.SimpleDateFormat
import groovy.time.TimeCategory

def vehiclesListResponse = new JsonSlurper().parseText(payload)

def highestDate = Date.parse("yyyy-MM-dd", "1900-00-00")
def highestDateFound = false
def response = [:]

def setHighestDate = { d ->
    if(d > highestDate) {
        highestDate = d
        // set boolean to indicate at least one vehicle had a date
        highestDateFound = true
    }
}

// Build vehicle list
vehiclesListResponse['vehicles']?.each{ v -> 
    if(v?.deliveryDate) {
        def d = Date.parse("yyyy-MM-dd", v?.deliveryDate)
        setHighestDate(d)
    }
}


// If no vehicles found, don't return the 'subscriptions' map
// If vehicles found, add three years to most recent RDR date
// - if this date is in the past, return expired status
// - else return valid status
use(TimeCategory) {
    if (highestDateFound) {
        
        def goPlacesMap = [:]

        def today = new Date()
        def expiryDate = highestDate + 3.years
      
        goPlacesMap['status'] = (today <= expiryDate) ? "valid" : "expired"
        goPlacesMap['expiry'] = expiryDate.format("yyyy-MM-dd")
        goPlacesMap['type'] = "complimentary"

        response['goPlaces'] = goPlacesMap    
    }
}
//println ('_________________________'+toJson(response))
return response
