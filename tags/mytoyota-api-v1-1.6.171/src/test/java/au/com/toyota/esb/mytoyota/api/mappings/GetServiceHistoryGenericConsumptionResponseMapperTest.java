package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;

import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

public class GetServiceHistoryGenericConsumptionResponseMapperTest extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;

	public GetServiceHistoryGenericConsumptionResponseMapperTest() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/get-vehicle-service-history-genericconsumption-response-mapper.groovy");
	}

	@Test
	public void testSuccessfulRequestMapper() throws Exception {
		
		String result = ScriptRunBuilder.runner(scriptRunner)
				.payload(IOUtils
						.toString(getClass().getResourceAsStream("/in/get-vehicles-vin-service-history-genericConsumptionApi-response.json"), "UTF-8"))
				.binding("queryStartDate", null)
				.binding("queryEndDate", null)
				.binding("vin", "JTDBR23E803142287")
				.run().toString();

		System.out.println(result);
		
		// expected
		String expectedResult = IOUtils
				.toString(getClass().getResourceAsStream("/out/get-vehicles-vin-serviceHistory-response.json"), "UTF-8");
		
		System.out.println(expectedResult);
		
		JSONAssert.assertEquals(expectedResult, result, true);
	}
	
	@Test
	public void testSuccessfulRequestMapperWithEndDate() throws Exception {
		
		String result = ScriptRunBuilder.runner(scriptRunner)
				.payload(IOUtils
						.toString(getClass().getResourceAsStream("/in/get-vehicles-vin-service-history-genericConsumptionApi-response.json"), "UTF-8"))
				.binding("queryStartDate", null)
				.binding("queryEndDate", "2019-04-01")
				.binding("vin", "JTDBR23E803142287")
				.run().toString();

		System.out.println(result);
		
		// expected
		String expectedResult = IOUtils
				.toString(getClass().getResourceAsStream("/out/get-vehicles-vin-serviceHistory-endDate-response.json"), "UTF-8");
		
		System.out.println(expectedResult);
		
		JSONAssert.assertEquals(expectedResult, result, true);
	}
	
	@Test
	public void testSuccessfulRequestMapperWithBlankPayload() throws Exception {
		
		String result = ScriptRunBuilder.runner(scriptRunner)
				.payload("[]")
				.binding("queryStartDate", null)
				.binding("queryEndDate", "2019-04-01")
				.binding("vin", "JTDBR23E803142287")
				.run().toString();

		System.out.println(result);
		
		// expected
		String expectedResult = "{\n" + 
				"    \"vin\": \"JTDBR23E803142287\",\n" + 
				"    \"services\": []\n" + 
				"}";
		
		System.out.println(expectedResult);
		
		JSONAssert.assertEquals(expectedResult, result, true);
	}
}
