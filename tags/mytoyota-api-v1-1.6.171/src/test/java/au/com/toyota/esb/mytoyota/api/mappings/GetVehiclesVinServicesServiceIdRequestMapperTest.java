package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

import javax.script.*;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

public class GetVehiclesVinServicesServiceIdRequestMapperTest extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;

	public GetVehiclesVinServicesServiceIdRequestMapperTest() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner
				.createScriptRunner("/mappings/get-vehicles-vin-services-serviceId-response-mapper.groovy");
	}
	
	@Test
	public void testGetServiceSuccessful() throws Exception {
		String expectedResult = IOUtils.toString(
				getClass().getResourceAsStream("/out/get-vehicles-vin-services-serviceId-response.json"));
		String payload = IOUtils
				.toString(getClass().getResourceAsStream("/in/get-vehicles-vin-services-serviceId-myKit-response.json"));
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.flowVar("myServiceBookingResponse", null)
				.flowVar("myKitResponse", payload)
				.payload(payload)
				.run().toString();

		System.out.println(result);
		JSONAssert.assertEquals(expectedResult, result, true);
	}
	
	@Test
	public void testGetUsersMeServicesSuccessful() throws Exception {
		String expectedResult = IOUtils.toString(
				getClass().getResourceAsStream("/out/get-users-me-services-response.json"));
		String payload = IOUtils
				.toString(getClass().getResourceAsStream("/in/get-users-me-vehicles-vin-services-myKit-response.json"));
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(payload)
				.flowVar("myServiceBookingResponse", null)
				.flowVar("myKitResponse", payload)
				.run().toString();

		System.out.println(result);
		JSONAssert.assertEquals(expectedResult, result, true);
	}
}
