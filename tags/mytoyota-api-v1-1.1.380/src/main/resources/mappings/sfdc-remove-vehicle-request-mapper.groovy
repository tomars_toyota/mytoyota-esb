import groovy.xml.MarkupBuilder

def writer = new StringWriter()
def xmlMarkup = new MarkupBuilder(writer)

xmlMarkup.mkp.xmlDeclaration(version: "1.0", encoding: "UTF-8")
xmlMarkup.'sp:removeAVehicle' (
	'xmlns:sp' : "http://soap.sforce.com/schemas/class/SP_UserRegWSTransactionStructure"
	) {
		'sp:request'{
			'sp:myToyotaId' (flowVars['myToyotaId'])
			'sp:VIN' (flowVars['vin'])
		}	
	}

//println '__________ payload to salesforce: ' + writer.toString()
return writer.toString()



// <sp:request>
// 	<sp:myToyotaId>?</sp:myToyotaId>
// 	<sp:VIN>?</sp:VIN>
// </sp:request>