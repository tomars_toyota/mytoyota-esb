package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

import javax.script.*;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.codehaus.jackson.map.ObjectMapper;
import org.xml.sax.SAXException;
import static org.junit.Assert.assertTrue;

public class GetSubscriptionStatusResponseMapperTest extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;

	public GetSubscriptionStatusResponseMapperTest() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/get-users-me-subscription-status-response-mapper.groovy");
	}

	//@Test
	public void testMapperValidSubscriptionStatus() throws Exception {
		// This mapper returns a map, rather than a JSON string. Therefore convert it to a JSON string before the assert()
		Object result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(getClass().getResourceAsStream("/in/get-users-me-subscription-status-mykit-5-vehicles-response.json")))
				.run();
		String resultJson = new ObjectMapper().writeValueAsString(result);
		System.out.println(resultJson);

		// expected
		String expectedResult = IOUtils.toString(getClass()
				.getResourceAsStream(
						"/out/get-users-me-subscription-status-valid-response.json"));
		JSONAssert.assertEquals(expectedResult, resultJson, true);
	}
	
	//@Test
	public void testMapperExpiredSubscriptionStatus() throws Exception {
		// This mapper returns a map, rather than a JSON string. Therefore convert it to a JSON string before the assert()
		Object result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(getClass().getResourceAsStream("/in/get-users-me-subscription-status-mykit-5-vehicles-expired-response.json")))
				.run();
		String resultJson = new ObjectMapper().writeValueAsString(result);
		System.out.println(resultJson);

		// expected
		String expectedResult = IOUtils.toString(getClass()
				.getResourceAsStream(
						"/out/get-users-me-subscription-status-expired-response.json"));
		JSONAssert.assertEquals(expectedResult, resultJson, true);
	}
	
	//@Test
	public void testMapperZeroVehiclesReturned() throws Exception {
		// This mapper returns a map, rather than a JSON string. Therefore convert it to a JSON string before the assert()
		Object result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(getClass().getResourceAsStream("/in/get-users-me-subscription-status-mykit-0-vehicles-response.json")))
				.run();
		String resultJson = new ObjectMapper().writeValueAsString(result);
		System.out.println(resultJson);

		// expected
		String expectedResult = "{}";
		JSONAssert.assertEquals(expectedResult, resultJson, true);
	}
}
