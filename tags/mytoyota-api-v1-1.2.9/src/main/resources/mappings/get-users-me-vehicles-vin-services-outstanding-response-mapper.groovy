import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper
import au.com.toyota.esb.mytoyota.api.util.GroovyHelper


def response = [:]
response.vehicle = [:]
response.upsells = []
response.serviceOperations = []

JsonSlurper js = new JsonSlurper()

def getVehicleResponse = payload
def responseVehicle = null
if(getVehicleResponse?.vehicleData?.vehicle)
	responseVehicle = getVehicleResponse?.vehicleData?.vehicle[0]
def upsellsList = getVehicleResponse?.vehicleData?.upsells
def serviceOperationList = getVehicleResponse?.vehicleData?.serviceOperations

response.vehicle.registrationNumber = responseVehicle?.regid
response.vehicle.dmsVehicleId = responseVehicle?.vehid

upsellsList.each { Map upsellItem ->
	def upsell = [:]
	upsell.description = upsellItem.opdsc
	upsell.price = upsellItem.stdchg
	upsell.id = upsellItem.x_rowid
	response.upsells << upsell
}

serviceOperationList.each { Map serviceOp ->
	def serviceOperation = [:]
	serviceOperation.operationTypeId = serviceOp.optid
	serviceOperation.operationId = serviceOp.opid
	serviceOperation.description = serviceOp.opdsc
	serviceOperation.tsaFlag = serviceOp.tsaqua
	serviceOperation.price = serviceOp.fixchg
	serviceOperation.externalClaimNumber = serviceOp.waropr
	serviceOperation.serviceCode = serviceOp.warscd
	serviceOperation.fixedChargeOperationFlag = serviceOp.fixflg
	serviceOperation.cappedPriceOperationFlag = serviceOp.capflg
	response.serviceOperations << serviceOperation
}


response = GroovyHelper.removeNulls(response)

return prettyPrint(toJson(response))