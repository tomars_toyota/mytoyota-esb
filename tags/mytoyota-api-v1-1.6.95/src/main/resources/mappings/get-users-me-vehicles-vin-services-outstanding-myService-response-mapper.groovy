import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper
import au.com.toyota.esb.mytoyota.api.util.GroovyHelper
import au.com.toyota.esb.mytoyota.api.util.ServiceDescriptionHelper

def parseNumber(numberString) {
	if (numberString == null) {
		return null
	}
	try {
		return new BigDecimal(numberString)
	}
	catch (java.lang.NumberFormatException e) {
		return null
	}

}

def response = [:]
response.vehicle = [:]
response.upsells = []
response.serviceOperations = []

JsonSlurper js = new JsonSlurper()

def getVehicleResponse = js.parseText(payload)
def upsellsList = []
def serviceOperationList = getVehicleResponse?.service

response.vehicle.registrationNumber = null
response.vehicle.dmsVehicleId = null

upsellsList.each { Map upsellItem ->
	def upsell = [:]
	upsell.description = upsellItem.opdsc
	upsell.price = upsellItem.stdchg
	upsell.id = upsellItem.x_rowid
	response.upsells << upsell
}

def cviServiceDetails = new JsonSlurper().parseText('[{"section":"General","serviceDescription":[{"action":"Check","description":"for normal operation of engine"},{"action":"Check","description":"for normal operation of drivetrain"},{"action":"Check","description":"for normal operation of exhaust system"},{"action":"Check","description":"for normal operation of brake pipes"},{"action":"Check","description":"for normal operation of hoses"},{"action":"Check","description":"for normal operation of steering"},{"action":"Check","description":"for normal operation of suspension linkages"},{"action":"Check","description":"for normal operation of drive shaft boots and couplings"},{"action":"Check","description":"for normal operation of door lock adjustment"},{"action":"Check","description":"for normal operation of body exterior"},{"action":"Check","description":"for normal operation of park brake adjustment"},{"action":"Inspect","description":"brake pedal free-play"},{"action":"Inspect","description":"brake fluid level"},{"action":"Inspect","description":"engine oil level"},{"action":"Inspect","description":"drive belt tension"},{"action":"Inspect","description":"coolant level and hose clamps"},{"action":"Inspect","description":"condition of fuel lines"}]}]')

serviceOperationList.each { Map serviceOp ->
	def serviceOperation = [:]
	//serviceOperation.operationTypeId = serviceOp.optid
	//serviceOperation.operationId = serviceOp.opid
	def serviceDesc = serviceOp.kilometers + " km or " + serviceOp.name + " Months Service"
	serviceOperation.description = ServiceDescriptionHelper.formatServiceDescription(serviceDesc, disableFormat)
	serviceOperation.tsaFlag = (serviceOp.servicePricingType == "TSA") ? true : false
	serviceOperation.price = parseNumber(serviceOp.price)
	serviceOperation.displayPrice = serviceOp.price
	serviceOperation.externalClaimNumber = serviceOp.code
	serviceOperation.serviceCode = serviceOp.serviceCode
	//serviceOperation.fixedChargeOperationFlag = serviceOp.fixflg
	//serviceOperation.cappedPriceOperationFlag = serviceOp.capflg
	serviceOperation.isComplimentary = (serviceOp.serviceType == "OSB_SERVICE_TYPE_CI") ? true : false
	serviceOperation.serviceExpiry = serviceOp.serviceExpiryDate

	// osb extra fields
	if (serviceOperation.isComplimentary) {
		serviceOperation.serviceDetails = cviServiceDetails
		serviceOperation.description = serviceOp.name
	}
	else {
		serviceOperation.serviceDetails = serviceOp.serviceDetails
	}
		
	serviceOperation.servicePricingTypeDesc = serviceOp.servicePricingTypeDesc
	serviceOperation.recommended = serviceOp.recommended
	serviceOperation.duration = serviceOp.duration
	serviceOperation.serviceDueDate = serviceOp.serviceDueDate
	serviceOperation.servicePricingType = serviceOp.servicePricingType
	serviceOperation.poaFlag = serviceOp.poaFlag
	serviceOperation.dealerPricingApplied = serviceOp.dealerPricingApplied
	serviceOperation.serviceType = serviceOp.serviceType
	serviceOperation.servicePricingTypeDesc = serviceOp.servicePricingTypeDesc
	serviceOperation.months = serviceOp.name
	serviceOperation.kilometers = serviceOp.kilometers
	response.serviceOperations << serviceOperation
}


response = GroovyHelper.removeNulls(response)

return prettyPrint(toJson(response))