import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import java.text.SimpleDateFormat


JsonSlurper js = new JsonSlurper()
//println '___________ apiRequest: '+sessionVars['apiRequest']
def apiRequest = js.parseText(sessionVars['apiRequest'])

//remove nulls
apiRequest = apiRequest.findAll {it.value != null && it.value != ""}


def request = [:]

request.myToyotaId = flowVars['myToyotaId']

if(apiRequest.contactDetails) {
	request.firstName = apiRequest.contactDetails['firstName']
	request.lastName = apiRequest.contactDetails['lastName']
	request.email = apiRequest.contactDetails['email']
	request.mobilePhone = apiRequest.contactDetails['phone']
}

request.bookingDate = new SimpleDateFormat("dd/MM/yyyy").format(new Date())
request.rego = apiRequest['registrationNumber']
request.vin = flowVars['vin']
request.bookingNo = flowVars['dmsBookingId']
request.bookingQuote  = apiRequest['serviceOperationPrice']
request.bookingTypeOfService = apiRequest['description']
if(apiRequest['dropOffDate'])
	request.dropOffDate = new SimpleDateFormat("dd/MM/yyyy").format(new Date().parse('yyyy-MM-dd', apiRequest['dropOffDate']))
if(apiRequest['dropOffTime'])
	request.dropOffTime = new SimpleDateFormat("hh:mm a").format(new Date().parse('HH:mm', apiRequest['dropOffTime']))
if(apiRequest['pickUpDate'])
	request.pickUpDate = new SimpleDateFormat("dd/MM/yyyy").format(new Date().parse('yyyy-MM-dd', apiRequest['pickUpDate']))
if(apiRequest['pickUpTime'])
	request.pickUpTime = new SimpleDateFormat("hh:mm a").format(new Date().parse('HH:mm', apiRequest['pickUpTime']))

def dealerSite = js.parseText(sessionVars['dealerResponse']).sites.find { it.sapCode == apiRequest.branchCode }
request.dealerPhone = dealerSite?.phone?.main
request.address = dealerSite?.address
request.webSite = dealerSite?.webSite

//remove nulls
request = request.findAll {it.value != null && it.value != ""}

return new JsonBuilder(request).toString()