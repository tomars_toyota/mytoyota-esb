import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import au.com.toyota.esb.mytoyota.api.util.GroovyHelper

//println '___________ payload: '+payload
def apiRequest = new JsonSlurper().parseText(payload)
if(!apiRequest)
	throw new org.mule.module.apikit.exception.BadRequestException('Invalid request')

//validate request
if(!apiRequest.dealerDetails)
    throw new org.mule.module.apikit.exception.BadRequestException('Invalid request: dealerId, branchCode, dmsId and dmsType are missing')
if(!apiRequest.dealerDetails.containsKey('dealerId'))
	throw new org.mule.module.apikit.exception.BadRequestException('Invalid request: dealerId is missing')
if(!apiRequest.dealerDetails.containsKey('branchCode'))
	throw new org.mule.module.apikit.exception.BadRequestException('Invalid request: branchCode is missing')
if(!apiRequest.dealerDetails.containsKey('dmsId'))
	throw new org.mule.module.apikit.exception.BadRequestException('Invalid request: dmsId is missing')
if(!apiRequest.dealerDetails.containsKey('dmsType'))
	throw new org.mule.module.apikit.exception.BadRequestException('Invalid request: dmsType is missing')

//set flow vars to use as query params when calling myKit
flowVars['dmsId'] = apiRequest.dealerDetails.dmsId
flowVars['dmsType'] = apiRequest.dealerDetails.dmsType

def request = [:]

request.bookingId = flowVars['serviceId']
request.dropOffDate = apiRequest.dropOffDate
request.dropOffTime = apiRequest.dropOffTime
request.pickUpDate = apiRequest.pickUpDate
request.pickUpTime = apiRequest.pickUpTime 
request.status = apiRequest.status
request.notes = apiRequest.notes
request.alertType = apiRequest.confirmationType
request.totalServicePrice = apiRequest.totalServicePrice
request.waitAtDealerFlag = apiRequest.waitAtDealerFlag?:false
request.expressServiceFlag = apiRequest.expressServiceFlag?:false
request.courtesyBusFlag = apiRequest.courtesyBusFlag?:false
request.loanVehicleFlag = apiRequest.loanVehicleFlag?:false

request.dealer = apiRequest.dealerDetails

//replace dmsId and branchCode with dmsID and branchId
request.dealer.dmsID = apiRequest.dealerDetails.dmsId
request.dealer.branchId = apiRequest.dealerDetails.branchCode
request.dealer.remove('dmsId')
request.dealer.remove('branchCode')


request.contactDetail = apiRequest.contactDetail
request.vehicle = apiRequest.vehicle
request.serviceOperation = apiRequest.serviceOperation
request.odometer = apiRequest.vehicleOdo

request = GroovyHelper.removeNulls(request)

return new JsonBuilder(request).toString()
