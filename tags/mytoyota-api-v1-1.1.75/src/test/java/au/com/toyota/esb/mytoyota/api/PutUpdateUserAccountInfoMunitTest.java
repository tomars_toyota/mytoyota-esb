package au.com.toyota.esb.mytoyota.api;

import static org.junit.Assert.assertTrue;
import static org.mule.munit.common.mocking.Attribute.attribute;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.custommonkey.xmlunit.Diff;
import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleException;
import org.mule.munit.common.mocking.SpyProcess;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.skyscreamer.jsonassert.JSONAssert;

import au.com.toyota.esb.mytoyota.api.munit.SimplifiedMockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;


public class PutUpdateUserAccountInfoMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"put-update-user-account-info.xml",
				"put-update-user-account-info-technical-adapter.xml",
				"create-salesforce-sessionid-soap-header.xml",
				"sfdc-update-user.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulRequest() throws Exception {
		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(
						attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Operation updateOwner"))
				.thenReturn(muleMessageWithPayload(""));
		testSuccessfulRequestImpl();
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.sfdc-update-user.response"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.sfdc-update-user.error")).times(0);
	}
	
	@Test
	public void testSuccessfulRequestFailSfdc() throws Exception {
		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(
						attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Operation updateOwner"))
				.thenThrow(new Exception("SFDC Failure"));
		
		testSuccessfulRequestImpl();
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.sfdc-update-user.response"))
				.times(0);
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.sfdc-update-user.error")).times(1);
	}
	

	private void testSuccessfulRequestImpl() throws Exception {
		
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("myKIT API POST Save Account"))
				.thenApply(new SimplifiedMockResponseTransformer(200, ""));
		
		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Operation login"))
				.thenReturn(muleMessageWithPayload(
						IOUtils.toString(getClass().getResourceAsStream("/in/sfdc-login-response.xml"))));
		
		
		String expectedResult = "{\"success\":true}";
		String requestPath = "/users/me";
		String apiRequest = IOUtils.toString(getClass().getResourceAsStream("/in/put-update-user-account-info-request.json"));
		
		MuleEvent event = testEvent(requestPath);
		event.getMessage().setPayload(apiRequest);
		event.setFlowVariable("myToyotaId", "MYT-0000058b");

		MuleEvent output = runFlow("put:/users/me:myToyota-config", event);

		System.out.println(output.getMessage().getPayload());

		JSONAssert.assertEquals(expectedResult, (String)output.getMessage().getPayload(), true);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.post-update-user-account-info.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.post-update-user-account-info.response"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("category")
				.withValue("esb.mytoyota-api-v1.update-user-account-info-technical-adapter.request")).times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("category")
				.withValue("esb.mytoyota-api-v1.update-user-account-info-technical-adapter.response")).times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category")
						.withValue("esb.mytoyota-api-v1.update-user-account-info-technical-adapter-mykit.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category")
						.withValue("esb.mytoyota-api-v1.update-user-account-info-technical-adapter-mykit.response"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.sfdc-update-user.request"))
				.times(1);
		
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.get-salesforce-sessionid-subflow.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-salesforce-sessionid-subflow.response"))
				.times(1);
	}

	@Test
	public void testFailuresStatusCodes() throws Exception {

		int[] statusCodes = new int[] { 404, 405, 415, 406, 401, 500 };

		String requestPath = "/users/me";
		String apiRequest = IOUtils.toString(getClass().getResourceAsStream("/in/put-update-user-account-info-request.json"));
		
		for (int i = 0; i < statusCodes.length; i++) {
			MuleEvent event = testEvent(requestPath);
			event.getMessage().setPayload(apiRequest);
			event.setFlowVariable("myToyotaId", "myToyotaId");
			new StatusCodesTestingHelper().runFailureTest(statusCodes[i], "myKIT API POST Save Account",
					"put:/users/me:myToyota-config", event);
		}
	}
}
