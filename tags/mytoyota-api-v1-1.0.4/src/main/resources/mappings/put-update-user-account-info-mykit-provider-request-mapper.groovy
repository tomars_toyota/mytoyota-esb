import groovy.json.JsonSlurper
import groovy.json.JsonBuilder

def user = new JsonSlurper().parseText(payload)
def result = [:]

if (user?.login)
	result.login = user.login

if (user?.title)
	result.title = user.title

if (user?.firstName)
	result.firstName = user.firstName

if (user?.lastName)
	result.lastName = user.lastName

if (user?.email)
	result.email = user.email

if (user?.mobile)
	result.mobile = user.mobile

if (user?.address?.postal?.fullAddress)
	result.fullAddress = user.address.postal.fullAddress

if (user?.address?.postal?.streetAddress)
	result.streetAddress = user.address.postal.streetAddress

if (user?.address?.postal?.postcode)
	result.postcode = user.address.postal.postcode

if (user?.address?.postal?.suburb)
	result.suburb = user.address.postal.suburb

if (user?.address?.postal?.state)
	result.state = user.address.postal.state

if (user?.dateOfBirth)
	result.dateOfBirth = user.dateOfBirth

if (user?.landline)
	result.landline = user.landline

if (user?.licenceId)
	result.licenceId = user.licenceId

if (user?.licenceState)
	result.licenceState = user.licenceState

if (user?.licenceCountry)
	result.licenceCountry = user.licenceCountry

if (user?.licenceExpiry)
	result.licenceExpiry = user.licenceExpiry

return new JsonBuilder(result).toString()