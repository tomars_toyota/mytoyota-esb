def requestPayload = flowVars['requestPayload']

def request = [:]

// mapping myKIT login field as null as it's currently still required in their API
request.login = null
request.password = requestPayload?.password ?: null
request.firstName = requestPayload?.firstName ?: null
request.lastName = requestPayload?.lastName ?: null 
request.email = requestPayload?.email ?: null
request.mobile = requestPayload?.mobile ?: null

return request

/*
	"title": "",
	"langKey": "",
	"fullAddress": "",
	"addressReference": "",
	"streetAddress": "",
	"postcode": "",
	"suburb": "",
	"state": "",
	"address": {
	  "createdBy": "",
	  "createdDate": "",
	  "lastModifiedBy": "",
	  "lastModifiedDate": "",
	  "id": 0,
	  "referenceId": "",
	  "formattedAddress": "",
	  "primaryName": "",
	  "secondaryName": "",
	  "unit": "",
	  "streetNo": "",
	  "streetAddress": "",
	  "streetName": "",
	  "streetType": "",
	  "suburb": "",
	  "state": "",
	  "postcode": "",
	  "country": "",
	  "geoLat": "",
	  "geoLon": "",
	  "geoLatStreet": "",
	  "getLonStreet": "",
	  "listing": false,
	  "postal": false,
	  "dpid": "",
	  "user": {
		"createdBy": "",
		"createdDate": "",
		"lastModifiedBy": "",
		"lastModifiedDate": "",
		"id": 0,
		"login": "",
		"title": "",
		"firstName": "",
		"lastName": "",
		"email": "",
		"mobile": "",
		"activated": false,
		"langKey": "",
		"activationKey": "",
		"activationKeyDate": "",
		"loginFailedCount": 0,
		"loginDisabledDate": "",
		"verificationKeyDate": "",
		"verificationKey": "",
		"verified": false,
		"acceptedTerms": ""
	  }
	},
	"acceptedTerms": false,
	"roles": [
	  ""
	]
  }
*/