package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

import javax.script.*;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

public class GetUsersDealersResponseMapper extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;

	public GetUsersDealersResponseMapper() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner
				.createScriptRunner("/mappings/get-user-dealers-response-mapper.groovy");
	}

	@Test
	public void testDealersResponseMapper() throws Exception {
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(getClass().getResourceAsStream("/in/get-users-dealers-mytoyotaapi-response.json")))
				.sessionVar("showDeletedOnly", false)
				.run().toString();

		System.out.println(result);

		// expected
		String expectedResult = IOUtils.toString(getClass()
				.getResourceAsStream(
						"/out/get-users-dealers-api-response.json"));
		JSONAssert.assertEquals(expectedResult, result, true);
	}
	
	@Test
	public void testDealersWithNullsResponseMapper() throws Exception {
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(getClass().getResourceAsStream("/in/get-users-dealers-with-nulls-mytoyotaapi-response.json")))
				.sessionVar("showDeletedOnly", false)
				.run().toString();

		System.out.println(result);

		// expected
		String expectedResult = IOUtils.toString(getClass()
				.getResourceAsStream(
						"/out/get-users-dealers-with-nulls-api-response.json"));
		JSONAssert.assertEquals(expectedResult, result, true);
	}
	
	@Test
	public void testDeletedDealersResponseMapper() throws Exception {
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(getClass().getResourceAsStream("/in/get-users-deleted-dealers-mytoyotaapi-response.json")))
				.sessionVar("showDeletedOnly", true)
				.run().toString();

		System.out.println(result);

		// expected
		String expectedResult = IOUtils.toString(getClass()
				.getResourceAsStream(
						"/out/get-users-deleted-dealers-api-response.json"));
		JSONAssert.assertEquals(expectedResult, result, true);
	}
}
