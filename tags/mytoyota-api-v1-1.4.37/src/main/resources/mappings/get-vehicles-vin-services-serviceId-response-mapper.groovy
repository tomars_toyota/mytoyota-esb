import groovy.json.JsonSlurper
import static groovy.json.JsonOutput.*
import au.com.toyota.esb.mytoyota.api.util.GroovyHelper


def mapService(service) {
	def mappedService = [:]
	mappedService.bookingId = service.bookingId
	mappedService.dropOffDate = service.dropOffDate
	mappedService.dropOffTime = service.dropOffTime
	mappedService.pickUpDate = service.pickUpDate
	mappedService.pickUpTime = service.pickUpTime 
	mappedService.status = service.status
	mappedService.notes = service.notes
	mappedService.alertType = service.confirmationType
	mappedService.totalServicePrice = service.totalServicePrice
	mappedService.waitAtDealerFlag = service.waitAtDealerFlag
	mappedService.expressServiceFlag = service.expressServiceFlag
	mappedService.courtesyBusFlag = service.courtesyBusFlag
	mappedService.loanVehicleFlag = service.loanVehicleFlag

	mappedService.dealerDetails = service.dealer

	//replace dmsId and branchCode with dmsID and branchId
	mappedService.dealerDetails.dmsId = service.dealer?.dmsID
	mappedService.dealerDetails.branchCode = service.dealer?.branchId
	mappedService.dealerDetails.remove('dmsID')
	mappedService.dealerDetails.remove('branchId')

	mappedService.contactDetail = service.contactDetail
	mappedService.vehicle = service.vehicle
	mappedService.serviceOperation = service.serviceOperation
	mappedService.vehicleOdo = service.odometer

	return mappedService
}

def mapMyServiceBookings(service) {
	def mappedService = [:]
	mappedService.bookingId = service.bookingRequestId
	mappedService.dropOffDate = service.appointment?.dropOffDate
	mappedService.dropOffTime = service.appointment?.dropOffTime
	mappedService.pickUpDate = service.appointment?.dropOffDate // myService don't have a pickUpDate
	mappedService.pickUpTime = service.appointment?.pickUpTime 
	mappedService.status = service.status
	mappedService.notes = service.vehicleConcernsComment
	mappedService.alertType = service.preferredContactMethod
	mappedService.totalServicePrice = (!service.repairOrder?.totalPrice) ? null : new BigDecimal(service.repairOrder?.totalPrice)
	mappedService.waitAtDealerFlag = (service.transportOption == "OSB_TRANSPORT_OPTION_WAIT") ? true : false
	mappedService.expressServiceFlag = (service.expressMaintenance == "true") ? true : false
	mappedService.courtesyBusFlag = (service.transportOption == "OSB_TRANSPORT_OPTION_BUS") ? true : false
	mappedService.loanVehicleFlag = (service.transportOption == "OSB_TRANSPORT_OPTION_CAR") ? true : false
	mappedService.transportOption = service.transportOption
	mappedService.transportOptionDesc = service.transportOptionDesc

	mappedService.dealerDetails = service.dealer
	mappedService.dealerDetails.dealerId = service.dealer.dealerCode
	mappedService.dealerDetails.branchCode = service.dealer.locationCode
	//replace dmsId and branchCode with dmsID and branchId
	mappedService.dealerDetails.dmsId = service.dealer.serviceSiteId
	mappedService.dealerDetails.remove("serviceSiteId")
	mappedService.dealerDetails.remove("locationCode")

	mappedService.contactDetail = service.contact
	mappedService.contactDetail.phone = service.contact.preferredContactNo

	mappedService.vehicle = service.vehicle

	if (service.repairOrder?.services != null && service.repairOrder?.services.size() > 0) {
		println "service.services.size(): " + service.repairOrder?.services.size()
		def serviceOp = service.repairOrder?.services.first()
		mappedService.serviceOperation = serviceOp
		mappedService.serviceOperation.description = serviceOp.serviceName
		mappedService.serviceOperation.externalClaimNumber = serviceOp.sapOperationCode
		mappedService.serviceOperation.price = (!serviceOp.price) ? null : new BigDecimal(serviceOp.price)
		mappedService.serviceOperation.totalOperationTime = (!serviceOp.duration) ? null : new BigDecimal(serviceOp.duration)
		mappedService.serviceOperation.tsaFlag = (serviceOp.servicePricingType == "TSA") ? true : false
		mappedService.serviceOperation.isComplimentary = (serviceOp.serviceType == "OSB_SERVICE_TYPE_CI") ? true : false
		mappedService.serviceOperation.procedureDescription = serviceOp.procedureDescription
		mappedService.serviceOperation.serviceDetails = serviceOp.serviceDetails

	}
	else {
		println "service.services: " + service.services
	}
	 
	mappedService.vehicleOdo = service.vehicle?.odometer

	return mappedService
	
	
		
}

def myServiceResponse = null 
if (flowVars.myServiceBookingResponse) {
	myServiceResponse = new JsonSlurper().parseText(flowVars.myServiceBookingResponse)
}

def response = [:]

servicesList = []

def myKitResponse = null

if (flowVars.myKitResponse) {
	//println '___________ payload: '+payload
	myKitResponse = new JsonSlurper().parseText(flowVars.myKitResponse)
}
else {
	if (payload) {
		myKitResponse = new JsonSlurper().parseText(payload)
	}	
}

if(myKitResponse instanceof Map) {
	response = mapService(myKitResponse)
} else if(myKitResponse instanceof List) {
	
	

	myKitResponse.each { service ->
		servicesList.add(mapService(service))
	}
	
}


// add the myService Bookings
if(myServiceResponse instanceof List) {
	myServiceResponse.each { serviceItem ->
		servicesList.add(mapMyServiceBookings(serviceItem))
	}
}
response = GroovyHelper.removeNulls(response,true)
response.services = GroovyHelper.removeNulls(servicesList, true)


return prettyPrint(toJson(response))