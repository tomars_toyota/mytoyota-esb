import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper
import au.com.toyota.esb.mytoyota.api.util.GroovyHelper
import java.text.*


serviceHistory = new JsonSlurper().parseText(payload)

serviceHistory.services.removeAll { item ->
	if (item == null) {
		return true
	}
	
	serviceDesc = item.serviceDescription
	
	if (serviceDesc == null) {
		return true;
	}

	// removing this rule as per MYT-3074 [ESB] Vehicle Service History API will be point to a new end point 
	/*if(serviceDesc.length() > 20) {
		return !(serviceDesc.toLowerCase().contains("complimentary") || serviceDesc.substring(0,20).contains("/") || serviceDesc.toLowerCase().contains("km"))
	} 
	else	{  
		return !(serviceDesc.toLowerCase().contains("complimentary"))
	}*/

}

return prettyPrint(toJson(serviceHistory))