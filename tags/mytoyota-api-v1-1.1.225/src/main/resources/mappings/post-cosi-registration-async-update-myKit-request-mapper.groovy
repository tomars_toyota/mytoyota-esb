import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper


//populate flowVars['myToyotaId'] to staisfy update myKit flow
flowVars['myToyotaId'] = sessionVars['myToyotaId']

order = new JsonSlurper().parseText(flowVars['orderResponse'])

myKitRequest = [:]
myKitRequest.title = order.title 
myKitRequest.mobile = order.mobile

//myKitRequest.landline = 								//yet to be added? 

address = order.address?.postal

postalAddress = [:]
postalAddress.streetAddress = address?.streetAddress
postalAddress.suburb = address?.suburb
postalAddress.state = address?.state
postalAddress.postcode = address?.postcode

myKitRequest.address = ['postal': postalAddress]


return prettyPrint(toJson(myKitRequest))