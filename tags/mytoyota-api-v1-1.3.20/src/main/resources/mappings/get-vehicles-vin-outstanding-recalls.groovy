import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper
import org.apache.commons.lang.StringUtils 
import au.com.toyota.esb.mytoyota.api.util.RecallCategoryHelper
import org.slf4j.Logger
import org.slf4j.LoggerFactory
  
Logger logger = LoggerFactory.getLogger("esb.mytoyota-api-v1.get-vehicles-vin-outstanding-recalls-mapping.script");



JsonSlurper js = new JsonSlurper()

def recallsResponse = js.parseText(payload)
response = [:]

def recallType = flowVars['recallType']

response.batchNumber = recallsResponse.batchNumber
response.compliancePlate = recallsResponse.compliancePlate
response.engineNumber = recallsResponse.engineNumber
response.ignitionKeyNumber = recallsResponse.ignitionKeyNumber
response.materialNumber = recallsResponse.materialNumber
response.production = recallsResponse.production
response.vin = recallsResponse.vin
recallsList = []

logger.info("flowVars.includeRecallCategory: " + flowVars.includeRecallCategory)

if(recallsResponse.outstandingRecalls) {

	recallsResponse.outstandingRecalls.groupBy({it.campaignCode}).each{
		addRecall = true
		if(recallType != null && !recallType.equalsIgnoreCase(it.value[0]['recallType']) ) addRecall = false
	    campaignItem = [:]
	    
	    campaignItem['lastUpdateTimstamp'] = it.value[0]['lastUpdateTimstamp']
	    campaignItem['campaignCode'] = it.key
	    campaignItem['campaignDescription'] = it.value[0]['campaignDescription']
	    
	    campaignItem['campaignItems'] = it.value
	    campaignItem['campaignItems'] = campaignItem['campaignItems'].each{
	        it.remove('lastUpdateTimstamp')
	        it.remove('campaignCode')
	        it.remove('campaignDescription')
	        it.remove('recallType')
	    }
	    
	    if (flowVars.includeRecallContent == true) {
	    	//def campaignCode = campaignItem.campaignCode
			//campaignItem.recallCategory = RecallCategoryHelper.identifyRecallCategory(campaignCode, takataUrgentCampaignCodes, takataNonUrgentCampaignCodes)
			def recallContent = sessionVars.campaignCodeContentMap.get(campaignItem.campaignCode)
			if (recallContent) 
				campaignItem << js.parseText(recallContent)
		}
	    
	    if(addRecall) recallsList.add(campaignItem)
	}
}

response.outstandingRecalls = recallsList



return prettyPrint(toJson(response))
