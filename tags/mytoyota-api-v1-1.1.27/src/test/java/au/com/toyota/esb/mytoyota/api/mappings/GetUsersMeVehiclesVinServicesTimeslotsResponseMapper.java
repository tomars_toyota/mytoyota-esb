package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

import javax.script.*;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

public class GetUsersMeVehiclesVinServicesTimeslotsResponseMapper extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;

	public GetUsersMeVehiclesVinServicesTimeslotsResponseMapper() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner
				.createScriptRunner("/mappings/get-users-me-vehicles-vin-services-timeslots-response-mapper.groovy");
	}

	@Test
	public void testResponseMapper() throws Exception {
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(getClass().getResourceAsStream("/in/get-users-me-vehicles-vin-services-timeslots-tune-response.json")))
				.run().toString();

		System.out.println(result);

		// expected
		String expectedResult = IOUtils.toString(getClass()
				.getResourceAsStream(
						"/out/get-users-me-vehicles-vin-services-timeslots-api-response.json"));
		JSONAssert.assertEquals(expectedResult, result, true);
	}
}
