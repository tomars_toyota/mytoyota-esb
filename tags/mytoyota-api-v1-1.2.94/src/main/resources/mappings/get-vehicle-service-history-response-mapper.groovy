import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper
import au.com.toyota.esb.mytoyota.api.util.GroovyHelper
import java.text.*


serviceHistory = new JsonSlurper().parseText(payload)

serviceHistory.services.removeAll { item ->
	serviceDesc = item.serviceDescription
	cutOffLength = serviceDesc.length() >= 20 ? 20 : serviceDesc.length()
	serviceDesc.toLowerCase().contains("complimentary") || serviceDesc.substring(0,cutOffLength).contains("/") 

}

return prettyPrint(toJson(GroovyHelper.removeNulls(serviceHistory, true)))