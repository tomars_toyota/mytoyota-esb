package au.com.toyota.esb.mytoyota.api.mappings;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

import javax.script.*;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.module.apikit.exception.BadRequestException;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

public class GetVehiclesVinResponseMapper extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;

	public GetVehiclesVinResponseMapper() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/get-vehicles-vin-response-mapper.groovy");
	}

	 @Test
	public void testSuccessfulResponseMapper() throws Exception {
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(getClass().getResourceAsStream("/in/get-vehicles-vin-vehiclesApi-response.json")))
				.run().toString();

		System.out.println(result);

		// expected
		String expectedResult = IOUtils.toString(getClass()
				.getResourceAsStream(
						"/out/get-vehicles-vin-api-response.json"));
		JSONAssert.assertEquals(expectedResult, result, true);
	}
	 
	 @Test
		public void testInvalidToyotaVinResponseMapper() throws Exception {
			try {
				ScriptRunBuilder.runner(scriptRunner)
						.payload(IOUtils.toString(
								getClass().getResourceAsStream("/in/get-vehicles-vin-vehiclesApi-lexus-response.json")))
						.run().toString();
			} catch (Exception e) {

				assertEquals("Not org.mule.module.apikit.exception.BadRequestException was thrown!",
						e.getCause().getCause() instanceof BadRequestException, true);
				return;
			}
			assertTrue(false);
		}
}
