package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;
import java.util.HashMap;

import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;
import org.mule.module.json.JsonData;
import static org.junit.Assert.assertTrue;

public class PostUsersMeVehiclesVinServiceRequestMarketoRequestMapperTest extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;

	public PostUsersMeVehiclesVinServiceRequestMarketoRequestMapperTest() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner
				.createScriptRunner("/mappings/post-users-me-vehicles-vin-services-serviceRequest-marketo-request-mapper.groovy");
	}
	
	@Test
	public void testSuccessful() throws Exception {
		String expectedResult = IOUtils.toString(
				getClass().getResourceAsStream("/out/post-users-me-vehicles-vin-services-serviceRequest-marketo-request.json"));
		String apiRequest = IOUtils
				.toString(getClass().getResourceAsStream("/in/post-users-me-vehicles-vin-services-request.json"));
		@SuppressWarnings("unchecked")
		HashMap<String,Object> serviceRequest =
		        new ObjectMapper().readValue(IOUtils.toString(
						getClass().getResourceAsStream("/out/post-users-me-vehicles-vin-services-serviceRequest-request.json")), HashMap.class);
		@SuppressWarnings("unchecked")
		HashMap<String,Object> dealerResponse =
		        new ObjectMapper().readValue(IOUtils
						.toString(getClass().getResourceAsStream("/in/get-dealers-by-dealer-code-api-response.json")), HashMap.class);
		
		String result = testSuccessfulImpl(expectedResult, apiRequest, dealerResponse, serviceRequest);

		JSONAssert.assertEquals(expectedResult, result, true);
	}
	
	private String testSuccessfulImpl(String expectedResult, String apiRequest, HashMap<String,Object> dealerResponse, HashMap<String,Object> serviceRequest) throws Exception {
		String result = ScriptRunBuilder.runner(scriptRunner)
				.payload("")
				.flowVar("vin", "JTMBFREV80D078597")
				.flowVar("myToyotaId", "MYT-000000c8")
				.sessionVar("apiRequest", apiRequest)
				.sessionVar("dealerResponse", dealerResponse)
				.sessionVar("serviceRequest", serviceRequest)
				.run().toString();

		System.out.println(result);
		
		return result;
	}
}
