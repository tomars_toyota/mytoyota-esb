import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper
import au.com.toyota.esb.mytoyota.api.util.GroovyHelper
import java.text.*
import java.util.List

def isValidDate(String dateString) {
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
    try {
        df.parse(dateString)
        return true
    } catch (Exception e) {
        return false
    }
}

def isExpired(String dateString) {
	Date now = new Date()
	if(isValidDate(dateString)) {
		Date parsedDate= Date.parse("yyyy-MM-dd'T'HH:mm:ss'Z'", dateString)
		if(parsedDate >= now) return false 
		else return true 
	} 
	else {
		return false
	} 
	

}
def isRewardApplicable(Boolean isTMCA, List userTypeList) {
	if (!userTypeList) {
		userTypeList = []
	}

	isApplicable = false;

	if (isTMCA) {
		isApplicable = userTypeList.contains(userTypeTMCA)
	}
	else {
		isApplicable = userTypeList.contains(userTypeGuest)
	}
	return isApplicable

}


scRewards = []
if(flowVars['scRewards'])
	scRewards = new JsonSlurper().parseText(flowVars['scRewards'])?.rewards

claimedRewards = []
if(flowVars.myKitClaimedRewards != null && !(flowVars.myKitClaimedRewards instanceof org.mule.transport.NullPayload)) 
	claimedRewards = new JsonSlurper().parseText(flowVars.myKitClaimedRewards)

isTMCA = Boolean.valueOf(flowVars['isTMCA'])
addVehicleRequired = Boolean.valueOf(flowVars['addVehicleRequired'])

rewardsList = []
scRewards.each { item ->
	item.isClaimed = item.id in claimedRewards.rewardId
	if(item?.cardFront?.cardImage && !item?.cardFront?.cardImage.startsWith("http"))
		item.cardFront.cardImage='https://'+ sitecoreHost + item.cardFront.cardImage

	if (item?.cardFront?.cardImage &&  item?.cardFront?.cardImage.contains("prod-toyota-cd.tmca-digital.com.au:443"))
		item.cardFront.cardImage = item.cardFront.cardImage.replace("prod-toyota-cd.tmca-digital.com.au:443", sitecoreHost)

	item.remove('isSuccessful')
	item.remove('message')

	// check if reward is applicable
	if (isRewardApplicable(isTMCA, item.userTypes)) {
		if (!isExpired(item.expiryDate)) {
			if(!isValidDate(item.expiryDate)) item.expiryDate = null
			rewardsList.add(item)
		}	
	}

	if (item.id == flowVars.caltexRewardId) {
		item.noOfRemainingClaims = flowVars.noOfRemainingClaims
		if (!flowVars.qrEnabled) {
			rewardsList.remove(item)	
		}
		
	}


	if (flowVars.isFeatured) {
		// frontend only wants the featured rewards.

		if (item.featuredCard == null || item.isFeatured != true) {
			rewardsList.remove(item)
		}
		else {
			if(item?.featuredCard?.featuredIcon && !item?.featuredCard?.featuredIcon.startsWith("http"))
				item.featuredCard.featuredIcon = 'https://'+ sitecoreHost + item.featuredCard.featuredIcon

			if (item?.featuredCard?.featuredIcon && item?.featuredCard?.featuredIcon.contains("prod-toyota-cd.tmca-digital.com.au:443"))
				item.featuredCard.featuredIcon = item.featuredCard.featuredIcon.replace("prod-toyota-cd.tmca-digital.com.au:443", sitecoreHost)

		}
	}

	item.remove('userTypes')
	
}

response = ["rewards":GroovyHelper.removeNulls(rewardsList, true), "addVehicleRequired": addVehicleRequired]
return prettyPrint(toJson(response))