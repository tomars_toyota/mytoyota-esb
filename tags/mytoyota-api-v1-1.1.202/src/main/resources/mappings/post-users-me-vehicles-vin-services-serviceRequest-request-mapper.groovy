import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper
import au.com.toyota.esb.mytoyota.api.util.GroovyHelper
import javax.swing.text.MaskFormatter
import java.text.DecimalFormat;

def formatPhone(String phoneNumber) {

	if(!phoneNumber)
		return ''

	phoneMask= "#### ### ###"

	MaskFormatter maskFormatter= new MaskFormatter(phoneMask)
	maskFormatter.setValueContainsLiteralCharacters(false)
	return maskFormatter.valueToString(phoneNumber)
}

DecimalFormat decimalFormat = new DecimalFormat("###,###")


JsonSlurper js = new JsonSlurper()

serviceRequestPayload = js.parseText(sessionVars['apiRequest'])

flowVars['vehicleOdo'] = serviceRequestPayload.vehicleOdo

dealer = js.parseText(payload)

//remove unrelated sites
dealer?.sites?.removeAll { it.sapCode != serviceRequestPayload?.sapCode } 

if(!dealer || !dealer.sites)
	throw new org.mule.module.apikit.exception.NotFoundException("Dealer not found")

mailRequest = [:]

mailRequest.account = emailAccount

addresseeList = []

//if isTesting use testing email(s)

if(isTesting?.toString() == 'true') {
	testEmails = testEmailsList?.split(',').findAll { it }.collect { it.trim() }
	for(testEmail in testEmails) {
		to = [:]
	
		to.email = testEmail
		to.name = dealer.dealerName
		to.type = 'to'

		addresseeList.add(to)	
	}
} else {
	to = [:]
	if(dealer?.sites[0]?.email?.service?.length() > 0) {
		to.email = dealer?.sites[0]?.email?.service
	} else if(dealer?.sites[0]?.email?.main?.length() > 0) {
		to.email = dealer?.sites[0]?.email?.main
	} else {
		throw new java.lang.Exception("No service email or main email found for dealer, can't sent service request email")
	}
	to.name = dealer.dealerName
	to.type = 'to'
	
	addresseeList.add(to)
}

//send a copy to customer
// cc = [:]
	
// if((serviceRequestPayload.alternativeContact && serviceRequestPayload.alterContactDetails.email)) {
// 	cc.email = serviceRequestPayload.alterContactDetails.email
// 	cc.name = serviceRequestPayload.alterContactDetails.firstName + serviceRequestPayload.alterContactDetails.lastName
// 	cc.type = 'cc'
// 	addresseeList.add(cc)
// } else if (serviceRequestPayload.contactDetails.email) {
// 	cc.email = serviceRequestPayload.contactDetails.email
// 	cc.name = serviceRequestPayload.contactDetails.firstName + serviceRequestPayload.contactDetails.lastName
// 	cc.type = 'cc'
// 	addresseeList.add(cc)
// }


mailRequest.to = addresseeList

mailRequest.template = emailTemplate

//build email contents
serviceRequest = [:]

//handlebars merg language used with email templates doesn't work with camelCase, so keys are mapped to '_' separated
serviceRequest.dealer_name = dealer?.dealerName
serviceRequest.service_desc = serviceRequestPayload.serviceDescription
serviceRequest.service_cost = serviceRequestPayload.serviceCost
serviceRequest.vehicle_desc = serviceRequestPayload.vehicleDescription
serviceRequest.service_code = serviceRequestPayload.serviceCode
serviceRequest.operation = serviceRequestPayload.operation
serviceRequest.vin = flowVars['vin']
serviceRequest.registration_number = serviceRequestPayload.registrationNumber
if(serviceRequestPayload.vehicleOdo)
	serviceRequest.odometer_reading_km = decimalFormat.format(serviceRequestPayload.vehicleOdo.toInteger())
serviceRequest.estimated_service_time = serviceRequestPayload.estimatedServiceTime
if(serviceRequestPayload.dropOffDate)
	serviceRequest.drop_off_date = Date.parse('yyyy-MM-dd', serviceRequestPayload.dropOffDate).format('EEEE dd MMMM yyyy')
if(serviceRequestPayload.dropOffTime)
	serviceRequest.drop_off_time = Date.parse('hh:mm', serviceRequestPayload.dropOffTime).format('h:mm a').toLowerCase()
if(serviceRequestPayload.pickUpDate)
	serviceRequest.pick_up_date = Date.parse('yyyy-MM-dd', serviceRequestPayload.pickUpDate).format('EEEE dd MMMM yyyy')
if(serviceRequestPayload.pickUpTime)	
	serviceRequest.pick_up_time = Date.parse('hh:mm', serviceRequestPayload.pickUpTime).format('h:mm a').toLowerCase()
serviceRequest.notes = serviceRequestPayload.notes
serviceRequest.tsa_flag = serviceRequestPayload.tsaFlag
serviceRequest.wait_at_dealer_flag = serviceRequestPayload.waitAtDealerFlag
serviceRequest.express_service_flag = serviceRequestPayload.expressServiceFlag
serviceRequest.courtesy_bus_flag = serviceRequestPayload.courtesyBusFlag
serviceRequest.loan_vehicle_flag = serviceRequestPayload.loanVehicleFlag

if(serviceRequestPayload.contactDetails?.size() > 0) {
	contact_details = [:]
	contact_details.title = serviceRequestPayload.contactDetails.title
	contact_details.first_name = serviceRequestPayload.contactDetails.firstName
	contact_details.last_name = serviceRequestPayload.contactDetails.lastName
	contact_details.email = serviceRequestPayload.contactDetails.email
	contact_details.phone = formatPhone(serviceRequestPayload.contactDetails.phone)
	
	serviceRequest.contact_details = contact_details

} else if(serviceRequestPayload.alterContactDetails?.size() > 0) {
	contact_details = [:]
	contact_details.title = serviceRequestPayload.alterContactDetails.title
	contact_details.first_name = serviceRequestPayload.alterContactDetails.firstName
	contact_details.last_name = serviceRequestPayload.alterContactDetails.lastName
	contact_details.email = serviceRequestPayload.alterContactDetails.email
	contact_details.phone = formatPhone(serviceRequestPayload.alterContactDetails.phone)
	
	serviceRequest.contact_details = contact_details
} 

serviceRequest.alternative_contact = serviceRequestPayload.alternativeContact



//println 'serviceRequest: '+serviceRequest

serviceRequestMap = [:]
serviceRequestMap.name = 'serviceRequest'
//serviceRequestMap.content = removeNulls(serviceRequest)
serviceRequestMap.content = serviceRequest

mailRequest.globalMergeVars = [serviceRequestMap]

mailRequest.fromAddress = fromAddress
mailRequest.mergeLang = mergeLanguage

mailRequest = GroovyHelper.removeNulls(mailRequest)

return prettyPrint(toJson(mailRequest))
