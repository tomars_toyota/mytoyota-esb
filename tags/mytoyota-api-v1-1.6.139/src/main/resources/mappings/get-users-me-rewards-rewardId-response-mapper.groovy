import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper
import au.com.toyota.esb.mytoyota.api.util.GroovyHelper
import java.text.*

def isValidDate(String dateString) {
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
    try {
        df.parse(dateString)
        return true
    } catch (Exception e) {
        return false
    }
}


reward = new JsonSlurper().parseText(flowVars.rewardDetailsPayload)
reward.remove('isSuccessful')
reward.remove('message')
if(reward?.cardFront?.cardImage && !reward?.cardFront?.cardImage.startsWith("http"))
	reward.cardFront.cardImage='https://'+ sitecoreHost + reward.cardFront.cardImage

// TODO: Hack the prod internal address due to sitecore couldn't replace it properly
if (reward?.cardFront?.cardImage &&  reward?.cardFront?.cardImage.contains("prod-toyota-cd.tmca-digital.com.au:443"))
	reward.cardFront.cardImage = reward.cardFront.cardImage.replace("prod-toyota-cd.tmca-digital.com.au:443", sitecoreHost)

if(!isValidDate(reward?.expiryDate)) reward.expiryDate = null


switch(flowVars.rewardType) {
	case "subscription" :
		def subscriptionPayload = new JsonSlurper().parseText(payload);
		if(subscriptionPayload) {
			reward << ["subscriptions": subscriptionPayload.subscriptions]
			if(!subscriptionPayload.subscriptions?.isEmpty()) {
				reward << ["isClaimed": true]
				break
			}	
		}
		reward << ["isClaimed": false]
		break
	case "competition" :
		def competitionPayload = new JsonSlurper().parseText(payload);
		if(competitionPayload) {
			reward << ["entries": competitionPayload.entries]
			if(!competitionPayload.entries?.isEmpty()) {
				reward << ["isClaimed": true]
				break
			}	
		}
		reward << ["isClaimed": false]
		break	
	default:
		reward << ["isClaimed": false]
		if(flowVars.guestCouponPayload != null && !(flowVars.guestCouponPayload instanceof org.mule.transport.NullPayload)) {
			def couponList = new JsonSlurper().parseText(flowVars.guestCouponPayload)?.coupons
			def couponFirstItem = couponList.find { item -> item.dailyClaimedStatus == "VALID" }
			if (couponFirstItem != null && couponFirstItem.caltexId != null) {
				def pagesList = reward.rewardDetails?.pages
				pagesList?.find { pageItem ->
					if (pageItem.pageType == "qr-code") {
						pageItem.offerCode = couponFirstItem.caltexId
						return true
					}
					else if (pageItem.pageType == "generic-static-coupon") {
						if (reward.couponType == "PREGEN") {
							pageItem.offerCode = couponFirstItem.couponCode
							// PREGEN couponType needs to throw an exception if we couldn't display a coupon
							if (!pageItem.offerCode) {
								println "throwing: " + pageItem.offerCode
								throw new org.mule.module.apikit.exception.NotFoundException("We couldn't get a code at the moment. Please try again later.")
							}
							
							// apply binding to offerlink if necessary
							def binding = [:]
							binding.offerCode = pageItem.offerCode
							
							def engine = new groovy.text.SimpleTemplateEngine()
							def text = pageItem?.offerLink?.url
							if (text) {
								def template = engine.createTemplate(text).make(binding)
								def result = template.toString()
								pageItem.offerLink.url = result
							}
							return true
						}
						else {
							return false
						}
					}
					return false
				}
				
			}
			else {
				if (reward.couponType == "PREGEN") {
					throw new org.mule.module.apikit.exception.NotFoundException("We couldn't get a code at the moment. Please try again later.")
				}
			}
			
		}	
	
}	

	
return prettyPrint(toJson(GroovyHelper.removeNulls(reward, true)))