
import groovy.json.JsonBuilder
def requestPayload = flowVars['requestPayload']

def result = [:]
def profile = [:]


profile.firstName = requestPayload?.firstName ?: ''
result.username = requestPayload?.login ?: ''
result.password = requestPayload?.password ?: ''
result.email = requestPayload?.email ?: ''
result.profile = profile

return new JsonBuilder(result).toString()

// won't map these, don't want this much customer data in gigya
// profile.lastName = requestPayload?.lastName ?: ''
// profile."phones.type" = "mobile"
// profile."phones.number" = requestPayload?.mobile ?: ''