package au.com.toyota.esb.mytoyota.api;

import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.junit.Assert.assertTrue;
import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.custommonkey.xmlunit.Diff;
import org.junit.Test;
import org.mule.DefaultMuleMessage;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.SimplifiedMockResponseTransformer;
import au.com.toyota.esb.test.BlockingPayloadSpy;

public class PostUsersRegisterMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"complete-registration-for-cosi-async.xml",
				"complete-registration-for-native-async.xml",
				"complete-registration-for-rdr-async.xml",
				"config.xml",
				"create-salesforce-sessionid-soap-header.xml",
				"get-orders-order-id-technical-adapter.xml",
				"get-user-id-availability-technical-adapter.xml",
				"get-user-id-availability.xml",
				"get-vehicle-details.xml",
				"get-vehicle-details-technical-adapter.xml",
				"post-passwordpolicy-validations-technical-adapter.xml",
				"post-passwords-encrypt-myKit-technical-adapter.xml",
				"post-register-user-technical-adapter.xml",
				"post-register-user.xml",
				"post-users-me-vehicles-technical-adapter.xml",
				"put-update-user-account-info-technical-adapter.xml",
				"sfdc-add-vehicle.xml",
				"sfdc-create-prospect.xml",
				"sfdc-register-guest-tmc.xml",
				"sfdc-technical-adapter.xml",
				"sfdc-update-user.xml",
				"sfdc-update-guest-preferences.xml",
				"sfdc-retrieve-vehicle-ownership-information.xml",
				"get-users-sfdc-technical-adapter.xml",
				"post-user-dealers-technical-adapter.xml",
				"sfdc-register-guest.xml",
				"post-register-user-idm-technical-adapter.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulRdrRequest() throws Exception {
		
		loadTheMocks();

		String payload = IOUtils.toString(getClass().getResourceAsStream("/in/post-users-registration-rdr-request.json"));
		
		MuleMessage msg = muleMessageWithPayload(payload);
		
		Map<String, Object> props = new HashMap<String,Object>();
		props.put("initiator", "rdr");
        
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
		MuleEvent event = testEvent("");
		event.setMessage(msg);
		
		MuleEvent output = runFlow("post:/users:myToyota-config", event);
		
		System.out.println("_______ output: "+ output.getMessage().getPayload());
		String expectedResponse = "{\"message\": \"Account is active\",\"myToyotaID\": \"MYT-000004b7\"}";
		
		assertJsonEquals(expectedResponse, output.getMessage().getPayloadAsString());
		
		verifyLoggerCall("esb.mytoyota-api-v1.post-register-user.request", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.post-register-user.response", 1);
		
		verifyLoggerCall("esb.mytoyota-api-v1.register-user-technical-adapter.request", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.register-user-technical-adapter.response", 1);
	}
	
	@Test
	public void testAsyncCompleteRdrRegistartion() throws Exception {	
		
		loadTheMocks(false);
		whenMessageProcessor("consumer").ofNamespace("ws")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Operation retrieveVehicleOwnershipInformation"))
			.thenApply(new SimplifiedMockResponseTransformer(200, IOUtils.toString(
				getClass().getResourceAsStream("/in/sfdc-retrieve-vehicle-ownership-information-no-vehicles-response.xml"))));
		
		
		MuleEvent event = testEvent("");

		MuleMessage message = new DefaultMuleMessage("", muleContext);
		event.setMessage(message);
		message.setProperty("requestPayload", buildRdrPayload(), PropertyScope.SESSION);

		runFlow("complete-registration-for-rdr", event);
		
		verifyLoggerCall("esb.mytoyota-api-v1.complete-registration-for-rdr.started", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.complete-registration-for-rdr.completed", 1);
		
		verifyLoggerCall("esb.mytoyota-api-v1.sfdc-register-guest.request", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.sfdc-register-guest.response", 1);
		
		verifyLoggerCall("esb.mytoyota-api-v1.get-users-sfdc-technical-adapter.request", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.get-users-sfdc-technical-adapter.response", 1);
		
		verifyLoggerCall("esb.mytoyota-api-v1.update-user-account-info-technical-adapter-mykit.request", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.update-user-account-info-technical-adapter-mykit.response", 1);
	
		//verify update sfdc and add vehcile haven't failed
		verifyLoggerCall("esb.mytoyota-api-v1.sfdc-update-user.error", 0);
		verifyLoggerCall("esb.mytoyota-api-v1.esb.mytoyota-api-v1.complete-registration-for-rdr.add-user-vehicle-mykit.error", 0);
	}
	
	@Test
	public void testSuccessfulNativeRequest() throws Exception {
				
		loadTheMocks();

		String payload = IOUtils.toString(getClass().getResourceAsStream("/in/post-users-registration-rdr-request.json"));
		
		MuleMessage msg = muleMessageWithPayload(payload);
		
		Map<String, Object> props = new HashMap<String,Object>();
		//props.put("initiator", "native");
        
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
		MuleEvent event = testEvent("");
		event.setMessage(msg);
		
//		MuleEvent event = testEvent(payload);
//		event.setMessage(msg);
		
		MuleEvent output = runFlow("post:/users:myToyota-config", event);
		
		System.out.println("_______ output: "+ output.getMessage().getPayload());
		String expectedResponse = "{\"message\": \"Account pending activation\",\"myToyotaID\": \"MYT-000004b7\"}";
		
		assertJsonEquals(expectedResponse, output.getMessage().getPayloadAsString());
		
		verifyLoggerCall("esb.mytoyota-api-v1.post-register-user.request", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.post-register-user.response", 1);
		
		verifyLoggerCall("esb.mytoyota-api-v1.register-user-technical-adapter.request", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.register-user-technical-adapter.response", 1);
		
		verifyLoggerCall("esb.mytoyota-api-v1.post-user-dealers-technical-adapter.request", 0);
		verifyLoggerCall("esb.mytoyota-api-v1.post-user-dealers-technical-adapter.response", 0);
	}
	
	
	@Test
	public void testAsyncCompleteNativeRegistration() throws Exception {
		testAsyncCompleteNativeRegistrationImpl(false);
		// verify myKIT was called to update user account with sfdc values
		verifyLoggerCall("esb.mytoyota-api-v1.update-user-account-info-technical-adapter-mykit.request", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.update-user-account-info-technical-adapter-mykit.response", 1);
	}
	
	@Test
	public void testAsyncCompleteNativeRegistrationWithFavoriteDealer() throws Exception {
		testAsyncCompleteNativeRegistrationImplWithFavoriteDealer(false);
		// verify myKIT was called to update user account with sfdc values
		verifyLoggerCall("esb.mytoyota-api-v1.update-user-account-info-technical-adapter-mykit.request", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.update-user-account-info-technical-adapter-mykit.response", 1);
	}
	
	@Test
	public void testAsyncCompleteNativeRegistrationNoSfdcDetailsToCopy() throws Exception {
		testAsyncCompleteNativeRegistrationImpl(true);
		// verify myKIT was called to update user account with sfdc values
		verifyLoggerCall("esb.mytoyota-api-v1.update-user-account-info-technical-adapter-mykit.request", 0);
		verifyLoggerCall("esb.mytoyota-api-v1.update-user-account-info-technical-adapter-mykit.response", 0);
	}
	
	private void testAsyncCompleteNativeRegistrationImpl(boolean noSfdcDetailsToCopy) throws Exception {	
		
		loadTheMocks(noSfdcDetailsToCopy);
		
		whenMessageProcessor("consumer").ofNamespace("ws")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Operation retrieveVehicleOwnershipInformation"))
			.thenApply(new SimplifiedMockResponseTransformer(200, IOUtils.toString(
				getClass().getResourceAsStream("/in/sfdc-retrieve-vehicle-ownership-information-response.xml"))));
			
		MuleEvent event = testEvent("");

		MuleMessage message = new DefaultMuleMessage("", muleContext);
		event.setMessage(message);
		message.setProperty("requestPayload", buildCorePayload(), PropertyScope.SESSION);

		runFlow("complete-registration-for-native", event);
		
		verifyLoggerCall("esb.mytoyota-api-v1.complete-registration-for-native.started", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.complete-registration-for-native.vehicle-ownerships-added.completed", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.complete-registration-for-native.no-favorite-dealer-added.completed", 1);
		
		verifyLoggerCall("esb.mytoyota-api-v1.sfdc-register-guest.request", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.sfdc-register-guest.response", 1);
		
		verifyLoggerCall("esb.mytoyota-api-v1.sfdc-retrieve-vehicle-ownership-information.request", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.sfdc-retrieve-vehicle-ownership-information.response", 1);
		
		verifyLoggerCall("esb.mytoyota-api-v1.add-existing-vehicles-to-mykit-subflow.started", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.add-existing-vehicles-to-mykit-subflow.completed", 1);
		
		//verify sfdc call hasn't failed
		verifyLoggerCall("esb.mytoyota-api-v1.sfdc-create-prospect.errorr", 0);
	}
	
	private void testAsyncCompleteNativeRegistrationImplWithFavoriteDealer(boolean noSfdcDetailsToCopy) throws Exception {	
		
		loadTheMocks(noSfdcDetailsToCopy);
		
		whenMessageProcessor("consumer").ofNamespace("ws")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Operation retrieveVehicleOwnershipInformation"))
			.thenApply(new SimplifiedMockResponseTransformer(200, IOUtils.toString(
				getClass().getResourceAsStream("/in/sfdc-retrieve-vehicle-ownership-information-response.xml"))));
			
		MuleEvent event = testEvent("");

		MuleMessage message = new DefaultMuleMessage("", muleContext);
		event.setMessage(message);
		message.setProperty("requestPayload", buildCorePayload(true), PropertyScope.SESSION);

		runFlow("complete-registration-for-native", event);
		
		verifyLoggerCall("esb.mytoyota-api-v1.complete-registration-for-native.started", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.complete-registration-for-native.vehicle-ownerships-added.completed", 1);
		
		verifyLoggerCall("esb.mytoyota-api-v1.sfdc-register-guest.request", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.sfdc-register-guest.response", 1);
		
		verifyLoggerCall("esb.mytoyota-api-v1.sfdc-retrieve-vehicle-ownership-information.request", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.sfdc-retrieve-vehicle-ownership-information.response", 1);
		
		verifyLoggerCall("esb.mytoyota-api-v1.add-existing-vehicles-to-mykit-subflow.started", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.add-existing-vehicles-to-mykit-subflow.completed", 1);
		
		//verify sfdc call hasn't failed
		verifyLoggerCall("esb.mytoyota-api-v1.sfdc-create-prospect.errorr", 0);
		

		verifyLoggerCall("esb.mytoyota-api-v1.post-user-dealers-technical-adapter.request", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.post-user-dealers-technical-adapter.response", 1);
	}
	
	@Test
	public void testAsyncCompleteNativeRegistrationNoVehicleOwnerships() throws Exception {	
		
		loadTheMocks(false);
		whenMessageProcessor("consumer").ofNamespace("ws")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Operation retrieveVehicleOwnershipInformation"))
			.thenApply(new SimplifiedMockResponseTransformer(200, IOUtils.toString(
				getClass().getResourceAsStream("/in/sfdc-retrieve-vehicle-ownership-information-no-vehicles-response.xml"))));
		
		MuleEvent event = testEvent("");

		MuleMessage message = new DefaultMuleMessage("", muleContext);
		event.setMessage(message);
		message.setProperty("requestPayload", buildCorePayload(), PropertyScope.SESSION);

		runFlow("complete-registration-for-native", event);
		
		verifyLoggerCall("esb.mytoyota-api-v1.complete-registration-for-native.started", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.complete-registration-for-native.no-vehicle-ownerships-added.completed", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.complete-registration-for-native.vehicle-ownerships-added.completed", 1);
		
		verifyLoggerCall("esb.mytoyota-api-v1.sfdc-register-guest.request", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.sfdc-register-guest.response", 1);
		
		verifyLoggerCall("esb.mytoyota-api-v1.sfdc-retrieve-vehicle-ownership-information.request", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.sfdc-retrieve-vehicle-ownership-information.response", 1);
		
		verifyLoggerCall("esb.mytoyota-api-v1.add-existing-vehicles-to-mykit-subflow.started", 0);
		verifyLoggerCall("esb.mytoyota-api-v1.add-existing-vehicles-to-mykit-subflow.completed", 0);
		
		//verify sfdc call hasn't failed
		verifyLoggerCall("esb.mytoyota-api-v1.sfdc-create-prospect.errorr", 0);
	}
	
	@Test
	public void testSuccessfulCosiRequest() throws Exception {
				
		loadTheMocks();

		String payload = IOUtils.toString(getClass().getResourceAsStream("/in/post-users-registration-cosi-request.json"));
		
		MuleMessage msg = muleMessageWithPayload(payload);
		
		Map<String, Object> props = new HashMap<String,Object>();
		props.put("initiator", "cosi");
        
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
		MuleEvent event = testEvent("");
		event.setMessage(msg);
		
		MuleEvent output = runFlow("post:/users:myToyota-config", event);
		
		System.out.println("_______ output: "+ output.getMessage().getPayload());
		String expectedResponse = "{\"message\": \"Account is active\",\"myToyotaID\": \"MYT-000004b7\"}";
		
		assertJsonEquals(expectedResponse, output.getMessage().getPayloadAsString());
		
		verifyLoggerCall("esb.mytoyota-api-v1.post-register-user.request", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.post-register-user.response", 1);
		
		verifyLoggerCall("esb.mytoyota-api-v1.register-user-technical-adapter.request", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.register-user-technical-adapter.response", 1);
	}
	
	@Test
	public void testAsyncCompleteCosiRegistartion() throws Exception {	
		
		String expectedSFDCRequest = IOUtils
				.toString(getClass().getResourceAsStream("/in/salesforce-update-guest-preferences-during-cosi-login-request.xml"));
		
		loadTheMocks();
		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(attribute("name").ofNamespace("doc")
						.withValue("Invoke Salesforce Operation retrieveVehicleOwnershipInformation"))
				.thenApply(new SimplifiedMockResponseTransformer(200, IOUtils.toString(getClass().getResourceAsStream(
						"/in/sfdc-retrieve-vehicle-ownership-information-no-vehicles-response.xml"))));
		
		MuleEvent event = testEvent("");

		MuleMessage message = new DefaultMuleMessage(buildCosiPayload(), muleContext);
		event.setMessage(message);
		message.setProperty("myToyotaId", "MYT-000004b7", PropertyScope.SESSION);
		
		// spy for user preferences payload
		BlockingPayloadSpy<?> spy = new BlockingPayloadSpy<>(1);
		spyMessageProcessor("component").ofNamespace("scripting")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Map updateGuestPreferences request")).after(spy);
		
		runFlow("complete-registration-for-cosi", event);
		
		// Assert SFDC Preferences Request
		System.out.println("SFDC Update Guest Preferences Request: " + spy.getFirstPayload().toString());
		Diff diff = new Diff(expectedSFDCRequest, spy.getFirstPayload().toString());
		assertTrue(diff.toString(), diff.similar());
		
		verifyLoggerCall("esb.mytoyota-api-v1.complete-registration-for-cosi.started", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.complete-registration-for-cosi.completed", 1);
		
		verifyLoggerCall("esb.mytoyota-api-v1.get-orders-by-order-id-technical-adapter.request", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.get-orders-by-order-id-technical-adapter.response", 1);
		
		verifyLoggerCall("esb.mytoyota-api-v1.update-user-account-info-technical-adapter-mykit.request", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.update-user-account-info-technical-adapter-mykit.response", 1);
		
//		verifyLoggerCall("esb.mytoyota-api-v1.sfdc-register-guest-tmc.request", 1);
//		verifyLoggerCall("esb.mytoyota-api-v1.sfdc-register-guest-tmc.response", 1);
		
		verifyLoggerCall("esb.mytoyota-api-v1.sfdc-update-guest-preferences.request", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.sfdc-update-guest-preferences.response", 1);
		
		//verify call hasn't failed
		verifyLoggerCall("esb.mytoyota-api-v1.complete-registration-for-cosi.error", 0);
		verifyLoggerCall("esb.mytoyota-api-v1.complete-registration-for-cosi.mykit.error", 0);
		verifyLoggerCall("esb.mytoyota-api-v1.sfdc-register-guest-tmc.error", 0);
	}
	
	private HashMap<String, Object> buildRdrPayload() {
		HashMap<String, Object> frontendRequestMap = buildCorePayload();
    	
    	frontendRequestMap.put("salesforceId", "0011900000NDTZc");
    	frontendRequestMap.put("vin", "JTNKU3JE60J021675");

    	HashMap<String, Object> vehicleUserMap = new HashMap<String, Object>();
    	vehicleUserMap.put("registrationNumber", "ABC123");
    	vehicleUserMap.put("state", "vic");
    	frontendRequestMap.put("vehicleUser", vehicleUserMap);
    	
    	return frontendRequestMap;
	}

	private HashMap<String, Object> buildCosiPayload() {
		HashMap<String, Object> frontendRequestMap = buildCorePayload();
    	
    	frontendRequestMap.put("cosi", "0007518430");
    	
    	return frontendRequestMap;
	}
	
	private HashMap<String, Object> buildCorePayload() {
		return buildCorePayload(false);
	}

	private HashMap<String, Object> buildCorePayload(boolean withFavoriteDealer) {
		HashMap<String, Object> frontendRequestMap = new HashMap<String, Object>();
    	
    	frontendRequestMap.put("login", "james.a.garner@gmail.com");
    	frontendRequestMap.put("password", "password1");
    	frontendRequestMap.put("email", "james.a.garner@gmail.com");
    	frontendRequestMap.put("firstName", "Pinky");
    	frontendRequestMap.put("lastName", "Ray");
    	frontendRequestMap.put("isMarketingOptIn", true);
    	
    	HashMap<String, Object> termsAndConditionsMap = new HashMap<String, Object>();
    	termsAndConditionsMap.put("acceptedVersion", "1.1");
    	frontendRequestMap.put("termsAndConditions", termsAndConditionsMap);
    	
    	HashMap<String, Object> privacyStatementMap = new HashMap<String, Object>();
    	privacyStatementMap.put("acceptedVersion", "1.1");
    	frontendRequestMap.put("privacyStatement", privacyStatementMap);
    	
    	if (withFavoriteDealer) {
    		HashMap<String, Object> favoriteDealer = new HashMap<String, Object>();
    		favoriteDealer.put("dealerCode", "37431");
    		favoriteDealer.put("branchCode", "37431");
    		frontendRequestMap.put("favoriteDealer", favoriteDealer);
    	}
    	
    	return frontendRequestMap;
	}
	
	
	private void loadTheMocks(boolean noSfdcDetailsToCopy) throws Exception {

		loadTheMocks();
		
		if (noSfdcDetailsToCopy)
			whenMessageProcessor("consumer").ofNamespace("ws")
					.withAttributes(attribute("name").ofNamespace("doc")
							.withValue("Invoke Salesforce Operation retrieveGuestInformation"))
					.thenReturn(muleMessageWithPayload(
							IOUtils.toString(getClass().getResourceAsStream("/in/get-users-sfdc-empty-response.xml"))));
		else
			whenMessageProcessor("consumer").ofNamespace("ws")
					.withAttributes(attribute("name").ofNamespace("doc")
							.withValue("Invoke Salesforce Operation retrieveGuestInformation"))
					.thenReturn(muleMessageWithPayload(
							IOUtils.toString(getClass().getResourceAsStream("/in/get-users-sfdc-response.xml"))));
	}
	
	/*
	 * Load mocks for all endpoints
	 */
	private void loadTheMocks() throws Exception {
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("IM API check loginID available"))
				.thenApply(new SimplifiedMockResponseTransformer(200, "{\"isAvailable\":true}"));

		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("myKIT API check email login available"))
				.thenApply(new SimplifiedMockResponseTransformer(200, "{\"valid\":true}"));

		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("myKIT API POST Register Account"))
				.thenApply(new SimplifiedMockResponseTransformer(200, "{\"myToyotaID\":\"MYT-000004b7\"}"));

		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("IM API Register User"))
				.thenApply(new SimplifiedMockResponseTransformer(201, null));

		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("IM API Update User"))
				.thenApply(new SimplifiedMockResponseTransformer(201, null));

		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("IM API Finalise User Registration"))
				.thenApply(new SimplifiedMockResponseTransformer(201, null));

		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("MyKit API: POST /passwords/encrypt"))
				.thenApply(new SimplifiedMockResponseTransformer(200, "{\"encryptedPassword\": \"dfajfefafe\"}"));
		
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("myKIT API PUT Save Account"))
				.thenApply(new SimplifiedMockResponseTransformer(200, null));
		
		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Partner Operation login"))
				.thenReturn(muleMessageWithPayload(
						IOUtils.toString(getClass().getResourceAsStream("/in/sfdc-login-response.xml"))));

		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(
						attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Operation updateOwner"))
				.thenReturn(muleMessageWithPayload(""));
		whenMessageProcessor("consumer").ofNamespace("ws")
			.withAttributes(
					attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Operation registerGuest"))
			.thenReturn(muleMessageWithPayload(""));
		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(
						attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Operation addAVehicle"))
				.thenReturn(muleMessageWithPayload(""));
		
		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(
						attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Operation addGuestVehicle"))
				.thenReturn(muleMessageWithPayload(""));
		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(
						attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Operation registerGuestNONRDR"))
				.thenReturn(muleMessageWithPayload(""));
		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Operation registerGuestTrackMyCar"))
				.thenReturn(muleMessageWithPayload(""));
		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(
						attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Operation updateGuestPreferences"))
				.thenReturn(muleMessageWithPayload(""));
		
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("My Toyota App API Post User Dealers"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							newMessage = originalMessage.createInboundMessage();

							newMessage.setProperty("http.status", 201, PropertyScope.INBOUND);

							newMessage.setPayload(IOUtils.toInputStream("", "UTF-8"));
						} catch (Exception e) {
							// catch exception statements
						}

						return newMessage;
					}
				});

		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("My Toyota App API POST Add Vehicle"))
				//.thenApply(new SimplifiedMockResponseTransformer(201, null));
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							newMessage = originalMessage.createInboundMessage();

							newMessage.setProperty("http.status", 201, PropertyScope.INBOUND);
							newMessage.setPayload(null);
							newMessage.setInvocationProperty("salesforceId", "0011900000NDTZc");
						} catch (Exception e) {
							// catch exception statements
						}

						return newMessage;
					}
				});

		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("My Toyota App API PUT Add Vehicle(s)"))
				.thenApply(new SimplifiedMockResponseTransformer(200, null));
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Vehicle API GET orders by order id"))
				.thenApply(new SimplifiedMockResponseTransformer(201, IOUtils.toString(
						getClass().getResourceAsStream("/in/get-orders-by-order-id-vehicle-api-response.json"))));
		whenMessageProcessor("outbound-endpoint").ofNamespace("vm")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Complete Registration Async"))
				.thenReturnSameEvent();
		
	}
	
	private void verifyLoggerCall (String category, int expectedNumberOfCalls) {
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue(category)).times(expectedNumberOfCalls);
	}
}
