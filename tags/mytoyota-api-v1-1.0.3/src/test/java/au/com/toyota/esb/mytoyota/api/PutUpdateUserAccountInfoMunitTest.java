package au.com.toyota.esb.mytoyota.api;

import static org.mule.munit.common.mocking.Attribute.attribute;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.json.JSONObject;
import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.skyscreamer.jsonassert.JSONAssert;


public class PutUpdateUserAccountInfoMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"create-salesforce-sessionid-soap-header.xml",
				"put-update-user-account-info.xml",
				"put-update-user-account-info-technical-adapter.xml",
				"mytoyota-app-api-auth-session.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulRequest() throws Exception {

		String expectedResult = "{\"success\":true}";
		String requestPath = "/users/me";
		String apiRequest = IOUtils.toString(getClass().getResourceAsStream("/in/put-update-user-account-info-request.json"));
		String salesforceSessionIdResponse = IOUtils.toString(getClass().getResourceAsStream("/in/salesforce-get-sessionid-response.xml"));
		String salesforceUpdateOwnerResponse = IOUtils.toString(getClass().getResourceAsStream("/in/salesforce-update-owner-response.xml"));
		String imUpdateAccountResponse = IOUtils.toString(getClass().getResourceAsStream("/in/im-update-account-response.json"));
		
		whenMessageProcessor("contains").ofNamespace("objectstore")
		.withAttributes(attribute("name").ofNamespace("doc").withValue("Check Auth Token in ObjectStore"))
		.thenReturn(muleMessageWithPayload(true));
		
		whenMessageProcessor("retrieve").ofNamespace("objectstore")
		.withAttributes(attribute("name").ofNamespace("doc").withValue("Get Auth Token from ObjectStore"))
		.thenReturnSameEvent();
		
		whenMessageProcessor("request").ofNamespace("http")
		.withAttributes(attribute("name").ofNamespace("doc").withValue("myKIT API POST Save Account"))
		.thenReturn(muleMessageWithPayload(""));
		
		whenMessageProcessor("consumer").ofNamespace("ws")
		.withAttributes(attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Operation login"))
		.thenReturn(muleMessageWithPayload(salesforceSessionIdResponse));
		
		whenMessageProcessor("consumer").ofNamespace("ws")
		.withAttributes(attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Operation login"))
		.thenReturn(muleMessageWithPayload(salesforceSessionIdResponse));
		
		whenMessageProcessor("consumer").ofNamespace("ws")
		.withAttributes(attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Operation updateOwner"))
		.thenReturn(muleMessageWithPayload(salesforceUpdateOwnerResponse));
		
		whenMessageProcessor("request").ofNamespace("http")
		.withAttributes(attribute("name").ofNamespace("doc").withValue("IM API Update User"))
		.thenReturn(muleMessageWithPayload(imUpdateAccountResponse));
		
		MuleEvent event = testEvent(requestPath);
		event.getMessage().setPayload(apiRequest);

		MuleEvent output = runFlow("put:/users/me:myToyota-config", event);

		System.out.println(output.getMessage().getPayload());

		JSONAssert.assertEquals(expectedResult, (String)output.getMessage().getPayload(), true);
	}
	
}
