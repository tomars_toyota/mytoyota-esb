import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper

//def groovyHelperFile = new File('./src/main/resources/mappings/GroovyHelper.groovy')
def removeNulls(col) {
    if (col instanceof List)
        return col.collect { removeNulls(it) }.findAll { it != null && it != '' && it != [:] && it != []}
    else if (col instanceof Map) 
        return col.collectEntries { [(it.key): removeNulls(it.value) ] }.findAll { it.value != null && it.value != '' && it.value != [] && it.value != [:]}
    else return col
}


def response = [:]
response.vehicle = [:]
response.upsells = []
response.serviceOperations = []

JsonSlurper js = new JsonSlurper()

def getVehicleResponse = payload
def responseVehicle = null
if(getVehicleResponse?.vehicleData?.vehicle)
	responseVehicle = getVehicleResponse?.vehicleData?.vehicle[0]
def upsellsList = getVehicleResponse?.vehicleData?.upsells
def serviceOperationList = getVehicleResponse?.vehicleData?.serviceOperations

response.vehicle.registrationNumber = responseVehicle?.regid
response.vehicle.dmsVehicleId = responseVehicle?.vehid

upsellsList.each { Map upsellItem ->
	def upsell = [:]
	upsell.description = upsellItem.opdsc
	upsell.price = upsellItem.stdchg
	upsell.id = upsellItem.x_rowid
	response.upsells << upsell
}

serviceOperationList.each { Map serviceOp ->
	def serviceOperation = [:]
	serviceOperation.operationTypeId = serviceOp.optid
	serviceOperation.operationId = serviceOp.opid
	serviceOperation.description = serviceOp.opdsc
	serviceOperation.tsaFlag = serviceOp.tsaqua
	serviceOperation.price = serviceOp.fixchg
	serviceOperation.externalClaimNumber = serviceOp.waropr
	serviceOperation.serviceCode = serviceOp.warscd
	serviceOperation.fixedChargeOperationFlag = serviceOp.fixflg
	serviceOperation.cappedPriceOperationFlag = serviceOp.capflg
	response.serviceOperations << serviceOperation
}


//new GroovyShell().parse(groovyHelperFile).with {
  response = removeNulls(response)
//}

return prettyPrint(toJson(response))