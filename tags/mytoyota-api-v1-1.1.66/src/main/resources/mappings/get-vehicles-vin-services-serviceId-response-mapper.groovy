import groovy.json.JsonSlurper
import static groovy.json.JsonOutput.*

//def groovyHelperFile = new File('./src/main/resources/mappings/GroovyHelper.groovy')
def removeNulls(col) {
    if (col instanceof List)
        return col.collect { removeNulls(it) }.findAll { it != null && it != '' && it != [:] && it != []}
    else if (col instanceof Map) 
        return col.collectEntries { [(it.key): removeNulls(it.value) ] }.findAll { it.value != null && it.value != '' && it.value != [] && it.value != [:]}
    else return col
}


def mapService(service) {
	mappedService = [:]
	mappedService.bookingId = service.bookingId
	mappedService.dropOffDate = service.dropOffDate
	mappedService.dropOffTime = service.dropOffTime
	mappedService.pickUpDate = service.pickUpDate
	mappedService.pickUpTime = service.pickUpTime 
	mappedService.status = service.status
	mappedService.notes = service.notes
	mappedService.alertType = service.confirmationType
	mappedService.totalServicePrice = service.totalServicePrice
	mappedService.waitAtDealerFlag = service.waitAtDealerFlag
	mappedService.expressServiceFlag = service.expressServiceFlag
	mappedService.courtesyBusFlag = service.courtesyBusFlag
	mappedService.loanVehicleFlag = service.loanVehicleFlag

	mappedService.dealerDetails = service.dealer

	//replace dmsId and branchCode with dmsID and branchId
	mappedService.dealerDetails.dmsId = service.dealer.dmsID
	mappedService.dealerDetails.branchCode = service.dealer.branchId
	mappedService.dealerDetails.remove('dmsID')
	mappedService.dealerDetails.remove('branchId')

	mappedService.contactDetail = service.contactDetail
	mappedService.vehicle = service.vehicle
	mappedService.serviceOperation = service.serviceOperation
	mappedService.vehicleOdo = service.odometer

	return mappedService
}

//println '___________ payload: '+payload
def myKitResponse = new JsonSlurper().parseText(payload)

def response = [:]

if(myKitResponse instanceof Map) {
	response = mapService(myKitResponse)
} else if(myKitResponse instanceof List) {
	
	servicesList = []

	myKitResponse.each { service ->
		servicesList.add(mapService(service))
	}
	response.services = servicesList
}

// new GroovyShell().parse(groovyHelperFile).with {
  response = removeNulls(response)
// }

return prettyPrint(toJson(response))