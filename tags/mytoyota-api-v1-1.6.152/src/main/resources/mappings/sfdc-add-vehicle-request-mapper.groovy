import groovy.xml.MarkupBuilder

def writer = new StringWriter()
def xmlMarkup = new MarkupBuilder(writer)


def vehicleRequest = payload

//validate request
if(!(vehicleRequest.containsKey('vehicleUser') &&
		vehicleRequest.vehicleUser.containsKey('registrationNumber') &&
		vehicleRequest.vehicleUser.containsKey('state') &&
		vehicleRequest.containsKey('vin')
	))
	throw new Exception('Invalid request')


//def rootEle =
xmlMarkup.mkp.xmlDeclaration(version: "1.0", encoding: "UTF-8")
xmlMarkup.'sp:addAVehicle' (
	'xmlns:sp' : "http://soap.sforce.com/schemas/class/SP_UserRegWSTransactionStructure"
	) {
		'sp:request'{
			'sp:myToyotaId' (flowVars['myToyotaId'])
			'sp:regoNo' (vehicleRequest.vehicleUser.registrationNumber)
			'sp:regoState' (vehicleRequest.vehicleUser.state)
			'sp:VIN' (vehicleRequest.vin)
		}	
	}

//println '__________ payload to salesforce: ' + writer.toString()
return writer.toString()



// <sp:request>
// 	<sp:myToyotaId>?</sp:myToyotaId>
// 	<sp:regoNo>?</sp:regoNo>
// 	<sp:regoState>?</sp:regoState>
// 	<sp:VIN>?</sp:VIN>
// </sp:request>