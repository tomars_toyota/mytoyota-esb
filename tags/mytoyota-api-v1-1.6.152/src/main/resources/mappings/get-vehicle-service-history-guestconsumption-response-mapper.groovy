import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper
import static au.com.toyota.esb.mytoyota.api.util.GroovyHelper.*
import java.text.*


def serviceHistoryResponse = null
def serviceHistoryList = []
def response = [:]

if (payload == null || payload == org.mule.transport.NullPayload) {
    return payload
}

def dateFormat = "yyyy-MM-dd";
SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd HHmmss.SSS")
SimpleDateFormat sdf = new SimpleDateFormat(dateFormat)

serviceHistoryResponse = new JsonSlurper().parseText(payload)

//println("serviceHistory queryStartDate: " + queryStartDate)
//println("serviceHistory queryEndDate: " + queryEndDate)

serviceHistoryResponse?.each { Map valueItem ->
		
	valueItem?.jobs.each { Map jobItem ->

		def service = [:]
		response.vin = valueItem?.vehicle?.VIN

		try {
			service.invoiceDateTime = valueItem?.retailInvoice?.invoiceDateTimeOfIssue
			service.invoiceDate = sdf.format(df.parse(valueItem?.retailInvoice?.invoiceDateTimeOfIssue))
		} catch (Exception e) {
			service.invoiceDate = null
		}

		// filter startDate / endDate
		def inputDate = isValidDate(service.invoiceDate, dateFormat) ? sdf.parse(service.invoiceDate) : null
		def startDate = isValidDate(queryStartDate, dateFormat) ? sdf.parse(queryStartDate) : null
		def endDate = isValidDate(queryEndDate, dateFormat) ? sdf.parse(queryEndDate) : null

		// do not return null invoice date in payload
		if (!isDateInRange(inputDate, startDate, endDate)) {
			return
		}

		service.dealerCode = valueItem?.location?.tmcaDealerCode
		service.dealerName = valueItem?.location?.dealerName
		service.servicingDealerBranchCode = valueItem?.location?.tmcaBranchCode
		service.servicingDealerBranchName = valueItem?.location?.branchName

		service.odometerReadingKm = valueItem?.vehicle?.mileage

		service.repairOrderNumber = valueItem?.repairOrderNumber
		
		//per job
		service.serviceCode = jobItem?.serviceOperation?.code
		service.serviceDescription = jobItem?.serviceOperation?.description
		service.serviceType = jobItem?.serviceOperation?.serviceType
		service.category = jobItem?.serviceOperation?.category

		if(service.category == 'PM') {
			service.categoryDescription = 'Scheduled Logbook Service'
		}

		if(service)
			serviceHistoryList << service
	}

}

//sort ascending order
response.services = serviceHistoryList.sort { it.invoiceDateTime }

return prettyPrint(toJson(removeNulls(response, true)))
