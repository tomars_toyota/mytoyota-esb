package au.com.toyota.esb.mytoyota.api;

import static org.junit.Assert.*;
import static org.mule.munit.common.mocking.Attribute.attribute;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.custommonkey.xmlunit.Diff;
import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleException;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.common.mocking.SpyProcess;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.skyscreamer.jsonassert.JSONAssert;

import au.com.toyota.esb.mytoyota.api.munit.SimplifiedMockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;

public class PutUpdateUsersVehicleVinTest extends FunctionalMunitSuite {

	private static String requestPath = "/users/vehicle";

	@Override
	protected String getConfigResources() {
		return StringUtils.join(
				new String[] { "put-users.xml", "put-update-user-account-info-technical-adapter.xml", 
							   "put-users-vehicles-vin.xml", "put-users-vehicles-vin-technical-adapter.xml", 
							   "config.xml" },
				" ");
	}

	@Test
	public void testSuccessfulRequest() throws Exception {

		// Mock myKIT
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("myKIT API PUT Update Vehicle"))
				.thenApply(new SimplifiedMockResponseTransformer(200, ""));

		String apiRequest = IOUtils
				.toString(getClass().getResourceAsStream("/in/put-update-users-vehicle-vin-request.json"));

		MuleEvent event = testEvent(requestPath);
		MuleMessage msg = muleMessageWithPayload(apiRequest);

		msg.setProperty("client_id", "8bbec807dc1944ee87df0b257f1bf354", PropertyScope.INBOUND);
		msg.setProperty("myToyotaId", "MYT-0000058b", PropertyScope.INBOUND);
		event.setMessage(msg);
		event.setFlowVariable("vin", "JTNKU3JE60J021675");

		MuleEvent output = runFlow("put:/users/vehicles/{vin}:myToyota-config", event);

//		System.out.println("___http.status: " + output.getMessage().getProperty("http.status", PropertyScope.INBOUND));
		assertTrue(output.getMessage().getProperty("http.status", PropertyScope.INBOUND).toString().equals("200"));

		// Verify main flow
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.put-users-vehicles-vin.request")).times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.put-users-vehicles-vin.response")).times(1);

		// Verify authorisation sub-flow
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.put-users.perform-authorisation-check.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.put-users.perform-authorisation-check.response"))
				.times(1);

		// myKIT technical adapter call
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category")
						.withValue("esb.mytoyota-api-v1.put-update-user-vehicle-vin-technical-adapter.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category")
						.withValue("esb.mytoyota-api-v1.put-update-user-vehicle-vin-technical-adapter.response"))
				.times(1);
	}

	@Test
	public void testFailureMissingMyToyotaID() throws Exception {
		// Mock myKIT (shouldn't reach this point)
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("myKIT API PUT Update Vehicle"))
				.thenApply(new SimplifiedMockResponseTransformer(200, ""));

		String expectedResult = "{\"message\":\"Invalid request, missing myToyota ID\"}";

		String apiRequest = IOUtils
				.toString(getClass().getResourceAsStream("/in/put-update-users-vehicle-vin-request.json"));

		MuleEvent event = testEvent(requestPath);
		MuleMessage msg = muleMessageWithPayload(apiRequest);
		msg.setProperty("client_id", "8bbec807dc1944ee87df0b257f1bf354", PropertyScope.INBOUND);
		event.setMessage(msg);
		event.setFlowVariable("vin", "JTNKU3JE60J021675");
		MuleEvent output = runFlow("put:/users/vehicles/{vin}:myToyota-config", event);

		JSONAssert.assertEquals(expectedResult, (String) output.getMessage().getPayload(), true);
		// Check return status of 400
		assertEquals(400, Integer.parseInt(output.getMessage().getOutboundProperty("http.status").toString()));

	}

	@Test
	public void testFailureUnauthorisedClientID() throws Exception {
		// Shouldn't reach this point
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("myKIT API PUT Update Vehicle"))
				.thenApply(new SimplifiedMockResponseTransformer(200, ""));

		String expectedResult = "{\"message\":\"Client ID not authorised on this resource\"}";

		String apiRequest = IOUtils
				.toString(getClass().getResourceAsStream("/in/put-update-users-vehicle-vin-request.json"));

		MuleEvent event = testEvent(requestPath);
		MuleMessage msg = muleMessageWithPayload(apiRequest);
		msg.setProperty("client_id", "notvalid", PropertyScope.INBOUND);
		event.setMessage(msg);
		event.setFlowVariable("vin", "JTNKU3JE60J021675");
		MuleEvent output = runFlow("put:/users/vehicles/{vin}:myToyota-config", event);

		JSONAssert.assertEquals(expectedResult, (String) output.getMessage().getPayload(), true);
		// Check return status of 401
		assertEquals(401, Integer.parseInt(output.getMessage().getOutboundProperty("http.status").toString()));

	}
}
