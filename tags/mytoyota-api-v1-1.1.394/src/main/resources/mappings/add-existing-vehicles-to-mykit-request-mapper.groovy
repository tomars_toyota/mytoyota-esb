import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper

def request = new JsonSlurper().parseText(payload)

// myKIT takes list as input
def vehicles = []

request?.vehicleOwnerships.each { vehicleRequest ->
	
	def vehicle = [:]
	vehicle.batchNumber = vehicleRequest?.batchNumber
	vehicle.vin = vehicleRequest?.vin
	vehicle.deliveryDate = vehicleRequest?.rdrDate
	vehicle.registrationNumber = vehicleRequest?.registrationNumber

	//vehicle user
	vehicleUserMap = [:]
	vehicleUserMap.discontinuedDate = vehicleRequest?.inactivationDate
	vehicleUserMap.registrationNumber = vehicleRequest?.registrationNumber
	vehicleUserMap.state = vehicleRequest?.state
	//remove blanks and nulls
	vehicleUserMap = vehicleUserMap.findAll {it.value}
	
	if(vehicleUserMap) {
		vehicle.vehicleUsers = []
		vehicle.vehicleUsers << vehicleUserMap
	}
	
	//remove blanks and nulls
	vehicle = vehicle.findAll {it.value}

	// Add vehicle to list
	vehicles.add(vehicle)
}

return prettyPrint(toJson(vehicles))

// {
//   "vehicleOwnerships": [
//     {
//       "batchNumber": "",
//       "inactivationDate": "",
//       "rdrDate": "",
//       "registrationNumber": "1EMC526",
//       "state": "VIC",
//       "vin": "5TDBK3FH80S020611"
//     },
//     {
//       "batchNumber": "YCT31E",
//       "inactivationDate": "2017-03-15",
//       "rdrDate": "2017-02-07",
//       "registrationNumber": "YCT31E",
//       "state": "VIC",
//       "vin": "JTNKU3JE30J084720"
//     }
//   ]
// }