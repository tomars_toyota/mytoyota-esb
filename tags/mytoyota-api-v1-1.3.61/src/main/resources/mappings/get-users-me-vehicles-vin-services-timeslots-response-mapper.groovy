import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper

JsonSlurper js = new JsonSlurper()
def timeSlotData = js.parseText(payload).timeSlotData

def response = [:]
response.availableDays = []

if(!timeSlotData)
	return prettyPrint(toJson(response))

def calendarDays = timeSlotData.calendarDays
def timeSlots = timeSlotData.timeSlots
def tuneDateOffset = 2415022

def availableSlots = []

timeSlots.each { slot ->
    if (slot["slAvail"] == true) {
        availableSlots << slot
    }   
}

timeSlots = availableSlots

calendarDays.each { Map day ->
	if(day.daySlots > 0) {
		def availableDay = [:]
		
		def outDate
		outTimeSlots = []
		
		timeSlots.each { Map slot ->
			if(slot.slDay == day.dayNum) {
				outTimeSlots.add(slot.slTime)
				outDate = new GregorianCalendar(1900, Calendar.JANUARY, 1, 0, 0, 0).time + (slot.slDat - tuneDateOffset)
			}
		}
		availableDay.date = outDate.format("yyyy-MM-dd")
		availableDay.times = outTimeSlots
		response.availableDays.add(availableDay)
	}
}

return prettyPrint(toJson(response))