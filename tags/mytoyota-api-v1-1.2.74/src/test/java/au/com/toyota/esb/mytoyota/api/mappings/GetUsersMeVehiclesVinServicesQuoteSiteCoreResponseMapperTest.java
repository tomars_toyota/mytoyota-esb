package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

import javax.script.*;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

public class GetUsersMeVehiclesVinServicesQuoteSiteCoreResponseMapperTest extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;

	public GetUsersMeVehiclesVinServicesQuoteSiteCoreResponseMapperTest() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/get-users-me-vehicles-vin-services-quote-response-mapper.groovy");
	}

	 @Test
	public void testSuccessfulResponseMapper() throws Exception {
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(getClass().getResourceAsStream("/in/get-users-me-vehicles-vin-services-quote-sitecore-response.json")))
				.sessionVar("dealerId", "2293")
				.sessionVar("branchCode", "2293")
				.run().toString();

		System.out.println(result);

		// expected
		String expectedResult = IOUtils.toString(getClass()
				.getResourceAsStream(
						"/out/get-users-me-vehicles-vin-services-quote-api-response.json"));
		JSONAssert.assertEquals(expectedResult, result, true);
	}
	 
	@Test
	public void testExpiredTSAResponseMapper() throws Exception {
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(getClass().getResourceAsStream("/in/get-users-me-vehicles-vin-services-quote-expired-tsa-services-sitecore-response.json")))
				.sessionVar("dealerId", "37425")
				.sessionVar("branchCode", "37425")
				.run().toString();

		System.out.println("**** Result"+result);

		// expected
		String expectedResult = IOUtils.toString(getClass()
				.getResourceAsStream(
						"/out/get-users-me-vehicles-vin-services-quote-expired-tsa-services-api-response.json"));
		JSONAssert.assertEquals(expectedResult, result, true);
	} 
}
