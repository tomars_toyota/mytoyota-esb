import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import java.text.SimpleDateFormat


JsonSlurper js = new JsonSlurper()
//println '___________ apiRequest: '+sessionVars['apiRequest']
def apiRequest = js.parseText(sessionVars['apiRequest'])

//remove nulls
apiRequest = apiRequest.findAll {it.value != null && it.value != ""}

def vehicle = js.parseText(sessionVars['vehicleResponse'])


def requestMap = [:]

requestMap.myToyotaId = flowVars['myToyotaId']

if(apiRequest.contactDetails) {
	requestMap.firstName = apiRequest.contactDetails['firstName']
	requestMap.lastName = apiRequest.contactDetails['lastName']
	requestMap.email = apiRequest.contactDetails['email']
	requestMap.mobilePhone = apiRequest.contactDetails['phone']
}

requestMap.serviceBookingDate = new SimpleDateFormat("dd/MM/yyyy").format(new Date())
requestMap.serviceBookingVehicleModel = vehicle?.vehicleLine?.description
requestMap.serviceBookingVehicleRego = apiRequest['registrationNumber']
requestMap.serviceBookingVehicleVIN = flowVars['vin']
requestMap.serviceBookingNumber = flowVars['dmsBookingId']
requestMap.serviceBookingQuote  = apiRequest['serviceOperationPrice']
requestMap.serviceBookingTypeofService = apiRequest['description']
if(apiRequest['dropOffDate'])
	requestMap.serviceBookingDropOffDate = new SimpleDateFormat("dd/MM/yyyy").format(new Date().parse('yyyy-MM-dd', apiRequest['dropOffDate']))
if(apiRequest['dropOffTime'])
	requestMap.serviceBookingDropOffTime = new SimpleDateFormat("hh:mm a").format(new Date().parse('HH:mm', apiRequest['dropOffTime']))
if(apiRequest['pickUpDate'])
	requestMap.serviceBookingPickUpDate = new SimpleDateFormat("dd/MM/yyyy").format(new Date().parse('yyyy-MM-dd', apiRequest['pickUpDate']))
if(apiRequest['pickUpTime'])
	requestMap.serviceBookingPickUpTime = new SimpleDateFormat("hh:mm a").format(new Date().parse('HH:mm', apiRequest['pickUpTime']))

dealer = js.parseText(sessionVars['dealerResponse'])
requestMap.serviceBookingDealerName = dealer?.dealerName


dealerSite = dealer?.sites.find { it.sapCode == apiRequest.branchCode }
requestMap.serviceBookingDealerPhoneNumber = dealerSite?.phone?.main

requestMap.serviceBookingDealerPostCode = dealerSite?.address?.postCode
requestMap.serviceBookingDealerState = dealerSite?.address?.state
requestMap.serviceBookingDealerAddress = dealerSite?.address?.streetAddress
requestMap.serviceBookingDealerSuburb = dealerSite?.address?.suburb

requestMap.serviceBookingDealerEmail = dealerSite?.email?.service ?: dealerSite?.email?.main
requestMap.serviceBookingDealerWebsite = dealerSite?.webSite

requestMap.serviceBookingCustomerComments = apiRequest['notes']

//remove nulls
requestMap = requestMap.findAll {it.value != null && it.value != ""}

request = ['input':[requestMap]]

return new JsonBuilder(request).toString()