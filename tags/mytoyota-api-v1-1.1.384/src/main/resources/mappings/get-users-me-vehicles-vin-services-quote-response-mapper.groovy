import static groovy.json.JsonOutput.*
import au.com.toyota.esb.mytoyota.api.util.GroovyHelper
import groovy.json.JsonSlurper


def serviceQuoteResponse = new JsonSlurper().parseText(payload)

def response = [:]

response.dealerId = sessionVars['dealerId']
response.branchCode = sessionVars['branchCode']


response.tsaEligible = serviceQuoteResponse.TsaEligible
response.production = serviceQuoteResponse.Production
response.quoteDate = serviceQuoteResponse.QuoteDate
response.quoteExpireDate = serviceQuoteResponse.QuoteExpireDate
response.retailServiceCode = serviceQuoteResponse.ServiceCode
response.hasOutstandingServices = serviceQuoteResponse.HasOutstandingServices

operationsList = []

serviceQuoteResponse?.Operations.each { op ->

    operationMap = [:]

    operationMap.isTsaService = op.IsTsaService

    if(op.TsaServiceDetails) {
        tsaServiceDetailsMap = [:]

        tsaServiceDetailsMap.serviceCode = op.TsaServiceDetails.ServiceCode
        tsaServiceDetailsMap.serviceDescription = op.TsaServiceDetails.ServiceDescription
        if(op.TsaServiceDetails.CustomerPaymentAmount)
            tsaServiceDetailsMap.customerPaymentAmount = op.TsaServiceDetails.CustomerPaymentAmount.toDouble()
        tsaServiceDetailsMap.serviceOperationCode = op.TsaServiceDetails.ServiceOperationCode
        if(op.TsaServiceDetails.EstimatedServiceTime)
            tsaServiceDetailsMap.estimatedServiceTime = op.TsaServiceDetails.EstimatedServiceTime.toDouble()
        tsaServiceDetailsMap.serviceDueDate = op.TsaServiceDetails.ServiceDueDate
        tsaServiceDetailsMap.serviceExpiryDate = op.TsaServiceDetails.ServiceExpiryDate
        
        operationMap.tsaServiceDetails = tsaServiceDetailsMap
    }

    operationMap.serviceNumber = op.serviceNumber
    operationMap.operation = op.operation
    operationMap.kilometres = op.kilometres
    operationMap.months = op.months
    
    // MYT-464
    // If IsTsaService is false AND both TSA and TSG prices are returned, 
    // set both of these prices to ZERO. This will allow the currently implemented 
    // front-end logic to display a message to the guest to contact the dealer
    if( op?.IsTsaService == false && 
        Double.parseDouble(op?.retailServiceCostIncGST) != 0 && 
        Double.parseDouble(op?.tsaServiceCostIncGST) != 0) {

        operationMap.retailServiceCostIncGst = 0.0
        operationMap.tsaServiceCostIncGst = 0.0

    } else {
        if(op.retailServiceCostIncGST)
            operationMap.retailServiceCostIncGst = op?.retailServiceCostIncGST.toDouble()
        if(op.tsaServiceCostIncGST)
            operationMap.tsaServiceCostIncGst = op?.tsaServiceCostIncGST.toDouble()
    }

    if(op.estimatedServiceTime)
        operationMap.estimatedServiceTime = op.estimatedServiceTime.toDouble()

    if(op.serviceKeys) {

        serviceKeysList = []
        
        //ignore serviceKeys where their action is not gonna be done this service 
        op.serviceKeys?.findAll { !it.Action?.startsWith('Activity not performed')}?.each { serviceKey ->
            serviceKeyMap = [:]

            serviceKeyMap.key = serviceKey.Key
            serviceKeyMap.section = serviceKey.Section
            serviceKeyMap.description = serviceKey.Description
            serviceKeyMap.action = serviceKey.Action
            serviceKeysList.add(serviceKeyMap)
        }

        operationMap.serviceKeys = serviceKeysList 
    }

    operationsList.add(operationMap)
}

response.operations = operationsList

response = GroovyHelper.removeNulls(response)

return prettyPrint(toJson(response))