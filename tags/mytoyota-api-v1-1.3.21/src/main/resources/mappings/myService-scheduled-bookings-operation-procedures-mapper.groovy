import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper


def bookedServices = sessionVars.bookedServices

def serviceItem = flowVars.serviceItem


if (serviceItem?.repairOrder?.services?.size() > 0) {
	def repairOrderServiceItem = serviceItem?.repairOrder?.services?.first()
	def operationCode = repairOrderServiceItem.sapOperationCode

	def outstandingServiceMatch = myServiceScheduledServiceResponse?.service?.find {record -> record.code == operationCode}

	def serviceDetailList = outstandingServiceMatch?.serviceDetails	


	def procedureDescriptionList = []

	serviceDetailList?.each { serviceDetailItem ->
		serviceDetailItem?.serviceDescription?.each { serviceDescriptionItem ->
			def procedureDescriptionItem = serviceDescriptionItem.action + " " + serviceDescriptionItem.description
			procedureDescriptionList.add(procedureDescriptionItem)
		}
	}


	repairOrderServiceItem.procedureDescription = procedureDescriptionList.join("\r\n")
}

bookedServices.add(serviceItem)
