import static groovy.json.JsonOutput.*
					
msg = [:]

// Handle missing myToyota ID
if(flowVars['isMissingId'] == true) {
	msg['message'] = 'Invalid request, missing myToyota ID'
	flowVars['statusReason'] = 'Invalid request, missing myToyota ID'
	flowVars['statusCode'] = "400"
} 
// Handle 401 Unauthorised if incorrect Client ID is passed
else if (flowVars['isAuthorised'] == false || (message.getProperty("http.status", org.mule.api.transport.PropertyScope.INBOUND) == 401)) {
	def errorMsg = (exception?.cause?.cause?.message ? exception.cause?.cause?.message : exception?.cause?.message)
	msg['message'] = (errorMsg == null) ? 'Unauthorised' : errorMsg
	flowVars['statusCode'] = "401"
} 
// Handle 404 Not Found which can be thrown from myKIT
else if ((message.getProperty("http.status", org.mule.api.transport.PropertyScope.INBOUND)) == 404) {
	def errorMsg = (exception?.cause?.cause?.message ? exception.cause?.cause?.message : exception?.cause?.message)
	msg['message'] = (errorMsg == null) ? 'Resource not found' : errorMsg
	flowVars['statusCode'] = "404"
} 
// Handle everything else as a 500
else {
	def errorMsg = (exception?.cause?.cause?.message ? exception.cause?.cause?.message : exception?.cause?.message)
	msg['message'] = (errorMsg == null) ? 'Resource not found' : errorMsg
	flowVars['statusCode'] = "500"
}
return toJson(msg)
