import org.mule.api.transport.PropertyScope
import org.mule.module.apikit.exception.*
import au.com.toyota.esb.mytoyota.api.exception.UnauthorisedException

status = message.getProperty("http.status", PropertyScope.INBOUND)

if(status in [200, 201, 202])
	return payload

reason = message.getProperty("http.reason", PropertyScope.INBOUND) ?: ''
excepMsg = message.getProperty("message", PropertyScope.INBOUND) ?: ''

msg  = ''

if(reason && excepMsg)
	msg = reason + ', ' + excepMsg
else if(reason)
	msg = reason
else if(excepMsg)
	msg = excepMsg


flowVars['originalPayload'] = payload

//println '_______status: ' + status

switch(status) {
	case 404:
		throw new NotFoundException(msg)
		break
	case 405:
		throw new MethodNotAllowedException(null, null)
		break
	case 415:
		throw new UnsupportedMediaTypeException()
		break
	case 406:
		throw new NotAcceptableException()
		break
	case 400:
		throw new BadRequestException(msg)
		break
	case 401:
		throw new UnauthorisedException(msg)
		break
	default:
		throw new Exception((String)msg)
}