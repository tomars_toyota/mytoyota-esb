import static groovy.json.JsonOutput.*
import au.com.toyota.esb.mytoyota.api.util.GroovyHelper
import groovy.json.JsonSlurper
import java.text.*

static String toCamelCase(String s){
   String[] parts = s.split(" ");
   String camelCaseString = "";
   for (String part : parts){
      camelCaseString = camelCaseString + (camelCaseString == "" ? "" : " ")  + toProperCase(part);
   }
   return camelCaseString;
}

static String toProperCase(String s) {
    return s.substring(0, 1).toUpperCase() +
               s.substring(1).toLowerCase();
}

def isExpired(String dateString) {
    Date now = new Date()

    try {
        Date parsedDate= Date.parse("yyyy-MM-dd", dateString)
        if(parsedDate > now) return false 
        else return true 
    } catch (ParseException e) {
        return false
    }    

}


def serviceQuoteResponse = new JsonSlurper().parseText(payload)

def response = [:]

def sapOutstandingServices = new JsonSlurper().parseText(flowVars['sapOutstandingServices'])
sptOperationCodeList = []
sptServiceNumberList = []

response.dealerId = sessionVars['dealerId']
response.branchCode = sessionVars['branchCode']


response.tsaEligible = serviceQuoteResponse.TsaEligible
response.production = serviceQuoteResponse.Production
response.quoteDate = serviceQuoteResponse.QuoteDate
response.quoteExpireDate = serviceQuoteResponse.QuoteExpireDate
response.retailServiceCode = serviceQuoteResponse.ServiceCode
response.hasOutstandingServices = serviceQuoteResponse.HasOutstandingServices

operationsList = []

serviceQuoteResponse?.Operations.each { op ->

    operationMap = [:]

    operationMap.isTsaService = op.IsTsaService
    serviceExpired = false
    if(op.TsaServiceDetails) {
        tsaServiceDetailsMap = [:]

        tsaServiceDetailsMap.serviceCode = op.TsaServiceDetails.ServiceCode
        tsaServiceDetailsMap.serviceDescription = op.TsaServiceDetails.ServiceDescription
        if(op.TsaServiceDetails.CustomerPaymentAmount)
            tsaServiceDetailsMap.customerPaymentAmount = op.TsaServiceDetails.CustomerPaymentAmount.toDouble()
        tsaServiceDetailsMap.serviceOperationCode = op.TsaServiceDetails.ServiceOperationCode
        if(op.TsaServiceDetails.EstimatedServiceTime)
            tsaServiceDetailsMap.estimatedServiceTime = op.TsaServiceDetails.EstimatedServiceTime.toDouble()
        tsaServiceDetailsMap.serviceDueDate = op.TsaServiceDetails.ServiceDueDate
        tsaServiceDetailsMap.serviceExpiryDate = op.TsaServiceDetails.ServiceExpiryDate
        serviceExpired = isExpired(op.TsaServiceDetails.ServiceExpiryDate)
        operationMap.tsaServiceDetails = tsaServiceDetailsMap
    }

    operationMap.serviceNumber = op.serviceNumber
    sptServiceNumberList.add(op.serviceNumber)
    operationMap.operation = op.operation
    sptOperationCodeList.add(op.operation)
    operationMap.kilometres = op.kilometres
    operationMap.months = op.months
    
    // MYT-464
    // If IsTsaService is false AND both TSA and TSG prices are returned, 
    // set both of these prices to ZERO. This will allow the currently implemented 
    // front-end logic to display a message to the guest to contact the dealer
    if( op?.IsTsaService == false && 
        Double.parseDouble(op?.retailServiceCostIncGST) != 0 && 
        Double.parseDouble(op?.tsaServiceCostIncGST) != 0) {

        operationMap.retailServiceCostIncGst = 0.0
        operationMap.tsaServiceCostIncGst = 0.0

    } else {
        if(op.retailServiceCostIncGST)
            operationMap.retailServiceCostIncGst = op?.retailServiceCostIncGST.toDouble()
        if(op.tsaServiceCostIncGST)
            operationMap.tsaServiceCostIncGst = op?.tsaServiceCostIncGST.toDouble()
    }

    if(op.estimatedServiceTime)
        operationMap.estimatedServiceTime = op.estimatedServiceTime.toDouble()

    if(op.serviceKeys) {

        serviceKeysList = []
        
        //ignore serviceKeys where their action is not gonna be done this service 
        op.serviceKeys?.findAll { !it.Action?.startsWith('Activity not performed')}?.each { serviceKey ->
            serviceKeyMap = [:]

            serviceKeyMap.key = serviceKey.Key
            serviceKeyMap.section = serviceKey.Section
            serviceKeyMap.description = serviceKey.Description
            serviceKeyMap.action = serviceKey.Action
            serviceKeysList.add(serviceKeyMap)
        }

        operationMap.serviceKeys = serviceKeysList 
    }

    if(!serviceExpired) operationsList.add(operationMap)
}


sapOutstandingServices?.outstandingServices?.each { service ->
    if (!sptOperationCodeList.contains(service.serviceOperationCode)) {
        cviMap = [:]

        cviMap.isTsaService = false
        cviMap.isComplimentary = true

        tsaServiceDetailsMap = [:]
        tsaServiceDetailsMap.serviceCode = service.serviceCode
        tsaServiceDetailsMap.serviceDescription = toCamelCase(service.serviceDescription)
        tsaServiceDetailsMap.customerPaymentAmount = service.customerPaymentAmount
        tsaServiceDetailsMap.serviceOperationCode = service.serviceOperationCode
        tsaServiceDetailsMap.estimatedServiceTime = null
        tsaServiceDetailsMap.serviceDueDate = service.serviceDueDate
        tsaServiceDetailsMap.serviceExpiryDate = service.serviceExpiryDate
        serviceExpired = isExpired(service.serviceExpiryDate)
        
        cviMap.tsaServiceDetails = tsaServiceDetailsMap

        cviMap.serviceNumber = sptServiceNumberList.min() - 1
        cviMap.operation = service.serviceOperationCode
        cviMap.kilometres = null
        cviMap.months = null
        cviMap.retailServiceCostIncGst = null
        cviMap.tsaServiceCostIncGst = null
        cviMap.estimatedServiceTime = null
        
        serviceKeysList = new JsonSlurper().parseText('[{"key":"E04","section":"HARDCODE","description":"engine","action":"Check for normal operation of"},{"key":"E04","section":"HARDCODE","description":"drivetrain","action":"Check for normal operation of"},{"key":"E04","section":"HARDCODE","description":"drive belt","action":"Inspect"},{"key":"E04","section":"HARDCODE","description":"all fluid levels & top up as required","action":"Check"},{"key":"E04","section":"HARDCODE","description":"engine coolant","action":"Inspect"},{"key":"E04","section":"HARDCODE","description":"cooling system, hoses & connections","action":"Inspect"},{"key":"E04","section":"HARDCODE","description":"brake pipes & hoses","action":"Check"},{"key":"E04","section":"HARDCODE","description":"fuel lines & connections","action":"Inspect"},{"key":"E04","section":"HARDCODE","description":"exhaust system for security & leaks","action":"Check"},{"key":"E04","section":"HARDCODE","description":"steering wheel, gear & linkages","action":"Inspect"},{"key":"E04","section":"HARDCODE","description":"front & rear suspension","action":"Inspect"},{"key":"E04","section":"HARDCODE","description":"drive shafts including CV boots","action":"Inspect"},{"key":"E04","section":"HARDCODE","description":"drive shaft bolts","action":"Tighten"},{"key":"E04","section":"HARDCODE","description":"door lock adjustment & body exterior","action":"Inspect"},{"key":"E04","section":"HARDCODE","description":"brake pedal height & hand brake lever operation","action":"Check"},{"key":"E04","section":"HARDCODE","description":"test vehicle & report any defects","action":"Road"}]')
        cviMap.serviceKeys = serviceKeysList 

        if (!serviceExpired) operationsList.add(0, cviMap)
    }
}




response.operations = operationsList

response = GroovyHelper.removeNulls(response)

return prettyPrint(toJson(response))