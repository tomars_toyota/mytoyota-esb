import groovy.json.JsonBuilder
def requestPayload = sessionVars['requestPayload']

def request = [:]

// mapping myKIT login field as null as it's currently still required in their API
request.login = null
//passwords are not kept in myKIT
// request.password = requestPayload?.password ?: null
request.title = requestPayload?.title ?: null
request.firstName = requestPayload?.firstName ?: null
request.lastName = requestPayload?.lastName ?: null 
request.email = requestPayload?.email ?: null
request.mobile = requestPayload?.mobile ?: null
request.fullAddress = requestPayload?.address?.postal?.fullAddress ?: null
request.addressReference = requestPayload?.address?.postal?.totalCheckId ?: null
request.streetAddress = requestPayload?.address?.postal?.streetAddress ?: null
request.postcode = requestPayload?.address?.postal?.postcode ?: null
request.suburb = requestPayload?.address?.postal?.suburb ?: null
request.state = requestPayload?.address?.postal?.state ?: null
request.dateOfBirth = requestPayload?.dateOfBirth ?: null
request.landline = requestPayload?.landline ?: null
request.licenceId = requestPayload?.licenceId ?: null
request.licenceState = requestPayload?.licenceState ?: null
request.licenceCountry = requestPayload?.licenceCountry ?: null
request.licenceExpiry = requestPayload?.licenceExpiry ?: null
if(requestPayload?.cosi) {
	cosiMap = [:]
	cosiMap.cosiId = requestPayload.cosi
	cosiMap.description = null
	request.cosi = cosiMap
}

return new JsonBuilder(request).toString()

/*
	"title": "",
	"langKey": "",
	"fullAddress": "",
	"addressReference": "",
	"streetAddress": "",
	"postcode": "",
	"suburb": "",
	"state": "",
	"address": {
	  "createdBy": "",
	  "createdDate": "",
	  "lastModifiedBy": "",
	  "lastModifiedDate": "",
	  "id": 0,
	  "referenceId": "",
	  "formattedAddress": "",
	  "primaryName": "",
	  "secondaryName": "",
	  "unit": "",
	  "streetNo": "",
	  "streetAddress": "",
	  "streetName": "",
	  "streetType": "",
	  "suburb": "",
	  "state": "",
	  "postcode": "",
	  "country": "",
	  "geoLat": "",
	  "geoLon": "",
	  "geoLatStreet": "",
	  "getLonStreet": "",
	  "listing": false,
	  "postal": false,
	  "dpid": "",
	  "requestPayload?.: {
		"createdBy": "",
		"createdDate": "",
		"lastModifiedBy": "",
		"lastModifiedDate": "",
		"id": 0,
		"login": "",
		"title": "",
		"firstName": "",
		"lastName": "",
		"email": "",
		"mobile": "",
		"activated": false,
		"langKey": "",
		"activationKey": "",
		"activationKeyDate": "",
		"loginFailedCount": 0,
		"loginDisabledDate": "",
		"verificationKeyDate": "",
		"verificationKey": "",
		"verified": false,
		"acceptedTerms": ""
	  }
	},
	"acceptedTerms": false,
	"roles": [
	  ""
	]
  }
*/