
import org.apache.commons.lang.RandomStringUtils
import org.joda.time.DateTime
import org.joda.time.DateTimeZone
// import org.joda.time.format.DateTimeFormat
// import org.joda.time.format.DateTimeFormatter
import java.text.SimpleDateFormat
import com.nimbusds.jose.*
import com.nimbusds.jose.crypto.*
import com.nimbusds.jwt.*
import java.security.MessageDigest
import java.security.SecureRandom
import javax.crypto.KeyGenerator
import javax.crypto.SecretKey

// Create JWT token
// Requires flowVars UID and myToyotaID to be set

def myToyotaId = flowVars['myToyotaId']
def UID = flowVars['UID']

MessageDigest md = MessageDigest.getInstance("SHA-256")
md.update(secretKey.getBytes())
byte[] encryptionKey = md.digest()

JWSSigner signer = new MACSigner(encryptionKey)

JWTClaimsSet claimsSet = new JWTClaimsSet()
claimsSet.setSubject(myToyotaId)
claimsSet.setIssueTime(new Date())
claimsSet.setIssuer(issuer)

// set expiry
exp = Calendar.getInstance()
nextYear = exp.get(Calendar.YEAR)+1
exp.set(Calendar.YEAR, nextYear)
claimsSet.setExpirationTime(exp.getTime()) //not expiring token for a year for now

//format expiry to map in response later
SimpleDateFormat f = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
def formattedExpiry = f.format(exp.getTime());

//Custom claim set
claimsSet.setCustomClaim("myToyotaId", myToyotaId)
claimsSet.setCustomClaim("UID", UID)
// flowVars['UID'] = payload.get('UID')

SignedJWT signedJWT = new SignedJWT(new JWSHeader(JWSAlgorithm.HS256), claimsSet)
signedJWT.sign(signer)

JWEObject jweObject = new JWEObject(
    new JWEHeader.Builder(JWEAlgorithm.DIR, EncryptionMethod.A256GCM).contentType("JWT").build(),
    new Payload(signedJWT))

jweObject.encrypt(new DirectEncrypter(encryptionKey))  

message.setOutboundProperty("Content-Type", "application/json")

payload = jweObject.serialize()

flowVars['APISessionToken'] = [:]
flowVars['APISessionToken']['value'] = payload
flowVars['APISessionToken']['expiry'] = formattedExpiry
