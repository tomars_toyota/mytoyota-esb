package au.com.toyota.esb.mytoyota.api;

import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.junit.Assert.assertEquals;
import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.List;
import java.util.Properties;

import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.util.IOUtils;
import org.mule.util.StringUtils;

import com.jayway.jsonpath.JsonPath;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;

/*
 * Munit test to unit test the functionality to get user notifications
 * @author: ibrahim.abouelela
 */
public class GetUserNotificationsMunitTest extends FunctionalMunitSuite {
	
	private static Properties adapterProperties = new Properties();

	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] { "get-users-notifications.xml",
				"get-users-notifications-technical-adapter.xml", "config.xml" }, " ");
	}

	@Test
	public void testSuccessfulRequestServiceBookingDisabled() throws Exception {
		
		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));

		String myToyotaId = "MYT-00000005";
	
		final String serviceBookingEnabled = "false";
		
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(
						attribute("name").ofNamespace("doc").withValue("My Toyota App API GET Retrieve Users Vehicles"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							newMessage = originalMessage.createInboundMessage();

							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);

							newMessage.setPayload(
									getClass().getResourceAsStream("/in/get-users-me-vehicles-mykit-response.json"));
						} catch (Exception e) {
							// catch exception statements
						}

						return newMessage;
					}
				});

		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Vehicle API GET Outstanding Recalls"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							newMessage = originalMessage.createInboundMessage();

							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							newMessage.setInvocationProperty("serviceBookingEnabled", serviceBookingEnabled);
							newMessage.setPayload(
									getClass().getResourceAsStream("/in/get-vehicles-vin-outstanding-recalls.json"));
						} catch (Exception e) {
							// catch exception statements
						}

						return newMessage;
					}
				});


		MuleEvent event = testEvent("");
		event.setFlowVariable("myToyotaId", myToyotaId);

		MuleMessage msg = muleMessageWithPayload(null);
		event.setMessage(msg);

		MuleEvent output = runFlow("get:/users/me/notifications:myToyota-config", event);
		String payload = output.getMessage().getPayloadAsString();
		// System.out.println(payload);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Log API Request")).times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Log API Response")).times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("category")
				.withValue("esb.mytoyota-api-v1.get-users-notifications-technical-adapter.request")).times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("category")
				.withValue("esb.mytoyota-api-v1.get-users-notifications-technical-adapter.response")).times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.get-users-recall-notifications.request"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category")
						.withValue("esb.mytoyota-api-v1.get-users-recall-notifications.response"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-users-service-booking-notifications.request"))
				.times(0);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-users-service-booking-notifications.response"))
				.times(0);

		String expectedResponse = IOUtils
				.toString(getClass().getResourceAsStream("/out/get-users-notifications-disabled-service-booking-api-response.json"));
		assertJsonEquals(expectedResponse, output.getMessage().getPayloadAsString());
		
	}
	
	@Test
	public void testSuccessfulRequest() throws Exception {
		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));
		

		String myToyotaId = "MYT-00000005";
		
		final String serviceBookingEnabled = adapterProperties.getProperty("api.notifications.serviceBooking.enabled");
		String reminderCutOffTime = adapterProperties.getProperty("service.booking.reminder.cutoff");
		final String targetReminderDateTime = "2017-11-28 " + reminderCutOffTime;
		final String recallType = "SSC";
		System.out.println("serviceBookingEnabled: " + serviceBookingEnabled);
		
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(
						attribute("name").ofNamespace("doc").withValue("My Toyota App API GET Retrieve Users Vehicles"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							newMessage = originalMessage.createInboundMessage();
							
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							newMessage.setInvocationProperty("serviceBookingEnabled", serviceBookingEnabled);
							newMessage.setInvocationProperty("targetReminderDateTime", targetReminderDateTime);
							newMessage.setPayload(
									getClass().getResourceAsStream("/in/get-users-me-vehicles-mykit-response.json"));
						} catch (Exception e) {
							// catch exception statements
						}

						return newMessage;
					}
				});

		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Vehicle API GET Outstanding Recalls"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							newMessage = originalMessage.createInboundMessage();

							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							newMessage.setInvocationProperty("serviceBookingEnabled", serviceBookingEnabled);
							newMessage.setInvocationProperty("recallType", recallType);
							newMessage.setInvocationProperty("targetReminderDateTime", targetReminderDateTime);
							newMessage.setPayload(
									getClass().getResourceAsStream("/in/get-vehicles-vin-outstanding-recalls-vehicleApi-response.json"));
						} catch (Exception e) {
							// catch exception statements
						}

						return newMessage;
					}
				});

		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(
						attribute("name").ofNamespace("doc").withValue("MyKit API: GET /users/{userLogin}/services"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							newMessage = originalMessage.createInboundMessage();

							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							newMessage.setInvocationProperty("targetReminderDateTime", targetReminderDateTime);
							newMessage.setPayload(
									getClass().getResourceAsStream("/in/get-users-me-services-myKit-response.json"));
						} catch (Exception e) {
							// catch exception statements
						}

						return newMessage;
					}
				});

		MuleEvent event = testEvent("");
		event.setFlowVariable("myToyotaId", myToyotaId);

		MuleMessage msg = muleMessageWithPayload(null);
		event.setMessage(msg);

		MuleEvent output = runFlow("get:/users/me/notifications:myToyota-config", event);
		String payload = output.getMessage().getPayloadAsString();
		// System.out.println(payload);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Log API Request")).times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Log API Response")).times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("category")
				.withValue("esb.mytoyota-api-v1.get-users-notifications-technical-adapter.request")).times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("category")
				.withValue("esb.mytoyota-api-v1.get-users-notifications-technical-adapter.response")).times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.get-users-recall-notifications.request"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category")
						.withValue("esb.mytoyota-api-v1.get-users-recall-notifications.response"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-users-service-booking-notifications.request"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-users-service-booking-notifications.response"))
				.times(1);

		String expectedResponse = IOUtils
				.toString(getClass().getResourceAsStream("/out/get-users-notifications-api-response.json"));
		assertJsonEquals(expectedResponse, output.getMessage().getPayloadAsString());
		
	}

	
}
