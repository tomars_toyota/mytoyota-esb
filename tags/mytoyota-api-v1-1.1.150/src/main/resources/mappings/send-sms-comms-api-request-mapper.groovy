import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper
import java.util.regex.Pattern

def request = new JsonSlurper().parseText(payload)

// Function to check if mobile is in E.164 format
// If not, convert to E.164 by dropping leading zero and add +61 country code
def auMobileE146Format = { String m ->

    if (m ==~ auMobileRegEx) {
        return '+61'+m.substring(1)
    } else if (m ==~ auMobileE164RegEx) {
        return m
    } else {
        throw new org.mule.module.apikit.exception.BadRequestException("Invalid mobile number: "+m)
    }
}

// Set the mobile number to be passed to PingWork's Comms API
sessionVars['mobileNumberURLencoded'] = java.net.URLEncoder.encode( auMobileE146Format(sessionVars['mobileNumber']) )

// Set the SMS message body
def message =   smsVerificationTemplateLine1+" "+request?.verificationCode+
                smsVerificationTemplateLineSeparator+
                smsVerificationTemplateLine2 

// Create request
def smsRequest = [:]

smsRequest['tag'] = sessionVars['ESBTransactionId']
smsRequest['context'] = "myToyota vehicle verification for VIN: "+sessionVars['vin']
smsRequest['message'] = message

return prettyPrint(toJson(smsRequest))