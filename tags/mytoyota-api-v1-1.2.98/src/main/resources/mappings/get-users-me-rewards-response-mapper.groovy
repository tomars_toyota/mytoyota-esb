import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper
import au.com.toyota.esb.mytoyota.api.util.GroovyHelper
import java.text.*

def isValidDate(String dateString) {
    SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'")
    try {
        df.parse(dateString)
        return true
    } catch (ParseException e) {
        return false
    }
}

def isExpired(String dateString) {
	Date now = new Date()
	if(isValidDate(dateString)) {
		Date parsedDate= Date.parse("yyyy-MM-dd'T'HH:mm:ss'Z'", dateString)
		if(parsedDate >= now) return false 
		else return true 
	} 
	else {
		return false
	}
	

}
scRewards = []
if(flowVars['scRewards'])
	scRewards = new JsonSlurper().parseText(flowVars['scRewards'])?.rewards
claimedRewards = new JsonSlurper().parseText(payload)

rewardsList = []
scRewards.each { item ->
	item.isClaimed = item.id in claimedRewards.rewardId
	if(item?.cardFront?.cardImage)
		item.cardFront.cardImage='https://'+ sitecoreHost + item.cardFront.cardImage
	item.remove('isSuccessful')
	item.remove('message')
	if (!isExpired(item.expiryDate)) rewardsList.add(item)
}

response = ["rewards":GroovyHelper.removeNulls(rewardsList, true)]
return prettyPrint(toJson(response))