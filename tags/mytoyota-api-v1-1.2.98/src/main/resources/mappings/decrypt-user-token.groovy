
def authTokenHeader = message.getInboundProperty(userTokenHeader)

if (authTokenHeader) {
	log.info "Found $userTokenHeader: $authTokenHeader\n"
	 
	def (userId, username) = authTokenHeader.tokenize(':')
	flowVars['tokenUserId'] = userId
	flowVars['tokenUsername'] = username
}

return payload