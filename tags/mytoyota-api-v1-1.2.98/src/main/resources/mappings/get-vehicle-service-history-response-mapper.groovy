import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper
import au.com.toyota.esb.mytoyota.api.util.GroovyHelper
import java.text.*


serviceHistory = new JsonSlurper().parseText(payload)

serviceHistory.services.removeAll { item ->
	serviceDesc = item.serviceDescription
	if(serviceDesc.length() > 20) !(serviceDesc.toLowerCase().contains("complimentary") || serviceDesc.substring(0,20).contains("/") || serviceDesc.toLowerCase().contains("km")) 
	else  !(serviceDesc.toLowerCase().contains("complimentary"))

}

return prettyPrint(toJson(GroovyHelper.removeNulls(serviceHistory, true)))