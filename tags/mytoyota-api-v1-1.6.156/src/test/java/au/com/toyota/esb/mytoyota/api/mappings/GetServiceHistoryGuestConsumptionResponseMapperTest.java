package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;

import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

public class GetServiceHistoryGuestConsumptionResponseMapperTest extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;

	public GetServiceHistoryGuestConsumptionResponseMapperTest() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/get-vehicle-service-history-guestconsumption-response-mapper.groovy");
	}

	@Test
	public void testSuccessfulRequestMapper() throws Exception {
		
		String result = ScriptRunBuilder.runner(scriptRunner)
				.payload(IOUtils
						.toString(getClass().getResourceAsStream("/in/get-vehicles-vin-service-history-guestConsumptionApi-response.json"), "UTF-8"))
				.binding("queryStartDate", null)
				.binding("queryEndDate", null)
				.binding("vin", "JTDBR23E803142287")
				.run().toString();

		System.out.println(result);
		
		// expected
		String expectedResult = IOUtils
				.toString(getClass().getResourceAsStream("/out/get-vehicles-vin-serviceHistory-response.json"), "UTF-8");
		
		System.out.println(expectedResult);
		
		JSONAssert.assertEquals(expectedResult, result, true);
	}
	
	@Test
	public void testSuccessfulRequestMapperWithEndDate() throws Exception {
		
		String result = ScriptRunBuilder.runner(scriptRunner)
				.payload(IOUtils
						.toString(getClass().getResourceAsStream("/in/get-vehicles-vin-service-history-guestConsumptionApi-response.json"), "UTF-8"))
				.binding("queryStartDate", null)
				.binding("queryEndDate", "2019-04-01")
				.binding("vin", "JTDBR23E803142287")
				.run().toString();

		System.out.println(result);
		
		// expected
		String expectedResult = IOUtils
				.toString(getClass().getResourceAsStream("/out/get-vehicles-vin-serviceHistory-endDate-response.json"), "UTF-8");
		
		System.out.println(expectedResult);
		
		JSONAssert.assertEquals(expectedResult, result, true);
	}
}
