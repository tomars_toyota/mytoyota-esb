/**
 * 
 */
package au.com.toyota.esb.mytoyota.api.util;

/**
 * @author johnver
 *
 */
public class CounterHelper {

	private Integer counter = 0;

	public Integer countAndIncrement() {
		counter = counter++;
		return counter;
	}

}
