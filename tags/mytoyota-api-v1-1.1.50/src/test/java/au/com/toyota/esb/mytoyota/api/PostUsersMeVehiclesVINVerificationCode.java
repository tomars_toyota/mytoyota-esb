package au.com.toyota.esb.mytoyota.api;

import static org.junit.Assert.assertEquals;
import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.Properties;

import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.util.IOUtils;
import org.mule.util.StringUtils;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;

public class PostUsersMeVehiclesVINVerificationCode extends FunctionalMunitSuite {
	
	private static Properties adapterProperties = new Properties();

	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"post-users-me-vehicles-vin-verificationcode.xml",
				"post-users-me-vehicles-vin-verificationcode-technical-adapter.xml",
				"send-verification-sms.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSendVerificationSMSSuccessful() throws Exception {
		
		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));
		String testMobileNumber = "0401111222";
		String expectedURLEncodedE164MobileNumber = "%2B61401111222";
		String testVinNumber = "JTMBFREV80D078597";
		// Mock myKIT response
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
				attribute("name").ofNamespace("doc").withValue("MyKit API: POST /users/{myToyotaId}/vehicles/{vin}/verificationCodes"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						
						try {
							newMessage = originalMessage.createInboundMessage();
							
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							
							newMessage.setPayload(IOUtils.toInputStream("{ \"verificationCode\":\"7423\" }", "UTF-8"));
						} catch (Exception e) {
							// catch exception statements
						}
						
						return newMessage;
					}
				});
				
		// Mock Ping response
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
			attribute("name").ofNamespace("doc").withValue("PING API: POST /comms/sms?phone"))
			.thenApply(new MockResponseTransformer() {
				@Override
				public MuleMessage transform(MuleMessage originalMessage) {
					MuleMessage newMessage = null;
					
					try {
						newMessage = originalMessage.createInboundMessage();
						
						newMessage.setProperty("http.status", 202, PropertyScope.INBOUND);
						
						newMessage.setPayload(IOUtils.toInputStream("queued", "UTF-8"));
					} catch (Exception e) {
						// catch exception statements
					}
					
					return newMessage;
				}
			});

		String requestPayload = IOUtils.toString(getClass().getResourceAsStream("/in/post-users-me-vehicles-vin-verificationcode.json"));

		MuleEvent event = testEvent(requestPayload);
		event.setFlowVariable("mobileNumber",testMobileNumber);
		event.setFlowVariable("vin",testVinNumber);
		event.setSessionVariable("ESBTransactionId", "ff587447-1e72-4adf-9852-223b1e244fe9");
		        
		// Invoke the flow
		@SuppressWarnings("unused")
		MuleEvent output = runFlow("post:/users/me/vehicles/{vin}/verificationCode:myToyota-config", event);
		
		// Assert VIN and mobileNumber sessionVars are set correctly
		assertEquals(output.getSessionVariable("vin").toString(), "JTMBFREV80D078597");
		assertEquals(output.getSessionVariable("mobileNumber").toString(), "0401111222");
		
		// Assert myKIT API Auth Header is set correctly
		// %2B61 = URL encoded +61 prefix for E164 format
		assertEquals(output.getSessionVariable("mobileNumberURLencoded"), expectedURLEncodedE164MobileNumber);
	}
	
	@Test
	public void testSendVerificationSMSSuccessfulCheckLoggingSteps() throws Exception {
		
		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));
		String testMobileNumber = "0401111222";
		String testVinNumber = "JTMBFREV80D078597";
		// Mock myKIT response
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
				attribute("name").ofNamespace("doc").withValue("MyKit API: POST /users/{myToyotaId}/vehicles/{vin}/verificationCodes"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						
						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							newMessage.setPayload(IOUtils.toInputStream("{ \"verificationCode\":\"7423\" }", "UTF-8"));
						} catch (Exception e) {
							// catch exception statements
						}
						
						return newMessage;
					}
				});
				
		// Mock Ping response
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
			attribute("name").ofNamespace("doc").withValue("PING API: POST /comms/sms?phone"))
			.thenApply(new MockResponseTransformer() {
				@Override
				public MuleMessage transform(MuleMessage originalMessage) {
					MuleMessage newMessage = null;
					
					try {
						newMessage = originalMessage.createInboundMessage();
						newMessage.setProperty("http.status", 202, PropertyScope.INBOUND);
						newMessage.setPayload(IOUtils.toInputStream("queued", "UTF-8"));
					} catch (Exception e) {
						// catch exception statements
					}
					
					return newMessage;
				}
			});

		String requestPayload = IOUtils.toString(getClass().getResourceAsStream("/in/post-users-me-vehicles-vin-verificationcode.json"));

		MuleEvent event = testEvent(requestPayload);
		event.setFlowVariable("mobileNumber",testMobileNumber);
		event.setFlowVariable("vin",testVinNumber);
		event.setSessionVariable("ESBTransactionId", "ff587447-1e72-4adf-9852-223b1e244fe9");
		        
		// Invoke the flow
		@SuppressWarnings("unused")
		MuleEvent output = runFlow("post:/users/me/vehicles/{vin}/verificationCode:myToyota-config", event);
		
		// Main flow
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Request")).times(1);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Response")).times(1);	

		// myKIT flow
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log myKIT Request")).times(1);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log myKIT Response")).times(1);
				
		// SMS flow
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log SMS Request")).times(1);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log SMS Response")).times(1);
	}
	
	@Test
	public void testInvalidMobileNumber() throws Exception {
		
		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));
		String testVinNumber = "JTMBFREV80D078597";
		// Mock myKIT response
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
				attribute("name").ofNamespace("doc").withValue("MyKit API: POST /users/{myToyotaId}/vehicles/{vin}/verificationCodes"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						
						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							newMessage.setPayload(IOUtils.toInputStream("{ \"verificationCode\":\"7423\" }", "UTF-8"));
						} catch (Exception e) {
							// catch exception statements
						}
						
						return newMessage;
					}
				});
				
		// Mock Ping response
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
			attribute("name").ofNamespace("doc").withValue("PING API: POST /comms/sms?phone"))
			.thenApply(new MockResponseTransformer() {
				@Override
				public MuleMessage transform(MuleMessage originalMessage) {
					MuleMessage newMessage = null;
					
					try {
						newMessage = originalMessage.createInboundMessage();
						newMessage.setProperty("http.status", 202, PropertyScope.INBOUND);
						newMessage.setPayload(IOUtils.toInputStream("queued", "UTF-8"));
					} catch (Exception e) {
						// catch exception statements
					}
					return newMessage;
				}
			});

		String requestPayload = IOUtils.toString(getClass().getResourceAsStream("/in/post-users-me-vehicles-vin-verificationcode-invalid-mobile.json"));

		MuleEvent event = testEvent(requestPayload);
		event.setFlowVariable("vin",testVinNumber);
		event.setSessionVariable("ESBTransactionId", "ff587447-1e72-4adf-9852-223b1e244fe9");
		        
		// Invoke the flow
		
		try{
			@SuppressWarnings("unused")
			MuleEvent output = runFlow("post:/users/me/vehicles/{vin}/verificationCode:myToyota-config", event);
		} catch (Exception e) {
			
			// Check bad request exception is thrown
			assertEquals("org.mule.module.apikit.exception.BadRequestException: Invalid mobile number: 401111222", e.getCause().getMessage());
			
			// Main flow
			verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Request")).times(1);	
			verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Response")).times(0);	

			// myKIT flow
			verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log myKIT Request")).times(1);	
			verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log myKIT Response")).times(1);

			// SMS flow
			verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log SMS Request")).times(1);	
			verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log SMS Response")).times(0);
		}
	}
}
