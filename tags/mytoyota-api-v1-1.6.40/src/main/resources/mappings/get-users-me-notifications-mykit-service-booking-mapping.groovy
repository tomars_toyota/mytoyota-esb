import java.util.*
//import org.slf4j.Logger
//import org.slf4j.LoggerFactory
import groovy.json.JsonSlurper
  
// removing logger as it is throwing some 
//Logger logger = LoggerFactory.getLogger("esb.mytoyota-api-v1.get-users-me-notifications-mykit-service-booking-mapping.script");


JsonSlurper js = new JsonSlurper()
def serviceBookingItems = []
def myServiceResponse = js.parseText(payload)
//println(sessionVars['vinList'])


def convertToDesiredTimeZone(String dateStr) {
	TimeZone auTimeZoneObj = TimeZone.getTimeZone(auTimeZone)
	def convertedDateTime = null
	try {
		convertedDateTime = Date.parse("yyyy-MM-dd HH:mm", dateStr, auTimeZoneObj)
	}
	catch (Exception e) {
		//logger.error("Not parseable: " + dateStr)
	}
	
	return convertedDateTime
}

TimeZone auTimeZoneObj = TimeZone.getTimeZone(auTimeZone)
def targetReminderCutOffDateTime = convertToDesiredTimeZone(flowVars.targetReminderDateTime)

sessionVars['vinList']?.each {
	serviceBookingMap = [:]
	serviceBookingMap.key = it
	serviceBookingMap.value = []

	myServiceResponse?.services?.each { service ->
		if(it == service.vehicle?.vin) {
			def bookingDropOffDateTime = convertToDesiredTimeZone(service.dropOffDate + " " + service.dropOffTime)
			if (bookingDropOffDateTime != null && targetReminderCutOffDateTime != null) {
				if (bookingDropOffDateTime <= targetReminderCutOffDateTime) {
					serviceBookingMap.value << service.bookingId
				}
				else {
					//logger.info("bookingDropOffDateTime: " + bookingDropOffDateTime + " is greater than " + targetReminderCutOffDateTime)
				}
			}
			else {
				// no filtering add all
				if (bookingDropOffDateTime != null) {
					serviceBookingMap.value << service.bookingId
					//logger.info("bookingDropOffDateTime is: " + bookingDropOffDateTime)
					//logger.info("targetReminderCutOffDateTime is: " + targetReminderCutOffDateTime)
				}
				
			}

			
		}
	}

	serviceBookingItems << serviceBookingMap
}

serviceBookingNotification=[:]
serviceBookingNotification.type = "service-booking"
serviceBookingNotification.items = serviceBookingItems

sessionVars['notication'].notifications << serviceBookingNotification

//println(serviceBookingNotification)

