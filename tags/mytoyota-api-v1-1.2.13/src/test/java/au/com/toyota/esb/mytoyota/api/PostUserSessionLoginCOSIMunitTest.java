package au.com.toyota.esb.mytoyota.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mule.munit.common.mocking.Attribute.attribute;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.custommonkey.xmlunit.Diff;
import org.junit.Ignore;
import org.junit.Test;
import org.mule.DefaultMuleMessage;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.SimplifiedMockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;
import au.com.toyota.esb.test.BlockingPayloadSpy;

@Ignore
public class PostUserSessionLoginCOSIMunitTest extends FunctionalMunitSuite {

	private static Properties adapterProperties = new Properties();

	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] { "post-create-user-session.xml",
				"post-create-user-session-technical-adapter.xml",
				"post-create-user-session-native-login-technical-adapter.xml",
				"post-create-user-session-social-login-registration-technical-adapter.xml",
				"get-user-id-availability.xml", 
				"get-user-id-availability-technical-adapter.xml",
				"post-passwordpolicy-validations-technical-adapter.xml", 
				"post-register-user-technical-adapter.xml",
				"get-user-account-info-technical-adapter.xml", 
				"complete-login-for-cosi-async.xml",
				"post-link-cosi-technical-adapter-flow.xml", 
				"get-orders-order-id.xml",
				"get-orders-order-id-technical-adapter.xml", 
				"put-update-user-account-info-technical-adapter.xml",
				"session-management-im-provider-interactions.xml", 
				"create-salesforce-sessionid-soap-header.xml",
				"post-passwords-encrypt-myKit-technical-adapter.xml", 
				"sfdc-technical-adapter.xml",
				"sfdc-update-user.xml", 
				"sfdc-create-prospect.xml",
				"sfdc-update-guest-preferences.xml",
				"config.xml" }, " ");
	}

	@Test
	public void testSuccessfulCOSILogin() throws Exception {

		loadTheMocks();

		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));

		String requestPath = "post:/sessions:application/json:myToyota-config";
		String customerOrderLoginRequest = IOUtils
				.toString(getClass().getResourceAsStream("/in/post-user-session-login-customer-order-request.json"));

		// Create Mule Event
		MuleEvent event = testEvent("");

		// Add query parameter value, for native this is blank. If you don't set
		// this the MUnit throws a MEL exception
		Map<String, Object> props = new HashMap<String, Object>();
		props.put("loginType", "customerOrder");

		// Prime the Mule Event message with the request payload
		MuleMessage requestMessage = muleMessageWithPayload(customerOrderLoginRequest);

		// Add query param map to Mule Event
		requestMessage.setProperty("http.query.params", props, PropertyScope.INBOUND);

		event.setMessage(requestMessage);

		// Invoke the flow
		MuleEvent output = runFlow(requestPath, event);
		MuleMessage responseMessage = output.getMessage();

		// High level flow request
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.post-create-user-session.request"))
				.times(1);
		// Technical adapter login request
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("category")
				.withValue("esb.mytoyota-api-v1.post-create-user-session-technical-adapter.request")).times(1);
		// Technical adapter native login request
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.post-create-user-session-native-login-technical-adapter-subflow.request"))
				.times(1);
		// Store session token in IM request
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.session-management-store-session-token.request"))
				.times(1);
		// Store session token in IM response
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.session-management-store-session-token.response"))
				.times(1);
		// Technical adapter native login response
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.post-create-user-session-native-login-technical-adapter-subflow.request"))
				.times(1);
		// Technical adapter login response
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("category")
				.withValue("esb.mytoyota-api-v1.post-create-user-session-technical-adapter.response")).times(1);
		// High level flow response
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.post-create-user-session.response"))
				.times(1);

//		assertEquals("myToyota ID", "MYT-00000000", responseMessage.getInvocationProperty("myToyotaId"));
//		assertEquals("UID", "93f295c9ad664c36be464d221acd6fb0", responseMessage.getInvocationProperty("UID"));
//		assertTrue(responseMessage.getInvocationProperty("APISessionToken") != null);
	}

	@Test
	public void testCompleteLoginForCOSIAsyncSuccess() throws Exception {

		loadTheMocks();

		String requestPayload = IOUtils
				.toString(getClass().getResourceAsStream("/out/post-sessions-login-response.json"));

		MuleEvent event = testEvent("");
		MuleMessage message = new DefaultMuleMessage("", muleContext);

		event.setMessage(message);
		event.getMessage().setPayload(requestPayload);

		message.setProperty("requestPayloadMap", buildPayload(), PropertyScope.SESSION);
		message.setProperty("myToyotaId", "MYT-0000063c", PropertyScope.SESSION);
		message.setProperty("loginType", "customerOrder", PropertyScope.SESSION);

		runFlow("complete-login-for-cosi", event);

		// High level flow request
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.complete-login-for-cosi.started"))
				.times(1);
		// Link COSI subflow request
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.post-link-cosi-technical-adapter.request"))
				.times(1);
		// Link COSI subflow response
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.post-link-cosi-technical-adapter.response"))
				.times(1);
		// Get Orders by Order ID request
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-orders-by-order-id.request"))
				.times(1);
		// Get Orders by Order ID response
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-orders-by-order-id.response"))
				.times(1);
		// Update user account - myKIT technical adapter request
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category")
						.withValue("esb.mytoyota-api-v1.update-user-account-info-technical-adapter-mykit.request"))
				.times(1);
		// Update user account - myKIT technical adapter response
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category")
						.withValue("esb.mytoyota-api-v1.update-user-account-info-technical-adapter-mykit.response"))
				.times(1);
		// High level flow response
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.complete-login-for-cosi.completed"))
				.times(1);
	}

	@Test
	public void testCompleteLoginForCOSIAsyncSfdcUpdateSuccess() throws Exception {

		loadTheMocks();

		String requestPayload = IOUtils
				.toString(getClass().getResourceAsStream("/out/post-sessions-login-response.json"));
		String expectedSFDCRequest = IOUtils
				.toString(getClass().getResourceAsStream("/in/salesforce-update-guest-preferences-during-cosi-login-request.xml"));
		
		MuleEvent event = testEvent("");
		MuleMessage message = new DefaultMuleMessage("", muleContext);

		event.setMessage(message);
		event.getMessage().setPayload(requestPayload);

		message.setProperty("requestPayload", buildPayload(), PropertyScope.SESSION);
		message.setProperty("myToyotaId", "MYT-000004b7", PropertyScope.SESSION);
		message.setProperty("loginType", "customerOrder", PropertyScope.SESSION);

		// spy for user preferences payload
		BlockingPayloadSpy<?> spy = new BlockingPayloadSpy<>(1);
		spyMessageProcessor("component").ofNamespace("scripting")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Map updateGuestPreferences request")).after(spy);

		runFlow("complete-login-for-cosi-subflow-update-sfdc-user-with-cosi-details", event);

		// Assert SFDC Preferences Request
		System.out.println("SFDC Update Guest Preferences Request: " + spy.getFirstPayload().toString());
		Diff diff = new Diff(expectedSFDCRequest, spy.getFirstPayload().toString());
		assertTrue(diff.toString(), diff.similar());
		
		// Sub-flow request
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.complete-login-for-cosi-async.started"))
				.times(1);
		// Sub-flow response
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.complete-login-for-cosi-async.completed"))
				.times(1);
		// Update user account - Salesforce update flow request
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.sfdc-update-user.request"))
				.times(1);
		// Update user account - Salesforce update flow response
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.sfdc-update-user.response"))
				.times(1);
		// Update user account - Salesforce technical adapter update flow
		// request
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.sfdc-technical-adapter.request"))
				.times(2);
		// Update user account - Salesforce technical adapter update flow
		// response
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.sfdc-technical-adapter.response"))
				.times(2);
		// Update user preferences - Salesforce technical adapter update flow
		// request
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.sfdc-update-guest-preferences.request"))
				.times(1);
		// Update user preferences - Salesforce technical adapter update flow
		// response
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.sfdc-update-guest-preferences.response"))
				.times(1);
	}

	private HashMap<String, Object> buildPayload() throws JsonParseException, JsonMappingException, IOException {
		final String loginResponse = IOUtils
				.toString(getClass().getResourceAsStream("/out/post-sessions-login-response.json"));
		// HashMap<String, Object> APISessionTokenMap = new HashMap<String,
		// Object>();
		// APISessionTokenMap.put("value",
		// "eyJjdHkiOiJKV1QiLCJlbmMiOiJBMjU2R0NNIiwiYWxnIjoiZGl");
		// APISessionTokenMap.put("expiry", "2018-01-11 02:09:23");
		// APISessionTokenMap.put("tokenType", "myToyotaSessionToken");

		HashMap<String, Object> APISessionTokenMap = new ObjectMapper().readValue(loginResponse, HashMap.class);

		HashMap<String, Object> requestPayloadMap = new HashMap<String, Object>();
		requestPayloadMap.put("APISessionToken", APISessionTokenMap);
		requestPayloadMap.put("myToyotaID", "MYT-0000063c");

		return requestPayloadMap;
	}

	@Test
	public void testFailuresStatusCodes() throws Exception {

		int[] statusCodes = new int[] { 404, 405, 415, 406, 401, 500 };

		MuleEvent event = testEvent("");

		Map<String, Object> props = new HashMap<String, Object>();
		props.put("loginType", "");
		String nativeLoginRequest = IOUtils
				.toString(getClass().getResourceAsStream("/in/post-user-session-login-native-request.json"));

		MuleMessage requestMessage = muleMessageWithPayload(nativeLoginRequest);

		requestMessage.setProperty("http.query.params", props, PropertyScope.INBOUND);

		event.setMessage(requestMessage);

		for (int i = 0; i < statusCodes.length; i++) {
			new StatusCodesTestingHelper().runFailureTest(statusCodes[i], "IM API Login",
					"post:/sessions:application/json:myToyota-config", event);
		}
	}

	/*
	 * Load mocks for all endpoints
	 */
	private void loadTheMocks() throws Exception {

		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("IM API Login"))
				.thenApply(new SimplifiedMockResponseTransformer(200, IOUtils.toString(
						getClass().getResourceAsStream("/in/post-user-session-login-native-im-provider-response.json"))));

		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("IM API Update User"))
				.thenApply(new SimplifiedMockResponseTransformer(200, IOUtils.toString(
						getClass().getResourceAsStream("/in/post-user-session-login-add-session-token-im-provider-response.json"))));
		
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("My Toyota App API POST Link COSI"))
				.thenApply(new SimplifiedMockResponseTransformer(201, null));

		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Vehicle API GET orders by order id"))
				.thenApply(new SimplifiedMockResponseTransformer(201, IOUtils.toString(
						getClass().getResourceAsStream("/in/get-orders-by-order-id-vehicle-api-response.json"))));

		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("myKIT API POST Save Account"))
				.thenApply(new SimplifiedMockResponseTransformer(201, null));

		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(
						attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Partner Operation login"))
				.thenReturn(muleMessageWithPayload(
						IOUtils.toString(getClass().getResourceAsStream("/in/sfdc-login-response.xml"))));

		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(
						attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Operation updateOwner"))
				.thenReturn(muleMessageWithPayload(""));
		
		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(
						attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Operation updateGuestPreferences"))
				.thenReturn(muleMessageWithPayload(""));
		
	}
}
