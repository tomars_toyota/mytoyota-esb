import java.util.Date
import java.security.MessageDigest
import com.nimbusds.jose.JWEObject
import com.nimbusds.jose.crypto.DirectDecrypter
import com.nimbusds.jwt.SignedJWT

def getTokenExpiry(String token) {
	
	//extract encryptionKey
	String secretKey = jwtSecretKey
	MessageDigest md = MessageDigest.getInstance("SHA-256")
	md.update(secretKey.getBytes())
	byte[] encryptionKey = md.digest()

	JWEObject jweObject = JWEObject.parse(token)
	jweObject.decrypt(new DirectDecrypter(encryptionKey))
	SignedJWT signedJWT = jweObject.getPayload().toSignedJWT()
   	
   	return signedJWT.getJWTClaimsSet().getExpirationTime()
}

def removeOldestToken(List tokensList) {

	//pick the oldest token
	oldestToken = null
	tokensList.each { token ->	
		if(!oldestToken)
			oldestToken = token
		else if(getTokenExpiry(oldestToken).compareTo(getTokenExpiry(token)) > 0)
			oldestToken = token
	}

	tokensList.remove(oldestToken)

	return tokensList
}

def request = [:]
def data = flowVars['userProfileMap']?.data
def tokenType = flowVars['APISessionToken']['tokenType']

if(tokenType == 'myToyotaSessionToken') {
	if(data.myToyotaSessionToken instanceof java.util.ArrayList){
		tokensList = data.myToyotaSessionToken
		if(tokensList.size() >= maxloggedInSessions.toInteger())
			tokensList = removeOldestToken(tokensList)
		tokensList.add(flowVars['APISessionToken']['value'])
		data.myToyotaSessionToken = tokensList
	} else {
		//for old accounts where data.myToyotaSessionToken is still a map
		data.myToyotaSessionToken = [flowVars['APISessionToken']['value']]
	}
} else {
	data[(tokenType)] = ["id" : flowVars['APISessionToken']['value']]
}


if(tokenType == 'tlinkSessionToken')
	data[(tokenType)].expiry = flowVars['APISessionToken']['expiry']

request.data = data
return request
