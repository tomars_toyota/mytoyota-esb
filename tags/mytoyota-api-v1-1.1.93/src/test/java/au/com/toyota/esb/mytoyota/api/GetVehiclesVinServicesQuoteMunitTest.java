package au.com.toyota.esb.mytoyota.api;

import java.util.HashMap;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.skyscreamer.jsonassert.JSONAssert;

//@Ignore
public class GetVehiclesVinServicesQuoteMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"get-vehicles-vin-services-quote.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulRequest() throws Exception {

		String expectedResponse = IOUtils.toString(getClass().getResourceAsStream("/out/get-vehicles-vin-services-quote-response.json"));
		String vin = "6T1BD3FKX0X144234";
		
		MuleEvent event = testEvent("");
		event.setFlowVariable("vin", vin);
		
		//prepare query parameters 
		MuleMessage message = muleMessageWithPayload(null);
		HashMap<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("dealerId", "37425");
		queryParams.put("branchCode", "37425");
		queryParams.put("kilometres", "30000");
		
		message.setProperty("http.query.params", queryParams, PropertyScope.INBOUND);
		event.setMessage(message);
		
		
		MuleEvent output = runFlow("get:/vehicles/{vin}/services/quote:myToyota-config", event);

		System.out.println(output.getMessage().getPayload());
		
		JSONAssert.assertEquals(expectedResponse, output.getMessage().getPayload().toString(), true);
	}
}
