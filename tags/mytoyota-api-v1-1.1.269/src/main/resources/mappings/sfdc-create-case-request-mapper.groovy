import groovy.xml.MarkupBuilder
import groovy.json.JsonSlurper
import java.text.SimpleDateFormat

def writer = new StringWriter()
def xmlMarkup = new MarkupBuilder(writer)

if(payload instanceof org.mule.transport.NullPayload)
	payload = null
	
def subject = "Vehicle "+vin+" ownership verification for myToyota user "+myToyotaId

xmlMarkup.mkp.xmlDeclaration(version: "1.0", encoding: "UTF-8")
xmlMarkup.'ent:create' (
	'xmlns:ent' : "urn:enterprise.soap.sforce.com",
	'xmlns:urn1' : "urn:sobject.enterprise.soap.sforce.com",
	'xmlns:xsi' : "http://www.w3.org/2001/XMLSchema-instance"
	) {
		'ent:sObjects' ('xsi:type' : "urn1:Case") {
			//'ent:AccountId' (flowVars['myToyotaUserAccountSFDCId'])
			'Origin' ("myToyota Vehicle Ownership")
			'ent:RecordTypeId' (recordTypeId)
			'Status' ("New")
			'Subject' (subject)
			//'ent:Vehicle__c' (flowVars['vinSFDCId'])
		}	
	}
//println '__________ payload to salesforce: ' + writer.toString()
return writer.toString()
