package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

import javax.script.*;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;
import static org.junit.Assert.assertTrue;

public class PostUsersMeVehiclesMyKitRequestMapperTest extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;

	public PostUsersMeVehiclesMyKitRequestMapperTest() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/post-users-me-vehicles-mykit-request-mapper.groovy");
	}

	@Test
	public void testSuccessfulRequestMapper() throws Exception {
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(getClass().getResourceAsStream("/in/post-users-me-vehicles-api-request.json")))
				.flowVar("skipVerify", null)
				.run().toString();

		System.out.println(result);
		//System.out.println("------------------------");
		
		// expected
		String expectedResult = IOUtils.toString(getClass()
				.getResourceAsStream(
						"/out/post-users-me-vehicles-mykit-request.json"));
		//System.out.println(expectedResult);
		JSONAssert.assertEquals(expectedResult, result, true);
	}
	
	@Test
	public void testSuccessfulRequestMapperWithVerificationSkipped() throws Exception {
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(getClass().getResourceAsStream("/in/post-users-me-vehicles-api-request.json")))
				.flowVar("skipVerify", "TrUe")
				.run().toString();

		System.out.println(result);
		//System.out.println("------------------------");
		
		// expected
		String expectedResult = IOUtils.toString(getClass()
				.getResourceAsStream(
						"/out/post-users-me-vehicles-mykit-request-with-verify-skipped.json"));
		//System.out.println(expectedResult);
		JSONAssert.assertEquals(expectedResult, result, true);
	}
	
	@Test
	public void testSuccessfulNoVehicleUserDetailsRequestMapper() throws Exception {
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(getClass().getResourceAsStream("/in/post-users-me-vehicles-no-vehicleUser-details-api-request.json")))
				.flowVar("skipVerify", "false")
				.run().toString();

		System.out.println(result);
		//System.out.println("------------------------");
		
		// expected
		String expectedResult = IOUtils.toString(getClass()
				.getResourceAsStream(
						"/out/post-users-me-vehicles-no-vehicleUser-details-mykit-request.json"));
		//System.out.println(expectedResult);
		JSONAssert.assertEquals(expectedResult, result, true);
	}
	
	@Test
	public void testSuccessfulNoCustomImageRequestMapper() throws Exception {
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(getClass().getResourceAsStream("/in/post-users-me-vehicles-api-request-no-customImage.json")))
				.flowVar("skipVerify", "false")
				.run().toString();

		System.out.println(result);
		//System.out.println("------------------------");
		
		// expected
		String expectedResult = IOUtils.toString(getClass()
				.getResourceAsStream(
						"/out/post-users-me-vehicles-mykit-request-no-customImage.json"));
		//System.out.println(expectedResult);
		JSONAssert.assertEquals(expectedResult, result, true);
	}

	@Test
	public void testEmptyRequestMapper() throws Exception {
		try {
			ScriptRunBuilder.runner(scriptRunner).payload("{}")
			.flowVar("skipVerify", "false")
			.run().toString();

		} catch (Exception e) {
			if (e.getMessage().contains("Invalid request"))
				assertTrue(true);
			else
				assertTrue(false);
			return;
		}
		assertTrue(false);
	}

	//post-users-me-vehicles-api-request-no-customImage.json
}
