import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper

def vehicleRequest = new JsonSlurper().parseText(payload)

// Build vehicle list
def vehicle = [:]
vehicle.batchNumber = vehicleRequest?.batchNumber
vehicle.compliancePlate = vehicleRequest?.compliancePlate
vehicle.engineNumber = vehicleRequest?.engineNumber
vehicle.vin = vehicleRequest?.vin
vehicle.materialNumber = vehicleRequest?.materialNumber
vehicle.vehicleDescription = vehicleRequest?.vehicleDescription
vehicle.katashikiCode = vehicleRequest?.katashikiCode
vehicle.transmission = vehicleRequest?.transmission
vehicle.sellingDealerCode = vehicleRequest?.sellingDealerCode
vehicle.sellingDealerName = vehicleRequest?.sellingDealerName
vehicle.registrationNumber = vehicleRequest?.registrationNumber
vehicle.production = vehicleRequest?.production

if(vehicleRequest?.suffix) {
    vehicle.suffixCode = vehicleRequest.suffix.code
    vehicle.suffixDescription = vehicleRequest.suffix.description
}
if(vehicleRequest?.trim) {
    vehicle.trimCode = vehicleRequest.trim.code
    vehicle.trimDescription = vehicleRequest.trim.description
}
if(vehicleRequest?.paint) {
    vehicle.paintCode = vehicleRequest.paint.code
    vehicle.paintDescription = vehicleRequest.paint.description
}

vehicle.salesType = vehicleRequest?.salesType	

//stock images
/*imagesList = []
vehicleRequest?.images.each { Map image ->
	imageMap = [:]
	imageMap.code = image.size
	imageMap.url = image.link
	//remove nulls
	imageMap = imageMap.findAll {it.value != null && it.value != ""}

	if(imageMap)
		imagesList << imageMap
}

if(imagesList)
	vehicle.images = imagesList
*/

//vehicle user
vehicleUserMap = [:]
if(vehicleRequest?.vehicleUser) {
	vehicleUserMap.registrationNumber = vehicleRequest.vehicleUser.registrationNumber
	vehicleUserMap.state = vehicleRequest.vehicleUser.state
	vehicleUserMap.vehicleImage = vehicleRequest.customImages[0].link

	//remove nulls
	vehicleUserMap = vehicleUserMap.findAll {it.value != null && it.value != ""}
}

if(vehicleUserMap) {
	vehicle.vehicleUsers = []
	vehicle.vehicleUsers << vehicleUserMap
}

//remove nulls
vehicle = vehicle.findAll {it.value != null && it.value != ""}

return prettyPrint(toJson(vehicle))