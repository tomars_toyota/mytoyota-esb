package au.com.toyota.esb.mytoyota.api;

import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.mule.munit.common.mocking.Attribute.attribute;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.munit.runner.functional.FunctionalMunitSuite;

public class GetVehiclesVinServicebookMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"get-vehicles-vin-servicebook.xml",
				"get-vehicles-vin-servicebook-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulRequest() throws Exception {

		String apiResponse = IOUtils.toString(getClass().getResourceAsStream("/in/get-vehicles-vin-servicebook-serviceapi-response.json"));
		String vin = "6T153BK360X070769";

		whenMessageProcessor("request").ofNamespace("http")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("Service API GET Servicebook"))
			.thenReturn(muleMessageWithPayload(apiResponse));

		MuleEvent event = testEvent("");
		event.setFlowVariable("vin", vin);

		MuleEvent output = runFlow("get:/vehicles/{vin}/servicebook:myToyota-config", event);

		System.out.println(output.getMessage().getPayload());
		assertJsonEquals(apiResponse, output.getMessage().getPayloadAsString());
	}
}
