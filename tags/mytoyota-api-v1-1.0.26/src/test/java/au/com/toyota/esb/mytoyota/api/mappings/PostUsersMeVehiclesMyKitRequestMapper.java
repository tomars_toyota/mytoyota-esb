package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

import javax.script.*;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;
import static org.junit.Assert.assertTrue;

public class PostUsersMeVehiclesMyKitRequestMapper extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;

	public PostUsersMeVehiclesMyKitRequestMapper() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/post-users-me-vehicles-mykit-request-mapper.groovy");
	}

	 @Test
	public void testSuccessfulRequestMapper() throws Exception {
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(getClass().getResourceAsStream("/in/post-users-me-vehicles-api-request.json")))
				.run().toString();

		System.out.println(result);
		//System.out.println("------------------------");
		
		// expected
		String expectedResult = IOUtils.toString(getClass()
				.getResourceAsStream(
						"/out/post-users-me-vehicles-mykit-request.json"));
		//System.out.println(expectedResult);
		JSONAssert.assertEquals(expectedResult, result, true);
	}
}
