import groovy.time.TimeCategory
import java.text.SimpleDateFormat

message.setOutboundProperty("Content-Type", "application/json")


use(TimeCategory) {
    exp = new Date() + 1.year
}

flowVars['APISessionToken'] = [:]
flowVars['APISessionToken']['value'] = java.util.UUID.randomUUID() as String
flowVars['APISessionToken']['expiry'] = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(exp.getTime())
flowVars['APISessionToken']['tokenType'] = tokenType

return payload
