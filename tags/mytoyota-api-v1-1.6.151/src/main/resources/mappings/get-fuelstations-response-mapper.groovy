import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper
import au.com.toyota.esb.mytoyota.api.util.GroovyHelper
import java.text.*

def fuelstationsResponse = null 
def fuelstationsList = []
def response = [:]

if (payload == null || payload == org.mule.transport.NullPayload) {
    return payload
}

fuelstationsResponse = new JsonSlurper().parseText(payload)

fuelstationsResponse?.value.each { Map valueItem ->
	def fuelstation = [:]
	fuelstation.brand = valueItem?.Brand
	fuelstation.state = valueItem?.State
	fuelstation.suburb = valueItem?.Suburb
	fuelstation.address = valueItem?.Address
	fuelstation.postcode = valueItem?.Postcode
	fuelstation.phone = valueItem?.Phone

	if(valueItem?.Location?.coordinates != null && valueItem?.Location?.coordinates?.size() == 2) {
		fuelstation.longitude = valueItem?.Location?.coordinates[0]
		fuelstation.latitude = valueItem?.Location?.coordinates[1]
	}

	fuelstation.locationId = valueItem?.LocationID
	fuelstation.locationName = valueItem?.LocationName

	if(fuelstation)
		fuelstationsList << fuelstation
}

response.fuelstations = fuelstationsList

return prettyPrint(toJson(GroovyHelper.removeNulls(response, true)))