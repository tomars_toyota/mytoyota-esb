package au.com.toyota.esb.mytoyota.api;

import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.cache.InvalidatableCachingStrategy;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.util.IOUtils;
import org.skyscreamer.jsonassert.JSONAssert;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;


public class GetVehiclesVinServiceHistoryMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"get-vehicle-service-history.xml",
				"get-vehicle-service-history-technical-adapter.xml",
				"config.xml"
		}, " ");
	}
	

	@Test
	public void testSuccessfulCallGuestConsumptionAPI() throws Exception {
		
		// Invalidate Cache before running unit test
		InvalidatableCachingStrategy cache = muleContext.getRegistry().<InvalidatableCachingStrategy>get("Generic_Caching_Strategy_Low_Currency");
		cache.invalidate();

		String vin = "JTDBR23E803142287";
		String requestPath = "/api/myToyota/v1/vehicles/" + vin + "/serviceHistory";
		String category = "PM";
		
		whenMessageProcessor("request").ofNamespace("http")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("Guest Consumption API GET Service History"))
			.thenApply(new MockResponseTransformer() {
			@Override
			public MuleMessage transform(MuleMessage originalMessage) {
				MuleMessage newMessage = null;
				
				try {
					newMessage = originalMessage.createInboundMessage();
					newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
					newMessage.setPayload(getClass().getResourceAsStream("/in/get-vehicles-vin-service-history-guestConsumptionApi-response.json"));
				} catch (Exception e) {
					// catch exception statements
				}
				
				return newMessage;
			}
		});

		MuleEvent event = testEvent(requestPath);
		event.setFlowVariable("vin", vin);
		
		MuleMessage message = muleMessageWithPayload(null);

		Map<String, Object> props = new HashMap<String, Object>();
		props.put("category", category);
		
		String queryString = generateQueryString(props);

		message.setProperty("http.query.params", props, PropertyScope.INBOUND);
		message.setProperty("http.query.string", queryString, PropertyScope.INBOUND);
		
		event.setMessage(message);

		MuleEvent output = runFlow("get:/vehicles/{vin}/serviceHistory:myToyota-config", event);
		
		// expected
		String expectedResult = IOUtils.toString(getClass().getResourceAsStream("/out/get-vehicles-vin-serviceHistory-response.json"));

		System.out.println(output.getMessage().getPayload());

		JSONAssert.assertEquals(expectedResult, output.getMessage().getPayloadAsString(), true);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
			.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-vehicle-service-history.request"))
			.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
			.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-vehicle-service-history.response"))
			.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
			.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-vehicle-service-history-technical-adapter.request"))
			.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
			.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-vehicle-service-history-technical-adapter.response"))
			.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
			.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-vehicle-service-history-call-guestconsumption-api-technical-adapter.request"))
			.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
			.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-vehicle-service-history-call-guestconsumption-api-technical-adapter.response"))
			.times(1);
	
	}

	private String generateQueryString(Map<String, Object> props) {
		String queryString = null;
		for (String key : props.keySet()) {
			queryString = (queryString != null) ? queryString + "&" : "";
			queryString = queryString + key + "=" + props.get(key);
			
		}
		return queryString;
	}

}
