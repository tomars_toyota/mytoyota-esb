import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper


def bookedServices = sessionVars.bookedServices

def serviceItem = flowVars.serviceItem

def cviServiceDetails = new JsonSlurper().parseText('[{"section":"General","serviceDescription":[{"action":"Check","description":"for normal operation of engine"},{"action":"Check","description":"for normal operation of drivetrain"},{"action":"Check","description":"for normal operation of exhaust system"},{"action":"Check","description":"for normal operation of brake pipes"},{"action":"Check","description":"for normal operation of hoses"},{"action":"Check","description":"for normal operation of steering"},{"action":"Check","description":"for normal operation of suspension linkages"},{"action":"Check","description":"for normal operation of drive shaft boots and couplings"},{"action":"Check","description":"for normal operation of door lock adjustment"},{"action":"Check","description":"for normal operation of body exterior"},{"action":"Check","description":"for normal operation of park brake adjustment"},{"action":"Inspect","description":"brake pedal free-play"},{"action":"Inspect","description":"brake fluid level"},{"action":"Inspect","description":"engine oil level"},{"action":"Inspect","description":"drive belt tension"},{"action":"Inspect","description":"coolant level and hose clamps"},{"action":"Inspect","description":"condition of fuel lines"}]}]')

if (serviceItem?.repairOrder?.services?.size() > 0) {
	def repairOrderServiceItem = serviceItem?.repairOrder?.services?.first()
	def operationCode = repairOrderServiceItem.sapOperationCode

	def outstandingServiceMatch = myServiceScheduledServiceResponse?.service?.find {record -> record.code == operationCode}

	def serviceDetailList = outstandingServiceMatch?.serviceDetails	

	if (outstandingServiceMatch != null && outstandingServiceMatch.serviceType == "OSB_SERVICE_TYPE_CI") {
		// hardcode the CVI inclusions for now.
		outstandingServiceMatch.serviceDetails = cviServiceDetails
		serviceDetailList = outstandingServiceMatch?.serviceDetails
	}


	def procedureDescriptionList = []

	serviceDetailList?.each { serviceDetailItem ->
		serviceDetailItem?.serviceDescription?.each { serviceDescriptionItem ->
			def procedureDescriptionItem = serviceDescriptionItem.action + " " + serviceDescriptionItem.description
			procedureDescriptionList.add(procedureDescriptionItem)
		}
	}

	repairOrderServiceItem.serviceDetails = serviceDetailList 
	repairOrderServiceItem.procedureDescription = procedureDescriptionList.join("\r\n")
}

bookedServices.add(serviceItem)
