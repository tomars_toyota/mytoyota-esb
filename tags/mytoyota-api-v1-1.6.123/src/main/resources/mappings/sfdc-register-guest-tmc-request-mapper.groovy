import groovy.xml.MarkupBuilder
import java.text.SimpleDateFormat
import groovy.json.JsonSlurper

def writer = new StringWriter()
def xmlMarkup = new MarkupBuilder(writer)

println 'orderResponse: ' + sessionVars['orderResponse']

order = new JsonSlurper().parseText(sessionVars['orderResponse'])

postalAddress = order?.address?.postal

originalRequest = flowVars['requestPayload']



xmlMarkup.mkp.xmlDeclaration(version: "1.0", encoding: "UTF-8")
xmlMarkup.'sp:registerGuestNONRDR' (
	'xmlns:sp' : "http://soap.sforce.com/schemas/class/SP_UserRegWSTransactionStructure"
	) {
		'sp:request'{
			if(postalAddress) {
				'sp:Address' {
					if(postalAddress.suburb != null)
						'sp:cityorsuburb' (postalAddress.suburb)
					if(postalAddress.postcode != null)
						'sp:postcode' (postalAddress.postcode)
					if(postalAddress.state != null)
						'sp:state' (postalAddress.state)
					if(postalAddress.streetAddress != null)
						'sp:street' (postalAddress.streetAddress)
				}
			}
			if(originalRequest?.isMarketingOptIn != null)
				'sp:marketingPreference' (originalRequest.isMarketingOptIn)
			if(sessionVars['myToyotaId'])
				'sp:myToyotaId' (sessionVars['myToyotaId'])
			if((order?.email != null) ||
					(originalRequest?.firstName != null) ||
					(originalRequest?.lastName != null) ||
					(sessionVars['encryptedPassword'])) {
				'sp:ownerDetails'{
					if(order?.dateOfBirth != null) {
						if(order.dateOfBirth == '') 
							'sp:dob' (order.dateOfBirth)
						else {
							'sp:dob' (new SimpleDateFormat("dd/MM/yyyy").format(new Date().parse('yyyy-MM-dd', order.dateOfBirth)))
						}
					}
					if(order?.email != null)
						'sp:email' (order.email)
					if(originalRequest?.firstName != null)
						'sp:firstName' (originalRequest.firstName)
					if(originalRequest?.lastName != null)
						'sp:lastName' (originalRequest.lastName)
					if(sessionVars['encryptedPassword'])
						'sp:Password' (sessionVars['encryptedPassword'])
					if(order?.mobile != null)
						'sp:personmobilephone' (order.mobile)
					if(order?.landline != null)
						'sp:phone' (order.landline)
					if(order?.title != null)
						'sp:salutation' (order.title)
				}
			}
			if(order?.email)
				'sp:OwnersPortalUserId' (order.email)
			'sp:trackMyCar'{
				if(order?.guestOrder?.vehicleDetails?.batchId != null)
					'sp:batchId' (order.guestOrder.vehicleDetails.batchId)
				if(order?.guestOrder?.cosi != null)
					'sp:cosi' (order.guestOrder.cosi)
				if(order?.guestOrder?.status?.statusDescription != null)
					'sp:statusDescription' (order.guestOrder.status.statusDescription)
			}
		}	
	}
//println '__________ payload to salesforce: ' + writer.toString()
return writer.toString()

// ** TARGET PAYLOAD **
// request
// 	Address
// 		cityorsuburb
// 		postcode
// 		state
// 		street
// 	*marketingPreference
// 	*myToyotaId
// 	ownerDetails
// 		dob
// 		*email
// 		*firstName
// 		*lastName 
// 		*Password 
// 		personmobilephone
// 		phone
// 		salutation
// 	*OwnersPortalUserId
// 	trackMyCar
// 		*batchId
// 		*cosi
// 		*statusDescription

