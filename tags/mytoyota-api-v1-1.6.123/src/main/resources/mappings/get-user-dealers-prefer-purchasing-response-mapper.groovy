import groovy.json.JsonSlurper
import static groovy.json.JsonOutput.*

def stripLeadingZeros = { it.replaceFirst("^0*", "") }

/* Structure: dealerVinMap = ['dealerId:branchId':[vins]] */
def dealerVinMap = message.getProperty("dealerVins",org.mule.api.transport.PropertyScope.INVOCATION)

def dealers = new JsonSlurper().parseText(payload)
/* Structure: dealerDetailMap = ['dealerId:branchId': dealerDetail] */
def dealerDetailMap = dealers.collectEntries({ [(it.dealerId + ":" + it.branchId) : it] })

/* For each entry in dealerDetailMap, enrich it with the vins from dealerVinMap */
dealerDetailMap.each { dealerCode, dealerDetail ->
		dealerId = stripLeadingZeros(dealerDetail.dealerId)
		branchId = stripLeadingZeros(dealerDetail.branchId)
		dealerDetail.favorite = false
    	dealerDetail.vehicles = dealerVinMap[dealerId + ":" + branchId]
}

return prettyPrint(toJson(dealerDetailMap.values()))