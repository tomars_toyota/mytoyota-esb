package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

import javax.script.*;
import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;
import org.mule.api.MuleMessage;
import org.mule.DefaultMuleMessage;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.mule.context.DefaultMuleContextFactory;
import org.mule.api.MuleContext;
import org.mule.api.transport.PropertyScope;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

public class GetUserAccountInfoGuestApiResponseMapperTest extends AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;
	private MuleContext muleContext;
	
	
	public GetUserAccountInfoGuestApiResponseMapperTest() throws ScriptException, SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/get-user-account-info-guest-api-provider-response-mapper.groovy");
	}
	
	@Test
	public void testRequestMapper() throws Exception {
		
		muleContext = new DefaultMuleContextFactory().createMuleContext();
		MuleMessage message = new DefaultMuleMessage(null,muleContext);
		
		message.setProperty("http.status", 200, PropertyScope.INBOUND);
		
		Object result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(getClass().getResourceAsStream(
						"/in/get-user-account-info-guest-api-response.json")))
				.flowVar("myToyotaId", "TEST-0000001")
				.message(message)
				.run();
		String resultJson = new ObjectMapper().writeValueAsString(result);
		System.out.println(resultJson);
		
		// expected
		String expectedResult = IOUtils.toString(getClass().getResourceAsStream(
				"/out/get-user-account-info-guest-api-response.json"));
		JSONAssert.assertEquals(expectedResult, resultJson, true);
	}
	
}
