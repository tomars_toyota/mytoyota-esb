import groovy.json.JsonSlurper
import static groovy.json.JsonOutput.*
import java.text.DateFormat
import java.text.SimpleDateFormat

def manualsResponse
if (payload != org.mule.transport.NullPayload) {
	manualsResponse = new JsonSlurper().parseText(payload)
} else {
	return ""
}


def response = [:]
def manualsList = []

manualsResponse?.each { manual ->
	def map = [:]
	map['type'] = manual?.documentType
	map['title'] = manual?.title
	map['link'] = manualsHost + basePath + manual?.prettyName
	manualsList.add(map)
}

response['vehicleDescription'] = sessionVars['vehicleDescription']
response['production'] = sessionVars['production']
response['manuals'] = manualsList
    

return prettyPrint(toJson(response))

