import groovy.json.JsonSlurper
import groovy.json.JsonOutput
import groovy.time.TimeCategory

vehicle = new JsonSlurper().parseText(payload)
if(vehicle?.makeCode != 'TO')
	flowVars['isValidVin'] = false
else
	flowVars['isValidVin'] = true

//validate deliveryDate
validDeliveryDate = false
try {
    if(vehicle?.deliveryDate) {
        Date.parse('yyyy-MM-dd', vehicle.deliveryDate)
        if(vehicle.deliveryDate.substring(0,4).toInteger() >= 1900)
            validDeliveryDate = true
    }
} catch(Exception e) {}


if(validDeliveryDate)
    use (TimeCategory) {
        warrantyEndDate = new Date().parse('yyyy-MM-dd', vehicle.deliveryDate) + 3.year
        if(warrantyEndDate)
            vehicle.warrantyEndDate = warrantyEndDate.format("yyyy-MM-dd")
    }
else
	vehicle.warrantyEndDate = null
return JsonOutput.prettyPrint(JsonOutput.toJson(vehicle))