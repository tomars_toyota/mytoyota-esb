import groovy.xml.MarkupBuilder
import groovy.json.JsonSlurper
import java.text.SimpleDateFormat

def writer = new StringWriter()
def xmlMarkup = new MarkupBuilder(writer)


def user = payload
def postalAddress = user?.address?.postal

//def rootEle =
xmlMarkup.mkp.xmlDeclaration(version: "1.0", encoding: "UTF-8")
xmlMarkup.'sp:updateOwner' (
	'xmlns:sp' : "http://soap.sforce.com/schemas/class/SP_UserRegWSTransactionStructure"
	) {
		'sp:request'{
			if(postalAddress) {
				'sp:Address' {
					if(postalAddress.suburb != null)
						'sp:cityorsuburb' (postalAddress.suburb)
					// if(postalAddress.country != null) 								//country is not being sent by front-end
					// 	'sp:country' (postalAddress.country)
					if(postalAddress.postcode != null)
						'sp:postcode' (postalAddress.postcode)
					if(postalAddress.state != null)
						'sp:state' (postalAddress.state)
					if(postalAddress.streetAddress != null)
						'sp:street' (postalAddress.streetAddress)
				}
			}
			if(user.isMarketingOptIn != null)
				'sp:marketingPreference' (user.isMarketingOptIn)
			if(sessionVars['myToyotaId'])
				'sp:myToyotaId' (sessionVars['myToyotaId'])
			if(user.dateOfBirth || 
					(user.email != null) ||
					(user.firstName != null) ||
					(user.landline != null) ||
					(user.lastName != null) ||
					(user.mobile != null) ||
					(user.title != null) ||
					(sessionVars['encryptedPassword'])) {
				'sp:ownerDetails'{
					if(user.dateOfBirth != null) {
						if(user.dateOfBirth == '') 
							'sp:dob' (user.dateOfBirth)
						else {
							'sp:dob' (new SimpleDateFormat("dd/MM/yyyy").format(new Date().parse('yyyy-MM-dd', user.dateOfBirth)))
						}
					}
					if(user.email != null)
						'sp:email' (user.email)
					if(user.firstName != null)
						'sp:firstName' (user.firstName)
					if(user.lastName != null)
						'sp:lastName' (user.lastName)
					if(sessionVars['encryptedPassword'])
						'sp:Password' (sessionVars['encryptedPassword'])
					if(user.mobile != null)
						'sp:personmobilephone' (user.mobile)
					if(user.landline != null)
						'sp:phone' (user.landline)
					if(user.title != null)
						'sp:salutation' (user.title)
				}
			}
			if(user.salesforceId)
				'sp:salesforceId' (user.salesforceId)
			if(sessionVars['requestPayload'] && sessionVars['requestPayload']['login'])
				'sp:OwnersPortalUserId' (sessionVars['requestPayload']['login'])
			if(user.vin != null)
				'sp:VIN' (user.vin)
		}	
	}
//println '__________ payload to salesforce: ' + writer.toString()
return writer.toString()



// <sp:request>
// 	<sp:Address>
// 		<sp:cityorsuburb>?</sp:cityorsuburb>
// 		<sp:country>?</sp:country>
// 		<sp:postcode>?</sp:postcode>
// 		<sp:state>?</sp:state>
// 		<sp:street>?</sp:street>
// 	</sp:Address>
// 	<sp:marketingPreference>?</sp:marketingPreference>
// 	<sp:myToyotaId>?</sp:myToyotaId>
// 	<sp:ownerDetails>
// 		<sp:dob>?</sp:dob>
// 		<sp:email>?</sp:email>
// 		<sp:firstName>?</sp:firstName>
// 		<sp:landline>?</sp:landline>
// 		<sp:lastName>?</sp:lastName>
// 		<sp:personmobilephone>?</sp:personmobilephone>
//		<sp:phone>?</sp:phone>
// 		<sp:salutation>?</sp:salutation>
// 	</sp:ownerDetails>
// 	<sp:salesforceId>?</sp:salesforceId>
// 	<sp:VIN>?</sp:VIN>
// </sp:request>