package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.PrintStream;
import au.com.toyota.esb.mytoyota.api.util.ServiceDescriptionHelper;

import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import groovy.lang.Binding;
import groovy.lang.GroovyShell;
import groovy.util.GroovyTestCase;
import junit.framework.TestCase;

public class ServiceDescriptionHelperMapperTest {

	
	public void setUp() {
        System.out.println("Inside setup()");
    }

	@Test
	public void serviceDescriptionTest() {
		
		String formatDescription = ServiceDescriptionHelper.formatServiceDescription("15000KM or 12 MTHS Service", false);
        
        Assert.assertEquals("15,000 km or 12 Months Service", formatDescription);
    }
	
	@Test
	public void serviceDescriptionWithCommaTest() {
		
		String formatDescription = ServiceDescriptionHelper.formatServiceDescription("15,000 KM or 12 MTHS Service", false);
        
        Assert.assertEquals("15,000 km or 12 Months Service", formatDescription);
    }
	
	@Test
	public void serviceDescriptionTuneTest() {
		
		String formatDescription = ServiceDescriptionHelper.formatServiceDescription("60000 Km or 36 Months Service", false);
        
        Assert.assertEquals("60,000 km or 36 Months Service", formatDescription);
    }
	
	@Test
	public void serviceDescriptionHistoryTest() {
		
		String formatDescription = ServiceDescriptionHelper.formatServiceDescription("CARRY OUT 6 MONTHS/10,000KM SERVICE, TOYOTA SERVIC", false);
        
        Assert.assertEquals("CARRY OUT 6 MONTHS/10,000 km SERVICE, TOYOTA SERVIC", formatDescription);
    }
	
	@Test
	public void serviceDescriptionDisableFormatTest() {
		
		String formatDescription = ServiceDescriptionHelper.formatServiceDescription("CARRY OUT 6 MONTHS/10,000KM SERVICE, TOYOTA SERVIC", true);
        
        Assert.assertEquals("CARRY OUT 6 MONTHS/10,000KM SERVICE, TOYOTA SERVIC", formatDescription);
    }
	
	@Test
	public void serviceDescriptionSAPTest() {
		
		String formatDescription = ServiceDescriptionHelper.formatServiceDescription("30,000KM / 18 MTHS Normal", false);
        
        Assert.assertEquals("30,000 km / 18 Months Normal", formatDescription);
    }
	
	
	
	

	
	
}
