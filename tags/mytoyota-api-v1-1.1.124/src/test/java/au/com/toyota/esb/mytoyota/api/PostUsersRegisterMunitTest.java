package au.com.toyota.esb.mytoyota.api;

import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;

import au.com.toyota.esb.mytoyota.api.munit.SimplifiedMockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;


//@Ignore
public class PostUsersRegisterMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"post-register-user.xml",
				"post-register-user-rdr-technical-adapter.xml",
				"post-users-me-vehicles-technical-adapter.xml",
				"get-user-id-availability.xml",
				"post-register-user-technical-adapter.xml",
				"post-passwordpolicy-validations-technical-adapter.xml",
				"get-user-id-availability-technical-adapter.xml",
				"put-update-user-account-info-technical-adapter.xml",
				"sfdc-update-user.xml",
				"sfdc-create-prospect.xml",
				"sfdc-add-vehicle.xml",
				"create-salesforce-sessionid-soap-header.xml",
				"post-passwords-encrypt-myKit-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulRdrRequest() throws Exception {
				
		loadTheMocks();

		String payload = IOUtils.toString(getClass().getResourceAsStream("/in/post-users-registration-rdr-request.json"));
		
		MuleMessage msg = muleMessageWithPayload(payload);
		
		Map<String, Object> props = new HashMap<String,Object>();
		props.put("initiator", "rdr");
        
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
		MuleEvent event = testEvent("");
		event.setMessage(msg);
		
		MuleEvent output = runFlow("post:/users:myToyota-config", event);
		
		System.out.println("_______ output: "+ output.getMessage().getPayload());
		String expectedResponse = "{\"message\": \"Account is already active\",\"myToyotaID\": \"MYT-000004b7\"}";
		
		assertJsonEquals(expectedResponse, output.getMessage().getPayloadAsString());
		
		verifyLoggerCall("esb.mytoyota-api-v1.sfdc-update-user.request", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.sfdc-update-user.response", 1);
		
		//verify update sfdc and add vehcile haven't failed
		verifyLoggerCall("esb.mytoyota-api-v1.sfdc-update-user.error", 0);
		verifyLoggerCall("esb.mytoyota-api-v1.post-add-user-vehicle-technical-adapter.error", 0);
	}
	
	@Test
	public void testSuccessfulNonRdrRequest() throws Exception {
				
		loadTheMocks();

		String payload = IOUtils.toString(getClass().getResourceAsStream("/in/post-users-registration-rdr-request.json"));
		
		
		MuleEvent event = testEvent(payload);
		
		MuleEvent output = runFlow("post:/users:myToyota-config", event);
		
		System.out.println("_______ output: "+ output.getMessage().getPayload());
		String expectedResponse = "{\"message\": \"Account pending activation\",\"myToyotaID\": \"MYT-000004b7\"}";
		
		assertJsonEquals(expectedResponse, output.getMessage().getPayloadAsString());
		
		//verify update sfdc and add vehcile haven't failed
		verifyLoggerCall("esb.mytoyota-api-v1.sfdc-create-prospect.request", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.sfdc-create-prospect.response", 1);
		
		//verify update sfdc and add vehcile haven't failed
		verifyLoggerCall("esb.mytoyota-api-v1.sfdc-create-prospect.error", 0);
	}
	
	/*
	 * Load mocks for all endpoints
	 */
	private void loadTheMocks() throws Exception {
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("IM API check loginID available"))
				.thenApply(new SimplifiedMockResponseTransformer(200, "{\"isAvailable\":true}"));

		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("myKIT API check email login available"))
				.thenApply(new SimplifiedMockResponseTransformer(200, "{\"valid\":true}"));

		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("myKIT API POST Register Account"))
				.thenApply(new SimplifiedMockResponseTransformer(200, "{\"myToyotaID\":\"MYT-000004b7\"}"));

		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("IM API Register User"))
				.thenApply(new SimplifiedMockResponseTransformer(201, null));

		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("IM API Update User"))
				.thenApply(new SimplifiedMockResponseTransformer(201, null));

		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("IM API Finalise User Registration"))
				.thenApply(new SimplifiedMockResponseTransformer(201, null));

		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("MyKit API: POST /passwords/encrypt"))
				.thenApply(new SimplifiedMockResponseTransformer(200, "{\"encryptedPassword\": \"dfajfefafe\"}"));
		
		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Operation login"))
				.thenReturn(muleMessageWithPayload(
						IOUtils.toString(getClass().getResourceAsStream("/in/sfdc-login-response.xml"))));

		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(
						attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Operation updateOwner"))
				.thenReturn(muleMessageWithPayload(""));
		
		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(
						attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Operation registerGuestNONRDR"))
				.thenReturn(muleMessageWithPayload(""));


		whenMessageProcessor("flow-ref")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Add vehicle to user at myKit"))
				.thenApply(new SimplifiedMockResponseTransformer(201, null));
	}
	
	private void verifyLoggerCall (String category, int expectedNumberOfCalls) {
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue(category)).times(expectedNumberOfCalls);
	}
}
