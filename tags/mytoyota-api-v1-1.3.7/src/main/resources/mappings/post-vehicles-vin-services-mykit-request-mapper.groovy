import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper


def apiRequest = new JsonSlurper().parseText(payload)

//println apiRequest
if(!apiRequest)
	throw new org.mule.module.apikit.exception.BadRequestException('Invalid request')

//remove nulls
apiRequest = apiRequest.findAll {it.value != null && it.value != ""}


//validate request
if(!(apiRequest.containsKey('bookingId') &&
		apiRequest.containsKey('myToyotaId') &&
		apiRequest.containsKey('dropOffDate') &&
		apiRequest.containsKey('dropOffTime') &&
		apiRequest.containsKey('pickUpDate') &&
		apiRequest.containsKey('pickUpTime') &&
		apiRequest.containsKey('dealerDetails') &&
		apiRequest.dealerDetails.containsKey('branchCode') &&
		apiRequest.dealerDetails.containsKey('dealerId') &&
		apiRequest.dealerDetails.containsKey('dmsId') &&
		apiRequest.dealerDetails.containsKey('dmsType') &&
		apiRequest.containsKey('serviceOperation') &&
		apiRequest.serviceOperation.containsKey('description') &&
		apiRequest.serviceOperation.containsKey('externalClaimNumber') &&
		apiRequest.serviceOperation.containsKey('operationId') &&
		apiRequest.serviceOperation.containsKey('operationTypeId') &&
		apiRequest.serviceOperation.containsKey('procedureDescription')
		
	))
	throw new org.mule.module.apikit.exception.BadRequestException('Invalid request')


flowVars['myToyotaId'] = apiRequest.myToyotaId

def request = [:]


request.bookingId = apiRequest.bookingId
request.dropOffDate = apiRequest.dropOffDate
request.dropOffTime = apiRequest.dropOffTime
request.pickUpDate = apiRequest.pickUpDate
request.pickUpTime = apiRequest.pickUpTime 
request.status = (apiRequest.status)?.toLowerCase()?:'scheduled'
request.waitAtDealerFlag = apiRequest.waitAtDealer?:false
request.expressServiceFlag = apiRequest.expressServiceFlag?:false
request.courtesyBusFlag = apiRequest.courtesyBusFlag?:false
request.loanVehicleFlag = apiRequest.loanVehicleFlag?:false
request.notes = apiRequest.notes
request.alertType = (apiRequest.confirmationType)?.toLowerCase() 
request.totalServicePrice = apiRequest.totalServicePrice

dealerMap = [:]
dealerMap.dealerId = apiRequest.dealerDetails.dealerId
dealerMap.branchId = apiRequest.dealerDetails.branchCode
dealerMap.dmsID = apiRequest.dealerDetails.dmsId
dealerMap.dmsType = apiRequest.dealerDetails.dmsType

//remove nulls
dealerMap = dealerMap.findAll {it.value != null && it.value != ""}

request.dealer = dealerMap


if(apiRequest.contactDetail) {
	contactDetailMap = [:]
	contactDetailMap.firstName = apiRequest.contactDetail.firstName
	contactDetailMap.lastName = apiRequest.contactDetail.lastName
	contactDetailMap.email = apiRequest.contactDetail.email
	contactDetailMap.phone = apiRequest.contactDetail.phone
	//remove nulls
	contactDetailMap = contactDetailMap.findAll {it.value != null && it.value != ""}

	if(contactDetailMap)
		request.contactDetail = contactDetailMap
}

vehicleMap = [:]
vehicleMap.vin = flowVars['vin']
request.vehicle = vehicleMap

serviceOperationMap = [:]
serviceOperationMap.operationTypeId = apiRequest.serviceOperation.operationTypeId
serviceOperationMap.operationId = apiRequest.serviceOperation.operationId
serviceOperationMap.description = apiRequest.serviceOperation.description
serviceOperationMap.tsaFlag = apiRequest.serviceOperation.tsaFlag?:false
//serviceOperationMap.price  = apiRequest.serviceOperation.serviceOperationPrice
serviceOperationMap.externalClaimNumber = apiRequest.serviceOperation.externalClaimNumber
serviceOperationMap.fixedChargeOperationFlag = apiRequest.serviceOperation.fixedChargeOperationFlag?:false
serviceOperationMap.cappedPriceOperationFlag = apiRequest.serviceOperation.cappedPriceOperationFlag?:false
serviceOperationMap.procedureDescription = apiRequest.serviceOperation.procedureDescription
serviceOperationMap.totalOperationTime = apiRequest.serviceOperation.totalOperationTime



//remove nulls
serviceOperationMap = serviceOperationMap.findAll {it.value != null && it.value != ""}

if(serviceOperationMap)
	request.serviceOperation = serviceOperationMap

//request.odometer = apiRequest.vehicleOdo

//remove nulls
request = request.findAll {it.value != null && it.value != ""}

return prettyPrint(toJson(request))
