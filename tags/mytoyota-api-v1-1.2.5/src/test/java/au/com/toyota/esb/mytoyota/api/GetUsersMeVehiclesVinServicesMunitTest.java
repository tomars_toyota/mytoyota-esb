package au.com.toyota.esb.mytoyota.api;

import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.util.IOUtils;
import org.mule.util.StringUtils;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;


public class GetUsersMeVehiclesVinServicesMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"get-users-me-vehicles-vin-services.xml",
				"get-users-me-services-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulGetServiceBooking() throws Exception {
		
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("MyKit API: GET /users/{userLogin}/services"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							newMessage = originalMessage.createInboundMessage();

							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);

							newMessage.setPayload(getClass().getResourceAsStream(
									"/in/get-users-me-vehicles-vin-services-myKit-response.json"));
						} catch (Exception e) {
							// catch exception statements
						}

						return newMessage;
					}
				});
		
		
		MuleEvent event = testEvent("");
		
		event.setFlowVariable("vin", "5TDBK3FH80S016123");
		
		Map<String, Object> props = new HashMap<String,Object>();
		props.put("status", "quoted");
		
		MuleMessage msg = muleMessageWithPayload(null);
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
        
        event.setMessage(msg);		
		
		// Invoke the flow
		MuleEvent output = runFlow("get:/users/me/vehicles/{vin}/services:myToyota-config", event);
		
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.get-users-me-vehciles-vin-services.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.get-users-me-vehciles-vin-services.response"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.get-users-me-services-technical-adapter.request"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.get-users-me-services-technical-adapter.response"))
				.times(1);
		
		System.out.println("_____________________"+output.getMessage().getPayloadAsString());
		String expectedResponse = IOUtils
				.toString(getClass().getResourceAsStream("/out/get-users-me-vehicles-vin-services-response.json"));
		assertJsonEquals(expectedResponse, output.getMessage().getPayloadAsString());
	}
	
	@Test
	public void testFailuresStatusCodes() throws Exception {
		
		int[] statusCodes = new int[] { 404, 405, 415, 406, 401, 500 };	
		
		MuleEvent event = testEvent("");
		
		Map<String, Object> props = new HashMap<String,Object>();
		props.put("status", "quoted");
		
		MuleMessage msg = muleMessageWithPayload(null);
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
        
        event.setMessage(msg);	
        				
		for (int i = 0; i < statusCodes.length; i++) {
			new StatusCodesTestingHelper().runFailureTest(statusCodes[i], "MyKit API: GET /users/{userLogin}/services",
					"get:/users/me/vehicles/{vin}/services:myToyota-config", event);
		}
	}
}
