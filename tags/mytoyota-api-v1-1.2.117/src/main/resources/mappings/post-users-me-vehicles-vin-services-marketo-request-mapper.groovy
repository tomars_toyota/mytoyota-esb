import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper
import java.text.SimpleDateFormat
import java.text.DecimalFormat

def formatCost(value) {
    pattern = "#####.00"
    moneyformat = new DecimalFormat(pattern)
    
    if(value <= 0)
        return value
    else {
        output = moneyformat.format(value)
        if(output.endsWith('.00'))
        	//remove '.00'
            return output.substring(0, output.length() - 3)
        else    
            return output
    }
}

def booleanNull(value) {
	return (value ? value : false)
}


JsonSlurper js = new JsonSlurper()
//println '___________ apiRequest: '+sessionVars['apiRequest']
def apiRequest = js.parseText(sessionVars['apiRequest'])

//remove nulls
apiRequest = apiRequest.findAll {it.value != null && it.value != ""}

def vehicle = js.parseText(sessionVars['vehicleResponse'])


def requestMap = [:]

requestMap.My_Toyota_Id__c = flowVars['myToyotaId']


requestMap.firstName = apiRequest.contactDetails?.firstName
requestMap.lastName = apiRequest.contactDetails?.lastName
requestMap.email = apiRequest.contactDetails?.email
requestMap.mobilePhone = apiRequest.contactDetails?.phone

requestMap.serviceBookingDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date())
requestMap.serviceBookingVehicleModel = vehicle?.vehicleLine?.description
requestMap.serviceBookingVehicleRego = apiRequest?.registrationNumber
requestMap.serviceBookingVehicleVIN = flowVars['vin']
requestMap.serviceBookingNumber = flowVars['dmsBookingId']

//adding description to service booking quote 
if(apiRequest?.serviceOperationPrice == null || apiRequest?.serviceOperationPrice == '' || (formatCost(apiRequest?.serviceOperationPrice) == 0 && !booleanNull(apiRequest.isComplimentary)) ) 
requestMap.serviceBookingQuote = "Pricing cannot be displayed. Please contact your preferred dealer for further details**."
else if(booleanNull(apiRequest.isComplimentary)) requestMap.serviceBookingQuote = formatCost(apiRequest.serviceOperationPrice) + " (Complimentary Vehicle Inspection***)"
else if(apiRequest.serviceTsaFlag) requestMap.serviceBookingQuote = formatCost(apiRequest.serviceOperationPrice) + " (Toyota Service Advantage*)"
else requestMap.serviceBookingQuote = formatCost(apiRequest.serviceOperationPrice) + " (Toyota Service Guarantee**)"


requestMap.serviceBookingTypeofService = apiRequest?.description
requestMap.serviceBookingDropOffDate = apiRequest?.dropOffDate ? apiRequest.dropOffDate : null
requestMap.serviceBookingDropOffTime = apiRequest?.dropOffTime ? new SimpleDateFormat("hh:mm a").format(new Date().parse('HH:mm', apiRequest.dropOffTime)) : null
requestMap.serviceBookingPickUpDate = apiRequest?.pickUpDate ? apiRequest.pickUpDate : null
requestMap.serviceBookingPickUpTime = apiRequest?.pickUpTime ? new SimpleDateFormat("hh:mm a").format(new Date().parse('HH:mm', apiRequest.pickUpTime)) : null

dealer = js.parseText(sessionVars['dealerResponse'])
requestMap.serviceBookingDealerName = dealer?.dealerName


dealerSite = dealer?.sites.find { it.sapCode == apiRequest.branchCode }
requestMap.serviceBookingDealerPhoneNumber = dealerSite?.phone?.main

requestMap.serviceBookingDealerPostCode = dealerSite?.address?.postCode
requestMap.serviceBookingDealerState = dealerSite?.address?.state
requestMap.serviceBookingDealerAddress = dealerSite?.address?.streetAddress
requestMap.serviceBookingDealerSuburb = dealerSite?.address?.suburb

requestMap.serviceBookingDealerEmail = dealerSite?.email?.service ?: dealerSite?.email?.main
requestMap.serviceBookingDealerWebsite = dealerSite?.webSite

requestMap.serviceBookingCustomerComments = apiRequest?.notes


request = ['input':[requestMap]]

return prettyPrint(toJson(request))