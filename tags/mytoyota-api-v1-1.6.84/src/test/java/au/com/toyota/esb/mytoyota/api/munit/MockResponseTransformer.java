package au.com.toyota.esb.mytoyota.api.munit;

import org.mule.api.MuleMessage;
import org.mule.modules.interceptor.processors.MuleMessageTransformer;
import org.mule.munit.runner.functional.FunctionalMunitSuite;

public class MockResponseTransformer extends FunctionalMunitSuite implements MuleMessageTransformer {
	int count = 0;
	String[] payloads;

	public MockResponseTransformer(String... payloads) {
		this.payloads = payloads;
	}

	@Override
	public MuleMessage transform(MuleMessage originalMessage) {
		return muleMessageWithPayload(payloads[count++]);
	}
}