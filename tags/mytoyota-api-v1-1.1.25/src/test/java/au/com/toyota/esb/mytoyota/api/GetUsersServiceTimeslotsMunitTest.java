package au.com.toyota.esb.mytoyota.api;

import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.HashMap;
import java.util.Map;

import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.util.IOUtils;
import org.mule.util.StringUtils;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;


/*
 * Munit test to unit test the functionality to get service dropoff/pickup timeslots through 
 * /users/me/vehicles/{vin}/services/dropOffSlots and
 * /users/me/vehicles/{vin}/services/pickUpSlots
 * @author: ahuwait
 */
public class GetUsersServiceTimeslotsMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"get-users-me-vehicles-vin-services-timeslots.xml",
				"get-users-me-vehicles-vin-services-timeslots-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulGetServiceDropoff() throws Exception {
		testSuccessfulGetServiceDropoffImpl("TUNE");
	}
	
	@Test
	public void testSuccessfulGetServiceDropoffTuneLowerCase() throws Exception {
		testSuccessfulGetServiceDropoffImpl("tune");
	}
	
	private void testSuccessfulGetServiceDropoffImpl(String dmsType) throws Exception {
		
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
			attribute("name").ofNamespace("doc").withValue("TUNE API: GET /gettimeslots"))
			.thenApply(new MockResponseTransformer() {
				@Override
				public MuleMessage transform(MuleMessage originalMessage) {
					MuleMessage newMessage = null;
					
					try {
						newMessage = originalMessage.createInboundMessage();
						
						newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
						newMessage.setProperty("Content-Type", "application/json;charset=UTF-8", PropertyScope.INBOUND);
						
						String tuneResponse = IOUtils.toString(getClass().getResourceAsStream("/in/get-users-me-vehicles-vin-services-timeslots-tune-response.json"));
						newMessage.setPayload(IOUtils.toInputStream(tuneResponse, "UTF-8"));
					} catch (Exception e) {
						// catch exception statements
					}
					
					return newMessage;
				}
			});
		
		MuleEvent event = testEvent("");
		
				
		Map<String, Object> props = new HashMap<String,Object>();
		props.put("totalOperationTime", "1.5");
        props.put("dmsType", dmsType);
		
		
		MuleMessage msg = muleMessageWithPayload(null);
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
        
        event.setMessage(msg);		
		
		// Invoke the flow
		MuleEvent output = runFlow("get:/users/me/vehicles/{vin}/services/dropOffSlots:myToyota-config", event);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Request - dropoff")).times(1);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Response - dropoff")).times(1);	

		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Request - dropoff tech adapter")).times(1);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Response - dropoff tech adapter")).times(1);	

		
//		System.out.println("_____________________"+output.getMessage().getPayloadAsString());
		String expectedResponse = IOUtils.toString(getClass().getResourceAsStream("/out/get-users-me-vehicles-vin-services-timeslots-api-response.json"));
		assertJsonEquals(expectedResponse, output.getMessage().getPayloadAsString());

	}
	
	@Test
	public void testNonTuneGetServiceDropoff() throws Exception {
		
		MuleEvent event = testEvent("");
		
		Map<String, Object> props = new HashMap<String,Object>();
		props.put("dmsType", "nonTune");
		
		MuleMessage msg = muleMessageWithPayload(null);
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
        
        event.setMessage(msg);		
		
		// Invoke the flow
        runFlow("get:/users/me/vehicles/{vin}/services/dropOffSlots:myToyota-config", event);
        
        verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Request - dropoff")).times(1);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Response - dropoff")).times(1);	

		verifyCallOfMessageProcessor("logger").withAttributes(attribute("name").ofNamespace("doc").withValue("Extend with Other DMS for dropoff")).times(1);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Request - dropoff tech adapter")).times(0);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Response - dropoff tech adapter")).times(0);				
	}
	
	@Test
	public void testSuccessfulGetServicePickupl() throws Exception {
		testSuccessfulGetServicePickupImpl("TUNE");
	}
	
	@Test
	public void testSuccessfulGetServicePickupTuneLowerCasel() throws Exception {
		testSuccessfulGetServicePickupImpl("tune");
	}
	
	private void testSuccessfulGetServicePickupImpl(String dmsType) throws Exception {
		
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
			attribute("name").ofNamespace("doc").withValue("TUNE API: GET /getoutslots"))
			.thenApply(new MockResponseTransformer() {
				@Override
				public MuleMessage transform(MuleMessage originalMessage) {
					MuleMessage newMessage = null;
					
					try {
						newMessage = originalMessage.createInboundMessage();
						
						newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
						newMessage.setProperty("Content-Type", "application/json;charset=UTF-8", PropertyScope.INBOUND);
						
						String tuneResponse = IOUtils.toString(getClass().getResourceAsStream("/in/get-users-me-vehicles-vin-services-timeslots-tune-response.json"));
						newMessage.setPayload(IOUtils.toInputStream(tuneResponse, "UTF-8"));
					} catch (Exception e) {
						// catch exception statements
					}
					
					return newMessage;
				}
			});
		
		MuleEvent event = testEvent("");
		
				
		Map<String, Object> props = new HashMap<String,Object>();
		props.put("totalOperationTime", "1.5");
        props.put("dropOffSlotSeq", "3");
        props.put("dropOffTime", "08:00");
        props.put("dropOffDate", "2016-04-20");
        props.put("dmsType", dmsType);
		
		
		MuleMessage msg = muleMessageWithPayload(null);
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
        
        event.setMessage(msg);		
		
		// Invoke the flow
		MuleEvent output = runFlow("get:/users/me/vehicles/{vin}/services/pickUpSlots:myToyota-config", event);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Request - pickup")).times(1);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Response - pickup")).times(1);	

		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Request - pickup tech adapter")).times(1);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Response - pickup tech adapter")).times(1);	

		
//		System.out.println("_____________________"+output.getMessage().getPayloadAsString());
		String expectedResponse = IOUtils.toString(getClass().getResourceAsStream("/out/get-users-me-vehicles-vin-services-timeslots-api-response.json"));
		assertJsonEquals(expectedResponse, output.getMessage().getPayloadAsString());

	}
	
	@Test
	public void testNonTuneGetServicePickup() throws Exception {
		
		MuleEvent event = testEvent("");
		
		Map<String, Object> props = new HashMap<String,Object>();
		props.put("dmsType", "nonTune");
		
		MuleMessage msg = muleMessageWithPayload(null);
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
        
        event.setMessage(msg);		
		
		// Invoke the flow
        runFlow("get:/users/me/vehicles/{vin}/services/pickUpSlots:myToyota-config", event);
        
        verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Request - pickup")).times(1);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Response - pickup")).times(1);	

		verifyCallOfMessageProcessor("logger").withAttributes(attribute("name").ofNamespace("doc").withValue("Extend with Other DMS for pickup")).times(1);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Request - pickup tech adapter")).times(0);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Response - pickup tech adapter")).times(0);				
	}
	
	@Test
	public void testDropOffFailuresStatusCodes() throws Exception {
		
		int[] statusCodes = new int[] { 404, 405, 415, 406, 401, 500 };	
		
		MuleEvent event = testEvent("");
		
		Map<String, Object> props = new HashMap<String,Object>();
		props.put("totalOperationTime", "1.5");
        props.put("dmsType", "TUNE");
		
		
		MuleMessage msg = muleMessageWithPayload(null);
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
        
        event.setMessage(msg);				
		for (int i = 0; i < statusCodes.length; i++) {
			new StatusCodesTestingHelper().runFailureTest(statusCodes[i], "TUNE API: GET /gettimeslots",
					"get:/users/me/vehicles/{vin}/services/dropOffSlots:myToyota-config", event);
		}
	}
	
	@Test
	public void testPickUpFailuresStatusCodes() throws Exception {
		
		int[] statusCodes = new int[] { 404, 405, 415, 406, 401, 500 };	
		
		MuleEvent event = testEvent("");
		
		Map<String, Object> props = new HashMap<String,Object>();
		props.put("totalOperationTime", "1.5");
        props.put("dropOffSlotSeq", "3");
        props.put("dropOffTime", "08:00");
        props.put("dropOffDate", "2016-04-20");
        props.put("dmsType", "TUNE");
		
		MuleMessage msg = muleMessageWithPayload(null);
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
        
        event.setMessage(msg);				
		for (int i = 0; i < statusCodes.length; i++) {
			new StatusCodesTestingHelper().runFailureTest(statusCodes[i], "TUNE API: GET /getoutslots",
					"get:/users/me/vehicles/{vin}/services/pickUpSlots:myToyota-config", event);
		}
	}
}
