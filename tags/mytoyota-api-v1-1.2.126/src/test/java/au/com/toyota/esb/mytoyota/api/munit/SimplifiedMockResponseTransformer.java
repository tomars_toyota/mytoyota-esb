package au.com.toyota.esb.mytoyota.api.munit;

import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;

public class SimplifiedMockResponseTransformer extends MockResponseTransformer {

	private int httpStatus;
	private String payload;

	public SimplifiedMockResponseTransformer(int httpStatus, String payload) {
		super();
		this.httpStatus = httpStatus;
		this.payload = payload;
	}

	@Override
	public MuleMessage transform(MuleMessage originalMessage) {
		MuleMessage newMessage = null;

		try {
			newMessage = originalMessage.createInboundMessage();

			newMessage.setProperty("http.status", httpStatus, PropertyScope.INBOUND);
			if (payload != null)
				newMessage.setPayload(payload);
		} catch (Exception e) {
			// catch exception statements
		}

		return newMessage;
	}

}
