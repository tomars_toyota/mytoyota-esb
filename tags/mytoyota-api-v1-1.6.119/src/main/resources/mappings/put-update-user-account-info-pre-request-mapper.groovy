// Pre-mapper to ensure:
// - don't pass through firstname or lastname if provided from SF blank (mandatory myKIT fields)
// - don't pass through email, as change of email is not currently supported
// - if an address is passed without a reference ID, then set the refID to 'other' because
//   the old address' reference ID stays in myKIT causing the front-ends to lookup the address based on this ID.

import groovy.json.JsonSlurper
import groovy.json.JsonBuilder

def payloadMap = new JsonSlurper().parseText(payload)

// Don't pass through firstname if provided from SF blank (mandatory myKIT fields)
if (!(payloadMap?.firstName))
    payloadMap.remove('firstName')

// Don't pass through lastname if provided from SF blank (mandatory myKIT fields)
if (!(payloadMap?.lastName))
    payloadMap.remove('lastName')

// Don't pass through email, as change of email is not currently supported
if (payloadMap?.email || payloadMap?.email?.length() == 0)
    payloadMap.remove('email')

	 
return new JsonBuilder(payloadMap).toString()
