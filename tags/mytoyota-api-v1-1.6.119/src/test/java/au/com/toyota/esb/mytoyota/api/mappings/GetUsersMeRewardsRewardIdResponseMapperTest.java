package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;
import java.util.Properties;

import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

public class GetUsersMeRewardsRewardIdResponseMapperTest extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;
	private Properties props;

	public GetUsersMeRewardsRewardIdResponseMapperTest() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/get-users-me-rewards-rewardId-response-mapper.groovy");
		//load properties
		props = new Properties();
		props.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));
	}

	@Test
	public void testSuccessfulRequestMapper() throws Exception {
		
		String sitecoreHost = props.getProperty("sitecore-app.host");
		
		String result = ScriptRunBuilder.runner(scriptRunner)
				.payload(IOUtils.toString(
						getClass().getResourceAsStream("/in/get-users-me-rewards-details-sitecore-response.json")))
				.flowVar("rewardDetailsPayload", IOUtils.toString(
						getClass().getResourceAsStream("/in/get-users-me-rewards-details-sitecore-response.json")))
				.binding("sitecoreHost", sitecoreHost)
				.run().toString();

		System.out.println(result);
		
		// expected
		String expectedResult = IOUtils
				.toString(getClass().getResourceAsStream("/out/get-users-me-rewards-rewardId-api-response.json"));
		JSONAssert.assertEquals(expectedResult, result, true);
	}
}
