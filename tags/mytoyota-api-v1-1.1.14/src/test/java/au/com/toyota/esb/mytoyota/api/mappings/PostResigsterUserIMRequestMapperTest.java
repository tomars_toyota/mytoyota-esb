package au.com.toyota.esb.mytoyota.api.mappings;

import groovy.json.JsonSlurper;

import java.io.IOException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

import javax.script.*;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;


public class PostResigsterUserIMRequestMapperTest extends AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;
	
	public PostResigsterUserIMRequestMapperTest() throws ScriptException, SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/post-register-user-im-provider-request-mapper.groovy");
	}
	
	@Test
	public void testRequestMapper() throws Exception {
		JsonSlurper jsonParser = new JsonSlurper();
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload("").flowVar("requestPayload", jsonParser.parseText(IOUtils.toString(getClass().getResourceAsStream(
						"/in/post-register-user-request.json"))))
				.run().toString();
		System.out.println(result);
		
		// expected
		String expectedResult = IOUtils.toString(getClass().getResourceAsStream(
				"/out/post-register-user-im-request.json"));
		JSONAssert.assertEquals(expectedResult, result, true);
	}
	
}
