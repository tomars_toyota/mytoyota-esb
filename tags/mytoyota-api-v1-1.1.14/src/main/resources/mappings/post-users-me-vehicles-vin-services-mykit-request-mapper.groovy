import groovy.json.JsonBuilder
import groovy.json.JsonSlurper
import java.text.SimpleDateFormat


JsonSlurper js = new JsonSlurper()
//println '___________ apiRequest: '+sessionVars['apiRequest']
def apiRequest = js.parseText(sessionVars['apiRequest'])
if(!apiRequest)
	throw new org.mule.module.apikit.exception.BadRequestException('Invalid request')

//remove nulls
apiRequest = apiRequest.findAll {it.value != null && it.value != ""}

//validate request
if(!(apiRequest.containsKey('dealerId') &&
		apiRequest.containsKey('branchCode') &&
		apiRequest.containsKey('operationTypeID') &&
		apiRequest.containsKey('operationID')
	))
	throw new org.mule.module.apikit.exception.BadRequestException('Invalid request')



def request = [:]


request.bookingId = flowVars['dmsBookingId']
request.dropOffDate = apiRequest['dropOffDate']
request.dropOffTime = apiRequest['dropOffTime']
request.pickUpDate = apiRequest['pickUpDate']
request.pickUpTime = apiRequest['pickUpTime'] 
request.status = 'scheduled'
request.notes = apiRequest['notes']
request.alertType = (apiRequest['confirmationType'])?.toLowerCase() 
request.totalServicePrice = apiRequest['totalServicePrice']
request.waitAtDealerFlag = apiRequest['waitAtDealerFlag']?:false
request.expressServiceFlag = apiRequest['expressServiceFlag']?:false
request.courtesyBusFlag = apiRequest['courtesyBusFlag']?:false
request.loanVehicleFlag = apiRequest['loanVehicleFlag']?:false

dealerMap = [:]
dealerMap.dealerId = apiRequest['dealerId']
dealerMap.branchId = apiRequest['branchCode']
dealerMap.dmsID = flowVars['dmsID']
dealerMap.dmsType = flowVars['dmsType']

//remove nulls
dealerMap = dealerMap.findAll {it.value != null && it.value != ""}

request.dealer = dealerMap


if(apiRequest.contactDetails) {
	contactDetailMap = [:]
	contactDetailMap.firstName = apiRequest.contactDetails['firstName']
	contactDetailMap.lastName = apiRequest.contactDetails['lastName']
	contactDetailMap.email = apiRequest.contactDetails['email']
	contactDetailMap.phone = apiRequest.contactDetails['phone']
	//remove nulls
	contactDetailMap = contactDetailMap.findAll {it.value != null && it.value != ""}

	if(contactDetailMap)
		request.contactDetail = contactDetailMap
}

vehicleMap = [:]
vehicleMap.vin = flowVars['vin']

request.vehicle = vehicleMap


serviceOperationMap = [:]
serviceOperationMap.operationTypeId = apiRequest['operationTypeID']
serviceOperationMap.operationId = apiRequest['operationID']
serviceOperationMap.description = apiRequest['description']
serviceOperationMap.tsaFlag = apiRequest['serviceTsaFlag']?:false
serviceOperationMap.price  = apiRequest['serviceOperationPrice']
serviceOperationMap.externalClaimNumber = apiRequest['externalNumberOfClaim']
serviceOperationMap.fixedChargeOperationFlag = apiRequest['fixedChargeOperationFlag']?:false
serviceOperationMap.cappedPriceOperationFlag = apiRequest['cappedPriceOperationFlag']?:false
serviceOperationMap.procedureDescription = apiRequest['inclusions']
serviceOperationMap.totalOperationTime = apiRequest['totalOperationTime']
//remove nulls
serviceOperationMap = serviceOperationMap.findAll {it.value != null && it.value != ""}


if(serviceOperationMap)
	request.serviceOperation = serviceOperationMap

//remove nulls
request = request.findAll {it.value != null && it.value != ""}

return new JsonBuilder(request).toString()
