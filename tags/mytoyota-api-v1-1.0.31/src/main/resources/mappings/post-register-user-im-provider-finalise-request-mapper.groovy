import groovy.json.JsonBuilder
def requestPayload = flowVars['requestPayload']

def payload = [:]
// myToyotaId returned back from Fabrics myToyota backend
def applicationIdentifiers = ['myToyota':['id':flowVars['myToyotaID']]]
payload.applicationIdentifiers = applicationIdentifiers

// map accepted TC version
if(requestPayload?.termsAndConditions?.acceptedVersion?.length() > 0) {
	def termsAndConditions = ['acceptedVersion':requestPayload?.termsAndConditions?.acceptedVersion]
	payload.termsAndConditions = termsAndConditions
}

// map accepted Privacy Statement version
if(requestPayload?.privacyPolicy?.acceptedVersion?.length() > 0) {
	def privacyPolicy = ['acceptedVersion':requestPayload?.privacyPolicy?.acceptedVersion]
	payload.privacyPolicy = privacyPolicy
}

return payload

// won't map these, don't want this much customer data in gigya
// profile.lastName = requestPayload?.lastName ?: ''
// profile."phones.type" = "mobile"
// profile."phones.number" = requestPayload?.mobile ?: ''

//"termsAndConditions": {
//	"versionAccepted": "1.0"
//  }
//  "privacyPolicy": {
//	"versionAccepted": "1.0"
//  }