package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

import javax.script.*;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.DefaultMuleMessage;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.context.DefaultMuleContextFactory;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

public class GetVehiclesVinServicesQuoteResponseMapperTest extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;

	public GetVehiclesVinServicesQuoteResponseMapperTest() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner
				.createScriptRunner("/mappings/get-vehicles-vin-services-quote-response-mapper.groovy");
	}
	
	@Test
	public void testSuccessful() throws Exception {
		String expectedResult = IOUtils.toString(
				getClass().getResourceAsStream("/out/get-vehicles-vin-services-quote-response.json"));
		
		//prepare query parameters 
		muleContext = new DefaultMuleContextFactory().createMuleContext();
		MuleMessage message = new DefaultMuleMessage(null,muleContext);
		HashMap<String, String> queryParams = new HashMap<String, String>();
		queryParams.put("dealerId", "37425");
		queryParams.put("branchCode", "37425");
		queryParams.put("kilometres", "30000");
		
		message.setProperty("http.query.params", queryParams, PropertyScope.INBOUND);
		
		//load properties
		Properties props = new Properties();
		props.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));
		
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(null)
				.message(message)
				.binding("pdfServiceUrl", props.getProperty("api.service.quote.pdf.url"))
				.flowVar("vin","6T1BD3FKX0X144234")
				.flowVar("dmsType","TUNE")
				.flowVar("serviceId", "TB16392302")
				.run().toString();

		System.out.println(result);
		JSONAssert.assertEquals(expectedResult, result, true);
	}
}
