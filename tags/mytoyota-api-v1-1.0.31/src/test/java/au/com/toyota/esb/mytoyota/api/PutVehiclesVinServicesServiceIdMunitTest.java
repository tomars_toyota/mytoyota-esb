package au.com.toyota.esb.mytoyota.api;

import org.junit.Test;

import org.mule.util.StringUtils;
import org.mule.util.IOUtils;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.modules.interceptor.processors.MuleMessageTransformer;

import static org.mule.munit.common.mocking.Attribute.attribute;


public class PutVehiclesVinServicesServiceIdMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"put-vehicles-vin-services-serviceId.xml",
				"put-vehicles-vin-services-serviceId-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulUpdateServiceBooking() throws Exception {
		
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
				attribute("name").ofNamespace("doc").withValue("MyKit API: PUT /services/{id}"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						
						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("Content-Type", "application/json;charset=UTF-8", PropertyScope.INBOUND);
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							newMessage.setPayload("");
						} catch (Exception e) {
							// catch exception statements
						}
						
						return newMessage;
					}
				});
		
		
		MuleEvent event = testEvent("");
		event.setFlowVariable("vin", "6T153BK360X070769");
		event.setFlowVariable("dmsType", "TUNE");
		event.setFlowVariable("dmsID", "61.10169.10169011116");
		event.setFlowVariable("serviceId", "TB16392444");
		
		
		MuleMessage msg = muleMessageWithPayload(IOUtils.toString(getClass()
				.getResourceAsStream("/in/put-vehicles-vin-services-serviceId-request.json")));
		
        event.setMessage(msg);		
		
		// Invoke the flow
		runFlow("put:/vehicles/{vin}/services/{serviceId}:myToyota-config", event);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.put-vehciles-vin-service.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.put-vehciles-vin-service.response"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.put-vehciles-vin-service-technical-adapter.request"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.put-vehciles-vin-service-technical-adapter.response"))
				.times(1);
	}
	
	//not possible to externalise this class because of the visibility of the method muleMessageWithPayload
	private class MockResponseTransformer implements MuleMessageTransformer {
		int count = 0;
		String[] payloads;

		public MockResponseTransformer(String... payloads) {
			this.payloads = payloads;
		}

		@Override
		public MuleMessage transform(MuleMessage originalMessage) {
			return muleMessageWithPayload(payloads[count++]);
		}
	}
	
}
