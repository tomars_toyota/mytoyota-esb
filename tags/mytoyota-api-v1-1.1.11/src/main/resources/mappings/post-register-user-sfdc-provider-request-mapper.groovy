import groovy.xml.MarkupBuilder
import groovy.json.JsonSlurper

def user = new JsonSlurper().parseText(payload)
				
def writer = new StringWriter()
def xmlMarkup = new MarkupBuilder(writer)

//def rootEle =
xmlMarkup.mkp.xmlDeclaration(version: "1.0", encoding: "UTF-8")
xmlMarkup.'sp:registerOwner' (
	'xmlns:sp' : "http://soap.sforce.com/schemas/class/SP_UserRegWSTransactionStructure"
	) {
		'sp:request'{
			'sp:batchNumber' (user?.applicationIdentifiers?.sap?.batchId)
			'sp:myToyotaId' (flowVars['myToyotaId'])
			'sp:ownerDetails' {
				if(user?.dateOfBirth)
					'sp:dob' (user.dateOfBirth)
				if(user?.email)
					'sp:email' (user.email)
				if(user?.firstName)
					'sp:firstName' (user.firstName)
				if(user?.landline)
					'sp:landline' (user.landline)
				if(user?.lastName)
					'sp:lastName' (user.lastName)
				if(user?.mobile)
					'sp:mobilePhone' (user.mobile)
				if(user?.title)
					'sp:title' (user.title)
			}
			if(user?.address?.postal) {
				'sp:postalAddress' {
					if(user.address.postal.suburb)
						'sp:cityOrSuburb' (user.address.postal.suburb)
					if(user.address.postal.postcode)
						'sp:postCode' (user.address.postal.postcode)
					if(user.address.postal.state)
						'sp:state' (user.address.postal.state)
					if(user.address.postal.streetAddress)
						'sp:streetAddress1' (user.address.postal.streetAddress)
				}
			}
			if(user?.address?.residential) {
				'sp:residentialAddress' {
					if(user.address.residential.suburb)
						'sp:cityOrSuburb' (user.address.residential.suburb)
					if(user.address.residential.postcode)
						'sp:postCode' (user.address.residential.postcode)
					if(user.address.residential.state)
						'sp:state' (user.address.residential.state)
					if(user.address.residential.streetAddress)
						'sp:streetAddress1' (user.address.residential.streetAddress)
				}
			}
			'sp:VIN' (flowVars['vin'])
		}	
	}
return writer.toString()
