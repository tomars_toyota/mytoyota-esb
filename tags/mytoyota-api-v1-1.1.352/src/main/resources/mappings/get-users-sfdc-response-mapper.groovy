import groovy.util.XmlSlurper
import static groovy.json.JsonOutput.*

def userResponse = new XmlSlurper().parseText(payload)?.result

user = [:]
if(sessionVars['guestPreferences']) {
	// Map preferences
	if (userResponse?.marketingPreference.text()) {
		user.globalUnsubscribe = userResponse?.marketingPreference.text()
	}
	if (userResponse?.guestPreferences?.goPlacesPreference.text()) {
		user.goPlacesPreference = userResponse?.guestPreferences?.goPlacesPreference.text()
	}
	if (userResponse?.guestPreferences?.pnpOptIn.text()) {
		user.prospectNurtureOptIn = userResponse?.guestPreferences?.pnpOptIn.text()
	}
	if (userResponse?.guestPreferences?.emailOptInTMC.text()) {
		user.trackMyCarEmailOptIn = userResponse?.guestPreferences?.emailOptInTMC.text()
	}
	if (userResponse?.guestPreferences?.smsOptInTMC.text()) {
		user.trackMyCarSMSOptIn = userResponse?.guestPreferences?.smsOptInTMC.text()
	}
}
else {
	user.firstName = userResponse?.ownerDetails?.firstName.text()
	user.lastName = userResponse?.ownerDetails?.lastName.text()
	user.email = userResponse?.ownerDetails?.email.text()
}



return prettyPrint(toJson(user))

// <?xml version="1.0" encoding="UTF-8"?>
// <retrieveGuestInformationResponse xmlns="http://soap.sforce.com/schemas/class/SP_UserRegWSTransactionStructure" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
//    <result>
//       <accountId xsi:nil="true"/>
//       <guestPreferences>
//          <emailOptInTMC>true</emailOptInTMC>
//          <goPlacesPreference>Both Hard And Soft Copy</goPlacesPreference>
//          <pnpOptIn>true</pnpOptIn>
//          <smsOptInTMC>true</smsOptInTMC>
//       </guestPreferences>
//       <marketingPreference>true</marketingPreference>
//       <myToyotaId xsi:nil="true"/>
//       <ownerDetails>
//          <cmUser xsi:nil="true"/>
//          <competitions xsi:nil="true"/>
//          <dob xsi:nil="true"/>
//          <email>gary@jobb.com</email>
//          <firstName>Gary</firstName>
//          <goPlacesviaApp xsi:nil="true"/>
//          <goPlacesviaMagazine xsi:nil="true"/>
//          <landline xsi:nil="true"/>
//          <lastName>Job</lastName>
//          <mobilePhone xsi:nil="true"/>
//          <OffersPromotions xsi:nil="true"/>
//          <preferredServicingDealer xsi:nil="true"/>
//          <receivePushNotification xsi:nil="true"/>
//          <receiveToyotaCommunication xsi:nil="true"/>
//          <serviceNotices xsi:nil="true"/>
//          <surveys xsi:nil="true"/>
//          <takeABreak xsi:nil="true"/>
//          <title xsi:nil="true"/>
//          <vehicleProductAnnouncements xsi:nil="true"/>
//          <vehiclesInterested xsi:nil="true"/>
//       </ownerDetails>
//       <responseMessage>SPMyToyota002; Guest Information Retrieved Successfully.</responseMessage>
//       <resultStatus>SUCCESS</resultStatus>
//    </result>
// </retrieveGuestInformationResponse>