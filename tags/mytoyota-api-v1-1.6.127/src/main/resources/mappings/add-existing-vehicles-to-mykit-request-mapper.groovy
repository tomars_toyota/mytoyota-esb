import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper
import au.com.toyota.esb.mytoyota.api.util.GroovyHelper

def request = new JsonSlurper().parseText(payload)

// myKIT takes list as input
def vehicles = []

request?.vehicleOwnerships.each { vehicleRequest ->
	
	def vehicle = [:]
	vehicle.batchNumber = vehicleRequest?.batchNumber
	vehicle.vin = vehicleRequest?.vin
	vehicle.deliveryDate = vehicleRequest?.rdrDate
	vehicle.registrationNumber = vehicleRequest?.registrationNumber

	//vehicle user
	vehicleUserMap = [:]
	vehicleUserMap.relationshipTypes = ["OWNER","DRIVER"]
	vehicleUserMap.discontinuedDate = vehicleRequest?.inactivationDate
	vehicleUserMap.registrationNumber = vehicleRequest?.registrationNumber
	vehicleUserMap.state = vehicleRequest?.state
	
	//MYT-1101 default isVerified to false
	vehicleUserMap.isVerified = false
	
	if(vehicleUserMap) {
		vehicle.vehicleUsers = []
		vehicle.vehicleUsers << vehicleUserMap
	}
	
	// Add vehicle to list
	vehicles.add(vehicle)
}

return prettyPrint(toJson(GroovyHelper.removeNulls(vehicles, true)))