package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;
import java.util.HashMap;
import java.util.Properties;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

import javax.script.*;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.DefaultMuleMessage;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.context.DefaultMuleContextFactory;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

public class GetVehiclesVinOutstandingRecallsResponseMapperTest extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;

	public GetVehiclesVinOutstandingRecallsResponseMapperTest() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner
				.createScriptRunner("/mappings/get-vehicles-vin-outstanding-recalls.groovy");
	}
	
	@Test
	public void testSuccessful() throws Exception {
		String expectedResult = IOUtils.toString(
				getClass().getResourceAsStream("/out/get-vehicles-vin-outstanding-recalls-response.json"));
		
		
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(
						getClass().getResourceAsStream("/in/get-vehicles-vin-outstanding-recalls-vehicleApi-response.json")))
				.flowVar("recallType", "SSC")
				.run().toString();
		

		System.out.println(result);
		JSONAssert.assertEquals(expectedResult, result, true);
	}
}
