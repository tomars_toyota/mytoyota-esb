import groovy.util.XmlSlurper
import static groovy.json.JsonOutput.*
import au.com.toyota.esb.mytoyota.api.util.GroovyHelper

def userResponse = new XmlSlurper().parseText(payload)?.result

user = [:]
if(sessionVars['guestPreferences']) {
	// Map preferences
	if (userResponse?.marketingPreference.text()) {
		user.globalUnsubscribe = userResponse?.marketingPreference.text()
	}
	if (userResponse?.guestPreferences?.goPlacesPreference.text()) {
		user.goPlacesPreference = userResponse?.guestPreferences?.goPlacesPreference.text()
	}
	if (userResponse?.guestPreferences?.pnpOptIn.text()) {
		user.prospectNurtureOptIn = userResponse?.guestPreferences?.pnpOptIn.text()
	}
	if (userResponse?.guestPreferences?.emailOptInTMC.text()) {
		user.trackMyCarEmailOptIn = userResponse?.guestPreferences?.emailOptInTMC.text()
	}
	if (userResponse?.guestPreferences?.smsOptInTMC.text()) {
		user.trackMyCarSMSOptIn = userResponse?.guestPreferences?.smsOptInTMC.text()
	}
}
else {
	user.firstName = userResponse?.ownerDetails?.firstName.text()
	user.lastName = userResponse?.ownerDetails?.lastName.text()
	user.email = userResponse?.ownerDetails?.email.text()

	//[MYT-253]support residential address and other fields
	user.dateOfBirth = userResponse?.ownerDetails?.dob.text()
	user.mobile = userResponse?.ownerDetails?.personmobilephone.text()
	user.landline = userResponse?.ownerDetails?.phone.text()
	
	addressMap = [:]

	residentialAddress = userResponse?.addressInformation?.billingAddress
	residentialAddressMap = [:]
	
	residentialAddressMap.suburb = residentialAddress?.cityorsuburb.text()
	residentialAddressMap.postcode = residentialAddress?.postcode.text()
	residentialAddressMap.state = residentialAddress?.state.text()
	residentialAddressMap.streetAddress = residentialAddress?.street.text()
	if(GroovyHelper.removeNulls(residentialAddressMap)){
		residentialAddressMap.totalCheckId = 'other'
		addressMap.residential = residentialAddressMap
	}

	postalAddress= userResponse?.addressInformation?.shippingAddress
	postalAddressMap = [:]
	
	postalAddressMap.suburb = postalAddress?.cityorsuburb.text()
	postalAddressMap.postcode = postalAddress?.postcode.text()
	postalAddressMap.state = postalAddress?.state.text()
	postalAddressMap.streetAddress = postalAddress?.street.text()
	
	if(GroovyHelper.removeNulls(postalAddressMap)){
		postalAddressMap.totalCheckId = 'other'
		addressMap.postal = postalAddressMap
	}

	addressMap =  GroovyHelper.removeNulls(addressMap)
	if(addressMap)
		user.address = addressMap
}

return prettyPrint(toJson(user))

// <?xml version="1.0" encoding="UTF-8"?>
// <retrieveGuestInformationResponse xmlns="http://soap.sforce.com/schemas/class/SP_UserRegWSTransactionStructure" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
// <result>
//     <addressInformation>
//        <billingAddress>
//           <cityorsuburb xsi:nil="true"/>
//           <country xsi:nil="true"/>
//           <postcode xsi:nil="true"/>
//           <state xsi:nil="true"/>
//           <street xsi:nil="true"/>
//        </billingAddress>
//        <shippingAddress>
//           <cityorsuburb>MASCOT</cityorsuburb>
//           <country>Australia</country>
//           <postcode>2020</postcode>
//           <state>NSW</state>
//           <street>Unit 10 109-123 O'riordan St</street>
//        </shippingAddress>
//     </addressInformation>
//     <cmUserId xsi:nil="true"/>
//     <guestPreferences>
//        <emailOptInTMC>false</emailOptInTMC>
//        <goPlacesPreference>Soft Copy Only</goPlacesPreference>
//        <pnpOptIn>false</pnpOptIn>
//        <smsOptInTMC>true</smsOptInTMC>
//     </guestPreferences>
//     <isActiveCMuser xsi:nil="true"/>
//     <isVehicleCMEquipped xsi:nil="true"/>
//     <marketingPreference>false</marketingPreference>
//     <myToyotaId xsi:nil="true"/>
//     <ownerDetails>
//        <dob xsi:nil="true"/>
//        <email>brendanleetoyota+33@gmail.com</email>
//        <firstName>Brendo</firstName>
//        <lastName>Lee</lastName>
//        <Password xsi:nil="true"/>
//        <personmobilephone>0434907792</personmobilephone>
//        <phone xsi:nil="true"/>
//        <salutation xsi:nil="true"/>
//     </ownerDetails>
//     <responseMessage>SPMyToyota002; Guest Information Retrieved Successfully.</responseMessage>
//     <resultStatus>SUCCESS</resultStatus>
//     <salesforceId xsi:nil="true"/>
//     <vehicleOwnerships xsi:nil="true"/>
//  </result>
// </retrieveGuestInformationResponse>