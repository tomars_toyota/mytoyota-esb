import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper
import java.text.SimpleDateFormat

def vehicleResponse = new JsonSlurper().parseText(payload)

def vehicle = [:]
vehicle.batchNumber = vehicleResponse?.batchNumber
vehicle.compliancePlate = vehicleResponse?.compliancePlate
vehicle.engineNumber = vehicleResponse?.engineNumber
vehicle.vin = vehicleResponse?.vin
vehicle.materialNumber = vehicleResponse?.materialNumber
vehicle.vehicleDescription = vehicleResponse?.vehicleDescription
vehicle.katashikiCode = vehicleResponse?.katashikiCode
vehicle.transmission = vehicleResponse?.transmission
vehicle.sellingDealerCode = vehicleResponse?.sellingDealerCode
vehicle.sellingDealerName = vehicleResponse?.sellingDealerName
vehicle.registrationNumber = vehicleResponse?.registrationNumber
vehicle.production = vehicleResponse?.production

//MYT-1101
vehicle.isVerified = vehicleResponse?.isVerified

if(vehicleResponse?.discontinuedDate)
	vehicle.discontinuedDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date(vehicleResponse.discontinuedDate))
if(vehicleResponse?.ownershipCreatedDate)
	vehicle.ownershipCreatedDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date(vehicleResponse.ownershipCreatedDate))

vehicle.releaseYear = vehicleResponse?.year

if(vehicleResponse?.suffixCode) {
	vehicle.suffix = [:]
	vehicle.suffix.code = vehicleResponse.suffixCode
	vehicle.suffix.description = vehicleResponse.suffixDescription
}
if(vehicleResponse?.trimCode) {
	vehicle.trim = [:]
	vehicle.trim.code = vehicleResponse.trimCode
	vehicle.trim.description = vehicleResponse.trimDescription
}
if(vehicleResponse?.paintCode) {
	vehicle.paint = [:]
	vehicle.paint.code = vehicleResponse.paintCode
	vehicle.paint.description = vehicleResponse.paintDescription
}


vehicle.salesType = vehicleResponse?.salesType	

//stock images
imagesList = []
vehicleResponse?.images.each { Map image ->
	imageMap = [:]
	imageMap.size = image.code
	imageMap.link = image.url

	//remove nulls
	imageMap = imageMap.findAll {it.value != null && it.value != ""}

	if(imageMap)
		imagesList << imageMap
}

if(imagesList)
	vehicle.images = imagesList


//vehicle user
vehicleUserMap = [:]

vehicleUserMap.registrationNumber = vehicleResponse?.userRegistrationNumber
vehicleUserMap.state = vehicleResponse?.state
// TODO: Mock for now
vehicleUserMap.relationshipTypes = ["DRIVER", "OWNER"]

//remove nulls
vehicleUserMap = vehicleUserMap.findAll {it.value != null && it.value != ""}

if(vehicleUserMap)
	vehicle.vehicleUser = vehicleUserMap

//custom image
if(vehicleResponse?.vehicleImage) {
	customImages = []
	customImage = [:]
	customImage.code = "primary"
	customImage.link = vehicleResponse?.vehicleImage
	customImages << customImage
	vehicle.customImages = customImages
}

vehicle.vehicleOdo = vehicleResponse?.odometer

//Temporary commented out as TLink - SF sync is down
//if(sessionVars['connectedMobility'])
//	vehicle.connectedMobility = sessionVars['connectedMobility'].findAll {it.value != null && it.value != ""}


//remove nulls
vehicle = vehicle.findAll {it.value != null && it.value != ""}


//println ('_________________________'+toJson(vehicle))

// Return
return prettyPrint(toJson(vehicle))