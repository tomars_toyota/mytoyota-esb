import java.util.Date
import java.security.MessageDigest
import com.nimbusds.jose.JWEObject
import com.nimbusds.jose.crypto.DirectDecrypter
import com.nimbusds.jwt.SignedJWT
import static au.com.toyota.esb.mytoyota.api.util.Encrypter.decrypt


def getTokenExpiry(String token) {
	
	//extract encryptionKey
	String secretKey = jwtSecretKey
	MessageDigest md = MessageDigest.getInstance("SHA-256")
	md.update(secretKey.getBytes())
	byte[] encryptionKey = md.digest()
	String decryptedToken = token
	
	if("true".equalsIgnoreCase(encryptionEnabled)) {		
		decryptedToken = decrypt(token, encryptionSecretKey)	
	}

	JWEObject jweObject = JWEObject.parse(decryptedToken)
	jweObject.decrypt(new DirectDecrypter(encryptionKey))
	SignedJWT signedJWT = jweObject.getPayload().toSignedJWT()
   	
   	return signedJWT.getJWTClaimsSet().getExpirationTime()
}

def removeOldestToken(List tokensList, int maxSize) {
	//pick the oldest token
	oldestToken = null
	tokensList.each { token ->	
		if(!oldestToken)
			oldestToken = token
		else if(getTokenExpiry(oldestToken).compareTo(getTokenExpiry(token)) > 0)
			oldestToken = token
	}

	tokensList.remove(oldestToken)

	//make sure the tokensList does not exceed the max size
	//caters for already exisisng lists in Gigya with too many tokens
	if(tokensList.size() >= maxSize)
		tokensList =removeOldestToken(tokensList, maxSize)

	return tokensList
}

def request = [:]
def data = flowVars['userProfileMap']?.data
def tokenType = flowVars['APISessionToken']['tokenType']

if(tokenType == 'myToyotaSessionToken') {
	if(data.myToyotaSessionToken instanceof java.util.ArrayList){
		tokensList = data.myToyotaSessionToken
		try {
			if(tokensList.size() >= maxloggedInSessions.toInteger())
				tokensList = removeOldestToken(tokensList, maxloggedInSessions.toInteger())
		} catch(Exception e) {
			tokensList.removeAll({true}) //remove everything if there is an error decrypting any of the tokens
		}
		tokensList.add(flowVars['APISessionToken']['encrypted'])
		data.myToyotaSessionToken = tokensList
	} else {
		//for old accounts where data.myToyotaSessionToken is still a map
		data.myToyotaSessionToken = [flowVars['APISessionToken']['encrypted']]
	}
} else {
	data[(tokenType)] = ["id" : flowVars['APISessionToken']['encrypted']]
}


if(tokenType == 'tlinkSessionToken')
	data[(tokenType)].expiry = flowVars['APISessionToken']['expiry']

request.data = data
return request


def storedTokens = payload.data?.myToyotaSessionToken


