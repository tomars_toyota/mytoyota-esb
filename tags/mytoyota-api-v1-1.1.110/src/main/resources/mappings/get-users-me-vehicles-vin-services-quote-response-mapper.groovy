import static groovy.json.JsonOutput.*
import au.com.toyota.esb.mytoyota.api.util.GroovyHelper
import groovy.json.JsonSlurper


def serviceQuoteResponse = new JsonSlurper().parseText(payload)

def response = [:]

response.dealerId = sessionVars['dealerId']
response.branchCode = sessionVars['branchCode']


response.tsaEligible = serviceQuoteResponse.TsaEligible
response.production = serviceQuoteResponse.Production
response.quoteDate = serviceQuoteResponse.QuoteDate
response.quoteExpireDate = serviceQuoteResponse.QuoteExpireDate
response.retailServiceCode = serviceQuoteResponse.ServiceCode
response.hasOutstandingServices = serviceQuoteResponse.HasOutstandingServices

operationsList = []

serviceQuoteResponse?.Operations.each { op ->

    operationMap = [:]

    operationMap.isTsaService = op.IsTsaService

    if(op.TsaServiceDetails) {
        tsaServiceDetailsMap = [:]

        tsaServiceDetailsMap.serviceCode = op.TsaServiceDetails.ServiceCode
        tsaServiceDetailsMap.erviceDescription = op.TsaServiceDetails.ServiceDescription
        if(op.TsaServiceDetails.CustomerPaymentAmount)
            tsaServiceDetailsMap.customerPaymentAmount = op.TsaServiceDetails.CustomerPaymentAmount.toDouble()
        tsaServiceDetailsMap.serviceOperationCode = op.TsaServiceDetails.ServiceOperationCode
        if(op.TsaServiceDetails.EstimatedServiceTime)
            tsaServiceDetailsMap.estimatedServiceTime = op.TsaServiceDetails.EstimatedServiceTime.toDouble()
        tsaServiceDetailsMap.serviceDueDate = op.TsaServiceDetails.ServiceDueDate
        tsaServiceDetailsMap.serviceExpiryDate = op.TsaServiceDetails.ServiceExpiryDate
        
        operationMap.tsaServiceDetails = tsaServiceDetailsMap
    }

    operationMap.serviceNumber = op.serviceNumber
    operationMap.operation = op.operation
    operationMap.kilometres = op.kilometres
    operationMap.months = op.months
    if(op.retailServiceCostIncGST)
        operationMap.retailServiceCostIncGst = op.retailServiceCostIncGST.toDouble()
    if(op.tsaServiceCostIncGST)
        operationMap.tsaServiceCostIncGst = op.tsaServiceCostIncGST.toDouble()
    if(op.estimatedServiceTime)
        operationMap.estimatedServiceTime = op.estimatedServiceTime.toDouble()
    
    if(op.serviceKeys) {

        serviceKeysList = []
        
        op.serviceKeys.each { serviceKey ->
            serviceKeyMap = [:]

            serviceKeyMap.key = serviceKey.Key
            serviceKeyMap.section = serviceKey.Section
            serviceKeyMap.description = serviceKey.Description
            serviceKeyMap.action = serviceKey.Action
            serviceKeysList.add(serviceKeyMap)
        }

        operationMap.serviceKeys = serviceKeysList 
    }

    operationsList.add(operationMap)
}

response.operations = operationsList

response = GroovyHelper.removeNulls(response)

return prettyPrint(toJson(response))