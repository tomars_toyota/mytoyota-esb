package au.com.toyota.esb.mytoyota.api.mappings;

import static org.junit.Assert.assertTrue;

import java.io.IOException;

import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

public class AddExistingUsersVehiclesVinMyKitRequestMapper extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;

	public AddExistingUsersVehiclesVinMyKitRequestMapper() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/add-existing-vehicles-to-mykit-request-mapper.groovy");
	}

	@Test
	public void testSuccessfulRequestMapper() throws Exception {
		String payload = IOUtils.toString(getClass().getResourceAsStream("/in/sfdc-retrieve-vehicle-ownership-information-response.json"));
		
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(payload)
				.run().toString();

		System.out.println(result);
		//System.out.println("------------------------");
		
		String expectedResult = IOUtils.toString(getClass().getResourceAsStream("/in/add-existing-vehicles-to-mykit-mapper-request.json"));
		JSONAssert.assertEquals(expectedResult, result, true);
	}
}
