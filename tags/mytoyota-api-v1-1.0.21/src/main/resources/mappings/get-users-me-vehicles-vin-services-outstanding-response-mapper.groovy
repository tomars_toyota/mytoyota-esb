import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper


def response = [:]
response.upsells = []
response.serviceOperations = []
//response.serviceOperation = [:]

JsonSlurper js = new JsonSlurper()

//def getVehicleResponse = js.parseText(sessionVars['getVehicleResponse'])
def getVehicleResponse = sessionVars['getVehicleResponse']
def upsellsList = getVehicleResponse?.vehicleData?.upsells
def serviceOperationList = getVehicleResponse?.vehicleData?.serviceOperations
//def serviceOp = getVehicleResponse.vehicleData.serviceOperations[0]

upsellsList.each { Map upsellItem ->
	def upsell = [:]
	upsell.description = upsellItem.opdsc
	upsell.price = upsellItem.stdchg
	upsell.id = upsellItem.x_rowid
	response.upsells << upsell
}

serviceOperationList.each { Map serviceOp ->
	def serviceOperation = [:]
	serviceOperation.operationTypeId = serviceOp.optid
	serviceOperation.operationId = serviceOp.opid
	serviceOperation.description = serviceOp.opdsc
	serviceOperation.tsaFlag = serviceOp.tsaqua
	serviceOperation.price = serviceOp.fixchg
	serviceOperation.externalClaimNumber = serviceOp.waropr
	serviceOperation.serviceCode = serviceOp.warscd
	serviceOperation.fixedChargeOperationFlag = serviceOp.fixflg
	serviceOperation.cappedPriceOperationFlag = serviceOp.capflg
	response.serviceOperations << serviceOperation
}
//response.serviceOperation.operationTypeId = serviceOp.optid
//response.serviceOperation.operationId = serviceOp.opid
//response.serviceOperation.description = serviceOp.opdsc
//response.serviceOperation.tsaFlag = serviceOp.tsaqua
//response.serviceOperation.price = serviceOp.fixchg
//response.serviceOperation.externalClaimNumber = serviceOp.waropr
//response.serviceOperation.serviceCode = serviceOp.warscd
//response.serviceOperation.fixedChargeOperationFlag = serviceOp.fixflg
//response.serviceOperation.cappedPriceOperationFlag = serviceOp.capflg
//response.serviceOperation.procedureDescription = js.parseText(payload).procedureData.Procedure[0].Description

// Populate the description for the first service.
response.serviceOperations[0].procedureDescription = js.parseText(payload).procedureData.Procedure[0].Description

return toJson(response)