import java.net.URLEncoder;
import groovy.json.JsonSlurper

JsonSlurper js = new JsonSlurper()
def request = js.parseText(payload)

// generate the 'x-www-form-urlencoded' encoded body
def postBody = new StringBuilder()
	.append("SiteID="       + URLEncoder.encode(flowVars['dmsID'], "UTF-8"))
	.append("&VIN="         + URLEncoder.encode(flowVars['vin'], "UTF-8"))
	.append("&Service="     + URLEncoder.encode(request['operationTypeID']+'|'+
			request['operationID']+'|'+
			request['serviceTsaFlag']+'|'+
			request['serviceOperationPrice']+'|'+
			request['externalNumberOfClaim']+'|'+
			request['serviceCode'], "UTF-8"))
	//.append("&VehID="       + URLEncoder.encode(request['vehicleID'], "UTF-8"))
	.append("&BookDate="    + getDaysSinceDayOne(request['dropOffDate']))
	.append("&BookTime="    + getSecSinceMidnight(request['dropOffTime']))
	.append(request.containsKey('pickUpDate') ? ("&OutDate=" + getDaysSinceDayOne(request['pickUpDate'])) : "")
	.append(request.containsKey('pickUpTime') ? ("&OutTime=" + getSecSinceMidnight(request['pickUpTime'])) : "")
	.append(getConfType(request['confirmationType']))
	.append(request.containsKey('confirmationTo') ? ("&ConfTo=" + URLEncoder.encode(request['confirmationTo'], "UTF-8")) : "")
	.append("&SrvPrc="      + request['totalServicePrice'])
	.append(request.containsKey('upsells') ? ("&UpSells=" + request.upsells.ids.join("|")) : "")
	.append((request.containsKey('notes') ? ("&Notes=" + URLEncoder.encode(request['notes'], "UTF-8")) : ""))
	.append((request.containsKey('waitAtDealerFlag') ? ("&Wait=" + request['waitAtDealerFlag']) : ""))
	.append((request.containsKey('notexpressServiceFlages') ? ("&Express=" + request['expressServiceFlag']) : ""))
	.append((request.containsKey('courtesyBusFlag') ? ("&CourtesyBus=" + request['courtesyBusFlag']) : ""))
	.append((request.containsKey('loanVehicleFlag') ? ("&Loan=" + request['loanVehicleFlag']) : ""))


int getDaysSinceDayOne(String stringDate) {
	def firstDay = new GregorianCalendar(0000, Calendar.JANUARY, 1, 0, 0, 0).time
	def tuneDateOffset = 2415022
	return (new Date().parse("yyyy-MM-dd", stringDate) - firstDay) + tuneDateOffset
}

int getSecSinceMidnight(String stringTime) {
	def parsedTime = new Date().parse("HH:mm", stringTime)
	((parsedTime.hours + (parsedTime.minutes / 60)) * 3600).intValue()
}

String getConfType(String confTypeIn) {
	if (confTypeIn == 'email')
		return '&ConfType=E'
	else if (confTypeIn == 'SMS')
		return '&ConfType=S'
	else
		return ''
}


//println postBody.toString()

return postBody.toString()
