package au.com.toyota.esb.mytoyota.api.metrics;

import org.apache.log4j.Logger;
import org.mule.api.context.notification.EndpointMessageNotificationListener;
import org.mule.api.processor.MessageProcessor;
import org.mule.context.notification.EndpointMessageNotification;
import org.mule.context.notification.MessageProcessorNotification;
import org.mule.module.http.internal.request.DefaultHttpRequester;

public class MuleEndpointListener
		implements EndpointMessageNotificationListener<EndpointMessageNotification> {
	private Logger log = Logger.getLogger("esb.mytoyota-api-v1.instrument");
	private long startTime = 0;
	private boolean logMetrics;

	
	public MuleEndpointListener(){
		log.info("MuleEndpointListener is up...");
	}
	
	@Override
	public void onNotification(EndpointMessageNotification notification) {
		log.debug("In MuleEndpointListener.onNotification: " + logMetrics);		
		if(logMetrics) {
			try {
				String transactionId = notification.getSource().getSessionProperty("transactionId");
				if (notification.getAction() == EndpointMessageNotification.MESSAGE_DISPATCH_BEGIN) {
					startTime = System.currentTimeMillis();
				}
				if (notification.getAction() == EndpointMessageNotification.MESSAGE_DISPATCH_END) {
					long executionTime = System.currentTimeMillis() - startTime;
					log.info("[METRICS][metric=RT][dir=INBOUND][threadId=" + Thread.currentThread().getId() + "][transactionId="+ transactionId +"][took(ms)=" + executionTime + "]");
				}
			} catch (Exception e) {
			}
		}
	}
}
