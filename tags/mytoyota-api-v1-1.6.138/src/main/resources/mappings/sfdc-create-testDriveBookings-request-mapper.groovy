import groovy.xml.MarkupBuilder
import groovy.json.JsonSlurper
import java.text.SimpleDateFormat

def writer = new StringWriter()
def xmlMarkup = new MarkupBuilder(writer)


if(payload instanceof org.mule.transport.NullPayload)
	payload = null

def testDriveBooking = new JsonSlurper().parseText(payload)


xmlMarkup.mkp.xmlDeclaration(version: "1.0", encoding: "UTF-8")
xmlMarkup.'sp:guestWebservices' (
	'xmlns:sp' : "http://soap.sforce.com/schemas/class/GuestVehicleWebserviceCalls"
	) {
		'sp:request' {
			'sp:campaignDetailsWrapper' {
				'sp:campaignDetailList' {
					'sp:campaignCode' (testDriveBooking.campaignDetails?.campaignCode)
					'sp:campaignName' (testDriveBooking.campaignDetails?.campaignName)
				}
			}
			'sp:dealerDetails' {
				'sp:DealerCode' (testDriveBooking.dealerDetails?.dealerCode)
				'sp:locationCode' (testDriveBooking.dealerDetails?.locationCode)
				
			}
			'sp:guestDetails' {
				'sp:ownersPortalUserId' (flowVars.myToyotaId)
			}
			
			'sp:leadDetails' {
				if (testDriveBooking.interestedVehicles) {
					'sp:carSelectionOne' (testDriveBooking.interestedVehicles.first().model)
				}
			}
			
			'sp:webFormType' (webFormTypeTestDrive)
			
		
		}	
	}
//println '__________ payload to salesforce: ' + writer.toString()
//'sp:email' (testDriveBooking.email)
//'sp:lastName' (testDriveBooking.lastName)
//'sp:firstName' (testDriveBooking.firstName)
//'sp:personmobilephone' (testDriveBooking.personmobilephone)
return writer.toString()

