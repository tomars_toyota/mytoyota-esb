import static groovy.json.JsonOutput.*
import java.text.SimpleDateFormat

def requestMap = [:]

serviceRequest = sessionVars['serviceRequest']

requestMap.My_Toyota_Id__c = flowVars['myToyotaId']
requestMap.title = serviceRequest?.contact_details?.title
requestMap.firstName = serviceRequest?.contact_details?.first_name
requestMap.lastName = serviceRequest?.contact_details?.last_name
requestMap.email = serviceRequest?.contact_details?.email
requestMap.mobilePhone = serviceRequest?.contact_details?.phone

dealer = sessionVars['dealerResponse']
dealerSite = dealer?.sites[0]
requestMap.serviceRequestDealerName = dealer?.dealerName
requestMap.serviceRequestDealerAddress = dealerSite?.address?.streetAddress
requestMap.serviceRequestDealerEmail = dealerSite?.email?.service ?: dealerSite?.email?.main
requestMap.serviceRequestDealerPhoneNumber = dealerSite?.phone?.main
requestMap.serviceRequestDealerPostCode = dealerSite?.address?.postCode
requestMap.serviceRequestDealerState = dealerSite?.address?.state
requestMap.serviceRequestDealerSuburb = dealerSite?.address?.suburb
requestMap.serviceRequestDealerWebsite = dealerSite?.webSite

requestMap.serviceRequestServiceDescription = serviceRequest?.service_desc
requestMap.serviceRequestVehicleDescription = serviceRequest?.vehicle_desc
requestMap.serviceRequestServiceCode = serviceRequest?.service_code
requestMap.serviceRequestVehicleVIN = serviceRequest?.vin
requestMap.serviceRequestVehicleRego = serviceRequest?.registration_number
requestMap.serviceRequestDropOffDate =  new SimpleDateFormat("YYYY-MM-dd").format(new Date().parse('E dd MMM yyyy', serviceRequest?.drop_off_date))
requestMap.serviceRequestDropOffTime = serviceRequest?.drop_off_time
requestMap.serviceRequestNotes = serviceRequest?.notes
// Always sent and set to true to enable Marketo to always send the email
// Previously Marketo was basing this off the 'ServiceCode' and 'ServiceDescription' fields above, however these were sometimes blank resulting in the email not being sent correctly
requestMap.sendServiceRequestEmail = true

request = ['input':[requestMap]]

return prettyPrint(toJson(request))