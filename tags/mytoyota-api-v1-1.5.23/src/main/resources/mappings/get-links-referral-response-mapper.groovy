import groovy.json.JsonSlurper
import static groovy.json.JsonOutput.*
import java.text.DateFormat
import java.text.SimpleDateFormat

def response = [:]

def queryString = message.getInboundProperty("http.query.string") !=null ? "?" + message.getInboundProperty("http.query.string") : ""

response.link = referralLink + queryString
response.shortLink = response.link

return prettyPrint(toJson(response))

