import groovy.json.JsonSlurper
//import groovy.json.JsonOutput.*
import groovy.json.JsonBuilder

def response = [:]
def procedureData = new JsonSlurper().parseText(payload).procedureData
if(!procedureData || !procedureData.Procedure)
	throw new org.mule.module.apikit.exception.NotFoundException("No description available for the provided procedure ")

response.operationDescription = procedureData.Procedure[0].Description
//return prettyPrint(toJson(response))
return new JsonBuilder(response).toString()