import groovy.json.JsonSlurper
import static groovy.json.JsonOutput.*

def stripLeadingZeros = { it.replaceFirst("^0*", "") }
def dealerVinMap = message.getProperty("dealerVins",org.mule.api.transport.PropertyScope.INVOCATION)
def dealers = new JsonSlurper().parseText(payload)
def dealerDetailMap = dealers.collectEntries({ [(it.dealerCode) : it] })

dealerDetailMap.each { dealerCode, dealerDetail ->
		dealerId = stripLeadingZeros(dealerDetail.dealerId)
		branchId = stripLeadingZeros(dealerDetail.branchId)
		dealerDetail.favorite = false
    	dealerDetail.vehicles = dealerVinMap[dealerId + ":" + branchId]
}

return prettyPrint(toJson(dealerDetailMap.values()))