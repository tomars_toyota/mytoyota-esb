package au.com.toyota.esb.mytoyota.api;

import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.Properties;

import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.util.IOUtils;
import org.mule.util.StringUtils;
import org.skyscreamer.jsonassert.JSONAssert;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;

public class PostUsersMeVehiclesVINVerificationCodeCheckMunitTest extends FunctionalMunitSuite {
	
	private static Properties adapterProperties = new Properties();

	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"post-users-me-vehicles-vin-verificationcode-check.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testVerificationCodeCheckSuccessful() throws Exception {
		
		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));
		
		String testVinNumber = "JTMBFREV80D078597";
		// Mock myKIT response
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
				attribute("name").ofNamespace("doc").withValue("MyKit API: POST /users/{myToyotaId}/vehicles/{vin}/verificationCodes/check"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						
						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							String myKITResponse = IOUtils.toString(getClass().getResourceAsStream("/out/post-users-me-vehicles-vin-verificationcode-check-myKIT-response.json"));
							newMessage.setPayload(myKITResponse);
						} catch (Exception e) {
							// catch exception statements
						}
						return newMessage;
					}
				});
				
		String requestPayload = IOUtils.toString(getClass().getResourceAsStream("/in/post-users-me-vehicles-vin-verificationcode-check.json"));
		String expectedResult = IOUtils.toString(getClass().getResourceAsStream("/out/post-users-me-vehicles-vin-verificationcode-check-response.json"));
		
		MuleEvent event = testEvent(requestPayload);
		event.setFlowVariable("vin",testVinNumber);
		        
		// Invoke the flow
		@SuppressWarnings("unused")
		MuleEvent output = runFlow("post:/users/me/vehicles/{vin}/verificationCode/check:myToyota-config", event);
		
		String result = output.getMessage().getPayload().toString();
		
		// Check response payload
		JSONAssert.assertEquals(expectedResult, result, true);

		// Main flow
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Request")).times(1);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Response")).times(1);
	}
	
	@Test
	public void testVerificationCodeCheckCodeExpired() throws Exception {
		
		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));
		
		String testVinNumber = "JTMBFREV80D078597";
		// Mock myKIT response
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
				attribute("name").ofNamespace("doc").withValue("MyKit API: POST /users/{myToyotaId}/vehicles/{vin}/verificationCodes/check"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						
						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							String myKITResponse = IOUtils.toString(getClass().getResourceAsStream("/out/post-users-me-vehicles-vin-verificationcode-check-expiredcode-myKIT-response.json"));
							newMessage.setPayload(myKITResponse);
						} catch (Exception e) {
							// catch exception statements
						}
						return newMessage;
					}
				});
				
		String requestPayload = IOUtils.toString(getClass().getResourceAsStream("/in/post-users-me-vehicles-vin-verificationcode-check.json"));
		String expectedResult = IOUtils.toString(getClass().getResourceAsStream("/out/post-users-me-vehicles-vin-verificationcode-check-expiredcode-response.json"));
		
		MuleEvent event = testEvent(requestPayload);
		event.setFlowVariable("vin",testVinNumber);
		        
		// Invoke the flow
		@SuppressWarnings("unused")
		MuleEvent output = runFlow("post:/users/me/vehicles/{vin}/verificationCode/check:myToyota-config", event);
		
		String result = output.getMessage().getPayload().toString();
		
		// Check response payload
		JSONAssert.assertEquals(expectedResult, result, true);

		// Main flow
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Request")).times(1);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Response")).times(1);
	}
	
	@Test
	public void testVerificationCodeCheckCodeInvalid() throws Exception {
		
		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));
		
		String testVinNumber = "JTMBFREV80D078597";
		// Mock myKIT response
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
				attribute("name").ofNamespace("doc").withValue("MyKit API: POST /users/{myToyotaId}/vehicles/{vin}/verificationCodes/check"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						
						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							String myKITResponse = IOUtils.toString(getClass().getResourceAsStream("/out/post-users-me-vehicles-vin-verificationcode-check-invalidcode-myKIT-response.json"));
							newMessage.setPayload(myKITResponse);
						} catch (Exception e) {
							// catch exception statements
						}
						return newMessage;
					}
				});
				
		String requestPayload = IOUtils.toString(getClass().getResourceAsStream("/in/post-users-me-vehicles-vin-verificationcode-check.json"));
		String expectedResult = IOUtils.toString(getClass().getResourceAsStream("/out/post-users-me-vehicles-vin-verificationcode-check-invalidcode-response.json"));
		
		MuleEvent event = testEvent(requestPayload);
		event.setFlowVariable("vin",testVinNumber);
		        
		// Invoke the flow
		@SuppressWarnings("unused")
		MuleEvent output = runFlow("post:/users/me/vehicles/{vin}/verificationCode/check:myToyota-config", event);
		
		String result = output.getMessage().getPayload().toString();
		
		// Check response payload
		JSONAssert.assertEquals(expectedResult, result, true);

		// Main flow
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Request")).times(1);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Response")).times(1);
	}
	
	@Test
	public void testFailuresStatusCodes() throws Exception {
		
		int[] statusCodes = new int[] { 404, 405, 415, 406, 401, 500 };	
		
		String vin = "JTMBFREV80D078597";
		String requestPayload = IOUtils.toString(getClass().getResourceAsStream("/in/post-users-me-vehicles-vin-verificationcode-check.json"));

		MuleEvent event = testEvent(requestPayload);
		event.setFlowVariable("vin", vin);
		
		for (int i = 0; i < statusCodes.length; i++) {
			new StatusCodesTestingHelper().runFailureTest(statusCodes[i], "MyKit API: POST /users/{myToyotaId}/vehicles/{vin}/verificationCodes/check",
					"post:/users/me/vehicles/{vin}/verificationCode/check:myToyota-config", event);
		}
	}
}
	