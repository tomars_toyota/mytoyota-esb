package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;
import java.util.HashMap;

import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.custommonkey.xmlunit.XMLTestCase;
import org.junit.Test;
import org.xml.sax.SAXException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

public class UpdateSfdcUserRequestMapperTest extends XMLTestCase {

    private ScriptRunner scriptRunner;

    public UpdateSfdcUserRequestMapperTest() throws ScriptException, SAXException, IOException {
        scriptRunner = ScriptRunner.createScriptRunner("/mappings/sfdc-update-user-request-mapper.groovy");
    }

    //@Test
    public void testRDRRequestMapping() throws Exception {

        HashMap<String, Object> payload = new HashMap<String, Object>();
        payload.put("firstName", "John");
        payload.put("lastName", "Smith");
        payload.put("mobile", "0412344321");
        payload.put("email", "jsmith@mail.com.au");
        payload.put("salesforceId", "0011900000NDTZc");
        payload.put("isMarketingOptIn", true);
        payload.put("vin", "JTNKU3JE60J021675");
            
        
        String result = ScriptRunBuilder
                .runner(scriptRunner)
                .payload(payload)
                .sessionVar("myToyotaId", "MYT-000003a3")
                .sessionVar("encryptedPassword", "teywrtyewtryweurtweiuyrt")
                .run().toString();

        System.out.println(result);

        // expected
        String expectedResult = IOUtils.toString(getClass().getResourceAsStream("/out/updatOwner-rdr-sfdc-request.xml"));
        assertXMLEqual(expectedResult, result);
    }
    
    //@Test
    public void testUpdateUserRequestMapping() throws Exception {

        HashMap<String, Object> postal = new HashMap<String, Object>();
        postal.put("suburb", "Melbourne");
        postal.put("postcode", "3000");
        postal.put("state", "Vic");
        postal.put("streetAddress", "446 Collins St");
        
        HashMap<String, Object> address = new HashMap<String, Object>();
        address.put("postal", postal);
        
        
        HashMap<String, Object> payload = new HashMap<String, Object>();
        
        payload.put("address", address);
        payload.put("dateOfBirth", "1985-05-20");
        payload.put("email", "jsmith@mail.com.au");
        payload.put("firstName", "John");
        payload.put("landline", "0312341234");
        payload.put("lastName", "Smith");
        payload.put("mobile", "0412344321");
        payload.put("title", "Mr");
            
        
        String result = ScriptRunBuilder
                .runner(scriptRunner)
                .payload(payload)
                .sessionVar("anything", null)    //initialise sessionVar otherwise groovy fails when triggered from Munit
                .run().toString();

        System.out.println(result);

        // expected
        String expectedResult = IOUtils.toString(getClass().getResourceAsStream("/out/updatOwner-update-user-sfdc-request.xml"));
        assertXMLEqual(expectedResult, result);
    }
    
    //@Test
    public void testUpdateUserIncludingOrderDetailsRequestMapping() throws Exception {
    	
    	HashMap<String, Object> guestOrder = new HashMap<String, Object>();
    	guestOrder.put("cosi", "0007521987");
    	HashMap<String, Object> vehicleDetails = new HashMap<String, Object>();
    	vehicleDetails.put("batchId", "0007789228");
    	HashMap<String, Object> status = new HashMap<String, Object>();
    	status.put("smoothedStatus", "50");
    	status.put("statusDescription", "Despatch To Dealer");
    	guestOrder.put("vehicleDetails", vehicleDetails);
    	guestOrder.put("status", status);
        
    	HashMap<String, Object> postal = new HashMap<String, Object>();
        postal.put("suburb", "Melbourne");
        postal.put("postcode", "3000");
        postal.put("state", "Vic");
        postal.put("streetAddress", "446 Collins St");
        
        HashMap<String, Object> address = new HashMap<String, Object>();
        address.put("postal", postal);
        
        
        HashMap<String, Object> payload = new HashMap<String, Object>();
        
        payload.put("address", address);
        payload.put("dateOfBirth", "1985-05-20");
        payload.put("email", "jsmith@mail.com.au");
        payload.put("firstName", "John");
        payload.put("landline", "0312341234");
        payload.put("lastName", "Smith");
        payload.put("mobile", "0412344321");
        payload.put("title", "Mr");
        payload.put("guestOrder", guestOrder);
            
        
        String result = ScriptRunBuilder
                .runner(scriptRunner)
                .payload(payload)
                .sessionVar("anything", null)    //initialise sessionVar otherwise groovy fails when triggered from Munit
                .run().toString();

        System.out.println(result);

        // expected
        String expectedResult = IOUtils.toString(getClass().getResourceAsStream("/out/updatOwner-update-user-with-order-details-sfdc-request.xml"));
        assertXMLEqual(expectedResult, result);
    }
    
    //@Test
    public void testUpdateUserPartialRequestMapping() throws Exception {

        HashMap<String, Object> postal = new HashMap<String, Object>();
        postal.put("suburb", "Melbourne");
        postal.put("postcode", "3000");
        postal.put("state", "Vic");
        postal.put("streetAddress", "446 Collins St");
        
        HashMap<String, Object> address = new HashMap<String, Object>();
        address.put("postal", postal);        
        
        HashMap<String, Object> payload = new HashMap<String, Object>();
        
        payload.put("address", address);
        payload.put("title", "");
        payload.put("dateOfBirth", "");
        
            
        
        String result = ScriptRunBuilder
                .runner(scriptRunner)
                .payload(payload)
                .sessionVar("anything", null)    //initialise sessionVars otherwise groovy fails when triggered from Munit
                .run().toString();

        System.out.println(result);

        // expected
        String expectedResult = IOUtils.toString(getClass().getResourceAsStream("/out/updatOwner-update-user-partial-sfdc-request.xml"));
        assertXMLEqual(expectedResult, result);
    }
}