package au.com.toyota.esb.mytoyota.api;

import static org.mule.munit.common.mocking.Attribute.attribute;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.skyscreamer.jsonassert.JSONAssert;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;

//@Ignore
public class PostSessionTLinkVerifyTokenMunitTest extends FunctionalMunitSuite {

	private static String login = "ahmed.huwait+108@gmail.com";
	private static String tlinkSessionToken = "6ca5060d-8416-4889-bc0c-0f7317f7c440";
	private static String tlinkSessionTokenInvalid = "6ca5060d-8416-4889-bc0c-0f7317f7c440-invalid";
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] { "post-session-tlink-verify-token.xml",
				"post-session-tlink-verify-token-technical-adapter.xml",
				"search-user-account-info-im-technical-adapter.xml",
				"config.xml" }, " ");
	}
	
	//@Test
	public void testValidTokenVerification() throws Exception {
		String expected = "{\"valid\":true,\"login\":\"ahmed.huwait+108@gmail.com\",\"token\":\""
				+ tlinkSessionToken
				+ "\"}";
		testImpl(tlinkSessionToken, expected);
	}
	
	//@Test
	public void testInvalidTokenVerification() throws Exception {
		String expected = "{\"valid\":false,\"login\":\"ahmed.huwait+108@gmail.com\",\"token\":\""
				+ tlinkSessionTokenInvalid
				+ "\"}";
		testImpl(tlinkSessionTokenInvalid, expected);
	}
	
	private void testImpl(final String token, String expectedResponse) throws Exception {
		// Mock search user profile "IM API Search User" response
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("IM API Search User"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							newMessage.setPayload(IOUtils.toString(getClass().getResourceAsStream(
									"/in/search-user-profile-im-api-response.json")));
							newMessage.setProperty("login", login, PropertyScope.INVOCATION);
							newMessage.setProperty("token", token, PropertyScope.INVOCATION);
						} catch (Exception e) {
						}
						return newMessage;
					}
				});
		
		MuleEvent event = testEvent("");
		MuleMessage msg = muleMessageWithPayload(null);
		msg.setOutboundProperty("login", login);
		msg.setOutboundProperty("tlinkSessionToken", token);
		event.setMessage(msg);

		// Invoke the flow
		MuleEvent output = runFlow("post:/sessions/tlink/verify:myToyota-config", event);
		String responseMessagePayload = output.getMessage().getPayloadAsString();

		System.out.println("**** response: " + responseMessagePayload);

		JSONAssert.assertEquals(expectedResponse, responseMessagePayload, false);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.post-sessions-tlink-verify-token.request"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.post-sessions-tlink-verify-token.response"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category")
						.withValue("esb.mytoyota-api-v1.post-session-tlink-verify-token-technical-adapter.request"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.post-session-tlink-verify-token-technical-adapter.response"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.post-session-tlink-verify-token-technical-adapter-subflow.request"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.post-session-tlink-verify-token-technical-adapter-subflow.response"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.search-user-account-technical-adapter.request"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.search-user-account-technical-adapter.response"))
				.times(1);
	}
}
