package au.com.toyota.esb.mytoyota.api;

import static org.junit.Assert.*;
import static org.mule.munit.common.mocking.Attribute.attribute;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.skyscreamer.jsonassert.JSONAssert;

import au.com.toyota.esb.mytoyota.api.munit.SimplifiedMockResponseTransformer;

public class DeleteUsersVehiclesVinTest extends FunctionalMunitSuite {

	private static String requestPath = "/users/vehicles/{vin}";

	@Override
	protected String getConfigResources() {
		return StringUtils.join(
				new String[] { 
						"put-users.xml", 
						"put-update-user-account-info-technical-adapter.xml", 
						"delete-users-vehicles-vin.xml", 
						"delete-users-vehicles-vin-technical-adapter.xml", 
						"config.xml" },
				" ");
	}

	//@Test
	public void testSuccessfulRequest() throws Exception {

		// Mock myKIT
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("My Toyota App API DELETE User Vehicle by VIN"))
				.thenApply(new SimplifiedMockResponseTransformer(200, ""));

		MuleEvent event = testEvent(requestPath);
		MuleMessage msg = muleMessageWithPayload("");

		msg.setProperty("client_id", "8bbec807dc1944ee87df0b257f1bf354", PropertyScope.INBOUND);
		msg.setProperty("myToyotaId", "MYT-0000058b", PropertyScope.INBOUND);
		event.setMessage(msg);
		event.setFlowVariable("vin", "JTNKU3JE60J021675");
		
		MuleEvent output = runFlow("delete:/users/vehicles/{vin}:myToyota-config", event);

		assertTrue(output.getMessage().getProperty("http.status", PropertyScope.INBOUND).toString().equals("200"));

		// Verify main flow
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.delete-users-vehicles-vin.request")).times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.delete-users-vehicles-vin.response")).times(1);

		// Verify authorisation sub-flow
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.put-users.perform-authorisation-check.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.put-users.perform-authorisation-check.response"))
				.times(1);

		// myKIT technical adapter call
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category")
						.withValue("esb.mytoyota-api-v1.delete-users-vehicles-vin-technical-adapter.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category")
						.withValue("esb.mytoyota-api-v1.delete-users-vehicles-vin-technical-adapter.response"))
				.times(1);
	}

	//@Test
	public void testFailureMissingMyToyotaID() throws Exception {

		String expectedResult = "{\"message\":\"Invalid request, missing myToyota ID\"}";

		MuleEvent event = testEvent(requestPath);
		MuleMessage msg = muleMessageWithPayload("");
		msg.setProperty("client_id", "8bbec807dc1944ee87df0b257f1bf354", PropertyScope.INBOUND);
		event.setMessage(msg);
		event.setFlowVariable("vin", "JTNKU3JE60J021675");
		MuleEvent output = runFlow("delete:/users/vehicles/{vin}:myToyota-config", event);

		JSONAssert.assertEquals(expectedResult, (String) output.getMessage().getPayload(), true);
		// Check return status of 400
		assertEquals(400, Integer.parseInt(output.getMessage().getOutboundProperty("http.status").toString()));

	}

	//@Test
	public void testFailureUnauthorisedClientID() throws Exception {
		String expectedResult = "{\"message\":\"Client ID not authorised on this resource\"}";

		MuleEvent event = testEvent(requestPath);
		MuleMessage msg = muleMessageWithPayload("");
		msg.setProperty("client_id", "notvalid", PropertyScope.INBOUND);
		event.setMessage(msg);
		MuleEvent output = runFlow("delete:/users/vehicles/{vin}:myToyota-config", event);

		JSONAssert.assertEquals(expectedResult, (String) output.getMessage().getPayload(), true);
		// Check return status of 401
		assertEquals(401, Integer.parseInt(output.getMessage().getOutboundProperty("http.status").toString()));

	}
}
