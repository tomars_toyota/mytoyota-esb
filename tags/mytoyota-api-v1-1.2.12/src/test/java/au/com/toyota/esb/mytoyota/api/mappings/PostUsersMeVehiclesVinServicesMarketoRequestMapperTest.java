package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;

import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;
import org.mule.module.json.JsonData;
import static org.junit.Assert.assertTrue;

public class PostUsersMeVehiclesVinServicesMarketoRequestMapperTest extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;

	public PostUsersMeVehiclesVinServicesMarketoRequestMapperTest() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner
				.createScriptRunner("/mappings/post-users-me-vehicles-vin-services-marketo-request-mapper.groovy");
	}
	
	//@Test
	public void testSuccessful() throws Exception {
		String expectedResult = IOUtils.toString(
				getClass().getResourceAsStream("/out/post-users-me-vehicles-vin-services-marketo-request.json"));
		String apiRequest = IOUtils
				.toString(getClass().getResourceAsStream("/in/post-users-me-vehicles-vin-services-request.json"));
		String dealerResponse = IOUtils
				.toString(getClass().getResourceAsStream("/in/get-dealers-by-dealer-code-api-response.json"));
		String vehicleResponse = IOUtils
				.toString(getClass().getResourceAsStream("/in/get-vehicle-details-response.json"));
		
		String result = testSuccessfulImpl(expectedResult, apiRequest, dealerResponse, vehicleResponse);
		
		
		//one of the fields to compare is set to currentDate, so it cannot be compared to a static value
		JsonData expectedJsonData = new JsonData(expectedResult);
		JsonData returnedJsonData = new JsonData(result);
		
		assertTrue(expectedJsonData.get("input[0]/My_Toyota_Id__c").equals(returnedJsonData.get("input[0]/My_Toyota_Id__c")));
		assertTrue(expectedJsonData.get("input[0]/firstName").equals(returnedJsonData.get("input[0]/firstName")));
		assertTrue(expectedJsonData.get("input[0]/lastName").equals(returnedJsonData.get("input[0]/lastName")));
		assertTrue(expectedJsonData.get("input[0]/email").equals(returnedJsonData.get("input[0]/email")));
		assertTrue(expectedJsonData.get("input[0]/mobilePhone").equals(returnedJsonData.get("input[0]/mobilePhone")));
		assertTrue(!returnedJsonData.get("input[0]/serviceBookingDate").isNull());
		assertTrue(expectedJsonData.get("input[0]/serviceBookingVehicleModel").equals(returnedJsonData.get("input[0]/serviceBookingVehicleModel")));
		assertTrue(expectedJsonData.get("input[0]/serviceBookingVehicleRego").equals(returnedJsonData.get("input[0]/serviceBookingVehicleRego")));
		assertTrue(expectedJsonData.get("input[0]/serviceBookingVehicleVIN").equals(returnedJsonData.get("input[0]/serviceBookingVehicleVIN")));
		assertTrue(expectedJsonData.get("input[0]/serviceBookingNumber").equals(returnedJsonData.get("input[0]/serviceBookingNumber")));
		assertTrue(expectedJsonData.get("input[0]/serviceBookingQuote").equals(returnedJsonData.get("input[0]/serviceBookingQuote")));
		assertTrue(expectedJsonData.get("input[0]/serviceBookingTypeofService").equals(returnedJsonData.get("input[0]/serviceBookingTypeofService")));
		assertTrue(expectedJsonData.get("input[0]/serviceBookingDropOffDate").equals(returnedJsonData.get("input[0]/serviceBookingDropOffDate")));
		assertTrue(expectedJsonData.get("input[0]/serviceBookingDropOffTime").equals(returnedJsonData.get("input[0]/serviceBookingDropOffTime")));
		assertTrue(expectedJsonData.get("input[0]/serviceBookingPickUpDate").equals(returnedJsonData.get("input[0]/serviceBookingPickUpDate")));
		assertTrue(expectedJsonData.get("input[0]/serviceBookingPickUpTime").equals(returnedJsonData.get("input[0]/serviceBookingPickUpTime")));
		assertTrue(expectedJsonData.get("input[0]/serviceBookingDealerName").equals(returnedJsonData.get("input[0]/serviceBookingDealerName")));
		assertTrue(expectedJsonData.get("input[0]/serviceBookingDealerPhoneNumber").equals(returnedJsonData.get("input[0]/serviceBookingDealerPhoneNumber")));
		assertTrue(expectedJsonData.get("input[0]/serviceBookingDealerPostCode").equals(returnedJsonData.get("input[0]/serviceBookingDealerPostCode")));
		assertTrue(expectedJsonData.get("input[0]/serviceBookingDealerState").equals(returnedJsonData.get("input[0]/serviceBookingDealerState")));
		assertTrue(expectedJsonData.get("input[0]/serviceBookingDealerAddress").equals(returnedJsonData.get("input[0]/serviceBookingDealerAddress")));
		assertTrue(expectedJsonData.get("input[0]/serviceBookingDealerSuburb").equals(returnedJsonData.get("input[0]/serviceBookingDealerSuburb")));
		assertTrue(expectedJsonData.get("input[0]/serviceBookingDealerEmail").equals(returnedJsonData.get("input[0]/serviceBookingDealerEmail")));
		assertTrue(expectedJsonData.get("input[0]/serviceBookingDealerWebsite").equals(returnedJsonData.get("input[0]/serviceBookingDealerWebsite")));
		assertTrue(expectedJsonData.get("input[0]/serviceBookingCustomerComments").equals(returnedJsonData.get("input[0]/serviceBookingCustomerComments")));
	}
	
	//@Test
	public void testSuccessfulWithNulls() throws Exception {
		String expectedResult = IOUtils.toString(getClass()
				.getResourceAsStream("/out/post-users-me-vehicles-vin-services-marketo-request-with-nulls.json"));
		String apiRequest = IOUtils.toString(
				getClass().getResourceAsStream("/in/post-users-me-vehicles-vin-services-request-wth-nulls.json"));
		String dealerResponse = IOUtils.toString(
				getClass().getResourceAsStream("/in/get-dealers-by-dealer-code-api-response-with-nulls.json"));
		String vehicleResponse = IOUtils
				.toString(getClass().getResourceAsStream("/in/get-vehicle-details-response.json"));
		
		String result = testSuccessfulImpl(expectedResult, apiRequest, dealerResponse, vehicleResponse);
		
		//one of the fields to compare is set to currentDate, so it cannot be compared to a static value
		JsonData expectedJsonData = new JsonData(expectedResult);
		JsonData returnedJsonData = new JsonData(result);
		
		assertTrue(expectedJsonData.get("input[0]/My_Toyota_Id__c").equals(returnedJsonData.get("input[0]/My_Toyota_Id__c")));
		assertTrue(returnedJsonData.get("input[0]/firstName").isNull());
		assertTrue(returnedJsonData.get("input[0]/lastName").isNull());
		assertTrue(returnedJsonData.get("input[0]/email").isNull());
		assertTrue(returnedJsonData.get("input[0]/mobilePhone").isNull());
		assertTrue(!returnedJsonData.get("input[0]/serviceBookingDate").isNull());
		assertTrue(expectedJsonData.get("input[0]/serviceBookingVehicleModel").equals(returnedJsonData.get("input[0]/serviceBookingVehicleModel")));
		assertTrue(expectedJsonData.get("input[0]/serviceBookingVehicleRego").equals(returnedJsonData.get("input[0]/serviceBookingVehicleRego")));
		assertTrue(expectedJsonData.get("input[0]/serviceBookingVehicleVIN").equals(returnedJsonData.get("input[0]/serviceBookingVehicleVIN")));
		assertTrue(expectedJsonData.get("input[0]/serviceBookingNumber").equals(returnedJsonData.get("input[0]/serviceBookingNumber")));
		assertTrue(expectedJsonData.get("input[0]/serviceBookingQuote").equals(returnedJsonData.get("input[0]/serviceBookingQuote")));
		assertTrue(expectedJsonData.get("input[0]/serviceBookingTypeofService").equals(returnedJsonData.get("input[0]/serviceBookingTypeofService")));
		assertTrue(returnedJsonData.get("input[0]/serviceBookingDropOffDate").isNull());
		assertTrue(returnedJsonData.get("input[0]/serviceBookingDropOffTime").isNull());
		assertTrue(returnedJsonData.get("input[0]/serviceBookingPickUpDate").isNull());
		assertTrue(returnedJsonData.get("input[0]/serviceBookingPickUpTime").isNull());
		assertTrue(expectedJsonData.get("input[0]/serviceBookingDealerName").equals(returnedJsonData.get("input[0]/serviceBookingDealerName")));
		assertTrue(expectedJsonData.get("input[0]/serviceBookingDealerPhoneNumber").equals(returnedJsonData.get("input[0]/serviceBookingDealerPhoneNumber")));
		assertTrue(expectedJsonData.get("input[0]/serviceBookingDealerPostCode").equals(returnedJsonData.get("input[0]/serviceBookingDealerPostCode")));
		assertTrue(expectedJsonData.get("input[0]/serviceBookingDealerState").equals(returnedJsonData.get("input[0]/serviceBookingDealerState")));
		assertTrue(expectedJsonData.get("input[0]/serviceBookingDealerAddress").equals(returnedJsonData.get("input[0]/serviceBookingDealerAddress")));
		assertTrue(expectedJsonData.get("input[0]/serviceBookingDealerSuburb").equals(returnedJsonData.get("input[0]/serviceBookingDealerSuburb")));
		assertTrue(returnedJsonData.get("input[0]/serviceBookingDealerEmail").isNull());
		assertTrue(expectedJsonData.get("input[0]/serviceBookingDealerWebsite").equals(returnedJsonData.get("input[0]/serviceBookingDealerWebsite")));
		assertTrue(expectedJsonData.get("input[0]/serviceBookingCustomerComments").equals(returnedJsonData.get("input[0]/serviceBookingCustomerComments")));
	}
	
	
	private String testSuccessfulImpl(String expectedResult, String apiRequest, String dealerResponse, String vehicleResponse) throws Exception {
		String result = ScriptRunBuilder.runner(scriptRunner)
				.payload("")
				.flowVar("vin", "JTMBFREV80D078597")
				.flowVar("myToyotaId", "MYT-000000c8")
				.flowVar("dmsBookingId", "TB16392302")
				.sessionVar("apiRequest", apiRequest)
				.sessionVar("dealerResponse", dealerResponse)
				.sessionVar("vehicleResponse", vehicleResponse)
				.run().toString();

		System.out.println(result);
		
		return result;
	}
}
