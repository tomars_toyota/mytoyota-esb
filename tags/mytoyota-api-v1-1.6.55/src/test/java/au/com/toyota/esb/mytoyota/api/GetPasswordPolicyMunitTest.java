package au.com.toyota.esb.mytoyota.api;

import static org.junit.Assert.assertTrue;
import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.util.IOUtils;
import org.mule.util.StringUtils;
import org.skyscreamer.jsonassert.JSONAssert;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;


/*
 * Munit test to unit test the functionality to post/add a device notification configurations
 * @author: ahuwait
 */
//@Ignore
public class GetPasswordPolicyMunitTest extends FunctionalMunitSuite {

	private static Properties adapterProperties = new Properties();
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"get-passwordpolicy.xml",
				"get-passwordpolicy-technical-adapter.xml",
				"config.xml"
		}, " ");
	}
	//@Test
	public void testOutboundCredentialGetPasswordPolicy() throws Exception {
		
		String requestPath = "/api/myToyota/v1/password/policy";
		
		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));
		
//		whenMessageProcessor("outbound-endpoint").ofNamespace("https")
		whenMessageProcessor("request").ofNamespace("http")
			.withAttributes(
			attribute("name").ofNamespace("doc").withValue("Get Password Policy from Sitecore"))
			.thenApply(new MockResponseTransformer() {
				@Override
				public MuleMessage transform(MuleMessage originalMessage) {
					MuleMessage newMessage = null;
					
					try {
						newMessage = originalMessage.createInboundMessage();
						newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
						String responsePayload = IOUtils.toString(getClass().getResourceAsStream("/in/get-password-policy-sitecore-response.json"));
						newMessage.setPayload(responsePayload);
					} catch (Exception e) {
						// catch exception statements
					}
					return newMessage;
				}
			});
		
		// Prepare the query parameters and message
		MuleEvent event = testEvent(requestPath);
		MuleMessage message = muleMessageWithPayload(null);
		
		Map<String, Object> queryParams = new HashMap<String,Object>();
		queryParams.put("language", "en");
		message.setProperty("http.query.params", queryParams, PropertyScope.INBOUND);
		message.setProperty("http.request.path", requestPath, PropertyScope.INBOUND);
		
		event.setMessage(message);	
		
		// Invoke the flow
		@SuppressWarnings("unused")
		MuleEvent output = runFlow("get:/password/policy:myToyota-config", event);
		System.out.println("******** event.getMessage(): "+event.getMessage().toString());
		if(event.getMessage().getProperty("Authorization", PropertyScope.OUTBOUND) != null)
			assertTrue(
					event.getMessage().getProperty("Authorization", PropertyScope.OUTBOUND).toString()
						.contains("Basic")
					);
		else
			assertTrue("Basic Auth property not found", false);
	}
	
	@Test
	public void testSuccessfulGetPasswordPolicy() throws Exception {
		
		String requestPath = "/api/myToyota/v1/password/policy";
		
		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));
		
		//whenMessageProcessor("outbound-endpoint").ofNamespace("https")
		whenMessageProcessor("request").ofNamespace("http")
			.withAttributes(
			attribute("name").ofNamespace("doc").withValue("Get Password Policy from Sitecore"))
			.thenApply(new MockResponseTransformer() {
				@Override
				public MuleMessage transform(MuleMessage originalMessage) {
					MuleMessage newMessage = null;
					
					try {
						newMessage = originalMessage.createInboundMessage();
						newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
						String responsePayload = IOUtils.toString(getClass().getResourceAsStream("/in/get-password-policy-sitecore-response.json"));
						newMessage.setPayload(responsePayload);
					} catch (Exception e) {
						// catch exception statements
					}
					return newMessage;
				}
			});
		
		// Prepare the query parameters and message
		MuleEvent event = testEvent(requestPath);
		MuleMessage message = muleMessageWithPayload(null);
		
		Map<String, Object> queryParams = new HashMap<String,Object>();
		queryParams.put("language", "en");
		message.setProperty("http.query.params", queryParams, PropertyScope.INBOUND);
		message.setProperty("http.request.path", requestPath, PropertyScope.INBOUND);
		
		event.setMessage(message);	
		
		// Invoke the flow
		MuleEvent output = runFlow("get:/password/policy:myToyota-config", event);
		
		System.out.println(output.getMessage().getPayload());
		
		// expected
		String expectedResult = IOUtils.toString(getClass().getResourceAsStream(
				"/out/get-password-policy-response.json"));
		
		JSONAssert.assertEquals(output.getMessage().getPayloadAsString(), expectedResult,true);
	}
	
	@Test
	public void testLoggingPointsGetPasswordPolicy() throws Exception {
		
		String requestPath = "/api/myToyota/v1/password/policy";
		
		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));
		
		//whenMessageProcessor("outbound-endpoint").ofNamespace("https")
		whenMessageProcessor("request").ofNamespace("http")
			.withAttributes(
			attribute("name").ofNamespace("doc").withValue("Get Password Policy from Sitecore"))
			.thenApply(new MockResponseTransformer() {
				@Override
				public MuleMessage transform(MuleMessage originalMessage) {
					MuleMessage newMessage = null;
					
					try {
						newMessage = originalMessage.createInboundMessage();
						newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
						String responsePayload = IOUtils.toString(getClass().getResourceAsStream("/in/get-password-policy-sitecore-response.json"));
						newMessage.setPayload(responsePayload);
					} catch (Exception e) {
						// catch exception statements
					}
					return newMessage;
				}
			});
		
		// Prepare the query parameters and message
		MuleEvent event = testEvent(requestPath);
		MuleMessage message = muleMessageWithPayload(null);
		
		Map<String, Object> queryParams = new HashMap<String,Object>();
		queryParams.put("language", "en");
		message.setProperty("http.query.params", queryParams, PropertyScope.INBOUND);
		message.setProperty("http.request.path", requestPath, PropertyScope.INBOUND);
		
		event.setMessage(message);	
		
		// Invoke the flow
		@SuppressWarnings("unused")
		MuleEvent output = runFlow("get:/password/policy:myToyota-config", event);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-password-policy.request"))
				.times(1);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-password-policy.response"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-password-policy.technical-adapter.request"))
				.atMost(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-password-policy.technical-adapter.response"))
				.atMost(1);
	}
	
	
	
	@Test
	public void testFailureGetPasswordPolicy() throws Exception {
		
		String requestPath = "/api/myToyota/v1/password/policy";
		
		// Prepare the query parameters and message
		MuleEvent event = testEvent(requestPath);
		MuleMessage message = muleMessageWithPayload(null);
		
		Map<String, Object> queryParams = new HashMap<String,Object>();
		queryParams.put("language", "en");
		message.setProperty("http.query.params", queryParams, PropertyScope.INBOUND);
		message.setProperty("http.request.path", requestPath, PropertyScope.INBOUND);
		
		event.setMessage(message);
		
		//whenMessageProcessor("outbound-endpoint").ofNamespace("https")
		whenMessageProcessor("request").ofNamespace("http")
			.withAttributes(
			attribute("name").ofNamespace("doc").withValue("Get Password Policy from Sitecore"))
			.thenThrow(new org.mule.module.apikit.exception.NotFoundException("Resource not found"));

		try {

			// Invoke the flow
			runFlow("get:/password/policy:myToyota-config", event);

		} catch (Exception e) {
			
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
					.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-password-policy.request"))
					.times(1);	
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
					.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-password-policy.response"))
					.times(0);
		
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
					.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-password-policy.technical-adapter.request"))
					.atMost(1);
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
					.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-password-policy.technical-adapter.response"))
					.times(0);
			
			assertTrue(e.getCause() instanceof org.mule.module.apikit.exception.NotFoundException);
		}

	}
	
	@Test
	public void testFailuresStatusCodes() throws Exception {

		int[] statusCodes = new int[] { 404, 405, 415, 406, 401, 500 };
		
		String requestPath = "/api/myToyota/v1/password/policy";
		
		MuleEvent event = testEvent(requestPath);

		for (int i = 0; i < statusCodes.length; i++) {
			
			final int currentStatusCode = statusCodes[i];
			
			//whenMessageProcessor("outbound-endpoint").ofNamespace("https")
			whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(
					attribute("name").ofNamespace("doc").withValue("Get Password Policy from Sitecore"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
	
						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", currentStatusCode, PropertyScope.INBOUND);
						} catch (Exception e) {
							// // catch exception statements
						}
	
						return newMessage;
					}
				});
			
			new StatusCodesTestingHelper().runFailureTest(currentStatusCode, null,
					"get:/password/policy:policy-technical-adapter", event);
		}
	}
}
