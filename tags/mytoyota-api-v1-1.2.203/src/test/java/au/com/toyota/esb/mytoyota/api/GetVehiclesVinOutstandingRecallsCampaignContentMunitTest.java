package au.com.toyota.esb.mytoyota.api;

import static org.junit.Assert.assertTrue;
import static org.mule.munit.common.mocking.Attribute.attribute;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.cache.InvalidatableCachingStrategy;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.skyscreamer.jsonassert.JSONAssert;

import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;



public class GetVehiclesVinOutstandingRecallsCampaignContentMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"get-vehicle-outstanding-recalls-campaignCode-content-technical-adapter.xml",
				"get-vehicle-outstanding-recalls-campaignCode-content.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulRequest() throws Exception {

		// Invalidate Cache as this unit test relies on different stubbed response from cached endpoint
		InvalidatableCachingStrategy cache = muleContext.getRegistry().<InvalidatableCachingStrategy>get("Generic_Caching_Strategy_Low_Currency");
		cache.invalidate();
		
		String vin = "6T153BK360X070769";
		String campaignCode = "BB123";
		String requestPath = "/api/myToyota/v1/vehicles/" + vin + "/outstandingRecalls/" + campaignCode + "/content";

		/*whenMessageProcessor("request").ofNamespace("http")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("Vehicle API GET vehicle by vin"))
			.thenApply(new MockResponseTransformer() {
			@Override
			public MuleMessage transform(MuleMessage originalMessage) {
				MuleMessage newMessage = null;
				
				try {
					newMessage = originalMessage.createInboundMessage();
					
					newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
					
					newMessage.setPayload(IOUtils.toString(getClass().getResourceAsStream("/in/get-vehicle-details-response.json")));
				} catch (Exception e) {
					// catch exception statements
				}
				
				return newMessage;
			}
		});*/

		MuleEvent event = testEvent(requestPath);
		event.setFlowVariable("vin", vin);
		event.setFlowVariable("campaignCode", campaignCode);

		MuleEvent output = runFlow("get:/vehicles/{vin}/outstandingRecalls/{campaignCode}/content:myToyota-config", event);

		String response = output.getMessage().getPayloadAsString();
		
		System.out.println(response);
		
		final String expected = IOUtils
				.toString(getClass().getResourceAsStream("/out/get-vehicles-vin-outstanding-recalls-campaignCode-content-info-response.json"));
		
		JSONAssert.assertEquals(expected, response, true);
		
	}
	
	@Test
	public void testSuccessfulTakataUrgentRequest() throws Exception {

		// Invalidate Cache as this unit test relies on different stubbed response from cached endpoint
		InvalidatableCachingStrategy cache = muleContext.getRegistry().<InvalidatableCachingStrategy>get("Generic_Caching_Strategy_Low_Currency");
		cache.invalidate();
		
		String vin = "6T153BK360X070769";
		String campaignCode = "BGG22";
		String requestPath = "/api/myToyota/v1/vehicles/" + vin + "/outstandingRecalls/" + campaignCode + "/content";

		/*whenMessageProcessor("request").ofNamespace("http")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("Vehicle API GET vehicle by vin"))
			.thenApply(new MockResponseTransformer() {
			@Override
			public MuleMessage transform(MuleMessage originalMessage) {
				MuleMessage newMessage = null;
				
				try {
					newMessage = originalMessage.createInboundMessage();
					
					newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
					
					newMessage.setPayload(IOUtils.toString(getClass().getResourceAsStream("/in/get-vehicle-details-response.json")));
				} catch (Exception e) {
					// catch exception statements
				}
				
				return newMessage;
			}
		});*/

		MuleEvent event = testEvent(requestPath);
		event.setFlowVariable("vin", vin);
		event.setFlowVariable("campaignCode", campaignCode);

		MuleEvent output = runFlow("get:/vehicles/{vin}/outstandingRecalls/{campaignCode}/content:myToyota-config", event);

		String response = output.getMessage().getPayloadAsString();
		
		System.out.println(response);
		
		final String expected = IOUtils
				.toString(getClass().getResourceAsStream("/out/get-vehicles-vin-outstanding-recalls-campaignCode-content-takata-urgent-response.json"));
		
		JSONAssert.assertEquals(expected, response, true);
		
	}
	
	@Test
	public void testSuccessfulTakataUrgentGGG35Request() throws Exception {

		// Invalidate Cache as this unit test relies on different stubbed response from cached endpoint
		InvalidatableCachingStrategy cache = muleContext.getRegistry().<InvalidatableCachingStrategy>get("Generic_Caching_Strategy_Low_Currency");
		cache.invalidate();
		
		String vin = "6T153BK360X070769";
		String campaignCode = "GGG35";
		String requestPath = "/api/myToyota/v1/vehicles/" + vin + "/outstandingRecalls/" + campaignCode + "/content";

		/*whenMessageProcessor("request").ofNamespace("http")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("Vehicle API GET vehicle by vin"))
			.thenApply(new MockResponseTransformer() {
			@Override
			public MuleMessage transform(MuleMessage originalMessage) {
				MuleMessage newMessage = null;
				
				try {
					newMessage = originalMessage.createInboundMessage();
					
					newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
					
					newMessage.setPayload(IOUtils.toString(getClass().getResourceAsStream("/in/get-vehicle-details-response.json")));
				} catch (Exception e) {
					// catch exception statements
				}
				
				return newMessage;
			}
		});*/

		MuleEvent event = testEvent(requestPath);
		event.setFlowVariable("vin", vin);
		event.setFlowVariable("campaignCode", campaignCode);

		MuleEvent output = runFlow("get:/vehicles/{vin}/outstandingRecalls/{campaignCode}/content:myToyota-config", event);

		String response = output.getMessage().getPayloadAsString();
		
		System.out.println(response);
		
		final String expected = IOUtils
				.toString(getClass().getResourceAsStream("/out/get-vehicles-vin-outstanding-recalls-campaignCode-content-takata-urgent-response.json"));
		
		JSONAssert.assertEquals(expected, response, true);
		
	}
	
	@Test
	public void testSuccessfulTakataNonUrgentRequest() throws Exception {

		// Invalidate Cache as this unit test relies on different stubbed response from cached endpoint
		InvalidatableCachingStrategy cache = muleContext.getRegistry().<InvalidatableCachingStrategy>get("Generic_Caching_Strategy_Low_Currency");
		cache.invalidate();
		
		String vin = "6T153BK360X070769";
		String campaignCode = "BGG23";
		String requestPath = "/api/myToyota/v1/vehicles/" + vin + "/outstandingRecalls/" + campaignCode + "/content";

		/*whenMessageProcessor("request").ofNamespace("http")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("Vehicle API GET vehicle by vin"))
			.thenApply(new MockResponseTransformer() {
			@Override
			public MuleMessage transform(MuleMessage originalMessage) {
				MuleMessage newMessage = null;
				
				try {
					newMessage = originalMessage.createInboundMessage();
					
					newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
					
					newMessage.setPayload(IOUtils.toString(getClass().getResourceAsStream("/in/get-vehicle-details-response.json")));
				} catch (Exception e) {
					// catch exception statements
				}
				
				return newMessage;
			}
		});*/

		MuleEvent event = testEvent(requestPath);
		event.setFlowVariable("vin", vin);
		event.setFlowVariable("campaignCode", campaignCode);

		MuleEvent output = runFlow("get:/vehicles/{vin}/outstandingRecalls/{campaignCode}/content:myToyota-config", event);

		String response = output.getMessage().getPayloadAsString();
		
		System.out.println(response);
		
		final String expected = IOUtils
				.toString(getClass().getResourceAsStream("/out/get-vehicles-vin-outstanding-recalls-campaignCode-content-takata-nonurgent-response.json"));
		
		JSONAssert.assertEquals(expected, response, true);
		
	}
	
	@Test
	public void testSuccessfulTakataNonUrgentAGG48Request() throws Exception {

		// Invalidate Cache as this unit test relies on different stubbed response from cached endpoint
		InvalidatableCachingStrategy cache = muleContext.getRegistry().<InvalidatableCachingStrategy>get("Generic_Caching_Strategy_Low_Currency");
		cache.invalidate();
		
		String vin = "6T153BK360X070769";
		String campaignCode = "AGG48";
		String requestPath = "/api/myToyota/v1/vehicles/" + vin + "/outstandingRecalls/" + campaignCode + "/content";

		/*whenMessageProcessor("request").ofNamespace("http")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("Vehicle API GET vehicle by vin"))
			.thenApply(new MockResponseTransformer() {
			@Override
			public MuleMessage transform(MuleMessage originalMessage) {
				MuleMessage newMessage = null;
				
				try {
					newMessage = originalMessage.createInboundMessage();
					
					newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
					
					newMessage.setPayload(IOUtils.toString(getClass().getResourceAsStream("/in/get-vehicle-details-response.json")));
				} catch (Exception e) {
					// catch exception statements
				}
				
				return newMessage;
			}
		});*/

		MuleEvent event = testEvent(requestPath);
		event.setFlowVariable("vin", vin);
		event.setFlowVariable("campaignCode", campaignCode);

		MuleEvent output = runFlow("get:/vehicles/{vin}/outstandingRecalls/{campaignCode}/content:myToyota-config", event);

		String response = output.getMessage().getPayloadAsString();
		
		System.out.println(response);
		
		final String expected = IOUtils
				.toString(getClass().getResourceAsStream("/out/get-vehicles-vin-outstanding-recalls-campaignCode-content-takata-nonurgent-response.json"));
		
		JSONAssert.assertEquals(expected, response, true);
		
	}
	
	@Test
	public void unsuccessfulRequest() throws Exception {
		
		// Invalidate Cache as this unit test relies on different stubbed response from cached endpoint
		InvalidatableCachingStrategy cache = muleContext.getRegistry().<InvalidatableCachingStrategy>get("Generic_Caching_Strategy_Low_Currency");
		cache.invalidate();
		
		String vin = "6T153BK360X070769";
		String campaignCode = null;
		String requestPath = "/api/myToyota/v1/vehicles/" + vin + "/outstandingRecalls/" + campaignCode + "/content";

		whenMessageProcessor("request").ofNamespace("http")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("Vehicle API GET vehicle by vin"))
			.thenThrow(new org.mule.module.apikit.exception.NotFoundException(requestPath));
		
		MuleEvent event = testEvent(requestPath);
		event.setFlowVariable("vin", vin);
		event.setFlowVariable("campaignCode", campaignCode);

		

		try {
			MuleEvent output = runFlow("get:/vehicles/{vin}/outstandingRecalls/{campaignCode}/content:myToyota-config", event);
		} catch (Exception e) {
			assertTrue("Should throw an exception", true);
		}

	}
	
	/*@Test
	public void testFailuresStatusCodes() throws Exception {
		
		int[] statusCodes = new int[] { 404, 405, 415, 406, 401, 500 };	
		
		String vin = "6T153BK360X070769";
		String campaignCode = null;
		String requestPath = "/api/myToyota/v1/vehicles/" + vin + "/outstandingRecalls/" + campaignCode + "/content";

		MuleEvent event = testEvent(requestPath);
		event.setFlowVariable("vin", vin);
		event.setFlowVariable("campaignCode", campaignCode);
		
		for (int i = 0; i < statusCodes.length; i++) {
			
			// Invalidate Cache as this unit test relies on different stubbed response from cached endpoint
			InvalidatableCachingStrategy cache = muleContext.getRegistry().<InvalidatableCachingStrategy>get("Generic_Caching_Strategy_Low_Currency");
			cache.invalidate();
			
			new StatusCodesTestingHelper().runFailureTest(statusCodes[i], "Vehicle API GET vehicle by vin",
					"get:/vehicles/{vin}/outstandingRecalls/{campaignCode}/content:myToyota-config", event);
		}
	}*/
}
