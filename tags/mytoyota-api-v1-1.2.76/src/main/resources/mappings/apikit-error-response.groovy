def exceptionPayload = [:]

exceptionPayload['success'] = false
exceptionPayload['errors']  = [[:]]

exceptionPayload['errors'][0]['code']        = exception?.getExceptionCode()
exceptionPayload['errors'][0]['description'] = exception?.getCauseException()?.getLocalizedMessage()?.toString()

return exceptionPayload