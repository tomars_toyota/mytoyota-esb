import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper
import au.com.toyota.esb.mytoyota.api.util.GroovyHelper

scRewards = []
if(flowVars['scRewards'])
	scRewards = new JsonSlurper().parseText(flowVars['scRewards'])?.rewards
claimedRewards = new JsonSlurper().parseText(payload)

rewardsList = []
scRewards.each { item ->
	item.isClaimed = item.id in claimedRewards.rewardId
	if(item?.cardFront?.cardImage)
		item.cardFront.cardImage='https://'+ sitecoreHost + item.cardFront.cardImage
	item.remove('isSuccessful')
	item.remove('message')
	rewardsList.add(item)
}

response = ["rewards":GroovyHelper.removeNulls(rewardsList, true)]
return prettyPrint(toJson(response))