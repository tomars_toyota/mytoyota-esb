import static groovy.json.JsonOutput.*
import org.mule.api.transport.PropertyScope;
import static au.com.toyota.esb.mytoyota.api.util.Encrypter.encrypt

sessionVars['plainPassword'] = payload.newPassword

request = [:]
request.password = payload.password
request.newPassword = payload.newPassword

token = message.getProperty('APISessionToken',PropertyScope.INBOUND)

if("true".equalsIgnoreCase(encryptionEnabled)) {
	println("token=" + token);
	println("secretKey=" + secretKey);
	token = encrypt(token, secretKey)
}
//[MYT-977]remove all saved APISessionTokens except the current one
request.data = ['myToyotaSessionToken': [token]]
	
return toJson(request)

