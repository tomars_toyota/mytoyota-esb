package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;

import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.custommonkey.xmlunit.XMLAssert;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.xml.sax.SAXException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

public class CreateTestDriveBookingsRequestMapper extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;

	public CreateTestDriveBookingsRequestMapper() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/sfdc-create-testDriveBookings-request-mapper.groovy");
	}

	@Test
	public void testSuccessfulRequestMapper() throws Exception {
		String payload = IOUtils.toString(getClass().getResourceAsStream("/in/post-users-me-testDriveBookings-request.json"));
		
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(payload)
				.flowVar("campaignCode", "RAV4-OFFER")
				.flowVar("myToyotaId", "TEST-11101")
				.binding("webFormTypeTestDrive", "Test_Drive")
				.run().toString();

		System.out.println(result);
		//System.out.println("------------------------");
		
		String expectedResult = IOUtils.toString(getClass().getResourceAsStream("/in/sfdc-create-testDriveBookings-request.xml"));
		XMLAssert.assertXMLEqual(expectedResult, result);
	}
}
