package au.com.toyota.esb.mytoyota.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.Properties;

import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.cache.InvalidatableCachingStrategy;
import org.mule.api.transport.PropertyScope;
import org.mule.module.apikit.exception.BadRequestException;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.util.IOUtils;
import org.skyscreamer.jsonassert.JSONAssert;

import com.jayway.jsonpath.JsonPath;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;

public class GetFuelstationsMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"get-fuelstations.xml",
				"get-fuelstations-technical-adapter.xml",
				"caltex-get-fuelstations.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulRequest() throws Exception {

		// Invalidate Cache as this unit test relies on different stubbed response from cached endpoint
		InvalidatableCachingStrategy cache = muleContext.getRegistry().<InvalidatableCachingStrategy>get("Generic_Caching_Strategy_High_Currency");
		cache.invalidate();
		
		String requestPath = "/api/myToyota/v1/fuelstations";
		
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Caltex API GET Fuel Stations"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						
						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							String responsePayload = IOUtils.toString(
									getClass().getResourceAsStream("/in/get-fuelstations-caltex-response.json"));
							newMessage.setPayload(responsePayload);
						} catch (Exception e) {
							// catch exception statements
						}
						return newMessage;
					}
				});
		
		MuleEvent event = testEvent(requestPath);
		MuleMessage message = muleMessageWithPayload(null);
		
		event.setMessage(message);
		
		// Invoke the flow
		MuleEvent output = runFlow("get:/fuelstations:myToyota-config", event);
		
		System.out.println(output.getMessage().getPayload());
		
		// expected
		String expectedResult = IOUtils.toString(getClass().getResourceAsStream(
				"/out/get-fuelstations-response.json"));
		
		JSONAssert.assertEquals(expectedResult, output.getMessage().getPayloadAsString(), true);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-fuelstations.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-fuelstations.response"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-fuelstations-technical-adapter.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-fuelstations-technical-adapter.response"))
				.times(1);
	}

}
