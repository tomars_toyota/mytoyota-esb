package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Properties;

import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;
import groovy.json.JsonSlurper;

public class GetUsersMeNotificationsMyKitServiceBookingMapperTest extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;
	private Properties props;

	public GetUsersMeNotificationsMyKitServiceBookingMapperTest() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/get-users-me-notifications-mykit-service-booking-mapping.groovy");
		//load properties
		props = new Properties();
		props.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));
	}

	@Test
	public void testSuccessfulRequestMapper() throws Exception {
		
		
		String auTimeZone = props.getProperty("service.booking.reminder.timezone");
		String reminderCutOffTime = props.getProperty("service.booking.reminder.cutoff");
		Map notificationObject = new HashMap<>();
		notificationObject.put("notifications", new ArrayList<>());
		
		List<String> vinList = new ArrayList<String>();
		vinList.add("5TDBK3FH80S016124");
		String result = ScriptRunBuilder.runner(scriptRunner)
				.payload(IOUtils
						.toString(getClass().getResourceAsStream("/out/get-users-me-services-response.json")))
				.flowVar("targetReminderDateTime", "2016-05-31 " + reminderCutOffTime)
				.sessionVar("vinList", vinList)
				.sessionVar("notication", notificationObject)
				.binding("auTimeZone", auTimeZone)
				.run().toString();

		System.out.println(result);

		
		// expected
		String expectedResult = IOUtils
				.toString(getClass().getResourceAsStream("/out/get-users-notifications-service-booking-mapping-response.json"));
		JSONAssert.assertEquals(expectedResult, result, true);
	}
	
	@Test
	public void testSuccessfulRequestWithCloseToCutOffServiceBookingMapper() throws Exception {
		
		
		String auTimeZone = props.getProperty("service.booking.reminder.timezone");
		String reminderCutOffTime = props.getProperty("service.booking.reminder.cutoff");
		Map notificationObject = new HashMap<>();
		notificationObject.put("notifications", new ArrayList<>());
		
		List<String> vinList = new ArrayList<String>();
		vinList.add("5TDBK3FH80S016124");
		String result = ScriptRunBuilder.runner(scriptRunner)
				.payload(IOUtils
						.toString(getClass().getResourceAsStream("/out/get-users-me-services-response.json")))
				.flowVar("targetReminderDateTime", "2016-05-31 " + reminderCutOffTime)
				.sessionVar("vinList", vinList)
				.sessionVar("notication", notificationObject)
				.binding("auTimeZone", auTimeZone)
				.run().toString();

		System.out.println(result);

		
		// expected
		String expectedResult = IOUtils
				.toString(getClass().getResourceAsStream("/out/get-users-notifications-service-booking-mapping-response.json"));
		JSONAssert.assertEquals(expectedResult, result, true);
	}
	
	
}
