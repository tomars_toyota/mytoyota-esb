import groovy.xml.MarkupBuilder
import groovy.json.JsonSlurper
import java.text.SimpleDateFormat

def writer = new StringWriter()
def xmlMarkup = new MarkupBuilder(writer)

if(payload instanceof org.mule.transport.NullPayload)
	payload = null

//def rootEle =
xmlMarkup.mkp.xmlDeclaration(version: "1.0", encoding: "UTF-8")
xmlMarkup.'ent:query' (
	'xmlns:ent' : "urn:enterprise.soap.sforce.com",
	// 'xmlns:xsi' : "http://www.w3.org/2001/XMLSchema-instance"
	) {
		'ent:queryString' ( flowVars['sfdcQueryString'] )
	}
//println '__________ payload to salesforce: ' + writer.toString()
return writer.toString()

// <urn:query>
//    <urn:queryString>SELECT ID,Name FROM Vehicle__c WHERE Name = 'JTNKU3JE90J063175'</urn:queryString>
// </urn:query>
// <urn:query>
//    <urn:queryString>SELECT ID,FirstName,LastName FROM Account WHERE My_Toyota_Id__c = 'MYT-00000620'</urn:queryString>
// </urn:query>
