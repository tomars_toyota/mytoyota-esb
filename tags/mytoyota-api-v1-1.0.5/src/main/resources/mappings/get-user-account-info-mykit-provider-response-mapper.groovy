import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper

def userAccount = new JsonSlurper().parseText(payload)

def response = [:]
def postalAddress = [:]
def applicationIdentifiers = [:]

response.login = userAccount?.login
response.password = null
response.title = userAccount?.title
response.firstName = userAccount?.firstName
response.lastName = userAccount?.lastName
response.email = userAccount?.email
response.mobile = userAccount?.mobile
response.landline = userAccount?.landline
response.dateOfBirth = userAccount?.dateOfBirth

//Map postal address from mykit
postalAddress.streetAddress = userAccount?.streetAddress
postalAddress.suburb = userAccount?.suburb
postalAddress.state = userAccount?.state
postalAddress.postcode = userAccount?.postcode
postalAddress.fullAddress = userAccount?.fullAddress
response.address = ["postal":postalAddress]

response.licenceId = userAccount?.licenceId
response.licenceState = userAccount?.licenceState
response.licenceCountry = userAccount?.licenceCountry
response.licenceExpiry = userAccount?.licenceExpiry
applicationIdentifiers.myToyota = ["id": userAccount?.myToyotaID]
response.applicationIdentifiers = applicationIdentifiers
//this needs to be fetched from mykit somehow
response.termsAndConditions = ["versionAccepted":"1.4"]
return toJson(response)