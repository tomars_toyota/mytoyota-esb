package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;
import java.util.HashMap;

import javax.script.ScriptException;

import org.custommonkey.xmlunit.XMLTestCase;
import org.junit.Test;
import org.xml.sax.SAXException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

public class PostUsersRegisterFinaliseAtImRequestMapperTest extends XMLTestCase {

	private ScriptRunner scriptRunner;

	public PostUsersRegisterFinaliseAtImRequestMapperTest() throws ScriptException, SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/post-register-user-im-provider-finalise-request-mapper.groovy");
	}

	@SuppressWarnings("unchecked")
	@Test
	public void testRequestMapper() throws Exception {

		String payload = "{\"UID\": \"36321682344328094734209\", "
				+ "\"regToken\": \"8756509438673458963049735954693860784508649078360474\"}";
		
		HashMap<String, Object> requestPayload = new HashMap<String, Object>();
		HashMap<String, Object> termsAndConditions = new HashMap<String, Object>();
		termsAndConditions.put("acceptedVersion", "1.0");
		requestPayload.put("termsAndConditions", termsAndConditions);
		
		HashMap<String, Object> privacyStatement = new HashMap<String, Object>();
		privacyStatement.put("acceptedVersion", "2.0");
		requestPayload.put("privacyStatement", privacyStatement);
	
		
		HashMap<String, Object> result = (HashMap<String, Object>) ScriptRunBuilder
				.runner(scriptRunner)
				.payload(payload)
				.sessionVar("requestPayload", requestPayload)
				.sessionVar("isRdr", true)
				.sessionVar("myToyotaID", "MYT-000004b7")
				.flowVar("anything","something")
				.run();

		System.out.println(result);
		
		assertEquals(
				(String) ((HashMap<String, Object>) (
							(HashMap<String, Object>) result.get("applicationIdentifiers"))
						.get("myToyota")).get("id"),
				"MYT-000004b7");
		assertEquals((String)((HashMap<String, Object>)result.get("termsAndConditions")).get("acceptedVersion"), "1.0");
		assertEquals((String)((HashMap<String, Object>)result.get("privacyStatement")).get("acceptedVersion"), "2.0");
	}
}
