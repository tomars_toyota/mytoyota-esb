package au.com.toyota.esb.mytoyota.api;

import static org.mule.munit.common.mocking.Attribute.attribute;

import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.util.IOUtils;
import org.mule.util.StringUtils;
import org.skyscreamer.jsonassert.JSONAssert;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;


/*
 * Munit test to unit test the functionality to get user notifications
 * @author: ibrahim.abouelela
 */
public class GetTCLatestMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"get-tc-latest.xml",
				"get-tc-latest-technical-adapter.xml",
				"config.xml"
		}, " ");
	}
	
	@Test
	public void testSuccessfulRequest() throws Exception {

		whenMessageProcessor("request").ofNamespace("http")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("Sitecore TC Latest"))
			.thenApply(new MockResponseTransformer() {
			@Override
			public MuleMessage transform(MuleMessage originalMessage) {
				MuleMessage newMessage = null;
				
				try {
					newMessage = originalMessage.createInboundMessage();
					
					newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
					
					newMessage.setPayload(getClass().getResourceAsStream("/in/get-tc-latest-response.json"));
				} catch (Exception e) {
					// catch exception statements
				}
				
				return newMessage;
			}
		});

		MuleEvent event = testEvent("");

		MuleEvent output = runFlow("get:/tc/latest:myToyota-config", event);
		String payload = output.getMessage().getPayloadAsString();
		System.out.println(payload);
		
		verifyCallOfMessageProcessor("log-message")
			.ofNamespace("esbutils")
			.withAttributes(
					attribute("name")
							.ofNamespace("doc")
							.withValue(
									"Log API Request"))
			.times(1);
		verifyCallOfMessageProcessor("log-message")
			.ofNamespace("esbutils")
			.withAttributes(
					attribute("name")
							.ofNamespace("doc")
							.withValue(
									"Log API Response"))
			.times(1);
		
		verifyCallOfMessageProcessor("log-message")
			.ofNamespace("esbutils")
			.withAttributes(
					attribute("name")
							.ofNamespace("doc")
							.withValue(
									"Log Request"))
			.times(1);
		
		verifyCallOfMessageProcessor("log-message")
			.ofNamespace("esbutils")
			.withAttributes(
					attribute("name")
							.ofNamespace("doc")
							.withValue(
									"Log Response"))
			.times(1);
		// expected
		String expectedResult = IOUtils.toString(getClass()
				.getResourceAsStream(
						"/out/get-tc-latest-api-response.json"));
		JSONAssert.assertEquals(expectedResult, payload, true);
	}
	
	@Test
	public void testFailuresStatusCodes() throws Exception {
		
		int[] statusCodes = new int[] { 404, 405, 415, 406, 401, 500 };	
		
		for (int i = 0; i < statusCodes.length; i++) {
			new StatusCodesTestingHelper().runFailureTest(statusCodes[i], "Sitecore TC Latest",
					"get:/tc/latest:myToyota-config");
		}
	}
}
