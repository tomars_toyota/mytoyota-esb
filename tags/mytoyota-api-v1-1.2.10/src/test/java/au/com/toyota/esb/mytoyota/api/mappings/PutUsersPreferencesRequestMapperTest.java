package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;
import java.util.HashMap;

import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.DefaultMuleMessage;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.context.DefaultMuleContextFactory;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.xml.sax.SAXException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

public class PutUsersPreferencesRequestMapperTest extends AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;
	
	public PutUsersPreferencesRequestMapperTest() throws ScriptException, SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/put-users-preferences-request-mapper.groovy");
	}
	
	//@Test
	public void testEmailRequestMapper() throws Exception {
		
		HashMap<String, String> queryParams = new HashMap<>();
		queryParams.put("email", "test@mail.com");
		queryParams.put("statusEmail", "true");
		
		muleContext = new DefaultMuleContextFactory().createMuleContext();
		MuleMessage message = new DefaultMuleMessage(null,muleContext);
		message.setProperty("http.query.params", queryParams, PropertyScope.INBOUND);
		
		ScriptRunBuilder
				.runner(scriptRunner)
				.sessionVar("dummyKey", "dummyValue")
				.flowVar("dummyKey", "dummyValue")
				.payload(IOUtils.toString(getClass().getResourceAsStream(
						"/in/put-users-preferences-request.json")))
				.message(message)
				.run();
	}
	
	//@Test
	public void testMobileRequestMapper() throws Exception {
		
		HashMap<String, String> queryParams = new HashMap<>();
		queryParams.put("mobile", "0412341234");
		
		muleContext = new DefaultMuleContextFactory().createMuleContext();
		MuleMessage message = new DefaultMuleMessage(null,muleContext);
		message.setProperty("http.query.params", queryParams, PropertyScope.INBOUND);
		
		ScriptRunBuilder
				.runner(scriptRunner)
				.sessionVar("dummyKey", "dummyValue")
				.flowVar("dummyKey", "dummyValue")
				.payload(null)
				.message(message)
				.run();
	}
}
