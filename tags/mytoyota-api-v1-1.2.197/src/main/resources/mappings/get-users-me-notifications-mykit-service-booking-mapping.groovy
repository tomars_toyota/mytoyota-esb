import java.util.*

def serviceBookingItems = []
def myKITResponse = payload
//println(sessionVars['vinList'])


def convertToDesiredTimeZone(String dateStr) {
	TimeZone auTimeZoneObj = TimeZone.getTimeZone(auTimeZone)
	def convertedDateTime = null
	try {
		convertedDateTime = Date.parse("yyyy-MM-dd HH:mm", dateStr, auTimeZoneObj)
	}
	catch (Exception e) {
		println "Not parseable: " + dateStr
	}
	
	return convertedDateTime
}

TimeZone auTimeZoneObj = TimeZone.getTimeZone(auTimeZone)
def targetReminderCutOffDateTime = convertToDesiredTimeZone(flowVars.targetReminderDateTime)

sessionVars['vinList']?.each {
	serviceBookingMap = [:]
	serviceBookingMap.key = it
	serviceBookingMap.value = []

	myKITResponse.each { service ->
		if(it == service.vehicle?.vin) {
			def bookingDropOffDateTime = convertToDesiredTimeZone(service.dropOffDate + " " + service.dropOffTime)
			if (bookingDropOffDateTime != null && targetReminderCutOffDateTime != null) {
				if (bookingDropOffDateTime <= targetReminderCutOffDateTime) {
					serviceBookingMap.value << service.bookingId
				}
				else {
					println "bookingDropOffDateTime: " + bookingDropOffDateTime + " is greater than " + targetReminderCutOffDateTime
				}
			}
			else {
				// no filtering add all
				serviceBookingMap.value << service.bookingId
				println "bookingDropOffDateTime is: " + bookingDropOffDateTime
				println "targetReminderCutOffDateTime is: " + targetReminderCutOffDateTime
			}

			
		}
	}

	serviceBookingItems << serviceBookingMap
}

serviceBookingNotification=[:]
serviceBookingNotification.type = "service-booking"
serviceBookingNotification.items = serviceBookingItems

sessionVars['notication'].notifications << serviceBookingNotification

//println(serviceBookingNotification)

