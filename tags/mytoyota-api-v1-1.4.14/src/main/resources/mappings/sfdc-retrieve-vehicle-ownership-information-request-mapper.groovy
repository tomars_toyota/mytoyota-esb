import groovy.xml.MarkupBuilder

def writer = new StringWriter()
def xmlMarkup = new MarkupBuilder(writer)


xmlMarkup.mkp.xmlDeclaration(version: "1.0", encoding: "UTF-8")
xmlMarkup.'sp:retrieveVehicleOwnershipInformation' (
	'xmlns:sp' : "http://soap.sforce.com/schemas/class/SP_UserRegWSTransactionStructure"
	) {
		'sp:request'{
			'sp:myToyotaId' (sessionVars['myToyotaID'])
		}	
	}
return writer.toString()

// <sp:retrieveVehicleOwnershipInformation>
//    <sp:request>
//       <sp:myToyotaId>TEST-000002ea</sp:myToyotaId>
//    </sp:request>
// </sp:retrieveVehicleOwnershipInformation>