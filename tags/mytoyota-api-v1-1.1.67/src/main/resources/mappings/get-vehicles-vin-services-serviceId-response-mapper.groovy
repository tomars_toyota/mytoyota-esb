import groovy.json.JsonSlurper
import static groovy.json.JsonOutput.*
import au.com.toyoya.esb.mytoyota.api.util.GroovyHelper


def mapService(service) {
	mappedService = [:]
	mappedService.bookingId = service.bookingId
	mappedService.dropOffDate = service.dropOffDate
	mappedService.dropOffTime = service.dropOffTime
	mappedService.pickUpDate = service.pickUpDate
	mappedService.pickUpTime = service.pickUpTime 
	mappedService.status = service.status
	mappedService.notes = service.notes
	mappedService.alertType = service.confirmationType
	mappedService.totalServicePrice = service.totalServicePrice
	mappedService.waitAtDealerFlag = service.waitAtDealerFlag
	mappedService.expressServiceFlag = service.expressServiceFlag
	mappedService.courtesyBusFlag = service.courtesyBusFlag
	mappedService.loanVehicleFlag = service.loanVehicleFlag

	mappedService.dealerDetails = service.dealer

	//replace dmsId and branchCode with dmsID and branchId
	mappedService.dealerDetails.dmsId = service.dealer.dmsID
	mappedService.dealerDetails.branchCode = service.dealer.branchId
	mappedService.dealerDetails.remove('dmsID')
	mappedService.dealerDetails.remove('branchId')

	mappedService.contactDetail = service.contactDetail
	mappedService.vehicle = service.vehicle
	mappedService.serviceOperation = service.serviceOperation
	mappedService.vehicleOdo = service.odometer

	return mappedService
}

//println '___________ payload: '+payload
def myKitResponse = new JsonSlurper().parseText(payload)

def response = [:]

if(myKitResponse instanceof Map) {
	response = mapService(myKitResponse)
} else if(myKitResponse instanceof List) {
	
	servicesList = []

	myKitResponse.each { service ->
		servicesList.add(mapService(service))
	}
	response.services = servicesList
}

response = GroovyHelper.removeNulls(response)

return prettyPrint(toJson(response))