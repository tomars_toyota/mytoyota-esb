import groovy.json.JsonSlurper
import org.mule.api.transport.PropertyScope
	
def userAccount = new JsonSlurper().parseText(payload)

def responseMap = [:]

responseMap.firstName = userAccount?.firstName
responseMap.lastName = userAccount?.lastName
sessionVars['myToyotaId'] = userAccount?.myToyotaID

return responseMap