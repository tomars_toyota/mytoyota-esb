import groovy.json.JsonSlurper

def capitaliseNames(name) {
	newName = ''
	name.tokenize().each { newName += it.toLowerCase().capitalize() + ' '}
	return newName.trim()
}

// Map First/Last from first record returned from /orders matching the email address
orders = new JsonSlurper().parseText(payload)?.orders
def responseMap = [:]
def record = orders?.iterator()?.next() // First map entry

responseMap.firstName = record?.firstName ? capitaliseNames(record.firstName) : null 
responseMap.lastName = record?.lastName ? capitaliseNames(record.lastName) : null 
responseMap.email = sessionVars['email']

return responseMap