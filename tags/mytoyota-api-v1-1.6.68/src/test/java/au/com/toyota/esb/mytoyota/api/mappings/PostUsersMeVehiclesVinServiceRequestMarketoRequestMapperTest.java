package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;
import java.util.HashMap;

import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.json.JSONObject;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;
import org.mule.module.json.JsonData;
import static org.junit.Assert.assertTrue;

public class PostUsersMeVehiclesVinServiceRequestMarketoRequestMapperTest extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;

	public PostUsersMeVehiclesVinServiceRequestMarketoRequestMapperTest() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner
				.createScriptRunner("/mappings/post-users-me-vehicles-vin-services-serviceRequest-marketo-request-mapper.groovy");
	}
	
	private static String DEFAULT_EXPECTED_RESULT;
	private static String DEFAULT_API_REQUEST;
	private static String DEFAULT_SERVICE_REQUEST;
	private static String DEFAULT_DEALER_RESPONSE;
	
	@BeforeClass
	public static void once() throws Exception {
		
		DEFAULT_EXPECTED_RESULT = IOUtils.toString(PostUsersMeVehiclesVinServicesMarketoRequestMapperTest.class
				.getResourceAsStream("/out/post-users-me-vehicles-vin-services-serviceRequest-marketo-request.json"));
		DEFAULT_API_REQUEST = IOUtils.toString(PostUsersMeVehiclesVinServicesMarketoRequestMapperTest.class
				.getResourceAsStream("/in/post-users-me-vehicles-vin-services-request.json"));
		DEFAULT_SERVICE_REQUEST = IOUtils.toString(PostUsersMeVehiclesVinServicesMarketoRequestMapperTest.class
				.getResourceAsStream("/out/post-users-me-vehicles-vin-services-serviceRequest-request.json"));
		DEFAULT_DEALER_RESPONSE = IOUtils.toString(PostUsersMeVehiclesVinServicesMarketoRequestMapperTest.class
				.getResourceAsStream("/in/get-dealers-by-dealer-code-api-response.json"));
		
	}
	
	@Test
	public void testSuccessful() throws Exception {
		
		String expectedResult = DEFAULT_EXPECTED_RESULT;
		String apiRequest = DEFAULT_API_REQUEST;
		@SuppressWarnings("unchecked")
		HashMap<String,Object> serviceRequest = new ObjectMapper().readValue(DEFAULT_SERVICE_REQUEST, HashMap.class);
		@SuppressWarnings("unchecked")
		HashMap<String,Object> dealerResponse = new ObjectMapper().readValue(DEFAULT_DEALER_RESPONSE, HashMap.class);
		
		String result = testSuccessfulImpl(expectedResult, apiRequest, dealerResponse, serviceRequest);
	}
	
	
	@Test
	public void testFallbackToMainPhoneNumberIfServicePhoneNumberNotPresent() throws Exception {
		
		//Arrange
		String apiRequest = DEFAULT_API_REQUEST;
		@SuppressWarnings("unchecked")
		HashMap<String,Object> serviceRequest = new ObjectMapper().readValue(DEFAULT_SERVICE_REQUEST, HashMap.class);
				
		JSONObject obj = null;
		obj = new JSONObject(DEFAULT_DEALER_RESPONSE);		
		obj.getJSONArray("sites").getJSONObject(0).getJSONObject("phone").put("service", "");
		
		@SuppressWarnings("unchecked")
		HashMap<String,Object> dealerResponse = new ObjectMapper().readValue(obj.toString(), HashMap.class);
		System.out.println("dealerResponse::" + dealerResponse);
		
		obj = new JSONObject(DEFAULT_EXPECTED_RESULT);
		obj.getJSONArray("input").getJSONObject(0).put("serviceRequestDealerPhoneNumber", "02 8045 5127");
		String expectedResult = obj.toString();
		
		//Act
		String result = testSuccessfulImpl(expectedResult, apiRequest, dealerResponse, serviceRequest);

		//Assert
		JSONAssert.assertEquals(expectedResult, result, true);
	}
	
	private String testSuccessfulImpl(String expectedResult, String apiRequest, HashMap<String,Object> dealerResponse, HashMap<String,Object> serviceRequest) throws Exception {
		String result = ScriptRunBuilder.runner(scriptRunner)
				.payload("")
				.flowVar("vin", "JTMBFREV80D078597")
				.flowVar("myToyotaId", "MYT-000000c8")
				.sessionVar("apiRequest", apiRequest)
				.sessionVar("dealerResponse", dealerResponse)
				.sessionVar("serviceRequest", serviceRequest)
				.run().toString();

		System.out.println(result);
		
		return result;
	}
}
