package au.com.toyota.esb.mytoyota.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mule.munit.common.mocking.Attribute.attribute;

import org.apache.commons.lang.StringUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.cache.InvalidatableCachingStrategy;
import org.mule.api.transport.PropertyScope;
import org.mule.module.apikit.exception.BadRequestException;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.json.JSONObject;

import com.jayway.jsonpath.JsonPath;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;

public class GetVehiclesMunitTest extends FunctionalMunitSuite{
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"get-vehicles.xml",
				"get-vehicles-technical-adapter.xml",
				"myService-get-access-token.xml",
				"config.xml"
		}, " ");
	}	
	
	@Test
	public void testSuccessfulGetVehicleArrayByRegistrationNumberAndState() throws Exception {

		// Invalidate Cache as this unit test relies on different stubbed response from cached endpoint
		InvalidatableCachingStrategy cache = muleContext.getRegistry().<InvalidatableCachingStrategy>get("Generic_Caching_Strategy_Low_Currency");
		cache.invalidate();
		
		String registrationNumber = "1CN5RI";
		String state = "VIC";
		String requestPath = "/api/myToyota/v1/vehicles?" + registrationNumber + "&" + state;
		
		mockAccessToken();	
		
		whenMessageProcessor("request").ofNamespace("http")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("PCF Service API GET Vehicles"))
			.thenApply(new MockResponseTransformer() {
			@Override
			public MuleMessage transform(MuleMessage originalMessage) {
				MuleMessage newMessage = null;
				
				try {
					newMessage = originalMessage.createInboundMessage();
					newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);					
					newMessage.setPayload(getClass().getResourceAsStream("/in/myService-get-vehicles-response.json"));
				} catch (Exception e) {
					// catch exception statements
				}
				
				return newMessage;
			}
		});
		
		MuleEvent event = testEvent(requestPath);	
		MuleEvent output = runFlow("get:/vehicles:myToyota-config", event);

		System.out.println(output.getMessage().getPayload());
		JSONObject obj = new JSONObject(output.getMessage().getPayloadAsString());		
		
		String expectedVIN = "JTNKU3JE50J069491";
		
		assertEquals("Just 1 vehicle needs to be returned", 1, obj.getJSONArray("vehicles").length());
		assertEquals("VIN should be correct", expectedVIN, obj.getJSONArray("vehicles").getJSONObject(0).getString("vin"));
	}
	
	@Test
	public void testNotFoundGetVehicleArrayByRegistrationNumberAndState() throws Exception {

		// Invalidate Cache as this unit test relies on different stubbed response from cached endpoint
		InvalidatableCachingStrategy cache = muleContext.getRegistry().<InvalidatableCachingStrategy>get("Generic_Caching_Strategy_Low_Currency");
		cache.invalidate();
		
		String registrationNumber = "1CN5";
		String state = "VIC";
		String requestPath = "/api/myToyota/v1/vehicles?" + registrationNumber + "&" + state;
		
		mockAccessToken();
		
		
		whenMessageProcessor("request").ofNamespace("http")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("PCF Service API GET Vehicles"))
			.thenApply(new MockResponseTransformer() {
			@Override
			public MuleMessage transform(MuleMessage originalMessage) {
				MuleMessage newMessage = null;
				
				try {
					newMessage = originalMessage.createInboundMessage();
					newMessage.setProperty("http.status", 404, PropertyScope.INBOUND);					
					newMessage.setPayload(getClass().getResourceAsStream("/in/myService-get-vehicles-404-response.json"));
				} catch (Exception e) {
					// catch exception statements
				}
				
				return newMessage;
			}
		});
		
		MuleEvent event = testEvent(requestPath);	
		MuleEvent output = runFlow("get:/vehicles:myToyota-config", event);

		System.out.println(output.getMessage().getPayload());
		JSONObject obj = new JSONObject(output.getMessage().getPayloadAsString());		
			
		assertEquals("Http Status should be 200", "200", output.getMessage().getOutboundProperty("http.status"));
		assertEquals("Payload should be empty", 0, obj.getJSONArray("vehicles").length());
	}


	private void mockAccessToken() {
		whenMessageProcessor("request").ofNamespace("http")
		.withAttributes(attribute("name").ofNamespace("doc").withValue("myService API POST Access Token"))
		.thenApply(new MockResponseTransformer() {
			@Override
			public MuleMessage transform(MuleMessage originalMessage) {
				MuleMessage newMessage = null;
				
				try {
					newMessage = originalMessage.createInboundMessage();
					newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);					
					newMessage.setPayload(getClass().getResourceAsStream("/in/myService-get-access-token.json"));
				} catch (Exception e) {
					// catch exception statements
				}
				
				return newMessage;
			}
		});
	}

}
