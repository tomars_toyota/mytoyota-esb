package au.com.toyota.esb.mytoyota.api;

import static org.junit.Assert.*;
import static com.jayway.jsonpath.Criteria.*;
import static com.jayway.jsonpath.Filter.*;
import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.List;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.munit.runner.functional.FunctionalMunitSuite;

import com.jayway.jsonpath.JsonPath;

//@Ignore
public class GetUserVehiclesMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"get-user-vehicles.xml",
				"get-user-vehicles-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulRequest() throws Exception {

		String requestPath = "/api/myToyota/v1/users/me/vehicles";

		String apiResponse = IOUtils.toString(getClass().getResourceAsStream("/in/user-get-vehicle-list-response.json"));
		
		
		whenMessageProcessor("contains").ofNamespace("objectstore")
		.withAttributes(attribute("name").ofNamespace("doc").withValue("Check Auth Token in ObjectStore"))
		.thenReturn(muleMessageWithPayload(true));
		
		whenMessageProcessor("retrieve").ofNamespace("objectstore")
		.withAttributes(attribute("name").ofNamespace("doc").withValue("Get Auth Token from ObjectStore"))
		.thenReturnSameEvent();
		
		whenMessageProcessor("request").ofNamespace("http")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("My Toyota App API GET Retrieve Users Vehicles"))
			.thenReturn(muleMessageWithPayload(apiResponse));

		MuleEvent event = testEvent(requestPath);

		MuleEvent output = runFlow("get:/users/me/vehicles:myToyota-config", event);

		String payload = output.getMessage().getPayloadAsString();
		System.out.println(payload);

		List<String> vehicles = JsonPath.read(payload, "$.vehicles");
		assertEquals(2, vehicles.size());
		
		assertEquals("JTNKU3JE60J021675", JsonPath.read(payload, "$.vehicles[0].vin"));
		assertEquals("JTNKU3JE60J021676", JsonPath.read(payload, "$.vehicles[1].vin"));
	}
	
	@Test
	public void unsuccessfulRequest() throws Exception {

		String requestPath = "/api/myToyota/v1/users/me/vehicles";

		whenMessageProcessor("contains").ofNamespace("objectstore")
		.withAttributes(attribute("name").ofNamespace("doc").withValue("Check Auth Token in ObjectStore"))
		.thenReturn(muleMessageWithPayload(true));
		
		whenMessageProcessor("retrieve").ofNamespace("objectstore")
		.withAttributes(attribute("name").ofNamespace("doc").withValue("Get Auth Token from ObjectStore"))
		.thenReturnSameEvent();
		
		whenMessageProcessor("request").ofNamespace("http")
		.withAttributes(attribute("name").ofNamespace("doc").withValue("My Toyota App API GET Retrieve Users Vehicles"))
			.thenThrow(new org.mule.module.apikit.exception.NotFoundException(requestPath));
		
		MuleEvent event = testEvent(requestPath);
//		event.setFlowVariable("vin", vin);

		try {
			runFlow("get:/users/me/vehicles:myToyota-config", event);
		} catch (Exception e) {
			assertTrue(e.getCause() instanceof org.mule.module.apikit.exception.NotFoundException);
			assertEquals(requestPath, e.getCause().getMessage());
		}

	}
	
}
