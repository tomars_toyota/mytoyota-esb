package au.com.toyota.esb.mytoyota.api.metrics;

import org.apache.log4j.Logger;
import org.mule.api.context.notification.EndpointMessageNotificationListener;
import org.mule.context.notification.EndpointMessageNotification;

public class MuleEndpointListener
		implements EndpointMessageNotificationListener<EndpointMessageNotification> {
	private Logger log = Logger.getLogger("esb.mytoyota-api-v1.instrument");
	private long startTime = 0;

	
	public MuleEndpointListener(){
		log.info("MuleEndpointListener is up...");
	}
	
	@Override
	public void onNotification(EndpointMessageNotification notification) {
		
		String uri = notification.getImmutableEndpoint().getEndpointURI().getPath();
		long threadId = Thread.currentThread().getId();
		log.info("[dir=INBOUND][threadId=" + threadId + "]uri = " + uri);
		log.info("[dir=INBOUND][threadId=" + threadId + "]notification.getClass() = " + notification.getClass());
		log.info("[dir=INBOUND][threadId=" + threadId + "]notification = " + notification);
		log.info("[dir=INBOUND][threadId=" + threadId + "]notification.getAction() = " + notification.getAction());
		log.info("[dir=INBOUND][threadId=" + threadId + "]notification.getActionName() = " + notification.getActionName());
		log.info("[dir=INBOUND][threadId=" + threadId + "]notification.EVENT_NAME = " + notification.EVENT_NAME);
		log.info("[dir=INBOUND][threadId=" + threadId + "]notification.getType() = " + notification.getType());
		log.info("[dir=INBOUND][threadId=" + threadId + "]notification..getImmutableEndpoint().getAddress()() = " + notification.getImmutableEndpoint().getAddress());
		log.info("[dir=INBOUND][threadId=" + threadId + "]notification.getAction() = " + notification.getImmutableEndpoint());
		log.info("[dir=INBOUND][threadId=" + threadId + "]notification.transactionId = " + notification.getSource().getSessionProperty("transactionId"));
		log.info("[dir=INBOUND][threadId=" + threadId + "]notification.getAction() = " + notification.getAction());
		log.info("[dir=INBOUND][threadId=" + threadId + "]notification.getAction() = " + notification.getAction());
		log.info("[dir=INBOUND][threadId=" + threadId + "]notification.getAction() = " + notification.getAction());
		log.info("[dir=INBOUND][threadId=" + threadId + "]notification.getAction() = " + notification.getAction());
		
	}
}
