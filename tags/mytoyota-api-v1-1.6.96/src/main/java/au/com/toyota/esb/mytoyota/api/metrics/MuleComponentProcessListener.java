package au.com.toyota.esb.mytoyota.api.metrics;

import org.apache.log4j.Logger;
import org.mule.api.context.notification.MessageProcessorNotificationListener;
import org.mule.api.processor.MessageProcessor;
import org.mule.context.notification.MessageProcessorNotification;
import org.mule.module.http.internal.request.DefaultHttpRequester;

public class MuleComponentProcessListener
		implements MessageProcessorNotificationListener<MessageProcessorNotification> {
	private Logger log = Logger.getLogger("esb.mytoyota-api-v1.instrument");
	private long startTime = 0;
	private boolean logMetrics;

	@Override
	public void onNotification(MessageProcessorNotification notification) {
		log.debug("In MuleComponentProcessListener.onNotification: " + logMetrics);		
		if(logMetrics) {
			MessageProcessor process = notification.getProcessor();
			String className = process.getClass().getSimpleName();
			boolean isOutbound = process instanceof DefaultHttpRequester;
	
			if (className.equalsIgnoreCase("DefaultHttpRequester") && isOutbound) {
				try {
					DefaultHttpRequester def = (DefaultHttpRequester) process;
					String transactionId = notification.getSource().getSession().getProperty("transactionId");
					if (notification.getAction() == MessageProcessorNotification.MESSAGE_PROCESSOR_PRE_INVOKE) {
						startTime = System.currentTimeMillis();
					}
					if (notification.getAction() == MessageProcessorNotification.MESSAGE_PROCESSOR_POST_INVOKE) {
						long executionTime = System.currentTimeMillis() - startTime;
						log.info("[METRICS][metric=RT][dir=OUTBOUND][threadId=" + Thread.currentThread().getId() + "][transactionId="+ transactionId +"][host=" + def.getHost()  +"][path=" + def.getPath() + "][method=" + def.getMethod() + "][took(ms)=" + executionTime + "]");
					}
				} catch (Exception e) {
				}
			}
		}
	}
	
	public boolean isLogMetrics() {
		return logMetrics;
	}

	public void setLogMetrics(boolean logMetrics) {
		this.logMetrics = logMetrics;
	}
}

