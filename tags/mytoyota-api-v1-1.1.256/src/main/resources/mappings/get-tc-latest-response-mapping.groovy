import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper
def response = [:]
response.termsAndConditions = []

JsonSlurper js = new JsonSlurper()
def jsonObject = js.parseText(payload)

jsonObject.each { Map tc ->
	
	def esbTc = [:]
	esbTc.version = tc.version
	esbTc.content = tc.content
	response.termsAndConditions << esbTc
	
}

return toJson(response)