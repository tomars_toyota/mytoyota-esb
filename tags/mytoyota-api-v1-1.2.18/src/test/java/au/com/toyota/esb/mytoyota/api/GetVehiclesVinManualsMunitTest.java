package au.com.toyota.esb.mytoyota.api;

import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mule.munit.common.mocking.Attribute.attribute;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.cache.InvalidatableCachingStrategy;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.SimplifiedMockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;

public class GetVehiclesVinManualsMunitTest extends FunctionalMunitSuite {

	@Override
	protected String getConfigResources() {
		return StringUtils
				.join(new String[] { 
						"get-vehicle-vin-manuals.xml", 
						"get-vehicle-vin-manuals-technical-adapter.xml",
						"get-vehicle-details-technical-adapter.xml", 
						"config.xml" }
				, " ");
	}

	@Test
	public void testSuccessfulRequest() throws Exception {

		// Invalidate Cache before running unit test
		InvalidatableCachingStrategy cache = muleContext.getRegistry().<InvalidatableCachingStrategy>get("Generic_Caching_Strategy_High_Currency");
		cache.invalidate();
				
		final String apiResponse = IOUtils
				.toString(getClass().getResourceAsStream("/out/get-vehicles-vin-manuals-api-response.json"));
		String vin = "6T1BK3FK00X246812";
		String requestPath = "/api/myToyota/v1/vehicles/" + vin + "manuals";
		
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Vehicle API GET vehicle by vin"))
				.thenApply(new SimplifiedMockResponseTransformer(200, IOUtils.toString(
						getClass().getResourceAsStream("/in/get-vehicles-vin-toyota-manuals-vehicle-details-response.json"))));

		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Toyota Manuals API"))
				.thenApply(new SimplifiedMockResponseTransformer(200, IOUtils.toString(
						getClass().getResourceAsStream("/in/get-vehicles-vin-toyota-manuals-response.json"))));

		MuleEvent event = testEvent(requestPath);
		event.setFlowVariable("vin", vin);

		MuleEvent output = runFlow("get:/vehicles/{vin}/manuals:myToyota-config", event);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("category")
				.withValue("esb.mytoyota-api-v1.get-vehicle-vin-manuals.request")).times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("category")
				.withValue("esb.mytoyota-api-v1.get-vehicle-vin-manuals.response")).times(1);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("category")
				.withValue("esb.mytoyota-api-v1.get-vehicle-vin-manuals-technical-adapter.request")).times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("category")
				.withValue("esb.mytoyota-api-v1.get-vehicle-vin-manuals-technical-adapter.response")).times(1);

		System.out.println(output.getMessage().getPayload());
		
		assertJsonEquals(apiResponse,output.getMessage().getPayloadAsString());
	}

	@Test
	public void testNotFoundExcaptionRequest() throws Exception {

		// Invalidate Cache before running unit test
		InvalidatableCachingStrategy cache = muleContext.getRegistry().<InvalidatableCachingStrategy>get("Generic_Caching_Strategy_High_Currency");
		cache.invalidate();
				
		final String apiResponse = IOUtils
				.toString(getClass().getResourceAsStream("/out/get-vehicles-vin-manuals-api-response.json"));
		String vin = "6T1BK3FK00X246812";
		String requestPath = "/api/myToyota/v1/vehicles/" + vin + "manuals";
		
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Vehicle API GET vehicle by vin"))
				.thenApply(new SimplifiedMockResponseTransformer(200, IOUtils.toString(
						getClass().getResourceAsStream("/in/get-vehicles-vin-toyota-manuals-vehicle-details-response.json"))));

		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Toyota Manuals API"))
				.thenApply(new SimplifiedMockResponseTransformer(200, "[]"));

		try {
			MuleEvent event = testEvent(requestPath);
			event.setFlowVariable("vin", vin);
			
			MuleEvent output = runFlow("get:/vehicles/{vin}/manuals:myToyota-config", event);
			
		} catch(Exception e) {
			
			System.out.println("**** Exception: "+e.getCause());
			
			assertTrue(e.getCause() instanceof javax.script.ScriptException);
			assertEquals("org.mule.module.apikit.exception.NotFoundException: No vehicle manuals found", e.getCause().getMessage());
		
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("category")
					.withValue("esb.mytoyota-api-v1.get-vehicle-vin-manuals.request")).times(1);
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("category")
					.withValue("esb.mytoyota-api-v1.get-vehicle-vin-manuals.response")).times(0);
			
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("category")
					.withValue("esb.mytoyota-api-v1.get-vehicle-vin-manuals-technical-adapter.request")).times(1);
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("category")
					.withValue("esb.mytoyota-api-v1.get-vehicle-vin-manuals-technical-adapter.response")).times(0);
		}
	}
}
