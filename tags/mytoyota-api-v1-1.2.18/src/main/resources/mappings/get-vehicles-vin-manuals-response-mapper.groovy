import groovy.json.JsonSlurper
import static groovy.json.JsonOutput.*
import java.text.DateFormat
import java.text.SimpleDateFormat

DateFormat titleDateFormat = new SimpleDateFormat("MMM yy");
DateFormat productionDateFormat = new SimpleDateFormat("yyyy-MM");

def manualsResponse
if (payload != org.mule.transport.NullPayload) {
	manualsResponse = new JsonSlurper().parseText(payload)
} else {
	return ""
}

// Extract dates from a string with the following appearance:
// Aurion 50 Series Owner's Manual (Apr 15 - Apr 16)
def getDateListFromTitle = { title ->
    def stringList = []
    def dateList = []
    
    // Extract Apr 15 - Apr 16
    def s = title.substring( title.indexOf("(")+1, title.lastIndexOf(")") )
    
    // Split string Apr 15 - Apr 16 into list [Apr 15 , Apr 16]
    stringList[0] = s.split("-")[0]?.trim()
    stringList[1] = s.split("-")[1]?.trim()
    
    // Parse strings into Date objects and store in list
    stringList.each { dateString -> 
        // Special handling for 'Current' to set to today's date, otherwise parse
        if( dateString?.toLowerCase().contains('current') ) {
            Date d = new Date().clearTime()
            dateList.add(d)
        } else {
            dateList.add( titleDateFormat.parse(dateString) )
        }
    }
    return dateList
}

def returnType = { t ->
    if (t == 'owners-manuals') return 'manual'
    else if (t == 'warranty-service-booklets') return 'warranty'
    else if (t == 'service-repair-manual') return 'repair'
    else return null
}

def response = [:]
def manualsList = []

manualsResponse?.each { manual ->
    // Sort through all manual entries that contain Katashiki Code substring
    if (sessionVars['katashikiCode'].contains( manual?.modelCode )) {
        def productionDate = productionDateFormat.parse(sessionVars['production'])
        // Parse the Title to extract the dates
        def dateList = getDateListFromTitle ( manual?.title )
        if( productionDate >= dateList[0] && productionDate < dateList[1]) {
            def map = [:]
            map['type'] = returnType( manual?.documentType )
            map['title'] = manual?.prettyName
            map['link'] = basePath+manual?.prettyName
            
            manualsList.add(map)
        }
    } // otherwise no matching manual found
}

// If there was a manual found, add the vehicle description and production date
if(manualsList.size() > 0) {
    response['vehicleDescription'] = sessionVars['vehicleDescription']
    response['production'] = sessionVars['production']
    response['manuals'] = manualsList
} else {
	throw new org.mule.module.apikit.exception.NotFoundException("No vehicle manuals found")
}

return prettyPrint(toJson(response))

