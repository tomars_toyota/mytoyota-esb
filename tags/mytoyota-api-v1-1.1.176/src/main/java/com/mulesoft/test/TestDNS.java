/*
 * Decompiled with CFR 0_118.
 * 
 * Could not load the following classes:
 *  org.apache.commons.lang.exception.ExceptionUtils
 *  org.mule.api.MuleEventContext
 *  org.mule.api.MuleMessage
 *  org.mule.api.lifecycle.Callable
 */
package com.mulesoft.test;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.InetAddress;
import java.net.UnknownHostException;
import org.apache.commons.lang.exception.ExceptionUtils;
import org.mule.api.MuleEventContext;
import org.mule.api.MuleMessage;
import org.mule.api.lifecycle.Callable;

public class TestDNS
implements Callable {
    public Object onCall(MuleEventContext eventContext) throws Exception {
    	String hostName = eventContext.getMessage().getInvocationProperty("hostname");
    	//String hostName = "services-uat.toyota.com.au";
        String digOutput = this.runCommand("dig " + hostName);
        String javaOutput = this.runJavaLookup(hostName);
        eventContext.getMessage().setPayload((Object)(digOutput + "\n" + javaOutput));
        return eventContext.getMessage();
    }

    private String runJavaLookup(String hostName) {
        StringBuilder result = new StringBuilder("About to resolve: '" + hostName + "'\n");
        try {
            InetAddress[] addresses = InetAddress.getAllByName(hostName);
            
            int addressesCount = addresses.length;
            
            if (addressesCount > 0) {
            	result.append("Result: ");
            }
            
            for (int i = 0; i < addressesCount; ++i) {
                result.append("'" + addresses[i] + "'");
            }
        }
        catch (UnknownHostException e) {
            result.append(ExceptionUtils.getStackTrace((Throwable)e));
        }
        return result.toString();
    }

    private String runCommand(String command) {
        StringBuilder result = new StringBuilder("About to run command: '" + command + "'\n");
        String s = null;
        try {
            Process p = Runtime.getRuntime().exec(command);
            BufferedReader stdInput = new BufferedReader(new InputStreamReader(p.getInputStream()));
            BufferedReader stdError = new BufferedReader(new InputStreamReader(p.getErrorStream()));
            while ((s = stdInput.readLine()) != null) {
                result.append(s).append(System.getProperty("line.separator"));
            }
            while ((s = stdError.readLine()) != null) {
                result.append(s).append(System.getProperty("line.separator"));
            }
        }
        catch (IOException e) {
            result.append("exception happened - here's what I know: " + e.getLocalizedMessage()).append(System.getProperty("line.separator"));
            e.printStackTrace();
        }
        return result.toString();
    }
}