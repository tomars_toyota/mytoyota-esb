import groovy.json.JsonBuilder

def result = [:]
result.data  = [:]
result.data.myToyotaAcceptedTerms = [:]
result.data.myToyotaAcceptedTerms.acceptedVersion = flowVars['acceptedTcVersion']

return result