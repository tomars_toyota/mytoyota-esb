package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;
import java.util.HashMap;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

import javax.script.*;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

import static org.junit.Assert.assertTrue;

public class PutUsersMeTcRequestMapper extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;

	public PutUsersMeTcRequestMapper() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/put-users-me-tc-request-mapper.groovy");
	}

	@Test
	public void testSuccessfulResponseMapper() throws Exception {
		
		
		HashMap<String, Object> myToyotaAcceptedTerms = new HashMap<String, Object>();
		myToyotaAcceptedTerms.put("acceptedVersion", "3.0");
		
		HashMap<String, Object> data = new HashMap<String, Object>();
		data.put("myToyotaAcceptedTerms", myToyotaAcceptedTerms);
		
		HashMap<String, Object> result = new HashMap<String, Object>();
		result.put("data", data);
		
		
		
		@SuppressWarnings("unchecked")
		HashMap<String, Object> returnedResult = (HashMap<String, Object>) ScriptRunBuilder
				.runner(scriptRunner)
				.payload(null)
				.flowVar("acceptedTcVersion", "3.0")
				.run();

		System.out.println("returnedResult: "+ returnedResult);

		assertTrue(returnedResult.equals(result));
		
	}
}
