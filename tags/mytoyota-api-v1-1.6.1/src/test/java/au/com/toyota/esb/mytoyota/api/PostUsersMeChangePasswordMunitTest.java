package au.com.toyota.esb.mytoyota.api;

import static org.mule.munit.common.mocking.Attribute.attribute;

import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.util.IOUtils;
import org.mule.util.StringUtils;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;


/*
 * Munit test to unit test the functionality for user change password
 * @author: jgarner
 */
public class PostUsersMeChangePasswordMunitTest extends FunctionalMunitSuite {
	
	private String token = "eyJjdHkiOiJKV1QiLCJlbmMiOiJBMjU2R0NNIiwiYWxnIjoiZGlyIn0.."
			+ "BZIfgmYPXx4PM-bq.YbrfGIXdE0urI4fRf2XK4iOtfEhQUiLNZ7Vq4U6rSCHZnc"
			+ "cwcc7blVbvMCcVm6Qb6gzQfdx0MXX1vl0fJPBJ9X5NB-laSk18dBwl1NUhLsOWI"
			+ "TUN3ovUSCvbP_4WKWHeBRkvyHsSWWEnJAh3pB6DWyFNF7yCBlzHYu0NDjhCBrqZ8"
			+ "lzT7G7cLUZYaGbmRB96Go46GCxLegPj6iiTVk48sotpRoPKOgpzRtUiAvoQnDvCx"
			+ "wuWT2XAh2T0T6SmunJrV0ITAup3klEy9eI38SIj5YblQvNjE7fD_oV8203klX1QxE"
			+ "2ZyAL42d5v1oAgXTp2Vip7gv6xyNDIQujgpwsRyaAiXCS2IBCjcCzo7bU__qxcAYQ"
			+ "2L7fsWg.9ZrmZrtMYlWNeI-qAgLk6w";

	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"post-user-change-password.xml",
				"post-user-change-password-technical-adapter.xml",
				"get-user-account-info-technical-adapter.xml",
				"post-passwords-encrypt-myKit-technical-adapter.xml",
				"sfdc-update-user.xml",
				"create-salesforce-sessionid-soap-header.xml",
				"sfdc-technical-adapter.xml",
				"config.xml"
		}, " ");
	}
	
	@Test
	public void testSuccessfulChangePassword() throws Exception {
		
		// Mock Identity Management API Password change successful
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
			attribute("name").ofNamespace("doc").withValue("IM API Change Password"))
			.thenApply(new MockResponseTransformer() {
				@Override
				public MuleMessage transform(MuleMessage originalMessage) {
					MuleMessage newMessage = null;
					
					try {
						newMessage = originalMessage.createInboundMessage();
						newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
						String responsePayload = IOUtils.toString(getClass().getResourceAsStream("/in/post-user-reset-password-passwordresettoken-im-response.json"));
						newMessage.setPayload(responsePayload);
					} catch (Exception e) {
						// catch exception statements
					}
					return newMessage;
				}
			});
		// Mock Identity Management API Get User Info
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
				attribute("name").ofNamespace("doc").withValue("IM API Get User"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						
						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							String responsePayload = IOUtils.toString(getClass().getResourceAsStream("/in/post-user-reset-password-passwordresettoken-im-get-user-profile-response.json"));
							newMessage.setPayload(responsePayload);
						} catch (Exception e) {
							// catch exception statements
						}
						return newMessage;
					}
				});
		// Mock encrypt password myKit API call response
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
				attribute("name").ofNamespace("doc").withValue("MyKit API: POST /passwords/encrypt"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						
						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							String responsePayload = "{\"encryptedPassword\": \"c7576b05dd8e64190e799d692be3d843\"}";
							
							newMessage.setPayload(responsePayload);
						} catch (Exception e) {
							// catch exception statements
						}
						return newMessage;
					}
				});		
		//Mock sfdc login response
		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Partner Operation login"))
				.thenReturn(muleMessageWithPayload(
						IOUtils.toString(getClass().getResourceAsStream("/in/sfdc-login-response.xml"))));

		//Mock sfdc updateOwner response
		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(
						attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Operation updateOwner"))
				.thenReturn(muleMessageWithPayload(""));
		
		String requestPayload = IOUtils.toString(getClass().getResourceAsStream("/in/post-users-me-changePassword-api-request.json"));
		
		
		MuleMessage message = muleMessageWithPayload(requestPayload);
		message.setProperty("APISessionToken", token, PropertyScope.INBOUND);
		
		MuleEvent event = testEvent(message);
		//event.setMessage(message);
				
		// Invoke the flow
		MuleEvent output = runFlow("post:/users/me/changePassword:myToyota-config", event);
		
		
		System.out.println("**** Result: "+output.getMessage().getPayload());
		
				
		verifyLoggerCall("esb.mytoyota-api-v1.post-user-change-password.request", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.post-user-change-password.response", 1);
		
		verifyLoggerCall("esb.mytoyota-api-v1.post-user-change-password-technical-adapter.request", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.post-user-change-password-technical-adapter.response", 1);
		
		verifyLoggerCall("esb.mytoyota-api-v1.get-user-account-info-im-technical-adapter.request", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.get-user-account-info-im-technical-adapter.response", 1);
		
		verifyLoggerCall("esb.mytoyota-api-v1.post-passwords-encrypt.mykit.request", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.post-passwords-encrypt.mykit.response", 1);
		
		verifyLoggerCall("esb.mytoyota-api-v1.get-salesforce-sessionid-subflow.request", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.get-salesforce-sessionid-subflow.response", 1);
		
		verifyLoggerCall("esb.mytoyota-api-v1.sfdc-update-user.request", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.sfdc-update-user.response", 1);
			
	}
	
	@Test
	public void testFailuresStatusCodes() throws Exception {

		int[] statusCodes = new int[] { 404, 405, 415, 406, 401, 500 };
		
		String requestPayload = IOUtils.toString(getClass().getResourceAsStream("/in/post-users-me-changePassword-api-request.json"));
		
		MuleEvent event = testEvent(requestPayload);
		
		for (int i = 0; i < statusCodes.length; i++) {
			new StatusCodesTestingHelper().runFailureTest(statusCodes[i], "IM API Change Password",
					"post:/users/me/changePassword:user-change-password-technical-adapter", event);
		}
	}
	
	private void verifyLoggerCall (String category, int expectedNumberOfCalls) {
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue(category)).times(expectedNumberOfCalls);
	}
}
