package au.com.toyota.esb.mytoyota.api;

import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.mule.munit.common.mocking.Attribute.attribute;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;


@Ignore
public class GetUsersMeVehiclesVinMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"get-users-me-vehicles-vin.xml",
				"get-users-me-vehicles-vin-technical-adapter.xml",
				"sfdc-get-user-connected-mobility-status.xml",
				"create-salesforce-sessionid-soap-header.xml",
				"sfdc-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulRequest() throws Exception {
		testSuccessfulRequestImpl(false);
	}
	
	@Test
	public void testSuccessfulRequestFailSfdc() throws Exception {
		testSuccessfulRequestImpl(true);
	}
	
	private void testSuccessfulRequestImpl(boolean failSfdc) throws Exception {
		
		String expectedResponse;
		if (failSfdc)
			expectedResponse = IOUtils.toString(getClass().getResourceAsStream("/out/get-users-me-vehicles-vin-api-response-no-connected-mobility.json"));
		else
			expectedResponse = IOUtils.toString(getClass().getResourceAsStream("/out/get-users-me-vehicles-vin-api-response.json"));
		
		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Partner Operation login"))
				.thenReturn(muleMessageWithPayload(
						IOUtils.toString(getClass().getResourceAsStream("/in/sfdc-login-response.xml"))));
		
		if (failSfdc)
			whenMessageProcessor("consumer").ofNamespace("ws")
					.withAttributes(attribute("name").ofNamespace("doc")
							.withValue("Invoke Salesforce Operation checkCMUserAndVehicle"))
					.thenThrow(new Exception("SFDC Failure"));
		else
			whenMessageProcessor("consumer").ofNamespace("ws")
					.withAttributes(attribute("name").ofNamespace("doc")
							.withValue("Invoke Salesforce Operation checkCMUserAndVehicle"))
					.thenReturn(muleMessageWithPayload(IOUtils
							.toString(getClass().getResourceAsStream("/in/sfdc-checkCMUserAndVehicle-response.xml"))));
		
		whenMessageProcessor("request").ofNamespace("http")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("My Toyota App API GET Retrieve User Vehicle by VIN"))
			.thenApply(new MockResponseTransformer() {
			@Override
			public MuleMessage transform(MuleMessage originalMessage) {
				MuleMessage newMessage = null;
				
				try {
					newMessage = originalMessage.createInboundMessage();
					
					newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
					
					newMessage.setPayload(getClass().getResourceAsStream("/in/get-users-me-vehicles-vin-mykit-response.json"));
				} catch (Exception e) {
					// catch exception statements
				}
				
				return newMessage;
			}
		});

		MuleEvent event = testEvent("");

		MuleEvent output = runFlow("get:/users/me/vehicles/{vin}:myToyota-config", event);

		String payload = output.getMessage().getPayloadAsString();
		System.out.println(payload);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-user-vehicles-vin.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-user-vehicles-vin.response"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-user-vehicles-vin-technical-adapter.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-user-vehicles-vin-technical-adapter.response"))
				.times(1);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.sfdc-get-user-connected-mobility-status.request"))
				.times(1);
		
		if (failSfdc)
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(attribute("category")
					.withValue("esb.mytoyota-api-v1.sfdc-get-user-connected-mobility-status.error")).times(1);
		else
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
					.withAttributes(attribute("category")
							.withValue("esb.mytoyota-api-v1.sfdc-get-user-connected-mobility-status.response"))
					.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.get-salesforce-sessionid-subflow.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-salesforce-sessionid-subflow.response"))
				.times(1);

		assertJsonEquals(expectedResponse, output.getMessage().getPayloadAsString());
	}
	
	@Test
	public void testFailuresStatusCodes() throws Exception {
		
		int[] statusCodes = new int[] { 404, 405, 415, 406, 401, 500 };	
		
		for (int i = 0; i < statusCodes.length; i++) {
			new StatusCodesTestingHelper().runFailureTest(statusCodes[i], "My Toyota App API GET Retrieve User Vehicle by VIN",
					"get:/users/me/vehicles/{vin}:myToyota-config");
		}
	}
}
