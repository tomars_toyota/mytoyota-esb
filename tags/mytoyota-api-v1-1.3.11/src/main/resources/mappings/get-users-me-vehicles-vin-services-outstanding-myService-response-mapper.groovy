import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper
import au.com.toyota.esb.mytoyota.api.util.GroovyHelper
import au.com.toyota.esb.mytoyota.api.util.ServiceDescriptionHelper


def response = [:]
response.vehicle = [:]
response.upsells = []
response.serviceOperations = []

JsonSlurper js = new JsonSlurper()

def getVehicleResponse = js.parseText(payload)
def upsellsList = []
def serviceOperationList = getVehicleResponse?.service

response.vehicle.registrationNumber = null
response.vehicle.dmsVehicleId = null

upsellsList.each { Map upsellItem ->
	def upsell = [:]
	upsell.description = upsellItem.opdsc
	upsell.price = upsellItem.stdchg
	upsell.id = upsellItem.x_rowid
	response.upsells << upsell
}

serviceOperationList.each { Map serviceOp ->
	def serviceOperation = [:]
	//serviceOperation.operationTypeId = serviceOp.optid
	//serviceOperation.operationId = serviceOp.opid
	def serviceDesc = serviceOp.kilometers + " km or " + serviceOp.name + " Months Service"
	serviceOperation.description = ServiceDescriptionHelper.formatServiceDescription(serviceDesc, disableFormat)
	serviceOperation.tsaFlag = (serviceOp.servicePricingType == "TSA") ? true : false
	serviceOperation.price = (serviceOp.price == null) ? null : new BigDecimal(serviceOp.price)
	serviceOperation.externalClaimNumber = serviceOp.code
	serviceOperation.serviceCode = serviceOp.serviceCode
	//serviceOperation.fixedChargeOperationFlag = serviceOp.fixflg
	//serviceOperation.cappedPriceOperationFlag = serviceOp.capflg
	serviceOperation.isComplimentary = (serviceOp.serviceType == "OSB_SERVICE_TYPE_CI") ? true : false
	serviceOperation.serviceExpiry = serviceOp.serviceExpiryDate

	// osb extra fields
	serviceOperation.serviceDetails = serviceOp.serviceDetails
	serviceOperation.servicePricingTypeDesc = serviceOp.servicePricingTypeDesc
	serviceOperation.recommended = serviceOp.recommended
	serviceOperation.duration = serviceOp.duration
	serviceOperation.serviceDueDate = serviceOp.serviceDueDate
	serviceOperation.servicePricingType = serviceOp.servicePricingType
	serviceOperation.servicePricingTypeDesc = serviceOp.servicePricingTypeDesc
	response.serviceOperations << serviceOperation
}


response = GroovyHelper.removeNulls(response)

return prettyPrint(toJson(response))