import groovy.json.JsonSlurper
import static groovy.json.JsonOutput.*
import au.com.toyota.esb.mytoyota.api.util.GroovyHelper


// Helper to remove leading zeroes
// Used when checking dealer code against known codes that support service booking
def stripLeadingZeros = { s ->
	return s.replaceFirst("^0*", "")
}


dealersResponse = new groovy.json.JsonSlurper().parseText(payload)

if(!dealersResponse?.dealers)
	throw new org.mule.module.apikit.exception.NotFoundException("Dealer not found")

dealer = dealersResponse.dealers[0]

// Dealer Service Booking safety check
// In order for a Dealer to be "Service Booking Enabled" it must:
//   1) Have a dmsID (serviceSiteId) value
//   2) Dealer code must be configured in properties
// The purpose of this is to ensure that Service Booking isn't possible even if the dmsID is populated
//
// Check dealer code against known dealer codes that support service booking
// - If it matches, map all fields
// - If it doesn't match, map all fields except dmsID (serviceSiteId)
// The lack of serviceSiteId will lead the myToyota front-ends down the 'Service Request' path instead

def serviceBookingSupportedFlag = false
// Read list of supported Dealer Codes from a property
def serviceBookingDealersList = serviceBookingDealerCodes.split(",").collect {it.trim() as String}
// Check if current Dealer Code is included
if ( serviceBookingDealersList.contains( stripLeadingZeros(dealer.dealerCode) ) ) {
	serviceBookingSupportedFlag = true
}

dealerMap = [:]

dealerMap.dealerCode = dealer.dealerCode
dealerMap.dealerName = dealer.dealerName
dealerMap.franchise = dealer.franchise
dealerMap.dealerPartsCode = dealer.dealerPartsCode
dealerMap.sapDealerCode = dealer.sapDealerCode
dealerMap.motorDealerLicenseNumber = dealer.motorDealerLicenseNumber
dealerMap.isMetro = dealer.isMetro == 'true' ? true : false
dealerMap.isRural = dealer.isRural == 'true' ? true : false
dealerMap.pricingTransparencyOptIn = dealer.pricingTransparencyOptIn == 'true' ?  true : false
dealerMap.dmsType = dealer.dmsVendorName

sitesList = []

//MYT-1256 get siteId from pricipal when Service branch does not have siteId
principalSiteId = ""
dealer.locations.each { location ->
	if(location.locationType?.toLowerCase() == 'principal') principalSiteId = location.serviceSiteId
}

dealer.locations.each { location ->
	site = [:]

	site.isPrincipal = location.locationType?.toLowerCase() == 'principal' ? true : false
	site.locationType = location.locationType
	site.branchCode = location.locationId
	site.sapCode = location.sapCode
	site.serviceRequestLeadTimeInDays = 2
	site.branchName = location.locationName
	
	//MYT-1256 get siteId from pricipal when Service branch does not have siteId
	if(serviceBookingSupportedFlag) {
		if(location.locationType?.toLowerCase() == 'service' && location.serviceSiteId == "") {
			site.dmsId = principalSiteId 
		}
		else {
			site.dmsId = location.serviceSiteId
		}
		if (myServiceIntegrationFlag) {
			dealerMap.dmsType = "myService"	
		}
		
	} else {
		site.dmsId = ""
	}
	
	site.address = location.address
	site.poBox = location.poBox
	site.phone = location.phone
	site.email = location.email
	site.webSite = location.webSite

	serviceTypesList = []

	location.serviceTypes.each { serviceType ->
		serviceTypeMap = [:]

		serviceTypeMap.name = serviceType.name
		serviceTypeMap.comments = serviceType.comments
		
		operatingHoursList = []

		serviceType.OperatingHours.each { opHoursSet ->
			operatingHoursMap = [:]
			
			operatingHoursMap.day = opHoursSet.day
			operatingHoursMap.isClosed = opHoursSet.closed == 'true' ? true : false
			if(!operatingHoursMap.isClosed) {
				operatingHoursMap.openingAt = opHoursSet.openingTime?.take(5)
				operatingHoursMap.closingAt = opHoursSet.closingTime?.take(5)
			}

			operatingHoursList.add(operatingHoursMap)
		}
		serviceTypeMap.hoursOfOperation = operatingHoursList

		serviceTypesList.add(serviceTypeMap)
	}

	site.serviceTypes = serviceTypesList

	sitesList.add(site)
}

dealerMap.sites = sitesList


//dealerMap = GroovyHelper.removeNulls(dealerMap)


return prettyPrint(toJson(dealerMap))