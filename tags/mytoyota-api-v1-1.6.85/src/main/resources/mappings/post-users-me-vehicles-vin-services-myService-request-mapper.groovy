import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper


def apiRequest = new JsonSlurper().parseText(flowVars.requestPayload)

//println apiRequest
if(!apiRequest)
	throw new org.mule.module.apikit.exception.BadRequestException('Invalid request')

//remove nulls
apiRequest = apiRequest.findAll {it.value != null && it.value != ""}

//validate request
if(!(apiRequest.containsKey('dealerId') &&
		apiRequest.containsKey('branchCode') &&
		apiRequest.containsKey('dealerEmail') &&
		apiRequest.containsKey('registrationNumber') &&
		apiRequest.containsKey('vehicleRegistrationState') &&
		apiRequest.containsKey('dropOffDate') &&
		apiRequest.containsKey('dropOffTime') &&
		apiRequest.containsKey('pickUpDate') &&
		apiRequest.containsKey('pickUpTime') &&
		apiRequest.containsKey('externalNumberOfClaim') &&
		apiRequest.containsKey('serviceCode')
	))
	throw new org.mule.module.apikit.exception.BadRequestException('Invalid request')

flowVars.updateOwnership = false
flowVars.updateProfile = false
def request = [:]

if (apiRequest.vehicleOdo) {
	flowVars.updateOwnership = true
}


//dealer
request.dealer = [:]
request.dealer.dealerCode = apiRequest.dealerId
request.dealer.dealerName = flowVars.dealerName
request.dealer.locationCode = apiRequest.branchCode
request.dealer.locationName = flowVars.branchName
request.dealer.serviceSiteId = flowVars.dmsId
request.dealer.email = apiRequest.dealerEmail

//vehicle
request.vehicle = [:]
request.vehicle.registrationNumber = apiRequest.registrationNumber
request.vehicle.state = apiRequest.vehicleRegistrationState
request.vehicle.odometer = apiRequest.vehicleOdo
request.vehicle.vin = flowVars.vin
def vehicleYearValue = ( !(apiRequest.vehicleYear) || apiRequest.vehicleYear == "0000") ? "2010" : apiRequest.vehicleYear
request.vehicle.manufacturedYear = vehicleYearValue
request.vehicle.model = apiRequest.vehicleModel
if (!request.vehicle.model) {
	request.vehicle.model = "Default"
}
request.vehicle.make = apiRequest.vehicleMake
if (!request.vehicle.make) {
	request.vehicle.make = "TOY"
}

//repairOrder
request.repairOrder = [:]
request.repairOrder.totalPrice = "" + apiRequest.totalServicePrice
request.repairOrder.totalDurationHours = apiRequest.totalOperationTime
request.repairOrder.services = []

def serviceItem = [:]
serviceItem.serviceCode = apiRequest.serviceCode
serviceItem.sapOperationCode = apiRequest.externalNumberOfClaim
serviceItem.dmsOperationCode = null
serviceItem.months = apiRequest.months
serviceItem.price = "" + apiRequest.serviceOperationPrice
serviceItem.duration = apiRequest.totalOperationTime
serviceItem.kilometers = apiRequest.kilometers // need
serviceItem.serviceName = apiRequest.description //
serviceItem.servicePricingType = apiRequest.servicePricingType 
serviceItem.serviceType = apiRequest.serviceType


request.repairOrder.services.add(serviceItem)


//appointment
request.appointment = [:]
request.appointment.dropOffDate = apiRequest.dropOffDate
request.appointment.dropOffTime = apiRequest.dropOffTime
request.appointment.pickUpDate = apiRequest.pickUpDate
request.appointment.pickUpTime = apiRequest.pickUpTime

//contact
if (apiRequest.contactDetails) {
	request.contact = [:]
	request.contact.alternativeContactNo = apiRequest.contactDetails.phone
	request.contact.email = apiRequest.contactDetails.email	
	request.contact.firstName = apiRequest.contactDetails.firstName
	request.contact.lastName = apiRequest.contactDetails.lastName
	request.contact.preferredContactNo = apiRequest.contactDetails.phone
	request.contact.vehicleRelationship = "OSB_CUSTOMER_OWNER"
	request.contact.preferredContactMethod = "EMAIL" // hardcoded for now, myToyota only supports email
	if (apiRequest.confirmationType) {
		// overwrite if the frontend is providing the value
		// Final check as OSB doesn't recognised the prefix OSB_CONTACT
		if (apiRequest.confirmationType == "OSB_CONTACT_SMS") {
			apiRequest.confirmationType = "SMS"
		}
		else if (apiRequest.confirmationType == "OSB_CONTACT_EMAIL") {
			apiRequest.confirmationType = "EMAIL"
		}
		else if (apiRequest.confirmationType == "OSB_CONTACT_BOTH") {
			apiRequest.confirmationType = "BOTH"
		}
		request.contact.preferredContactMethod = apiRequest.confirmationType
	}

	request.contact.myToyotaId = flowVars.myToyotaId
	flowVars.updateProfile = apiRequest.contactDetails.isUpdateMyContact

	
}

request.vehicleConcernsComment = apiRequest.notes
request.transportOption = apiRequest.waitAtDealer ? "OSB_TRANSPORT_OPTION_WAIT" : "OSB_TRANSPORT_OPTION_NONE"
if (apiRequest.transportOption) {
	// overwrite if the frontend is providing the value
	request.transportOption	= apiRequest.transportOption
}
request.expressMaintenance = "false" // hardcoded for now myToyota only supports express Maintenance

request.customerComments = []
request.bookingSource = "myToyota"
request.status = "INITIATED"
request.privacyFlag = true


//remove nulls
request = request.findAll {it.value != null && it.value != ""}

return prettyPrint(toJson(request))
