package au.com.toyota.esb.mytoyota.api;

import static org.junit.Assert.assertEquals;
import static org.mule.munit.common.mocking.Attribute.attribute;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.module.http.internal.request.ResponseValidatorException;
import org.mule.munit.runner.functional.FunctionalMunitSuite;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;

public class DeleteUsersMeVehiclesVinMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"delete-users-me-vehicles-vin.xml",
				"delete-users-me-vehicles-vin-technical-adapter.xml",
				"sfdc-remove-vehicle.xml",
				"create-salesforce-sessionid-soap-header.xml",
				"sfdc-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulRequest() throws Exception {

		whenMessageProcessor("request").ofNamespace("http").withAttributes(
				attribute("name").ofNamespace("doc").withValue("My Toyota App API DELETE User Vehicle by VIN"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						
						try {
							newMessage = originalMessage.createInboundMessage();
							
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							
							newMessage.setPayload(IOUtils.toInputStream("", "UTF-8"));
						} catch (Exception e) {
							// catch exception statements
						}
						
						return newMessage;
					}
				});
		
		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Operation login"))
				.thenReturn(muleMessageWithPayload(
						IOUtils.toString(getClass().getResourceAsStream("/in/sfdc-login-response.xml"))));

		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(
						attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Operation removeAVehicle"))
				.thenReturn(muleMessageWithPayload(""));

		MuleEvent event = testEvent("");
		event.setFlowVariable("vin", "JTNKU3JE60J021675");

		runFlow("delete:/users/me/vehicles/{vin}:myToyota-config", event);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.delete-user-vehicles-vin.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.delete-user-vehicles-vin.response"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.delete-user-vehicles-vin-technical-adapter.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.delete-user-vehicles-vin-technical-adapter.response"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.sfdc-remove-vehicle.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.sfdc-remove-vehicle.response"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.get-salesforce-sessionid-subflow.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-salesforce-sessionid-subflow.response"))
				.times(1);
	}
	
	@Test
	public void testFailure() throws Exception {
		
		
		String requestPayload = "";
		MuleEvent event = testEvent(requestPayload);

		whenMessageProcessor("request").ofNamespace("http").withAttributes(
			attribute("name").ofNamespace("doc").withValue("My Toyota App API DELETE User Vehicle by VIN"))
			.thenThrow(new java.lang.Exception("Response code 500 mapped as failure."));

		try {

			// Invoke the flow
			runFlow("delete:/users/me/vehicles/{vin}:myToyota-config", event);
			
			// validating exception thrown
			assertEquals(true, false);

		} catch (java.lang.Exception e) {
			
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
					.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.delete-user-vehicles-vin.request"))
					.times(1);
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
					.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.delete-user-vehicles-vin.response"))
					.times(0);
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
					attribute("category").withValue("esb.mytoyota-api-v1.delete-user-vehicles-vin-technical-adapter.request"))
					.times(1);
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
					attribute("category").withValue("esb.mytoyota-api-v1.delete-user-vehicles-vin-technical-adapter.response"))
					.times(0);

		}

	}
	
	@Test
	public void testFailuresStatusCodes() throws Exception {
		
		int[] statusCodes = new int[] { 404, 405, 415, 406, 401, 500 };
		
		for (int i = 0; i < statusCodes.length; i++) {
			new StatusCodesTestingHelper().runFailureTest(statusCodes[i], "My Toyota App API DELETE User Vehicle by VIN",
					"delete:/users/me/vehicles/{vin}:myToyota-config");
		}
	}
}
