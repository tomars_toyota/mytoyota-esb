import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper

def capitaliseNames(name) {
	newName = ''
	name.tokenize().each { newName += it.toLowerCase().capitalize() + ' '}
	return newName.trim()
}

println '____ payload: ' + payload

order = new JsonSlurper().parseText(payload)
order.firstName = order.firstName ? capitaliseNames(order.firstName) : null 
order.lastName = order.lastName ? capitaliseNames(order.lastName) : null 

return prettyPrint(toJson(order))