import groovy.json.JsonSlurper

def request = new JsonSlurper().parseText(payload)

def payload = [:]
payload.data = [:]

// map accepted TC version
if(request?.termsAndConditions?.acceptedVersion?.length() > 0) {
	payload.data.myToyotaAcceptedTerms = ['acceptedVersion':request?.termsAndConditions?.acceptedVersion]
}

// map accepted Privacy Statement version
if(request?.privacyStatement?.acceptedVersion?.length() > 0) {
	payload.data.myToyotaAcceptedPrivacyPolicy = ['acceptedVersion':request?.privacyStatement?.acceptedVersion]
}

return payload