package au.com.toyota.esb.mytoyota.api;

import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;

//@Ignore
public class GetLegalTermsLatestMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"get-legalTerms-latest.xml",
				"get-legalTerms-latest-mytoyota-technical-adapter.xml",
				"get-legalTerms-latest-tlink-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	//@Test
	public void testSuccessfulMyToyotaRequest() throws Exception {

		String requestPath = "/api/myToyota/v1/legalTerms/latest";
		final String tcSiteCoreResponse = "{\"version\": 25.5,"
				+ "\"content\": \"25.5 tc contents\"}";

		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Sitecore TC Latest"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setPayload(tcSiteCoreResponse);
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
						} catch (Exception e) {
							// catch exception statements
						}

						return newMessage;
					}
				});

		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Sitecore Privacy Statement Latest"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							newMessage = originalMessage.createInboundMessage();
							//working around resetting fowVars by MUnit
							newMessage.setInvocationProperty("latestTC", tcSiteCoreResponse);
							newMessage.setPayload("{\"version\": 50.3,"
									+ "\"content\": \"50.3 privacy policy contents\"}");
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
						} catch (Exception e) {
							// catch exception statements
						}

						return newMessage;
					}
				});

		MuleEvent event = testEvent(requestPath);
		
//		MuleMessage msg = muleMessageWithPayload(null);
//		
//		Map<String, Object> props = new HashMap<String,Object>();
//		props.put("application", "myToyota");
//		
//		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
		
//		event.setMessage(msg);

		MuleEvent output = runFlow("get:/legalTerms/latest:myToyota-config", event);

		System.out.println(output.getMessage().getPayload());
		assertJsonEquals(
				"{\"termsAndConditions\":{"
					+ "\"version\":\"25.5\","
					+ "\"content\":\"25.5 tc contents\""
				+ "},"
				+ "\"privacyStatement\":{"
					+ "\"version\":\"50.3\","
					+ "\"content\":\"50.3 privacy policy contents\""
				+ "}}",
				output.getMessage().getPayloadAsString());
	}
	
	//@Test
	public void testSuccessfulTlinkRequest() throws Exception {

		String requestPath = "/api/myToyota/v1/legalTerms/latest";
		
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Get TLink Sitecore TC Latest"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setPayload("{\"version\": 50.3,"
									+ "\"content\": \"50.3 tlink tc contents\"}");
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
						} catch (Exception e) {
							// catch exception statements
						}

						return newMessage;
					}
				});

		MuleEvent event = testEvent(requestPath);
		
		MuleMessage msg = muleMessageWithPayload(null);
		
		Map<String, Object> props = new HashMap<String,Object>();
		props.put("application", "tlink");
		
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
		
		event.setMessage(msg);

		MuleEvent output = runFlow("get:/legalTerms/latest:myToyota-config", event);

		System.out.println(output.getMessage().getPayload());
		assertJsonEquals(
				"{\"termsAndConditions\":{"
					+ "\"version\":\"50.3\","
					+ "\"content\":\"50.3 tlink tc contents\""
				+ "}}",
				output.getMessage().getPayloadAsString());
	}	
}
