package au.com.toyota.esb.mytoyota.api.mappings;

import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.junit.Assert.*;
import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.DefaultMuleMessage;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.context.DefaultMuleContextFactory;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

public class SendVerificationSMSRequestMapperTest extends AbstractMuleContextTestCase {

	private ScriptRunner scriptRunner;

	public SendVerificationSMSRequestMapperTest() throws ScriptException {
		scriptRunner = ScriptRunner
				.createScriptRunner("/mappings/send-sms-comms-api-request-mapper.groovy");
	}
	
	//@Test
    public void testRequestMappingSuccess() throws Exception {
        String request = IOUtils
                .toString(getClass().getResourceAsStream("/in/send-verification-sms-request.json"));
        
        String expectedResult = IOUtils
                .toString(getClass().getResourceAsStream("/out/send-verification-sms-api-request.json"));
        
		//load properties
		Properties props = new Properties();
		props.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));
		
		String result = ScriptRunBuilder
                .runner(scriptRunner)
                .payload("{ \"verificationCode\":\"7423\" }")
                .sessionVar("mobileNumber","0401111222")
                .sessionVar("vin","JTMBFREV80D078597")
                .binding("auMobileRegEx", props.getProperty("api.regex.au.mobilephone"))
                .binding("auMobileE164RegEx", props.getProperty("api.regex.e164.au.mobilephone"))
                .binding("smsVerificationTemplateLine1", props.getProperty("api.sms.verification.template.line.1"))
                .binding("smsVerificationTemplateLine2", props.getProperty("api.sms.verification.template.line.2"))
                .binding("smsVerificationTemplateLineSeparator", props.getProperty("api.sms.verification.template.line.separator"))
                .sessionVar("ESBTransactionId", "ff587447-1e72-4adf-9852-223b1e244fe9")
                .run().toString();
		
		System.out.println(result);
	
		JSONAssert.assertEquals(expectedResult, result, true);
    }
	
	//@Test
    public void testRequestMappingInvalidMobileNumber() throws Exception {
        String request = IOUtils
                .toString(getClass().getResourceAsStream("/in/send-verification-sms-request.json"));
        
        //load properties
		Properties props = new Properties();
		props.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));
		
		try { 
			String result = ScriptRunBuilder
	                .runner(scriptRunner)
	                .payload("{ \"verificationCode\":\"7423\" }")
	                .sessionVar("mobileNumber","401111222")
	                .sessionVar("vin","JTMBFREV80D078597")
	                .binding("auMobileRegEx", props.getProperty("api.regex.au.mobilephone"))
	                .binding("auMobileE164RegEx", props.getProperty("api.regex.e164.au.mobilephone"))
	                .binding("smsVerificationTemplateLine1", props.getProperty("api.sms.verification.template.line.1"))
	                .binding("smsVerificationTemplateLine2", props.getProperty("api.sms.verification.template.line.2"))
	                .binding("smsVerificationTemplateLineSeparator", props.getProperty("api.sms.verification.template.line.separator"))
	                .sessionVar("ESBTransactionId", "ff587447-1e72-4adf-9852-223b1e244fe9")
                .run().toString();
		 } catch (Exception e) {
	            if(e.getMessage().contains("Invalid mobile number: 401111222"))
	                assert(true);
	            else
	                assert(false);
	            return;
	    }
		assert(false);
    }
}