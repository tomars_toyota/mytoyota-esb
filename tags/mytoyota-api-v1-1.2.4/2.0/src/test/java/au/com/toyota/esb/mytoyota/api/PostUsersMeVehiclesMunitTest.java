package au.com.toyota.esb.mytoyota.api;

import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.HashMap;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Test;
import org.mule.DefaultMuleMessage;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.module.apikit.exception.BadRequestException;
import org.mule.munit.runner.functional.FunctionalMunitSuite;

import au.com.toyota.esb.mytoyota.api.munit.BlockingMuleEventSpy;
import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;

import org.custommonkey.xmlunit.*;
//import org.custommonkey.xmlunit.XMLTestCase;

public class PostUsersMeVehiclesMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"post-users-me-vehicles.xml",
				"post-users-me-vehicles-technical-adapter.xml",
				"get-vehicle-details.xml",
				"get-vehicle-details-technical-adapter.xml",
				"sfdc-add-vehicle.xml",
				"sfdc-create-case.xml",
				"sfdc-add-attachment.xml",
				"create-salesforce-sessionid-soap-header.xml",
				"sfdc-technical-adapter.xml",
				"config.xml",
				"complete-rego-verification-async.xml",
				"sfdc-query.xml"
		}, " ");
	}
	
	//@Test
	public void testIncorrectParametersMissingBatchTest() throws Exception {
		
		String payload = IOUtils.toString(getClass().getResourceAsStream("/in/post-users-me-vehicles-missing-batch-number-api-request.json"));
		MuleEvent event = testEvent(payload);

		try {
			runFlow("post:/users/me/vehicles:myToyota-config", event);
		} catch (Exception e) {
			assertEquals("Not org.mule.module.apikit.exception.BadRequestException was thrown!",
					e.getCause().getCause() instanceof BadRequestException, true);
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.post-add-user-vehicle.request"))
				.times(1);
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.post-add-user-vehicle.response"))
				.times(0);
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.post-add-user-vehicle-technical-adapter.request"))
				.times(0);
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.post-add-user-vehicle-technical-adapter.response"))
				.times(0);
			return;
		}
		assertTrue(false);
	}
	
	//@Test
	public void testIncorrectParametersMissingRegistrationDetailsTest() throws Exception {
		
		String payload = IOUtils.toString(getClass().getResourceAsStream("/in/post-users-me-vehicles-missing-registration-details-api-request.json"));
		MuleEvent event = testEvent(payload);

		try {
			runFlow("post:/users/me/vehicles:myToyota-config", event);
		} catch (Exception e) {
			assertEquals("Not org.mule.module.apikit.exception.BadRequestException was thrown!",
					e.getCause().getCause() instanceof BadRequestException, true);
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.post-add-user-vehicle.request"))
				.times(1);
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.post-add-user-vehicle.response"))
				.times(0);
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.post-add-user-vehicle-technical-adapter.request"))
				.times(0);
			verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.post-add-user-vehicle-technical-adapter.response"))
				.times(0);
			return;
		}
		assertTrue(false);
	}

	//@Test
	public void testVinBatchNumberSuccessfulRequest() throws Exception {

		// Mock Get Vehicle Details
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
				attribute("name").ofNamespace("doc").withValue("Vehicle API GET vehicle by vin"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						
						try {
							newMessage = originalMessage.createInboundMessage();
							
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							
							newMessage.setPayload(IOUtils.toString(getClass().getResourceAsStream("/in/get-vehicle-details-response.json")));
						} catch (Exception e) {
							// catch exception statements
						}
						
						return newMessage;
					}
				});

		// Mock myKIT
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
				attribute("name").ofNamespace("doc").withValue("My Toyota App API POST Add Vehicle"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						
						try {
							newMessage = originalMessage.createInboundMessage();
							
							newMessage.setProperty("http.status", 201, PropertyScope.INBOUND);
							
							newMessage.setPayload(IOUtils.toInputStream("", "UTF-8"));
						} catch (Exception e) {
							// catch exception statements
						}
						
						return newMessage;
					}
				});
		
		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Partner Operation login"))
				.thenReturn(muleMessageWithPayload(
						IOUtils.toString(getClass().getResourceAsStream("/in/sfdc-login-response.xml"))));
		
		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Operation addAVehicle"))
				.thenReturn(muleMessageWithPayload(""));

		
		String payload = IOUtils.toString(getClass().getResourceAsStream("/in/post-users-me-vehicles-api-request.json"));
		MuleEvent event = testEvent(payload);

		runFlow("post:/users/me/vehicles:myToyota-config", event);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.post-add-user-vehicle.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.post-add-user-vehicle.response"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.post-add-user-vehicle-technical-adapter.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.post-add-user-vehicle-technical-adapter.response"))
				.times(1);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-vehicle-details-technical-adapter.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-vehicle-details-technical-adapter.response"))
				.times(1);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.sfdc-add-vehicle.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.sfdc-add-vehicle.response"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.get-salesforce-sessionid-subflow.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-salesforce-sessionid-subflow.response"))
				.times(1);
		
		System.out.println("**** Result: "+event.getMessage().getPayload());
	}
	
	//@Test
	public void testVinRegistrationDetailsSuccessfulRequest() throws Exception {

		whenMessageProcessor("request").ofNamespace("http").withAttributes(
				attribute("name").ofNamespace("doc").withValue("My Toyota App API POST Add Vehicle"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						
						try {
							newMessage = originalMessage.createInboundMessage();
							
							newMessage.setProperty("http.status", 201, PropertyScope.INBOUND);
							
							newMessage.setPayload(IOUtils.toInputStream("", "UTF-8"));
						} catch (Exception e) {
							// catch exception statements
						}
						
						return newMessage;
					}
				});
		
		String payload = IOUtils.toString(getClass().getResourceAsStream("/in/post-users-me-vehicles-api-registration-details-request.json"));
		MuleEvent event = testEvent(payload);

		MuleEvent output = runFlow("post:/users/me/vehicles:myToyota-config", event);
		
		assert(output.getMessage().getOutboundProperty("http.status").equals("202"));
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.post-add-user-vehicle.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.post-add-user-vehicle.response"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.post-add-user-vehicle-technical-adapter.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.post-add-user-vehicle-technical-adapter.response"))
				.times(1);
		
	}
	
	//@Test
	public void testAsyncCompleteRegoVerification() throws Exception {	
		
		// Mock Login
		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Enterprise Operation login"))
				.thenReturn(muleMessageWithPayload(
						IOUtils.toString(getClass().getResourceAsStream("/in/sfdc-enterprise-wsdl-login-response.xml"))));
		// Mock create case
		whenMessageProcessor("consumer").ofNamespace("ws")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Operation create"))
			.thenReturn(muleMessageWithPayload(
					IOUtils.toString(getClass().getResourceAsStream("/in/sfdc-create-case-response.xml"))));

		// Mock create attachment for case
		whenMessageProcessor("consumer").ofNamespace("ws")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Operation create (for attachment)"))
			.thenReturn(muleMessageWithPayload(
					IOUtils.toString(getClass().getResourceAsStream("/in/sfdc-create-attachment-response.xml"))));
		
		//Mock sfdc-query-vin
		whenMessageProcessor("flow-ref")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("sfdc-query-vin"))
			.thenReturn(muleMessageWithPayload(
					IOUtils.toString(getClass().getResourceAsStream("/in/sfdc-query-vin-response.xml"))));

		//Mock sfdc-query-myToyotaId
		whenMessageProcessor("flow-ref")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("sfdc-query-myToyotaId"))
			.thenReturn(muleMessageWithPayload(
					IOUtils.toString(getClass().getResourceAsStream("/in/sfdc-query-myToyotaId-response.xml"))));
		
		MuleEvent event = testEvent("");
				
		MuleMessage message = new DefaultMuleMessage("", muleContext);
		
		event.setMessage(message);
		message.setProperty("myToyotaId", "TEST-000002cf", PropertyScope.SESSION);
		message.setProperty("requestPayload", buildCompleteRegoPayload(), PropertyScope.SESSION);
		
		
		
		BlockingMuleEventSpy<String> appAPIRequestMessageSpy = new BlockingMuleEventSpy<String>(1, 5000);
		spyMessageProcessor("component").ofNamespace("scripting").withAttributes(
			attribute("name").ofNamespace("doc").withValue("Map Create Case request"))
			.after(appAPIRequestMessageSpy);

		runFlow("complete-rego-verification-async", event);
		
		appAPIRequestMessageSpy.block();

		System.out.println("_________ Payload at (Map Create Case request): "+appAPIRequestMessageSpy.getFirstPayload());

		
		XMLUnit.setIgnoreWhitespace(true);

		DetailedDiff diff = new DetailedDiff(
				XMLUnit.compareXML(IOUtils.toString(getClass().getResourceAsStream("/in/sfdc-create-case-request.xml")),
						appAPIRequestMessageSpy.getFirstPayload()));
        assertTrue(diff.similar());
        
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.sfdc-create-case.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.sfdc-create-case.response"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.sfdc-add-attachment.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.sfdc-add-attachment.response"))
				.times(1);

		// Two calls to Salesforce means session ID flows invoked twice
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.get-salesforce-sessionid-subflow.request"))
				.times(2);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-salesforce-sessionid-subflow.response"))
				.times(2);
		
	}
	
	private HashMap<String, Object> buildCompleteRegoPayload() throws Exception {
		HashMap<String, Object> payload = new HashMap<String, Object> ();
		payload.put("vin", "JTNKU3JE60J021675");
		
		HashMap<String, Object> registrationDetails = new HashMap<String, Object> ();
		registrationDetails.put("documentFilename", "registration-papers-file.png");
		registrationDetails.put("encodedImage",
				"iVBORw0KGgoAAAANSUhEUgAAAAcAAAAKCAYAAAB4zEQNAAAAGXRFWHRTb2Z0d2FyZQ"
						+ "BBZG9iZSBJbWFnZVJlYWR5ccllPAAAA9FpVFh0WE1MOmNvbS5hZG9iZS54bXAAAAAAADw/eHBhY2tldCBiZWdpbj0i77u/IiB"
						+ "pZD0iVzVNME1wQ2VoaUh6cmVTek5UY3prYzlkIj8+IDx4OnhtcG1ldGEgeG1sbnM6eD0iYWRvYmU6bnM6bWV0YS8iIHg6eG1"
						+ "wdGs9IkFkb2JlIFhNUCBDb3JlIDUuNi1jMDY3IDc5LjE1Nzc0NywgMjAxNS8wMy8zMC0yMzo0MDo0MiAgICAgICAgIj4gPHJ"
						+ "kZjpSREYgeG1sbnM6cmRmPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5LzAyLzIyLXJkZi1zeW50YXgtbnMjIj4gPHJkZjpEZXN"
						+ "jcmlwdGlvbiByZGY6YWJvdXQ9IiIgeG1sbnM6eG1wUmlnaHRzPSJodHRwOi8vbnMuYWRvYmUuY29tL3hhcC8xLjAvcmlnaHR"
						+ "zLyIgeG1sbnM6eG1wTU09Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFwLzEuMC9tbS8iIHhtbG5zOnN0UmVmPSJodHRwOi8vbnM"
						+ "uYWRvYmUuY29tL3hhcC8xLjAvc1R5cGUvUmVzb3VyY2VSZWYjIiB4bWxuczp4bXA9Imh0dHA6Ly9ucy5hZG9iZS5jb20veGFw"
						+ "LzEuMC8iIHhtcFJpZ2h0czpNYXJrZWQ9IkZhbHNlIiB4bXBNTTpPcmlnaW5hbERvY3VtZW50SUQ9InhtcC5kaWQ6NTIxMUE2M"
						+ "jM0QjIwNjgxMTgzRDE4RkI3OTA0NDI1RDEiIHhtcE1NOkRvY3VtZW50SUQ9InhtcC5kaWQ6RDAxRDI3NEQ4ODBCMTFFNUIyOD"
						+ "k5MEY4QUQxRkE5NjgiIHhtcE1NOkluc3RhbmNlSUQ9InhtcC5paWQ6RDAxRDI3NEM4ODBCMTFFNUIyODk5MEY4QUQxRkE5Njg"
						+ "iIHhtcDpDcmVhdG9yVG9vbD0iQWRvYmUgUGhvdG9zaG9wIENDIDIwMTUgKE1hY2ludG9zaCkiPiA8eG1wTU06RGVyaXZlZEZy"
						+ "b20gc3RSZWY6aW5zdGFuY2VJRD0ieG1wLmlpZDpiN2RjNDQ0MS00YWU1LTQzZWYtOGUyYi0xODVhNmI0NzFlN2EiIHN0UmVm"
						+ "OmRvY3VtZW50SUQ9ImFkb2JlOmRvY2lkOnBob3Rvc2hvcDo0ZTkwNDVlYy1iY2Y5LTExNzgtYTFmMS1lNTU5ODdjMTdiYWIiL"
						+ "z4gPC9yZGY6RGVzY3JpcHRpb24+IDwvcmRmOlJERj4gPC94OnhtcG1ldGE+IDw/eHBhY2tldCBlbmQ9InIiPz7IllEZAAAAQE"
						+ "lEQVR42mL8//8/A04AlRQE4nJckjNBTCiNIWkMxO8wFCDZiakAzUFpUMn/YMXE6MRrZwc+14L86YLuTYAAAwDm/DCNfDxPNQA"
						+ "AAABJRU5ErkJggg==");
		payload.put("registrationDetails", registrationDetails);
		
		HashMap<String, Object> vehicleUser = new HashMap<String, Object> ();
		vehicleUser.put("registrationNumber", "AAA111");
		vehicleUser.put("state", "VIC");
		payload.put("vehicleUser", vehicleUser);
		
		return payload;
	}
}
