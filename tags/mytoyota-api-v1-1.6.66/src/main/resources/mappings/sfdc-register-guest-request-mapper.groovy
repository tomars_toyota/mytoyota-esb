import groovy.xml.MarkupBuilder
import groovy.json.JsonSlurper
import java.text.SimpleDateFormat

def writer = new StringWriter()
def xmlMarkup = new MarkupBuilder(writer)


if(payload instanceof org.mule.transport.NullPayload)
	payload = null

def user = payload
def postalAddress = user?.address?.postal
def residentialAddress = user?.address?.residential
def ownerRelationshipType = "O"


xmlMarkup.mkp.xmlDeclaration(version: "1.0", encoding: "UTF-8")
xmlMarkup.'sp:registerGuest' (
	'xmlns:sp' : "http://soap.sforce.com/schemas/class/GuestRegVehicleWebserviceCalls"
	) {
		'sp:request'{
			
			if(user?.isMarketingOptIn != null)
				'sp:guestPreferences' {
					'sp:marketingPreference' (user.isMarketingOptIn)
				}
			if(sessionVars['myToyotaId'])
				'sp:myToyotaId' (sessionVars['myToyotaId'])
			if(user?.salesforceId)
				'sp:salesforceId' (user.salesforceId)

			if((user?.dateOfBirth != null) || 
					(user?.email != null) ||
					(user?.firstName != null) ||
					(user?.landline != null) ||
					(user?.lastName != null) ||
					(user?.mobile != null) ||
					(user?.title != null) ||
					(sessionVars['encryptedPassword']) ||
					(sessionVars['updateLastLogin']) ||
					postalAddress ||
					residentialAddress) {
				'sp:guestDetails'{
					if(user?.dateOfBirth != null) {
						if(user.dateOfBirth == '') 
							'sp:dob' (user.dateOfBirth)
						else
							'sp:dob' (new SimpleDateFormat("dd/MM/yyyy").format(new Date().parse('yyyy-MM-dd', user.dateOfBirth)))
					}
					if(user?.email != null)
						'sp:email' (user.email)
					if(user?.firstName != null)
						'sp:firstName' (user.firstName)
					if(user?.lastName != null)
						'sp:lastName' (user.lastName)
					if(sessionVars['encryptedPassword'])
						'sp:password' (sessionVars['encryptedPassword'])
					if(user?.mobile != null)
						'sp:personmobilephone' (user.mobile)
					if(user?.landline != null)
						'sp:phone' (user.landline)
					if(user?.title != null)
						'sp:salutation' (user.title)
					if(sessionVars['updateLastLogin'])
						'sp:lastLoginTime' (new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssXXX").format(new Date()))
					if(user?.email)
						'sp:ownersPortalUserId' (user.email)

					if(postalAddress) {
						'sp:shippingAddressDetails' {
							if(postalAddress.suburb != null)
								'sp:cityorsuburb' (postalAddress.suburb)
							if(postalAddress.postcode != null)
								'sp:postcode' (postalAddress.postcode)
							if(postalAddress.state != null)
								'sp:state' (postalAddress.state)
							if(postalAddress.streetAddress != null)
								'sp:street' (postalAddress.streetAddress)
						}
					}
					if(residentialAddress) {
						'sp:billingAddressDetails' {
							if(residentialAddress.suburb != null)
								'sp:cityorsuburb' (residentialAddress.suburb)
							if(residentialAddress.postcode != null)
								'sp:postcode' (residentialAddress.postcode)
							if(residentialAddress.state != null)
								'sp:state' (residentialAddress.state)
							if(residentialAddress.streetAddress != null)
								'sp:street' (residentialAddress.streetAddress)
						}
					}
				}
			}
			
			if(user?.guestOrder != null || 
				user?.vin != null) {
				
				'sp:vehicleDetails'{
					if(user?.guestOrder) {
						'sp:trackMyCar'{
							if(user?.guestOrder?.cosi != null)
					   			'sp:cosi' (user.guestOrder.cosi)
					   		if(user?.guestOrder?.vehicleDetails?.batchId != null)
					   			'sp:batchId' (user.guestOrder.vehicleDetails.batchId)
					   		if(user?.guestOrder?.status?.statusDescription != null) 
								'sp:statusDescription' (user.guestOrder.status.statusDescription)
						}
					}
					if(user?.vin != null)
						'sp:vin' (user.vin)
				}
			}

			if(user?.vin != null)
				'sp:guestType' (ownerRelationshipType)
		
		}	
	}
//println '__________ payload to salesforce: ' + writer.toString()
return writer.toString()



// <gues:registerGuest>
//      <gues:request>
//         <!--Optional:-->
//         <gues:CMUserFlag>true</gues:CMUserFlag>
//         <!--Optional:-->
//         <gues:guestDetails>
//            <!--Optional:-->
//            <gues:billingAddressDetails>
//               <!--Optional:-->
//               <gues:cityorsuburb>Geelong</gues:cityorsuburb>
//               <!--Optional:-->
//               <gues:country>Australia</gues:country>
//               <!--Optional:-->
//               <gues:postcode>3040</gues:postcode>
//               <!--Optional:-->
//               <gues:state>VIC</gues:state>
//               <!--Optional:-->
//               <gues:street>osbonee</gues:street>
//            </gues:billingAddressDetails>
//            <!--Optional:-->
//           <!-- <gues:dob>17-oct-2018</gues:dob> -->
//            <!--Optional:-->
//            <gues:email>sunish@gmail.com</gues:email>
//            <!--Optional:-->
//            <gues:firstName>sunish</gues:firstName>
//            <!--Optional:-->
//          <!--  <gues:lastLoginTime>sharma</gues:lastLoginTime> -->
//            <!--Optional:-->
//            <gues:lastName>chartur</gues:lastName>
//            <!--Optional:-->
//            <gues:ownersPortalUserId>sunish@gmail.com</gues:ownersPortalUserId>
//            <!--Optional:-->
//            <gues:password>fdsgfsdgj</gues:password>
//            <!--Optional:-->
//            <gues:personmobilephone>042398675</gues:personmobilephone>
//            <!--Optional:-->
//            <gues:phone>042398675</gues:phone>
//            <!--Optional:-->
//            <gues:salutation>Mr</gues:salutation>
//            <!--Optional:-->
//            <gues:shippingAddressDetails>
//               <!--Optional:-->
//               <gues:cityorsuburb></gues:cityorsuburb>
//               <!--Optional:-->
//               <gues:country></gues:country>
//               <!--Optional:-->
//               <gues:postcode></gues:postcode>
//               <!--Optional:-->
//               <gues:state>?</gues:state>
//               <!--Optional:-->
//               <gues:street></gues:street>
//            </gues:shippingAddressDetails>
//         </gues:guestDetails>
//         <!--Optional:-->
//         <gues:guestPreferences>
//            <!--Optional:-->
//            <gues:emailOptInTMC>true</gues:emailOptInTMC>
//            <!--Optional:-->
//            <gues:goPlacesPreference>true</gues:goPlacesPreference>
//            <!--Optional:-->
//            <gues:marketingPreference>true</gues:marketingPreference>
//            <!--Optional:-->
//            <gues:pnpOptIn>true</gues:pnpOptIn>
//            <!--Optional:-->
//            <gues:smsOptInTMC>true</gues:smsOptInTMC>
//         </gues:guestPreferences>
//         <!--Optional:-->
//         <gues:guestType></gues:guestType>
//         <!--Optional:-->
//         <gues:myToyotaId>mytochris</gues:myToyotaId>
//         <!--Optional:-->
//         <gues:salesforceId></gues:salesforceId>
//         <!--Optional:-->
//         <gues:vehicleDetails>
//            <!--Optional:-->
//            <gues:batchNumber></gues:batchNumber>
//            <!--Optional:-->
//           <!-- <gues:rdrDate></gues:rdrDate> -->
//            <!--Optional:-->
//            <gues:regoNo></gues:regoNo>
//            <!--Optional:-->
//            <gues:regoState></gues:regoState>
//            <!--Optional:-->
//            <gues:trackMyCar>
//               <!--Optional:-->
//               <gues:batchId>batch123</gues:batchId>
//               <!--Optional:-->
//               <gues:cosi>chr123</gues:cosi>
//               <!--Optional:-->
//               <gues:statusDescription>active</gues:statusDescription>
//            </gues:trackMyCar>
//            <!--Optional:-->
//            <gues:vin></gues:vin>
//         </gues:vehicleDetails>
//      </gues:request>
//   </gues:registerGuest>