package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;

import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.xml.sax.SAXException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

public class PostUsersMeVehiclesVinServicesValidateRequestTest extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;

	public PostUsersMeVehiclesVinServicesValidateRequestTest() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner
				.createScriptRunner("/mappings/post-users-me-vehicles-vin-services-validate-request.groovy");
	}
	
	@Test
	public void testUnsuccessful() throws Exception {
		String apiRequest = IOUtils
				.toString(getClass().getResourceAsStream("/in/post-users-me-vehicles-vin-services-request-unsuccessful.json"));
		try { 
			ScriptRunBuilder
				.runner(scriptRunner)
				.payload("")
				.flowVar("dmsID","61.10169.10169011116")
				.flowVar("dmsType","TUNE")
				.flowVar("vin","JTMBFREV80D078597")
				.flowVar("dmsBookingId", "TB16392302")
				.sessionVar("apiRequest", apiRequest)
				.run().toString();
		} catch (Exception e) {
			if(e.getMessage().contains("Invalid request"))
				assert(true);
			else
				assert(false);
			return;
		}
		assert(false);
	}
}
