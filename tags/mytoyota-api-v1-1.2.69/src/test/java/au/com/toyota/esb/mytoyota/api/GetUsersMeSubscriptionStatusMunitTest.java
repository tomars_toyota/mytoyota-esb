package au.com.toyota.esb.mytoyota.api;

import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;

@Ignore
public class GetUsersMeSubscriptionStatusMunitTest extends FunctionalMunitSuite {

	@Override
	protected String getConfigResources() {
		return StringUtils
				.join(new String[] { "get-user-account-info.xml", "get-user-account-info-technical-adapter.xml",
						"get-users-me-vehicles-technical-adapter.xml", "config.xml" }, " ");
	}

	@Test
	public void testSuccessfulWithSubscriptionQueryParamRequest() throws Exception {

		String expectedResponse = IOUtils.toString(getClass().getResourceAsStream(
				"/out/get-users-me-subscription-status-includeSubscriptions-true-api-response.json"));

		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("My User Details from myKIT"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							newMessage = originalMessage.createInboundMessage();

							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);

							newMessage.setPayload(
									getClass().getResourceAsStream("/in/get-user-account-info-mykit-response.json"));
						} catch (Exception e) {
							// catch exception statements
						}

						return newMessage;
					}
				});

		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(
						attribute("name").ofNamespace("doc").withValue("My Toyota App API GET Retrieve Users Vehicles"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							newMessage = originalMessage.createInboundMessage();

							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);

							newMessage.setPayload(
									getClass().getResourceAsStream("/in/get-users-me-vehicles-mykit-response.json"));
						} catch (Exception e) {
							// catch exception statements
						}

						return newMessage;
					}
				});

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("includeSubscriptions", "true");

		MuleMessage msg = muleMessageWithPayload(null);
		msg.setProperty("http.query.params", params, PropertyScope.INBOUND);

		MuleEvent event = testEvent("");
		event.setMessage(msg);

		MuleEvent output = runFlow("get:/users/me:myToyota-config", event);

		String payload = output.getMessage().getPayloadAsString();
		System.out.println(payload);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-user-account-info.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-user-account-info.response"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-user-account-info-technical-adapter.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-user-account-info-technical-adapter.response"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-user-vehicles-technical-adapter.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-user-vehicles-technical-adapter.response"))
				.times(1);
		assertJsonEquals(expectedResponse, output.getMessage().getPayloadAsString());
	}

	@Test
	public void testSuccessfulWithoutSubscriptionQueryParamRequest() throws Exception {

		String expectedResponse = IOUtils.toString(getClass().getResourceAsStream(
				"/out/get-users-me-subscription-status-includeSubscriptions-false-api-response.json"));

		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("My User Details from myKIT"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							newMessage = originalMessage.createInboundMessage();

							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);

							newMessage.setPayload(
									getClass().getResourceAsStream("/in/get-user-account-info-mykit-response.json"));
						} catch (Exception e) {
							// catch exception statements
						}

						return newMessage;
					}
				});

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("", "");

		MuleMessage msg = muleMessageWithPayload(null);
		msg.setProperty("http.query.params", params, PropertyScope.INBOUND);

		MuleEvent event = testEvent("");
		event.setMessage(msg);

		MuleEvent output = runFlow("get:/users/me:myToyota-config", event);

		String payload = output.getMessage().getPayloadAsString();
		System.out.println(payload);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-user-account-info.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-user-account-info.response"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-user-account-info-technical-adapter.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-user-account-info-technical-adapter.response"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-user-vehicles-technical-adapter.request"))
				.times(0);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-user-vehicles-technical-adapter.response"))
				.times(0);
		assertJsonEquals(expectedResponse, output.getMessage().getPayloadAsString());
	}

}
