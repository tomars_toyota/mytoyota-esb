package au.com.toyota.esb.mytoyota.api;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.module.apikit.exception.BadRequestException;
import org.mule.munit.runner.functional.FunctionalMunitSuite;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;


@Ignore
public class PostUsersMeTlinkMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"post-users-me-tlink.xml",
				"post-users-me-tlink-technical-adapter.xml",
				"get-users-me-vehicles-technical-adapter.xml",
				"get-user-account-info-technical-adapter.xml",
				"create-salesforce-sessionid-soap-header.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulRequest() throws Exception {
		
		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(attribute("name").ofNamespace("doc")
						.withValue("Invoke Salesforce Operation activateDeactivateCMUser"))
				.thenReturn(muleMessageWithPayload(""));
		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Partner Operation login"))
				.thenReturn(muleMessageWithPayload(
						IOUtils.toString(getClass().getResourceAsStream("/in/sfdc-login-response.xml"))));
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
				attribute("name").ofNamespace("doc").withValue("My User Details from myKIT"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						
						try {
							newMessage = originalMessage.createInboundMessage();
							
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							
							newMessage.setPayload(IOUtils.toString(
									getClass().getResourceAsStream("/in/get-users-me-myKit-response.json")));
						} catch (Exception e) {
							// catch exception statements
						}
						
						return newMessage;
					}
				});

		String requestPath = "/users/me/tlink";
		String apiRequest = IOUtils.toString(getClass().getResourceAsStream("/in/post-users-me-tlink-request.json"));

		Map<String, Object> params = new HashMap<String, Object>();
		params.put("includeSubscriptions", "");

		MuleMessage msg = muleMessageWithPayload(null);
		msg.setProperty("http.query.params", params, PropertyScope.INBOUND);
		
		msg.setPayload(apiRequest);
		
		MuleEvent event = testEvent(requestPath);
		event.setMessage(msg);
		//event.getMessage().setPayload(apiRequest);
		event.setFlowVariable("myToyotaId", "MYT-0000058b");

		runFlow("post:/users/me/tlink:myToyota-config", event);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.post-users-me-tlink.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.post-users-me-tlink.response"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.post-users-me-tlink-technical-adapter.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.post-users-me-tlink-technical-adapter.response"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-user-account-info-technical-adapter.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-user-account-info-technical-adapter.response"))
				.times(1);
	}
	
	@Test
	public void testFailedRequestInvalidUserDetails() throws Exception {
		
		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(attribute("name").ofNamespace("doc")
						.withValue("Invoke Salesforce Operation activateDeactivateCMUser"))
				.thenReturn(muleMessageWithPayload(""));
		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Partner Operation login"))
				.thenReturn(muleMessageWithPayload(
						IOUtils.toString(getClass().getResourceAsStream("/in/sfdc-login-response.xml"))));
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
				attribute("name").ofNamespace("doc").withValue("My User Details from myKIT"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						
						try {
							newMessage = originalMessage.createInboundMessage();
							
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							
							newMessage.setPayload(IOUtils.toString(
									getClass().getResourceAsStream("/in/get-users-me-myKit-response-no-firstName.json")));
						} catch (Exception e) {
							// catch exception statements
						}
						
						return newMessage;
					}
				});

		String requestPath = "/users/me/tlink";
		String apiRequest = IOUtils.toString(getClass().getResourceAsStream("/in/post-users-me-tlink-request.json"));

		MuleEvent event = testEvent(requestPath);
		event.getMessage().setPayload(apiRequest);
		event.setFlowVariable("myToyotaId", "MYT-0000058b");

		try { 
			runFlow("post:/users/me/tlink:myToyota-config", event);
		} catch (Exception e) {
			assertEquals("No Exception was thrown!",
					e.getCause().getCause() instanceof Exception, true);
			return;
		}
		
		assertTrue("No error was thrown!", false);		
	}
	
	
	@Test
	public void testFailedRequest() throws Exception {
		
		String requestPath = "/users/me/tlink";
		String apiRequest = IOUtils.toString(getClass().getResourceAsStream("/in/post-users-me-tlink-failed-request.json"));

		MuleEvent event = testEvent(requestPath);
		event.getMessage().setPayload(apiRequest);
		event.setFlowVariable("myToyotaId", "MYT-0000058b");

		try { 
			runFlow("post:/users/me/tlink:myToyota-config", event);
		} catch(Exception e) {
			assertEquals("No org.mule.module.apikit.exception.BadRequestException was thrown!",
					e.getCause().getCause() instanceof BadRequestException, true);
			return;
		}
		assertTrue("No error was thrown!", false);
	}

	@Test
	public void testFailedRequestNoVin() throws Exception {
		
		String requestPath = "/users/me/tlink";
		String apiRequest = IOUtils.toString(getClass().getResourceAsStream("/in/post-users-me-tlink-failed-request-no-vin.json"));

		MuleEvent event = testEvent(requestPath);
		event.getMessage().setPayload(apiRequest);
		event.setFlowVariable("myToyotaId", "MYT-0000058b");

		try { 
			runFlow("post:/users/me/tlink:myToyota-config", event);
		} catch(Exception e) {
			assertEquals("No org.mule.module.apikit.exception.BadRequestException was thrown!",
					e.getCause().getCause() instanceof BadRequestException, true);
			return;
		}
		assertTrue("No error was thrown!", false);
	}
}
