package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;
import java.util.Properties;

import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

public class GetFuelstationsResponseMapperTest extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;
	private Properties props;

	public GetFuelstationsResponseMapperTest() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/get-fuelstations-response-mapper.groovy");
	}

	@Test
	public void testSuccessfulRequestMapper() throws Exception {
		
		String result = ScriptRunBuilder.runner(scriptRunner)
				.payload(IOUtils
						.toString(getClass().getResourceAsStream("/in/get-fuelstations-caltex-response.json")))
				.run().toString();

		System.out.println(result);
		
		// expected
		String expectedResult = IOUtils
				.toString(getClass().getResourceAsStream("/out/get-fuelstations-response.json"));
		JSONAssert.assertEquals(expectedResult, result, true);
	}
}
