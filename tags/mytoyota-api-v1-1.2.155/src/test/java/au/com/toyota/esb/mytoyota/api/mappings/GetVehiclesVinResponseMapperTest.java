package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;

import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

public class GetVehiclesVinResponseMapperTest extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;

	public GetVehiclesVinResponseMapperTest() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/get-vehicles-vin-response-mapper.groovy");
	}

	@Test
	public void testSuccessfulResponseMapper() throws Exception {
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(getClass().getResourceAsStream("/in/get-vehicles-vin-vehiclesApi-response.json")))
				.flowVar("dummy",null)
				.flowVar("includeSellingBranch",true)
				.run().toString();

		System.out.println(result);

		// expected
		String expectedResult = IOUtils.toString(getClass()
				.getResourceAsStream(
						"/out/get-vehicles-vin-api-response.json"));
		JSONAssert.assertEquals(expectedResult, result, true);
	}
	 

	@Test
	public void testSuccessfulZeroDeliveryDateResponseMapper() throws Exception {
		String result = ScriptRunBuilder.runner(scriptRunner)
				.payload(IOUtils.toString(getClass()
						.getResourceAsStream("/in/get-vehicles-vin-vehiclesApi-zero-deliveryDate-response.json")))
				.flowVar("dummy", null)
				.flowVar("includeSellingBranch",false)
				.run().toString();

		System.out.println(result);

		// expected
		String expectedResult = IOUtils
				.toString(getClass().getResourceAsStream("/out/get-vehicles-vin-api-zero-deliveryDate-response.json"));
		JSONAssert.assertEquals(expectedResult, result, true);
	}
	
	@Test
	public void testSuccessfulInvalidDeliveryDateResponseMapper() throws Exception {
		String result = ScriptRunBuilder.runner(scriptRunner)
				.payload(IOUtils.toString(getClass()
						.getResourceAsStream("/in/get-vehicles-vin-vehiclesApi-invalid-deliveryDate-response.json")))
				.flowVar("dummy", null)
				.flowVar("includeSellingBranch",false)
				.run().toString();

		System.out.println(result);

		// expected
		String expectedResult = IOUtils
				.toString(getClass().getResourceAsStream("/out/get-vehicles-vin-api-invalid-deliveryDate-response.json"));
		JSONAssert.assertEquals(expectedResult, result, true);
	}
}
