import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper

def vehiclesListResponse = new JsonSlurper().parseText(payload)
def response = [:]

// Vehicle list placeholder
response['vehicles'] = []

// Build vehicle list
vehiclesListResponse.each{ v -> 
	def vehicle = [:]
	vehicle.batchNumber = v?.batchNumber
	vehicle.compliancePlate = v?.compliancePlate
	vehicle.engineNumber = v?.engineNumber
	vehicle.vin = v?.vin
	vehicle.materialNumber = v?.materialNumber
	vehicle.vehicleDescription = v?.vehicleDescription
	vehicle.katashikiCode = v?.katashikiCode
	vehicle.transmission = v?.transmission
	vehicle.sellingDealerCode = v?.sellingDealerCode
	vehicle.sellingDealerName = v?.sellingDealerName
	vehicle.registrationNumber = v?.registrationNumber
	vehicle.userRegistrationNumber = v?.userRegistrationNumber
	vehicle.state = v?.state
	vehicle.production = v?.production
	vehicle.discontinuedDate = v?.discontinuedDate
	vehicle.year = v?.year
	
	// Add to vehicle list
	response['vehicles'].add(vehicle)
}

// Return
return toJson(response)