package au.com.toyota.esb.mytoyota.api;

import static org.junit.Assert.assertTrue;
import static org.mule.munit.common.mocking.Attribute.attribute;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.custommonkey.xmlunit.Diff;
import org.json.JSONObject;
import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleException;
import org.mule.munit.common.mocking.SpyProcess;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.skyscreamer.jsonassert.JSONAssert;
import org.w3c.dom.Element;


public class PutUpdateUserAccountInfoMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"create-salesforce-sessionid-soap-header.xml",
				"put-update-user-account-info.xml",
				"put-update-user-account-info-technical-adapter.xml",
				"mytoyota-app-api-auth-session.xml",
				"config.xml"
		}, " ");
	}

	//@Test
	public void testSuccessfulRequest() throws Exception {

		String expectedResult = "{\"success\":true}";
		String requestPath = "/users/me";
		String apiRequest = IOUtils.toString(getClass().getResourceAsStream("/in/put-update-user-account-info-request.json"));
		String salesforceSessionIdResponse = IOUtils.toString(getClass().getResourceAsStream("/in/salesforce-get-sessionid-response.xml"));
		String salesforceUpdateOwnerResponse = IOUtils.toString(getClass().getResourceAsStream("/in/salesforce-update-owner-response.xml"));
		String imUpdateAccountResponse = IOUtils.toString(getClass().getResourceAsStream("/in/im-update-account-response.json"));
		
		//Assert mykit request
		spyMessageProcessor("request").ofNamespace("http")
		.withAttributes(attribute("name").ofNamespace("doc").withValue("myKIT API POST Save Account")).before( new SpyProcess() {
			
			@Override
			public void spy(MuleEvent arg0) throws MuleException {
				try {
					String myKitExpectedRequest = IOUtils.toString(getClass().getResourceAsStream("/out/put-update-user-account-info-mykit-request.json"));
					System.out.println(arg0.getMessage().getPayloadAsString());
					JSONAssert.assertEquals(myKitExpectedRequest, arg0.getMessage().getPayloadAsString(), true);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		
		//Assert sfdc login request
		spyMessageProcessor("consumer").ofNamespace("ws")
		.withAttributes(attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Operation login")).before( new SpyProcess() {
			
			@Override
			public void spy(MuleEvent arg0) throws MuleException {
				try {
					String expectedResult = IOUtils.toString(getClass().getResourceAsStream(
							"/out/salesforce-login-request.xml"));
					System.out.println(arg0.getMessage().getPayloadAsString());
					Diff diff = new Diff(expectedResult, arg0.getMessage().getPayloadAsString());
					assertTrue(diff.similar());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		
		//Assert sfdc updateOwner request
		spyMessageProcessor("consumer").ofNamespace("ws")
		.withAttributes(attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Operation updateOwner")).before( new SpyProcess() {
			
			@Override
			public void spy(MuleEvent arg0) throws MuleException {
				try {
					String expectedResult = IOUtils.toString(getClass().getResourceAsStream(
							"/out/put-update-user-account-info-sfdc-request.xml"));
					System.out.println(arg0.getMessage().getPayloadAsString());
					Diff diff = new Diff(expectedResult, arg0.getMessage().getPayloadAsString());
					assertTrue(diff.similar());
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		
		//Assert IM request
		spyMessageProcessor("request").ofNamespace("http")
		.withAttributes(attribute("name").ofNamespace("doc").withValue("IM API Update User")).before( new SpyProcess() {
			
			@Override
			public void spy(MuleEvent arg0) throws MuleException {
				try {
					String imExpectedRequest = IOUtils.toString(getClass().getResourceAsStream("/out/put-update-user-account-info-im-request.json"));
					System.out.println(arg0.getMessage().getPayloadAsString());
					JSONAssert.assertEquals(imExpectedRequest, arg0.getMessage().getPayloadAsString(), true);
				} catch (Exception e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
			}
		});
		
		//Check Authtoken in cache 
		whenMessageProcessor("contains").ofNamespace("objectstore")
		.withAttributes(attribute("name").ofNamespace("doc").withValue("Check Auth Token in ObjectStore"))
		.thenReturn(muleMessageWithPayload(true));
		//Return Authtoken in cache
		whenMessageProcessor("retrieve").ofNamespace("objectstore")
		.withAttributes(attribute("name").ofNamespace("doc").withValue("Get Auth Token from ObjectStore"))
		.thenReturnSameEvent();
		//Save in myKIT
		whenMessageProcessor("request").ofNamespace("http")
		.withAttributes(attribute("name").ofNamespace("doc").withValue("myKIT API POST Save Account"))
		.thenReturn(muleMessageWithPayload(""));
		//Login Salesforce
//		whenMessageProcessor("consumer").ofNamespace("ws")
//		.withAttributes(attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Operation login"))
//		.thenReturn(muleMessageWithPayload(salesforceSessionIdResponse));
//		
//		//Update Owner in Salsforce
//		whenMessageProcessor("consumer").ofNamespace("ws")
//		.withAttributes(attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Operation updateOwner"))
//		.thenReturn(muleMessageWithPayload(salesforceUpdateOwnerResponse));
		
		//Update IM
		whenMessageProcessor("request").ofNamespace("http")
		.withAttributes(attribute("name").ofNamespace("doc").withValue("IM API Update User"))
		.thenReturn(muleMessageWithPayload(imUpdateAccountResponse));
		
		MuleEvent event = testEvent(requestPath);
		event.getMessage().setPayload(apiRequest);
		//Setting "myToyotaId" flowvar which normally gets extracted of authtoken
		event.setFlowVariable("myToyotaId", "myToyotaId");

		MuleEvent output = runFlow("put:/users/me:myToyota-config", event);

		System.out.println(output.getMessage().getPayload());

		JSONAssert.assertEquals(expectedResult, (String)output.getMessage().getPayload(), true);
		verifyCallOfMessageProcessor("request").ofNamespace("http")
		.withAttributes(attribute("name").ofNamespace("doc").withValue("IM API Update User")).times(1);
		verifyCallOfMessageProcessor("request").ofNamespace("http")
		.withAttributes(attribute("name").ofNamespace("doc").withValue("IM API Update User")).times(1);
//		verifyCallOfMessageProcessor("consumer").ofNamespace("ws")
//		.withAttributes(attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Operation updateOwner")).times(1);
//		verifyCallOfMessageProcessor("consumer").ofNamespace("ws")
//		.withAttributes(attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Operation login")).times(1);
	}
	
}
