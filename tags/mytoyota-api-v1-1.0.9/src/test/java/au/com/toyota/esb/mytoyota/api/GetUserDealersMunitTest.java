package au.com.toyota.esb.mytoyota.api;

import au.com.toyota.esb.mytoyota.api.munit.BlockingMuleEventSpy;

import org.junit.Test;

import java.util.Properties;

import org.mule.util.StringUtils;
import org.mule.util.IOUtils;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.modules.interceptor.processors.MuleMessageTransformer;
import org.mule.module.http.internal.request.ResponseValidatorException;

import static org.junit.Assert.*;
import static org.mule.munit.common.mocking.Attribute.attribute;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;


/*
 * Munit test to unit test the functionality to get dealers of a user
 * @author: swapnil
 */
public class GetUserDealersMunitTest extends FunctionalMunitSuite {
	
	private static Properties adapterProperties = new Properties();

	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"get-user-dealers.xml",
				"get-user-dealers-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulGetUserDealers() throws Exception {
		
		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));

		final String responsePayload = IOUtils.toString(getClass().getResourceAsStream("/out/get-user-dealers-response.json"));

		whenMessageProcessor("request").ofNamespace("http").withAttributes(
			attribute("name").ofNamespace("doc").withValue("My Toyota App API Get User Dealers"))
			.thenApply(new MockResponseTransformer() {
				@Override
				public MuleMessage transform(MuleMessage originalMessage) {
					MuleMessage newMessage = null;
					
					try {
						newMessage = originalMessage.createInboundMessage();
						
						newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
						newMessage.setProperty("Content-Type", "application/json;charset=UTF-8", PropertyScope.INBOUND);
						
						newMessage.setPayload(IOUtils.toInputStream(responsePayload, "UTF-8"));
					} catch (Exception e) {
						// catch exception statements
					}
					
					return newMessage;
				}
			});
		
		BlockingMuleEventSpy<String> appAPIRequestMessageSpy = new BlockingMuleEventSpy<String>(1, 5000);
		spyMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(
			attribute("name").ofNamespace("doc").withValue("Log Get User Dealers App Request"))
			.after(appAPIRequestMessageSpy);

		MuleEvent event = testEvent("");

		// Invoke the flow
		MuleEvent output = runFlow("get:/users/me/dealers:myToyota-config", event);
		
		appAPIRequestMessageSpy.block();

		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Get User Dealers App Request")).times(1);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Get User Dealers App Response")).times(1);	

		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Get User Dealers App Request")).times(1);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Get User Dealers App Response")).times(1);	

		MuleEvent appAPIRequestMessageMuleEvent = appAPIRequestMessageSpy.getMuleEvent();
			
		// Assert app api request
		assertEquals(adapterProperties.getProperty("api.mytoyota-app.authTokenHeaderValue"), 
			appAPIRequestMessageMuleEvent.getMessage().getProperty(adapterProperties.getProperty("api.mytoyota-app.authTokenHeader"), PropertyScope.OUTBOUND));
		
		assertEquals("", appAPIRequestMessageSpy.getFirstPayload());

		// Assert api response
		assertEquals(200, Integer.parseInt(output.getMessage().getOutboundProperty("http.status").toString()));
		assertEquals("application/json", output.getMessage().getOutboundProperty("Content-Type"));

		assertJsonEquals(responsePayload, output.getMessage().getPayloadAsString());

	}
	
	@Test
	public void testFailureGetUserDealers() throws Exception {
		
		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));

		MuleEvent event = testEvent(null);

		whenMessageProcessor("request").ofNamespace("http").withAttributes(
			attribute("name").ofNamespace("doc").withValue("My Toyota App API Get User Dealers"))
			.thenThrow(new ResponseValidatorException("Response code 400 mapped as failure.", event));
			
		try {

			// Invoke the flow
			runFlow("get:/users/me/dealers:myToyota-config", event);
			
			// validating exception thrown
			assertEquals(true, false);

		} catch (ResponseValidatorException e) {

			
			verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Get User Dealers App Request")).times(1);	
			verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Get User Dealers App Response")).times(0);	

			verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Get User Dealers App Request")).times(1);	
			verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Get User Dealers App Response")).times(0);
			
		}

	}
	
	//not possible to externalise this class because of the visibility of the method muleMessageWithPayload
	private class MockResponseTransformer implements MuleMessageTransformer {
		int count = 0;
		String[] payloads;

		public MockResponseTransformer(String... payloads) {
			this.payloads = payloads;
		}

		@Override
		public MuleMessage transform(MuleMessage originalMessage) {
			return muleMessageWithPayload(payloads[count++]);
		}
	}
	
}
