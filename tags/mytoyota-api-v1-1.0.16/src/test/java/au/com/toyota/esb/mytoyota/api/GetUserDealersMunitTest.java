package au.com.toyota.esb.mytoyota.api;

import java.util.HashMap;
import java.util.Map;
import org.junit.Test;

import java.util.Properties;

import org.mule.util.StringUtils;
import org.mule.util.IOUtils;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.modules.interceptor.processors.MuleMessageTransformer;
import org.mule.module.http.internal.request.ResponseValidatorException;

import static org.junit.Assert.*;
import static org.mule.munit.common.mocking.Attribute.attribute;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;


/*
 * Munit test to unit test the functionality to get dealers of a user
 * @author: swapnil
 */
public class GetUserDealersMunitTest extends FunctionalMunitSuite {
	
	private static Properties adapterProperties = new Properties();

	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"get-user-dealers.xml",
				"get-user-dealers-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulGetUserDealers() throws Exception {
		
		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));

		final String responsePayload = IOUtils.toString(getClass().getResourceAsStream("/in/get-users-dealers-mytoyotaapi-response.json"));

		whenMessageProcessor("request").ofNamespace("http").withAttributes(
			attribute("name").ofNamespace("doc").withValue("My Toyota App API Get User Dealers"))
			.thenApply(new MockResponseTransformer() {
				@Override
				public MuleMessage transform(MuleMessage originalMessage) {
					MuleMessage newMessage = null;
					
					try {
						newMessage = originalMessage.createInboundMessage();
						
						newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
						newMessage.setProperty("Content-Type", "application/json;charset=UTF-8", PropertyScope.INBOUND);
						
						newMessage.setPayload(IOUtils.toInputStream(responsePayload, "UTF-8"));
					} catch (Exception e) {
						// catch exception statements
					}
					
					return newMessage;
				}
			});
		
		MuleEvent event = testEvent("");
		Map<String, Object> props = new HashMap<String,Object>();
		//props.put("showDeletedOnly", true);
        
		
		MuleMessage msg = muleMessageWithPayload(null);
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
		event.setMessage(msg);	
        
		// Invoke the flow
		MuleEvent output = runFlow("get:/users/me/dealers:myToyota-config", event);
		

		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Get User Dealers App Request")).times(1);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Get User Dealers App Response")).times(1);	

		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Get User Dealers App Request")).times(1);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Get User Dealers App Response")).times(1);	

		assertJsonEquals(IOUtils.toString(getClass().getResourceAsStream(
				"/out/get-users-dealers-api-response.json")), output
				.getMessage().getPayloadAsString());

	}
	
	@Test
	public void testSuccessfulGetUserDeletedDealers() throws Exception {
		
		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));

		final String responsePayload = IOUtils.toString(getClass().getResourceAsStream("/in/get-users-deleted-dealers-mytoyotaapi-response.json"));

		whenMessageProcessor("request").ofNamespace("http").withAttributes(
			attribute("name").ofNamespace("doc").withValue("My Toyota App API Get User Deleted Dealers"))
			.thenApply(new MockResponseTransformer() {
				@Override
				public MuleMessage transform(MuleMessage originalMessage) {
					MuleMessage newMessage = null;
					
					try {
						newMessage = originalMessage.createInboundMessage();
						
						newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
						newMessage.setProperty("Content-Type", "application/json;charset=UTF-8", PropertyScope.INBOUND);
						
						newMessage.setPayload(IOUtils.toInputStream(responsePayload, "UTF-8"));
					} catch (Exception e) {
						// catch exception statements
					}
					
					return newMessage;
				}
			});
		
		MuleEvent event = testEvent("");
		Map<String, Object> props = new HashMap<String,Object>();
		props.put("showDeletedOnly", true);
        
		
		MuleMessage msg = muleMessageWithPayload(null);
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
		event.setMessage(msg);	
        
		// Invoke the flow
		MuleEvent output = runFlow("get:/users/me/dealers:myToyota-config", event);
		

		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Get User Dealers App Request")).times(1);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Get User Dealers App Response")).times(1);	

		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Get User Dealers App Request")).times(1);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Get User Dealers App Response")).times(1);	

		assertJsonEquals(IOUtils.toString(getClass().getResourceAsStream(
				"/out/get-users-deleted-dealers-api-response.json")), output
				.getMessage().getPayloadAsString());

	}
	
	@Test
	public void testFailureGetUserDealers() throws Exception {
		
		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));

		MuleEvent event = testEvent("");
		Map<String, Object> props = new HashMap<String,Object>();
		MuleMessage msg = muleMessageWithPayload(null);
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
		event.setMessage(msg);	
        
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
			attribute("name").ofNamespace("doc").withValue("My Toyota App API Get User Dealers"))
			.thenThrow(new ResponseValidatorException("Response code 400 mapped as failure.", event));
			
		try {

			// Invoke the flow
			runFlow("get:/users/me/dealers:myToyota-config", event);
			
			// validating exception thrown
			assertEquals(true, false);

		} catch (ResponseValidatorException e) {

			
			verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Get User Dealers App Request")).times(1);	
			verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Get User Dealers App Response")).times(0);	

			verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Get User Dealers App Request")).times(1);	
			verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Get User Dealers App Response")).times(0);
			
		}

	}
	
	//not possible to externalise this class because of the visibility of the method muleMessageWithPayload
	private class MockResponseTransformer implements MuleMessageTransformer {
		int count = 0;
		String[] payloads;

		public MockResponseTransformer(String... payloads) {
			this.payloads = payloads;
		}

		@Override
		public MuleMessage transform(MuleMessage originalMessage) {
			return muleMessageWithPayload(payloads[count++]);
		}
	}
	
}
