package au.com.toyota.esb.mytoyota.api;

import java.util.HashMap;
import java.util.Map;
import org.junit.Test;

import org.mule.util.StringUtils;
import org.mule.util.IOUtils;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.modules.interceptor.processors.MuleMessageTransformer;

import static org.mule.munit.common.mocking.Attribute.attribute;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;


/*
 * Munit test to unit test the functionality to get service operation time through 
 * /users/me/vehicles/{vin}/services/operationTime
 * @author: ahuwait
 */
public class GetUsersServiceOperationTimeMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"get-users-me-vehicles-vin-services-operationtime.xml",
				"get-users-me-vehicles-vin-services-operationtime-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulGetServiceOperationTime() throws Exception {
		
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
			attribute("name").ofNamespace("doc").withValue("TUNE API: GET /getoperationtime"))
			.thenApply(new MockResponseTransformer() {
				@Override
				public MuleMessage transform(MuleMessage originalMessage) {
					MuleMessage newMessage = null;
					
					try {
						newMessage = originalMessage.createInboundMessage();
						
						newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
						newMessage.setProperty("Content-Type", "application/json;charset=UTF-8", PropertyScope.INBOUND);
						
						String tuneResponse = "{\"operationTime\":[{\"timeReq\":1.5}]}";
						newMessage.setPayload(IOUtils.toInputStream(tuneResponse, "UTF-8"));
					} catch (Exception e) {
						// catch exception statements
					}
					
					return newMessage;
				}
			});
		
		
		MuleEvent event = testEvent("");
		
		Map<String, Object> props = new HashMap<String,Object>();
		props.put("vin", "6T153BK360X070769");
        props.put("operationTypeID", "SCHEDULE");
        props.put("operationID", "TOY30895-S0750M45S-1");
        props.put("upsellOptions", "0x000000000f584386|0x000000000f584387|0x000000000f584388");
        props.put("dmsType", "TUNE");
		
		MuleMessage msg = muleMessageWithPayload(null);
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
        
        event.setMessage(msg);		
		
		// Invoke the flow
		MuleEvent output = runFlow("get:/users/me/vehicles/{vin}/services/operationTime:myToyota-config", event);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Request")).times(1);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Response")).times(1);	

		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Request - tech adapter")).times(1);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils").withAttributes(attribute("name").ofNamespace("doc").withValue("Log Response - tech adapter")).times(1);	

		
		//System.out.println("_____________________"+output.getMessage().getPayloadAsString());
		String expectedResponse = "{\"totalOperationTime\":1.5}";
		assertJsonEquals(expectedResponse, output.getMessage().getPayloadAsString());

	}
	
	//not possible to externalise this class because of the visibility of the method muleMessageWithPayload
	private class MockResponseTransformer implements MuleMessageTransformer {
		int count = 0;
		String[] payloads;

		public MockResponseTransformer(String... payloads) {
			this.payloads = payloads;
		}

		@Override
		public MuleMessage transform(MuleMessage originalMessage) {
			return muleMessageWithPayload(payloads[count++]);
		}
	}
	
}
