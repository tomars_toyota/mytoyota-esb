import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper


def response = [:]
response.upsells = []
response.serviceOperation = [:]

JsonSlurper js = new JsonSlurper()

//def getVehicleResponse = js.parseText(sessionVars['getVehicleResponse'])
def getVehicleResponse = sessionVars['getVehicleResponse']
def upsellsList = getVehicleResponse.vehicleData.upsells
def serviceOp = getVehicleResponse.vehicleData.serviceOperations[0]

upsellsList.each { Map upsellItem ->
	
	def upsell = [:]
	upsell.description = upsellItem.opdsc
	upsell.price = upsellItem.stdchg
	upsell.id = upsellItem.x_rowid
	response.upsells << upsell
	
}

response.serviceOperation.operationTypeId = serviceOp.optid
response.serviceOperation.operationId = serviceOp.opid
response.serviceOperation.description = serviceOp.opdsc
response.serviceOperation.tsaFlag = serviceOp.tsaqua
response.serviceOperation.price = serviceOp.fixchg
response.serviceOperation.externalClaimNumber = serviceOp.waropr
response.serviceOperation.serviceCode = serviceOp.warscd
response.serviceOperation.fixedChargeOperationFlag = serviceOp.fixflg
response.serviceOperation.cappedPriceOperationFlag = serviceOp.capflg
response.serviceOperation.procedureDescription = js.parseText(payload).procedureData.Procedure[0].Description

return toJson(response)