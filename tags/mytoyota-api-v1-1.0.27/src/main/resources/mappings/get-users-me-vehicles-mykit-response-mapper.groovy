import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper
import java.text.SimpleDateFormat

def vehiclesListResponse = new JsonSlurper().parseText(payload)
def response = [:]

// Vehicle list placeholder
response['vehicles'] = []

// Build vehicle list
vehiclesListResponse.each{ v -> 
	def vehicle = [:]
	vehicle.batchNumber = v?.batchNumber
	vehicle.compliancePlate = v?.compliancePlate
	vehicle.engineNumber = v?.engineNumber
	vehicle.vin = v?.vin
	vehicle.materialNumber = v?.materialNumber
	vehicle.vehicleDescription = v?.vehicleDescription
	vehicle.katashikiCode = v?.katashikiCode
	vehicle.transmission = v?.transmission
	vehicle.sellingDealerCode = v?.sellingDealerCode
	vehicle.sellingDealerName = v?.sellingDealerName
	vehicle.registrationNumber = v?.registrationNumber
	vehicle.production = v?.production

	if(v?.discontinuedDate)
		vehicle.discontinuedDate = new SimpleDateFormat("yyyy-MM-dd").format(new Date(v.discontinuedDate))

	vehicle.releaseYear = v?.year
	
	if(v?.suffixCode) {
		vehicle.suffix = [:]
		vehicle.suffix.code = v.suffixCode
		vehicle.suffix.description = v.suffixDescription
	}
	if(v?.trimCode) {
		vehicle.trim = [:]
		vehicle.trim.code = v.trimCode
		vehicle.trim.description = v.trimDescription
	}
	if(v?.paintCode) {
		vehicle.paint = [:]
		vehicle.paint.code = v.paintCode
		vehicle.paint.description = v.paintDescription
	}


	vehicle.salesType = v?.salesType	
	
	//stock images
	/*imagesList = []
	v?.images.each { Map image ->
		imageMap = [:]
		imageMap.size = image.code
		imageMap.link = image.url

		//remove nulls
		imageMap = imageMap.findAll {it.value != null && it.value != ""}

		if(imageMap)
			imagesList << imageMap
	}

	if(imagesList)
		vehicle.images = imagesList
	*/

	//vehicle user
	vehicleUserMap = [:]

	vehicleUserMap.registrationNumber = v?.userRegistrationNumber
	vehicleUserMap.state = v?.state

	//remove nulls
	vehicleUserMap = vehicleUserMap.findAll {it.value != null && it.value != ""}

	if(vehicleUserMap)
		vehicle.vehicleUser = vehicleUserMap

	//custom image
	if(v?.vehicleImage) {
		customImages = []
		customImage = [:]
		customImage.code = "primary"
		customImage.link = v?.vehicleImage
		customImages << customImage
		vehicle.customImages = customImages
	}


	//remove nulls
	vehicle = vehicle.findAll {it.value != null && it.value != ""}
	
	// Add to vehicle list
	response['vehicles'].add(vehicle)
}


//println ('_________________________'+toJson(response))

// Return
return prettyPrint(toJson(response))