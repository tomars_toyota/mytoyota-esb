package au.com.toyota.esb.mytoyota.api;

import java.util.List;

import org.junit.Test;
import org.mule.util.StringUtils;
import org.mule.util.IOUtils;
import org.mule.api.MuleEvent;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.skyscreamer.jsonassert.JSONAssert;

import com.jayway.jsonpath.JsonPath;

import static org.junit.Assert.assertEquals;
import static org.mule.munit.common.mocking.Attribute.attribute;


/*
 * Munit test to unit test the functionality to get user notifications
 * @author: ibrahim.abouelela
 */
public class GetTCLatestMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"get-tc-latest.xml",
				"get-tc-latest-technical-adapter.xml",
				"config.xml"
		}, " ");
	}
	
	@Test
	public void testSuccessfulRequest() throws Exception {

		String apiResponse = IOUtils.toString(getClass().getResourceAsStream("/in/get-tc-latest-response.json"));
		
		whenMessageProcessor("request").ofNamespace("http")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("Sitecore TC Latest"))
			.thenReturn(muleMessageWithPayload(apiResponse));

		MuleEvent event = testEvent("");

		MuleEvent output = runFlow("get:/tc/latest:myToyota-config", event);
		String payload = output.getMessage().getPayloadAsString();
		System.out.println(payload);
		
		verifyCallOfMessageProcessor("log-message")
			.ofNamespace("esbutils")
			.withAttributes(
					attribute("name")
							.ofNamespace("doc")
							.withValue(
									"Log API Request"))
			.times(1);
		verifyCallOfMessageProcessor("log-message")
			.ofNamespace("esbutils")
			.withAttributes(
					attribute("name")
							.ofNamespace("doc")
							.withValue(
									"Log API Response"))
			.times(1);
		
		verifyCallOfMessageProcessor("log-message")
			.ofNamespace("esbutils")
			.withAttributes(
					attribute("name")
							.ofNamespace("doc")
							.withValue(
									"Log Request"))
			.times(1);
		
		verifyCallOfMessageProcessor("log-message")
			.ofNamespace("esbutils")
			.withAttributes(
					attribute("name")
							.ofNamespace("doc")
							.withValue(
									"Log Response"))
			.times(1);
		// expected
		String expectedResult = IOUtils.toString(getClass()
				.getResourceAsStream(
						"/out/get-tc-latest-api-response.json"));
		JSONAssert.assertEquals(expectedResult, payload, true);
	}
	
}
