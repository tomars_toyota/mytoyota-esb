import groovy.json.JsonSlurper
import static groovy.json.JsonOutput.*
import au.com.toyota.esb.mytoyota.api.util.GroovyHelper



dealersResponse = new groovy.json.JsonSlurper().parseText(payload)

if(!dealersResponse?.dealers)
	throw new org.mule.module.apikit.exception.NotFoundException("Dealer not found")

dealer = dealersResponse.dealers[0]


dealerMap = [:]

dealerMap.dealerCode = dealer.dealerCode
dealerMap.dealerName = dealer.dealerName
dealerMap.franchise = dealer.franchise
dealerMap.dealerPartsCode = dealer.dealerPartsCode
dealerMap.sapDealerCode = dealer.sapDealerCode
dealerMap.motorDealerLicenseNumber = dealer.motorDealerLicenseNumber
dealerMap.isMetro = dealer.isMetro == 'true' ? true : false
dealerMap.isRural = dealer.isRural == 'true' ? true : false
dealerMap.pricingTransparencyOptIn = dealer.pricingTransparencyOptIn == 'true' ?  true : false
dealerMap.dmsType = dealer.dmsVendorName

sitesList = []
dealer.locations.each { location ->
	site = [:]

	site.isPrincipal = location.locationType?.toLowerCase() == 'principal' ? true : false
	site.locationType = location.locationType
	site.branchCode = location.locationId
	site.sapCode = location.sapCode
	site.serviceRequestLeadTimeInDays = 2
	site.branchName = location.locationName
	site.dmsId = location.serviceSiteId
	site.address = location.address
	site.poBox = location.poBox
	site.phone = location.phone
	site.email = location.email
	site.webSite = location.webSite

	serviceTypesList = []

	location.serviceTypes.each { serviceType ->
		serviceTypeMap = [:]

		serviceTypeMap.name = serviceType.name
		serviceTypeMap.comments = serviceType.comments
		
		operatingHoursList = []

		serviceType.OperatingHours.each { opHoursSet ->
			operatingHoursMap = [:]
			
			operatingHoursMap.day = opHoursSet.day
			operatingHoursMap.isClosed = opHoursSet.closed == 'true' ? true : false
			if(!operatingHoursMap.isClosed) {
				operatingHoursMap.openingAt = opHoursSet.openingTime?.take(5)
				operatingHoursMap.closingAt = opHoursSet.closingTime?.take(5)
			}

			operatingHoursList.add(operatingHoursMap)
		}
		serviceTypeMap.hoursOfOperation = operatingHoursList

		serviceTypesList.add(serviceTypeMap)
	}

	site.serviceTypes = serviceTypesList

	sitesList.add(site)
}

dealerMap.sites = sitesList


dealerMap = GroovyHelper.removeNulls(dealerMap)


return prettyPrint(toJson(dealerMap))