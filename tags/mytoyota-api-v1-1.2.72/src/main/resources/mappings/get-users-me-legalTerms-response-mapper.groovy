import groovy.json.JsonSlurper
import groovy.json.JsonBuilder

//def acceptedTcVersion = new JsonSlurper().parseText(payload).data?.myToyotaAcceptedTerms?.acceptedVersion
def profile = new JsonSlurper().parseText(payload)
//println profile
def acceptedTcVersion = profile?.data?.myToyotaAcceptedTerms?.acceptedVersion
def acceptedPrivacyVersion = profile?.data?.myToyotaAcceptedPrivacyPolicy?.acceptedVersion

//println '_______acceptedTcVersion: ' + acceptedTcVersion
//println '_______acceptedPrivacyVersion: ' + acceptedPrivacyVersion

if (acceptedTcVersion == null && acceptedPrivacyVersion == null)
	throw new org.mule.module.apikit.exception.NotFoundException("No available accepted terms and conditions or accepted privacy policy version")

def response = [:]

if(acceptedTcVersion != null)
	response["termsAndConditions"] = ["acceptedVersion":acceptedTcVersion]
if(acceptedPrivacyVersion != null)
	response["privacyStatement"] = ["acceptedVersion":acceptedPrivacyVersion]		

return new JsonBuilder(response).toString()