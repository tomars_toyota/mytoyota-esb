package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Properties;

import java.util.HashMap;

import javax.script.ScriptException;

import org.mule.DefaultMuleMessage;
import org.mule.api.MuleContext;
import org.mule.api.MuleEventContext;
import org.mule.api.MuleMessage;
import org.mule.context.DefaultMuleContextFactory;

import org.apache.commons.io.IOUtils;
import org.custommonkey.xmlunit.XMLTestCase;
import org.junit.Test;
import org.xml.sax.SAXException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

public class SessionTokensCreateMapperTest extends XMLTestCase {

	private ScriptRunner createTokenScriptRunner;
	private ScriptRunner verifyTokenScriptRunner;
	
	private MuleContext muleContext;
	private Properties props;

	public SessionTokensCreateMapperTest() throws ScriptException, SAXException, IOException {
		createTokenScriptRunner = ScriptRunner.createScriptRunner("/mappings/jwt-token-create.groovy");
		verifyTokenScriptRunner = ScriptRunner.createScriptRunner("/mappings/jwt-token-validate.groovy");
		
		//load properties
		props = new Properties();
		props.load(getClass().getResourceAsStream("/mytoyota-api-v1-constants.properties"));
				
	}
					
	@Test
	public void testCreateVerifyToken() throws Exception {
		
		muleContext = new DefaultMuleContextFactory().createMuleContext();
		MuleMessage message = new DefaultMuleMessage(null,muleContext);
		String secretKey = props.getProperty("jwt.secret.key");
		
		String issuer = props.getProperty("jwt.issuer");
		
		HashMap<String, Object> flowVars = new HashMap<String, Object>();
		flowVars.put("myToyotaId", "MYT-0000063c");
		flowVars.put("UID", "04707fa24bc5400cbf7cd3db2ecbf94a");
		
		String createdToken = ScriptRunBuilder
				.runner(createTokenScriptRunner)
				.payload("")
				.flowVars(flowVars)
				.binding("tokenType", "tlinkSessionToken")
				.binding("secretKey", secretKey)
				.binding("issuer", issuer)
				.message(message)
				.run().toString();
		
		HashMap<String, Object> sessionToken = (HashMap<String, Object>) flowVars.get("APISessionToken");
		
		assertNotNull( sessionToken );
		assert( sessionToken.get("value").toString().length() > 0);
		assert( sessionToken.get("tokenType").toString().equals("tlinkSessionToken"));
		
	}
}
