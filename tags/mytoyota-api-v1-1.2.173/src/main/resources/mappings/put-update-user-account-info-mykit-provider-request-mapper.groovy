import groovy.json.JsonSlurper
import groovy.json.JsonBuilder
import au.com.toyota.esb.mytoyota.api.util.GroovyHelper

def user = new JsonSlurper().parseText(payload)
def result = [:]

if (user?.login != null)
	result.login = user.login

if (user?.title != null)
	result.title = user.title

if (user?.firstName != null)
	result.firstName = user.firstName

if (user?.lastName != null)
	result.lastName = user.lastName

if (user?.email != null)
	result.email = user.email

if (user?.mobile != null)
	result.mobile = user.mobile

//remove nulls and leave blanks
if(user)
	user.address = GroovyHelper.removeNulls(user.address, true)

if(user?.address?.postal) {
	postalAddress = user.address.postal
	addressMap = [:]

	addressMap.formattedAddress = postalAddress.fullAddress
	addressMap.referenceId = postalAddress.totalCheckId ?: 'other'
	addressMap.streetAddress = postalAddress.streetAddress
	addressMap.postcode = postalAddress.postcode
	addressMap.suburb = postalAddress.suburb
	addressMap.state = postalAddress.state

	addressMap.primaryName  = postalAddress.primaryName
    addressMap.secondaryName  = postalAddress.secondaryName
    addressMap.unit = postalAddress.unit
    addressMap.streetNo = postalAddress.streetNo
    addressMap.streetName = postalAddress.streetName
    addressMap.streetType = postalAddress.streetType
    //addressMap.country = postalAddress.country 					//country is not being snet by fronte-end
    addressMap.geoLat = postalAddress.geoLat
    addressMap.geoLon = postalAddress.geoLon
    addressMap.geoLatStreet = postalAddress.geoLatStreet
    addressMap.geoLonStreet = postalAddress.geoLonStreet
    addressMap.listing = postalAddress.listing
    addressMap.postal = postalAddress.postal
    addressMap.dpid = postalAddress.dpid

    //remove nulls
    addressMap = addressMap.findAll { it.value != null}

    if(addressMap)
    	result.address = addressMap
}

//[MYT-253]add residential address
if(user?.address?.residential) {
	residentialAddress = user.address.residential
	residentialAddressMap = [:]

	residentialAddressMap.formattedAddress = residentialAddress.fullAddress
	residentialAddressMap.referenceId = residentialAddress.totalCheckId ?: 'other'
	residentialAddressMap.streetAddress = residentialAddress.streetAddress
	residentialAddressMap.postcode = residentialAddress.postcode
	residentialAddressMap.suburb = residentialAddress.suburb
	residentialAddressMap.state = residentialAddress.state

	residentialAddressMap.primaryName  = residentialAddress.primaryName
    residentialAddressMap.secondaryName  = residentialAddress.secondaryName
    residentialAddressMap.unit = residentialAddress.unit
    residentialAddressMap.streetNo = residentialAddress.streetNo
    residentialAddressMap.streetName = residentialAddress.streetName
    residentialAddressMap.streetType = residentialAddress.streetType
    residentialAddressMap.geoLat = residentialAddress.geoLat
    residentialAddressMap.geoLon = residentialAddress.geoLon
    residentialAddressMap.geoLatStreet = residentialAddress.geoLatStreet
    residentialAddressMap.geoLonStreet = residentialAddress.geoLonStreet
    residentialAddressMap.listing = residentialAddress.listing
    residentialAddressMap.postal = residentialAddress.postal
    residentialAddressMap.dpid = residentialAddress.dpid

    //remove nulls
    residentialAddressMap = residentialAddressMap.findAll { it.value != null}

    if(residentialAddressMap)
    	result.residentialAddress = residentialAddressMap
}


if (user?.dateOfBirth != null) {
	if(user?.dateOfBirth == '')
		result.dateOfBirth = user.dateOfBirth
	else {
		//convert date to timestamp
		result.dateOfBirth = new Date().parse('yyyy-MM-dd', user.dateOfBirth).time.toString()
	}
}

if (user?.landline != null)
	result.landline = user.landline
	
if (user?.avatar)
	result.avatar = user.avatar
		
if (user?.licenceId != null)
	result.licenceId = user.licenceId

if (user?.licenceState != null)
	result.licenceState = user.licenceState

if (user?.licenceCountry != null)
	result.licenceCountry = user.licenceCountry

if (user?.licenceExpiry != null) {
	if(user.licenceExpiry == '')
		result.licenceExpiry = user.licenceExpiry
	else
		//convert date to timestamp
		result.licenceExpiry = new Date().parse('yyyy-MM-dd', user.licenceExpiry).time.toString()
}

return new JsonBuilder(result).toString()