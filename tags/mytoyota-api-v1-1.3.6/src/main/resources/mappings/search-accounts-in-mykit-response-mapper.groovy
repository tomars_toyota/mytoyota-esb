import groovy.json.JsonSlurper

if (!payload) {
	throw new org.mule.module.apikit.exception.NotFoundException("Email or mobile number not found in myToyota")
}	

userAccountsList = []

new JsonSlurper().parseText(payload)?.each { userAccount ->

	userAccountMap = [:]
	userAccountMap.firstName = userAccount?.firstName
	userAccountMap.lastName = userAccount?.lastName	
	userAccountMap.myToyotaId = userAccount?.myToyotaID
	userAccountsList.add(userAccountMap)
}

if(!userAccountsList)
	throw new org.mule.module.apikit.exception.NotFoundException("Email or mobile number not found in myToyota")

return userAccountsList