import groovy.xml.MarkupBuilder

def writer = new StringWriter()
def xmlMarkup = new MarkupBuilder(writer)

//def rootEle =
xmlMarkup.mkp.xmlDeclaration(version: "1.0", encoding: "UTF-8")
xmlMarkup.'sp:retrieveGuestInformation' (
	'xmlns:sp' : "http://soap.sforce.com/schemas/class/SP_UserRegWSTransactionStructure"
	) {
		'sp:request'{
			if (flowVars['salesforceId']) {
				'sp:salesforceId' (flowVars['salesforceId'])
			} else if (flowVars['myToyotaId']) {
				'sp:myToyotaId' (flowVars['myToyotaId'])
			}
		}	
	}
return writer.toString()
