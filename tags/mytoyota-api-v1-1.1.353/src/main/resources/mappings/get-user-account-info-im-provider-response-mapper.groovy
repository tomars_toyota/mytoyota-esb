import static groovy.json.JsonOutput.*

def response = [:]
def data = [:]

response.login = payload?.loginIDs?.username ?:''
response.UID = payload?.UID ?:''
response.firstName = payload?.profile?.firstName ?:''
response.lastName = payload?.profile?.lastName ?:''
response.email = payload?.profile?.email ?:''
// Assumes one number for now, need to include check "type": "mobile"
response.mobile = payload?.profile?.phones?.number ?:''
data = payload?.data
response.data = data

return toJson(response)