package au.com.toyota.esb.mytoyota.api.mappings;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.io.IOException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

import javax.script.*;
import org.apache.commons.io.IOUtils;
import org.codehaus.jackson.map.ObjectMapper;
import org.junit.Test;
import org.mule.api.MuleMessage;
import org.mule.DefaultMuleMessage;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.mule.context.DefaultMuleContextFactory;
import org.mule.api.MuleContext;
import org.mule.api.transport.PropertyScope;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

public class GetUserAccountByEmailMyKitResponseMapperTest extends AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;
	private MuleContext muleContext;
	
	
	public GetUserAccountByEmailMyKitResponseMapperTest() throws ScriptException, SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/get-account-by-email-from-mykit-response-mapper.groovy");
	}
	
	@Test
	public void testRequestMapper() throws Exception {
		Object result = ScriptRunBuilder
				.runner(scriptRunner)
				.sessionVar("myToyotaId", "MYT-123")
				.payload(IOUtils.toString(getClass().getResourceAsStream(
						"/in/get-user-account-info-mykit-response.json")))
				.run();
		String resultJson = new ObjectMapper().writeValueAsString(result);
		System.out.println(resultJson);
		// expected
		String expectedResult = IOUtils.toString(getClass().getResourceAsStream(
				"/out/get-user-account-info-by-email-api-response.json"));
		JSONAssert.assertEquals(expectedResult, resultJson, true);
	}
	
//	@Test
//	public void testNoAccountFound() throws Exception {
//		try {
//			Object result = ScriptRunBuilder
//					.runner(scriptRunner)
//					.sessionVar("email", "six@sixtree.com.au")
//					.payload("")
//					.run();
//		} catch (Exception e) {
//			System.out.println("**** Exception: "+e.getCause());
//			assertTrue(e.getCause() instanceof javax.script.ScriptException);
//			assertEquals("org.mule.module.apikit.exception.NotFoundException: Email not found in myToyota", e.getCause().getMessage());
//		}
//	}
}
