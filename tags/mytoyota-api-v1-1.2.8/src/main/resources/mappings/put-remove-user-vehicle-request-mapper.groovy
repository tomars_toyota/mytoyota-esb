def vehicle = payload['vehicle']

def result = [:]

result.batchNumber = vehicle?.batchNumber ?: ''
result.compliancePlate = vehicle?.compliancePlate ?: ''
result.engineNumber = vehicle?.engineNumber ?: ''
result.vin = vehicle?.vin ?: ''
result.materialNumber = vehicle?.materialNumber ?: ''
result.vehicleDescription = vehicle?.vehicleDescription ?: ''
result.katashikiCode = vehicle?.katashikiCode ?: ''
result.transmission = vehicle?.transmission ?: ''
result.sellingDealerCode = vehicle?.sellingDealerCode ?: ''
result.sellingDealerName = vehicle?.sellingDealerName ?: ''
result.registrationNumber = vehicle?.registrationNumber ?: ''
result.production = vehicle?.production ?: ''
result.year = vehicle?.year ?: ''

//result.endDate = new Date().format('yyyy-MM-dd')
//result.comments = vehicle?.relationship?.comments ?: ''

return result