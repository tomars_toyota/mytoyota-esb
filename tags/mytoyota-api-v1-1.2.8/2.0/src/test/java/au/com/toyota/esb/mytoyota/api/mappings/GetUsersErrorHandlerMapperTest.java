package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;

import javax.script.ScriptException;
import javax.xml.namespace.QName;

import org.junit.Test;
import org.mule.DefaultMuleMessage;
import org.mule.api.ExceptionPayload;
import org.mule.api.MuleContext;
import org.mule.api.MuleMessage;
import org.mule.context.DefaultMuleContextFactory;
import org.mule.message.DefaultExceptionPayload;
import org.mule.module.ws.consumer.SoapFaultException;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;
import org.mule.tck.junit4.AbstractMuleContextTestCase;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;


public class GetUsersErrorHandlerMapperTest extends AbstractMuleContextTestCase {
	
	private ScriptRunner scriptRunner;
	private MuleContext muleContext;

	public GetUsersErrorHandlerMapperTest() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner
				.createScriptRunner("/mappings/get-users-error-handler.groovy");
	}

	@Test
	public void testMissingIdResponseMapper() throws Exception {
		
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload("")
				.flowVar("isMissingId", true)
				.run().toString();
		
		System.out.println(result);

		// expected
		String expectedResult = "{\"message\":\"Invalid request, missing user ID\"}";
		JSONAssert.assertEquals(expectedResult, result, true);
	}
	
	@Test
	public void testSfdcErrorResponseMapper() throws Exception {
		
				
		QName faultCode = new QName("Clinet");
		
		SoapFaultException soapFaultException = new SoapFaultException(null, faultCode, null, "Invalid salesforceId", null);
		
		ExceptionPayload exceptionPayload = new DefaultExceptionPayload(soapFaultException);
		
		muleContext = new DefaultMuleContextFactory().createMuleContext();
		MuleMessage message = new DefaultMuleMessage(null,muleContext);
		message.setExceptionPayload(exceptionPayload);
		
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload("")
				.flowVar("targetSystem", "sfdc")
				.message(message)
				.run().toString();
		
		System.out.println(result);

		// expected
		String expectedResult = "{\"message\":\"Failed to get required user\","
				+ "\"error\":{"
				+ "\"domain\":\"Salesforce\","
				+ "\"message\":\"Invalid salesforceId.The current MuleMessage is null!\"}}";
		JSONAssert.assertEquals(expectedResult, result, true);
	}
	
	@Test
	public void testDeafultErrorResponseMapper() throws Exception {
				
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload("")
				.flowVar("targetSystem", "anything")
				.run().toString();
		
		System.out.println(result);

		// expected
		String expectedResult = "{\"message\":\"Failed to get required user\"}";
		JSONAssert.assertEquals(expectedResult, result, true);
	}
}
