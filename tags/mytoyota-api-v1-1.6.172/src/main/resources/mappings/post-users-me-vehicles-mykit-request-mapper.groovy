import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper

if(!payload)
	throw new org.mule.module.apikit.exception.BadRequestException('Invalid request')
	
def vehicleRequest = new JsonSlurper().parseText(payload)
def vehicleMaster = new JsonSlurper().parseText(flowVars.vehicle)
def skipVerify = new Boolean(flowVars['skipVerify'])

// Build vehicle list
def vehicle = [:]
vehicle.batchNumber = vehicleRequest?.batchNumber
vehicle.compliancePlate = vehicleRequest?.compliancePlate
vehicle.engineNumber = vehicleRequest?.engineNumber
vehicle.vin = vehicleRequest?.vin
vehicle.materialNumber = (vehicleRequest != null)?vehicleRequest.materialNumber:vehicleMaster?.materialNumber
vehicle.deliveryDate = vehicleRequest?.deliveryDate
vehicle.vehicleDescription = vehicleRequest?.vehicleDescription
vehicle.katashikiCode = vehicleRequest?.katashikiCode
vehicle.transmission = vehicleRequest?.transmission
vehicle.sellingDealerCode = vehicleRequest?.sellingDealerCode
vehicle.sellingDealerName = vehicleRequest?.sellingDealerName
vehicle.registrationNumber = vehicleRequest?.registrationNumber
vehicle.production = vehicleRequest?.production


if(vehicleRequest?.suffix) {
    vehicle.suffixCode = vehicleRequest.suffix.code
    vehicle.suffixDescription = vehicleRequest.suffix.description
}
if(vehicleRequest?.trim) {
    vehicle.trimCode = vehicleRequest.trim.code
    vehicle.trimDescription = vehicleRequest.trim.description
}
if(vehicleRequest?.paint) {
    vehicle.paintCode = vehicleRequest.paint.code
    vehicle.paintDescription = vehicleRequest.paint.description
}

vehicle.salesType = vehicleRequest?.salesType	

//stock images
imagesList = []
vehicleRequest?.images.each { Map image ->
	imageMap = [:]
	imageMap.code = image.size
	imageMap.url = image.link
	//remove nulls
	imageMap = imageMap.findAll {it.value != null }

	if(imageMap)
		imagesList << imageMap
}

if(imagesList)
	vehicle.images = imagesList


//vehicle user
vehicleUserMap = [:]


//MYT-1101 default isVerified to true
// set to true regardless if there is no vehicleUser object
if (vehicle?.vin) {
	vehicleUserMap.isVerified = true
}

if(vehicleRequest?.vehicleUser) {
	vehicleUserMap.registrationNumber = vehicleRequest.vehicleUser.registrationNumber
	vehicleUserMap.state = vehicleRequest.vehicleUser.state

	
	//remove nulls
	vehicleUserMap = vehicleUserMap.findAll {it.value != null }
	
	
}

if(vehicle?.vin || vehicleRequest?.vehicleUser){
	if(skipVerify){
		// set as DRIVER by default
		vehicleUserMap.relationshipTypes = ["DRIVER"]
	} else {	
		// set as OWNER,DRIVER for backward compatibility
		vehicleUserMap.relationshipTypes = ["OWNER","DRIVER"]
	}
}


if(vehicleRequest.customImages)
	vehicleUserMap.vehicleImage = vehicleRequest.customImages[0].encodedImage


if(vehicleUserMap) {
	vehicle.vehicleUsers = []
	vehicle.vehicleUsers << vehicleUserMap
}

//remove nulls
vehicle = vehicle.findAll {it.value != null }
//merge the data from vehicle object received from SAP
vehicle = vehicleMaster + vehicle
if(!vehicle)
	throw new org.mule.module.apikit.exception.BadRequestException('Invalid request')

vehicle.skipVerify = skipVerify
return prettyPrint(toJson(vehicle))