import groovy.json.JsonSlurper
import static groovy.json.JsonOutput.*
import au.com.toyota.esb.mytoyota.api.util.GroovyHelper
			
if(flowVars['userAccount'])
	userAccount = new JsonSlurper().parseText(flowVars['userAccount']) 

vehicles = new JsonSlurper().parseText(payload)?.vehicles
activeVehicle = vehicles.find {!it.discontinuedDate}

accountSummary = [:]
accountSummary.firstName = userAccount?.firstName
accountSummary.lastName = userAccount?.lastName
accountSummary.vin = activeVehicle?.vin
accountSummary.vehicleDescription = activeVehicle?.vehicleDescription

accountSummary = GroovyHelper.removeNulls(accountSummary)
return prettyPrint(toJson(accountSummary))