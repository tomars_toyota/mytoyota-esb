import static groovy.json.JsonOutput.*
import java.text.SimpleDateFormat

def requestMap = [:]

serviceRequest = sessionVars['serviceRequest']

requestMap.My_Toyota_Id__c = flowVars['myToyotaId']
requestMap.title = serviceRequest?.contact_details?.title
requestMap.firstName = serviceRequest?.contact_details?.first_name
requestMap.lastName = serviceRequest?.contact_details?.last_name
requestMap.email = serviceRequest?.contact_details?.email
requestMap.mobilePhone = serviceRequest?.contact_details?.phone // Phone (integer?)

dealer = sessionVars['dealerResponse']
dealerSite = dealer?.sites[0]
requestMap.serviceRequestDealerName = dealer?.dealerName
requestMap.serviceBookingDealerAddress = dealerSite?.address?.streetAddress
requestMap.serviceBookingDealerEmail = dealerSite?.email?.service ?: dealerSite?.email?.main
requestMap.serviceBookingDealerPhoneNumber = dealerSite?.phone?.main // Phone (integer?)
requestMap.serviceBookingDealerPostCode = dealerSite?.address?.postCode // Integer
requestMap.serviceBookingDealerState = dealerSite?.address?.state
requestMap.serviceBookingDealerSuburb = dealerSite?.address?.suburb
requestMap.serviceBookingDealerWebsite = dealerSite?.webSite

requestMap.serviceRequestServiceDescription = serviceRequest?.service_desc
requestMap.serviceRequestVehicleDescription = serviceRequest?.vehicle_desc
requestMap.serviceRequestServiceCode = serviceRequest?.service_code
requestMap.serviceRequestVehicleVIN = serviceRequest?.vin
requestMap.serviceRequestVehicleRego = serviceRequest?.registration_number
requestMap.serviceRequestDropOffDate =  new SimpleDateFormat("YYYY-MM-dd").format(new Date().parse('E dd MMM yyyy', serviceRequest?.drop_off_date))
requestMap.serviceRequestDropOffTime = serviceRequest?.drop_off_time
requestMap.serviceRequestNotes = serviceRequest?.notes

request = ['input':[requestMap]]

return prettyPrint(toJson(request))