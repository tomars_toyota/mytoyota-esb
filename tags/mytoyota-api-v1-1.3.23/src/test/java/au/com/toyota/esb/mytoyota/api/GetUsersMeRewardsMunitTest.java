package au.com.toyota.esb.mytoyota.api;

import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.util.IOUtils;
import org.mule.util.StringUtils;
import org.skyscreamer.jsonassert.JSONAssert;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;

public class GetUsersMeRewardsMunitTest extends FunctionalMunitSuite {

	private static Properties adapterProperties = new Properties();
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(
				new String[] { 
						"get-users-me-rewards.xml", 
						"get-users-me-rewards-technical-adapter.xml", 
						"config.xml" },
				" ");
	}

	@Test
	public void testSuccessfulGetGuestRewards() throws Exception {

		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));
		
		String requestPath = "/api/myToyota/v1/users/me/rewards";
		final String userDetailsResponsePayload = IOUtils
				.toString(getClass().getResourceAsStream("/in/get-users-me-myKit-response.json"));
		final String userVehicleResponsePayload = IOUtils
				.toString(getClass().getResourceAsStream("/in/get-users-me-vehicles-mykit-response.json"));
		final Boolean addVehicleRequired = false;
		final Boolean isTMCA = false;
		final String scResponsePayload = IOUtils
				.toString(getClass().getResourceAsStream("/in/get-users-me-rewards-sitecore-response.json"));
		final String myKitResponsePayload = IOUtils
				.toString(getClass().getResourceAsStream("/in/get-users-me-rewards-mykit-response.json"));
		
		// stub myKit user response
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("My Toyota App API GET User Details"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							newMessage.setPayload(userDetailsResponsePayload);
						} catch (Exception e) {
							// catch exception statements
						}
						return newMessage;
					}
				});
		
		// stub myKit user vehicles
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("My Toyota App API GET User's Vehicles"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							newMessage.setPayload(userVehicleResponsePayload);
							newMessage.setInvocationProperty("scRewards", scResponsePayload);
							newMessage.setInvocationProperty("isTMCA", isTMCA);
						} catch (Exception e) {
							// catch exception statements
						}
						return newMessage;
					}
				});

		// stub SC response
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Get Rewards List from Sitecore"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
	
						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							newMessage.setPayload(scResponsePayload);
							newMessage.setInvocationProperty("isTMCA", isTMCA);
							newMessage.setInvocationProperty("addVehicleRequired", false);
						} catch (Exception e) {
							// catch exception statements
						}
						return newMessage;
					}
				});
		
		// stub myKit response
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("My Toyota App API GET User's Rewards"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							newMessage.setPayload(myKitResponsePayload);
							newMessage.setInvocationProperty("scRewards", scResponsePayload);
							newMessage.setInvocationProperty("isTMCA", isTMCA);
							newMessage.setInvocationProperty("addVehicleRequired", addVehicleRequired);
						} catch (Exception e) {
							// catch exception statements
						}
						return newMessage;
					}
				});

		MuleEvent event = testEvent(requestPath);
		MuleMessage message = muleMessageWithPayload(null);

		event.setMessage(message);

		Map<String, Object> props = new HashMap<String, Object>();
		props.put("onlyActive", "true");

		message.setProperty("http.query.params", props, PropertyScope.INBOUND);

		// Invoke the flow
		MuleEvent output = runFlow("get:/users/me/rewards:myToyota-config", event);

		System.out.println(output.getMessage().getPayload());

		// expected
		String expectedResult = IOUtils
				.toString(getClass().getResourceAsStream("/out/get-users-me-rewards-api-response.json"));

		JSONAssert.assertEquals(expectedResult, output.getMessage().getPayloadAsString(), true);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-users-me-rewards.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-users-me-rewards.response"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-users-me-rewards.technical-adapter.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-users-me-rewards.technical-adapter.response"))
				.times(1);
	}
	
	@Test
	public void testSuccessfulGetTMCARewards() throws Exception {

		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));
		
		String requestPath = "/api/myToyota/v1/users/me/rewards";
		final String userDetailsResponsePayload = IOUtils
				.toString(getClass().getResourceAsStream("/in/get-users-me-myKit-tmca-staff-response.json"));
		final Boolean addVehicleRequired = false;
		final Boolean isTMCA = true;
		final String scResponsePayload = IOUtils
				.toString(getClass().getResourceAsStream("/in/get-users-me-rewards-sitecore-response.json"));
		final String myKitResponsePayload = IOUtils
				.toString(getClass().getResourceAsStream("/in/get-users-me-rewards-mykit-response.json"));
		
		// stub myKit user response
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("My Toyota App API GET User Details"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							newMessage.setPayload(userDetailsResponsePayload);
						} catch (Exception e) {
							// catch exception statements
						}
						return newMessage;
					}
				});

		// stub SC response
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Get Rewards List from Sitecore"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
	
						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							newMessage.setPayload(scResponsePayload);
							newMessage.setInvocationProperty("isTMCA", isTMCA);
							newMessage.setInvocationProperty("addVehicleRequired", false);
						} catch (Exception e) {
							// catch exception statements
						}
						return newMessage;
					}
				});
		
		// stub myKit response
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("My Toyota App API GET User's Rewards"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							newMessage.setPayload(myKitResponsePayload);
							newMessage.setInvocationProperty("scRewards", scResponsePayload);
							newMessage.setInvocationProperty("isTMCA", isTMCA);
							newMessage.setInvocationProperty("addVehicleRequired", addVehicleRequired);
						} catch (Exception e) {
							// catch exception statements
						}
						return newMessage;
					}
				});

		MuleEvent event = testEvent(requestPath);
		MuleMessage message = muleMessageWithPayload(null);

		event.setMessage(message);

		Map<String, Object> props = new HashMap<String, Object>();
		props.put("onlyActive", "true");

		message.setProperty("http.query.params", props, PropertyScope.INBOUND);

		// Invoke the flow
		MuleEvent output = runFlow("get:/users/me/rewards:myToyota-config", event);

		System.out.println(output.getMessage().getPayload());

		// expected
		String expectedResult = IOUtils
				.toString(getClass().getResourceAsStream("/out/get-users-me-rewards-tmca-api-response.json"));

		JSONAssert.assertEquals(expectedResult, output.getMessage().getPayloadAsString(), true);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-users-me-rewards.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-users-me-rewards.response"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-users-me-rewards.technical-adapter.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-users-me-rewards.technical-adapter.response"))
				.times(1);
	}
	
	@Test
	public void testSuccessfulGetGuestRewardsButNoVehicle() throws Exception {

		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));
		
		String requestPath = "/api/myToyota/v1/users/me/rewards";
		final String userDetailsResponsePayload = IOUtils
				.toString(getClass().getResourceAsStream("/in/get-users-me-myKit-response.json"));
		final String userVehicleResponsePayload = IOUtils
				.toString(getClass().getResourceAsStream("/in/get-users-me-vehicles-mykit-response-account-summary-no-active-vehicles.json"));
		final Boolean isTMCA = false;
		final Boolean addVehicleRequired = true;
		final String scResponsePayload = IOUtils
				.toString(getClass().getResourceAsStream("/in/get-users-me-rewards-sitecore-response.json"));
		final String myKitResponsePayload = IOUtils
				.toString(getClass().getResourceAsStream("/in/get-users-me-rewards-mykit-response.json"));
		
		// stub myKit user response
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("My Toyota App API GET User Details"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							newMessage.setPayload(userDetailsResponsePayload);
						} catch (Exception e) {
							// catch exception statements
						}
						return newMessage;
					}
				});
		
		// stub myKit user vehicles
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("My Toyota App API GET User's Vehicles"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							newMessage.setPayload(userVehicleResponsePayload);
							newMessage.setInvocationProperty("isTMCA", isTMCA);
						} catch (Exception e) {
							// catch exception statements
						}
						return newMessage;
					}
				});
		
		// stub SC response
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Get Rewards List from Sitecore"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
	
						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							newMessage.setPayload(scResponsePayload);
							newMessage.setInvocationProperty("isTMCA", isTMCA);
							newMessage.setInvocationProperty("addVehicleRequired", false);
						} catch (Exception e) {
							// catch exception statements
						}
						return newMessage;
					}
				});
		
		// stub myKit response
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("My Toyota App API GET User's Rewards"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							newMessage.setPayload(myKitResponsePayload);
							newMessage.setInvocationProperty("scRewards", scResponsePayload);
							newMessage.setInvocationProperty("isTMCA", isTMCA);
							newMessage.setInvocationProperty("addVehicleRequired", addVehicleRequired);
						} catch (Exception e) {
							// catch exception statements
						}
						return newMessage;
					}
				});



		MuleEvent event = testEvent(requestPath);
		MuleMessage message = muleMessageWithPayload(null);

		event.setMessage(message);

		Map<String, Object> props = new HashMap<String, Object>();
		props.put("onlyActive", "true");

		message.setProperty("http.query.params", props, PropertyScope.INBOUND);

		// Invoke the flow
		MuleEvent output = runFlow("get:/users/me/rewards:myToyota-config", event);

		System.out.println(output.getMessage().getPayload());

		// expected
		String expectedResult = IOUtils
				.toString(getClass().getResourceAsStream("/out/get-users-me-rewards-require-add-vehicle-api-response.json"));

		JSONAssert.assertEquals(expectedResult, output.getMessage().getPayloadAsString(), true);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-users-me-rewards.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-users-me-rewards.response"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-users-me-rewards.technical-adapter.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-users-me-rewards.technical-adapter.response"))
				.times(1);
	}
}
