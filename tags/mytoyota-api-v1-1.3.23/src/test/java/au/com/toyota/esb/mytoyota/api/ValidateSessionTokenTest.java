package au.com.toyota.esb.mytoyota.api;

import static org.mule.munit.common.mocking.Attribute.attribute;

import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.util.IOUtils;
import org.mule.util.StringUtils;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;

public class ValidateSessionTokenTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"session-management-validate-token.xml",
				"session-management-im-provider-interactions.xml",
				"get-user-account-info-technical-adapter.xml",
				"config.xml"
		}, " ");
	}
	
	@Test
	public void testSuccessfulValidateSession() throws Exception {
		
		
		final String token = "eyJjdHkiOiJKV1QiLCJlbmMiOiJBMjU2R0NNIiwiYWxnIjoiZGlyIn0.."
				+ "BZIfgmYPXx4PM-bq.YbrfGIXdE0urI4fRf2XK4iOtfEhQUiLNZ7Vq4U6rSCHZnc"
				+ "cwcc7blVbvMCcVm6Qb6gzQfdx0MXX1vl0fJPBJ9X5NB-laSk18dBwl1NUhLsOWI"
				+ "TUN3ovUSCvbP_4WKWHeBRkvyHsSWWEnJAh3pB6DWyFNF7yCBlzHYu0NDjhCBrqZ8"
				+ "lzT7G7cLUZYaGbmRB96Go46GCxLegPj6iiTVk48sotpRoPKOgpzRtUiAvoQnDvCx"
				+ "wuWT2XAh2T0T6SmunJrV0ITAup3klEy9eI38SIj5YblQvNjE7fD_oV8203klX1QxE"
				+ "2ZyAL42d5v1oAgXTp2Vip7gv6xyNDIQujgpwsRyaAiXCS2IBCjcCzo7bU__qxcAYQ"
				+ "2L7fsWg.9ZrmZrtMYlWNeI-qAgLk6w";
		
		mockGetUserAccountInfoImRespons(token);
		
		MuleMessage message = muleMessageWithPayload(null);
		message.setProperty("http.request.path", "/api/myToyota/v1/users/me/services", PropertyScope.INBOUND);
		message.setProperty("APISessionToken", token, PropertyScope.INBOUND);
		
		MuleEvent event = testEvent("");
		event.setMessage(message);
		
		// Invoke the flow
		runFlow("session-management-validate-token-flow", event);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.session-management-validate-token-against-im-provider-subflow.response"))
				.times(1);	
	}
	
	@Test
	public void testSuccessfulInvalidateSession() throws Exception {
		try {
			
			final String invalidToken = "eyJjdHkiOiJKV1QiLCJlbmMiOiJBMjU2R0NNIiwiYWxnIjoiZGlyIn0.."
					+ "CKpSeDaG1HFSKsDb.46xiUqu1n7Hs96mxTLdqP4CnLpRHC_oH7iDVCuvZDahnyfQhMytxR"
					+ "g7HNSSZ5RUfTPjtGvGe6I4RTNpbagAk-YTKVx6CyW7JMDRNbgKtvF_fMrMkjHhezabiJEb"
					+ "vk-XddYMG69hNPWEuAdZXMvepk0Cq2k7rgnqGA24H8P1VOtwjx3DKI949xUwbj28swF-E3"
					+ "dDcWuep3_c_jPWxAdYw6vTAwl2SXmoKUqgvdXZlziaYphc02eZtWaMF6LmrW3iiPcRyuOcW"
					+ "84KZmAAoZ6EUGGG3aorAtqKlRMv9NZ3J7NmwPyqKV9CBqB_aPczpc1Nj5mfNuiiFgk9YTki"
					+ "JRP-MGGgC5To3Oz3M-CUQ_qb3FTEo6oIq3jMS7A.Xrth2PUPPzJCvSQbcecOjw";
			
			mockGetUserAccountInfoImRespons(invalidToken);
			
			MuleMessage message = muleMessageWithPayload(null);
			message.setProperty("http.request.path", "/api/myToyota/v1/users/me/services", PropertyScope.INBOUND);
			message.setProperty("APISessionToken", invalidToken, PropertyScope.INBOUND);
			
			MuleEvent event = testEvent("");
			event.setMessage(message);
			
			// Invoke the flow
			runFlow("session-management-validate-token-flow", event);
			assert(false);
		} catch (Exception e) {
			assert (e.getCause().getCause() instanceof au.com.toyota.esb.mytoyota.api.exception.UnauthorisedException);
		}
	}
	
	private void mockGetUserAccountInfoImRespons(final String token) throws Exception {
		// Mock get-user-account-info-im-technical-adapter flow-ref
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("IM API Get User"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							String responsePayload = IOUtils.toString(
									getClass().getResourceAsStream("/in/get-user-account-info-im-response.json"));
							newMessage.setPayload(responsePayload);
							newMessage.setProperty("token", token, PropertyScope.INVOCATION);
						} catch (Exception e) {
							// catch exception statements
						}
						return newMessage;
					}
				});
	}
}
