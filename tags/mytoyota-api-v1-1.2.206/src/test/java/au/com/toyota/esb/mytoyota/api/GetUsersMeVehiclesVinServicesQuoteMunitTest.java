package au.com.toyota.esb.mytoyota.api;

import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.HashMap;
import java.util.Map;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;


//@Ignore
public class GetUsersMeVehiclesVinServicesQuoteMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"get-users-me-vehicles-vin-services-quote.xml",
				"get-users-me-vehicles-vin-services-quote-technical-adapter.xml",
				"get-vehicle-outstanding-services-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulRequest() throws Exception {
		
		whenMessageProcessor("outbound-endpoint").ofNamespace("https")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("GET Sitecore v2 Service Quote"))
			.thenApply(new MockResponseTransformer() {
			@Override
			public MuleMessage transform(MuleMessage originalMessage) {
				MuleMessage newMessage = null;
				
				try {
					newMessage = originalMessage.createInboundMessage();
					
					newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
					
					newMessage.setPayload(getClass().getResourceAsStream("/in/get-users-me-vehicles-vin-services-quote-sitecore-response.json"));
				} catch (Exception e) {
					// catch exception statements
				}
				
				return newMessage;
			}
		});
		
		
			whenMessageProcessor("request").ofNamespace("http")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("Vehicle API GET Outstanding Services"))
			.thenApply(new MockResponseTransformer() {
			@Override
			public MuleMessage transform(MuleMessage originalMessage) {
				MuleMessage newMessage = null;
				
				try {
					newMessage = originalMessage.createInboundMessage();
					
					newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
					
					newMessage.setPayload(getClass().getResourceAsStream("/in/get-users-me-vehicles-vin-sap-outstanding-response.json"));
				} catch (Exception e) {
					// catch exception statements
				}
				
				return newMessage;
			}
		});

		MuleEvent event = testEvent("");
		event.setFlowVariable("vin", "MR0EB3CB200442617");
		
		Map<String, Object> props = new HashMap<String,Object>();
		props.put("dealerId", "37272");
        props.put("branchCode", "37272");
        props.put("date", "2016-10-20");
        
		MuleMessage msg = muleMessageWithPayload(null);
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
		msg.setProperty("http.request.uri",
				"users/me/vehicles/MR0EB3CB200442617/services/quote?dealerId=37272&branchCode=37272",
				PropertyScope.INBOUND);
		
		event.setMessage(msg);
		

		MuleEvent output = runFlow("get:/users/me/vehicles/{vin}/services/quote:myToyota-config", event);

		System.out.println(output.getMessage().getPayloadAsString());
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-users-me-vehicles-vin-services-quote.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-users-me-vehicles-vin-services-quote.response"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-users-me-vehicles-vin-services-quote-technical-adapter.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-users-me-vehicles-vin-services-quote-technical-adapter.response"))
				.times(1);
		
		String expectedResponse = IOUtils.toString(
				getClass().getResourceAsStream("/out/get-users-me-vehicles-vin-services-quote-api-response.json"));
		
		assertJsonEquals(expectedResponse, output.getMessage().getPayloadAsString());
	}
	 
	@Test
	public void testFailuresStatusCodes() throws Exception {

		int[] statusCodes = new int[] { 404, 405, 415, 406, 401, 500 };
		
		String requestPath = "/users/me/vehicles/{vin}/services/quote";
		
		MuleEvent event = testEvent(requestPath);
		
		event.setFlowVariable("vin", "MR0EB3CB200442617");
		
		Map<String, Object> props = new HashMap<String,Object>();
		props.put("dealerId", "37272");
        props.put("branchCode", "37272");
        props.put("date", "2016-10-20");
        
		MuleMessage msg = muleMessageWithPayload(null);
		msg.setProperty("http.query.params", props, PropertyScope.INBOUND);
		
		event.setMessage(msg);

		for (int i = 0; i < statusCodes.length; i++) {
			
			final int currentStatusCode = statusCodes[i];
			
			whenMessageProcessor("outbound-endpoint").ofNamespace("https").withAttributes(
					attribute("name").ofNamespace("doc").withValue("GET Sitecore v2 Service Quote"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
	
						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", currentStatusCode, PropertyScope.INBOUND);
						} catch (Exception e) {
							// // catch exception statements
						}
	
						return newMessage;
					}
				});
			
			new StatusCodesTestingHelper().runFailureTest(currentStatusCode, null,
					"get:/users/me/vehicles/{vin}/services/quote:myToyota-config", event);
		}
	}
}
