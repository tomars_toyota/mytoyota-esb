package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

import javax.script.*;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;
import static org.junit.Assert.assertTrue;

public class PutUsersMeVehiclesVinMyKitRequestMapperTest extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;

	public PutUsersMeVehiclesVinMyKitRequestMapperTest() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/put-users-me-vehicles-vin-request-mapper.groovy");
	}

	@Test
	public void testSuccessfulRequestMapper() throws Exception {
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(IOUtils.toString(getClass().getResourceAsStream("/in/put-users-me-vehicles-vin-api-request.json")))
				.flowVar("dummyName", "dummyValue")
				.run().toString();

		System.out.println(result);
		
		// expected
		String expectedResult = IOUtils.toString(getClass()
				.getResourceAsStream(
						"/out/put-users-me-vehicles-vin-mykit-request.json"));
		//System.out.println(expectedResult);
		JSONAssert.assertEquals(expectedResult, result, true);
	}
	
	@Test
	public void testEmptyRequestMapper() throws Exception {
		try {
			ScriptRunBuilder.runner(scriptRunner)
				.payload("{}")
				.flowVar("dummyName", "dummyValue")
				.run().toString();

		} catch (Exception e) {
			if (e.getMessage().contains("Invalid request"))
				assertTrue(true);
			else
				assertTrue(false);
			return;
		}
		assertTrue(false);
	}
}
