// Pre-mapper to ensure:
// - don't pass through firstname or lastname if provided from SF blank (mandatory myKIT fields)
// - don't pass through email, as change of email is not currently supported

import groovy.json.JsonSlurper
import groovy.json.JsonBuilder

def payloadMap = new JsonSlurper().parseText(payload)

if (!(payloadMap?.firstName))
    payloadMap.remove('firstName')
    
if (!(payloadMap?.lastName))
    payloadMap.remove('lastName')
    
if (payloadMap?.email || payloadMap?.email?.length() == 0)
    payloadMap.remove('email')

return new JsonBuilder(payloadMap).toString()