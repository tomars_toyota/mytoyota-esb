import groovy.xml.MarkupBuilder

def writer = new StringWriter()
def xmlMarkup = new MarkupBuilder(writer)

//def rootEle =
xmlMarkup.mkp.xmlDeclaration(version: "1.0", encoding: "UTF-8")
xmlMarkup.'sp:retrieveGuestInformation' (
	'xmlns:sp' : "http://soap.sforce.com/schemas/class/SP_UserRegWSTransactionStructure"
	) {
		'sp:request'{
			'sp:salesforceId' (flowVars['salesforceId'])
		}	
	}
return writer.toString()