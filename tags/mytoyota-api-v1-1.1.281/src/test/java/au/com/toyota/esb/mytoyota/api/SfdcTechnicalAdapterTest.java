package au.com.toyota.esb.mytoyota.api;

import static org.mule.munit.common.mocking.Attribute.attribute;

import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.util.IOUtils;
import org.mule.util.StringUtils;


public class SfdcTechnicalAdapterTest extends FunctionalMunitSuite {

	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"create-salesforce-sessionid-soap-header.xml",
				"sfdc-technical-adapter.xml",
				"config.xml"
		}, " ");
	}
	
	@Test
	public void testCreateSfdcSessionIdCaching() throws Exception {
		//Mock sfdc login response
		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Partner Operation login"))
				.thenReturn(muleMessageWithPayload(
						IOUtils.toString(getClass().getResourceAsStream("/in/sfdc-login-response.xml"))));
				
		MuleEvent event = testEvent("");
		event.setFlowVariable("sfdcSoapApiType", "sp");
		
		// Invoke the flow 1st time
		runFlow("create-salesforce-sessionid-soap-header-flow", event);
		
		
		// Invoke the flow 2nd time
		runFlow("create-salesforce-sessionid-soap-header-flow", event);
		
		verifyLoggerCall("esb.mytoyota-api-v1.get-salesforce-sessionid-subflow.request", 2);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.get-salesforce-sessionid-subflow.request.partner.login.no-cached-session-found"))
				.atMost(1);
		verifyLoggerCall("esb.mytoyota-api-v1.get-salesforce-sessionid-subflow.response", 2);
	}
	
	@Test
	public void testSuccessfulSfdcCall() throws Exception {

		//Mock sfdc call flow response
		whenMessageProcessor("flow-ref").withAttributes(
			attribute("name").ofNamespace("doc").withValue("call-sfdc-op"))
			.thenReturn(muleMessageWithPayload(""));
		
		MuleEvent event = testEvent("");
		
		// Invoke the flow
		runFlow("call-sfdc", event);
		
		verifyLoggerCall("esb.mytoyota-api-v1.sfdc-technical-adapter.request", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.sfdc-technical-adapter.response", 1);
	}
	
	@Test
	public void testSuccessfulSfdcCallAfterInvalidSessionIdExc() throws Exception {

		/*
		 * normal flow call to sfdc is meant to fail with an invalid session ID
		 * exception, then the error handler call to work after re-login
		 */
		
		//Mock sfdc call flow response
		whenMessageProcessor("flow-ref").withAttributes(
			attribute("name").ofNamespace("doc").withValue("call-sfdc-op"))
			.thenThrow(new Exception("INVALID_SESSION_ID"));
		
		//Mock sfdc login response
		whenMessageProcessor("consumer").ofNamespace("ws")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Partner Operation login"))
				.thenReturn(muleMessageWithPayload(
						IOUtils.toString(getClass().getResourceAsStream("/in/sfdc-login-response.xml"))));

		//Mock sfdc call flow response
		whenMessageProcessor("flow-ref").withAttributes(
			attribute("name").ofNamespace("doc").withValue("call-sfdc-op-after-exc"))
			.thenReturn(muleMessageWithPayload(""));
		
		
		MuleEvent event = testEvent("");
		
		// Invoke the flow
		runFlow("call-sfdc", event);
		
		verifyLoggerCall("esb.mytoyota-api-v1.sfdc-technical-adapter.request", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.sfdc-technical-adapter.response", 0);
		
		verifyLoggerCall("esb.mytoyota-api-v1.get-salesforce-sessionid-subflow.request", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.get-salesforce-sessionid-subflow.request.partner.login.no-cached-session-found", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.get-salesforce-sessionid-subflow.response", 1);
		
		verifyLoggerCall("esb.mytoyota-api-v1.sfdc-technical-adapter.invalid-session-handler.request", 1);
		verifyLoggerCall("esb.mytoyota-api-v1.sfdc-technical-adapter.invalid-session-handler.response", 1);
	}
	
	@Test
	public void testAllSuccessfulSfdcOpsCalls() throws Exception {

		String[] ops = new String[] { "updateOwner", "updateOwnerPreferences", "removeAVehicle", "checkCMUserAndVehicle", "registerGuestNONRDR",
				"addAVehicle" };
		
		for (int i = 0; i < ops.length; i++) {
			//Mock sfdc call flow response
			whenMessageProcessor("consumer").ofNamespace("ws")
					.withAttributes(attribute("name").ofNamespace("doc").withValue("Invoke Salesforce Operation "+ ops[i]))
					.thenReturn(muleMessageWithPayload(""));
			
			MuleEvent event = testEvent("");
			event.setFlowVariable("sfdcOp", ops[i]);
			
			// Invoke the flow
			runFlow("call-sfdc", event);
		}
		
		verifyLoggerCall("esb.mytoyota-api-v1.sfdc-technical-adapter.request", ops.length);
		verifyLoggerCall("esb.mytoyota-api-v1.sfdc-technical-adapter.response", ops.length);
	}
	
	private void verifyLoggerCall (String category, int expectedNumberOfCalls) {
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue(category)).times(expectedNumberOfCalls);
	}
}
