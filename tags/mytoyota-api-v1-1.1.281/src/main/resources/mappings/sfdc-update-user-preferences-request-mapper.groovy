import groovy.xml.MarkupBuilder
import groovy.json.JsonSlurper
import java.text.SimpleDateFormat

def writer = new StringWriter()
def xmlMarkup = new MarkupBuilder(writer)


if(payload instanceof org.mule.transport.NullPayload)
	payload = null

def user = payload

//def rootEle =
xmlMarkup.mkp.xmlDeclaration(version: "1.0", encoding: "UTF-8")
xmlMarkup.'sp:updateOwnerPreferences' (
	'xmlns:sp' : "http://soap.sforce.com/schemas/class/SP_UserRegWSTransactionStructure"
	) {
		'sp:request'{
			
			// Map preferences
			if(user?.trackMyCarEmailOptIn != null ||
			   user?.goPlacesPreference  != null ||
			   user?.prospectNurtureOptIn  != null ||
			   user?.trackMyCarSMSOptIn  != null) {
			   	'sp:guestPreferences' {
				   	if(user?.trackMyCarEmailOptIn != null)
						'sp:emailOptInTMC' (user.trackMyCarEmailOptIn)
					if(user?.goPlacesPreference != null && (
						user?.goPlacesPreference == "Soft Copy Only" || 
						user?.goPlacesPreference == "Hard Copy Only" || 
						user?.goPlacesPreference == "Both Hard And Soft Copy" || 
						user?.goPlacesPreference == "Neither")) {
						'sp:goPlacesPreference' (user.goPlacesPreference)
					}
					if(user?.prospectNurtureOptIn != null)
						'sp:pnpOptIn' (user.prospectNurtureOptIn)
					if(user?.trackMyCarSMSOptIn != null)
						'sp:smsOptInTMC' (user.trackMyCarSMSOptIn)
				}
			}
			
			// isMarketingOptIn = used in the User Registration process
			// globalUnsubscribe = used in the User Preferences update (true = unsubscribe)
			// They both map to the marketingPreference field, however only one
			// should be received at any given time in myToyota API
			if(user?.isMarketingOptIn != null)
				'sp:marketingPreference' (user.isMarketingOptIn)
			if(user?.globalUnsubscribe != null)
				'sp:marketingPreference' (user.globalUnsubscribe)
			
			if(sessionVars['myToyotaId'])
				'sp:myToyotaId' (sessionVars['myToyotaId'])
				
			if( user?.email != null     ||
				user?.firstName != null ||
				user?.lastName != null) {
				'sp:ownerDetails'{
					if(user?.email != null)
						'sp:email' (user.email)
					if(user?.firstName != null)
						'sp:firstName' (user.firstName)
					if(user?.lastName != null)
						'sp:lastName' (user.lastName)
				}
			}
		}	
	}
//println '__________ payload to salesforce: ' + writer.toString()
return writer.toString()
