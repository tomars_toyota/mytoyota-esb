import groovy.xml.MarkupBuilder
import groovy.json.JsonSlurper
import java.text.SimpleDateFormat

def writer = new StringWriter()
def xmlMarkup = new MarkupBuilder(writer)

if(payload instanceof org.mule.transport.NullPayload)
	payload = null

//def rootEle =
xmlMarkup.mkp.xmlDeclaration(version: "1.0", encoding: "UTF-8")
xmlMarkup.'ent:create' (
	'xmlns:ent' : "urn:enterprise.soap.sforce.com",
	'xmlns:urn1' : "urn:sobject.enterprise.soap.sforce.com",
	'xmlns:xsi' : "http://www.w3.org/2001/XMLSchema-instance"
	) {
		'ent:sObjects' ('xsi:type' : "urn1:Case") {
			'Origin' ("Email")
			'Status' ("Awaiting Dealer")
			if(sessionVars['myToyotaId']) {
				'Subject' ("myToyota Vehicle Addition Test Case ["+sessionVars['myToyotaId']+"]")
			}
			else {
				'Subject' ("myToyota Vehicle Addition Test Case")
			}
		}	
	}
println '__________ payload to salesforce: ' + writer.toString()
return writer.toString()

//       <urn:create>
//          <!--Zero or more repetitions:-->
//          <urn:sObjects xsi:type="urn1:Case">
//             <!--Zero or more repetitions:-->
//             <Origin>Email</Origin>
//             <Status>Awaiting Dealer</Status>
//             <Subject>My Toyota Test Case [James Garner]</Subject> 
//          </urn:sObjects>
//       </urn:create>