package au.com.toyota.esb.mytoyota.api;

import static org.junit.Assert.*;
import static org.mule.munit.common.mocking.Attribute.attribute;
import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Before;
import org.junit.Test;
import org.mule.api.ExceptionPayload;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.message.DefaultExceptionPayload;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.skyscreamer.jsonassert.JSONAssert;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;

//@Ignore
public class PostSessionTLinkCreateVerifyTokenMunitTest extends FunctionalMunitSuite {

	private static Properties adapterProperties = new Properties();
	private static String login = "james.a.garner+10@gmail.com";
	private static String sessionToken;
	private static HashMap<String, Object> apiSessionToken;
	private static String UID = "04707fa24bc5400cbf7cd3db2ecbf94a";
	private static String myToyotaId = "MYT-0000063c";

	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] { "post-create-user-session-tlink.xml",
				"post-create-user-session-tlink-technical-adapter.xml", "get-user-account-info-technical-adapter.xml",
				"session-management-im-provider-interactions.xml",
				"post-session-tlink-verify-token-technical-adapter.xml", "post-session-tlink-verify-token.xml",
				"config.xml" }, " ");
	}
	
	@Before
	public void initSessionToken() throws Exception {
		// Mock get user profile "IM API Get User" response
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("IM API Get User"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							newMessage.setPayload(IOUtils.toString(getClass().getResourceAsStream(
									"/in/post-user-session-tlink-create-get-user-profile-im-provider-response.json")));
						} catch (Exception e) {
						}
						return newMessage;
					}
				});

		// Mock store tlink session token "IM API Update User" response
		String imUpdateUserResponse = IOUtils.toString(getClass()
				.getResourceAsStream("/in/post-user-session-login-add-session-token-im-provider-response.json"));
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("IM API Update User"))
				.thenReturn(muleMessageWithPayload(imUpdateUserResponse));

		// Populate the flowVars and Outbound vars that result from successful
		// processing of the APISessionToken
		MuleEvent event = testEvent("");
		event.setFlowVariable("UID", UID);
		event.setFlowVariable("myToyotaId", myToyotaId);
		MuleMessage msg = muleMessageWithPayload(null);
		msg.setOutboundProperty("x-mytoyota-id", myToyotaId);
		msg.setOutboundProperty("x-user-id", UID);
		event.setMessage(msg);

		// Invoke the flow
		MuleEvent output = runFlow("post:/sessions/tlink:myToyota-config", event);
		MuleMessage responseMessage = output.getMessage();
		apiSessionToken = responseMessage.getInvocationProperty("APISessionToken");
		
		System.out.println("**** Create session token 'value': " + apiSessionToken.get("value"));
		System.out.println("**** Create session token 'tokenType': " + apiSessionToken.get("tokenType"));

		// Set sessionToken for next validation unit test
		sessionToken = apiSessionToken.get("value").toString();
	}

	@Test
	public void testSuccessfulTLinkTokenCreation() throws Exception {

		// High level flow request
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Log API Request")).times(1);
		// Technical adapter & IM Update User Details
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Log Request")).times(3);
		// Technical adapter & IM Update User Details
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Log Response")).times(3);
		// High level flow response
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Log API Response")).times(1);

		// Verify created token
		assert (apiSessionToken.get("value").toString().length() > 0);
		assert (apiSessionToken.get("tokenType").toString().equals("tlinkSessionToken"));

		// Set sessionToken for next validation unit test
		sessionToken = apiSessionToken.get("value").toString();
		assertNotNull(sessionToken);
		
	}

	@Test
	public void testSuccessfulTLinkTokenValidationValidResult() throws Exception {

		// requires static sessionToken to be set from previous unit test
		assertNotNull(sessionToken);

		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));

		// Mock get user profile "IM API Get User" response
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("IM API Get User"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						try {
							newMessage = originalMessage.createInboundMessage();
							// Manually re-set vars lost in unit testing during
							// HTTP call
							newMessage.setProperty("token", sessionToken, PropertyScope.SESSION);
							newMessage.setProperty("login", login, PropertyScope.SESSION);

							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							newMessage.setPayload(
									"{\"login\": \"james.a.garner+10@gmail.com\", \"UID\": \"04707fa24bc5400cbf7cd3db2ecbf94a\", \"data\": {\"tlinkSessionToken\": {\"id\": \""
											+ sessionToken + "\"} } }");
							// newMessage.setPayload(IOUtils.toString(getClass().getResourceAsStream("/in/post-user-session-tlink-create-get-user-profile-im-provider-response.json")));
						} catch (Exception e) {
						}
						return newMessage;
					}
				});

		// Had trouble with flowVar values being lost only in unit test
		// execution therefore mocking this groovy to returned expected values
		// for a 'valid=true' scenario
		whenMessageProcessor("component").ofNamespace("scripting")
				.withAttributes(
						attribute("name").ofNamespace("doc").withValue("Compare passed token against stored token"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						try {
							newMessage = originalMessage.createInboundMessage();
							// Manually re-set vars lost in unit testing during
							// HTTP call
							newMessage.setInvocationProperty("valid", true);
							newMessage.setInvocationProperty("token", sessionToken);
							newMessage.setInvocationProperty("login", login);
							newMessage.setPayload("{\"login\": \"" + login
									+ "\", \"UID\": \"04707fa24bc5400cbf7cd3db2ecbf94a\", \"data\": {\"tlinkSessionToken\": {\"id\": \""
									+ sessionToken + "\"} } }");
						} catch (Exception e) {
						}
						return newMessage;
					}
				});

		MuleEvent event = testEvent("");
		MuleMessage msg = muleMessageWithPayload(null);
		Map<String, Object> httpHeaders = new HashMap<String, Object>();
		httpHeaders.put("tlinkSessionToken", sessionToken);
		httpHeaders.put("login", login);
		msg.setProperty("http.headers", httpHeaders, PropertyScope.INBOUND);
		event.setMessage(msg);

		// Invoke the flow
		@SuppressWarnings("unused")
		MuleEvent output = runFlow("post:/sessions/tlink/verify:myToyota-config", event);
		MuleMessage responseMessage = output.getMessage();

		// High level flow request
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Log API Request")).atLeast(1);
		// Technical adapter & IM Update User Details
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Log Request")).atLeast(3);
		// Technical adapter & IM Update User Details
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Log Response")).atLeast(3);
		// High level flow response
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Log API Response")).atLeast(1);

		String expectedResponse = "{\"valid\":true,\"login\":\"" + login + "\",\"token\":\"" + sessionToken + "\"}";
		System.out.println("**** Result: " + responseMessage.getPayloadAsString());

		JSONAssert.assertEquals(expectedResponse, responseMessage.getPayloadAsString(), true);
	}

	@Test
	public void testSuccessfulTLinkTokenValidationInvalidResult() throws Exception {

		// requires static sessionToken to be set from previous unit test
		assertNotNull(sessionToken);

		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));

		// Mock get user profile "IM API Get User" response
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("IM API Get User"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						try {
							newMessage = originalMessage.createInboundMessage();
							// Manually re-set vars lost in unit testing during
							// HTTP call
							newMessage.setProperty("token", sessionToken, PropertyScope.SESSION);
							newMessage.setProperty("login", login, PropertyScope.SESSION);

							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							newMessage.setPayload(
									"{\"login\": \"james.a.garner+10@gmail.com\", \"UID\": \"04707fa24bc5400cbf7cd3db2ecbf94a\", \"data\": {\"tlinkSessionToken\": {\"id\": \""
											+ sessionToken + "\"} } }");
							// newMessage.setPayload(IOUtils.toString(getClass().getResourceAsStream("/in/post-user-session-tlink-create-get-user-profile-im-provider-response.json")));
						} catch (Exception e) {
						}
						return newMessage;
					}
				});

		// Had trouble with flowVar values being lost only in unit test
		// execution therefore mocking this groovy to returned expected values
		// for a 'valid=true' scenario
		whenMessageProcessor("component").ofNamespace("scripting")
				.withAttributes(
						attribute("name").ofNamespace("doc").withValue("Compare passed token against stored token"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						try {
							newMessage = originalMessage.createInboundMessage();
							// Manually re-set vars lost in unit testing during
							// HTTP call
							newMessage.setInvocationProperty("valid", false);
							newMessage.setInvocationProperty("token", sessionToken);
							newMessage.setInvocationProperty("login", login);

							newMessage.setPayload("{\"login\": \"" + login
									+ "\", \"UID\": \"04707fa24bc5400cbf7cd3db2ecbf94a\", \"data\": {\"tlinkSessionToken\": {\"id\": \""
									+ sessionToken + "\"} } }");
						} catch (Exception e) {
						}
						return newMessage;
					}
				});

		MuleEvent event = testEvent("");
		MuleMessage msg = muleMessageWithPayload(null);
		Map<String, Object> httpHeaders = new HashMap<String, Object>();
		httpHeaders.put("tlinkSessionToken", sessionToken);
		httpHeaders.put("login", login);
		msg.setProperty("http.headers", httpHeaders, PropertyScope.INBOUND);
		event.setMessage(msg);

		// Invoke the flow
		@SuppressWarnings("unused")
		MuleEvent output = runFlow("post:/sessions/tlink/verify:myToyota-config", event);
		MuleMessage responseMessage = output.getMessage();

		// High level flow request
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Log API Request")).atLeast(1);
		// Technical adapter & IM Update User Details
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Log Request")).atLeast(3);
		// Technical adapter & IM Update User Details
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Log Response")).atLeast(3);
		// High level flow response
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Log API Response")).atLeast(1);

		String expectedResponse = "{\"valid\":false,\"login\":\"" + login + "\",\"token\":\"" + sessionToken + "\"}";
		System.out.println("**** Result: " + responseMessage.getPayloadAsString());

		JSONAssert.assertEquals(expectedResponse, responseMessage.getPayloadAsString(), true);
	}

	@Test
	public void testUnsuccessfulTLinkTokenValidation() throws Exception {

		final String invalidToken = "eyJjdHkiOiJKV1QiLCJlbmMiOiJBMjU2R0NNIiwiYWxnIjoiZGlyIn0";

		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));

		// Ideally this mock would return a BadRequestException as well as
		// setting the flowVars
		whenMessageProcessor("component").ofNamespace("scripting")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Process Session Token"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setInvocationProperty("token", invalidToken);
							newMessage.setInvocationProperty("login", login);
						} catch (Exception e) {
						}
						return newMessage;
					}
				});

		// Throw an exception to simulate the token being processed
		// unsuccessfully. Ideally this could be rolled into the above mock
		// rather than this workaround approach
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("IM API Get User"))
				.thenThrow(new org.mule.module.apikit.exception.BadRequestException("Invalid JWE Header"));

		MuleEvent event = testEvent("");
		// event.setFlowVariable("token", sessionToken);
		// event.setFlowVariable("login", login);
		MuleMessage msg = muleMessageWithPayload(null);
		Map<String, Object> httpHeaders = new HashMap<String, Object>();
		httpHeaders.put("tlinkSessionToken", sessionToken);
		httpHeaders.put("login", login);
		msg.setProperty("http.headers", httpHeaders, PropertyScope.INBOUND);
		event.setMessage(msg);

		// Invoke the flow
		@SuppressWarnings("unused")
		MuleEvent output = runFlow("post:/sessions/tlink/verify:myToyota-config", event);
		MuleMessage responseMessage = output.getMessage();

		// High level flow request
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Log API Request")).atLeast(1);
		// Technical adapter & IM Update User Details
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Log Request")).atLeast(3);
		// Technical adapter & IM Update User Details
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Log Response")).atLeast(1);
		// High level flow response
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Log API Response")).atLeast(1);

		String expectedResponse = "{\"valid\":false,\"login\":\"" + login + "\",\"token\":\"" + invalidToken + "\"}";
		System.out.println("**** Result: " + responseMessage.getPayloadAsString());

		JSONAssert.assertEquals(expectedResponse, responseMessage.getPayloadAsString(), true);
	}

	@Test
	public void testFailuresStatusCodes() throws Exception {

		int[] statusCodes = new int[] { 404, 405, 415, 406, 401, 500 };
		MuleEvent event = testEvent("");
		Map<String, Object> props = new HashMap<String, Object>();
		MuleMessage requestMessage = muleMessageWithPayload("");
		event.setMessage(requestMessage);

		for (int i = 0; i < statusCodes.length; i++) {
			new StatusCodesTestingHelper().runFailureTest(statusCodes[i], "IM API Get User",
					"post:/sessions/tlink:myToyota-config", event);
		}
	}
}
