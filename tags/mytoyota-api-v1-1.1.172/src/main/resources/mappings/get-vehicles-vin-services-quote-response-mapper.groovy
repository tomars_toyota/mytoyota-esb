import org.mule.api.transport.PropertyScope
import static groovy.json.JsonOutput.*

params = message.getProperty("http.query.params", PropertyScope.INBOUND)

response = [:]
response.dealerId = params['dealerId']
response.branchCode = params['branchCode']
response.kilometres = params['kilometres']
response.serviceQuoteLink =  pdfServiceUrl+flowVars['vin']+'/'+params['kilometres']+'/'+params['dealerId']+'/'+params['branchCode']

return prettyPrint(toJson(response))