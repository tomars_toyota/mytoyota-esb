import groovy.json.JsonSlurper
import static groovy.json.JsonOutput.*

def removeNulls(col) {
    if (col instanceof List) 
        return col.findAll { it }.collect { removeNulls(it) }
    else if (col instanceof Map) 
        return col.findAll { it.value }.collectEntries { [(it.key): removeNulls(it.value) ] }
    else return col
}

//println '___________ payload: '+payload
def myKitResponse = new JsonSlurper().parseText(payload)

def response = [:]

response.bookingId = myKitResponse.bookingId
response.dropOffDate = myKitResponse.dropOffDate
response.dropOffTime = myKitResponse.dropOffTime
response.pickUpDate = myKitResponse.pickUpDate
response.pickUpTime = myKitResponse.pickUpTime 
response.status = myKitResponse.status
response.notes = myKitResponse.notes
response.alertType = myKitResponse.confirmationType
response.totalServicePrice = myKitResponse.totalServicePrice
response.waitAtDealerFlag = myKitResponse.waitAtDealerFlag
response.expressServiceFlag = myKitResponse.expressServiceFlag
response.courtesyBusFlag = myKitResponse.courtesyBusFlag
response.loanVehicleFlag = myKitResponse.loanVehicleFlag

response.dealerDetails = myKitResponse.dealer

//replace dmsId and branchCode with dmsID and branchId
response.dealerDetails.dmsId = myKitResponse.dealer.dmsID
response.dealerDetails.branchCode = myKitResponse.dealer.branchId
response.dealerDetails.remove('dmsID')
response.dealerDetails.remove('branchId')



response.contactDetail = myKitResponse.contactDetail
response.vehicle = myKitResponse.vehicle
response.serviceOperation = myKitResponse.serviceOperation


return prettyPrint(toJson(removeNulls(response)))
