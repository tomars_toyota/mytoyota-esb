package au.com.toyota.esb.mytoyota.api;

import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.mule.munit.common.mocking.Attribute.attribute;

import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.util.IOUtils;
import org.mule.util.StringUtils;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;


/*
 * Munit test to unit test the functionality to get user's T&C agreed version 
 * GET: /users/me/tc
 * @author: ahuwait
 */
public class GetUsersMeTcMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"get-users-me-tc.xml",
				"get-users-me-tc-technical-adapter.xml",
				"get-user-account-info-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulGetTc() throws Exception {
		
		whenMessageProcessor("request").ofNamespace("http")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("IM API Get User"))
			.thenApply(new MockResponseTransformer() {
				@Override
				public MuleMessage transform(MuleMessage originalMessage) {
					MuleMessage newMessage = null;
					
					try {
						newMessage = originalMessage.createInboundMessage();
						
						newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
						newMessage.setProperty("Content-Type", "application/json;charset=UTF-8", PropertyScope.INBOUND);
						
						String imResponse = IOUtils.toString(getClass().getResourceAsStream("/in/get-users-me-tc-im-response.json"));
						
						newMessage.setPayload(IOUtils.toInputStream(imResponse, "UTF-8"));
					} catch (Exception e) {
						// catch exception statements
					}
					
					return newMessage;
				}
			});
		
		
		MuleEvent event = testEvent("");
		event.setFlowVariable("UID", "6159e1e3566c42b28d96c48208c66059");
		event.setMessage(muleMessageWithPayload(null));		
		
		// Invoke the flow
		MuleEvent output = runFlow("get:/users/me/tc:myToyota-config", event);
		//System.out.println("_____________________"+output.getMessage().getPayloadAsString());
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
			.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-users-me-tc.request"))
			.times(1);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
			.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-users-me-tc.response"))
			.times(1);	

		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
			.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-users-me-tc-technical-adapter.request"))
			.times(1);	
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
			.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-users-me-tc-technical-adapter.response"))
			.times(1);	

		
		
		String expectedResponse = IOUtils.toString(getClass().getResourceAsStream("/out/get-users-me-tc-response.json"));
		assertJsonEquals(expectedResponse, output.getMessage().getPayloadAsString());

	}
	
	@Test
	public void testFailuresStatusCodes() throws Exception {
		
		int[] statusCodes = new int[] { 404, 405, 415, 406, 401, 500 };	
		
		MuleEvent event = testEvent("");
		event.setFlowVariable("UID", "6159e1e3566c42b28d96c48208c66059");
		event.setMessage(muleMessageWithPayload(null));
		
		for (int i = 0; i < statusCodes.length; i++) {
			new StatusCodesTestingHelper().runFailureTest(statusCodes[i], "IM API Get User",
					"get:/users/me/tc:myToyota-config", event);
		}
	}
}
