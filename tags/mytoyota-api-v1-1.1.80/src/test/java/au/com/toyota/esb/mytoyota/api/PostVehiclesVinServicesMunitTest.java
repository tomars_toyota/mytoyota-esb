package au.com.toyota.esb.mytoyota.api;

import static org.mule.munit.common.mocking.Attribute.attribute;

import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.util.IOUtils;
import org.mule.util.StringUtils;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;

public class PostVehiclesVinServicesMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"post-vehicles-vin-services.xml",
				"post-vehicles-vin-services-myKit-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulCreateServiceBooking() throws Exception {
		
		whenMessageProcessor("request").ofNamespace("http").withAttributes(
				attribute("name").ofNamespace("doc").withValue("MyKit API: POST /users/{myToyotaId}/services"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;
						
						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("Content-Type", "application/json;charset=UTF-8", PropertyScope.INBOUND);
							newMessage.setProperty("http.status", 201, PropertyScope.INBOUND);
							newMessage.setPayload("");
						} catch (Exception e) {
							// catch exception statements
						}
						
						return newMessage;
					}
				});
		
		
		MuleEvent event = testEvent("");
		event.setFlowVariable("vin", "6T153BK360X070769");
		
		
		MuleMessage msg = muleMessageWithPayload(IOUtils.toString(getClass()
				.getResourceAsStream("/in/post-vehicles-vin-services-request.json")));
		
        event.setMessage(msg);		
		
		// Invoke the flow
		runFlow("post:/vehicles/{vin}/services:myToyota-config", event);
		
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.post-vehciles-vin-service.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.post-vehciles-vin-service.response"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.post-vehicles-vin-services.mykit.createbooking.request"))
				.times(1);

		verifyCallOfMessageProcessor("log-message").ofNamespace("esbutils")
				.withAttributes(attribute("category").withValue(
						"esb.mytoyota-api-v1.post-vehicles-vin-services.mykit.createbooking.response"))
				.times(1);
	}
	
	@Test
	public void testFailuresStatusCodes() throws Exception {

		int[] statusCodes = new int[] { 404, 405, 415, 406, 401, 500 };

		for (int i = 0; i < statusCodes.length; i++) {
			MuleEvent event = testEvent("");
			event.setFlowVariable("vin", "6T153BK360X070769");
			
			MuleMessage msg = muleMessageWithPayload(IOUtils.toString(getClass()
					.getResourceAsStream("/in/post-vehicles-vin-services-request.json")));
			
	        event.setMessage(msg);
	        
			new StatusCodesTestingHelper().runFailureTest(statusCodes[i], "MyKit API: POST /users/{myToyotaId}/services",
					"post:/vehicles/{vin}/services:myToyota-config", event);
		}
	}
}
