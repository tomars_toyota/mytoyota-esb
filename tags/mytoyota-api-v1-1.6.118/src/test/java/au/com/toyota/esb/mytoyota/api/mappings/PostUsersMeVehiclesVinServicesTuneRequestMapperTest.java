package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

import javax.script.*;
import org.junit.Assert;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.xml.sax.SAXException;

public class PostUsersMeVehiclesVinServicesTuneRequestMapperTest extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;

	public PostUsersMeVehiclesVinServicesTuneRequestMapperTest() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner
				.createScriptRunner("/mappings/post-users-me-vehicles-vin-services-tune-request-mapper.groovy");
	}
	
	@Test
	public void testResponseMapperWithUpsells() throws Exception {
		String expectedResult = "SiteID=61.10169.10169011116&Rego=CA13HJ&Vehid=295795&Service=TOY9983-S0000CVI-1%7CSCHEDULE%7Cfalse%7C150.3%7C0D1D33%7CADVASA44R++++++1003"
				+ "&BookDate=2457510&BookTime=27000&OutDate=2457510&OutTime=46800&ConfType=&ConfTo=dealer%40toyota.com.au&SrvPrc=250"
				+ "&UpSells=0x0000000007ae1441%2C345.00%2C0x0000000007ae1488%2C319.00%2C0x0000000007ae1489%2C0&Notes=Please+check+the+wipers&Wait=false&CourtesyBus=false&Loan=true"
				+ "&FrnCID=MYT-000001f6&CustType=B&BusinessName=My+Company&FirstName=John&LastName=Smith&Phone=0412341234&Email=jsmith%40email.com";
		
		testResponseMapperImpl(IOUtils.toString(getClass().getResourceAsStream("/in/post-users-me-vehicles-vin-services-request.json")), expectedResult);
	}
	
	@Test
	public void testResponseMapperWithoutUpsells() throws Exception {
		String expectedResult = "SiteID=61.10169.10169011116&Rego=CA13HJ&Vehid=295795&Service=TOY9983-S0000CVI-1%7CSCHEDULE%7Cfalse%7C150%7C0D1D33%7CADVASA44R++++++1003"
				+ "&BookDate=2457510&BookTime=27000&OutDate=2457510&OutTime=46800&ConfType=&ConfTo=dealer%40toyota.com.au&SrvPrc=250"
				+ "&Notes=Please+check+the+wipers&Wait=false&CourtesyBus=false&Loan=true&FrnCID=MYT-000001f6&CustType=P";
		
		testResponseMapperImpl(IOUtils.toString(getClass().getResourceAsStream("/in/post-users-me-vehicles-vin-services-request-no-upsells.json")), expectedResult);
	}
	
	private void testResponseMapperImpl(String payload, String expectedResult) throws Exception {
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload(payload)
				.flowVar("dmsID","61.10169.10169011116")
				.flowVar("vin","JTMBFREV80D078597")
				.flowVar("myToyotaId","MYT-000001f6")
				.run().toString();

		System.out.println(result);

		Assert.assertEquals(expectedResult, result);
	}
}
