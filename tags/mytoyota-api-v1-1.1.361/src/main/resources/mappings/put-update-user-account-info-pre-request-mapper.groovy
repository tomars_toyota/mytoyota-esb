// Pre-mapper to ensure:
// - don't pass through firstname or lastname if provided from SF blank (mandatory myKIT fields)
// - don't pass through email, as change of email is not currently supported
// - if an address is passed without a reference ID, then set the refID to 'other' because
//   the old address' reference ID stays in myKIT causing the front-ends to lookup the address based on this ID.

import groovy.json.JsonSlurper
import groovy.json.JsonBuilder

def payloadMap = new JsonSlurper().parseText(payload)

// Don't pass through firstname if provided from SF blank (mandatory myKIT fields)
if (!(payloadMap?.firstName))
    payloadMap.remove('firstName')

// Don't pass through lastname if provided from SF blank (mandatory myKIT fields)
if (!(payloadMap?.lastName))
    payloadMap.remove('lastName')

// Don't pass through email, as change of email is not currently supported
if (payloadMap?.email || payloadMap?.email?.length() == 0)
    payloadMap.remove('email')

// If an address (or part of address) is received without a totalCheckId, then set it to 'other' because
//  the old address' referenceId stays in myKIT causing the front-ends to lookup the old address based on this ID.
if ((
	(payloadMap?.address?.postal?.streetAddress || payloadMap?.address?.postal?.streetAddress?.length() == 0) ||
	(payloadMap?.address?.postal?.suburb        || payloadMap?.address?.postal?.suburb?.length() == 0) ||
	(payloadMap?.address?.postal?.state         || payloadMap?.address?.postal?.state?.length() == 0) ||
	(payloadMap?.address?.postal?.postcode      || payloadMap?.address?.postal?.postcode?.length() == 0) ||
	(payloadMap?.address?.postal?.country       || payloadMap?.address?.postal?.country?.length() == 0)) && 
	(payloadMap?.address?.postal?.totalCheckId == null)) {
	 	payloadMap.address.postal['totalCheckId'] = 'other'
	 }

return new JsonBuilder(payloadMap).toString()
