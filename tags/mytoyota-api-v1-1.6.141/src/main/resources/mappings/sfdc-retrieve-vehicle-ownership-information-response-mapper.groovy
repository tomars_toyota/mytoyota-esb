import groovy.json.JsonOutput
import org.slf4j.Logger
import org.slf4j.LoggerFactory

Logger logger = LoggerFactory.getLogger("esb.mytoyota-api-v1.sfdc-retrieve-vehicle-ownership-information-response-mapper.script");

if (payload == null || payload == org.mule.transport.NullPayload) {
    logger.info("payload is null" + payload)
    return payload
}

def sfdcResult = null
try {
    sfdcResult = new groovy.util.XmlSlurper().parseText(payload).result    
}
catch (Exception e) {
    logger.info("parse exception. payload is: " + payload)
    return org.mule.transport.NullPayload
}


response = [:]
vehicleOwnerships = []

sfdcResult?.vehicleOwnerships?.vehicleOwnership?.each { vo ->
    voMap = [:]
    voMap['batchNumber'] = vo?.batchNumber?.text()
    voMap['inactivationDate'] = vo?.inactivationDate?.text()
    voMap['rdrDate'] = vo?.rdrDate?.text()
    voMap['registrationNumber'] = vo?.registrationNumber?.text()
    voMap['state'] = vo?.state?.text()
    voMap['vin'] = vo?.vin?.text()
    vehicleOwnerships.add(voMap)
}

// Map response if a vehicle ownership existed, otherwise a Null Payload
if (vehicleOwnerships.size() > 0) {
	response['vehicleOwnerships'] = vehicleOwnerships
	return JsonOutput.toJson(response)
} else {
	return org.mule.transport.NullPayload
}

//       <retrieveVehicleOwnershipInformationResponse xmlns="http://soap.sforce.com/schemas/class/SP_UserRegWSTransactionStructure" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
//          <result>
//             <cmUserId xsi:nil="true"/>
//             <guestPreferences xsi:nil="true"/>
//             <isActiveCMuser xsi:nil="true"/>
//             <isVehicleCMEquipped xsi:nil="true"/>
//             <marketingPreference xsi:nil="true"/>
//             <myToyotaId xsi:nil="true"/>
//             <ownerDetails xsi:nil="true"/>
//             <responseMessage>SPMyToyota002; All Vehicle Ownership Record(s) Sent Successfully.</responseMessage>
//             <resultStatus>SUCCESS</resultStatus>
//             <salesforceId xsi:nil="true"/>
//             <vehicleOwnerships>
//                <vehicleOwnership>
//                   <batchNumber xsi:nil="true"/>
//                   <inactivationDate xsi:nil="true"/>
//                   <rdrDate xsi:nil="true"/>
//                   <registrationNumber>1EMC526</registrationNumber>
//                   <state>VIC</state>
//                   <vin>5TDBK3FH80S020611</vin>
//                </vehicleOwnership>
//                <vehicleOwnership>
//                   <batchNumber>YCT31E</batchNumber>
//                   <inactivationDate>2017-03-15</inactivationDate>
//                   <rdrDate>2017-02-07</rdrDate>
//                   <registrationNumber>YCT31E</registrationNumber>
//                   <state>VIC</state>
//                   <vin>JTNKU3JE30J084720</vin>
//                </vehicleOwnership>
//             </vehicleOwnerships>
//          </result>
//       </retrieveVehicleOwnershipInformationResponse>