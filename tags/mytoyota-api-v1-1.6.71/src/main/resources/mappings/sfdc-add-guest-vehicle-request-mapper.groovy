import groovy.xml.MarkupBuilder

def writer = new StringWriter()
def xmlMarkup = new MarkupBuilder(writer)


def vehicleRequest = payload

//validate request
if(!(vehicleRequest.containsKey('vehicleUser') &&
		vehicleRequest.vehicleUser.containsKey('registrationNumber') &&
		vehicleRequest.vehicleUser.containsKey('state') &&
		vehicleRequest.containsKey('vin')
	))
	throw new Exception('Invalid request')


//def rootEle =
xmlMarkup.mkp.xmlDeclaration(version: "1.0", encoding: "UTF-8")
xmlMarkup.'sp:addGuestVehicle' (
	'xmlns:sp' : "http://soap.sforce.com/schemas/class/GuestRegVehicleWebserviceCalls"
	) {
		'sp:request'{
			'sp:myToyotaId' (flowVars['myToyotaId'])
			'sp:guestType' (flowVars['relationshipType'])
			'sp:vehicleDetails'{
				'sp:regoNo' (vehicleRequest.vehicleUser.registrationNumber)
				'sp:regoState' (vehicleRequest.vehicleUser.state)
				'sp:vin' (vehicleRequest.vin)	
			}
			
		}	
	}

//println '__________ payload to salesforce: ' + writer.toString()
return writer.toString()



// <sp:request>
// 	<sp:myToyotaId>?</sp:myToyotaId>
//  <sp:guestType>?</sp:guestType>
// 	<sp:vehicleDetails>
//          <!--Optional:-->
//          <sp:batchNumber>?</sp:batchNumber>
//          <!--Optional:-->
//          <sp:rdrDate>?</sp:rdrDate>
//          <!--Optional:-->
//          <sp:regoNo>?</sp:regoNo>
//          <!--Optional:-->
//          <sp:regoState>?</sp:regoState>
//         	<!--Optional:-->
//          <sp:trackMyCar>
//         	<!--Optional:-->
//         	<sp:batchId>?</sp:batchId>
//          <!--Optional:-->
//         	<sp:cosi>?</sp:cosi>
//        	<!--Optional:-->
//         	<sp:statusDescription>?</sp:statusDescription>
//      	</sp:trackMyCar>
//      	<!--Optional:-->
//      <sp:vin>?</sp:vin>
//  </sp:vehicleDetails>
// </sp:request>