import groovy.json.JsonSlurper
import groovy.json.JsonOutput
import groovy.time.TimeCategory

vehicle = new JsonSlurper().parseText(payload)
if(vehicle?.deliveryDate)
    use (TimeCategory) {
        warrantyEndDate = new Date().parse('yyyy-MM-dd', vehicle.deliveryDate) + 3.year
        if(warrantyEndDate)
            vehicle.warrantyEndDate = warrantyEndDate.format("yyyy-MM-dd")
    }
return JsonOutput.prettyPrint(JsonOutput.toJson(vehicle))