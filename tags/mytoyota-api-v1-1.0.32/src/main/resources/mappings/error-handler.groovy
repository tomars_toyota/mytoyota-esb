import org.mule.api.transport.PropertyScope
import org.mule.module.apikit.exception.*
import au.com.toyota.esb.mytoyota.api.exception.UnauthorisedException

status = message.getProperty("http.status", PropertyScope.INBOUND)

if(status in [200, 201])
	return payload

reason = message.getProperty("http.reason", PropertyScope.INBOUND)
if(!reason)
	reason = ""
flowVars['originalPayload'] = payload

println '_______status: ' + status

switch(status) {
	case 404:
		throw new NotFoundException(reason)
		break
	case 405:
		throw new MethodNotAllowedException(null, null)
		break
	case 415:
		throw new UnsupportedMediaTypeException()
		break
	case 406:
		throw new NotAcceptableException()
		break
	case 400:
		throw new BadRequestException(reason)
		break
	case 401:
		throw new UnauthorisedException(reason)
		break
	default:
		throw new Exception((String)reason)
}