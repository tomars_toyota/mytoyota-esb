package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.Properties;

import java.util.HashMap;

import javax.script.ScriptException;

import org.mule.DefaultMuleMessage;
import org.mule.api.MuleContext;
import org.mule.api.MuleEventContext;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.context.DefaultMuleContextFactory;

import org.apache.commons.io.IOUtils;
import org.custommonkey.xmlunit.XMLTestCase;
import org.junit.Test;
import org.xml.sax.SAXException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

public class SessionTokensCreateMapperTest extends XMLTestCase {

	private ScriptRunner createTokenScriptRunner;
	private ScriptRunner verifyTokenScriptRunner;
	
	private MuleContext muleContext;
	private Properties props;
	
	private static HashMap<String, Object> sessionToken;

	private final String myToyotaId = "MYT-0000063c";
	private final String UID = "04707fa24bc5400cbf7cd3db2ecbf94a";
	
	public SessionTokensCreateMapperTest() throws ScriptException, SAXException, IOException {
		createTokenScriptRunner = ScriptRunner.createScriptRunner("/mappings/jwt-token-create.groovy");
		verifyTokenScriptRunner = ScriptRunner.createScriptRunner("/mappings/jwt-token-validate.groovy");
		
		//load properties
		props = new Properties();
		props.load(getClass().getResourceAsStream("/mytoyota-api-v1-constants.properties"));
				
	}
	
	/*
	 * This unit test was split in two however the 'verify' relied on the 
	 * 'create' executing first but there's no guarantee with JUnit that 
	 * tests will be executed in order.
	 * */
	@Test
	public void testCreateAndVerifyToken() throws Exception {
		
		muleContext = new DefaultMuleContextFactory().createMuleContext();
		MuleMessage message = new DefaultMuleMessage(null,muleContext);
		
		String secretKey = props.getProperty("jwt.secret.key");
		String issuer = props.getProperty("jwt.issuer");
		
		HashMap<String, Object> createFlowVars = new HashMap<String, Object>();
		createFlowVars.put("myToyotaId", myToyotaId);
		createFlowVars.put("UID", UID);
		
		String createdToken = ScriptRunBuilder
				.runner(createTokenScriptRunner)
				.payload("")
				.flowVars(createFlowVars)
				.binding("tokenType", "tlinkSessionToken")
				.binding("secretKey", secretKey)
				.binding("issuer", issuer)
				.message(message)
				.run().toString();
		
		sessionToken = (HashMap<String, Object>) createFlowVars.get("APISessionToken");
		
		assertNotNull( sessionToken );
		assert( sessionToken.get("value").toString().length() > 0);
		assert( sessionToken.get("tokenType").toString().equals("tlinkSessionToken"));
		
		System.out.println("**** Session Token 'Value': " + sessionToken.get("value"));
		System.out.println("**** Session Token 'tokenType': " + sessionToken.get("tokenType"));
		
		System.out.println("**** Now verifying the token ****");
		
//		assertNotNull( sessionToken );
//		assertNotNull( sessionToken.get("value") );
//		assertNotNull( sessionToken.get("tokenType") );
		
		muleContext = new DefaultMuleContextFactory().createMuleContext();
		message = new DefaultMuleMessage(null,muleContext);
		message.setProperty("tlinkSessionToken", sessionToken.get("value"), PropertyScope.INBOUND);
		
		HashMap<String, Object> verifyFlowVars = new HashMap<String, Object>();
		verifyFlowVars.put("retrieveToken", "false");
		
		String verifiedToken = ScriptRunBuilder
				.runner(verifyTokenScriptRunner)
				.payload("")
				.flowVars(verifyFlowVars)
				.binding("jwtSecretKey", secretKey)
				.binding("jwtIssuer", issuer)
				.binding("tokenType", sessionToken.get("tokenType") )
				.message(message)
				.run().toString();
		
		assert( verifyFlowVars.get("validToken").equals("true") );
		assert( verifyFlowVars.get("myToyotaId").equals(myToyotaId) );
		assert( verifyFlowVars.get("UID").equals(UID) );
	}
}
