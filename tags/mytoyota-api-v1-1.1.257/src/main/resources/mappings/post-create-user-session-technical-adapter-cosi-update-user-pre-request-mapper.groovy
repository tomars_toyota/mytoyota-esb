// Pre-mapper to:
// - Ensure agreed fields on JIRA MYT-619 are mapped through
// - Apply very simplistic title case transformation to firstname, lastname

import groovy.json.JsonSlurper
import groovy.json.JsonBuilder

def capitaliseNames = { name ->
    newName = ''
    name.tokenize().each { newName += it.toLowerCase().capitalize() + ' '}
    return newName.trim()
}

def returnEmptyStringIfNull = { map, field ->
    if (map?.containsKey(field) && map[field] == null)
        return ""
    else
        return map[field]
}

def payloadMap = new JsonSlurper().parseText(payload)
def request = [:]

if (payloadMap?.title)
    request['title'] = capitaliseNames(payloadMap?.title)

if (payloadMap?.firstName)
    request['firstName'] = capitaliseNames(payloadMap?.firstName)

if (payloadMap?.lastName)
    request['lastName'] = capitaliseNames(payloadMap?.lastName)

if (payloadMap?.mobile)
    request['mobile'] = payloadMap?.mobile

if (payloadMap?.landline)
    request['landline'] = payloadMap?.landline

if (payloadMap?.dateOfBirth)
    request['dateOfBirth'] = payloadMap?.dateOfBirth

if (payloadMap?.address?.postal) {
	def address = [:]
	def postal = [:]
    // nulls will be ignored in myKIT however as per MYT-619 the null
    // values from the COSI should be empty strings so they are cleared in myKIT
	address['streetAddress'] = returnEmptyStringIfNull(payloadMap?.address?.postal, "streetAddress")
    address['suburb'] = returnEmptyStringIfNull(payloadMap?.address?.postal, "suburb")
    address['postcode'] = returnEmptyStringIfNull(payloadMap?.address?.postal, "postcode")
    address['state'] = returnEmptyStringIfNull(payloadMap?.address?.postal, "state")
    // If an address (or part of address) is received without a totalCheckId, then set it to 'other' because
    //  the old address' referenceId stays in myKIT causing the front-ends to lookup the old address based on this ID.
    if (payloadMap?.address?.postal?.totalCheckId == null) {
        address['totalCheckId'] = "other"
    }
    
    postal['postal'] = address
    
    request['address'] = postal
}

// Enrich the update users data with COSI details (required for the mapping to Salesforce)
if (payloadMap?.guestOrder) {
    def guestOrders = [:]
    def status = [:]
    def vehicleDetails = [:]

    status['smoothedStatus'] = payloadMap?.guestOrder?.status?.smoothedStatus
    vehicleDetails['batchId'] = payloadMap?.guestOrder?.vehicleDetails?.batchId

    guestOrders['cosi'] = payloadMap?.guestOrder?.cosi
    guestOrders['vehicleDetails'] = vehicleDetails
    guestOrders['status'] = status

    request['guestOrders'] = guestOrders
}

return new JsonBuilder(request).toString()
