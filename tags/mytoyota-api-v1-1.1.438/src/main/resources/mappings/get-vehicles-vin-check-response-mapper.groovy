import static groovy.json.JsonOutput.*

validationResponse = [:]
validationResponse.vin = flowVars['vin']
	
if(flowVars['isValidVin'] == false)
	validationResponse.isValid = false
else 
	validationResponse.isValid = true

return prettyPrint(toJson(validationResponse))