import groovy.xml.MarkupBuilder
import groovy.json.JsonSlurper

def writer = new StringWriter()
def xmlMarkup = new MarkupBuilder(writer)

def user = payload

xmlMarkup.mkp.xmlDeclaration(version: "1.0", encoding: "UTF-8")
xmlMarkup.'sp:registerGuestNONRDR' (
	'xmlns:sp' : "http://soap.sforce.com/schemas/class/SP_UserRegWSTransactionStructure"
	) {
		'sp:request'{
			if(user.isMarketingOptIn != null)
				'sp:marketingPreference' (user.isMarketingOptIn)
			'sp:myToyotaId' (sessionVars['myToyotaId'])
			'sp:ownerDetails'{
				'sp:email' (user.email)
				'sp:firstName' (user.firstName)
				'sp:lastName' (user.lastName)
				if(sessionVars['encryptedPassword'])
					'sp:Password' (sessionVars['encryptedPassword'])
			}
			'sp:OwnersPortalUserId' (user.login)
			if(user.vin != null)
				'sp:VIN' (user.vin)
		}	
	}
//println '__________ payload to salesforce: ' + writer.toString()
return writer.toString()

// ** TARGET PAYLOAD **
// request
// 	marketingPreference
// 	myToyotaId
// 	ownerDetails
// 		email
// 		firstName
// 		lastName 
// 		Password
// 	OwnersPortalUserId
// 	VIN