package au.com.toyota.esb.mytoyota.api;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Assert;
import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.skyscreamer.jsonassert.JSONAssert;





public class GetLinksReferralMunitTest extends FunctionalMunitSuite {
	
	private static Properties adapterProperties = new Properties();
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"get-links-referral-technical-adapter.xml",
				"get-links-referral.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulRequest() throws Exception {
		
		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));
		

		String urlParams = "source=myService&refId=123";
		String requestPath = "/api/myToyota/v1/links/referral" + urlParams;

		MuleEvent event = testEvent(requestPath);
		event.getMessage().setProperty("http.query.string", urlParams, PropertyScope.INBOUND);
		Map<String, String> params = new HashMap<String, String>();
		params.put("source", "myService");
		params.put("refId", "123");
		event.getMessage().setProperty("http.query.params", params, PropertyScope.INBOUND);
	
		MuleEvent output = runFlow("get:/links/referral:myToyota-config", event);

		String response = output.getMessage().getPayloadAsString();
		
		System.out.println(response);
		
		final String expected = IOUtils
				.toString(getClass().getResourceAsStream("/out/get-links-referral-response.json"));
		
		JSONAssert.assertEquals(expected, response, true);
		
	}
	
	@Test
	public void testRequiredParameters() throws Exception {
		
		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));
		

		String urlParams = "";
		String requestPath = "/api/myToyota/v1/links/referral" + urlParams;

		MuleEvent event = testEvent(requestPath);
		event.getMessage().setProperty("http.query.string", urlParams, PropertyScope.INBOUND);
	
		

		try {
			MuleEvent output = runFlow("get:/links/referral:myToyota-config", event);
		
			String response = output.getMessage().getPayloadAsString();
			
			Assert.assertTrue("Should throw an exception", false);
		}catch (Exception e) {
			Assert.assertTrue("Should throw an exception", true);
		}
		
		
		
	}
	
	
}
