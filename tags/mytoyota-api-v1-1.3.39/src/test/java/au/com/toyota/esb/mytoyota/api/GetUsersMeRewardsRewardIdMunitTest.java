package au.com.toyota.esb.mytoyota.api;

import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;
import org.mule.util.IOUtils;
import org.mule.util.StringUtils;
import org.skyscreamer.jsonassert.JSONAssert;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;

public class GetUsersMeRewardsRewardIdMunitTest extends FunctionalMunitSuite {

	private static Properties adapterProperties = new Properties();
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(
				new String[] { 
						"get-users-me-rewads-rewardId.xml", 
						"get-users-me-rewads-rewardId-technical-adapter.xml", 
						"myService-get-access-token.xml",
						"reward-get-guest-coupons.xml",
						"config.xml" },
				" ");
	}

	@Test
	public void testSuccessfulGetFeatures() throws Exception {

		adapterProperties.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));
		
		String requestPath = "/api/myToyota/v1/users/me/rewards/{rewardId}";
		
		// stub SC response
//		whenMessageProcessor("outbound-endpoint").ofNamespace("https")
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Get Reward Details from Sitecore"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							newMessage = originalMessage.createInboundMessage();
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
							String scResponsePayload = IOUtils.toString(getClass()
									.getResourceAsStream("/in/get-users-me-rewards-details-sitecore-response.json"));
							newMessage.setPayload(scResponsePayload);
							newMessage.setInvocationProperty("sitecoreHost", adapterProperties.getProperty("sitecore-app.host"));
						} catch (Exception e) {
							// catch exception statements
						}
						return newMessage;
					}
				});

		

		MuleEvent event = testEvent(requestPath);
		MuleMessage message = muleMessageWithPayload(null);

		event.setMessage(message);

		Map<String, Object> props = new HashMap<String, Object>();
		props.put("rewardType", "competition");

		message.setProperty("http.query.params", props, PropertyScope.INBOUND);

		// Invoke the flow
		MuleEvent output = runFlow("get:/users/me/rewards/{rewardId}:myToyota-config", event);

		System.out.println(output.getMessage().getPayload());

		// expected
		String expectedResult = IOUtils
				.toString(getClass().getResourceAsStream("/out/get-users-me-rewards-rewardId-api-response.json"));

		JSONAssert.assertEquals(expectedResult, output.getMessage().getPayloadAsString(), true);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.get-users-me-rewards-rewardId.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(
						attribute("category").withValue("esb.mytoyota-api-v1.get-users-me-rewards-rewardId.response"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category")
						.withValue("esb.mytoyota-api-v1.get-users-me-rewards-rewardId.technical-adapter.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category")
						.withValue("esb.mytoyota-api-v1.get-users-me-rewards-rewardId.technical-adapter.response"))
				.times(1);
	}
}
