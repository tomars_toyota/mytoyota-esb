package au.com.toyota.esb.mytoyota.api;

import static net.javacrumbs.jsonunit.JsonAssert.assertJsonEquals;
import static org.mule.munit.common.mocking.Attribute.attribute;

import java.util.HashMap;

import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.junit.Ignore;
import org.junit.Test;
import org.mule.api.MuleEvent;
import org.mule.api.MuleMessage;
import org.mule.api.transport.PropertyScope;
import org.mule.munit.runner.functional.FunctionalMunitSuite;

import au.com.toyota.esb.mytoyota.api.munit.MockResponseTransformer;
import au.com.toyota.esb.mytoyota.api.munit.StatusCodesTestingHelper;


//@Ignore
public class GetUsersMeVehiclesMunitTest extends FunctionalMunitSuite {
	
	@Override
	protected String getConfigResources() {
		return StringUtils.join(new String[] {
				"get-users-me-vehicles.xml",
				"get-users-me-vehicles-technical-adapter.xml",
				"get-vehicle-details-technical-adapter.xml",
				"config.xml"
		}, " ");
	}

	@Test
	public void testSuccessfulRequest() throws Exception {

		whenMessageProcessor("request").ofNamespace("http")
			.withAttributes(attribute("name").ofNamespace("doc").withValue("My Toyota App API GET Retrieve Users Vehicles"))
			.thenApply(new MockResponseTransformer() {
			@Override
			public MuleMessage transform(MuleMessage originalMessage) {
				MuleMessage newMessage = null;
				
				try {
					newMessage = originalMessage.createInboundMessage();
					
					newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);
					
					newMessage.setPayload(getClass().getResourceAsStream("/in/get-users-me-vehicles-mykit-response.json"));
				} catch (Exception e) {
					// catch exception statements
				}
				
				return newMessage;
			}
		});
		
		whenMessageProcessor("request").ofNamespace("http")
				.withAttributes(attribute("name").ofNamespace("doc").withValue("Vehicle API GET vehicle by vin"))
				.thenApply(new MockResponseTransformer() {
					@Override
					public MuleMessage transform(MuleMessage originalMessage) {
						MuleMessage newMessage = null;

						try {
							newMessage = originalMessage.createInboundMessage();
							
							HashMap<String, Object> userVehicleMap = new HashMap<>();
							
							userVehicleMap.put("batchNumber",  "0007473958");
							userVehicleMap.put("compliancePlate",  "2016-01");
							userVehicleMap.put("engineNumber",  "1GD8047780");
							userVehicleMap.put("vin",  "JTEBR3FJ60K015246");
							userVehicleMap.put("materialNumber",  "4C78720B0FH221G3");
							userVehicleMap.put("deliveryDate",  "2016-01-09");
							userVehicleMap.put("vehicleDescription",  "Yaris YR 1.3L Petrol Manual 5 Door Hatch");
							userVehicleMap.put("katashikiCode",  "GDJ150R-GKTEYQ");
							userVehicleMap.put("transmission",  "Automatic");
							userVehicleMap.put("sellingDealerCode",  "8224");
							userVehicleMap.put("sellingDealerName",  "MIKE CARNEY TOYOTA");
							userVehicleMap.put("registrationNumber",  "MH664W");
							userVehicleMap.put("userRegistrationNumber",  "MH664W");
							userVehicleMap.put("state",  "VIC");
							userVehicleMap.put("production", "2016-01");
							userVehicleMap.put("discontinuedDate",  1462259108170L);
							userVehicleMap.put("ownershipCreatedDate",  1462259108170L);
							userVehicleMap.put("suffixCode",  null);
							userVehicleMap.put("suffixDescription",  null);
							userVehicleMap.put("trimCode",  null);
							userVehicleMap.put("trimDescription",  null);
							userVehicleMap.put("paintCode",  null);
							userVehicleMap.put("paintDescription",  null);
							userVehicleMap.put("salesType",  null);
							userVehicleMap.put("image",  null);
							userVehicleMap.put("year",  "2016");
							userVehicleMap.put("isVerified",  false);
							
							
							newMessage.setInvocationProperty("userVehicle", userVehicleMap);
							newMessage.setProperty("http.status", 200, PropertyScope.INBOUND);

							newMessage.setPayload(IOUtils
									.toString(getClass().getResourceAsStream("/in/get-vehicle-details-response-for-get-users-me-vehicles.json")));
						} catch (Exception e) {
							// catch exception statements
						}

						return newMessage;
					}
				});

		MuleEvent event = testEvent("");

		runFlow("get:/users/me/vehicles:myToyota-config", event);

		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-user-vehicles.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils")
				.withAttributes(attribute("category").withValue("esb.mytoyota-api-v1.get-user-vehicles.response"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-user-vehicles-technical-adapter.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-user-vehicles-technical-adapter.response"))
				.times(1);
		
		//the first call returns two vehicles, only one is unverified, hence get-vehicle-details is only called once
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-vehicle-details-technical-adapter.request"))
				.times(1);
		verifyCallOfMessageProcessor("log-message").ofNamespace("utils").withAttributes(
				attribute("category").withValue("esb.mytoyota-api-v1.get-vehicle-details-technical-adapter.response"))
				.times(1);
	}
	
	@Test
	public void testFailuresStatusCodes() throws Exception {
		
		int[] statusCodes = new int[] { 404, 405, 415, 406, 401, 500 };	
		
		for (int i = 0; i < statusCodes.length; i++) {
			new StatusCodesTestingHelper().runFailureTest(statusCodes[i], "My Toyota App API GET Retrieve Users Vehicles",
					"get:/users/me/vehicles:myToyota-config");
		}
	}
}
