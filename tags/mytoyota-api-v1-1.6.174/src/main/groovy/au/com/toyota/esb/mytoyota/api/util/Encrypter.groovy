package au.com.toyota.esb.mytoyota.api.util

import javax.crypto.spec.SecretKeySpec;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.Cipher;
import java.nio.charset.StandardCharsets; 
import java.security.MessageDigest; 
import java.security.NoSuchAlgorithmException;

public class Encrypter {

	private static final String CIPHER_ALGORITHM = "AES/CBC/PKCS5Padding";
	private static final String KEY_ALGORITHM = "AES";
	
	    // Key must be exactly 16 bytes
    def static expandKey(secret) {

        for (def i=0; i<4; i++) {

            secret += secret ;
        }

        return secret.substring(0, 16);
    }

    // do the magic
    def static encrypt(plainText, secret) {

        secret = expandKey(secret);
        def cipher = Cipher.getInstance(CIPHER_ALGORITHM, "SunJCE");
        SecretKeySpec key = new SecretKeySpec(secret.getBytes("UTF-8"), KEY_ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(secret.getBytes("UTF-8")));

        return cipher.doFinal(plainText.getBytes("UTF-8")).encodeBase64().toString() ;
    }

    // undo the magic
    def static decrypt(cypherText, secret) {

        byte[] decodedBytes = cypherText.decodeBase64();

        secret = expandKey(secret);
        def cipher = Cipher.getInstance(CIPHER_ALGORITHM, "SunJCE");
        SecretKeySpec key = new SecretKeySpec(secret.getBytes("UTF-8"), KEY_ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(secret.getBytes("UTF-8")));

        return new String(cipher.doFinal(decodedBytes), "UTF-8");
    }
    
    def static hash(plainText, hashAlgo) {

        MessageDigest md = MessageDigest.getInstance(hashAlgo);         
		byte[] hashInBytes = md.digest(plainText.getBytes(StandardCharsets.UTF_8));          
		StringBuilder sb = new StringBuilder();         
		for (byte b : hashInBytes) {             
			sb.append(String.format("%02x", b));         
		}

        return sb.toString();
    }


}
