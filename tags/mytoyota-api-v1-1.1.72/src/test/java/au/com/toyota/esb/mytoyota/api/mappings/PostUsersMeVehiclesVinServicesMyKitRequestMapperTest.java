package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

import javax.script.*;
import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.module.apikit.exception.BadRequestException;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

public class PostUsersMeVehiclesVinServicesMyKitRequestMapperTest extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;

	public PostUsersMeVehiclesVinServicesMyKitRequestMapperTest() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner
				.createScriptRunner("/mappings/post-users-me-vehicles-vin-services-mykit-request-mapper.groovy");
	}
	
	@Test
	public void testSuccessful() throws Exception {
		String expectedResult = IOUtils.toString(
				getClass().getResourceAsStream("/out/post-users-me-vehicles-vin-services-mykit-request.json"));
		String apiRequest = IOUtils
				.toString(getClass().getResourceAsStream("/in/post-users-me-vehicles-vin-services-request.json"));
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload("")
				.flowVar("dmsID","61.10169.10169011116")
				.flowVar("dmsType","TUNE")
				.flowVar("vin","JTMBFREV80D078597")
				.flowVar("dmsBookingId", "TB16392302")
				.sessionVar("apiRequest", apiRequest)
				.run().toString();

		System.out.println(result);
		JSONAssert.assertEquals(expectedResult, result, true);
	}
	
	@Test
	public void testUnsuccessful() throws Exception {
		String apiRequest = IOUtils
				.toString(getClass().getResourceAsStream("/in/post-users-me-vehicles-vin-services-request-unsuccessful.json"));
		try { 
			ScriptRunBuilder
				.runner(scriptRunner)
				.payload("")
				.flowVar("dmsID","61.10169.10169011116")
				.flowVar("dmsType","TUNE")
				.flowVar("vin","JTMBFREV80D078597")
				.flowVar("dmsBookingId", "TB16392302")
				.sessionVar("apiRequest", apiRequest)
				.run().toString();
		} catch (Exception e) {
			if(e.getMessage().contains("Invalid request"))
				assert(true);
			else
				assert(false);
			return;
		}
		assert(false);
	}
}
