import static groovy.json.JsonOutput.*
					
msg = [:]

// Handle missing myToyota ID
if(flowVars['isMissingId'] == true) {
	msg['message'] = 'Invalid request, missing myToyota ID'
	flowVars['statusReason'] = 'Invalid request, missing myToyota ID'
	flowVars['statusCode'] = "400"
} 
// Handle 404 Not Found which can be thrown from myKIT
else if ((message.getProperty("http.status", org.mule.api.transport.PropertyScope.INBOUND)) == 404) {
	msg['message'] = 'Not Found'
	flowVars['statusReason'] = 'Not Found'
	flowVars['statusCode'] = "404"
}
return toJson(msg)