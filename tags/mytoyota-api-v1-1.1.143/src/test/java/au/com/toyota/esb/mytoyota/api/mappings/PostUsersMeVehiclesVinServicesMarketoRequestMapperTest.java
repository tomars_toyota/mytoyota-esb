package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;

import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

public class PostUsersMeVehiclesVinServicesMarketoRequestMapperTest extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;

	public PostUsersMeVehiclesVinServicesMarketoRequestMapperTest() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner
				.createScriptRunner("/mappings/post-users-me-vehicles-vin-services-marketo-request-mapper.groovy");
	}
	
	@Test
	public void testSuccessful() throws Exception {
		String expectedResult = IOUtils.toString(
				getClass().getResourceAsStream("/out/post-users-me-vehicles-vin-services-marketo-request.json"));
		String apiRequest = IOUtils
				.toString(getClass().getResourceAsStream("/in/post-users-me-vehicles-vin-services-request.json"));
		String dealerResponse = IOUtils
				.toString(getClass().getResourceAsStream("/in/get-dealers-by-dealer-code-api-response.json"));
		String result = ScriptRunBuilder
				.runner(scriptRunner)
				.payload("")
				.flowVar("vin","JTMBFREV80D078597")
				.flowVar("myToyotaId","MYT-000000c8")
				.flowVar("dmsBookingId", "TB16392302")
				.sessionVar("apiRequest", apiRequest)
				.sessionVar("dealerResponse", dealerResponse)
				.run().toString();

		System.out.println(result);
		JSONAssert.assertEquals(expectedResult, result, true);
	}
}
