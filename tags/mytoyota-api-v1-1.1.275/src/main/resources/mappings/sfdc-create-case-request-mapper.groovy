import groovy.xml.MarkupBuilder
import groovy.json.JsonSlurper
import java.text.SimpleDateFormat

def writer = new StringWriter()
def xmlMarkup = new MarkupBuilder(writer)

if(payload instanceof org.mule.transport.NullPayload)
	payload = null

def year = Calendar.getInstance().get(Calendar.YEAR).toString();

def subject = year+" MYTOYOTA CHANGE OF OWNERSHIP REQUEST"
def caseDetails = "Please refer to document attached to confirm guest ownership details within myToyota.\nmyToyota ID: "+myToyotaId+"\nVIN: "+vin

xmlMarkup.mkp.xmlDeclaration(version: "1.0", encoding: "UTF-8")
xmlMarkup.'ent:create' (
	'xmlns:ent' : "urn:enterprise.soap.sforce.com",
	'xmlns:urn1' : "urn:sobject.enterprise.soap.sforce.com",
	'xmlns:xsi' : "http://www.w3.org/2001/XMLSchema-instance"
	) {
		'ent:sObjects' ('xsi:type' : "urn1:Case") {
			//'ent:AccountId' (flowVars['myToyotaUserAccountSFDCId'])
			'Origin' ("myToyota Vehicle Ownership")
			//'Case_Details__c' (caseDetails)
			'Description' (caseDetails)
			'ent:RecordTypeId' (recordTypeId)
			'Status' ("New")
			'Subject' (subject)
			//'ent:Vehicle__c' (flowVars['vinSFDCId'])
		}	
	}
//println '__________ payload to salesforce: ' + writer.toString()
return writer.toString()