import groovy.json.JsonSlurper
import groovy.json.JsonBuilder

def request = new JsonSlurper().parseText(payload);
def response = ["myToyotaId": flowVars.myToyotaId];
response << ["campaignCode": request.campaignCode];
response << ["activatedRewards": request.subscriptions.collect({["offerCode":it.subscriptionCode]})];

return new JsonBuilder(response).toString();