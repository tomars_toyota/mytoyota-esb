package au.com.toyota.esb.mytoyota.api.mappings;

import java.io.IOException;
import java.util.Properties;

import javax.script.ScriptException;

import org.apache.commons.io.IOUtils;
import org.junit.Test;
import org.mule.tck.junit4.AbstractMuleContextTestCase;
import org.skyscreamer.jsonassert.JSONAssert;
import org.xml.sax.SAXException;

import au.com.toyota.esb.test.script.ScriptRunBuilder;
import au.com.toyota.esb.test.script.ScriptRunner;

public class GetUsersMeRewardsResponseMapperTest extends
		AbstractMuleContextTestCase {
	private ScriptRunner scriptRunner;
	private Properties props;

	public GetUsersMeRewardsResponseMapperTest() throws ScriptException,
			SAXException, IOException {
		scriptRunner = ScriptRunner.createScriptRunner("/mappings/get-users-me-rewards-response-mapper.groovy");
		//load properties
		props = new Properties();
		props.load(getClass().getResourceAsStream("/mytoyota-api-v1.properties"));
	}

	@Test
	public void testSuccessfulRequestMapper() throws Exception {
		
		String sitecoreHost = props.getProperty("sitecore-app.host");
		String userTypeTMCA = props.getProperty("app.mytoyota.mytoyota-app-api.get.users.me.rewards.usertype.tmca");
		String userTypeGuest = props.getProperty("app.mytoyota.mytoyota-app-api.get.users.me.rewards.usertype.guest");
		Boolean isTMCA = false;
		Boolean addVehicleRequired = false;
		
		String result = ScriptRunBuilder.runner(scriptRunner)
				.payload(IOUtils
						.toString(getClass().getResourceAsStream("/in/get-users-me-rewards-mykit-response.json")))
				.flowVar("scRewards",
						IOUtils.toString(
								getClass().getResourceAsStream("/in/get-users-me-rewards-sitecore-response.json")))
				.flowVar("isTMCA", isTMCA)
				.flowVar("addVehicleRequired", addVehicleRequired)
				.flowVar("myKitClaimedRewards", IOUtils
						.toString(getClass().getResourceAsStream("/in/get-users-me-rewards-mykit-response.json")))
				.binding("sitecoreHost", sitecoreHost)
				.binding("userTypeTMCA", userTypeTMCA)
				.binding("userTypeGuest", userTypeGuest)
				.run().toString();

		System.out.println(result);
		
		// expected
		String expectedResult = IOUtils
				.toString(getClass().getResourceAsStream("/out/get-users-me-rewards-api-response.json"));
		JSONAssert.assertEquals(expectedResult, result, true);
	}
	
	@Test
	public void testSuccessfulTMCAMapper() throws Exception {
		
		String sitecoreHost = props.getProperty("sitecore-app.host");
		String userTypeTMCA = props.getProperty("app.mytoyota.mytoyota-app-api.get.users.me.rewards.usertype.tmca");
		String userTypeGuest = props.getProperty("app.mytoyota.mytoyota-app-api.get.users.me.rewards.usertype.guest");
		Boolean isTMCA = true;
		Boolean addVehicleRequired = false;
		
		String result = ScriptRunBuilder.runner(scriptRunner)
				.payload(IOUtils
						.toString(getClass().getResourceAsStream("/in/get-users-me-rewards-mykit-response.json")))
				.flowVar("scRewards",
						IOUtils.toString(
								getClass().getResourceAsStream("/in/get-users-me-rewards-sitecore-response.json")))
				.flowVar("myKitClaimedRewards", IOUtils
						.toString(getClass().getResourceAsStream("/in/get-users-me-rewards-mykit-response.json")))
				.flowVar("isTMCA", isTMCA)
				.flowVar("addVehicleRequired", addVehicleRequired)
				.binding("sitecoreHost", sitecoreHost)
				.binding("userTypeTMCA", userTypeTMCA)
				.binding("userTypeGuest", userTypeGuest)
				.run().toString();

		System.out.println(result);
		
		// expected
		String expectedResult = IOUtils
				.toString(getClass().getResourceAsStream("/out/get-users-me-rewards-tmca-api-response.json"));
		JSONAssert.assertEquals(expectedResult, result, true);
	}
	
	@Test
	public void testSuccessfulAddVehicleRequired() throws Exception {
		
		String sitecoreHost = props.getProperty("sitecore-app.host");
		String userTypeTMCA = props.getProperty("app.mytoyota.mytoyota-app-api.get.users.me.rewards.usertype.tmca");
		String userTypeGuest = props.getProperty("app.mytoyota.mytoyota-app-api.get.users.me.rewards.usertype.guest");
		Boolean isTMCA = false;
		Boolean addVehicleRequired = true;
		
		String result = ScriptRunBuilder.runner(scriptRunner)
				.payload(IOUtils
						.toString(getClass().getResourceAsStream("/in/get-users-me-rewards-mykit-response.json")))
				.flowVar("scRewards",
						IOUtils.toString(
								getClass().getResourceAsStream("/in/get-users-me-rewards-sitecore-response.json")))
				.flowVar("myKitClaimedRewards", IOUtils
						.toString(getClass().getResourceAsStream("/in/get-users-me-rewards-mykit-response.json")))
				.flowVar("isTMCA", isTMCA)
				.flowVar("addVehicleRequired", addVehicleRequired)
				.binding("sitecoreHost", sitecoreHost)
				.binding("userTypeTMCA", userTypeTMCA)
				.binding("userTypeGuest", userTypeGuest)
				.run().toString();

		System.out.println(result);
		
		// expected
		String expectedResult = IOUtils
				.toString(getClass().getResourceAsStream("/out/get-users-me-rewards-require-add-vehicle-api-response.json"));
		JSONAssert.assertEquals(expectedResult, result, false);
	}
}
