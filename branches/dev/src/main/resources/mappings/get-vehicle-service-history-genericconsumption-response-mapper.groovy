import static groovy.json.JsonOutput.*
import groovy.json.JsonSlurper
import static au.com.toyota.esb.mytoyota.api.util.GroovyHelper.*
import java.text.*


def serviceHistoryResponse = null
def serviceHistoryList = []
def response = [:]

if (payload == null || payload == org.mule.transport.NullPayload) {
    return payload
}

def dateFormat = "yyyy-MM-dd";
SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd HHmmss.SSS")
SimpleDateFormat sdf = new SimpleDateFormat(dateFormat)

serviceHistoryResponse = new JsonSlurper().parseText(payload)

//println("serviceHistory vin: " + vin)
//println("serviceHistory queryStartDate: " + queryStartDate)
//println("serviceHistory queryEndDate: " + queryEndDate)

def startDate = isValidDate(queryStartDate, dateFormat) ? sdf.parse(queryStartDate) : null
def endDate = isValidDate(queryEndDate, dateFormat) ? sdf.parse(queryEndDate) : null

serviceHistoryResponse?.each { Map valueItem ->

	def invoiceDateTime = null
	def invoiceDate = null

	// get first Retail invoiceDateTime
	def invoiceItem = valueItem?.invoices.find { it?.invoiceType == 'Retail' }

	try {
		invoiceDateTime = invoiceItem?.invoiceDateTimeOfIssue
		invoiceDate = sdf.format(df.parse(invoiceItem?.invoiceDateTimeOfIssue))
	} catch (Exception e) {
		invoiceDate = null
	}

	// filter startDate / endDate
	def inputDate = isValidDate(invoiceDate, dateFormat) ? sdf.parse(invoiceDate) : null

	// do not return null invoice date in payload
	if (!isDateInRange(inputDate, startDate, endDate)) {
		return
	}
		def filteredServices = valueItem?.jobs.findAll{it?.serviceOperation?.category == 'PM' || (it?.serviceOperation?.category == 'Other' && it?.serviceOperation?.serviceType == 'Miscellaneous')} 
    def service = [:]

  service.invoiceDateTime = invoiceDateTime
  service.invoiceDate = invoiceDate

  service.dealerCode = valueItem?.location?.tmcaDealerCode
  service.dealerName = valueItem?.location?.dealerName
  service.servicingDealerBranchCode = valueItem?.location?.tmcaBranchCode
  service.servicingDealerBranchName = valueItem?.location?.branchName

  service.odometerReadingKm = valueItem?.vehicle?.mileage

  service.repairOrderNumber = valueItem?.repairOrderNumber
  def jobs = []
  filteredServices.each { Map jobItem ->
  
  if(jobItem?.serviceOperation?.category == 'PM')
  {
  service.serviceCode = jobItem?.serviceOperation?.code  
  service.serviceDescription = jobItem?.serviceOperation?.description
  service.serviceType = jobItem?.serviceOperation?.serviceType
  service.category = jobItem?.serviceOperation?.category

  if(service.category == 'PM') {
   service.categoryDescription = 'Scheduled Logbook Service'
  }
}
  def job = [:]
  
  //per job
  job.serviceCode = jobItem?.serviceOperation?.code  
  job.serviceDescription = jobItem?.serviceOperation?.description
  job.serviceType = jobItem?.serviceOperation?.serviceType
  job.category = jobItem?.serviceOperation?.category

  if(job.category == 'PM') {
   job.categoryDescription = 'Scheduled Logbook Service'
  }
  else if (job.category == 'Other' && job.serviceType  == 'Miscellaneous') {
   job.categoryDescription = 'Extras Service'
  }
  jobs << job
    service.jobs = jobs

}
  if(service && service.category != null)
  {
   serviceHistoryList << service
  }

}

response.vin = vin

if(serviceHistoryList?.size() > 0) {
	//sort ascending order
	serviceHistoryList = removeNulls(serviceHistoryList, true)
	response.services = serviceHistoryList.sort { it.invoiceDateTime }
} else {
	response.services = serviceHistoryList
}

return prettyPrint(toJson(response))
