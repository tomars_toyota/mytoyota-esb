package au.com.toyota.esb.mytoyota.api.munit;

import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

import org.mule.api.MuleEvent;
import org.mule.api.MuleException;

import au.com.toyota.esb.test.PayloadSpy;

public class BlockingMuleEventSpy<T> extends PayloadSpy<T> {

	public final static long DEFAULT_TIMEOUT = 10000;

	private MuleEvent event;

	private CountDownLatch latch;
	private long timeout;

	public BlockingMuleEventSpy(int count) {
		this(count, DEFAULT_TIMEOUT);
	}

	public BlockingMuleEventSpy(int count, long timeout) {
		super();
		this.latch = new CountDownLatch(count);
		this.timeout = timeout;
	}

	@Override
	public void spy(MuleEvent event) throws MuleException {
		super.spy(event);
		latch.countDown();
		this.event = event;
	}

	public void block() throws InterruptedException {
		latch.await(timeout, TimeUnit.MILLISECONDS);
	}

	public void block(long timeout) throws InterruptedException {
		latch.await(timeout, TimeUnit.MILLISECONDS);
	}

	public MuleEvent getMuleEvent() {
		return this.event;
	}

}
