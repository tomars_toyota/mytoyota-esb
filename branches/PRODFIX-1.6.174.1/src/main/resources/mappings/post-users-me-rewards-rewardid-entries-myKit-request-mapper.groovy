import groovy.json.JsonSlurper
import groovy.json.JsonBuilder
import au.com.toyota.esb.mytoyota.api.util.GroovyHelper

def entryPayload = new JsonSlurper().parseText(payload)
def response = [:]
response << entryPayload



return new JsonBuilder(response).toString()