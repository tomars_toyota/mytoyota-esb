def lowerCaseEmail = new groovy.json.JsonSlurper().parseText(payload)?.email.toLowerCase()
flowVars['isTMCA'] = lowerCaseEmail.endsWith(tmcaStaffDomain) ? true : false