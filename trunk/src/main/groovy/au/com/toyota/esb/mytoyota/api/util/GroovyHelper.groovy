package au.com.toyota.esb.mytoyota.api.util
import java.text.SimpleDateFormat

public class GroovyHelper {


	def static DATE_FMT = "yyyy-MM-dd'T'HH:mm:ss'Z'"

	def static removeNulls(col) {
	    if (col instanceof List)
	        return col.collect { removeNulls(it) }.findAll { it != null && it != '' && it != [:] && it != []}
	    else if (col instanceof Map) 
	        return col.collectEntries { [(it.key): removeNulls(it.value) ] }.findAll { it.value != null && it.value != '' && it.value != [] && it.value != [:]}
	    else return col
	}

	def static removeNulls(col, ignoreBlanks) {
	    if(!ignoreBlanks)
	        if (col instanceof List)
	            return col.collect { removeNulls(it) }.findAll { it != null && it != '' && it != [:] && it != []}
	        else if (col instanceof Map) 
	            return col.collectEntries { [(it.key): removeNulls(it.value) ] }.findAll { it.value != null && it.value != '' && it.value != [] && it.value != [:]}
	        else return col
	    else
	        if (col instanceof List)
	            return col.collect { removeNulls(it, ignoreBlanks) }.findAll { it != null && it != [:] && it != []}
	        else if (col instanceof Map) 
	            return col.collectEntries { [(it.key): removeNulls(it.value, ignoreBlanks) ] }.findAll { it.value != null && it.value != [] && it.value != [:]}
	        else return col
	}


	def static isValidDate(String dateString, String dateFormat) {
	    return parseDate(dateString, dateFormat) != null        
	}
	
	def static isValidDate(String dateString) {
	  	return isValidDate(dateString, DATE_FMT)
	}
	
	def static parseDate(String dateString, String dateFormat) {
		SimpleDateFormat df = new SimpleDateFormat(dateFormat)
		try {
	        return df.parse(dateString)
	    } catch (Exception e) {
	        return null
	    }
	}
	
	def static parseDate(String dateString) {
	    return parseDate(dateString, DATE_FMT)
	}
	

	def static formatDate(Date date, String dateFormat) {
		SimpleDateFormat df = new SimpleDateFormat(dateFormat)
	    try {
	        return df.format(date)
	    } catch (Exception e) {
	        return null
	    }
	}
	
	def static formatDate(Date date) {
	    return formatDate(date, DATE_FMT)
	}
	
	def static formatDate(String dateString, String inputDateFormat, String dateFormat) {
	    try {
	        return parseDate(dateString, inputDateFormat).format(dateFormat)
	    } catch (Exception e) {
	        return null
	    }
	}
	
	def static formatDate(String dateString, String dateFormat) {
	    try {
	        return formatDate(parseDate(dateString), dateFormat)
	    } catch (Exception e) {
	        return null
	    }
	}
	
	def static formatDate(String dateString) {
	    return formatDate(dateString, DATE_FMT)
	}

	def static isDateInRange(Date inputDate, Date startDate, Date endDate) {
		if (inputDate == null) {
			return false
		}
		if (startDate != null && endDate != null) {
			return startDate <= inputDate && inputDate <= endDate
		} else if (startDate != null && endDate == null) {
			return startDate <= inputDate
		} else if (startDate == null && endDate != null) {
			return inputDate <= endDate
		} else {
			return true
		}
	}

}